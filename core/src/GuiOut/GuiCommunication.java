package GuiOut;
import com.thdgaming.naruto.MGraphics;

import Gui.iCommand;
import screen.old.GameScr;
import lib.mVector;
import model.Command;

public class GuiCommunication { // giao diện giao tiếp NPC dùng dialog thể hiện thông tin
	public static int menuX, menuY, menuW, menuH, menuTemY, hPlus; // thông số hiển thị 
	public static int w;
	public static Command cmd1, cmd2, cmd3;
	public static RunText runtext = new RunText();
	public static String infoText =  null; // sửa tạm cho hết lỗi câu nói xàm của NPC
	public static int archorRunText;
	public static mVector menuItems;
	public static int[] color = { 0xffa89982, 0xffbaaa92, 0xfff8b848,
		0xffebebeb, 0xffa69780, 0xffc7b59c, 0xffb6a58e, 0xff706354,
		0xffe3d5be, 0xffaf9f89, 0xfff8f8a8 };
	 //  dialog 
	
	public static void paintTabNew(MGraphics g, int xTab, int yTab, int wTab,
                                   int hTab, boolean ismore, byte colorBack) {
//		if (GameCanvas.lowGraphic) {
//			paintDialog(graphic, xTab, yTab, wTab, hTab, colorBack);
//		} else {
			if (hTab < 32)
				hTab = 32;
			g.setColor(color[0]);
			g
					.fillRect(xTab + loadImageInterface.imgLine[0].getWidth() - 2, yTab + 3, wTab - 2 * loadImageInterface.imgLine[0].getWidth() + 4,
							hTab - 5);
			g
					.fillRect(xTab + 4, yTab + loadImageInterface.imgLine[0].getWidth() - 2, wTab - 8, hTab - 2
							* loadImageInterface.imgLine[0].getWidth() + 4);
			g.setColor(color[1]);
			g
					.fillRect(xTab + loadImageInterface.imgLine[0].getWidth() - 2, yTab + 4, wTab - 2 * loadImageInterface.imgLine[0].getWidth() + 4,
							hTab - 7);
			g.fillRect(xTab + 5, yTab + loadImageInterface.imgLine[0].getWidth() - 2, wTab - 10, hTab - 2 * loadImageInterface.imgLine[0].getWidth()
					+ 4);
			g.setColor(color[0]);
			g
					.fillRect(xTab + loadImageInterface.imgLine[0].getWidth() - 2, yTab + 5, wTab - 2 * loadImageInterface.imgLine[0].getWidth() + 4,
							hTab - 9);
			g.fillRect(xTab + 6, yTab + loadImageInterface.imgLine[0].getWidth() - 2, wTab - 12, hTab - 2 * loadImageInterface.imgLine[0].getWidth()
					+ 4);
			g.setColor(color[2]);
			g.fillRect(xTab + 7, yTab + 6, wTab - 14, hTab - 12);
			for (int i = 0; i <= (wTab - 15) / 32; i++) {
				for (int j = 0; j <= (hTab - 11) / 32; j++) {
					if (i == (wTab - 15) / 32) {
						if (j == (hTab - 11) / 32) {
							g.drawImage(loadImageInterface.imgInside[colorBack], xTab
									+ wTab - 39, yTab + hTab - 37, 0);
						} else {
							g.drawImage(loadImageInterface.imgInside[colorBack], xTab
									+ wTab - 39, yTab + 7 + j * 32, 0);
						}
					} else {
						if (j == (hTab - 11) / 32) {
							g.drawImage(loadImageInterface.imgInside[colorBack], xTab + 8
									+ i * 32, yTab + hTab - 37, 0);
						} else {
							g.drawImage(loadImageInterface.imgInside[colorBack], xTab + 8
									+ i * 32, yTab + 7 + j * 32, 0);
						}
					}
				}
			}
			g.drawImage(loadImageInterface.imgLine[0], xTab, yTab, 0);
			g.drawRegion(loadImageInterface.imgLine[0], 0, 0, loadImageInterface.imgLine[0].getWidth(), loadImageInterface.imgLine[0].getWidth(), 2, xTab + wTab - loadImageInterface.imgLine[0].getWidth(), yTab,
					0);
			g.drawImage(loadImageInterface.imgLine[1], xTab + 2, yTab + hTab - loadImageInterface.imgLine[1].getWidth(), 0);
			g.drawRegion(loadImageInterface.imgLine[1], 0, 0, 30, 30, 2, xTab + wTab - 32, yTab + hTab
					- loadImageInterface.imgLine[1].getWidth(), 0);
			if (ismore)
				g.drawImage(loadImageInterface.imgLine[2], xTab + wTab / 2, yTab + 2, 3);
//		}

	}
	public static void paint(MGraphics g){
		int xpaint = menuX + 6, ypaint = menuY + 8;
		// graphic.drawImage(img, xpaint+5, menuY, MGraphics.BOTTOM|MGraphics.LEFT);
//		paintDialog(graphic, menuX, menuTemY, w, menuH, 12);
		paintDialog(g, 100, 100 , 200, 100, 12); // pain khung giao tiep voi NPC 
//		MainObject obj = MainObject.get_Object(IdNpc, typeO);
//		if (obj == null)
//			return;
//		obj.paintBigAvatar(graphic, menuX + w - 10, menuY);
//		Font3dWhite(graphic, obj.name, xpaint + 10, ypaint, 0);
		if (runtext != null)
			runtext.paintText(g, archorRunText);
		GameScr.resetTranslate(g);
		for (int i = 0; i < menuItems.size(); i++) {
			iCommand cmd = (iCommand) menuItems.elementAt(i);
			cmd.paint(g, cmd.xCmd, cmd.yCmd);
		}
		

	}
	

	static int[] colorDia = { 0xff782a12, 0xfff8f8a8, 0xff510600, 0xfff8b848,
		0xfff8b848 };
	public static void paintDialog(MGraphics g, int xDia, int yDia, int wDia,
                                   int hDia, int Indexcolor) {
		if (wDia < 35)
			wDia = 35;
		int numw = (wDia - 6) / 32;
		int numh = (hDia - 6) / 32;
		if (hDia % 2 != 0)
			hDia += 1;
			if (hDia < 32) {
				for (int i = 0; i <= numw; i++) {
					for (int j = 0; j <= numh; j++) {
						if (i == numw) {
							if (j == numh) {
								g.drawRegion(loadImageInterface.imgInside[Indexcolor], 0,
										0, 32, hDia, 0, xDia - 3 + wDia - 32,
										yDia, 0);
							} else
								g.drawRegion(loadImageInterface.imgInside[Indexcolor], 0,
										0, 32, hDia, 0, xDia - 3 + wDia - 32,
										yDia + 3 + 32 * j, 0);
						} else {
							if (j == numh) {
								g.drawRegion(loadImageInterface.imgInside[Indexcolor], 0,
										0, 32, hDia, 0, xDia + 3 + i * 32,
										yDia, 0);
							} else
								g.drawRegion(loadImageInterface.imgInside[Indexcolor], 0,
										0, 32, hDia, 0, xDia + 3 + i * 32, yDia
												+ 3 + 32 * j, 0);
						}
					}
				}
			} else {
				for (int i = 0; i <= numw; i++) {
					for (int j = 0; j <= numh; j++) {
						if (i == numw) {
							if (j == numh) {
								g.drawImage(loadImageInterface.imgInside[Indexcolor], xDia
										- 3 + wDia - 32, yDia - 3 + hDia - 32,
										0);
							} else
								g.drawImage(loadImageInterface.imgInside[Indexcolor], xDia
										- 3 + wDia - 32, yDia + 3 + 32 * j, 0);
						} else {
							if (j == numh) {
								g.drawImage(loadImageInterface.imgInside[Indexcolor], xDia
										+ 3 + i * 32, yDia - 3 + hDia - 32, 0);
							} else
								g.drawImage(loadImageInterface.imgInside[Indexcolor], xDia
										+ 3 + i * 32, yDia + 3 + 32 * j, 0);
						}
					}
			}

			g.drawRegion(loadImageInterface.imgPopup, 0, 0, 5, 5, 0, xDia, yDia, 0);
			g.drawRegion(loadImageInterface.imgPopup, 0, 5, 5, 5, 0, xDia + wDia - 5, yDia, 0);
			g.drawRegion(loadImageInterface.imgPopup, 0, 15, 5, 5, 0, xDia, yDia + hDia - 5, 0);
			g.drawRegion(loadImageInterface.imgPopup, 0, 10, 5, 5, 0, xDia + wDia - 5, yDia + hDia
					- 5, 0);

			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia, wDia - 6, 1);
			g.fillRect(xDia, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[1]);
			g.fillRect(xDia + 3, yDia + 1, wDia - 6, 1);
			g.fillRect(xDia + 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + 2, wDia - 6, 1);
			g.fillRect(xDia + 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + hDia - 1, wDia - 6, 1);
			g.fillRect(xDia + wDia - 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[4]);
			g.fillRect(xDia + 3, yDia + hDia - 2, wDia - 6, 1);
			g.fillRect(xDia + wDia - 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia + hDia - 3, wDia - 6, 1);
			g.fillRect(xDia + wDia - 3, yDia + 3, 1, hDia - 6);
		}
	}
	
	static int[] colorLow = { 0xffd6c7ae, 0xff9c8c77, 0xff706354, 0xff887a67,
		0xff4b4339 };
	
	public static void paintRectLowGraphic(MGraphics g, int x, int y, int w,
                                           int h, int indexColor) {
		g.setColor(colorLow[indexColor]);
		g.fillRect(x, y, w, h);

	}
	
	
}
