package GuiOut;

import real.mFont;
import screen.old.GameScr;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import model.Command;



public class AvMain {
	
	public Command left, right, center;
	public static Bitmap[] tab = new Bitmap[4];
	public static int wimg;
	public static Bitmap imgtextfield, imgInfo,
			imghpmp, imgcolorhpmp, imgPopup, imgBackInfo, imgWorldMap,
			imgSelect, imgFocusMap, imgDelaySkill, imgLoadImg, imgEyeDie,
			imgHotKey, imgHotKey2, imgMess, imgColorItem, imgLogo,imgicongt;
	public static FrameImage fraPlayerDie, fraObjMiniMap, fraPk, fraStar,
			fraQuest,fraMonSample;
	public static Bitmap[] textf = new Bitmap[2];
	public static final int SELECTED_COLOR = 0xff9c8c77;

	public void paint(MGraphics g) {
		GameScr.resetTranslate(g);
//		if (GameCanvas.currentDialog == null && !GameCanvas.menu2.isShowMenu) {
//			paintCmd(graphic);
//		}
	}

	public void update() {
	}

	public void keypress(int keyCode) {

	}

	public void paintCmd(MGraphics g) {
		
//			if (left != null) {
//				left.paint(graphic, GameCanvas.wCommand, GameCanvas.h
//						- iCommand.hButtonCmd / 2 - 1);
//			}
//			if (right != null) {
//				right.paint(graphic, GameCanvas.w - GameCanvas.wCommand, GameCanvas.h
//						- iCommand.hButtonCmd / 2 - 1);
//			}
//			if (center != null) {
//				center.paint(graphic, GameCanvas.hw, GameCanvas.h
//						- iCommand.hButtonCmd / 2 - 1);
//			}
		
	}

	public void paintCmd_OnlyText(MGraphics g) {
//		if (GameCanvas.menu2.isShowMenu || GameCanvas.currentDialog != null)
//			return;
//		GameCanvas.resetTrans(graphic);
//		if (GameCanvas.isSmallScreen)
//			paintCmd_OnlyText_Small(graphic);
//		else {
//			if (left != null) {
//				Font3dWhite(graphic, left.caption, 30, GameCanvas.h
//						- GameCanvas.hCommand / 2 - 4, 2);
//			}
//			if (right != null) {
//				Font3dWhite(graphic, right.caption, GameCanvas.w - 30, GameCanvas.h
//						- GameCanvas.hCommand / 2 - 4, 2);
//			}
//			if (center != null) {
//				Font3dWhite(graphic, center.caption, GameCanvas.hw, GameCanvas.h
//						- GameCanvas.hCommand / 2 - 4, 2);
//			}
//		}
	}

	public void paintCmdSmall(MGraphics g) {
		GameScr.resetTranslate(g);
//		if (left != null) {
//			left.paint(graphic, GameCanvas.wCommand, GameCanvas.h
//					- iCommand.hButtonCmd / 2 - 1);
//		}
//		if (right != null) {
//			right.paint(graphic, GameCanvas.w - GameCanvas.wCommand, GameCanvas.h
//					- iCommand.hButtonCmd / 2 - 1);
//		}
//		if (center != null) {
//			center.paint(graphic, GameCanvas.hw, GameCanvas.h - iCommand.hButtonCmd
//					/ 2 - 1);
//		}
	}

	public void paintCmd_OnlyText_Small(MGraphics g) {
//		if (left != null) {
//			mFont.tahoma_7b_white.drawString(graphic, left.caption, 27, GameCanvas.h
//					- GameCanvas.hCommand / 2 + 1, 2);
//		}
//		if (right != null) {
//			mFont.tahoma_7b_white.drawString(graphic, right.caption,
//					GameCanvas.w - 27, GameCanvas.h - GameCanvas.hCommand / 2
//							+ 1, 2);
//		}
//		if (center != null) {
//			mFont.tahoma_7b_white.drawString(graphic, center.caption, GameCanvas.hw,
//					GameCanvas.h - GameCanvas.hCommand / 2 + 1, 2);
//		}
	}

	public void commandTab(int index, int subIndex) {

	}

	public void commandMenu(int index, int subIndex) {

	}

	public void commandPointer(int index, int subIndex) {

	}

	public void paintRect(MGraphics g, int x, int y, int w, int h,
                          boolean isSelect) {
		g.setColor(0);
		g.fillRect(x, y, w, h);
		g.setColor(0xff10634d);
		if (isSelect)
			g.setColor(0xffffffff);
		g.fillRect(x + 2, y + 2, w - 4, h - 4);
	}

	public void paintRect(MGraphics g, int x, int y, int w, int h) {
		g.setColor(0);
		g.fillRect(x, y, w, h);
		g.setColor(0xff10634d);
		g.fillRect(x + 2, y + 2, w - 4, h - 4);
	}

	public void paintRect(MGraphics g, int x, int y, int w, int h, int color,
                          int color2) {
		g.setColor(color);
		g.fillRect(x, y, w, h);
		g.setColor(color2);
		g.fillRect(x + 2, y + 2, w - 4, h - 4);
	}

	public static int color[] = { 0xff481810, 0xfff8b848, 0xff989078,
			0xffe0d0b8, 0xff9a8870 };

	static int[] colorDia = { 0xff782a12, 0xfff8f8a8, 0xff510600, 0xfff8b848,
			0xfff8b848 };

	public static void paintDialog(MGraphics g, int xDia, int yDia, int wDia,
                                   int hDia, int Indexcolor) {
		if (wDia < 35)
			wDia = 35;
		int numw = (wDia - 6) / 32;
		int numh = (hDia - 6) / 32;
		if (hDia % 2 != 0)
			hDia += 1;
		if (GameCanvas.lowGraphic) {
			if (Indexcolor > 2) {
				if (Indexcolor == 8 || Indexcolor == 12)
					Indexcolor = 4;
				else
					Indexcolor = 3;
			}
			g.setColor(colorDia[0]);
			g.fillRect(xDia, yDia, wDia, hDia);
			g.setColor(colorDia[4]);
			g.drawRect(xDia + 1, yDia + 1, wDia - 3, hDia - 3);
			MainTabNew.paintRectLowGraphic(g, xDia + 3, yDia + 3, wDia-6, hDia-6,
					Indexcolor);
		} else {
			if (hDia < 32) {
				for (int i = 0; i <= numw; i++) {
					for (int j = 0; j <= numh; j++) {
						if (i == numw) {
							if (j == numh) {
								g.drawRegion(MainTabNew.imgTab[Indexcolor], 0,
										0, 32, hDia, 0, xDia - 3 + wDia - 32,
										yDia, 0);
							} else
								g.drawRegion(MainTabNew.imgTab[Indexcolor], 0,
										0, 32, hDia, 0, xDia - 3 + wDia - 32,
										yDia + 3 + 32 * j, 0);
						} else {
							if (j == numh) {
								g.drawRegion(MainTabNew.imgTab[Indexcolor], 0,
										0, 32, hDia, 0, xDia + 3 + i * 32,
										yDia, 0);
							} else
								g.drawRegion(MainTabNew.imgTab[Indexcolor], 0,
										0, 32, hDia, 0, xDia + 3 + i * 32, yDia
												+ 3 + 32 * j, 0);
						}
					}
				}
			} else {
				for (int i = 0; i <= numw; i++) {
					for (int j = 0; j <= numh; j++) {
						if (i == numw) {
							if (j == numh) {
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										- 3 + wDia - 32, yDia - 3 + hDia - 32,
										0);
							} else
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										- 3 + wDia - 32, yDia + 3 + 32 * j, 0);
						} else {
							if (j == numh) {
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										+ 3 + i * 32, yDia - 3 + hDia - 32, 0);
							} else
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										+ 3 + i * 32, yDia + 3 + 32 * j, 0);
						}
					}
				}
			}

			g.drawRegion(imgPopup, 0, 0, 5, 5, 0, xDia, yDia, 0);
			g.drawRegion(imgPopup, 0, 5, 5, 5, 0, xDia + wDia - 5, yDia, 0);
			g.drawRegion(imgPopup, 0, 15, 5, 5, 0, xDia, yDia + hDia - 5, 0);
			g.drawRegion(imgPopup, 0, 10, 5, 5, 0, xDia + wDia - 5, yDia + hDia
					- 5, 0);

			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia, wDia - 6, 1);
			g.fillRect(xDia, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[1]);
			g.fillRect(xDia + 3, yDia + 1, wDia - 6, 1);
			g.fillRect(xDia + 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + 2, wDia - 6, 1);
			g.fillRect(xDia + 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + hDia - 1, wDia - 6, 1);
			g.fillRect(xDia + wDia - 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[4]);
			g.fillRect(xDia + 3, yDia + hDia - 2, wDia - 6, 1);
			g.fillRect(xDia + wDia - 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia + hDia - 3, wDia - 6, 1);
			g.fillRect(xDia + wDia - 3, yDia + 3, 1, hDia - 6);
		}
	}

	public static void paintDialogNew(MGraphics g, int xDia, int yDia,
                                      int wDia, int hDia, int Indexcolor) {
		if (GameCanvas.lowGraphic) {
			paintDialog(g, xDia, yDia, wDia, hDia, Indexcolor);
		} else {
			int numw = (wDia - 6) / 32;
			int numh = (hDia - 6) / 32;
			if (hDia % 2 != 0)
				hDia += 1;
			for (int i = 0; i <= numw; i++) {
				for (int j = 0; j <= numh; j++) {
					if (j <= 1 || j == numh || i == 0 || i == numw) {
						if (i == numw) {
							if (j == numh) {
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										- 3 + wDia - 32, yDia - 3 + hDia - 32,
										0);
							} else
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										- 3 + wDia - 32, yDia + 3 + 32 * j, 0);
						} else {
							if (j == numh) {
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										+ 3 + i * 32, yDia - 3 + hDia - 32, 0);
							} else
								g.drawImage(MainTabNew.imgTab[Indexcolor], xDia
										+ 3 + i * 32, yDia + 3 + 32 * j, 0);
						}
					}
				}
			}
			g.drawRegion(imgPopup, 0, 0, 5, 5, 0, xDia, yDia, 0);
			g.drawRegion(imgPopup, 0, 5, 5, 5, 0, xDia + wDia - 5, yDia, 0);
			g.drawRegion(imgPopup, 0, 15, 5, 5, 0, xDia, yDia + hDia - 5, 0);
			g.drawRegion(imgPopup, 0, 10, 5, 5, 0, xDia + wDia - 5, yDia + hDia
					- 5, 0);
			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia, wDia - 6, 1);
			g.fillRect(xDia, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[1]);
			g.fillRect(xDia + 3, yDia + 1, wDia - 6, 1);
			g.fillRect(xDia + 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + 2, wDia - 6, 1);
			g.fillRect(xDia + 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[2]);
			g.fillRect(xDia + 3, yDia + hDia - 1, wDia - 6, 1);
			g.fillRect(xDia + wDia - 1, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[4]);
			g.fillRect(xDia + 3, yDia + hDia - 2, wDia - 6, 1);
			g.fillRect(xDia + wDia - 2, yDia + 3, 1, hDia - 6);
			g.setColor(colorDia[0]);
			g.fillRect(xDia + 3, yDia + hDia - 3, wDia - 6, 1);
			g.fillRect(xDia + wDia - 3, yDia + 3, 1, hDia - 6);
		}
	}

	public static void paintTabNew(MGraphics g, int xTab, int yTab, int wTab,
                                   int hTab, boolean ismore, byte colorBack) {
		if (GameCanvas.lowGraphic) {
			paintDialog(g, xTab, yTab, wTab, hTab, colorBack);
		} else {
			if (hTab < 32)
				hTab = 32;
			g.setColor(color[0]);
			g
					.fillRect(xTab + wimg - 2, yTab + 3, wTab - 2 * wimg + 4,
							hTab - 5);
			g
					.fillRect(xTab + 4, yTab + wimg - 2, wTab - 8, hTab - 2
							* wimg + 4);
			g.setColor(color[1]);
			g
					.fillRect(xTab + wimg - 2, yTab + 4, wTab - 2 * wimg + 4,
							hTab - 7);
			g.fillRect(xTab + 5, yTab + wimg - 2, wTab - 10, hTab - 2 * wimg
					+ 4);
			g.setColor(color[0]);
			g
					.fillRect(xTab + wimg - 2, yTab + 5, wTab - 2 * wimg + 4,
							hTab - 9);
			g.fillRect(xTab + 6, yTab + wimg - 2, wTab - 12, hTab - 2 * wimg
					+ 4);
			g.setColor(color[2]);
			g.fillRect(xTab + 7, yTab + 6, wTab - 14, hTab - 12);
			for (int i = 0; i <= (wTab - 15) / 32; i++) {
				for (int j = 0; j <= (hTab - 11) / 32; j++) {
					if (i == (wTab - 15) / 32) {
						if (j == (hTab - 11) / 32) {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab
									+ wTab - 39, yTab + hTab - 37, 0);
						} else {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab
									+ wTab - 39, yTab + 7 + j * 32, 0);
						}
					} else {
						if (j == (hTab - 11) / 32) {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab + 8
									+ i * 32, yTab + hTab - 37, 0);
						} else {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab + 8
									+ i * 32, yTab + 7 + j * 32, 0);
						}
					}
				}
			}
			g.drawImage(tab[0], xTab, yTab, 0);
			g.drawRegion(tab[0], 0, 0, wimg, wimg, 2, xTab + wTab - wimg, yTab,
					0);
			g.drawImage(tab[1], xTab + 2, yTab + hTab - wimg, 0);
			g.drawRegion(tab[1], 0, 0, 30, 30, 2, xTab + wTab - 32, yTab + hTab
					- wimg, 0);
			if (ismore)
				g.drawImage(tab[2], xTab + wTab / 2, yTab + 2, 3);
		}

	}

	public static void paintRectNice(MGraphics g, int xTab, int yTab, int wTab,
                                     int hTab, byte colorBack) {
		if(hTab%2==1)
			hTab+=1;
		if (GameCanvas.lowGraphic) {
			if (colorBack > 2) {
				if (colorBack == 8 || colorBack == 12)
					colorBack = 4;
				else
					colorBack = 3;
			}
			MainTabNew
					.paintRectLowGraphic(g, xTab, yTab, wTab, hTab, colorBack);
		} else {
			for (int i = 0; i <= (wTab) / 32; i++) {
				for (int j = 0; j <= (hTab) / 32; j++) {
					if (i == (wTab) / 32) {
						if (j == (hTab) / 32) {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab
									+ wTab - 32, yTab + hTab - 32, 0);
						} else {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab
									+ wTab - 32, yTab + j * 32, 0);
						}
					} else {
						if (j == (hTab) / 32) {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab + i
									* 32, yTab + hTab - 32, 0);
						} else {
							g.drawImage(MainTabNew.imgTab[colorBack], xTab + i
									* 32, yTab + j * 32, 0);
						}
					}
				}
			}
		}
	}

	public static void fill(int x, int y, int w, int h, int color, MGraphics g) {
		g.setColor(color);
		g.fillRect(x, y, w, h);
	}

	public static void paintRectText(MGraphics g, int xText, int yText,
                                     int wText, int hText, boolean isFocus) {
		g.setColor(0xffc09860);
		if (isFocus)
			g.setColor(0xfffdf9d8);
		g.drawRegion(imgtextfield, 0, isFocus ? 30 : 0, 4, 15, 0, xText, yText,
				0);
		g.drawRegion(imgtextfield, 0, (isFocus ? 30 : 0) + 15, 4, 15, 0, xText
				+ wText - 4, yText + hText - 15, 0);
		g.drawRegion(imgtextfield, 0, (isFocus ? 30 : 0) + 11, 4, 4, 0, xText,
				yText + hText - 4, 0);
		g.drawRegion(imgtextfield, 0, (isFocus ? 30 : 0) + 15, 4, 4, 0, xText
				+ wText - 4, yText, 0);
		g.fillRect(xText + 4, yText, wText - 8, hText);
		g.fillRect(xText, yText + 4, wText, hText - 8);
	}

	public void paintSelect(MGraphics g, int x, int y, int w, int h) {
		g.setColor(SELECTED_COLOR);
		g.fillRect(x, y, w, h);
	}

	public void paintFormList(MGraphics g, int x, int y, int w, int h,
                              String name) {
		paintDialogNew(g, x - 6, y - 6, w + 12, h + 12, 0);
		paintRectNice(g, x, y + GameCanvas.hCommand, w,
				h - GameCanvas.hCommand, (byte) 2);
//		mFont.tahoma_7b_black.drawString(graphic, name, x + w / 2, y + GameCanvas.hCommand / 4,
//				2);
		MainTabNew.paintNameItem(g, x + w / 2,  y + GameCanvas.hCommand / 4, w,name,MainTabNew.COLOR_BLACK);
	}

	public int resetSelect(int select, int max, boolean isreset) {
		if (select < 0) {
			if (isreset)
				select = max;
			else
				select = 0;
		} else if (select > max)
			if (isreset)
				select = 0;
			else
				select = max;
		return select;
	}

	public void updatekey() {
//		if (GameCanvas.keyMyHold[5]) {
//			if (center != null) {
//				GameCanvas.clearKeyPressed(5);
//				GameCanvas.clearKeyHold(5);
//				center.perform();
//
//			}
//		} else if (GameCanvas.keyMyHold[12]) {
//			if (left != null) {
//				// perform(left);
//				GameCanvas.clearKeyPressed(12);
//				GameCanvas.clearKeyHold(12);
//				left.perform();
//			}
//		} else if (GameCanvas.keyMyHold[13]) {
//			if (right != null) {
//				// perform(right);
//				GameCanvas.clearKeyPressed(13);
//				GameCanvas.clearKeyHold(13);
//				right.perform();
//
//			}
//		}
	}

	public void updatePointer() {
		if (GameCanvas.isTouch) {
			if (left != null) {
//				if (left.isPosCmd())
//					left.updatePointer();
//				else {
//					if (GameCanvas.isPointSelect(0, GameCanvas.h
//							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
//							GameCanvas.hCommand + 10)) {
//						left.perform();
//					}
//				}
			}
			if (right != null) {
//				if (right.isPosCmd())
//					right.updatePointer();
//				else {
//					if (GameCanvas.isPointSelect(GameCanvas.w
//							- GameCanvas.wCommand * 2, GameCanvas.h
//							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
//							GameCanvas.hCommand + 10)) {
//						right.perform();
//					}
//				}
			}
			if (center != null) {
//				if (center.isPosCmd())
//					center.updatePointer();
//				else {
//					if (GameCanvas.isPointSelect(GameCanvas.hw
//							- GameCanvas.wCommand, GameCanvas.h
//							- GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
//							GameCanvas.hCommand + 10)) {
//						// perform(center);
//						center.perform();
//
//					}
//				}
			}
		}
	}

	public static void Font3dWhite(MGraphics g, String str, int x, int y, int ar) {
		mFont.tahoma_7b_white.drawString(g, str, x + 1, y + 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x, y, ar);
	}

	public static void Font3dColor(MGraphics g, String str, int x, int y,
                                   int ar, byte color) {
		mFont.tahoma_7b_white.drawString(g, str, x + 1, y + 1, ar);
		MainTabNew.setTextColorName(color).drawString(g, str, x, y, ar);
	}

	public static void Font3dColorAndColor(MGraphics g, String str, int x,
                                           int y, int ar, byte color, byte color2) {
		MainTabNew.setTextColorName(color).drawString(g, str, x + 1, y + 1, ar);
		MainTabNew.setTextColorName(color2).drawString(g, str, x, y, ar);
	}

	public static void FontBorderColor(MGraphics g, String str, int x, int y,
                                       int ar, int color) {
		mFont.tahoma_7b_white.drawString(g, str, x - 1, y - 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x - 1, y + 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x + 1, y - 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x + 1, y + 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x - 1, y, ar);
		mFont.tahoma_7b_white.drawString(g, str, x + 1, y, ar);
		mFont.tahoma_7b_white.drawString(g, str, x, y - 1, ar);
		mFont.tahoma_7b_white.drawString(g, str, x, y + 1, ar);
		MainTabNew.setTextColorName(color).drawString(g, str, x, y, ar);
	}

}
