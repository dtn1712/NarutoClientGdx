package GuiOut;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


import real.mFont;
import screen.old.GameScr;
import lib.Bitmap;
import lib.mVector;
import model.Command;


public class MainTabNew extends AvMain {
	
	public byte typeTab;
	public static byte maxTypeTab = 12;
	public static final byte INVENTORY = 0;
	public static final byte EQUIP = 1;
	public static final byte MY_INFO = 2;
	public static final byte SKILLS = 3;
	public static final byte QUEST = 4;
	public static final byte CHAT = 5;
	public static final byte GOLD = 6;
	public static final byte CONFIG = 7;
	public static final byte SHOP = 8;
	public static final byte CHEST = 9;
	public static final byte REBUILD = 10;
	public static final byte FUNCTION = 11;
	public static final byte CLAN_INVENTORY = 12;
	// 
	public static final byte COLOR_WHITE = 0;
	public static final byte COLOR_BLUE = 1;
	public static final byte COLOR_YELLOW = 2;
	public static final byte COLOR_VIOLET = 3;
	public static final byte COLOR_ORANGE = 4;
	public static final byte COLOR_GREEN = 5;
	public static final byte COLOR_RED = 6;
	public static final byte COLOR_BLACK = 7;
	public int xTab, yTab, wSmall, hSmall, numWSmall, numHSmall, xMoney,
			yMoney, xChar, yChar, sizeFocus, numWBlack, numHBlack;
	public static int wbackground, hbackground;
	public boolean isClan = false;
	//
	public static int wblack, hblack, hMaxContent;
	//
	int xBegin, yBegin;
	//
	public String nameTab = "";
	public static byte wOneItem = 20, wOne5;
	public static byte Focus = 0;
	public static final byte TAB = 0;
	public static final byte INFO = 1;
	public static Bitmap[] imgTab = new Bitmap[14];
	public static MainTabNew instance;
	Command cmdBack;
	public static int longwidth = 0, xlongwidth, ylongwidth;
	public static int timeRequest = 15;
//	public ListNew listContent = null;
	public static boolean is320 = false;
	Bitmap imgStarRebuild;

	public static MainTabNew gI() {
		if (instance == null) {
			instance = new MainTabNew();
		}
		return instance;
	}

	public MainTabNew() {
		if (GameCanvas.isTouch) {
			wOneItem = 26;
		} else if (GameCanvas.w >= 240) {
			wOneItem = 24;
		}
		if (GameCanvas.h < 240 && wOneItem > 24)
			wOneItem = 24;
		hMaxContent = GameCanvas.h - GameCanvas.hCommand * 2;
		wOne5 = (byte) (wOneItem / 5);
		wbackground = GameCanvas.w / 32 + 1;
		hbackground = GameCanvas.h / 32 + 1;
		int tw = GameCanvas.w / wOneItem;
		if (tw > 9)
			tw = 9;
		int htam = (GameCanvas.h / 5 * 4 - GameCanvas.hCommand / 2);
		if (GameCanvas.isTouch)
			htam += GameCanvas.hCommand / 2;
		int th = htam / wOneItem;
		if (th > 8)
			th = 8;
//		wSmall = (tw - 1) * wOneItem - (wOne5 * 3)
//				+ (GameCanvas.isSmallScreen ? wOne5 : 0);// khung nhá»�
		hSmall = th * wOneItem + wOne5;
		if (hSmall % 2 != 0)
			hSmall -= 1;
		if (GameCanvas.isTouch) {
			if (GameCanvas.w >= 380) {
				longwidth = 170;
				timeRequest = 5;
				hMaxContent = hSmall - wOneItem - wOne5;
				xTab = (GameCanvas.w - (tw) * wOneItem - longwidth) / 2;
				xlongwidth = GameCanvas.w - xTab - (longwidth);
				ylongwidth = yTab + GameCanvas.h / 5;
			} else if (GameCanvas.w > 315) {
				is320 = true;
//				if (LoginScreen.indexInfoLogin == 1)
//					LoginScreen.indexInfoLogin = 2;
				tw = 8;
				wSmall = (tw - 1) * wOneItem - (wOne5 * 3);
				longwidth = 130;
				timeRequest = 5;
				hMaxContent = hSmall - wOneItem - wOne5;
				xlongwidth = GameCanvas.w - xTab - (longwidth) + 5;
				ylongwidth = yTab + GameCanvas.h / 5;
			}
		}

		if (is320) {
			xTab = -5;
			xlongwidth = xTab + wSmall + wOneItem + wOne5 * 2;
			longwidth = GameCanvas.w - xlongwidth;
		} else {
			xTab = (GameCanvas.w - (tw) * wOneItem - longwidth) / 2;
		}
		yTab = 0;
		numWSmall = wSmall / 32;
		numHSmall = hSmall / 32;
		wblack = (wSmall) / wOneItem * wOneItem;
		hblack = ((hSmall) / wOneItem - 1) * wOneItem;
		numWBlack = wblack / 32;
		numHBlack = hblack / 32;
		xMoney = GameCanvas.w - (xTab - 9) - 72;
		if (GameCanvas.isTouch) {
			if (xMoney > GameCanvas.w - 112)
				xMoney = GameCanvas.w - 112;
		}
		yMoney = 5;
//		if (GameCanvas.isSmallScreen)
//			yMoney = 2;
		xChar = 0;// xTab - 9;
		yChar = GameCanvas.h / 10 - 21;
//		if (GameCanvas.isSmallScreen)
//			yChar += 4;
		sizeFocus = wOne5 + wOneItem;
		if (sizeFocus > 32)
			sizeFocus = 32;

	}

	public void init() {

	}

	public void backTab() {
//		GameScreen.cameraSub.setAll(0, 0, 0, 0);
//		if (GameCanvas.isTouch) {
//			if (GameCanvas.currentScreen == GameCanvas.AllInfo) {
//				GameCanvas.AllInfo.lastScreen.Show();
//			} else if (GameCanvas.currentScreen == GameCanvas.shopNpc) {
//				GameCanvas.shopNpc.lastScreen.Show();
//			}
//		}
	}

	public void keypress(int keyCode) {

	}

	public static int[] color = { 0xffa89982, 0xffbaaa92, 0xfff8b848,
			0xffebebeb, 0xffa69780, 0xffc7b59c, 0xffb6a58e, 0xff706354,
			0xffe3d5be, 0xffaf9f89, 0xfff8f8a8 };

	public void paintHairShop(MGraphics g, int hair) {
		GameScr.resetTranslate(g);
//		GameScreen.player.paintShowHairPlayer(graphic, GameCanvas.hw,
//				GameCanvas.h / 10 + 15, hair);

	}

	public void paintTab(MGraphics g, String nameTab, int idSelect,
                         mVector vec, boolean isClan) {
		int size = vec.size();
		if (GameCanvas.lowGraphic) {
			paintRectLowGraphic(g, 0, 0, GameCanvas.w, GameCanvas.h, 0);
			paintRectLowGraphic(g, xMoney, yMoney, 22 * 3, 32, 1);
			paintRectLowGraphic(g, xTab + wOneItem + wOne5 * 2, yTab
					+ GameCanvas.h / 5, wSmall, hSmall, 1);
		} else {
			for (int i = 0; i < wbackground; i++) {// background
				for (int j = 0; j < hbackground; j++) {
					g.drawImage(imgTab[0], i * 32, j * 32, 0);
				}
			}
			for (int i = 0; i < 3; i++) {// nen money
				g.drawImage(imgTab[1], xMoney + 22 * i, yMoney, 0);
			}
			for (int i = 0; i <= numWSmall; i++) {// khung mÃ u Ä‘en nhat
				for (int j = 0; j <= numHSmall; j++) {
					if (i == 0 || i == numWSmall || j == numHSmall || j == 0) {
						if (i == numWSmall) {
							if (j == numHSmall) {
								g.drawImage(imgTab[1], xTab + wOneItem + wOne5
										* 2 + wSmall - 32, yTab + GameCanvas.h
										/ 5 + hSmall - 32, 0);
							} else
								g.drawImage(imgTab[1], xTab + wOneItem + wOne5
										* 2 + wSmall - 32, yTab + j * 32
										+ GameCanvas.h / 5, 0);
						} else {
							if (j == numHSmall) {
								g.drawImage(imgTab[1], xTab + wOneItem + wOne5
										* 2 + i * 32, yTab + GameCanvas.h / 5
										+ hSmall - 32, 0);
							} else
								g.drawImage(imgTab[1], xTab + wOneItem + wOne5
										* 2 + i * 32, yTab + j * 32
										+ GameCanvas.h / 5, 0);
						}
					}
				}
			}
		}
		AvMain.Font3dWhite(g, nameTab,
				xTab + wOneItem + wOne5 * 2 + wSmall / 2, yTab + GameCanvas.h
						/ 5 + wOneItem / 2 - 6, 2);
//		GameScreen.infoGame.paintInfoPlayer(graphic, 0, 0, !GameCanvas.isSmallScreen,
//				GameCanvas.isSmallScreen ? mFont.tahoma_7_black
//						: mFont.tahoma_7_white);

		g.drawRegion(imgTab[4], 0, 0, 14, 14, 0, xMoney + 4, yMoney + 2, 0);
		g.drawRegion(imgTab[4], 0, 14, 14, 14, 0, xMoney + 4, yMoney + 17, 0);
		if (isClan) {
//			PaintInfoGameScreen.fraEvent.drawFrame(10, xMoney - 12,
//					yMoney + 10, 0, 3, graphic);
//			if (GameScreen.player.myClan != null) {
//				mFont.tahoma_7_white.drawString(graphic, MainItem
//						.getDotNumber(GameScreen.player.myClan.coin),
//						xMoney + 19, yMoney + 3, 0);
//				mFont.tahoma_7_white.drawString(graphic, MainItem
//						.getDotNumber(GameScreen.player.myClan.gold),
//						xMoney + 19, yMoney + 18, 0);
//			}
		} else {
//			mFont.tahoma_7_white.drawString(graphic, MainItem
//					.getDotNumber(GameScreen.player.coin), xMoney + 19,
//					yMoney + 3, 0);
//			mFont.tahoma_7_white.drawString(graphic, MainItem
//					.getDotNumber(GameScreen.player.gold), xMoney + 19,
//					yMoney + 18, 0);
		}
		if (GameCanvas.lowGraphic) {
			paintRectLowGraphic(g, xTab + wOne5, yTab + GameCanvas.h / 5
					+ ((wOneItem) * idSelect), wOne5 + wOneItem, wOneItem, 1);
		} else {
			if (wOne5 + wOneItem > 32) {// nen focus trai
				g.drawRegion(imgTab[1], 0, 0, wOneItem, wOneItem, 0, xTab
						+ wOne5, yTab + GameCanvas.h / 5
						+ ((wOneItem) * idSelect), 0);
				g.drawRegion(imgTab[1], 0, 0, wOne5, wOneItem, 0, xTab + wOne5
						+ wOneItem, yTab + GameCanvas.h / 5
						+ ((wOneItem) * idSelect), 0);
			} else {
				g.drawRegion(imgTab[1], 0, 0, wOne5 + wOneItem, wOneItem, 0,
						xTab + wOne5, yTab + GameCanvas.h / 5
								+ ((wOneItem) * idSelect), 0);
			}
		}
		if (!GameCanvas.isTouch && Focus == TAB) {// khung focus tab trái
			g.setColor(color[3]);
			g.drawRect(xTab + wOne5 + 1, yTab + GameCanvas.h / 5
					+ ((wOneItem) * idSelect) + 1, wOne5 + wOneItem - 3,
					wOneItem - 5);
		}
		g.setColor(color[0]);
		for (int i = 0; i < size; i++) {// hình tab trái
			MainTabNew tab = (MainTabNew) vec.elementAt(i);
			int t = 0;
			if (i != idSelect) {
				g.drawRect(xTab + wOne5, yTab + GameCanvas.h / 5
						+ ((wOneItem) * i), wOne5 + wOneItem, (wOneItem));
			} else if (Focus == TAB || GameCanvas.isTouch) {
				t = -1 + (GameCanvas.gameTick / 2) % 3;
			}
			int indexp = tab.typeTab;
			if (indexp > maxTypeTab)
				indexp = typeTab;
			g.drawRegion(imgTab[3], 0, indexp * 16, 16, 16, 0, xTab + wOne5
					+ wOne5 / 2 + wOneItem / 2 + t, yTab + GameCanvas.h / 5
					+ (wOneItem) / 2 + (wOneItem) * i, MGraphics.VCENTER
					| MGraphics.HCENTER);
//			if (tab.typeTab == MY_INFO && Player.diemTiemNang > 0) {
//				PaintInfoGameScreen.fralevelup.drawFrame(
//						GameCanvas.gameTick / 4 % 2, xTab + wOne5 + wOne5 / 2
//								+ wOneItem + t - 4, yTab + GameCanvas.h / 5
//								+ (wOneItem) + (wOneItem) * i - 6, 0, 3, graphic);
//			} else if (tab.typeTab == SKILLS && Player.diemKyNang > 0) {
//				PaintInfoGameScreen.fralevelup.drawFrame(
//						2 + GameCanvas.gameTick / 4 % 2, xTab + wOne5 + wOne5
//								/ 2 + wOneItem + t - 4, yTab + GameCanvas.h / 5
//								+ (wOneItem) + (wOneItem) * i - 6, 0, 3, graphic);
//			}
		}
		// }
		if (GameCanvas.lowGraphic) {
			paintRectLowGraphic(g, xTab + wOneItem + wOne5 * 3, yTab
					+ GameCanvas.h / 5 + wOneItem, wblack, hblack, 2);
		} else {
			for (int i = 0; i <= numWBlack; i++) {// nen den thong tin
				for (int j = 0; j <= numHBlack; j++) {
					if (i == numWBlack) {
						if (j == numHBlack) {
							g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
									+ wblack - 32, yTab + GameCanvas.h / 5
									+ wOneItem + hblack - 32, 0);
						} else
							g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
									+ wblack - 32, yTab + GameCanvas.h / 5
									+ wOneItem + j * 32, 0);
					} else {
						if (j == numHBlack) {
							g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
									+ i * 32, yTab + GameCanvas.h / 5
									+ wOneItem + hblack - 32, 0);
						} else
							g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
									+ i * 32, yTab + GameCanvas.h / 5
									+ wOneItem + j * 32, 0);
					}

				}
			}
		}
		if (longwidth > 0) {
			GameScr.resetTranslate(g);
			int indexPaint = 12;
			if (GameCanvas.lowGraphic) {
				paintRectLowGraphic(g, xlongwidth, ylongwidth, longwidth,
						hSmall, indexPaint);
			} else {
				int maxw = (longwidth) / 32, maxh = hSmall / 32;
				for (int i = 0; i <= maxw; i++) {
					for (int j = 0; j <= maxh; j++) {
						indexPaint = 12;
						if (j == 0)
							indexPaint = 12;
						if (i == maxw) {
							if (j == maxh) {
								g.drawImage(imgTab[indexPaint], xlongwidth
										+ (longwidth) - 32, ylongwidth + hSmall
										- 32, 0);
							} else
								g.drawImage(imgTab[indexPaint], xlongwidth
										+ (longwidth) - 32,
										ylongwidth + j * 32, 0);
						} else {
							if (j == maxh) {
								g.drawImage(imgTab[indexPaint], xlongwidth + i
										* 32, ylongwidth + hSmall - 32, 0);
							} else
								g.drawImage(imgTab[indexPaint], xlongwidth + i
										* 32, ylongwidth + j * 32, 0);
						}

					}

				}
			}
			for (int i = xlongwidth; i < xlongwidth + (longwidth); i += 6) {
				g.fillRect(i, ylongwidth + wOneItem, 4, 1);
			}
		}
	}

	public void paintPopupContent(MGraphics g, boolean isOnlyName) {
		if (longwidth > 0) {
			paintContentNew(g, isOnlyName);
		} else {
			paintContent(g, isOnlyName);
		}
	}

	public void paintContent(MGraphics g, boolean isOnlyName) {
		int begin = 4;
		// mFont fontname = setTextColorName(colorName);
		TabScreenNew.timeRepaint = 10;
		g.setClip(-g.getTranslateX(), -g.getTranslateY(), GameCanvas.w,
				GameCanvas.h);
		int th = 1;
		if (mContent != null)
			th = mContent.length;
		if (mPlusContent != null)
			th += mPlusContent.length;
//		int h = (th + 1) * GameCanvas.hText + 8;
//		if (h > hMaxContent)
//			h = hMaxContent;
		if (xCon + wContent > GameCanvas.w)
			xCon = GameCanvas.w / 2 - wContent / 2;
		int yp = yCon;
//		graphic.setColor(color[10]);
//		graphic.fillRect(xCon - 1, yp - 1, wContent + 2, h + 2);
//		graphic.setColor(color[2]);
//		graphic.fillRect(xCon, yp, wContent + 1, h + 1);
//		if (GameCanvas.lowGraphic) {
//			paintRectLowGraphic(graphic, xCon, yp, wContent, h, 4);
//		} else {
//			int maxw = wContent / 32, maxh = h / 32;
//			for (int i = 0; i <= maxw; i++) {
//				for (int j = 0; j <= maxh; j++) {
//					if (i == maxw) {
//						if (j == maxh) {
//							graphic.drawImage(imgTab[12], xCon + wContent - 32, yp
//									+ h - 32, 0);
//						} else
//							graphic.drawImage(imgTab[12], xCon + wContent - 32, yp
//									+ j * 32, 0);
//					} else {
//						if (j == maxh) {
//							graphic.drawImage(imgTab[12], xCon + i * 32, yp + h - 32,
//									0);
//						} else
//							graphic.drawImage(imgTab[12], xCon + i * 32, yp + j * 32,
//									0);
//					}
//
//				}
//			}
//		}
//		graphic.setClip(xCon + 1, yp + 1, wContent - 2, h - 2);
		if (!isOnlyName) {
			if (name != null)
				// fontname.drawString(graphic, name, xCon + begin, yp + 2, 0);
				paintNameItem(g, xCon + (wContent) / 2, yp + 2,
						wContent, name, colorName);
			// if (cmY > 0) {
//			if (listContent != null) {
//				graphic.setClip(xCon, yp + GameCanvas.hText, wContent, hMaxContent
//						- GameCanvas.hText);
//				graphic.translate(0, -listContent.cmx);
//			}
			// }
		}
		if (mPlusContent != null) {
//			for (int j = 0; j < mPlusContent.length; j++) {
//				yp += GameCanvas.hText;
//
//				setTextColor(mPlusColor[j]).drawString(graphic, mPlusContent[j],
//						xCon + begin, yp + 2, 0);
//			}
		}
		if (mContent != null) {
//			for (int j = 0; j < mContent.length; j++) {
//				mFont m = null;
//				if (mcolor != null)
//					m = setTextColor(mcolor[j]);
//				else
//					m = mFont.tahoma_7_white;
//				m.drawString(graphic, mContent[j], xCon + begin, yp + 2 + (j + 1)
//						* GameCanvas.hText, 0);
//				if (mSubContent != null) {
//					int t = m.getWidth(mContent[j]) + 5;
//					m = setTextColor(mSubColor[j]);
//					m.drawString(graphic, mSubContent[j], xCon + t + begin, yp + 2
//							+ (j + 1) * GameCanvas.hText, 0);
//				}
//			}
		} else {
			if (!isOnlyName) {
				if (name != null) {
//					fontname.drawString(graphic, name, xCon + begin, yp + 2, 0);
					paintNameItem(g, xCon + (wContent) / 2, yp + 2,
							wContent, name, colorName);
				}
//				mFont.tahoma_7_white.drawString(graphic, T.danglaydulieu, xCon + 2,
//						yp + GameCanvas.hText + 3, 0);
			} else {
//				if (name != null){
////					mFont.tahoma_7b_white.drawString(graphic, name, xCon + wContent
////							/ 2, yp + GameCanvas.hText / 4, 2);
//					paintNameItem(graphic, xCon + (wContent) / 2, yp + GameCanvas.hText / 4,
//							wContent, name, colorName);
//				}
			}
		}
		GameScr.resetTranslate(g);
	}

	public void paintContentNew(MGraphics g, boolean isOnlyName) {
		int begin = 4;
		// mFont fontname = setTextColorName(colorName);
		TabScreenNew.timeRepaint = 10;
		GameScr.resetTranslate(g);
		int xpaint = xlongwidth;
		int ypaint = ylongwidth;
		int yp = ypaint;
		g.setClip(xpaint + 1, yp + 1, (longwidth) - 2, hSmall - 2);
		if (!isOnlyName) {
			// fontname.drawString(graphic, name, xpaint + (longwidth) / 2, yp
			// + wOneItem / 2 - 5, 2);
			paintNameItem(g, xpaint + (longwidth) / 2, yp + wOneItem / 2 - 5,
					longwidth, name, colorName);
//			if (listContent != null) {
//				graphic.setClip(xpaint, yp + wOneItem + 2, longwidth, hMaxContent
//						- wOneItem - 2);
//				graphic.translate(0, -listContent.cmx);
//			}
//			yp += (wOneItem - GameCanvas.hText) + GameCanvas.hText / 4;

		}
		if (mPlusContent != null) {
//			for (int j = 0; j < mPlusContent.length; j++) {
//				yp += GameCanvas.hText;
//				setTextColor(mPlusColor[j]).drawString(graphic, mPlusContent[j],
//						xpaint + begin, yp + 2, 0);
//			}
		}
		if (mContent != null) {
			for (int j = 0; j < mContent.length; j++) {
				mFont m = null;
				if (mcolor != null)
					m = setTextColor(mcolor[j]);
				else
					m = mFont.tahoma_7_white;
//				m.drawString(graphic, mContent[j], xpaint + begin, yp + 2 + (j + 1)
//						* GameCanvas.hText, 0);
				if (mSubContent != null) {
					int t = m.getWidth(mContent[j]) + 5;
					m = setTextColor(mSubColor[j]);
//					m.drawString(graphic, mSubContent[j], xpaint + t + begin, yp + 2
//							+ (j + 1) * GameCanvas.hText, 0);
				}
			}
		} else {
//			if (!isOnlyName) {
//				// fontname.drawString(graphic, name, xpaint + (longwidth) / 2, yp
//				// + wOneItem / 2 - 5, 0);
////				paintNameItem(graphic, xpaint + (longwidth) / 2, yp + wOneItem / 2
////						- 5, longwidth, name, colorName);
////				mFont.tahoma_7_white.drawString(graphic, T.danglaydulieu, xpaint
////						+ (longwidth) / 2,
////						yp + wOneItem + GameCanvas.hText + 3, 2);
//			} else {
			if (isOnlyName)
				mFont.tahoma_7b_white.drawString(g, name, xpaint + (longwidth)
						/ 2, yp + wOneItem / 2 - 5, 2);
//			}
		}
		GameScr.resetTranslate(g);
	}

	public static void  paintNameItem(MGraphics g, int x, int y, int w, String name,
                                      int colorName) {
		if (mFont.tahoma_7b_white.getWidth(name) <= w) { // black đoi white
			mFont fontname = setTextColorName(colorName);
			fontname.drawString(g, name, x, y, 2);
		} else {
			if (nameCur.compareTo(name.trim()) != 0)
				getTextName(name);
			mFont fontname = setTextColor(colorName);
			fontname.drawString(g, namePaint[0], x, y - 6, 2);
			fontname.drawString(g, namePaint[1], x, y + 6, 2);
		}
	}

	static String nameCur = "";
	static String[] namePaint = new String[2];

	public static void getTextName(String name) {
	
		nameCur = name.trim();
		namePaint = new String[2];
		for (int i = 0; i < namePaint.length; i++) {
			namePaint[i] = "";
		}
//		String[] m = mFont.split(nameCur, " ");
//		for (int i = 0; i < m.length; i++) {
//			if (i <= m.length / 2) {
//				namePaint[0] += m[i];
//				if (i < m.length / 2)
//					namePaint[0] += " ";
//			} else {
//				namePaint[1] += m[i];
//				if (i < m.length - 1)
//					namePaint[1] += " ";
//			}
//		}
	}

	public static mFont setTextColor(int id) {
		switch (id) {
//		case COLOR_WHITE:
//			return mFont.tahoma_7_white;
//		case COLOR_BLUE:
//			return mFont.tahoma_7_blue;
//		case COLOR_YELLOW:
//			return mFont.tahoma_7_yellow;
//		case COLOR_VIOLET:
//			return mFont.tahoma_7_violet;
//		case COLOR_ORANGE:
//			return mFont.tahoma_7_orange;
//		case COLOR_GREEN:
//			return mFont.tahoma_7_green;
//		case COLOR_RED:
//			return mFont.tahoma_7_red;
//		case COLOR_BLACK:
//			return mFont.tahoma_7_black;
		}
		return mFont.tahoma_7_white;
	}

	public static mFont setTextColorName(int id) {
		switch (id) {
//		case COLOR_WHITE:
//			return mFont.tahoma_7b_white;
//		case COLOR_BLUE:
//			return mFont.tahoma_7b_blue;
//		case COLOR_YELLOW:
//			return mFont.tahoma_7b_yellow;
//		case COLOR_VIOLET:
//			return mFont.tahoma_7b_violet;
//		case COLOR_ORANGE:
//			return mFont.tahoma_7b_orange;
//		case COLOR_GREEN:
//			return mFont.tahoma_7b_green;
//		case COLOR_BLACK:
//			return mFont.tahoma_7b_black;
		}
		return mFont.tahoma_7b_white;
	}

	public static int timePaintInfo;
	public String[] mContent, mSubContent, mPlusContent = null;
	public int[] mcolor, mSubColor, mPlusColor;
	public String name;
	public int wContent, colorName;
	public int xCon, yCon;

	public void setPaintInfo() {
	}

//	public void setYCon(MainItem item) {
//	}

	public void setPosCmd(mVector vecListCmd) {
		if (vecListCmd == null)
			return;
		int size = vecListCmd.size();
		if (size == 0)
			return;
		int yp = ylongwidth + hSmall;
		int xp = xlongwidth;

		if (size == 1) {
			Command cmd = (Command) vecListCmd.elementAt(0);
			if (MainTabNew.is320) {
//				cmd.setPos(xp + (longwidth) / 2, yp - 10,
//						PaintInfoGameScreen.fraButton2, cmd.caption);
			} else {
//				cmd.setPos(xp + (longwidth) / 2, yp - 15, null, cmd.caption);
			}
		} else if (size == 2) {
			Command cmd = (Command) vecListCmd.elementAt(0);
			if (MainTabNew.is320) {
//				cmd.setPos(xp + longwidth / 4, yp - 10,
//						PaintInfoGameScreen.fraButton2, cmd.caption);
			} else {
//				cmd.setPos(xp + longwidth / 4, yp - 15, null, cmd.caption);
			}
			Command cmd2 = (Command) vecListCmd.elementAt(1);

			if (MainTabNew.is320) {
//				cmd2.setPos(xp + longwidth / 4 * 3 + 2, yp - 10,
//						PaintInfoGameScreen.fraButton2, cmd2.caption);
			} else {
//				cmd2.setPos(xp + longwidth / 4 * 3 + 2, yp - 15, null,
//						cmd2.caption);
			}
		} else {
			for (int i = 0; i < size; i++) {
				Command cmd = (Command) vecListCmd.elementAt(i);
				if (i == size - 1 && size % 2 == 1) {
					if (MainTabNew.is320) {
//						cmd.setPos(xp + longwidth / 2, yp - 10,
//								PaintInfoGameScreen.fraButton2, cmd.caption);
					} else {
//						cmd.setPos(xp + (longwidth) / 2, yp - 15
//								- ((size - 1) / 2) * 30 + (i / 2) * 30, null,
//								cmd.caption);
					}
				} else {
					if (MainTabNew.is320) {
//						cmd.setPos(xp + longwidth / 4 + (i % 2)
//								* (longwidth / 2 + 2), yp - 10
//								- ((size - 1) / 2) * 22 + (i / 2) * 22,
//								PaintInfoGameScreen.fraButton2, cmd.caption);
					} else {
//						cmd.setPos(xp + longwidth / 4 + (i % 2)
//								* (longwidth / 2 + 2), yp - 15
//								- ((size - 1) / 2) * 30 + (i / 2) * 30, null,
//								cmd.caption);
					}
				}
			}
		}
	}

	static int[] colorLow = { 0xffd6c7ae, 0xff9c8c77, 0xff706354, 0xff887a67,
			0xff4b4339 };

	public static void paintRectLowGraphic(MGraphics g, int x, int y, int w,
                                           int h, int indexColor) {
		g.setColor(colorLow[indexColor]);
		g.fillRect(x, y, w, h);

	}

}
