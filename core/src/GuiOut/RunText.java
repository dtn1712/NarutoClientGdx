package GuiOut;

import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


public class RunText {
	public boolean showDlg = false;
	public int nStepToShow, currentDlgStep;
	public int dlgActionType;// -1: bình thường, 0umup

	public String[] dlgChainInfo;
	public int dlgFocusX, dlgFocusY, dlgRunX, dlgRunY, dlgW, dlgX;
	mFont f;
	public static int hText = 14;
	
	public void startDialogChain(String dlgChain, int ActionType, int xFocus,
			int yFocus, int w, mFont f) {
		this.f = f;
		dlgActionType = ActionType;
		dlgFocusX = xFocus;
		dlgFocusY = yFocus;
		currentDlgStep = 0;
		 dlgW = w;
		if (dlgW > GameCanvas.w - 10)
			dlgW = GameCanvas.w - 10;
		dlgChainInfo = f.splitFontArray(dlgChain, dlgW);
		dlgX = dlgFocusX;
		showDlg = true;
		dlgRunX = 0;
		dlgRunY = 0;
		
	}

	public boolean checkDlgStep() {
		if (dlgRunY < dlgChainInfo.length) {
			return false;
		}
		return true;
	}

	public boolean nextDlgStep() {
		if (dlgRunY < dlgChainInfo.length) {
			dlgRunY = dlgChainInfo.length;
			dlgRunX = 0;
			return false;
		}
		dlgRunX = dlgRunY = 0;
		return true;
	}

	public void updateDlg() {
		if (showDlg) {
			if (dlgRunY < dlgChainInfo.length) {
				dlgRunX += 2;
				if (dlgRunX >= dlgChainInfo[dlgRunY].length()) {
					dlgRunX = 0;
					dlgRunY++;
				}
			}
		}
	}

	public void paintText(MGraphics g) {
		int dlgTextY = -1;
		dlgTextY = (dlgFocusY);
		for (int i = 0; i < dlgRunY; i++) {
			f.drawString(g, dlgChainInfo[i], dlgX, dlgTextY + i
					* hText, 0);
		}
		if (dlgRunY < dlgChainInfo.length) {
			f.drawString(g, dlgChainInfo[dlgRunY].substring(0, dlgRunX), dlgX,
					dlgTextY + dlgRunY * hText, 0);

		}
	}

	public void paintText(MGraphics g, int archor) {
		int dlgTextY = -1;
		dlgTextY = (dlgFocusY);
		if (archor == 2) {
			for (int i = 0; i < dlgRunY; i++) {
				f.drawString(g, dlgChainInfo[i], dlgX+dlgW/2, dlgTextY + i
						* hText, 2);
			}
			if (dlgRunY < dlgChainInfo.length) {
				f.drawString(g, dlgChainInfo[dlgRunY].substring(0, dlgRunX),
						dlgX+dlgW/2, dlgTextY + dlgRunY * hText, 2);

			}
		} else {
			for (int i = 0; i < dlgRunY; i++) {
				f.drawString(g, dlgChainInfo[i], dlgX, dlgTextY + i
						* hText, 0);
			}
			if (dlgRunY < dlgChainInfo.length) {
				f.drawString(g, dlgChainInfo[dlgRunY].substring(0, dlgRunX),
						dlgX, dlgTextY + dlgRunY * hText, 0);

			}
		}
	}
}
