package model;

import screen.old.Layer;

import com.thdgaming.naruto.MGraphics;

public class Position {
	public int x, y, anchor;
	public int g, v, w, h, color = 0, limitY;
	public Layer layer;

	public Position() {
		this.x = 0;
		this.y = 0;
	}

	public Position(int x, int y, int anchor) {
		this.x = x;
		this.y = y;
		this.anchor = anchor;
	}

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public short yTo, xTo, distant;

	public void setPosTo(int xT, int yT) {
		xTo = (short) xT;
		yTo = (short) yT;
		distant = (short) Res.distance(x, y, xTo, yTo);
	}

	public int translate() {
		if (x == xTo && y == yTo)
			return -1;
		if (Math.abs((xTo - x) / 2) <= 1 && Math.abs((yTo - y) / 2) <= 1) {
			x = xTo;
			y = yTo;
			return 0;
		}
		if (x != xTo) {
			x += (xTo - x) / 2;
		}
		if (y != yTo) {
			y += (yTo - y) / 2;
		}
		if (Res.distance(x, y, xTo, yTo) <= distant / 5)
			return 2;
		return 1;
	}

	public void update() {
		layer.update();
	}

	public void paint(MGraphics g) {
		layer.paint(g, x, y);
	}
}
