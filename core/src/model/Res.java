package model;


import java.util.Random;

import lib.Cout;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;



public class Res {
	// sincos
	private static short sin[] = { 0, 18, 36, 54, 71, 89, 107, 125, 143, 160,
			178, 195, 213, 230, 248, 265, 282, 299, 316, 333, 350, 367, 384,
			400, 416, 433, 449, 465, 481, 496, 512, 527, 543, 558, 573, 587,
			602, 616, 630, 644, 658, 672, 685, 698, 711, 724, 737, 749, 761,
			773, 784, 796, 807, 818, 828, 839, 849, 859, 868, 878, 887, 896,
			904, 912, 920, 928, 935, 943, 949, 956, 962, 968, 974, 979, 984,
			989, 994, 998, 1002, 1005, 1008, 1011, 1014, 1016, 1018, 1020,
			1022, 1023, 1023, 1024, 1024 };
	private static short cos[];
	private static int tan[];

	public static void init() {
		cos = new short[91];
		tan = new int[91];
		for (int i = 0; i <= 90; i++) {
			cos[i] = sin[90 - i];
			if (cos[i] == 0)
				tan[i] = Integer.MAX_VALUE;
			else
				tan[i] = (sin[i] << 10) / cos[i];
		}
	}

	// tinh sin goc 0-359
	public static final int sin(int a) {
		a = fixangle(a);
		if (a >= 0 && a < 90)
			return sin[a];
		if (a >= 90 && a < 180)
			return sin[180 - a];
		if (a >= 180 && a < 270)
			return -sin[a - 180];
		return -sin[360 - a];
	}

	// tinh cos goc 0-359
	public static final int cos(int a) {
		a = fixangle(a);
		
		if (a >= 0 && a < 90)
			return cos[a];
		if (a >= 90 && a < 180)
			return -cos[180 - a];
		if (a >= 180 && a < 270)
			return -cos[a - 180];
		return cos[360 - a];
	}

	// tinh tan goc 0-359
	public static final int tan(int a) {
		a = fixangle(a);
		if (a >= 0 && a < 90)
			return tan[a];
		if (a >= 90 && a < 180)
			return -tan[180 - a];
		if (a >= 180 && a < 270)
			return tan[a - 180];
		return -tan[360 - a];
	}

	public static final int atan(int a) {
		for (int i = 0; i <= 90; i++)
			if (tan[i] >= a)
				return i;
		return 0;
	}

	// tinh goc tao boi dx, dy
//	public static final int angle(int dx, int dy) {
//		int angle;
//		if (dx != 0) {
//			int tan = Math.abs((dy << 10) / dx);
//			angle = cos(tan);
//			if (dy >= 0 && dx < 0)
//				angle = 180 - angle;
//			if (dy < 0 && dx < 0)
//				angle = 180 + angle;
//			if (dy < 0 && dx >= 0)
//				angle = 360 - angle;
//		} else {
//			angle = dy > 0 ? 90 : 270;
//		}
//
//		return angle;
//	}
	public static final int angle(int dx, int dy) {
		  int angle;
		  if (dx != 0) {
		   int tan = Math.abs((dy << 10) / dx);
		   System.out.println("TAN ----> "+tan);
//		   angle = cos(tan);
		   angle = atan(tan);
		   Cout.println("AAAAAAAAA ----> "+angle);
		   if (dy >= 0 && dx < 0)
		    angle = 180 - angle;
		   if (dy < 0 && dx < 0)
		    angle = 180 + angle;
		   if (dy < 0 && dx >= 0)
		    angle = 360 - angle;
		  } else {
		   angle = dy > 0 ? 90 : 270;
		  }

		  return angle;
		 }
	// dieu chinh lai goc <0 hoac >360 ve trong khoang 0-360
	public static final int fixangle(int angle) {
		if (angle >= 360)
			angle -= 360;
		if (angle < 0)
			angle += 360;

		return angle;
	}

	public static int xetVX(int goc, int d) {
		return (cos(fixangle(goc)) * d) >> 10;

	}

	public static int xetVY(int goc, int d) {
		return ((sin(fixangle(goc)) * d) >> 10);
	}

	public static Random r = new Random();

	public static int random(int a, int b) {
		return a + (r.nextInt(b - a));
	}

	public static double pitago(int dx, int dy) {
		return Math.sqrt(dx * dx + dy * dy);
	}

	public static int s2tick(int currentTimeMillis) {
		int a = 0;
		a = currentTimeMillis * 16 / 1000;
		if ((currentTimeMillis * 16) % 1000 >= 5)
			a += 1;
		return a;
	}

	public static int distance(int x1, int y1, int x2, int y2) {
		return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}

	public static int sqrt(int a) {
		int x, x1;

		if (a <= 0)
			return 0;
		x = (a + 1) / 2;
		do {
			x1 = x;
			x = x / 2 + a / (2 * x);
		} while (Math.abs(x1 - x) > 1);
		return x;
	}

	public static int rnd(int a) {
		return r.nextInt(a);
	}
	
	

	public static int abs(int i) {

		return i > 0 ? i : -i;

	}

	public static boolean inRect(int x1, int y1, int width, int height, int x2, int y2){
		return ((x2 >= x1 && x2 <= x1 + width) && (y2 >= y1 && y2 <= y1 + height));
	}
	
	public static void resetTrans(MGraphics g) {
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
	}

	public static void out(String s) {
		System.out.println(s);
	}
	public static String[] split(String original, String separator, int count)
	{
	    String[] result;
	    int index = original.indexOf(separator);
	    if(index >= 0)
	        result = split(original.substring(index + separator.length()), separator, count + 1);
	    else
	    {
	        result = new String[count + 1];
	        index = original.length();
	    }
	    result[count] = original.substring(0, index);
	    return result;
	}

	
}
