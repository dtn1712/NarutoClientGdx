package model;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;

import lib.Bitmap;
import lib.Rms;
import lib.mSystem;



public class Image {
	public static FilePack filePack = null;
	public static Bitmap createImage(String path) {
		Bitmap img = null;
		  byte []data = Rms.loadRMS(mSystem.getPathRMS(path));
		  if(data!=null){
		      img = Image.createImage(data, 0, data.length); 
		      data = null;
		      return img;
		  }
//		  String x= "/x";
//		  path = x + (MGraphics.zoomLevel) + path;
		  try {
			img = Image.createImage2(path);
			if(img!=null&&img.image!=null){
				img.width = img.image.getWidth();
				img.height = img.image.getHeight();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		  return img;
	}
	public static String loiloadanh = "";
public static Bitmap createImage2(String path) throws IOException {
		
	Bitmap img = new Bitmap();
		try {	
			
//			if(MGraphics.zoomLevel ==1)
//				img.texture= new Texture(Gdx.files.internal(LibSysTem.res+url), Format.RGBA4444, false);
//			else 
				img.image = new Texture(Gdx.files.internal(""+path), Format.RGBA8888,false);
			if(img.image==null)
				throw new IllegalArgumentException("!!! createImage scr is NULL-----------."+path);			
			img.width=img.image.getWidth();
			img.height=img.image.getHeight();	
			img.image.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		//	img.sp = new Sprite(img.texture);
		//	img.sp.flip(false, true);
		//	img.sp.scale(MGraphics.zoomLevel/2);
		} catch (Exception e) {
			if(path.contains("imgfocus"))
			loiloadanh = e.toString();
		}
		if(img.image==null)
			return null;
		return img;
		
	}
	public static Bitmap createImage(byte[] data, int offset, int lenght) {
		Bitmap img = new Bitmap();			
//		Gdx.app.postRunnable(new Runnable() {			
//			@Override
//			public void run() {
				try {				
				// TODO Auto-generated method stub	
				Pixmap p=new Pixmap(data,offset,lenght);		
				img.image = new Texture(p);
				img.width=img.image.getWidth();
				img.height=img.image.getHeight();	
				p.dispose();
				data = null;
				p = null;
			//	img.sp = new Sprite(img.texture);
			//	img.sp.flip(false, true);
		//		img.sp.scale(MGraphics.zoomLevel/2);
			//	p.dispose();
				
				} catch (Exception e) {
					// TODO: handle exception
				}
//			}
//		});	
		
		return img;
	}

	public static Bitmap createImage(int width, int height) {
		
		Bitmap bmp = null;
		return bmp;
	}

	public static Bitmap loadImageFromAsset(String strPath)
    {
		return createImage(strPath);
    }
	
	public static Bitmap loadImagePack(String filepackName) {
		Bitmap bmp = null;
		filePack = new FilePack(filepackName);
		bmp = filePack.loadImage(filepackName + ".png");
		filePack = null;
		return bmp;
	}
	public static Bitmap loadImagePack(String filepackName, String fileName) {
		Bitmap bmp = null;
		filePack = new FilePack(filepackName);
		bmp = filePack.loadImage(fileName);
		filePack = null;
		return bmp;
	}

	
	public static String convert(String str) {
		String strimg = str;
		if (str.charAt(0) == '/')
			strimg = strimg.substring(1);
		strimg = strimg.replaceAll("/", "_");
		if (strimg.indexOf('.') > -1)
			strimg = strimg.substring(0, strimg.indexOf('.'));

		return strimg.toLowerCase();
	}

	public static Bitmap getResizedBitmap(Bitmap bm) {
		if (bm == null)
			return null;

		return null;
//		int width, height;
//		float scaleWidth, scaleHeight;
//		
//		width = bm.getWidth();
//		height = bm.getHeight();
//
//		scaleWidth = (float) GameCanvas.rw / (float) width;
//		scaleHeight = (float) GameCanvas.rh / (float) height;
//
//		if (scaleWidth < scaleHeight)
//			scaleWidth = scaleHeight;
//		else
//			scaleHeight = scaleWidth;
//		
//		// CREATE A MATRIX FOR THE MANIPULATION
//		Matrix matrix = new Matrix();
//		
//		// RESIZE THE BIT MAP
//		matrix.postScale(scaleWidth, scaleHeight);
//		
//		// RECREATE THE NEW BITMAP
//		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,matrix, false);
//		
//		return resizedBitmap;
	}
	

	public static int getDrawableID(String filename) {
		return 0;
	}
	
	public static int getWidth(Bitmap image) {
		if(image==null) return 1;
		return image.getWidth() /*/ GameCanvas.zoomLevel*/;
	}

	public static int getHeight(Bitmap image) {
		if(image==null) return 1;
		return image.getHeight() /*/ GameCanvas.zoomLevel*/;
	}
}
