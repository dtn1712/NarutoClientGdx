package model;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Char;
import lib.Bitmap;
import real.mFont;
import screen.old.GameScr;

public class ChatPopup extends Effect2 implements IActionListener {
	public int sayWidth = 100, delay, sayRun;
	public String[] says;
	int cx, cy, ch;
	public Char c;
	boolean outSide = false;

	// =====================
	int currentLine;
	String[] lines;
	public Command cmdNextLine;
	public static Bitmap imgGoc;
	public static ChatPopup currentMultilineChatPopup;

	public static void addChatPopupMultiLine(String chat, int howLong, Char c) {
		String[] lines = Res.split(chat, "\n", 0);
		if (lines.length == 1) {
			addChatPopup(lines[0], howLong, c);
			return;
		}
		currentMultilineChatPopup = addChatPopup(lines[0], howLong, c);
		currentMultilineChatPopup.currentLine = 0;
		currentMultilineChatPopup.lines = lines;
		currentMultilineChatPopup.cmdNextLine = new Command(mResources.NEXT, currentMultilineChatPopup,8000,null);
	}

	public static ChatPopup addChatPopupOutSide(String chat, int howLong, Char c) {
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 64;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = c;
		cp.cx = c.cx;
		cp.cy = c.cy;
		c.chatPopup = cp;
		cp.sayRun = 7;
		cp.outSide = true;
		vEffect2Outside.addElement(cp);
		return cp;
	}

	public static ChatPopup addChatPopup(String chat, int howLong, Char c) {
		if(imgGoc==null) imgGoc = GameCanvas.loadImage("/GuiNaruto/imgGoc.png");
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 64;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = c;
		cp.cx = c.cx;
		cp.cy = c.cy;
		c.chatPopup = cp;
		cp.sayRun = 7;
		vEffect2.addElement(cp);
		return cp;
	}

	public static void addChatPopup(String chat, int howLong, int x, int y) {
		if(imgGoc==null) imgGoc = GameCanvas.loadImage("/GuiNaruto/imgGoc.png");
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 60;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = null;
		cp.cx = x;
		cp.cy = y;
		cp.sayRun = 7;
		vEffect2.addElement(cp);
	}

	public void update() {
		if (c != null) {
			cx = c.cx;
			cy = c.cy;
			ch = c.ch + 10;
		}
		if (delay > 0) {
			delay--;
		}

		if (sayRun > 1)
			sayRun--;

		if ((c != null && c.chatPopup != null && c.chatPopup != this) || (c != null && c.chatPopup == null) || delay == 0) {
			vEffect2Outside.removeElement(this);
			vEffect2.removeElement(this);
		}
	}

	public void paint(MGraphics g) {
		int cx = this.cx;
		int cy = this.cy;
		if (outSide) {
			cx -= GameScr.cmx;
			cy -= GameScr.cmy;
			cy += 35;
		}
		g.setColor(0xffFFFFFF);
		g.fillRect(cx - sayWidth / 2 , cy - ch - 15 + sayRun - says.length * 12 - 9, sayWidth , (says.length + 1) * 12 - 1);
		g.setColor(0xff000000);
		g.drawLine(cx - sayWidth / 2+1 , cy - ch - 15 + sayRun - says.length * 12 - 10,
		cx - sayWidth / 2+1+ sayWidth -2,cy - ch - 15 + sayRun - says.length * 12 - 10,true);
		g.drawLine(cx - sayWidth / 2 , cy - ch - 15 + sayRun - says.length * 12 - 9+1,
				cx - sayWidth / 2,cy - ch - 15 + sayRun - says.length * 12 - 9+ (says.length + 1) * 12 - 3,true);
		g.drawLine(cx - sayWidth / 2+1 , cy - ch - 15 + sayRun - says.length * 12 - 9+(says.length + 1) * 12 - 1,
				   cx - sayWidth / 2+1 + sayWidth-2 ,cy - ch - 15 + sayRun - says.length * 12 - 9+ (says.length + 1) * 12 - 1,true);
		g.drawLine(cx - sayWidth / 2+3+ sayWidth-2  , cy - ch - 15 + sayRun - says.length * 12 - 8, 
				cx - sayWidth / 2+3 + sayWidth-2 , cy - ch - 15 + sayRun - says.length * 12 - 11+ (says.length + 1) * 12 - 1,true);
		
//		graphic.drawRoundRect(cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10, sayWidth + 1, (says.length + 1) * 12, 12, 12);
//		SmallImage.drawSmallImage(graphic, 941, cx - 3, cy - ch - 15 + sayRun + 2, 0, MGraphics.TOP | MGraphics.HCENTER);
		
		
		g.drawImage(loadImageInterface.imgGocChat,cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10, MGraphics.TOP| MGraphics.LEFT,true);
		g.drawRegion(loadImageInterface.imgGocChat, 0, 0, loadImageInterface.imgGocChat.getWidth(), loadImageInterface.imgGocChat.getHeight(), 2,
				cx - sayWidth / 2 - 1+sayWidth + 2-loadImageInterface.imgGocChat.getWidth(), cy - ch - 15 + sayRun - says.length * 12 - 10, MGraphics.TOP| MGraphics.LEFT, true);
		g.drawRegion(loadImageInterface.imgGocChat, 0, 0, loadImageInterface.imgGocChat.getWidth(), loadImageInterface.imgGocChat.getHeight(), 6,
				cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10+(says.length + 1) * 12-loadImageInterface.imgGocChat.getHeight(), MGraphics.TOP| MGraphics.LEFT, true);
		g.drawRegion(loadImageInterface.imgGocChat, 0, 0, loadImageInterface.imgGocChat.getWidth(), loadImageInterface.imgGocChat.getHeight(), 3,
				cx - sayWidth / 2 - 1+sayWidth + 2-loadImageInterface.imgGocChat.getWidth(), cy - ch - 15 + sayRun - says.length * 12 - 10+(says.length + 1) * 12-loadImageInterface.imgGocChat.getHeight()+1, MGraphics.TOP| MGraphics.LEFT, true);
		
		g.drawImage(imgGoc,  cx - sayWidth/4, cy - ch - 15 + sayRun+imgGoc.getHeight()/2-1, MGraphics.TOP| MGraphics.LEFT,true);
		for (int i = 0; i < says.length; i++) {
			mFont.tahoma_7.drawString(g, says[i], cx, cy - ch - 15 + sayRun + i * 12 - says.length * 12 - 4, 2);
		}
	}

	public void updateKey() {
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(GameCanvas.currentScreen.center)) {
			GameCanvas.keyPressed[5] = false;
			Screen.keyTouch = -1;
			cmdNextLine.performAction();
		}
	}

	public void paintCmd(MGraphics g) {
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paint.paintTabSoft(g);
		GameCanvas.paint.paintCmdBar(g, null, cmdNextLine, null);

	}

	public void perform(int idAction, Object p) {
		if(idAction==8000)
		{
			int currentLine = currentMultilineChatPopup.currentLine;
			currentLine++;
			if (currentLine >= currentMultilineChatPopup.lines.length) {
				currentMultilineChatPopup.c.chatPopup = null;
				currentMultilineChatPopup = null;
				return; // END
			}
			ChatPopup cp = addChatPopup(currentMultilineChatPopup.lines[currentLine], currentMultilineChatPopup.delay, currentMultilineChatPopup.c);
			cp.currentLine = currentLine;
			cp.lines = currentMultilineChatPopup.lines;
			cp.cmdNextLine = currentMultilineChatPopup.cmdNextLine;
			currentMultilineChatPopup = cp;
		}
		
	}

	public void perform() {
		// TODO Auto-generated method stub
		
	}

	
}