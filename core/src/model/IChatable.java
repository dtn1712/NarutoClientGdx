package model;

public interface IChatable {
	public void onChatFromMe(String text,String to);
	public void onCancelChat();
}
