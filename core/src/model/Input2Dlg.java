package model;

import lib.TField;

import java.util.Vector;

import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


public class Input2Dlg extends Dialog implements IActionListener{
	protected String[] info;
	public TField tfInput;
	public  TField tfInput2;
	int padLeft;
	int focus, line, titleLenght;
	public Command cmdClose = new Command("",1000, null);
	
	public Input2Dlg(){
		
		tfInput = new TField();
		tfInput2 = new TField();
	}
	

	public void setTitle(String title, String title2) {
		titleLenght = mFont.tahoma_8b.getWidth(title) > mFont.tahoma_8b.getWidth(title2) ? mFont.tahoma_8b.getWidth(title) : mFont.tahoma_8b.getWidth(title2);
		padLeft = 40;
		focus = 0;
		if (GameCanvas.w <= 176)
			padLeft = 10;
		
		tfInput.x = padLeft + titleLenght + 10;
		tfInput.y = GameCanvas.h - 2 * Screen.ITEM_HEIGHT - 50;
		tfInput.width = GameCanvas.w - 2 * (padLeft) - titleLenght -20;
		tfInput.height = Screen.ITEM_HEIGHT + 2;
		tfInput.isFocus = true;
		tfInput.title = title;
		
		tfInput2.x = padLeft + titleLenght + 10;
		tfInput2.y = GameCanvas.h - Screen.ITEM_HEIGHT - 43;
		tfInput2.width = GameCanvas.w - 2 * (padLeft) - titleLenght-20;
		tfInput2.height = Screen.ITEM_HEIGHT + 2;
		tfInput2.title = title2;
		right = tfInput.cmdClear;
	}

	public void show(String info, Command left, Command ok, int type1,int type2) {
		tfInput.setText("");
		tfInput.setIputType(type1);
		
		tfInput2.setText("");
		tfInput2.setIputType(type2);
		
		this.info = mFont.tahoma_8b.splitFontArray(info, GameCanvas.w  - (padLeft*3));
		Vector lines = mFont.tahoma_8b.splitFontVector(info, GameCanvas.w  - (padLeft*3));
		line = lines.size();
		System.out.println("line value:"+line);
		this.left = left;
		center = ok;
		show();

	}

	public void paint(MGraphics g) {

		GameCanvas.paint.paintInputDlg(g, padLeft, GameCanvas.h - 85 - Screen.cmdH - line*13, GameCanvas.w - padLeft*2, 80 +line*13, info);
		mFont.tahoma_8b.drawString(g, tfInput.title + ": ", tfInput.x - titleLenght -5, tfInput.y + 5, 0);
		tfInput.paint(g);
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		mFont.tahoma_8b.drawString(g, tfInput2.title+": ", tfInput2.x - titleLenght -5, tfInput2.y + 5, 0);
		tfInput2.paint(g);
		super.paint(g);
	}

	public void keyPress(int keyCode) {
		if(focus==0)
			tfInput.keyPressed(keyCode);
		else
			tfInput2.keyPressed(keyCode);
		super.keyPress(keyCode);
	}

	public void update() {
		if (GameCanvas.keyPressed[2]) {
			focus = 0;
		}
		if (GameCanvas.keyPressed[8]) {
			focus = 1;
		}
		if (focus == 0) {
			tfInput.isFocus = true;
			tfInput2.isFocus = false;
			right = tfInput.cmdClear;
			tfInput.update();
		} else {
			tfInput.isFocus = false;
			tfInput2.isFocus = true;
			right = tfInput2.cmdClear;
			tfInput2.update();
		}
		if(GameCanvas.isTouch){
			if(GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick){
				if(GameCanvas.isPointerHoldIn(tfInput.x, tfInput.y, tfInput.width, tfInput.height))
					focus = 0;
				if(GameCanvas.isPointerHoldIn(tfInput2.x, tfInput2.y, tfInput2.width, tfInput2.height))
					focus = 1;
			}
		}
		if(left != null){
			left.x = GameCanvas.w/2 - 160;
			left.y = GameCanvas.h - 26;
		}
		if(center != null){
			center.x = GameCanvas.w/2 - 35;
			center.y = GameCanvas.h - 26;
		}
		
		if(right!= null){
			right.x = GameCanvas.w/2 + 88;
			right.y = GameCanvas.h - 26;
		}
		super.update();
	}

	public void show() {
		GameCanvas.currentDialog = this;
	}

	public void hide() {
		GameCanvas.endDlg();
	}

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		
	}

}
