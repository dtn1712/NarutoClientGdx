package model;


import com.thdgaming.naruto.MGraphics;

import lib.mVector;

public abstract class Effect2 {
	public static mVector vEffect2 = new mVector();
	public static mVector vRemoveEffect2 = new mVector();
	public static mVector vEffect2Outside = new mVector();
	public static mVector vAnimateEffect = new mVector();
	
	public abstract void update();
	public abstract void paint(MGraphics g);
}
