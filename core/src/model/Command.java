
package model;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


import real.mFont;
import screen.old.GameScr;
import lib.Bitmap;


public class Command {

	public String caption;
    public String[] subCaption;
    public IActionListener actionListener;
    public int idAction;
    public Bitmap back , focus , img,imgFocus;
	public int x = 0, y = 0, w = Screen.cmdW, h = Screen.cmdH;
	int lenCaption = 0;
	private boolean isFocus = false;
	public Object p;
    /**
     * @param caption
     * @param action
     */
	 public Command(String caption, IActionListener actionListener,int action, Object p, int x, int y) {
		super();
		this.caption = caption;
		this.idAction = action;
        this.actionListener=actionListener;
        this.p=p;
		this.x = x;
		this.y = y;
		this.w = Screen.cmdW;
		this.h = Screen.cmdH;
		this.back = null;
		this.focus = null;
	}
    public Command(String caption, IActionListener actionListener,int action, Object p) {
        super();
        this.caption = caption;
        this.idAction = action;
        this.actionListener=actionListener;
        this.p=p;
    }
    public Command(String caption, int action, Object p) {
        super();
        this.caption = caption;
        this.idAction = action;
        this.p=p;
    }
    public Command(String caption, int action) {
        super();
        this.caption = caption;
        this.idAction = action;
    }
    public Command(String caption, int action, int x, int y) {
        super();
        this.caption = caption;
        this.idAction = action;
        this.x = x;
        this.y = y;
    }
    
	public void performAction()
	{
		if (idAction > 0){
			if(actionListener!=null)
				actionListener.perform(idAction, p);
			else{
				GameScr.gI().actionPerform(idAction,p);
			}
		}
	}

    public void paint(MGraphics g) {

//		if(GameScr.isPaintTeam){
//			x = GameScr.popupX + GameScr.popupW;
//    		y = GameScr.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			}
			else
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2,2);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
//			}
		}
		

    }
    public void paintW(MGraphics g) {

//		if(GameScr.isPaintTeam){
//			x = GameScr.popupX + GameScr.popupW;
//    		y = GameScr.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);  
		if (img != null) {
			if(isFocus){
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 0,
						x + w/4, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 2,
						x + 3*w/4-2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);

//				graphic.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			}
			else{
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 0,
						x + w/4, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 2,
						x + 3*w/4-2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);

			}
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2,2);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 , y + 6, 2);
//			}
		}
		

    }
    public void paintFocus(MGraphics g, boolean isFocus) {

//		if(GameScr.isPaintTeam){
//			x = GameScr.popupX + GameScr.popupW;
//    		y = GameScr.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			}
			else
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2,2);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
//			}
		}
		

    }
    public void paint(MGraphics g, int transform) { //lat hinh
//		if(GameScr.isPaintTeam){
//			x = GameScr.popupX + GameScr.popupW;
//    		y = GameScr.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawRegion(imgFocus, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER, false);
				//graphic.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER);
			}
			else
				g.drawRegion(img, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER, false);
			
//				graphic.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2 , MGraphics.HCENTER | MGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER| MGraphics.HCENTER);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER| MGraphics.HCENTER);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2, y + 6, MGraphics.VCENTER| MGraphics.HCENTER);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, MGraphics.VCENTER| MGraphics.HCENTER);
//			}
		}
		
}
    public void paint(MGraphics g, Boolean isUclip) {
//		if(GameScr.isPaintTeam){
//			x = GameScr.popupX + GameScr.popupW;
//    		y = GameScr.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, MGraphics.HCENTER | MGraphics.VCENTER,true);
			}
			else
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2 , MGraphics.HCENTER | MGraphics.VCENTER,true);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(),2,true);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(), 2,true);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(), 2,true);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(graphic, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, MGraphics.VCENTER|MGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
//			}
		}
		
}

    public boolean input() {
    	isFocus = false;
    	if (GameCanvas.isPointerHoldIn(x, y, w, h)){
			if (GameCanvas.isPointerDown) 
				isFocus = true;
			if (GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick) {
				return true;
			}
			else if ( GameCanvas.isPointerClick) {
				GameCanvas.isPointerClick = false;
				return true;
			}
    	}
    	
		return false;
	}
    
    public void setPos(int x, int y, Bitmap img, Bitmap imgFocus) {
    	  this.img = img;
    	  this.imgFocus=imgFocus;
    	  this.x = x;
    	  this.y = y;
    	  if (img != null) {
    	   this.w = Image.getWidth(img);
    	   this.h = Image.getHeight(img);
    	  }
    }

}
