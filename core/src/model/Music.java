package model;

import java.io.IOException;

import com.team.njonline.GameMidlet;



public class Music {
	public static Music gI;
	public static boolean isSound = true;
	public static int volume = 0;
	private final static int MAX_VOLUME = 10;
	public static int[] soundID ;

	//MUSIC
	public final static int MLogin = 0;
	public final static int MBClick = 1;
	public final static int MTone = 2;
	public final static int MSanzu = 3;
	public final static int MChakumi = 4;
	public final static int MChai = 5;
	public final static int MOshin = 6;
	public final static int MEchigo = 7;
	public final static int MKojin = 8;
	public final static int MHaruna = 9;
	public final static int MHirosaki = 10;
	public final static int MOokaza = 11;
	public final static int MGiotuyet = 12;
	public final static int MHangdong = 13;
	public final static int MDeKeu = 14;
	public final static int MChimKeu = 15;
	public final static int MBuocChan = 16;
	public final static int MNuocChay = 17;
	public final static int MBomMau = 18;
	
	//SOUND
	public final static int MKiemGo = 19;
	public final static int MKiem = 20;
	public final static int MTieu = 21;
	public final static int MKunai = 22;
	public final static int MCung = 23;
	public final static int MDao = 24;
	public final static int MQuat = 25;
	public final static int MCung2 = 26;
	public final static int MTieu2 = 27;
	public final static int MTieu3 = 28;
	public final static int MKiem2 = 29;
	public final static int MKiem3 = 30;
	public final static int MDao2 = 31;
	public final static int MDao3 = 32;
	public final static int MCung3 = 33;

	public static void init() {}

	public static int getSoundPoolSource(int index, String fileName) 
	{
		return 0;
	}

	
	public static void play(int id, float volume) {

	

	}

	public static void play(int id, float volume, boolean isLoop) {

		
	}


	public static void pauseMusic(int id, int index) {
	}

	public static void resumeMusic(int id, int index) {
	}

	public static Music gI() {
		if (gI == null)
			gI = new Music();
		return gI;
	}


	public static void resumeAll() {
	}

	public static void releaseAll() {
	}

	public static void stopAll() {
	}

	public static void setVolume(int id, int index, int soundVolume) {
	}
}
