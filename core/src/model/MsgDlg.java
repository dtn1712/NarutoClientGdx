package model;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import lib.mSystem;
import real.mFont;


public class MsgDlg extends Dialog {
	public String[] info;
	public boolean isWait;
	public int timeShow;
	public int w = 240,y;
	public boolean isDlgTime = false;
	public int timeLive;
	public long timestart;
	public static Bitmap[] imgpopup = new Bitmap[2];
	public static Bitmap[] imgbtnpopup = new Bitmap[2];
	int h=110;
	private int padLeft;

	public MsgDlg() {
		padLeft = 30;
		if (GameCanvas.w <= 176)
			padLeft = 10;
	}
	public static void loadbegin(){
		for (int i = 0; i < imgpopup.length; i++) {
			imgpopup[i] = GameCanvas.loadImage("/wood_bar"+i+".png");
		}
		for (int i = 0; i < imgbtnpopup.length; i++) {
			imgbtnpopup[i] = GameCanvas.loadImage("/btnpopup"+i+".png");
		}
		
	}
	public void setTimeLive(int time){
		this.isDlgTime = true;
		this.timeLive = time;
		this.timestart = mSystem.currentTimeMillis();
	}
	public void pleasewait() {
		setInfo(mResources.PLEASEWAIT, null, null, null);
		GameCanvas.currentDialog = this;
	}

	public void show() {
		GameCanvas.currentDialog = this;
	}
	
	public void setInfo(String info){
		this.info = mFont.tahoma_7_white.splitFontArray(info, 240);
		isWait = false;
//		h = 80;
//		if (this.info.length >= 5)
//			h = this.info.length * mFont.tahoma_8b.getHeight() + 20;
		if(this.left==null&&this.right==null) {
			this.center.x = GameCanvas.w/2 - 35;
			this. y = GameCanvas.h/2 - h/2+40;
			this.center.y = this. y  + h-54;
		}
	}

	public void setInfo(String info, Command left, Command center, Command right) {
		this.info = mFont.tahoma_7_white.splitFontArray(info, 240);
		this.left = left;
		this.center = center;
		this.right = right;

		this.y = GameCanvas.h - h - 35;
		this. y = GameCanvas.h/2 - h/2+40;
		if(center!=null){
			this.center.x = GameCanvas.w/2 - 35;
			this.center.y = GameCanvas.h - 26;
			
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 115;
				this.left.y = GameCanvas.h - 26;
			}
			
			if(right!=null){
				this.right.x = GameCanvas.w/2 + 45;
				this.right.y = GameCanvas.h - 26;
			}
			if(this.left==null&&this.right==null) {
				this. y = GameCanvas.h/2 - h/2+40;
				this.center.x = GameCanvas.w/2 - imgbtnpopup[1].getWidth()/2;
				this.center.y = this. y  + h-54;
				this.center.img = imgbtnpopup[1];
				this.center.imgFocus = imgbtnpopup[0];
				this.center.w = imgbtnpopup[1].getWidth();
				this.center.h = imgbtnpopup[1].getHeight();
			}
		}else{
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 3*imgbtnpopup[1].getWidth()/2;
				this.left.y = this. y  + h-54;
				this.left.img = imgbtnpopup[1];
				this.left.imgFocus = imgbtnpopup[0];
				this.left.w = imgbtnpopup[1].getWidth();
				this.left.h = imgbtnpopup[1].getHeight();
//				this.left.x = GameCanvas.w/2 - 80;
//				this.left.y = GameCanvas.h - 26;
			}
			if(right!=null){

				this.right.x = GameCanvas.w/2 + imgbtnpopup[1].getWidth()/2;
				this.right.y = this. y  + h-54;
				this.right.img = imgbtnpopup[1];
				this.right.imgFocus = imgbtnpopup[0];
				this.right.w = imgbtnpopup[1].getWidth();
				this.right.h = imgbtnpopup[1].getHeight();
//				this.right.x = GameCanvas.w/2 + 10;
//				this.right.y = GameCanvas.h - 26;
			}
		}
		
		
	
		isWait = false;
//		h = 80;
//		if (this.info.length >= 5)
//			h = this.info.length * mFont.tahoma_8b.getHeight() + 20;
	}

	public void paint(MGraphics g) {
		int yDlg = y;
		//GameCanvas.paint.paintFrame(padLeft, yDlg, GameCanvas.w - (padLeft * 2), h, graphic);
		
		g.drawImage(imgpopup[1], GameCanvas.w/2-imgpopup[1].getWidth()/2, yDlg+imgpopup[0].getHeight()/8, MGraphics.VCENTER| MGraphics.HCENTER);
		g.drawRegion(imgpopup[1], 0, 0, imgpopup[1].getWidth(), imgpopup[1].getHeight(), 2,
				GameCanvas.w/2+imgpopup[1].getWidth()/2, yDlg+imgpopup[0].getHeight()/8, MGraphics.VCENTER| MGraphics.HCENTER);
		mFont.tahoma_10b.drawString(g, "Thông báo", GameCanvas.hw,yDlg-imgpopup[0].getHeight()/3, 2);
		g.drawImage(imgpopup[0], GameCanvas.w/2-imgpopup[0].getWidth()/2, yDlg+imgpopup[0].getHeight(), MGraphics.VCENTER| MGraphics.HCENTER);
		g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
				GameCanvas.w/2+imgpopup[0].getWidth()/2, yDlg+imgpopup[0].getHeight(), MGraphics.VCENTER| MGraphics.HCENTER);
		
		g.drawImage(imgpopup[0], GameCanvas.w/2-imgpopup[0].getWidth()/2, yDlg+2*imgpopup[0].getHeight()-1, MGraphics.VCENTER| MGraphics.HCENTER);
		g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
				GameCanvas.w/2+imgpopup[0].getWidth()/2, yDlg+2*imgpopup[0].getHeight()-1, MGraphics.VCENTER| MGraphics.HCENTER);
		int yStart = yDlg + (h - (info.length==1?1:2) * mFont.tahoma_8b.getHeight()) / 2 - 2-16;
		if (isWait) {
			yStart += 8;
//			GameCanvas.paintShukiren(GameCanvas.hw, yStart - 12, graphic, false);
		}
		for (int i = 0, y = yStart; i < info.length; i++, y += mFont.tahoma_8b.getHeight()+4) {
			mFont.tahoma_7_white.drawString(g, info[i], GameCanvas.hw, y-10+(i>1?mFont.tahoma_7_white.getHeight()+4:0), 2);
		}
		super.paint(g);
	}
	
	

	public void update() {
		if(isDlgTime){
			if((mSystem.currentTimeMillis()-timestart)/1000>timeLive) GameCanvas.endDlg();
		}
		if(timeShow > 0){
			timeShow --;
			if(timeShow == 1){
				GameCanvas.endDlg();
				timeShow = 0;
			}
		}
		super.update();
	}
	
	
}
