package model;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import Objectgame.Char;
import lib.Cout;
import screen.old.GameScr;

public class ServerEffect extends Effect2 {
	public EffectCharPaint eff;
	int i0, x, y, dir = 1;
	Char c;
	
	short loopCount = 0;
	private long endTime = 0;
	
	public static void addServerEffect(int id, int cx, int cy, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.x = cx;
		e.y = cy;
		e.loopCount = (short) loopCount;
		vEffect2.addElement(e);
	}
	
	public static void addServerEffect(int id, int cx, int cy, int loopCount, byte dir) {
		Cout.println(" addserrver "+id);
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.x = cx;
		e.y = cy;
		e.loopCount = (short) loopCount;
		e.dir = dir;
		vEffect2.addElement(e);
	}

	public static void addServerEffect(int id, Char c, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.c = c;
		e.loopCount = (short) loopCount;
		vEffect2.addElement(e);
	}

	public static void addServerEffectWithTime(int id, int cx, int cy, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.x = cx;
		e.y = cy;
		e.endTime = System.currentTimeMillis() + timeLengthInSecond * 1000;
		vEffect2.addElement(e);
	}

	public static void addServerEffectWithTime(int id, Char c, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScr.efs[id];
		e.c = c;
		e.endTime = System.currentTimeMillis() + timeLengthInSecond * 1000;
		vEffect2.addElement(e);
	}
	

	public void paint(MGraphics g) {
		try{
			if (c != null) {
				x = c.cx;
				y = c.cy;
			}
			int xp, yp;
			xp = x + eff.arrEfInfo[i0].dx * dir;
			yp = y + eff.arrEfInfo[i0].dy;
			if (GameCanvas.isPaint(xp, yp)) {
				
				SmallImage.drawSmallImage(g, eff.arrEfInfo[i0].idImg, xp, yp, (dir == 1) ? 0 : 2, MGraphics.VCENTER | MGraphics.HCENTER);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}

	public void update() {
		if (endTime != 0) {
			i0++;
			if (i0 >= eff.arrEfInfo.length) {
				i0 = 0;
			}
			if (System.currentTimeMillis() - endTime > 0){
				System.out.println("endtime ----> "+endTime);
//				if(eff.idEf == 120)
//					GameCanvas.isBallEffect = false;
				vEffect2.removeElement(this);
			}
		} else {
			i0++;
			if (i0 >= eff.arrEfInfo.length) {
				loopCount--;
				if (loopCount <= 0){
//					if(eff.idEf == 120)
//						GameCanvas.isBallEffect = false;
					vEffect2.removeElement(this);
				}
				else
					i0 = 0;
			}
		}
		if (GameCanvas.gameTick % 11 == 0 && c!=null && c!=Char.myChar()) {
			if (!GameScr.vCharInMap.contains(this.c)){
				
				vEffect2.removeElement(this);
			}
		}
	}


}
