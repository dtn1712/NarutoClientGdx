package model;


public class Effect {

	public static EffectTemplate[] effTemplates;
	public static final byte EFF_ME 		= 0;
	public static final byte EFF_FRIEND 	= 1;

	public int timeStart, timeLenght;
	public short param;
	public EffectTemplate template;
	
	public Effect(byte templateId, int timeStart, int timeLenght, short param) {
		this.template = effTemplates[templateId];
		this.timeStart = timeStart;
		this.timeLenght = timeLenght / 1000;
		this.param = param;
	}
}
