package model;

import screen.old.GameScr;

public class Timer {
	public static IActionListener timeListener;
	public static int idAction;
	public static long timeExecute;
	public static boolean isON;

	public static void setTimer(IActionListener actionListener,int action, long timeEllapse) {
	//	System.out.println("SET TIMER " + timeEllapse);
		timeListener = actionListener;
		idAction = action;
		timeExecute = System.currentTimeMillis() + timeEllapse;
		isON = true;
	}

	public static void update() {
		long now = System.currentTimeMillis();
		if (isON && now > timeExecute) {
			//System.out.println("TIMER EXECUTE");
			isON = false;
			try {
				if (idAction > 0){		    		
		    		GameScr.gI().perform(idAction, null);
		    	}
//				if (timerAction != null )
//					timerAction.perform();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
