package model;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;

import com.team.njonline.GameMidlet;
import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import lib.MyStream;

public class EffectData {

	public Bitmap img;
	public ImageInfo[] imgInfo;
	public Frame[] frame;
	public short[] arrFrame;
	public int ID;

	public ImageInfo getImageInfo(byte id) {
		for (int i = 0; i < imgInfo.length; i++)
			if (imgInfo[i].ID == id)
				return imgInfo[i];
		return null;
	}
	
	public static DataInputStream readdatamobFromRes(String path){
		DataInputStream iss = null;
		try {
			iss = new DataInputStream(MyStream.readFile("/mapitem/mapItem"));
			if(iss != null)
				return iss;
			else 
				return null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
		
	}

	public int width, height;

	public void readData(String patch) {
		InputStream iss = null;
		DataInputStream is = null;
		try {
			iss = GameMidlet.asset.open(patch);
			is = new DataInputStream(iss);
		} catch (Exception e) {
			return;
		}
		readData(is);

	}

	public void readData(DataInputStream is) {
		int left = 0, top = 0, right = 0, bottom = 0;
		try {
			byte smallNumImg = is.readByte();
			imgInfo = new ImageInfo[smallNumImg];
			for (int i = 0; i < smallNumImg; i++) {
				imgInfo[i] = new ImageInfo();
				imgInfo[i].ID = is.readByte();
				imgInfo[i].x0 = is.readByte();
				imgInfo[i].y0 = is.readByte();
				imgInfo[i].w = is.readByte();
				imgInfo[i].h = is.readByte();
			}
			short nFrame = is.readShort();

			frame = new Frame[nFrame];
			for (int i = 0; i < nFrame; i++) {
				frame[i] = new Frame();
				byte nPart = is.readByte();
				frame[i].dx = new short[nPart];
				frame[i].dy = new short[nPart];
				frame[i].idImg = new byte[nPart];
				for (int a = 0; a < nPart; a++) {
					frame[i].dx[a] = is.readShort();
					frame[i].dy[a] = is.readShort();
					frame[i].idImg[a] = is.readByte();
					if (i == 0) {
						if (left > frame[i].dx[a])
							left = frame[i].dx[a];
						if (top > frame[i].dy[a])
							top = frame[i].dy[a];
						if (right < frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w)
							right = frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w;
						if (bottom < frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h)
							bottom = frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h;
						width = right - left;
						height = bottom - top;
						
					}
				}

			}
			short nFrameCount = is.readShort();
		//	Res.out("nFrame count = " + nFrameCount + " ...........................................................");
			arrFrame = new short[nFrameCount];
			for (int i = 0; i < nFrameCount; i++) {
				arrFrame[i] = is.readShort();
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void readData(byte[] data) {

		DataInputStream is = null;
		ByteArrayInputStream array = new ByteArrayInputStream(data);
		is = new DataInputStream(array);
		readData(is);
	}

	public void paintFrame(MGraphics g, int f, int x, int y, int trans, int layer) {
		
		
		if (frame != null && frame.length != 0) {
			Frame fr = frame[f];
			for (int i = 0; i < fr.dx.length; i++) {
				ImageInfo im = getImageInfo(fr.idImg[i]);
				 try{
				if (trans == 0) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), 0);
				}
				if (trans == 1) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, Sprite.TRANS_MIRROR, x - fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), StaticObj.TOP_RIGHT);
				}
				 }catch (Exception e) {
//				 System.out.println("ERROR AT EFFECTDATA "+ID+" "+im.x0+","+
//				 im.y0+","+im.w+","+ im.h);
				 }
			}

		}
	}
}
