package model;

public class Member {

	public int iconId;
	public int level;
	public int type;
	public String name;
	public boolean isOnline;
	public int pointClan, pointClanWeek;
	
	public Member(int classId, int level, int type, String name, int pointClan, boolean isOnline){
		switch (classId) {
			case 0: // chua nhap
				iconId = 647;
				break;
			case 1: // kiem
				iconId = 1182;
				break;
			case 2: // fi tien
				iconId = 1181;
				break;
			case 3: // kunai
				iconId = 643;
				break;
			case 4: // cung
				iconId = 645;
				break;
			case 5: // dao
				iconId = 676;
				break;
			case 6: // quat
				iconId = 1119;
				break;
		}
		this.level = level;
		this.type = type;
		this.name = name;
		this.pointClan = pointClan;
		this.isOnline = isOnline;
	}
}
