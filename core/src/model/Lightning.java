package model;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.mVector;

public class Lightning extends Effect2 {
	public static void addLight(mVector position, EPosition firstPos, boolean isContinue, int color) {
		Lightning l = new Lightning();
		l.color[1] = color;
		for (int i = 0; i < position.size(); i++) {
			EPosition pCon = (EPosition) position.elementAt(i);
			if (pCon != null) {
				if (Res.abs(pCon.x - firstPos.x) >= 100 || Res.abs(pCon.y - firstPos.y) >= 50)
					position.removeElementAt(i);
			}
		}
		l.setInfo(position, firstPos, isContinue);
		vEffect2.addElement(l);
	}

	int[] color = new int[] { 0xfcfcfd, 0xaab7fc };
	public mVector listPos = new mVector(), list[];
	public EPosition posangle;
	long timeDel = 0;
	public boolean isContinue = false, isRemove = true, isRun;

	public Lightning() {
	}

	public void reset() {
		timeDel = (System.currentTimeMillis() / 10);
		listPos.removeAllElements();
	}

	public void setInfo(mVector position, EPosition angle, boolean isContinue) {
		if (position.size() == 0)
			return;
		isRun = true;
		this.isContinue = isContinue;
		if (!isContinue)
			orderVector(position);
		listPos = position;
		posangle = angle;
		list = new mVector[position.size()];
		for (int i = 0; i < list.length; i++)
			list[i] = new mVector();
		angle.follow = -1;
		list[0].addElement(angle);
		int index = -1;
		for (int i = 0; i < position.size(); i++) {
			int x = angle.x;
			int y = angle.y;
			if (isContinue && index != -1) {
				EPosition pCon = (EPosition) position.elementAt(index);
				x = pCon.x;
				y = pCon.y;
				
			}
			if (!isContinue)
				index = rndIndexList(position);
			else
				index++;
//			int rnd = rndIndex(position, 1);
			int follow = list[index].size() - 1;
			EPosition posTo = (EPosition) position.elementAt(index);
			int angle1 = Res.angle(posTo.x - x, -(posTo.y - y));
			int disPart = Res.rnd(15) + 10;
			int a = 0;
			int ang = 0;
			while (true) {
				ang = 0;
				if (a != 0)
					ang = angle1 - 5 + Res.rnd(10);
				ang = Res.fixangle(ang);
				int xa = ((disPart * (a)) * (Res.cos(ang))) >> 10;
				int ya = -((disPart * (a)) * Res.sin(ang)) >> 10;
				EPosition posNext = new EPosition(x + xa, y + ya, follow++);
				list[index].addElement(posNext);
				if (Res.distance(x, y, x + xa, y + ya) >= Res.distance(x, y, posTo.x, posTo.y) - 20) {
					break;
				}
				a++;
			}
		}
		//
		for (int i = 0; i < list.length; i++) {
			int limit = list[i].size();
			EPosition pa = (EPosition) position.elementAt(i);
			pa.follow = (byte) (list[i].size() - 1);
			pa.index = -1;
			EPosition pz = new EPosition(pa.x, pa.y, pa.follow);
			pz.index = -1;
			list[i].addElement(pz);
			for (int a = 1; a < limit; a++) {
				EPosition p = (EPosition) list[i].elementAt(a);
				int rnd = Res.rnd(2);
				for (int b = 0; b < rnd; b++) {
					int ang = 180 + Res.rnd(180);
					int dis = 5 + Res.rnd(10);
					int xa = (dis * (Res.cos(Res.fixangle(ang)))) >> 10;
					int ya = -(dis * Res.sin(Res.fixangle(ang))) >> 10;
					EPosition po = new EPosition(p.x + xa, p.y + ya, a);
					po.index = 0;
					list[i].addElement(po);
				}
			}
		}
		//
	}

	public static mVector orderVector(mVector obj) {
		int a = obj.size();
		for (int i = 0; i < a - 1; i++) {
			EPosition o = (EPosition) obj.elementAt(i);
			for (int j = i + 1; j < a; j++) {
				EPosition o1 = (EPosition) obj.elementAt(j);
				if (o.x > o1.x) {
					obj.setElementAt(o, j);
					obj.setElementAt(o1, i);
					o = o1;
				}
			}
		}
		return obj;
	}

//	private int rndIndex(Vector pos, int chieu) {
//		int num = 0;
//		for (int i = 0; i < pos.size(); i++) {
//			Position p = (Position) pos.elementAt(i);
//			if (p.index != -1)
//				num++;
//		}
//		if (num != 0)
//			num = Res.rnd(num);
//		else
//			return -1;
//		int num1 = 0;
//		for (int i = 0; i < pos.size(); i++) {
//			Position p = (Position) pos.elementAt(i);
//			if (p.index != -1) {
//				num1++;
//				if (num == num1) {
//					return i;
//				}
//			}
//		}
//		return -1;
//	}

	private int rndIndexList(mVector pos) {
		int num = 0;
		for (int i = 0; i < pos.size(); i++) {
			EPosition p = (EPosition) pos.elementAt(i);
			if (p.index == -1)
				num++;
		}
		if (num != 0)
			num = Res.rnd(num);
		else
			return -1;
		int num1 = 0;
		for (int i = 0; i < pos.size(); i++) {
			EPosition p = (EPosition) pos.elementAt(i);
			if (p.index == -1) {
				if (num == num1) {
					p.index = 0;
					return i;
				}
				num1++;
			}
		}
		return -1;
	}
	int tick=0;
	public void update() {
		if(posangle == null){
			vRemoveEffect2.addElement(this);
			return;
		}
		int line = 0; 
		try{
		if (GameCanvas.gameTick % 2 == 1) {
			line = 1;
			posangle.follow = -1;
			posangle.index = -1;
			line = 2;
			for (int i = 0; i < listPos.size(); i++) {
				EPosition pos = (EPosition) listPos.elementAt(i);
				pos.index = -1;
				pos.follow = -1;
			}
			line = 3;
			if (isContinue && isRemove && listPos.size() > 1 && (System.currentTimeMillis() / 10 - timeDel > 30)) {
				timeDel = System.currentTimeMillis() / 10;
				posangle = (EPosition) listPos.elementAt(0);
				listPos.removeElementAt(0);
			}
			line = 4;
			setInfo(listPos, posangle, isContinue);
			if (tick>3) {
				aa = 7;
				isRun = false;
				vEffect2.removeElement(this);
			}
			line = 5;
			tick++;
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	int cou = 0;
	int dem = 0, aa = 7;

	public void paint(MGraphics g) {
		dem = 0;
		// Res.imgSmoke.drawFrame(cou/3, posangle.x, posangle.y, 0, 3, graphic);
		cou++;
		if (cou >= 12)
			cou = 0;
		if (list != null)
			for (int i = 0; i < list.length; i++) {
				for (int a = 0; a < list[i].size(); a++) {
					EPosition pos0 = (EPosition) list[i].elementAt(a);
					if (pos0.follow < 0 || pos0.follow >= list[i].size())
						continue;
					EPosition pos1 = (EPosition) list[i].elementAt(pos0.follow);
					if(GameCanvas.isPaint(pos0.x, pos0.y)&&GameCanvas.isPaint(pos1.x, pos1.y))
						paintLine(g, pos0, pos1);
					if (isContinue && isRemove) {
						dem++;
						if (dem >= aa) {
							aa += 7;
							return;
						}
					}
				}
				EPosition p = (EPosition) listPos.elementAt(i);
				// Res.backSmoke.drawFrame(1+p.count/4, p.x, p.y, 0, 3, graphic);
				p.count++;
				if (p.count >= 12)
					p.count = 0;
			}
	}

	private void paintLine(MGraphics g, EPosition pos1, EPosition pos2) {
		g.setColor(color[0]);
		g.drawLine(pos1.x, pos1.y, pos2.x, pos2.y);
		if (pos1.index == -1) {
			g.setColor(color[1]);
			g.drawLine(pos1.x - 1, pos1.y, pos2.x - 1, pos2.y);
			if ((isContinue) && isRemove)
				g.drawLine(pos1.x + 1, pos1.y, pos2.x + 1, pos2.y);
		}
	}
}
