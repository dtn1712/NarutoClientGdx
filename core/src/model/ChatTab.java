package model;

import java.util.Vector;

import lib.mVector;

import real.mFont;
import screen.old.GameScr;

public class ChatTab {
	public ChatTab(String ownerName, int type) {
		this.ownerName = ownerName;
		this.type = type;
	}

	public ChatTab() {
		
	}

	public int type; // 0:public, 1:party, 2:PM, 3:Global, 4:Clan
	public String ownerName;
	public mVector contents = new mVector();

	public void addChat(String whoChat, String text) {
		boolean isCameraLow = false;
		if (GameScr.isPaintMessage && ChatManager.gI().getCurrentChatTab() == this && GameScr.indexRow == contents.size() - 1) {
			isCameraLow = true;
		}
		contents.addElement("c3@" + whoChat);
		Vector v = mFont.tahoma_7_white.splitFontVector(text, 160);
		for (int i = 0; i < v.size(); i++)
			contents.addElement("c0" + v.elementAt(i));

//		if (isCameraLow) {
//			GameScr.getInstance().scrollDownAlert();
//		}
		clear();
	}

	private void clear() {
		while (true) {
			if (contents.size() > 50) {
				contents.removeElementAt(1);
			} else
				break;
		}

	}

	public void addInfo(String text) {
		boolean isCameraLow = false;
		if (GameScr.isPaintMessage && ChatManager.gI().getCurrentChatTab() == this && GameScr.indexRow == contents.size() - 1) {
			isCameraLow = true;
		}
		Vector v = mFont.tahoma_7_white.splitFontVector(text, 160);
		for (int i = 0; i < v.size(); i++)
			contents.addElement(v.elementAt(i));

//		if (isCameraLow) {
//			GameScr.getInstance().scrollDownAlert();
//		}
		clear();
	}
}
