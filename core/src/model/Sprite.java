package model;

import lib.Bitmap;

public class Sprite extends Layer {

	public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT90 = 5;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;
    
    public static final int TRANS_MIRROR = 2;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_MIRROR_ROT180 = 1;
    public static final int TRANS_MIRROR_ROT270 = 4;
    

    private static final int INVERTED_AXES = 0x4;
    private static final int X_FLIP = 0x2;
    private static final int Y_FLIP = 0x1;
    private static final int ALPHA_BITMASK = 0xff000000;
    private static final int FULLY_OPAQUE_ALPHA = 0xff000000;

    Bitmap sourceImage;

    int numberFrames; // = 0;

    int[] frameCoordsX;

    int[] frameCoordsY;

    int srcFrameWidth;

    int srcFrameHeight;

    int[] frameSequence;

    private int sequenceIndex; // = 0

    private boolean customSequenceDefined; // = false;

    int dRefX; // =0

    int dRefY; // =0

    int collisionRectX; // =0

    int collisionRectY; // =0

    int collisionRectWidth;

    int collisionRectHeight;

    int t_currentTransformation;

    int t_collisionRectX;

    int t_collisionRectY;

    int t_collisionRectWidth;

    int t_collisionRectHeight;

    Paint mPaint;
    Rect mSrcRect;
    Rect mDestRect;

    public Sprite(Bitmap image) {
        super(Image.getWidth(image), Image.getHeight(image));
        
        mPaint = new Paint();
        mSrcRect = new Rect();
        mDestRect = new Rect();

        initializeFrames(image, Image.getWidth(image), Image.getHeight(image), false);

        initCollisionRectBounds();

        setTransformImpl(TRANS_NONE);        
    }

    public Sprite(Bitmap image, int frameWidth, int frameHeight) {

        super(frameWidth, frameHeight);

        mPaint = new Paint();
        mSrcRect = new Rect();
        mDestRect = new Rect();

        if ((frameWidth < 1 || frameHeight < 1) ||
            ((Image.getWidth(image) % frameWidth) != 0) ||
            ((Image.getHeight(image) % frameHeight) != 0)) {
             throw new IllegalArgumentException();
        }

        initializeFrames(image, frameWidth, frameHeight, false);

        initCollisionRectBounds();

        setTransformImpl(TRANS_NONE);        
    }

    public Sprite(Sprite s) {

        super(s != null ? s.getWidth() : 0,
                     s != null ? s.getHeight() : 0);

        if (s == null) {
            throw new NullPointerException();
        }

        this.sourceImage = Bitmap.createBitmap(s.sourceImage);

        this.numberFrames = s.numberFrames;

        this.frameCoordsX = new int[this.numberFrames];
        this.frameCoordsY = new int[this.numberFrames];

        System.arraycopy(s.frameCoordsX, 0, 
                         this.frameCoordsX, 0,
                         s.getRawFrameCount());

        System.arraycopy(s.frameCoordsY, 0, 
                         this.frameCoordsY, 0,
                         s.getRawFrameCount());

        this.x = s.getX();
        this.y = s.getY();

        this.dRefX = s.dRefX;
        this.dRefY = s.dRefY;

        this.collisionRectX = s.collisionRectX;
        this.collisionRectY = s.collisionRectY;
        this.collisionRectWidth = s.collisionRectWidth;
        this.collisionRectHeight = s.collisionRectHeight;

        this.srcFrameWidth = s.srcFrameWidth;
        this.srcFrameHeight = s.srcFrameHeight;

        setTransformImpl(s.t_currentTransformation);

        this.setVisible(s.isVisible()); 

        this.frameSequence = new int[s.getFrameSequenceLength()];
        this.setFrameSequence(s.frameSequence);
        this.setFrame(s.getFrame());

        this.setRefPixelPosition(s.getRefPixelX(), s.getRefPixelY());
        
    }

    public void defineReferencePixel(int inp_x, int inp_y) {
        dRefX = inp_x;
        dRefY = inp_y;
    }

    public void setRefPixelPosition(int inp_x, int inp_y) {

        x = inp_x - getTransformedPtX(dRefX, dRefY, t_currentTransformation);
        y = inp_y - getTransformedPtY(dRefX, dRefY, t_currentTransformation);

    }
      
    public int getRefPixelX() {
        return (this.x + getTransformedPtX(dRefX, dRefY, this.t_currentTransformation));
    }
    
    public int getRefPixelY() {
        return (this.y + getTransformedPtY(dRefX, dRefY, this.t_currentTransformation));
    }

    public void setFrame(int inp_sequenceIndex) {
        if (inp_sequenceIndex < 0 || inp_sequenceIndex >= frameSequence.length) {
            throw new IndexOutOfBoundsException();
        }
        sequenceIndex = inp_sequenceIndex;
    }

    public final int getFrame() {
        return sequenceIndex;
    }

    public int getRawFrameCount() {
        return numberFrames;
    }

    public int getFrameSequenceLength() {
        return frameSequence.length;
    }

    public void nextFrame() {
        sequenceIndex = (sequenceIndex + 1) % frameSequence.length;
    }

    public void prevFrame() {
        if (sequenceIndex == 0) {
            sequenceIndex = frameSequence.length - 1;
        } else {
            sequenceIndex--;
        }
    }

    public final void paint() {}

    public void setFrameSequence(int sequence[]) {

        if (sequence == null) {
            // revert to the default sequence
            sequenceIndex = 0;
            customSequenceDefined = false;
            frameSequence = new int[numberFrames];
            // copy frames indices into frameSequence
            for (int i = 0; i < numberFrames; i++)
            {
                frameSequence[i] = i;
            }
            return;
        }

        if (sequence.length < 1) {
             throw new IllegalArgumentException();
        }

        for (int i = 0; i < sequence.length; i++)
        {
            if (sequence[i] < 0 || sequence[i] >= numberFrames) {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        customSequenceDefined = true;
        frameSequence = new int[sequence.length];
        System.arraycopy(sequence, 0, frameSequence, 0, sequence.length);
        sequenceIndex = 0;
    }
   
    public void setImage(Bitmap img, int frameWidth, int frameHeight) {

        if ((frameWidth < 1 || frameHeight < 1) ||
            ((Image.getWidth(img) % frameWidth) != 0) ||
            ((Image.getHeight(img) % frameHeight) != 0)) {
             throw new IllegalArgumentException();
        }

        int noOfFrames = (Image.getWidth(img) / frameWidth)*(Image.getHeight(img) / frameHeight);

        boolean maintainCurFrame = true;
        if (noOfFrames < numberFrames) {
            maintainCurFrame = false; 
            customSequenceDefined = false;
        }

        if (! ((srcFrameWidth == frameWidth) &&
               (srcFrameHeight == frameHeight))) {

            int oldX = this.x + 
                getTransformedPtX(dRefX, dRefY, this.t_currentTransformation);

            int oldY = this.y +
                getTransformedPtY(dRefX, dRefY, this.t_currentTransformation);


            setWidthImpl(frameWidth);
            setHeightImpl(frameHeight);

            initializeFrames(img, frameWidth, frameHeight, maintainCurFrame);

            initCollisionRectBounds();

            this.x = oldX - 
                getTransformedPtX(dRefX, dRefY, this.t_currentTransformation);

            this.y = oldY -
                getTransformedPtY(dRefX, dRefY, this.t_currentTransformation);

            computeTransformedBounds(this.t_currentTransformation);

        } else {
            initializeFrames(img, frameWidth, frameHeight, maintainCurFrame);
        }

    }

    public void defineCollisionRectangle(int inp_x, int inp_y, 
                                         int width, int height) {

        if (width < 0 || height < 0) {
             throw new IllegalArgumentException();
        }

        collisionRectX = inp_x;
        collisionRectY = inp_y;
        collisionRectWidth = width;
        collisionRectHeight = height;

        setTransformImpl(t_currentTransformation);
    }

    public void setTransform(int transform) {
        setTransformImpl(transform);
    }

    public final boolean collidesWith(Sprite s, boolean pixelLevel) 
    {
        
        // check if either of the Sprite's are not visible
        if (!(s.visible && this.visible)) {
            return false;
        }
    
        // these are package private 
        // and can be accessed directly
        int otherLeft    = s.x + s.t_collisionRectX;
        int otherTop     = s.y + s.t_collisionRectY;
        int otherRight   = otherLeft + s.t_collisionRectWidth;
        int otherBottom  = otherTop  + s.t_collisionRectHeight;
    
        int left   = this.x + this.t_collisionRectX;
        int top    = this.y + this.t_collisionRectY;
        int right  = left + this.t_collisionRectWidth;
        int bottom = top  + this.t_collisionRectHeight;
    
        // check if the collision rectangles of the two sprites intersect
        if (intersectRect(otherLeft, otherTop, otherRight, otherBottom,
                  left, top, right, bottom)) {
    
            // collision rectangles intersect
            if (pixelLevel) {
    
                if (this.t_collisionRectX < 0) {
                    left = this.x;
                }
                if (this.t_collisionRectY < 0) {
                    top = this.y;
                }
                if ((this.t_collisionRectX + this.t_collisionRectWidth)
                    > this.width) {
                    right = this.x + this.width;
                }
                if ((this.t_collisionRectY + this.t_collisionRectHeight)
                    > this.height) {
                    bottom = this.y + this.height;
                }
        
                // similarly for the other Sprite
                if (s.t_collisionRectX < 0) {
                    otherLeft = s.x;
                }
                if (s.t_collisionRectY < 0) {
                    otherTop = s.y;
                }
                if ((s.t_collisionRectX + s.t_collisionRectWidth)
                    > s.width) {
                    otherRight = s.x + s.width;
                }
                if ((s.t_collisionRectY + s.t_collisionRectHeight)
                    > s.height) {
                    otherBottom = s.y + s.height;
                }
        
                if (!intersectRect(otherLeft, otherTop, otherRight, otherBottom,
                          left, top, right, bottom)) {
        
                    // if they don't intersect, return false;
                    return false;
                }
        
                int intersectLeft = (left < otherLeft) ? otherLeft : left;
                int intersectTop  = (top < otherTop) ? otherTop : top;
        
                // used once, optimize.
                int intersectRight  = (right < otherRight) 
                                      ? right : otherRight;
                int intersectBottom = (bottom < otherBottom) 
                                      ? bottom : otherBottom;
        
                int intersectWidth  = Math.abs(intersectRight - intersectLeft);
                int intersectHeight = Math.abs(intersectBottom - intersectTop);
        
                int thisImageXOffset = getImageTopLeftX(intersectLeft, 
                                    intersectTop,
                                    intersectRight,
                                    intersectBottom);
                
                int thisImageYOffset = getImageTopLeftY(intersectLeft, 
                                    intersectTop,
                                    intersectRight,
                                    intersectBottom);
        
                int otherImageXOffset = s.getImageTopLeftX(intersectLeft, 
                                       intersectTop,
                                       intersectRight,
                                       intersectBottom);
                
                int otherImageYOffset = s.getImageTopLeftY(intersectLeft, 
                                       intersectTop,
                                       intersectRight,
                                       intersectBottom);
        
                // check if opaque pixels intersect.
        
                return doPixelCollision(thisImageXOffset, thisImageYOffset,
                            otherImageXOffset, otherImageYOffset,
                            this.sourceImage,
                            this.t_currentTransformation,
                            s.sourceImage, 
                            s.t_currentTransformation,
                            intersectWidth, intersectHeight);
        
            } else {
                // collides!
                return true;
            }
        }
        return false;
    }

    public final boolean collidesWith(TiledLayer t, boolean pixelLevel) {

        // check if either this Sprite or the TiledLayer is not visible
        if (!(t.visible && this.visible)) {
            return false;
        }

        int tLx1 = t.x;
        int tLy1 = t.y;
        int tLx2 = tLx1 + t.width;
        int tLy2 = tLy1 + t.height;

        int tW = t.getCellWidth();
        int tH = t.getCellHeight();

        int sx1 = this.x + this.t_collisionRectX;
        int sy1 = this.y + this.t_collisionRectY;
        int sx2 = sx1 + this.t_collisionRectWidth;
        int sy2 = sy1 + this.t_collisionRectHeight;

        // number of cells
        int tNumCols = t.getColumns();
        int tNumRows = t.getRows();

        // temporary loop variables.
        int startCol; // = 0;
        int endCol;   // = 0;
        int startRow; // = 0;
        int endRow;   // = 0;

        if (!intersectRect(tLx1, tLy1, tLx2, tLy2, sx1, sy1, sx2, sy2)) {
           
            return false;
        }

            startCol = (sx1 <= tLx1) ? 0 : (sx1 - tLx1)/tW;
            startRow = (sy1 <= tLy1) ? 0 : (sy1 - tLy1)/tH;
            endCol = (sx2 < tLx2) ? ((sx2 - 1 - tLx1)/tW) : tNumCols - 1;
            endRow = (sy2 < tLy2) ? ((sy2 - 1 - tLy1)/tH) : tNumRows - 1;

        if (!pixelLevel) {
            // check for intersection with a non-empty cell,
            for (int row = startRow; row <= endRow; row++) {
                for (int col = startCol; col <= endCol; col++) {
                    if (t.getCell(col, row) != 0) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            if (this.t_collisionRectX < 0) {
                sx1 = this.x;
            }
            if (this.t_collisionRectY < 0) {
                sy1 = this.y;
            }
            if ((this.t_collisionRectX + this.t_collisionRectWidth)
                > this.width) {
                sx2 = this.x + this.width;
            }
            if ((this.t_collisionRectY + this.t_collisionRectHeight)
                > this.height) {
                sy2 = this.y + this.height;
            }
                
            if (!intersectRect(tLx1, tLy1, tLx2, tLy2, sx1, sy1, sx2, sy2)) {
                return (false);
            }
            startCol = (sx1 <= tLx1) ? 0 : (sx1 - tLx1)/tW;
            startRow = (sy1 <= tLy1) ? 0 : (sy1 - tLy1)/tH;
            endCol = (sx2 < tLx2) ? ((sx2 - 1 - tLx1)/tW) : tNumCols - 1;
            endRow = (sy2 < tLy2) ? ((sy2 - 1 - tLy1)/tH) : tNumRows - 1;

            // current cell coordinates
            int cellTop    = startRow * tH + tLy1;
            int cellBottom = cellTop  + tH;

            // the index of the current tile.
            int tileIndex; // = 0;

            for (int row = startRow; row <= endRow; 
                 row++, cellTop += tH, cellBottom += tH) {

                // current cell coordinates
                int cellLeft   = startCol * tW + tLx1;
                int cellRight  = cellLeft + tW;

                for (int col = startCol; col <= endCol; 
                     col++, cellLeft += tW, cellRight += tW) {

                    tileIndex = t.getCell(col, row);

                    if (tileIndex != 0) {
                        
                        int intersectLeft = (sx1 < cellLeft) ? cellLeft : sx1;
                        int intersectTop  = (sy1 < cellTop) ? cellTop  : sy1;
                        
                        // used once, optimize.
                        int intersectRight  = (sx2 < cellRight) ? 
                                               sx2 : cellRight;
                        int intersectBottom = (sy2 < cellBottom) ? 
                                               sy2 : cellBottom;

                        if (intersectLeft > intersectRight) {
                            int temp = intersectRight;
                            intersectRight = intersectLeft;
                            intersectLeft = temp;
                        }

                        if (intersectTop > intersectBottom) {
                            int temp = intersectBottom;
                            intersectBottom = intersectTop;
                            intersectTop = temp;
                        }

                        int intersectWidth  = intersectRight  - intersectLeft;
                        int intersectHeight = intersectBottom - intersectTop;

                        int image1XOffset = getImageTopLeftX(intersectLeft, 
                                                             intersectTop,
                                                             intersectRight,
                                                             intersectBottom);

                        int image1YOffset = getImageTopLeftY(intersectLeft, 
                                                             intersectTop,
                                                             intersectRight,
                                                             intersectBottom);

                        int image2XOffset = t.tileSetX[tileIndex] +
                                            (intersectLeft - cellLeft);
                        int image2YOffset = t.tileSetY[tileIndex] +
                                            (intersectTop - cellTop);

                        if (doPixelCollision(image1XOffset,
                                             image1YOffset,
                                             image2XOffset,
                                             image2YOffset,
                                             this.sourceImage, 
                                             this.t_currentTransformation,
                                             t.sourceImage,
                                             TRANS_NONE,
                                             intersectWidth, intersectHeight)) {
                            // intersection found with this tile
                            return true;
                        }
                    }
                } // end of for col
            }// end of for row

            return false;
        }

    }

    public final boolean collidesWith(Bitmap image, int inp_x, 
                                     int inp_y, boolean pixelLevel) {

        // check if this Sprite is not visible
        if (!(visible)) {
            return false;
        }

        // if image is null 
        // image.getWidth() will throw NullPointerException
        int otherLeft    = inp_x;
        int otherTop     = inp_y;
        int otherRight   = inp_x + Image.getWidth(image);
        int otherBottom  = inp_y + Image.getHeight(image);

        int left   = x + t_collisionRectX;
        int top    = y + t_collisionRectY;
        int right  = left + t_collisionRectWidth;
        int bottom = top  + t_collisionRectHeight;

        // first check if the collision rectangles of the two sprites intersect
        if (intersectRect(otherLeft, otherTop, otherRight, otherBottom,
                          left, top, right, bottom)) {

            // collision rectangles intersect
            if (pixelLevel) {

                if (this.t_collisionRectX < 0) {
                    left = this.x;
                }
                if (this.t_collisionRectY < 0) {
                    top = this.y;
                }
                if ((this.t_collisionRectX + this.t_collisionRectWidth)
                    > this.width) {
                    right = this.x + this.width;
                }
                if ((this.t_collisionRectY + this.t_collisionRectHeight)
                    > this.height) {
                    bottom = this.y + this.height;
                }

                // recheck if the updated collision area rectangles intersect
                if (!intersectRect(otherLeft, otherTop, 
                                   otherRight, otherBottom,
                                   left, top, right, bottom)) {

                    // if they don't intersect, return false;
                    return false;
                }

                // within the collision rectangles
                int intersectLeft = (left < otherLeft) ? otherLeft : left;
                int intersectTop  = (top < otherTop) ? otherTop : top;

                // used once, optimize.
                int intersectRight  = (right < otherRight) 
                                      ? right : otherRight;
                int intersectBottom = (bottom < otherBottom) 
                                      ? bottom : otherBottom;

                int intersectWidth  = Math.abs(intersectRight - intersectLeft);
                int intersectHeight = Math.abs(intersectBottom - intersectTop);

                int thisImageXOffset = getImageTopLeftX(intersectLeft, 
                                                        intersectTop,
                                                        intersectRight,
                                                        intersectBottom);
                
                int thisImageYOffset = getImageTopLeftY(intersectLeft, 
                                                        intersectTop,
                                                        intersectRight,
                                                        intersectBottom);

                int otherImageXOffset = intersectLeft - inp_x;
                int otherImageYOffset = intersectTop  - inp_y;

                // check if opaque pixels intersect.
                return doPixelCollision(thisImageXOffset, thisImageYOffset,
                                        otherImageXOffset, otherImageYOffset,
                                        this.sourceImage,
                                        this.t_currentTransformation,
                                        image, 
                                        Sprite.TRANS_NONE,
                                        intersectWidth, intersectHeight);

            } else {
                // collides!
                return true;
            }
        }
        return false;

    }

    private void initializeFrames(Bitmap image, int fWidth, int fHeight, boolean maintainCurFrame) {

        int imageW = Image.getWidth(image);
        int imageH = Image.getHeight(image);
            
        int numHorizontalFrames = imageW / fWidth;
        int numVerticalFrames   = imageH / fHeight;

        sourceImage = image;

        srcFrameWidth = fWidth;
          srcFrameHeight = fHeight;

        numberFrames = numHorizontalFrames*numVerticalFrames;

        frameCoordsX = new int[numberFrames];
        frameCoordsY = new int[numberFrames];

        if (!maintainCurFrame) {
            sequenceIndex = 0;
        }

        if (!customSequenceDefined) {
            frameSequence = new int[numberFrames];
        }

        int currentFrame = 0;

        for (int yy = 0; yy < imageH; yy += fHeight) {
            for (int xx = 0; xx < imageW; xx += fWidth) {

                frameCoordsX[currentFrame] = xx;
                frameCoordsY[currentFrame] = yy;
        
                if (!customSequenceDefined) {
                    frameSequence[currentFrame] = currentFrame;
                }
                currentFrame++;

            }
        }
    }

    /**
     * initialize the collision rectangle
     */
    private void initCollisionRectBounds() {

        // reset x and y of collision rectangle
        collisionRectX = 0;
        collisionRectY = 0;

        // intialize the collision rectangle bounds to that of the sprite
        collisionRectWidth = this.width;
        collisionRectHeight = this.height;

    }

    private boolean intersectRect(int r1x1, int r1y1, int r1x2, int r1y2, 
                                  int r2x1, int r2y1, int r2x2, int r2y2) {
        if (r2x1 >= r1x2 || r2y1 >= r1y2 || r2x2 <= r1x1 || r2y2 <= r1y1) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean doPixelCollision(int image1XOffset,
                                            int image1YOffset,
                                            int image2XOffset,
                                            int image2YOffset,
                                            //Image image1, int transform1, 
                                            //Image image2, int transform2,
                                            Bitmap image1, int transform1, 
                                            Bitmap image2, int transform2,
                                            int width, int height) {

        // starting point of comparison
        int startY1;
        // x and y increments
        int xIncr1, yIncr1;

        // .. for image 2
        int startY2;
        int xIncr2, yIncr2;

        int numPixels = height * width;

        int[] argbData1 = new int[numPixels];
        int[] argbData2 = new int[numPixels];

        if (0x0 != (transform1 & INVERTED_AXES)) {
           
            if (0x0 != (transform1 & Y_FLIP)) {
                xIncr1 = -(height); // - scanlength

                startY1 = numPixels - height; // numPixels - scanlength
            } else {
                xIncr1 = height; // + scanlength
                
                startY1 = 0;
            }

            if (0x0 != (transform1 &  X_FLIP)) {
                yIncr1 = -1;

                startY1 += (height - 1);
            } else {
                yIncr1 = +1;
            }

            image1.getPixels(argbData1, 0, height, image1XOffset, image1YOffset, height, width);

        } else {

            // scanlength = width

            if (0x0 != (transform1 & Y_FLIP)) {

                startY1 = numPixels - width; // numPixels - scanlength

                yIncr1  = -(width); // - scanlength
            } else {
                startY1 = 0;

                yIncr1  = width; // + scanlength
            }

            if (0x0 != (transform1 &  X_FLIP)) {
                xIncr1  = -1;

                startY1 += (width - 1);
            } else {
                xIncr1  = +1;
            }

            image1.getPixels(argbData1, 0, width, image1XOffset, image1YOffset, width, height);


        }


        if (0x0 != (transform2 & INVERTED_AXES)) {
            // inverted axes

            if (0x0 != (transform2 & Y_FLIP)) {
                xIncr2 = -(height);

                startY2 = numPixels - height;
            } else {
                xIncr2 = height;

                startY2 = 0;
            }

            if (0x0 != (transform2 &  X_FLIP)) {
                yIncr2 = -1;
                
                startY2 += height - 1;
            } else {
                yIncr2 = +1;
            }

            image2.getPixels(argbData2, 0, height, image2XOffset, image2YOffset, height, width);

        } else {

            if (0x0 != (transform2 & Y_FLIP)) {
                startY2 = numPixels - width;

                yIncr2  = -(width);
            } else {
                startY2 = 0;

                yIncr2  = +width;
            }

            if (0x0 != (transform2 &  X_FLIP)) {
                xIncr2  = -1;

                startY2 += (width - 1);
            } else {
                xIncr2  = +1;
            }

            image2.getPixels(argbData2, 0, width, image2XOffset, image2YOffset, width, height);

        }


        int x1, x2;
        int xLocalBegin1, xLocalBegin2;

        // the loop counters
        int numIterRows;
        int numIterColumns;

        for (numIterRows = 0, xLocalBegin1 = startY1, xLocalBegin2 = startY2;
            numIterRows < height;
            xLocalBegin1 += yIncr1, xLocalBegin2 += yIncr2, numIterRows++) {

            for (numIterColumns = 0, x1 = xLocalBegin1, x2 = xLocalBegin2;
                 numIterColumns < width;
                 x1 += xIncr1, x2 += xIncr2, numIterColumns++) {

                if (((argbData1[x1] & ALPHA_BITMASK) == FULLY_OPAQUE_ALPHA) && 
                    ((argbData2[x2] & ALPHA_BITMASK) == FULLY_OPAQUE_ALPHA)) {

                    return true;
                }
                
            } // end for x        
            
        } // end for y
        
        // worst case!  couldn't find a single colliding pixel!
        return false;
    }

    private int getImageTopLeftX(int x1, int y1, int x2, int y2) {
        int retX = 0;

        switch (this.t_currentTransformation) {

        case TRANS_NONE:
        case TRANS_MIRROR_ROT180:
            retX = x1 - this.x;
            break;

        case TRANS_MIRROR:
        case TRANS_ROT180:
            retX = (this.x + this.width) - x2;
            break;

        case TRANS_ROT90:
        case TRANS_MIRROR_ROT270:
            retX = y1 - this.y;
            break;

        case TRANS_ROT270:
        case TRANS_MIRROR_ROT90:
            retX = (this.y + this.height) - y2;
            break;

        default:

            return retX;
        }

        retX += frameCoordsX[frameSequence[sequenceIndex]];

        return retX;
    }

    private int getImageTopLeftY(int x1, int y1, int x2, int y2) {
        int retY = 0;

        switch (this.t_currentTransformation) {

        case TRANS_NONE:
        case TRANS_MIRROR:
            retY = y1 - this.y;
            break;

        case TRANS_ROT180:
        case TRANS_MIRROR_ROT180:
            retY = (this.y + this.height) - y2;
            break;

        case TRANS_ROT270:
        case TRANS_MIRROR_ROT270:
            retY = x1 - this.x;
            break;

        case TRANS_ROT90:
        case TRANS_MIRROR_ROT90:
            retY = (this.x + this.width) - x2;
            break;
        }

        retY += frameCoordsY[frameSequence[sequenceIndex]];

        return retY;
    }

    /**
     * Sets the transform for this Sprite
     *
     * @param transform the desired transform for this Sprite
     */
    private void setTransformImpl(int transform) {

        this.x = this.x + 
            getTransformedPtX(dRefX, dRefY, this.t_currentTransformation) -
            getTransformedPtX(dRefX, dRefY, transform);

        this.y = this.y +
            getTransformedPtY(dRefX, dRefY, this.t_currentTransformation) -
            getTransformedPtY(dRefX, dRefY, transform);

        computeTransformedBounds(transform);

        // set the current transform to be the one requested
        t_currentTransformation = transform;

    }

    private void computeTransformedBounds(int transform) {
        switch (transform) {

        case TRANS_NONE:

            t_collisionRectX = collisionRectX;
            t_collisionRectY = collisionRectY;
            t_collisionRectWidth = collisionRectWidth;
            t_collisionRectHeight = collisionRectHeight;
            this.width = srcFrameWidth;
            this.height = srcFrameHeight;

            break;

        case TRANS_MIRROR:

            t_collisionRectX = srcFrameWidth - 
                               (collisionRectX + collisionRectWidth);

            t_collisionRectY = collisionRectY;
            t_collisionRectWidth = collisionRectWidth;
            t_collisionRectHeight = collisionRectHeight;

            this.width = srcFrameWidth;
            this.height = srcFrameHeight;

            break;

        case TRANS_MIRROR_ROT180:

            t_collisionRectY = srcFrameHeight - 
                               (collisionRectY + collisionRectHeight);

            t_collisionRectX = collisionRectX;
            t_collisionRectWidth = collisionRectWidth;
            t_collisionRectHeight = collisionRectHeight;

            // width and height are as before
            this.width = srcFrameWidth;
            this.height = srcFrameHeight;
    
            break;

        case TRANS_ROT90:

            t_collisionRectX = srcFrameHeight - 
                               (collisionRectHeight + collisionRectY);
            t_collisionRectY = collisionRectX;

            t_collisionRectHeight = collisionRectWidth;
            t_collisionRectWidth = collisionRectHeight;

            // set width and height
            this.width = srcFrameHeight;
            this.height = srcFrameWidth;

            break;

        case TRANS_ROT180:

            t_collisionRectX = srcFrameWidth - (collisionRectWidth + 
                                                collisionRectX);
            t_collisionRectY = srcFrameHeight - (collisionRectHeight + 
                                                 collisionRectY);

            t_collisionRectWidth = collisionRectWidth;
            t_collisionRectHeight = collisionRectHeight;

            // set width and height
            this.width = srcFrameWidth;
            this.height = srcFrameHeight;

            break;

        case TRANS_ROT270:

            t_collisionRectX = collisionRectY;
            t_collisionRectY = srcFrameWidth - (collisionRectWidth + 
                                                collisionRectX);

            t_collisionRectHeight = collisionRectWidth;
            t_collisionRectWidth = collisionRectHeight;

            // set width and height
            this.width = srcFrameHeight;
            this.height = srcFrameWidth;

            break;

        case TRANS_MIRROR_ROT90:

            t_collisionRectX = srcFrameHeight - (collisionRectHeight + 
                                                 collisionRectY);
            t_collisionRectY = srcFrameWidth - (collisionRectWidth + 
                                                collisionRectX);

            t_collisionRectHeight = collisionRectWidth;
            t_collisionRectWidth = collisionRectHeight;

            // set width and height
            this.width = srcFrameHeight;
            this.height = srcFrameWidth;

            break;

        case TRANS_MIRROR_ROT270:

            t_collisionRectY = collisionRectX;
            t_collisionRectX = collisionRectY;

            t_collisionRectHeight = collisionRectWidth;
            t_collisionRectWidth = collisionRectHeight;

            // set width and height
            this.width = srcFrameHeight;
            this.height = srcFrameWidth;

            break;

        default:
            // INVALID TRANSFORMATION!
            throw new IllegalArgumentException();

        }
    }

    int getTransformedPtX(int inp_x, int inp_y, int transform) {

        int t_x = 0;

          switch (transform) {

          case TRANS_NONE:
             t_x = inp_x;
              break;
          case TRANS_MIRROR:
             t_x = srcFrameWidth - inp_x - 1;
              break;
          case TRANS_MIRROR_ROT180:
             t_x = inp_x;
              break;
          case TRANS_ROT90:
             t_x = srcFrameHeight - inp_y - 1;
              break;
          case TRANS_ROT180:
             t_x = srcFrameWidth - inp_x - 1;
              break;
          case TRANS_ROT270:
             t_x = inp_y;
              break;
          case TRANS_MIRROR_ROT90:
             t_x = srcFrameHeight - inp_y - 1;
              break;
          case TRANS_MIRROR_ROT270:
             t_x = inp_y;
              break;
          default:
              // for safety/completeness.
//              Logging.report(Logging.ERROR, LogChannels.LC_HIGHUI,
//                             "Sprite: transform=" + transform);

              break;
          }

        return t_x;

    }

    int getTransformedPtY(int inp_x, int inp_y, int transform) {

        int t_y = 0;

          switch (transform) {
  
          case TRANS_NONE:
             t_y = inp_y;
              break;
          case TRANS_MIRROR:
             t_y = inp_y;
              break;
          case TRANS_MIRROR_ROT180:
             t_y = srcFrameHeight - inp_y - 1;
              break;
          case TRANS_ROT90:
             t_y = inp_x;
              break;
          case TRANS_ROT180:
             t_y = srcFrameHeight - inp_y - 1;
              break;
          case TRANS_ROT270:
             t_y = srcFrameWidth - inp_x - 1;
              break;
          case TRANS_MIRROR_ROT90:
             t_y = srcFrameWidth - inp_x - 1;
              break;
          case TRANS_MIRROR_ROT270:
             t_y = inp_x;
              break;
          default:

              break;
          }

        return t_y;

    }
}
