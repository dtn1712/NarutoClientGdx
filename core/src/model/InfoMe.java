package model;


import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import Objectgame.InfoItem;

import lib.mVector;


public class InfoMe {
	public static mVector infoWaitToShow = new mVector();
	public static InfoItem info;
	public static int p1 = 5, p2, p3, x, strWidth;
	public static int limLeft = 2;
	public static int hI = 20;
	public static void paint(MGraphics g) {
		int xI = limLeft, yI = GameCanvas.h - 23, wI = GameCanvas.w;
		if (GameCanvas.isTouch) {
			if (GameCanvas.w >= 450) {
				xI = 130;
				wI = GameCanvas.w - 2 * xI;
			} else {
				xI = 80;
				wI = GameCanvas.w - 2 * xI - 10;
			}
			yI = GameCanvas.h - 60;
			limLeft = xI + 2;
		}
		if (info == null)
			return;
		if ((GameCanvas.currentDialog != null && GameCanvas.currentDialog.center != null))
			return;
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		
		if(GameCanvas.isTouch)
			GameCanvas.paint.paintFrame(xI, yI-4, wI + 10, hI+8,g);
		else{
			g.setColor(0);
			g.fillRect(xI - 2, yI, wI + 2, hI);
		}
		g.setClip(xI, yI, wI, hI);
//		info.f.drawString(graphic, info.s, x, yI + 3, 0);
		
	}

	public static void update() {
	
		if (p1 == 0) // Chay tu phai qua trai
		{
			x += (limLeft - x) / 3;
			if (x - limLeft < 3) {
				x = limLeft + 2;
				p1 = 2;
				p2 = 0;
			}
		} else if (p1 == 2) {
			p2++;
			if (p2 > info.speed) {
				p1 = 3;
				p2 = 0;
			}
		} else if (p1 == 3) {
			if (x + strWidth < limLeft+GameCanvas.w-20)				
				x -= 6;
			else
				x -= 2;
			if (x + strWidth < limLeft) {
				p1 = 4;
				p2 = 0;
			}
		} else if (p1 == 4) {
			p2++;
			if (p2 > 10) {
				p1 = 5;
				p2 = 0;
			}
		} else if (p1 == 5) {
			if (infoWaitToShow.size() > 0) {
				InfoItem next = (InfoItem) infoWaitToShow.firstElement();
				infoWaitToShow.removeElementAt(0);
				if (info != null && next.s.equals(info.s)) {
					return;
				}
				info = next;
//				strWidth = info.f.getWidth(info.s);
				p1 = p2 = 0;
				x = GameCanvas.w;
			} else{
				info = null;
			
			}	
		}
		
	

	}

	public static void addInfo(String s) {

		if(canMergeString(s))return;
		if (GameCanvas.w == 128)
			limLeft = 1;
		if(infoWaitToShow.size()>10)
			infoWaitToShow.removeElementAt(0);
		infoWaitToShow.addElement(new InfoItem(s));
		
	}

	private static boolean canMergeString(String s) {
		if (info != null && info.s != null && s.equals(info.s))
			return true;
		
		if (infoWaitToShow.size() > 0 && s.equals(((InfoItem) (infoWaitToShow.lastElement())).s))
			return true;
		if (s.length() < 8)
			return false;
		
		if (info != null && info.s != null && p1 < 3 && info.s.length() >= 8)
		{
			String m1 = s.substring(0, 8);
			String m2 = info.s.substring(0,8);
			if(m1.equals(m2))
			{
				int i=7;
				for(;i<s.length();i++)
				{
					if(i>=info.s.length())break;
					if ((int) s.charAt(i) >= 48 && (int) s.charAt(i) <= 57) break;
					if(s.charAt(i)!=info.s.charAt(i))break;
				
				}
				String append = s.substring(i,s.length());
				info.s+=", "+append;
				p1=2;p2=0;
				return true;
			}
		}
		if(infoWaitToShow.size()>0)
		{
			String s2=((InfoItem)(infoWaitToShow.lastElement())).s;
			if (s2.length() >= 8)
			{
				String m1 = s.substring(0, 8);
				String m2 = s2.substring(0,8);
				if(m1.equals(m2))
				{
					int i=7;
					for(;i<s.length();i++)
					{
						if(i>=s2.length())break;
						if ((int) s.charAt(i) >= 48 && (int) s.charAt(i) <= 57) break;
						if(s.charAt(i)!=s2.charAt(i))break;
					}
					String append = s.substring(i,s.length());
					s2+=", "+append;
					return true;
				}
			}
		}
		return false;
	}

	public static void addInfo(String s, int speed, mFont f) {
		if(canMergeString(s))return;
		if (GameCanvas.w == 128)
			limLeft = 1;
		if(infoWaitToShow.size()>10)
			infoWaitToShow.removeElementAt(0);
//		infoWaitToShow.addElement(new InfoItem(s, f, speed));
	}

	public static boolean isEmpty() {
		return p1 == 5 && infoWaitToShow.size() == 0;
	}
}
