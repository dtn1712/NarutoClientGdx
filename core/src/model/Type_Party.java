package model;

public class Type_Party {
	
	 public static final byte INVITE_PARTY = 0;
	 public static final byte ACCEPT_INVITE_PARTY = 1;
	 public static final byte OUT_PARTY = 2;
	 public static final byte REQUEST_JOIN_PARTY= 3;
	 public static final byte KICK_OUT_PARTY= 4;
	 public static final byte CHANGE_BOSS_PARTY= 5;
	 public static final byte GET_INFOR_PARTY= 6;
	 public static final byte GET_INFOR_NEARCHAR= 7;
	 public static final byte DISBAND_PARTY= 8;
	
}
