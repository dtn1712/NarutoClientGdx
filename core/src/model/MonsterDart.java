package model;



import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import screen.old.GameScr;
import Objectgame.Mob;


import Objectgame.Char;

public class MonsterDart extends Effect2 {
	
	DartInfo info;

	public int angle;
	public int vx, vy; // van toc theo phuong x, phuong y
	public int va = 256 * 48; // SPEED
	public int x, y, z,xTo,yTo;
	int life;
	public boolean isSpeedUp = false;
	public int dame, dameMp, typeAtt;
	public Char c;
	
	public boolean isBoss;
	public int idBoss;
	public byte levelBoss;
	int countangle = 1;
	public void setAngle(int angle) {
		this.angle = angle;
	
		vx = (va * Res.cos(angle)) >> 10;
		vy = (va * Res.sin(angle)) >> 10;
	}

	public boolean isEnd = false;

	public static void addMonsterDart(int x, int y, boolean isBoss, int dame, int dameMp, Char c,int dartType) {
		vEffect2.addElement(new MonsterDart(x, y, isBoss, dame, dameMp, c, dartType));
	}
	public static void addMonsterDart(int x, int y, boolean isBoss, int dame, int dameMp, int xTo,int yTo,int dartType) {
		vEffect2.addElement(new MonsterDart(x, y, isBoss, dame, dameMp, xTo,yTo, dartType));
	}

	public MonsterDart(int x, int y,boolean isBoss, byte levelBoss, int idBoss, int dame, int dameMp, Char c) {
		this.typeAtt = 0;
		this.x = x;
		this.y = y;
		this.isBoss = isBoss;
		this.levelBoss = levelBoss;
		this.idBoss = idBoss;
		this.dame = dame;
		this.dameMp = dameMp;
		this.c = c;
		if (isBoss) {
			setAngle(countangle * 90);
			countangle++;
			if (countangle > 3)
				countangle = 1;
		}else{
		if (x > c.cx)
			setAngle(240);
		else
			setAngle(300);
		}
	}
	
	public MonsterDart(int x, int y, boolean isBoss, int dame, int dameMp, Char c,int dartType) {
		info = GameScr.darts[dartType];
		this.x = x;
		this.y = y;
		this.isBoss = isBoss;
		this.dame = dame;
		this.dameMp = dameMp;
		this.c = c;
		va = info.va;
		setAngle(Res.angle(c.cx-x, c.cy-y));
//		if(x>=GameScr.cmx && x<=GameScr.cmx+GameCanvas.w)
//			SoundMn.gI().mobKame(dartType);
	}
	
	public MonsterDart(int x, int y, boolean isBoss, int dame, int dameMp,int xTo,int yTo,int dartType) {
		info = GameScr.darts[dartType];
		this.x = x;
		this.y = y;
		this.isBoss = isBoss;
		this.dame = dame;
		this.dameMp = dameMp;
		this.xTo=xTo;
		this.yTo=yTo;
		va = info.va;
		setAngle(Res.angle(xTo-x, yTo-y));
//		if(x>=GameScr.cmx && x<=GameScr.cmx+GameCanvas.w)
//			SoundMn.gI().mobKame(dartType);
		c=null;
	}
	
	

	public void update() {
		int dx = 0, dy = 0;
		int a;
		if(typeAtt == 0){
			dx = c.cx - x;
			dy = c.cy - 10 - y;
			life++;
			if (c.statusMe == Char.A_DEADFLY || c.statusMe == Char.A_DEAD) {
				x += (c.cx - x) / 2;
				y += (c.cy - y) / 2;
			}
			if ((Res.abs(dx) < 16 && Res.abs(dy) < 16) || life > 60) {
				c.doInjure(dame, dameMp, isBoss, idBoss);
				vEffect2.removeElement(this);
				return;
			}
		}
	
		
		
		a = Res.angle(dx, dy);
		if (Math.abs(a - angle) < 90 || dx * dx + dy * dy > 64 * 64) {
			if (Math.abs(a - angle) < 15)
				angle = a;
			else if (a - angle >= 0 && a - angle < 180 || a - angle < -180)
				angle = Res.fixangle(angle + 15);
			else
				angle = Res.fixangle(angle - 15);
		}
		if (!isSpeedUp)
			if (va < 8 << 10)
				va += 1024;
		vx = (va * Res.cos(angle)) >> 10;
		vy = (va * Res.sin(angle)) >> 10;
		//
		dx += vx;
		int deltaX = dx >> 10;
		x += deltaX;
		dx = dx & 0x3ff;
		dy += vy;
		int deltaY = dy >> 10;
		y += deltaY;
		dy = dy & 0x3ff;
	}
	private int tick(int time) {
		return GameCanvas.gameTick % time;
	}

	public void paint(MGraphics g) {
		//------level boss
		int tick;
			if(isBoss){
				tick = tick(7);
				if(idBoss == 114){
					if (tick < 4)
						SmallImage.drawSmallImage(g, 1299, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else
						SmallImage.drawSmallImage(g, 1307, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
				}else if(idBoss == 115){
					tick = tick(20);
					if (tick < 4)
						SmallImage.drawSmallImage(g, 1379, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 8)
						SmallImage.drawSmallImage(g, 1380, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 12)
						SmallImage.drawSmallImage(g, 1379, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 16)
						SmallImage.drawSmallImage(g, 1382, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
				}else if(idBoss == 116){
					tick = tick(17);
					if (tick < 4)
						SmallImage.drawSmallImage(g, 1399, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 8)
						SmallImage.drawSmallImage(g, 1400, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 12)
						SmallImage.drawSmallImage(g, 1401, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 16)
						SmallImage.drawSmallImage(g, 1402, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
				}else if(idBoss == 139){
					tick = tick(20);
					if (tick < 4)
						SmallImage.drawSmallImage(g, 1459, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 8)
						SmallImage.drawSmallImage(g, 1380, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 12)
						SmallImage.drawSmallImage(g, 1461, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 16)
						SmallImage.drawSmallImage(g, 1382, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
				}else if(idBoss == 144 || idBoss == 163){
					tick = tick(20);
					if (tick < 4)
						SmallImage.drawSmallImage(g, 1459, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 8)
						SmallImage.drawSmallImage(g, 1380, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 12)
						SmallImage.drawSmallImage(g, 1461, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
					else if (tick < 16)
						SmallImage.drawSmallImage(g, 1382, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
				}
					
			}
			else if(levelBoss > 0)
				SmallImage.drawSmallImage(g, 926, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
			else 
				SmallImage.drawSmallImage(g, 1022, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
	}
	
	public static void addMonsterDart(int x2, int y2, boolean checkIsBoss, int dame2, int dameMp2, Mob mobToAttack,
			byte dartType) {
		// TODO Auto-generated method stub
		addMonsterDart(x2, y2, checkIsBoss, dame2, dameMp2, mobToAttack.x, mobToAttack.y, dartType);
	}
	
}
