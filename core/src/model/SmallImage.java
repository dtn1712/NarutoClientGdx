package model;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


import lib.Bitmap;
import lib.Hashtable;

import lib.Rms;
import real.Service;
import real.mFont;
import screen.old.GameScr;

public class SmallImage {
	public static int[][] smallImg;
	public static SmallImage instance;
	public static Bitmap imgbig[];
	public static Hashtable imgNew = new Hashtable();
	public static Bitmap imgEmpty = null;
	
	public static SmallImage gI() {
		if (instance == null) {
			instance = new SmallImage();
		}
		return instance;
	}
	
	public static void freeBig()
	{
		imgbig = null;
		System.gc();
	}
	public static void loadBigImage() {
		imgbig = null;
		System.gc();
		imgbig = new Bitmap[] { GameCanvas.loadImage("/img/Big0.png"), GameCanvas.loadImage("/img/Big1.png"), GameCanvas.loadImage("/img/Big2.png"), GameCanvas.loadImage("/img/Big3.png"), GameCanvas.loadImage("/img/Big4.png"),GameCanvas.loadImage("/img/Big5.png"),GameCanvas.loadImage("/img/Big6.png"), GameCanvas.loadImage("/img/Big7.png")};
		imgEmpty = Image.createImage(1,1);
	}

	public SmallImage() {
		readImage();
	}

	public static void init() {
		instance = null;
		instance = new SmallImage();

	}
	public static int IdBigImage[];
	public static void readImage() {
		try {
			DataInputStream file;
//			file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_image"));
			file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_image")));
//			file = GameCanvas.readdatafile("file/shinobi_image");
			int sum = file.readShort();
			System.out.println("SUM IMAGE ---> "+sum);
//			sum = 7;
			IdBigImage = new int[sum];
			smallImg = new int[sum][7];
			for (int i = 0; i < sum; i++) {
				IdBigImage[i] = i;
				smallImg[i][0] = file.readShort();// id hình lớn //
				//System.out.println("ID HINH LON ----> "+smallImg[i][0]);
				smallImg[i][1] = file.readShort();// x cắt
				smallImg[i][2] = file.readShort();// y cắt
				smallImg[i][3] = file.readShort();// w cắt
				smallImg[i][4] = file.readShort();// h cắt
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static boolean isExitsImage(int id){
		if(id >= smallImg.length
				|| smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth() 
				|| smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
				|| smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
				|| smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
			){
			
			Image img = (Image) imgNew.get(id + "");
			if (img == null || img.equals(imgEmpty)) {
				Service.gI().requestIcon(id);
				return false;
			}
		}
		return true;
	}
	
	public static int getWith(int id) {
		return smallImg[id][5];
	}

	public static int getHeight(int id) {
		return smallImg[id][6];
	}
	

	public static void drawSmallImage(MGraphics g, int id, int x, int y, int transform, int anchor) {
		
		//loadFromServer(id);
//		System.out.println("BIG IMAGE ----> "+smallImg[id][0]);
		try{
			Bitmap img = null;
			if(id >= smallImg.length || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth() 
					|| smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
					|| smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
					|| smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()){
//				if(id >= 926 && id <= 1051){
//					Image img = (Image) imgNew.get(id+"");
//					if(img == null)
//						System.out.println("IMAGE NULLL K PAINT");
//				}
				
				img = (Bitmap) imgNew.get(id+"");
//				if(img == null){
//					System.out.println("ImageNull");
//					imgNew.put(id+"", imgEmpty);
//					Service.getInstance().requestIcon(id);
//				}
//				else
					if(img != null){
						g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor);
					}
			}
			else{
				if(id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038){
//					System.out.println(smallImg[id][0]+","+smallImg[id][1]+" , "+ smallImg[id][2]+ " , "+smallImg[id][3]+" , "+smallImg[id][4]+" --------> "+imgbig[smallImg[id][0]]+" , "+ x +" , "+y);
					GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
//					graphic.setColor(0xffffff);
//					graphic.drawRect(x, y, 100, 100);
				}
				g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

public static void drawSmallImage(MGraphics g, int id, int x, int y, int transform, int anchor, Boolean isUclip) {
		
		//loadFromServer(id);
		try{
			Bitmap img = null;
			if(id >= smallImg.length
					 ||smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth() 
					|| smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
					|| smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
					|| smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
					/*|| smallImg[id][5] >= imgbig[smallImg[id][0]].getHeight()
					|| smallImg[id][6] >= imgbig[smallImg[id][0]].getHeight()*/){
//				if(id >= 926 && id <= 1051){
//					Image img = (Image) imgNew.get(id+"");
//					if(img == null)
//						System.out.println("IMAGE NULLL K PAINT");
//				}
				
				img = (Bitmap) imgNew.get(id+"");
//				if(img == null){
//					System.out.println("ImageNull");
//					imgNew.put(id+"", imgEmpty);
//					Service.getInstance().requestIcon(id);
//				}
//				else
					if(img != null){
						g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor,isUclip);
					}
			}
			else{
				if(id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038){
//					System.out.println(smallImg[id][0]+","+smallImg[id][1]+" , "+ smallImg[id][2]+ " , "+smallImg[id][3]+" , "+smallImg[id][4]+" --------> "+imgbig[smallImg[id][0]]+" , "+ x +" , "+y);
					GameScr.startFlyText("FUCK", x, y, 0, 2, mFont.RED);
//					graphic.setColor(0xffffff);
//					graphic.drawRect(x, y, 100, 100);
				}
				g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4], transform, x, y, anchor,isUclip);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
//	public static void loadFromServer(Part body){
//		short id = GameScr.parts[].template.iconID;
//		if(id == 1891){
//			System.out.println("zzzzzzzzzzz");
//			for(int i = 0; i < 26; i++){
//				id += i;
//				Image img = (Image) imgNew.get(id+"");
//				if(img == null){
//					imgNew.put(id+"", imgEmpty);
//					Service.getInstance().requestIcon(id);
//			}
//		}
//	}
//	
//	}
}
