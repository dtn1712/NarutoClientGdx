package model;

public class Cmd {
	
	public static final byte LOGIN = 1;
	public static final byte CHAR_INFO = 3;
	public static final byte PLAYER_MOVE = 4;
	public static final byte PLAYER_INFO = 5;
	public static final byte REQUEST_MONSTER_INFO = 6;
	public static final byte PLAYER_REMOVE = 7;
	public static final byte ATTACK = 8;
	public static final byte ITEM = 9;
	public static final byte CHANGE_MAP = 10;
	public static final byte SELECT_CHAR = 11;
	public static final byte CREATE_CHAR = 12;
	public static final byte GIVEUP_ITEM = 13; //vứt bỏ item
	public static final byte PICK_REMOVE_ITEM = 14;
	public static final byte NPC_REQUEST = 17;
	public static final byte BUY_ITEM_FROM_SHOP = 18;
	public static final byte CHAT= 19;
	public static final byte MENU_NPC = 24;
	public static final byte CHAR_LIST = 27;
	public static final byte DROP_ITEM = 31;
	public static final byte PARTY = 32;
	public static final byte TRADE = 34;//trade
	public static final byte SKILL_CHAR= 35;
	public static final byte DIALOG = 37;
	public static final byte REQUEST_IMAGE = -51;
	public static final byte GET_ITEM_INVENTORY = 30;
	public static final byte DIE = 39;
	public static final byte FRIEND = 40;
	public static final byte QUEST = 43;//quest
	public static final byte NPC = 44;
	public static final byte REMOVE_TARGET = 53;
	public static final byte REQUEST_SHOP = 54; // yeu cau item
	public static final byte CMD_DYNAMIC_MENU = 55;// menu shop;
	public static final byte NPC_TEAMPLATE = 57;
	public static final byte MAP_TEAMPLATE = 58;
	
	
	
	public static final byte KEY_WINDOWPHONE = -127;
	public static final byte FULL_SIZE = -126;

}
