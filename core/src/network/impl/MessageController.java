package network.impl;

import java.util.HashMap;
import java.util.Map;

import model.Cmd;
import real.controller.ActorDeadController;
import real.controller.AddInventorySlotController;
import real.controller.AddXuFromGoldController;
import real.controller.AlertMessageController;
import real.controller.AlertTimeController;
import real.controller.AttackController;
import real.controller.BossAppearController;
import real.controller.BossDisappearController;
import real.controller.ChangeMapController;
import real.controller.ChangeRegionController;
import real.controller.CharConfigController;
import real.controller.CharExpController;
import real.controller.CharInfoController;
import real.controller.CharListController;
import real.controller.CharPositionController;
import real.controller.CharReviveController;
import real.controller.CharSkillStudyController;
import real.controller.CharWearingController;
import real.controller.ChatController;
import real.controller.ComeHomeController;
import real.controller.CompetedAttackController;
import real.controller.GetInventoryController;
import real.controller.IController;
import real.controller.ItemDropController;
import real.controller.ItemTemplateController;
import real.controller.LoginController;
import real.controller.MapTemplateController;
import real.controller.MenuNpcController;
import real.controller.MonsterInfoController;
import real.controller.MonsterMoveController;
import real.controller.MoveController;
import real.controller.NpcController;
import real.controller.NpcTemplateController;
import real.controller.PartyController;
import real.controller.PickUpItemController;
import real.controller.PlayerOutController;
import real.controller.RegionInfoController;
import real.controller.RegisterController;
import real.controller.RemoveExileController;
import real.controller.RequestAuthKeyController;
import real.controller.RequestCharInfoController;
import real.controller.RequestImageController;
import real.controller.RequestListImageController;
import real.controller.RequestMenuShopController;
import real.controller.RequestShopInfoController;
import real.controller.ServerNotificationController;
import real.controller.ServerTopRightMsgController;
import real.controller.SkillCharController;
import real.controller.StatusAttackController;
import real.controller.TakeOffItemTradeController;
import real.controller.TalkNPCController;
import real.controller.UpdateCharController;
import real.controller.UpdateClientController;
import real.controller.UpgradeItemController;
import real.controller.clan.CreateClanController;
import real.controller.clan.GetClanInfoController;
import real.controller.clan.GetClanInvitationController;
import real.controller.clan.GetListClanController;
import real.controller.friend.FriendInviteController;
import real.controller.friend.RequestFriendListController;
import real.controller.friend.UnfriendController;
import real.controller.minigame.JoinMiniGameController;
import real.controller.minigame.MiniGameBombZoneController;
import real.controller.minigame.MiniGamePlayerStatusController;
import real.controller.minigame.MiniGameTemplateController;
import real.controller.quest.DailyQuestController;
import real.controller.quest.QuestController;
import real.controller.trade.AcceptInviteTradeController;
import real.controller.trade.CancelTradeController;
import real.controller.trade.CompleteTradeController;
import real.controller.trade.ConfirmTradeController;
import real.controller.trade.InviteTradeController;
import real.controller.trade.LockTradeController;
import real.controller.trade.MoveItemTradeController;

public class MessageController {

    private static final Map<Byte, IController> controllers = new HashMap<Byte, IController>();

    private static final MessageController instance = new MessageController();

    public static MessageController getInstance()
    {
        return instance;
    }

    private MessageController()
    {
        controllers.put(Cmd.REQUEST_AUTH_KEY, new RequestAuthKeyController());
        controllers.put(Cmd.QUEST, new QuestController());
        controllers.put(Cmd.DAILY_QUEST, new DailyQuestController());
        controllers.put(Cmd.TALK_NPC, new TalkNPCController());
        controllers.put(Cmd.SKILL_CHAR, new SkillCharController());
        controllers.put(Cmd.REQUEST_SHOP_INFO, new RequestShopInfoController());
        controllers.put(Cmd.REQUEST_MENU_SHOP, new RequestMenuShopController());
        controllers.put(Cmd.CHAR_CONFIG, new CharConfigController());
        controllers.put(Cmd.PARTY, new PartyController());
        controllers.put(Cmd.MENU_NPC, new MenuNpcController());
        controllers.put(Cmd.NPC, new NpcController());
        controllers.put(Cmd.PICK_UP_ITEM, new PickUpItemController());
        controllers.put(Cmd.ITEM_DROP, new ItemDropController());
        controllers.put(Cmd.CHANGE_REGION, new ChangeRegionController());
        controllers.put(Cmd.ACTOR_DEAD, new ActorDeadController());
        controllers.put(Cmd.ATTACK, new AttackController());
        controllers.put(Cmd.MONSTER_INFO, new MonsterInfoController());
        controllers.put(Cmd.CHAT, new ChatController());
        controllers.put(Cmd.GET_ITEM_INVENTORY, new GetInventoryController());
        controllers.put(Cmd.PLAYER_OUT, new PlayerOutController());
        controllers.put(Cmd.BASIC_CHAR_INFO, new RequestCharInfoController());
        controllers.put(Cmd.REQUEST_IMAGE, new RequestImageController());
        controllers.put(Cmd.REQUEST_LIST_IMAGE, new RequestListImageController());
        controllers.put(Cmd.CHANGE_MAP, new ChangeMapController());
        controllers.put(Cmd.MAIN_CHAR_INFO, new CharInfoController());
        controllers.put(Cmd.CHAR_LIST, new CharListController());
        controllers.put(Cmd.LOGIN, new LoginController());
        controllers.put(Cmd.NPC_TEMPLATE, new NpcTemplateController());
        controllers.put(Cmd.MAP_TEMPLATE, new MapTemplateController());
        controllers.put(Cmd.CHAR_SKILL_STUDY, new CharSkillStudyController());
        controllers.put(Cmd.UPDATE_CHAR, new UpdateCharController());
        controllers.put(Cmd.COME_HOME, new ComeHomeController());
        controllers.put(Cmd.REGISTER, new RegisterController());
        controllers.put(Cmd.CHAR_EXP, new CharExpController());
        controllers.put(Cmd.REGION_INFO, new RegionInfoController());
        controllers.put(Cmd.COMPETED_ATTACK, new CompetedAttackController());
        controllers.put(Cmd.STATUS_ATTACK, new StatusAttackController());
        controllers.put(Cmd.SERVER_NOTIFICATION, new ServerNotificationController());
        controllers.put(Cmd.UPDATE_CLIENT, new UpdateClientController());
        controllers.put(Cmd.UPGRADE_ITEM, new UpgradeItemController());
        controllers.put(Cmd.BOSS_APPEAR, new BossAppearController());
        controllers.put(Cmd.BOSS_DISAPPEAR, new BossDisappearController());
        controllers.put(Cmd.REMOVE_EXILE, new RemoveExileController());
        controllers.put(Cmd.CHAR_WEARING, new CharWearingController());
        controllers.put(Cmd.CHAR_REVIVE, new CharReviveController());
        controllers.put(Cmd.ITEM_TEMPLATE, new ItemTemplateController());
        controllers.put(Cmd.CHAR_POSITION, new CharPositionController());
        controllers.put(Cmd.TRADE_INVITE, new InviteTradeController());
        controllers.put(Cmd.TRADE_ACCEPT_INVITE, new AcceptInviteTradeController());
        controllers.put(Cmd.TRADE_CANCEL, new CancelTradeController());
        controllers.put(Cmd.TRADE_MOVE_ITEM, new MoveItemTradeController());
        controllers.put(Cmd.TRADE_TAKE_OFF, new TakeOffItemTradeController());
        controllers.put(Cmd.TRADE_COMPLETE, new CompleteTradeController());
        controllers.put(Cmd.TRADE_LOCK, new LockTradeController());
        controllers.put(Cmd.TRADE_CONFIRM, new ConfirmTradeController());
        controllers.put(Cmd.FRIEND_INVITE, new FriendInviteController());
        controllers.put(Cmd.FRIEND_GET_LIST, new RequestFriendListController());
        controllers.put(Cmd.FRIEND_KICK, new UnfriendController());
        controllers.put(Cmd.ALERT_TIME, new AlertTimeController());
        controllers.put(Cmd.SERVER_TOP_RIGHT_MSG, new ServerTopRightMsgController());
        controllers.put(Cmd.ALERT_MESSAGE, new AlertMessageController());
        controllers.put(Cmd.MONSTER_MOVE, new MonsterMoveController());

        controllers.put(Cmd.MINI_GAME_TEMPLATE, new MiniGameTemplateController());
        controllers.put(Cmd.MINI_GAME_BOMB_ZONE, new MiniGameBombZoneController());
        controllers.put(Cmd.JOIN_MINI_GAME, new JoinMiniGameController());
        controllers.put(Cmd.MINI_GAME_PLAYER_STATUS, new MiniGamePlayerStatusController());

        controllers.put(Cmd.CLAN_CREATE, new CreateClanController());
        controllers.put(Cmd.CLAN_GET_LIST, new GetListClanController());
        controllers.put(Cmd.CLAN_GET_INFO, new GetClanInfoController());
        controllers.put(Cmd.CLAN_GET_LIST_INVITATIONS, new GetClanInvitationController());

        controllers.put(Cmd.ADD_XU_FROM_GOLD, new AddXuFromGoldController());
        controllers.put(Cmd.ADD_INVENTORY_SLOT, new AddInventorySlotController());

        controllers.put(Cmd.MOVE, new MoveController());
    }

    public Map<Byte, IController> getControllers()
    {
        return controllers;
    }

}
