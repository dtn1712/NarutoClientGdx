package network.impl;

import android.util.Log;

import com.google.common.primitives.Longs;

import java.nio.ByteBuffer;

import model.Cmd;
import network.Message;
import network.Session;

public class MessageEncoder {

    public static byte[] encode(Message m)
    {
        try
        {
            int sendByteCount = Session.getInstance().getSendByteCount();
            int finalDataSize = 0;
            byte[] encryptedData = null;
            byte cmd = m.getCommand();
            if (Session.getInstance().getKey() != null)
            {
                byte[] data = m.getData();
                int size;

                if (data != null)
                {
                    size = data.length;
                    encryptedData = Cmd.isRequireEncryptionCommand(cmd) ? MessageCipher.encrypt(Session.getInstance().getKey(), data) : data;
                    sendByteCount += 5 + data.length;
                }
                else
                {
                    size = 0;
                    sendByteCount += 5;
                }

                finalDataSize = size;
                if (encryptedData != null)
                {
                    finalDataSize = encryptedData.length;
                }

            }

            Session.getInstance().setSendByteCount(sendByteCount);

            finalDataSize = Cmd.isFastMessageCommand(cmd) ? finalDataSize + 8 : finalDataSize;

            byte[] finalData = new byte[5 + finalDataSize];
            finalData[0] = cmd;

            byte[] sizeBytes = ByteBuffer.allocate(4).putInt(finalDataSize).array();
            finalData[1] = sizeBytes[0];
            finalData[2] = sizeBytes[1];
            finalData[3] = sizeBytes[2];
            finalData[4] = sizeBytes[3];

            if (finalDataSize > 0 && encryptedData != null)
            {
                if (Cmd.isFastMessageCommand(cmd) && Session.getInstance().getSessionId() != -1)
                {
                    byte[] sessionId = Longs.toByteArray(Session.getInstance().getSessionId());
                    System.arraycopy(sessionId, 0, finalData, 5, 8);
                    System.arraycopy(encryptedData, 0, finalData, 13, finalDataSize - 8);
                }
                else
                {
                    System.arraycopy(encryptedData, 0, finalData, 5, finalDataSize);
                }
            }
            return finalData;
        }
        catch (Exception e)
        {
            Gdx.app.error(MessageEncoder.class.getName(), "Failed to encode", e);
        }

        return null;
    }
}
