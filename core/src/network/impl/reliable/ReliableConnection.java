package network.impl.reliable;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.Executors;

import Gui.Constants;
import model.Cmd;
import network.IConnection;
import network.IMessageExecutor;
import network.IMessageReader;
import network.IMessageWriter;
import network.Message;
import network.Session;
import screen.LoginScreen;
import utils.IOUtils;

public class ReliableConnection implements IConnection {

    private boolean connected;
    private long timeConnected;

    private DataInputStream reader;
    private DataOutputStream writer;

    private IMessageWriter messageWriter;
    private IMessageReader messageReader;

    private IMessageExecutor messageExecutor = new ReliableMessageExecutor();
    private Vector<Message> writerMessageQueue = new Vector<Message>();

    private Socket tcpClient;

    private int readMessageErrorCount = 0;

    public boolean connect(String host, int port)
    {
        long startConnectAt = System.currentTimeMillis();
        while (System.currentTimeMillis() - startConnectAt < Constants.CONNECT_TIMEOUT_SECONDS * 1000) {
            try {

                tcpClient = new Socket(host, port);
                tcpClient.setKeepAlive(true);
                tcpClient.setTcpNoDelay(true);

                reader = new DataInputStream(tcpClient.getInputStream());
                writer = new DataOutputStream(tcpClient.getOutputStream());

                messageReader = new ReliableMessageReader(reader);
                messageWriter = new ReliableMessageWriter(writer);

                connected = true;
                timeConnected = System.currentTimeMillis();
                break;
            } catch (Exception e) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                Gdx.app.error(ReliableConnection.class.getName(), "[connect] Exception", e);
            }
        }

        if (connected) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    while (connected) {
                        while (writerMessageQueue.size() > 0) {
                            try {
                                Message message = writerMessageQueue.elementAt(0);
                                writerMessageQueue.removeElementAt(0);
                                messageWriter.writeMessage(message);
                            } catch (Exception e) {
                                Gdx.app.error(ReliableConnection.class.getName(), "Failed to read message", e);
                            }
                        }
                        try {
                            Thread.sleep(5);
                        } catch (Exception e) {
                            Gdx.app.error(ReliableConnection.class.getName(), "Failed to sleep", e);
                        }
                    }
                }
            });

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    long startTime = System.currentTimeMillis();
                    while (connected && System.currentTimeMillis() - startTime < Constants.CONNECT_TIMEOUT_SECONDS * 1000
                            && (Session.getInstance().getSessionId() == -1 || Session.getInstance().getKey() == null)) {
                        IOUtils.writeMessage(Cmd.REQUEST_AUTH_KEY);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            Gdx.app.error(ReliableConnection.class.getName(), "Failed to sleep", e);
                        }
                    }

                    if (Session.getInstance().getSessionId() == -1 || Session.getInstance().getKey() == null) {
                        LoginScreen.getInstance().clear();
                        GameCanvas.getInstance().resetToLoginScreen(false);
                    }
                }
            });

            Executors.newSingleThreadExecutor().execute(new Runnable() {
               @Override
               public void run() {
                    try {
                        while (connected) {
                            Message message = messageReader.readMessage();
                            if (message != null) {
                                readMessageErrorCount = 0;
                                messageExecutor.execute(message);
                            } else {
                                readMessageErrorCount++;
                                if (readMessageErrorCount >= Constants.MAX_READ_ERROR_MESSAGE_RETRY) {
                                    readMessageErrorCount = 0;
                                    break;
                                }
                            }
                            try {
                                Thread.sleep(5);
                            } catch (Exception e) {
                                Gdx.app.error(ReliableConnection.class.getName(), "[Thread.sleep] Exception", e);
                            }
                        }
                    } catch (Exception e) {
                        Gdx.app.error(ReliableConnection.class.getName(), "Failed to read message", e);
                    }


                    if (!connected) return;

                    Gdx.app.error(ReliableConnection.class.getName(), "error read message!");
                    if (messageExecutor != null) {
                        if (System.currentTimeMillis() - timeConnected > 500) {
                            messageExecutor.onDisconnected();
                        } else {
                            messageExecutor.onConnectionFail();
                        }
                    }
                    close();
                }
            });


            return true;
        } else {
            return false;
        }
    }

    public void close()
    {
        try
        {
            try {
                if (reader != null) reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (writer != null) writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (tcpClient != null) tcpClient.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            messageReader = null;
            messageWriter = null;
            connected = false;
            timeConnected = -1;
            readMessageErrorCount = 0;
            writerMessageQueue.clear();
        }
        catch (Exception e)
        {
            Gdx.app.error(ReliableConnection.class.getName(), "[close] Exception", e);
        }
    }

    public void sendMessage(Message message)
    {
        writerMessageQueue.add(message);
    }

    public boolean isConnected()
    {
        return connected;
    }

    @Override
    public boolean isServerAvailable(String host, int port) {
        try  {
            Socket ignored = new Socket(host, port);
            ignored.close();
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

}
