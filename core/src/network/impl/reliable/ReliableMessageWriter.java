package network.impl.reliable;

import android.util.Log;

import java.io.DataOutputStream;

import network.IMessageWriter;
import network.Message;
import network.impl.MessageEncoder;

public class ReliableMessageWriter implements IMessageWriter {

    private DataOutputStream writer;

    public ReliableMessageWriter(DataOutputStream writer)
    {
        this.writer = writer;
    }

    public void writeMessage(Message m)
    {
        try
        {
            byte[] finalData = MessageEncoder.encode(m);
            if (finalData != null) {
                writer.write(finalData);
                writer.flush();
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(ReliableMessageWriter.class.getName(), "[ReliableMessageWriter] Exception", e);
        }
    }

}
