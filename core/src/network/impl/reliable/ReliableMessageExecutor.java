package network.impl.reliable;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.util.Map;

import network.IMessageExecutor;
import network.Message;
import network.impl.MessageController;
import real.controller.IController;

public class ReliableMessageExecutor implements IMessageExecutor
{

    public void onConnectionFail()
    {
        GameCanvas.getInstance().currentScreen.isConnect = false;
        GameCanvas.getInstance().currentScreen.connectCallBack = true;
    }

    public void onDisconnected()
    {
        GameCanvas.getInstance().currentScreen.isConnect = false;
        GameCanvas.getInstance().currentScreen.isCloseConnect = true;
        GameCanvas.getInstance().currentScreen.connectCallBack = true;
        GameCanvas.getInstance().resetToLoginScreen(false);
    }

    public void execute(Message msg)
    {
        try
        {
            Map<Byte, IController> controllers = MessageController.getInstance().getControllers();
            if (controllers.containsKey(msg.getCommand()))
            {
                controllers.get(msg.getCommand()).processMessage(msg);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(ReliableMessageExecutor.class.getName(), "Failed to execute message", e);
        }
    }
}