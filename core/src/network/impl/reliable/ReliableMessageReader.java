package network.impl.reliable;

import android.util.Log;

import java.io.DataInputStream;

import model.Cmd;
import network.IMessageReader;
import network.Message;
import network.Session;
import network.impl.MessageCipher;

public class ReliableMessageReader implements IMessageReader
{

    private DataInputStream reader;

    public ReliableMessageReader(DataInputStream reader)
    {
        this.reader = reader;
    }

    public Message readMessage()
    {
        int receiveByteCount = Session.getInstance().getReceiveByteCount();
        try
        {
            // read message command
            byte cmd = reader.readByte();
            byte[] b = {reader.readByte(), reader.readByte(), reader.readByte(), reader.readByte()};

            int size = ((b[0] & 0xff) << 24) | ((b[1] & 0xff) << 16) |
                       ((b[2] & 0xff) << 8) | (b[3] & 0xff);


            byte[] data = new byte[size];
            reader.readFully(data);

            receiveByteCount += (5 + size);
            Session.getInstance().setReceiveByteCount(receiveByteCount);

            if (Cmd.isRequireEncryptionCommand(cmd))
            {
                // Decrypt the message
                byte[] decryptData = MessageCipher.decrypt(Session.getInstance().getKey(), data);
                return new Message(cmd, decryptData);
            }
            return new Message(cmd, data);
        }
        catch (Exception e)
        {
            Gdx.app.error(ReliableMessageReader.class.getName(), "[readMessage] Exception:", e);
        }
        return null;
    }
}
