package network.impl.fast;

import android.util.Log;

import java.util.Map;

import network.IMessageExecutor;
import network.Message;
import network.impl.MessageController;
import network.impl.reliable.ReliableMessageExecutor;
import real.controller.IController;

public class FastMessageExecutor implements IMessageExecutor{

    public void execute(Message msg)
    {
        try
        {
            Map<Byte, IController> controllers = MessageController.getInstance().getControllers();
            if (controllers.containsKey(msg.getCommand()))
            {
                controllers.get(msg.getCommand()).processMessage(msg);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(ReliableMessageExecutor.class.getName(), "Failed to execute message", e);
        }
    }

    public void onDisconnected()
    {
        throw new UnsupportedOperationException();
    }

    public void onConnectionFail()
    {
        throw new UnsupportedOperationException();
    }
}
