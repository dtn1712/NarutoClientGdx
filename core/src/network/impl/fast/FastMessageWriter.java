package network.impl.fast;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;

import network.IMessageWriter;
import network.Message;
import network.impl.MessageEncoder;

public class FastMessageWriter implements IMessageWriter
{
    private DatagramSocket udpClient;
    private SocketAddress endPoint;

    public FastMessageWriter(DatagramSocket udpClient, SocketAddress endPoint)
    {
        this.udpClient = udpClient;
        this.endPoint = endPoint;
    }

    public void writeMessage(Message m)
    {
        try
        {
            byte[] finalData = MessageEncoder.encode(m);
            if (finalData != null) {
                DatagramPacket datagramPacket = new DatagramPacket(finalData, finalData.length, endPoint);
                udpClient.send(datagramPacket);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(FastMessageWriter.class.getName(), "[writeMessage] Exception", e);
        }
    }
}
