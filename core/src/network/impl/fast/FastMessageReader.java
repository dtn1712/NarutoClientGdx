package network.impl.fast;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import network.IMessageReader;
import network.Message;

public class FastMessageReader implements IMessageReader
{

    private DatagramSocket udpClient;

    public FastMessageReader(DatagramSocket udpClient)
    {
        this.udpClient = udpClient;
    }

    public Message readMessage()
    {
        byte[] receiveData = new byte[65535];
        Message message = null;
        try
        {
            DatagramPacket datagramPacket = new DatagramPacket(receiveData, receiveData.length);
            udpClient.receive(datagramPacket);

            // read message command
            byte cmd = receiveData[0];
            int size = ((receiveData[1] & 0xff) << 24) | ((receiveData[2] & 0xff) << 16) |
                       ((receiveData[3] & 0xff) << 8) | (receiveData[4] & 0xff);

//            if (Cmd.isRequireEncryptionCommand(cmd))
//            {
//                // Decrypt the message
//                byte[] dataTemp = new byte[size];
//                System.arraycopy(receiveData, 5, dataTemp, 0, size);
//                byte[] data = MessageCipher.decrypt(Session.getInstance().getKey(),dataTemp);
//                message = new Message(cmd, data);
//            }
//            else
//            {
//                byte[] data = new byte[size];
//                System.arraycopy(receiveData, 5, data, 0, size);
//                message = new Message(cmd, data);
//            }

            byte[] data = new byte[size];
            System.arraycopy(receiveData, 5, data, 0, size);
            message = new Message(cmd, data);
        }
        catch (Exception e)
        {
            Gdx.app.error(FastMessageReader.class.getName(), "[readMessage] Exception", e);
        }
        return message;
    }
}
