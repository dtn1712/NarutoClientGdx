package network.impl.fast;

import android.util.Log;

import com.badlogic.gdx.Gdx;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Vector;
import java.util.concurrent.Executors;

import Gui.Constants;
import model.Cmd;
import network.IConnection;
import network.IMessageExecutor;
import network.IMessageReader;
import network.IMessageWriter;
import network.Message;
import network.Session;
import network.impl.reliable.ReliableConnection;
import utils.IOUtils;

public class FastConnection implements IConnection
{

    private DatagramSocket udpClient;
    private boolean connected;
    private long timeConnected;
    private IMessageWriter messageWriter;
    private IMessageReader messageReader;
    private Vector<Message> writerMessageQueue = new Vector<Message>();
    private IMessageExecutor messageExecutor = new FastMessageExecutor();

    public boolean connect(String host, int port)
    {
        long startConnectAt = System.currentTimeMillis();
        while (System.currentTimeMillis() - startConnectAt < Constants.CONNECT_TIMEOUT_SECONDS * 1000) {
            try
            {
                SocketAddress remoteEndPoint = new InetSocketAddress(host, port);

                udpClient = new DatagramSocket();

                messageWriter = new FastMessageWriter(udpClient, remoteEndPoint);
                messageReader = new FastMessageReader(udpClient);

                connected = true;
                timeConnected = System.currentTimeMillis();
                break;
            } catch (Exception e) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                Gdx.app.error(ReliableConnection.class.getName(), "[connect] Exception", e);
            }
        }

        if (connected) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    while (connected) {
                        while (writerMessageQueue.size() > 0) {
                            Message message = writerMessageQueue.elementAt(0);
                            writerMessageQueue.removeElementAt(0);
                            messageWriter.writeMessage(message);
                        }
                        try {
                            Thread.sleep(5);
                        } catch (Exception e) {
                            Gdx.app.error(FastConnection.class.getName(), "[Thread.sleep] Exception", e);
                        }
                    }
                }
            });

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    while (connected) {
                        if (Session.getInstance().getSessionId() != -1) {
                            IOUtils.writeMessage(Cmd.HANDSHAKE);
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            Gdx.app.error(FastConnection.class.getName(), "[Thread.sleep] Exception", e);
                        }
                    }
                }
            });

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (connected) {
                            Message message = messageReader.readMessage();
                            if (message != null) {
                                messageExecutor.execute(message);
                            }
                            try {
                                Thread.sleep(5);
                            } catch (Exception e) {
                                Gdx.app.error(FastConnection.class.getName(), "[Thread.sleep] Exception", e);
                            }
                        }
                    } catch (Exception e) {
                        Gdx.app.error(FastConnection.class.getName(), "[readMessage] Exception", e);
                    }

                    if (!connected) return;

                    Gdx.app.error(FastConnection.class.getName(), "Error read message!");
                    if (messageExecutor != null) {
                        if (System.currentTimeMillis() - timeConnected > 500)
                            messageExecutor.onDisconnected();
                        else
                            messageExecutor.onConnectionFail();
                    }
                    close();
                }
            });

            return true;
        } else {
            return false;
        }
    }

    public void close()
    {
        try {

            try {
                if (udpClient != null) {
                    udpClient.disconnect();
                    udpClient.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            writerMessageQueue.clear();
            connected = false;
            timeConnected = -1;
            messageWriter = null;
            messageReader = null;
        } catch (Exception e) {
            Gdx.app.error(ReliableConnection.class.getName(), "[close] Exception", e);
        }
    }

    public void sendMessage(Message message)
    {
        writerMessageQueue.add(message);
    }

    public boolean isConnected()
    {
        return connected;
    }

    @Override
    public boolean isServerAvailable(String host, int port) {
        try  {
            DatagramSocket ignored = new DatagramSocket();
            ignored.connect(new InetSocketAddress(host, port));
            ignored.close();
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }


}
