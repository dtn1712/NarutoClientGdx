package network;
import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lombok.Getter;
import lombok.Setter;
import model.Cmd;

@Getter
@Setter
public class Session{

	private static final Session instance = new Session();


	public static Session getInstance()
	{
		return instance;
	}

	private int sendByteCount;
	private int receiveByteCount;

	private long sessionId = -1;
	private byte[] key;

	public void sendMessage(Message message)
	{
		try
		{
			IConnection fastConnection = GameCanvas.getInstance().fastConnection;
			IConnection reliableConnection = GameCanvas.getInstance().reliableConnection;
			if (Cmd.isFastMessageCommand(message.command))
			{
				if (fastConnection != null && fastConnection.isConnected())
				{
					fastConnection.sendMessage(message);
				}
			}
			else
			{
				if (reliableConnection != null && reliableConnection.isConnected())
				{
					reliableConnection.sendMessage(message);
				}
			}
		}
		catch (Exception e)
		{
			Gdx.app.error(Session.class.getName(), "Failed to send message", e);
		}

	}

	public void disconnect()
	{
		IConnection fastConnection = GameCanvas.getInstance().fastConnection;
		IConnection reliableConnection = GameCanvas.getInstance().reliableConnection;
		if (reliableConnection != null)  {
			reliableConnection.close();
		}
		if (fastConnection != null) {
			fastConnection.close();
		}
		key = null;
		sessionId = -1;
	}

	public String getByteCount()
	{
		int kb = receiveByteCount + sendByteCount;
		return kb / 1024 + "." + kb % 1024 / 102 + "Kb";
	}


}
