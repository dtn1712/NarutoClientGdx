package network;

public interface IMessageExecutor {

	void execute(Message message);

	void onDisconnected();

	void onConnectionFail();

}
