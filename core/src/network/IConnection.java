package network;

public interface IConnection {

    boolean connect(String host, int port);

    void close();

    void sendMessage(Message message);

    boolean isConnected();

    boolean isServerAvailable(String host, int port);

}
