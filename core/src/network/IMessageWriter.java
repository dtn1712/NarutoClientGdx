package network;

public interface IMessageWriter {

    void writeMessage(Message message);
}
