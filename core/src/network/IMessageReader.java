package network;

public interface IMessageReader {

    Message readMessage();
}
