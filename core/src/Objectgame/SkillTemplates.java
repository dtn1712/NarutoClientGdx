package Objectgame;

import lib.Hashtable;

public class SkillTemplates {
	public static Hashtable hSkilltemplate = new Hashtable();


	public static void add(SkillTemplate it) {
		hSkilltemplate.put(new Short(it.id), it);
	}
	public static ItemTemplate get(short id)
	{
		return (ItemTemplate) hSkilltemplate.get(new Short(id));
	}
	
	public static short getIcon(short itemTemplateID) {
		return get(itemTemplateID).iconID;
	}

}
