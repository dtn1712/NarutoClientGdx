package Objectgame;

import java.util.Hashtable;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import model.Command;
import model.IActionListener;
import model.Part;
import model.SmallImage;


import real.mFont;
import screen.old.GameScr;
import lib.Cout;
import lib.mVector;

public class Npc extends Char implements IActionListener {

	public static final byte BINH_KHI = 0;
	public static final byte PHONG_CU = 1;
	public static final byte TRANG_SUC = 2;
	public static final byte DUOC_PHAM = 3;
	public static final byte TAP_HOA = 4;
	public static final byte THU_KHO = 5;
	public static final byte DA_LUYEN = 6;
	public static final byte XA_PHU_LANG = 7;
	public static final byte XA_PHU_TRUONG = 8;
	public static final byte CHU_NHIEM_HOA = 9;
	public static final byte CHU_NHIEM_THUY = 10;
	public static final byte CHU_NHIEM_GIO = 11;
	public static final byte TRUONG_LANG_TONE = 12;
	public static final byte KHU_VUC = 13;
	public static final byte GIAO_THU_1 = 14;
	public static final byte GIAO_THU_2 = 15;
	public static final byte GIAO_THU_3 = 16;
	public static final byte CHAU_BA_REI = 17;
	public static final byte TRUONG_LANG_CHAI = 18;
	public static final byte TRUONG_LANG_KOJIN = 19;
	public static final byte TRUONG_LANG_CHAKUMI = 20;
	public static final byte TRUONG_LANG_ECHIGO = 21;
	public static final byte TRUONG_LANG_SANZU = 22;
	public static final byte TRUONG_LANG_OSHIN = 23;
	public static final byte TIEN_TRANG = 24;
	public static final byte DAU_TRUONG = 25;
	public static final byte THUONG_NHAN = 26;
	public static final byte TRU_CO_QUAN = 27;
	public static final byte KY_TRAN = 28;

	public NpcTemplate template;
	public int npcId;
	public byte typeNV = -1;
	public boolean isFocus = true;//dg có nv khong
	public static Hashtable arrNpcTemplate = new Hashtable();
	public int sys;
	public short avatar;

//	public Npc(int npcId, int status, int cx, int cy, int templateId) {
//		this.npcId = npcId;
//
//		this.cx = cx;
//		this.cy = cy;
//		this.statusMe = status;
//		template = Npc.arrNpcTemplate[templateId];
//	}
	
	public Npc(short cx, short cy, short templateId, short avatar) {
		System.out.println("NPC: "+cx+" :: "+cy+" :: "+templateId+" ::: "+avatar);
		this.npcId = npcId;
		this.avatar = avatar;
		this.cx = cx;
		this.cy = cy-20;
		template = (NpcTemplate)Npc.arrNpcTemplate.get(templateId+"");
		Cout.println(getClass(), templateId+ " npc  "+template);
	}

	public static void clearEffTask() {
		for (int i = 0; i < GameScr.vNpc.size(); i++) {
			Npc npc = (Npc) GameScr.vNpc.elementAt(i);
			npc.effTask = null;
			npc.indexEffTask = -1;
		}
	}

	

	public void paintHead(MGraphics g) {

		Part ph = GameScr.parts[template.headId];
		if (cdir == 1)
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, GameCanvas.w - 31 - g.getTranslateX(), 2 - g.getTranslateY(), 0, 0);
		else
			SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, GameCanvas.w - 31 - g.getTranslateX(), 2 - g.getTranslateY(), 2, 24);
	}

	public void paint(MGraphics g) {
//		cy-=20;
		if(!isPaint())return;
		if (statusMe == A_HIDE)
			return;
		if (cTypePk != PK_NORMAL) {
			super.paint(g);
			return;
		}
		if (template == null)
			return;
		
//		if (template.npcTemplateId == 13) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1060, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			mFont.tahoma_7_white.drawString(graphic, TileMap.zoneID + "", cx, cy - 10 - 2 - mFont.tahoma_7.getHeight(), mFont.CENTER);
//		}else if (template.npcTemplateId == 31) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1291, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//		}
//		else if (template.npcTemplateId == 27) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1224, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//		} else {
			Part ph = GameScr.parts[template.headId], pl = GameScr.parts[template.legId], pb = GameScr.parts[template.bodyId], pw = null;
			if(template.npcTemplateId == 34)
				 pw = GameScr.parts[167];
			if (cdir == 1) {
				SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
				SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
				SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
				if (pw != null)
					SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][3][1] + pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 0, 0);

			} else {
				SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
				SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
				SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
				if(pw != null)
					SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][3][1] - pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 2, 24);
			}
			if(typeNV>-1){
				g.drawRegion(GameScr.imgQuest, 0, (typeNV*2+(GameCanvas.gameTick/10)%2)*GameScr.imgQuest.getHeight()/4, GameScr.imgQuest.getWidth(), GameScr.imgQuest.getHeight()/4, 0,  cx, cy - ch - 3 - mFont.tahoma_7.getHeight()-10, MGraphics.VCENTER| MGraphics.HCENTER, false);
			}
			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
				mFont.tahoma_7_yellow.drawString(g, template.name, cx, cy - ch - mFont.tahoma_7.getHeight() - 7, mFont.CENTER, mFont.tahoma_7_grey);
				SmallImage.drawSmallImage(g, 988, cx, cy - ch - 2, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
			} else {
				mFont.tahoma_7_yellow.drawString(g, template.name, cx, cy - ch - 3 - mFont.tahoma_7.getHeight(), mFont.CENTER, mFont.tahoma_7_grey);
			}
//		}

//			cy+=20;
	}
	public void paint(MGraphics g, int cx, int cy) {
//		cy-=20;
		if(!isPaint())return;
		if (statusMe == A_HIDE)
			return;
		if (cTypePk != PK_NORMAL) {
			super.paint(g);
			return;
		}
		if (template == null)
			return;
		
//		if (template.npcTemplateId == 13) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1060, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			mFont.tahoma_7_white.drawString(graphic, TileMap.zoneID + "", cx, cy - 10 - 2 - mFont.tahoma_7.getHeight(), mFont.CENTER);
//		}else if (template.npcTemplateId == 31) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1291, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//		}
//		else if (template.npcTemplateId == 27) {
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.equals(this)) {
//				SmallImage.drawSmallImage(graphic, 988, cx, cy - ch - 1, 0, Graphics.BOTTOM | Graphics.HCENTER);
//			}
//			SmallImage.drawSmallImage(graphic, 1224, cx, cy, 0, Graphics.BOTTOM | Graphics.HCENTER);
//		} else {
			Part ph = GameScr.parts[template.headId], pl = GameScr.parts[template.legId], pb = GameScr.parts[template.bodyId], pw = null;
			if(template.npcTemplateId == 34)
				 pw = GameScr.parts[167];
			if (cdir == 1) {
				SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
				SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
				SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
				if (pw != null)
					SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx + CharInfo[cf][3][1] + pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 0, 0);

			} else {
				SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id, cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx, cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
				SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id, cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx, cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
				SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx, cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
				if(pw != null)
					SmallImage.drawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id, cx - CharInfo[cf][3][1] - pw.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 2, 24);
			}
//		}

//			cy+=20;
	}
	public void NhiemVu(boolean ischeckNPC) { //true ko hien popup chi de check paint icon NV tren dau NPC
		mVector menu = new mVector();
		int idnpc = this.npcId;
		Cout.println(getClass(), Quest.listUnReceiveQuest.size()+ "npc nhiemvu");
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
			final Quest quest = (Quest) Quest.listUnReceiveQuest
					.elementAt(i);
			if (quest.idNPC_From == idnpc) {
				Command cmd = new Command(quest.name,this, 1,new MenuObject(0,i,npcId));
				//cmd.setFraCaption(fraQuest, 1, 1);
				menu.addElement(cmd);
			}
			else {
				Cout.println(getClass(),quest.idNPC_From+ "  idnpc "+idnpc);
			}
		}
		for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
			final Quest quest = (Quest)Quest.vecQuestDoing_Main
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 3,new MenuObject(2,i,npcId));
//				cmd.setFraCaption(fraQuest, 1, 2);
				menu.addElement(cmd);
			}
		}
		for (int i = 0; i <Quest.vecQuestDoing_Sub.size(); i++) {
			final Quest quest = (Quest)Quest.vecQuestDoing_Sub
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 4,new MenuObject(3,i,npcId));
//				cmd.setFraCaption(fraQuest, 1, 2);
				menu.addElement(cmd);
			}
		}
		for (int i = 0; i <Quest.vecQuestFinish.size(); i++) {
			final Quest quest = (Quest)Quest.vecQuestFinish
					.elementAt(i);
			if (quest.idNPC_To == idnpc) {
				Command cmd = new Command(quest.name,this, 2,new MenuObject(1,i,npcId));
				//cmd.setFraCaption(fraQuest, 1, 3);
				menu.addElement(cmd);
			}
		}

		Cout.println(getClass(), "npc nhiemvu menu "+menu.size());
		if (menu.size() == 0||ischeckNPC) {
//			GameCanvas.clearKeyHold();
//			GameCanvas.clearKeyPressed();
			// cmdTroChuyen.perform();
//            if(!HuongDan.isHuongDan)
//			GameCanvas.menu.doCloseMenu();
		} else {
			//GameCanvas.menu.doCloseMenu();
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
			if(!ischeckNPC&&menu.size()!=0){
				GameCanvas.menu.startAt(menu,0);
//				if(menu.size()==1){
//					Command cmdnv = (Command)menu.elementAt(0);
//					cmdnv.performAction();
//				}
//				GameCanvas.menu.startAt(menu, 0);
			}
		}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub

		Cout.println(getClass(), "npc perform "+idAction);
		// TODO Auto-generated method stub
		int subIndex = ((MenuObject)p).idOption;
		switch (idAction) {
		case 1:// co the nhan
			if (subIndex >= 0 && subIndex <= Quest.listUnReceiveQuest.size()) {
				Quest quest = (Quest) Quest.listUnReceiveQuest
						.elementAt(subIndex);
				quest.beginQuest();
			}
			break;
		case 2:// hoan thanh
			if (subIndex >= 0 && subIndex <= Quest.vecQuestFinish.size()) {
				Quest quest = (Quest) Quest.vecQuestFinish
						.elementAt(subIndex);
				quest.beginQuest();
//				Service.gI().ReQuest((byte)1, (short)quest.ID, (byte)(quest.isMain==true?0:1));
			}
			break;
		case 3:// dang lam main
			if (subIndex >= 0
					&& subIndex <= Quest.vecQuestDoing_Main.size()) {
				Quest quest = (Quest) Quest.vecQuestDoing_Main
						.elementAt(subIndex);

				//quest.beginQuest();
				String[] help = new String[1];
				help[0] = quest.strDetailHelp;
				quest.mstrTalk= help;
				quest.show_Info_Quest_Doing();
			}
			break;
		case 4:// // dang lam sub
			if (subIndex >= 0 && subIndex <= Quest.vecQuestDoing_Sub.size()) {
				Quest quest = (Quest) Quest.vecQuestDoing_Sub
						.elementAt(subIndex);
				quest.show_Info_Quest_Doing();
			}
			break;
//		case 4:
//			GlobalService.gI().getlist_from_npc((byte) ID);
//			break;
		case 5:
			NhiemVu(false);
			break;
		case 6:// gui thong tin confirm menu
			MenuObject menu=(MenuObject)p;
//			Service.gI().doSelectMenu(menu.idActor,menu.idMenu,menu.idOption);
			break;
		case 7:// lay menu ve

			break;
		}
		
	
	}
	public void hide() {
		statusMe = Char.A_HIDE;
		chatPopup = null;
	}
}