package Objectgame;

import lib.Hashtable;

public class Skills {
	public static Hashtable skills = new Hashtable();

	public static void add(Skill skill) {
		skills.put(new Short((short) skill.skillId), skill);
	}
	public static Skill get(short skillId)
	{
		return (Skill) skills.get(new Short(skillId));
	}
}
