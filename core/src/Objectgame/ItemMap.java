package Objectgame;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import model.SmallImage;
import screen.old.GameScr;

public class ItemMap {

	public int x, y, xEnd, yEnd, f, vx, vy;
	public int itemMapID;
	public int IdCharMove;
	public ItemTemplate template;
	public byte status,type;
	
	public ItemMap(short itemMapID, short itemTemplateID,
			int x, int y, int xEnd, int yEnd) {
		this.itemMapID = itemMapID;
		this.template = ItemTemplates.get(itemTemplateID);
		this.x = xEnd;
		this.y = y;
		this.xEnd = xEnd;
		this.yEnd = yEnd;

		vx = (xEnd - x) >> 2;
		vy = 5;
	}
	public ItemMap(short itemID, byte type,String name, short iconID){
		this.itemMapID = itemID;
		
	}
	
	public ItemMap(short itemMapID, short itemTemplateID, int x, int y) {
		this.itemMapID = itemMapID;
		this.template = ItemTemplates.get(itemTemplateID);
		this.x = this.xEnd = x;
		this.y = this.yEnd = y;
		this.status = 1;
	}
	
	public void setPoint(int xEnd, int yEnd){
		this.xEnd = xEnd;
		this.yEnd = yEnd;
		vx = (xEnd - x) >> 2;
		vy = (yEnd - y) >> 2;
		status = 2;
	}

	public void update() {
		if(status == 2 && x == xEnd && y == yEnd){
			GameScr.vItemMap.removeElement(this);
			if(Char.myChar().itemFocus != null && Char.myChar().itemFocus.equals(this))
				Char.myChar().itemFocus = null;
			return;
		}
		if(status > 0){
			if(vx == 0)
				x = xEnd;
			if(vy == 0)
				y = yEnd;
			if (x != xEnd) {
				x += vx;
				if((vx > 0 && x > xEnd) || (vx < 0 && x < xEnd))
					x = xEnd;
			}
			if (y != yEnd) {
				y += vy;
				if((vy > 0 && y > yEnd) || (vy < 0 && y < yEnd))
					y = yEnd;
			}
		}
		else{
			status -= 4;
			if(status < -12){
				y -= 12;
				status = 1;
			}
		}
	}

	public void paint(MGraphics g) {
		try{
			//if(status <= 0)
				//SmallImage.drawSmallImage(graphic, template.iconID, x, y + status, 0, MGraphics.BOTTOM|MGraphics.HCENTER);
			//else
			g.translate(0, -GameCanvas.transY);
			SmallImage.drawSmallImage(g, template.iconID, x, y, 0, MGraphics.BOTTOM| MGraphics.HCENTER);
			if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.equals(this) && status != 2) {
				SmallImage.drawSmallImage(g, 988, x, y - 20, 0, MGraphics.VCENTER| MGraphics.HCENTER);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
