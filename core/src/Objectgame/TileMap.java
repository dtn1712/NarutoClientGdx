package Objectgame;



import lib.Bitmap;
import lib.Cout;
import lib.Hashtable;


import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Vector;

import com.thdgaming.naruto.GameCanvas;
import com.team.njonline.GameMidlet;
import com.thdgaming.naruto.MGraphics;


import screen.old.GameScr;



//import model.Party;


public class TileMap {
	public static final int T_EMPTY = 0;
	public static final int T_TOP = 2;
	public static final int T_LEFT = 2 << 1;
	public static final int T_RIGHT = 2 << 2;
	public static final int T_TREE = 2 << 3;
	public static final int T_WATERFALL = 2 << 4;
	public static final int T_WATERFLOW = 2 << 5;
	public static final int T_TOPFALL = 2 << 6;
	public static final int T_OUTSIDE = 2 << 7;
	public static final int T_DOWN1PIXEL = 2 << 8;
	public static final int T_BRIDGE = 2 << 9;
	public static final int T_UNDERWATER = 2 << 10;
	public static final int T_SOLIDGROUND = 2 << 11;
	public static final int T_BOTTOM = 2 << 12;
	public static final int T_DIE = 2 << 13;
	public static final int T_HEBI = 2 << 14;
	public static final int T_BANG = 2 << 15;
	public static final int T_JUM8 = 2 << 16;
	public static final int T_NT0 = 2 << 17;
	public static final int T_NT1 = 2 << 18;
	public static final int T_CENTER = 1;

	public static int tmw, tmh, pxw, pxh, tileID, lastTileID = -1;
	public static int[] maps;
	public static int[] types;
	// Tileo
	public static Bitmap imgMaptile;

	public static byte size = 24;
	public static int[] iX;

	public static String mapName = "";
	public static short mapID;
	public static byte lastBgID = -1, zoneID, bgID;
	public static Vector vGo = new Vector();
	public static Vector vItemBg = new Vector();
	public static Vector vCurrItem = new Vector();
	public static Hashtable listNameAllMap = new Hashtable();
	public static Bitmap[] bgItem = new Bitmap[8];


	public static BgItem getBIById(int id) {
		for (int i = 0; i < vItemBg.size(); i++) {
			BgItem bi = (BgItem) vItemBg.elementAt(i);
			if (bi.id == id)
				return bi;
		}
		return null;
	}


	public static void setTile(int index, int mapsArr[], int type) {
		for (int i = 0; i < mapsArr.length; i++) {
			if (maps[index] == mapsArr[i]) {
//				if(maps[index] != -1){ // sửa để test
//					types[index] |= T_TOP;
//				}else
					types[index] |= type;
//				types[index] |= 2;

//				types[index] |= T_TOP;
				return;
			}
		}
	}
	public static int[][] tileType;
	public static int[][][] tileIndex;

	public static void loadMap(int tileId) {
//		tileId = 2;
		Cout.println(tileId+" size "+size+"leng "+tileType.length+"TILEMAP WIDTH ::: HIEGHT ----> "+tmw+" :::: "+tmh);
		pxh = tmh * size;
		pxw = tmw * size;
		
		
		int tile = tileId - 1;
//		int tile = tileId;
		
		try {
			for (int i = 0; i <tmw * tmh; i++) {
				for(int a=0;a<tileType[tile].length;a++){
					setTile(i, tileIndex[tile][a], tileType[tile][a]);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	public static void loadMapFromResource(int mapID) throws Exception {
		InputStream iss = null;
		DataInputStream is = null;
		Cout.println("TileMap loadma "+("map/" + TileMap.mapID));
		iss =  GameMidlet.asset.open("map/" + TileMap.mapID);
		is = new DataInputStream(iss);
		TileMap.tmw = (char) is.read();
		TileMap.tmh = (char) is.read();

		Cout.println(TileMap.tmw+ "  TileMap loadma "+TileMap.tmh);
		TileMap.maps = new int[is.available()];
		for (int i = 0; i < TileMap.tmw * TileMap.tmh; i++){
			TileMap.maps[i] = (char) is.read();
		}
		TileMap.types = new int[TileMap.maps.length];
	}

	public static final int tileAt(int x, int y) {
		try {
			return maps[y * tmw + x];
		} catch (Exception ex) {
			return 1000;
		}
	}


	public static final int tileTypeAtPixel(int px, int py) {
		try {

			return types[(py / size) * tmw + (px / size)];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static final boolean tileTypeAt(int px, int py, int t) {
		try {

			return (types[(py / size) * tmw + (px / size)] & t) == t;
		} catch (Exception ex) {
			return false;
		}
	}

	public static final void setTileTypeAtPixel(int px, int py, int t) {

		types[(py / size) * tmw + (px / size)] |= t;
	}

	public static final void killTileTypeAt(int px, int py, int t) {
		types[(py / size) * tmw + (px / size)] &= ~t;
	}

	public static final int tileYofPixel(int py) {
		return (py / size) * size;
	}

	public static final int tileXofPixel(int px) {
		return (px / size) * size;
	}

	public static void loadMapfile(int MapID){
		try{
			TileMap.vCurrItem.removeAllElements();
			TileMap.vItemBg.removeAllElements();
			loadMapFromResource(MapID);

			
						
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void loadimgTile(int TileID){
		TileID = TileID - 1;
		imgMaptile = null;
		System.gc();
		imgMaptile = GameCanvas.loadImage("/tile"+TileID+".png");
		
		
	}
	
	public static void paintMap(MGraphics g){
		
		GameScr.gI().paintBgItem(g, 1);
		GameScr.gssw = GameCanvas.w / TileMap.size + 2;
		GameScr.gssh = GameCanvas.h / TileMap.size + 2;
		if ( GameCanvas.w  % 24 != 0)
			GameScr.gssw += 1;
		
		GameScr.gssx = GameScr.cmx / TileMap.size - 1;
		if (GameScr.gssx < 0)
			GameScr.gssx = 0;
		GameScr.gssy = GameScr.cmy  / TileMap.size;
		GameScr.gssxe = GameScr.gssx + GameScr.gssw;
		GameScr.gssye = GameScr.gssy + GameScr.gssh;
		if (GameScr.gssy < 0)
			GameScr.gssy = 0;
		if (GameScr.gssye > TileMap.tmh - 1)
			GameScr.gssye = TileMap.tmh - 1;
		
//		loadMap(1);
//		positionY=GameScr.gssy*size;
		for (int a = GameScr.gssx; a < GameScr.gssxe; a++) {
			for (int b = GameScr.gssy; b < GameScr.gssye; b++){
				
				try {
					
					int t = maps[b * tmw + a] - 1;
					
					if (t != -1){
						g.drawRegion(imgMaptile, 0, t * size, size, size, 0, a * size, (b * size), 0);
					}
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
			}
		}
	}
	
}

