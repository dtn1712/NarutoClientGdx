package Objectgame;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


import model.SmallImage;
import model.Sprite;
import model.StaticObj;

public class Skill {

	public static final byte ATT_STAND = 0;
	public static final byte ATT_FLY = 1;

	public static final byte SKILL_AUTO_USE = 0;
	public static final byte SKILL_CLICK_USE_ATTACK = 1;
	public static final byte SKILL_CLICK_USE_BUFF = 2;
	public static final byte SKILL_CLICK_NPC		= 3;
	public static final byte SKILL_CLICK_LIVE		= 4;

	public SkillTemplate template;
	public short skillId;
	public int point;
	public int level;
	public int coolDown;
	public long lastTimeUseThisSkill;
	public int dx;
	public int dy;
	public int maxFight;
	public int manaUse;
	public SkillOption[] options;
	public boolean paintCanNotUseSkill = false;

	public String strTimeReplay() {
		if (coolDown % 1000 == 0)
			return coolDown / 1000 + "";
		int time = coolDown % 1000;
		
		return (coolDown / 1000) + "." + (time % 100 == 0 ? time / 100 : time / 10);
	}

	public void paint(int x, int y, MGraphics g) {
		
		SmallImage.drawSmallImage(g, template.iconId, x, y, Sprite.TRANS_NONE, StaticObj.VCENTER_HCENTER);
		long now = System.currentTimeMillis();
		long d = now - lastTimeUseThisSkill;
		
		if (d < coolDown) {
			g.setColor(0x333333);
			if (paintCanNotUseSkill && GameCanvas.gameTick % 6 > 2)
				g.setColor(0x444444);
			int pxCooldown = (int) (d * 18 / coolDown);
			g.fillRect(x - 9, y - 9 + pxCooldown, 18, 18 - pxCooldown);
		} else
			// Làm ẩu, nhưng k sao
			paintCanNotUseSkill = false;
	}
	
	public boolean isCooldown() {
		long now = System.currentTimeMillis();
		long d = now - lastTimeUseThisSkill;
		if (d < coolDown)
			return true;
		else
			return false;
	}
}
