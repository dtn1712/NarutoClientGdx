package Objectgame;

public class Friend {
	public static final byte INVITE_ADD_FRIEND= 0;
	public static final byte ACCEPT_ADD_FRIEND = 1;
	public static final byte UNFRIEND = 2;
	public static final byte REQUEST_FRIEND_LIST = 3;
}
