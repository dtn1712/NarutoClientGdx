package Objectgame;

import com.thdgaming.naruto.MGraphics;

import real.mFont;
import lib.mVector;
import model.ChatManager;
import model.Info;
import model.Scroll;
import model.mResources;
import network.Message;

public class ChatWorld {
	
	public static mVector listNodeChat= new mVector();
	
	public ChatWorld()
	{
		
	}
	//add chat world
	public  static void ChatWorld (Message msg)
	{
		 try {
			 	short idplayerGui = msg.reader().readShort();
				String namethanggui =  msg.reader().readUTF();
				String mess = msg.reader().readUTF();
				
				//show message on screen
				ChatManager.gI().addChat(mResources.GLOBALCHAT[0], namethanggui, mess);
				if(!ChatManager.blockGlobalChat)
					Info.addInfo(namethanggui+": "+mess, 80, mFont.tahoma_7b_yellow);
				
				NodeChat note = new NodeChat(mess, false,namethanggui);
				mVector dem2 = new mVector();
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0 ; j--) {
					NodeChat nodechat =(NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(nodechat!=null)
					dem2.addElement(nodechat);
				}
				ChatWorld.listNodeChat.removeAllElements();
				ChatWorld.listNodeChat = dem2;
				ChatWorld.listNodeChat.addElement(note);
				mVector dem = new mVector();
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0 ; j--) {
					NodeChat nodechat =(NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(nodechat!=null)
					dem.addElement(nodechat);
				}
				ChatWorld.listNodeChat.removeAllElements();
				ChatWorld.listNodeChat = dem;
				int demt = 0;
				for (int j = ChatWorld.listNodeChat.size()-1; j >=0; j--) {
					NodeChat node2 = (NodeChat)ChatWorld.listNodeChat.elementAt(j);
					if(node2!=null){
						demt+=node2.hnode+NodeChat.HSTRING/2;
					}
				}
//				if(idplayerGui ==Char.myChar().charID)//chinh no
//				{
//					NodeChat node = new NodeChat(mess, true,namethanggui);
//					ChatWorld.listNodeChat.addElement(node);
//				}
//				else// orther player
//				{
//					NodeChat node = new NodeChat(mess, false,namethanggui);
//					ChatWorld.listNodeChat.addElement(node);
//				}
				
				if(ChatWorld.listNodeChat.size()>=200)//limit 200 message
				{
					ChatWorld.listNodeChat.removeElementAt(ChatWorld.listNodeChat.size()-1);
				}
				
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
	}
	
	public static void Paint(MGraphics g, int xTab, int yTab, int width, Scroll scrMain )
	{
		int notehdem = 1;
		for (int i =ChatWorld.listNodeChat.size()-1;i>=0;i--) 
		{
		    NodeChat node = (NodeChat)ChatWorld.listNodeChat.elementAt(i);
		    if(node!=null)
		    {
			     if(!node.isMe)
			     {
//				      if(node.type!=NodeChat.t_EMO)
//				      {
//				    	  node.paintNodeChat(graphic, xTab+NodeChat.HSTRING/2, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+1,0);
//				      }
				      node.paint(g, xTab+NodeChat.HSTRING,yTab+ NodeChat.HSTRING/2+ (notehdem));
			     }
			     else
			     {
//				      if(node.type!=NodeChat.t_EMO)
//				      {
//				    	  node.paintNodeChat(graphic, xTab-5*NodeChat.HSTRING/4+width-node.wnode+40, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+1,1);
//				      }
				      node.paint(g, xTab+width-NodeChat.HSTRING-node.wnode+40, yTab+ NodeChat.HSTRING/2+ (notehdem));
			     }
			     	notehdem+=node.hnode+NodeChat.HSTRING/2;
			     	
		    }
		}
		if(notehdem>scrMain.height)
     		scrMain.cmtoY=-scrMain.height+notehdem;//dung de scroll tu duoi len
	}
	public static int update()
	{
		int dem = 0;
		for (int i = ChatWorld.listNodeChat.size()-1; i >=0; i--) {
			NodeChat node = (NodeChat)ChatWorld.listNodeChat.elementAt(i);
			if(node!=null){
				dem+=node.hnode+NodeChat.HSTRING/2;
			}
		}
		return dem;
	}
	
}
