package Objectgame;


public class TaskOrder {

	public static final byte TASK_DAY = 0;
	public static final byte TASK_BOSS = 1;
	public static final byte TASK_GIOITHIEU = 2;
	public static final byte TASK_SUKIEN1 = 3;
	public static final byte TASK_SUKIEN2 = 4;
	public static final byte TASK_SUKIEN3 = 5;
	public static final byte TASK_SUKIEN4 = 6;
	public int taskId;
	public int count;
	public int maxCount;
	public String name;
	public String description;
	public int killId;
	public int mapId;
	
	public TaskOrder(byte taskId, int count, int maxCount, String name, String description, int killId, int mapId){
		this.count = count;
		this.maxCount = maxCount;
		this.taskId = taskId;
		this.name = name;
		this.description = description;
		this.killId = killId;
		this.mapId = mapId;
	}
}
