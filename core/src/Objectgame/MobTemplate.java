package Objectgame;


import lib.Bitmap;
import model.EffectData;

import model.Frame;
import model.ImageInfo;

public class MobTemplate {
	public byte rangeMove, speed, type;
	public short mobTemplateId;
	public int hp;
	public String name;
	public Bitmap imgs[] = new Bitmap[8];
	public ImageInfo[] imginfo;
	public Frame[] frameBoss;
	public byte[] frameBossMove;
	public byte[][] frameBossAttack;
	
	public ImageInfo getImgInfo(byte frame){
		return imginfo[frame];
	}

}
