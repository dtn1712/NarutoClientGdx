package Objectgame;

import java.util.Vector;

import network.Message;
import lib.Cout;
import lib.mVector;
/*
 * this class use for chat of char
 */
public class ChatPrivate {
	public static Vector  vOtherchar= new Vector();//list chater
	public static boolean isBlock;//khoa tin nhan lai khong cho truot
	public static int nTinChuaDoc;
	
	// add when clicking chat private
	
	public static void AddNewChater(short idplayerNhan,String namethangnhan )
	{
		OtherChar other = new OtherChar(idplayerNhan,namethangnhan);
//		for (int i = 0; i < 7; i++) {
//			NodeChat note = new NodeChat(i+" chat o đây nè thì phai chat ngay đây nè." +
//					" chat o đây nè thì phai chat ngay đây nè.", false,other.name);
//			mVector dem2 = new mVector();
//			
//			for (int j = other.listChat.size()-1; j >=0 ; j--) {
//				NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
//				if(nodechat!=null)
//				dem2.addElement(nodechat);
//			}
//			other.listChat.removeAllElements();
//			other.listChat = dem2;
//			
//			other.listChat.addElement(note);
//			mVector dem = new mVector();
//			for (int j = other.listChat.size()-1; j >=0 ; j--) {
//				NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
//				if(nodechat!=null)
//				dem.addElement(nodechat);
//			}
//			other.listChat.removeAllElements();
//			other.listChat = dem;
//			int demt = 0;
//			for (int j = other.listChat.size()-1; j >=0; j--) {
//				NodeChat node2 = (NodeChat)other.listChat.elementAt(j);
//				if(node2!=null){
//					demt+=node2.hnode+NodeChat.HSTRING/2;
//				}
//			}
//		}
		vOtherchar.addElement(other);
		

	}

	//addd chat 
	public static void ChatFriend(Message msg)
	{
		 try {
			short idplayerGui = msg.reader().readShort();
			String namethanggui =  msg.reader().readUTF();
			short idplayerNhan = msg.reader().readShort();
			String namethangnhan =  msg.reader().readUTF();
			String mess = msg.reader().readUTF();
			
			if(idplayerGui ==Char.myChar().charID)//chinh no
			{
				boolean isAdded = false;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
					OtherChar other = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
					//thang nguoi nhan
					if(other.id==idplayerNhan||other.name.equals(namethangnhan)){
						isAdded = true;
						NodeChat note = new NodeChat(mess, true,other.name);
						mVector dem2 = new mVector();
						for (int j = other.listChat.size()-1; j >=0 ; j--) {
							NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
							if(nodechat!=null)
							dem2.addElement(nodechat);
						}
						other.listChat.removeAllElements();
						other.listChat = dem2;
						other.listChat.addElement(note);
						mVector dem = new mVector();
						for (int j = other.listChat.size()-1; j >=0 ; j--) {
							NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
							if(nodechat!=null)
							dem.addElement(nodechat);
						}
						other.listChat.removeAllElements();
						other.listChat = dem;
						int demt = 0;
						for (int j = other.listChat.size()-1; j >=0; j--) {
							NodeChat node2 = (NodeChat)other.listChat.elementAt(j);
							if(node2!=null){
								demt+=node2.hnode+NodeChat.HSTRING/2;
							}
						}
						if(ChatPrivate.isBlock&&idplayerGui!=Char.myChar().charID){
						}else{
//							ChatRieng.scrChat.cmtoY = (demt-ChatRieng.hChat);
						}
						
//						other.listChat.addElement(note);
					}
				}
				
				if(!isAdded||ChatPrivate.vOtherchar.size()==0)//nhan tin tu player khac lan dau tien
				{
					OtherChar other = new OtherChar(idplayerNhan,namethangnhan);
					
					NodeChat note = new NodeChat(mess, true,other.name);
					mVector dem2 = new mVector();
					
					for (int j = other.listChat.size()-1; j >=0 ; j--) {
						NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
						if(nodechat!=null)
						dem2.addElement(nodechat);
					}
					other.listChat.removeAllElements();
					other.listChat = dem2;
					
					other.listChat.addElement(note);
					mVector dem = new mVector();
					for (int j = other.listChat.size()-1; j >=0 ; j--) {
						NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
						if(nodechat!=null)
						dem.addElement(nodechat);
					}
					other.listChat.removeAllElements();
					other.listChat = dem;
					int demt = 0;
					for (int j = other.listChat.size()-1; j >=0; j--) {
						NodeChat node2 = (NodeChat)other.listChat.elementAt(j);
						if(node2!=null){
							demt+=node2.hnode+NodeChat.HSTRING/2;
						}
					}
					if(ChatPrivate.isBlock&&idplayerGui!=Char.myChar().charID){
					}else{
						//ChatRieng.scrChat.cmtoY = (demt-ChatRieng.hChat);
					}
					ChatPrivate.vOtherchar.addElement(other);
				}
			}
			else if(idplayerNhan == Char.myChar().charID){ //nhan tin tu thang khac
				boolean isAdded = false;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
					OtherChar other = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
					other.nTinChuaDoc ++;
					if(other.id==idplayerGui||other.name.equals(namethanggui)){
						isAdded = true;
						NodeChat note = new NodeChat(mess, false,namethanggui);
						mVector dem2 = new mVector();
						for (int j = other.listChat.size()-1; j >=0 ; j--) {
							NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
							if(nodechat!=null)
							dem2.addElement(nodechat);
						}
						other.listChat.removeAllElements();
						other.listChat = dem2;
						
						other.listChat.addElement(note);
						mVector dem = new mVector();
						for (int j = other.listChat.size()-1; j >=0 ; j--) {
							NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
							if(nodechat!=null)
							dem.addElement(nodechat);
						}
						other.listChat.removeAllElements();
						other.listChat = dem;
						int demt = 0;
						for (int j = other.listChat.size()-1; j >=0; j--) {
							NodeChat node2 = (NodeChat)other.listChat.elementAt(j);
							if(node2!=null){
								demt+=node2.hnode+NodeChat.HSTRING/2;
							}
						}
						if(ChatPrivate.isBlock&&idplayerGui!=Char.myChar().charID){
						}else{
//							ChatRieng.scrChat.cmtoY = (demt-ChatRieng.hChat);
						}
					
//						other.listChat.addElement(note);
					}
				}
				if(!isAdded||ChatPrivate.vOtherchar.size()==0)// neu chua luu lan nao
				{
					OtherChar other = new OtherChar(idplayerGui,namethanggui);
					other.nTinChuaDoc ++;
					NodeChat note = new NodeChat(mess, false,namethanggui);
					mVector dem2 = new mVector();
					for (int j = other.listChat.size()-1; j >=0 ; j--) {
						NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
						if(nodechat!=null)
						dem2.addElement(nodechat);
					}
					other.listChat.removeAllElements();
					other.listChat = dem2;
					
					other.listChat.addElement(note);
					mVector dem = new mVector();
					for (int j = other.listChat.size()-1; j >=0 ; j--) {
						NodeChat nodechat =(NodeChat)other.listChat.elementAt(j);
						if(nodechat!=null)
						dem.addElement(nodechat);
					}
					other.listChat.removeAllElements();
					other.listChat = dem;
					int demt = 0;
					for (int j = other.listChat.size()-1; j >=0; j--) {
						NodeChat node2 = (NodeChat)other.listChat.elementAt(j);
						if(node2!=null){
							demt+=node2.hnode+NodeChat.HSTRING/2;
						}
					}
					if(ChatPrivate.isBlock&&idplayerGui!=Char.myChar().charID){
					}else{
//						ChatRieng.scrChat.cmtoY = (demt-ChatRieng.hChat);
					}

					ChatPrivate.vOtherchar.addElement(other);
				}
				ChatPrivate.nTinChuaDoc = 0;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
					OtherChar acc = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
					if(acc!=null) ChatPrivate.nTinChuaDoc+=acc.nTinChuaDoc;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
