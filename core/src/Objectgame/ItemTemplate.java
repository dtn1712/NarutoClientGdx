package Objectgame;


public class ItemTemplate {
	
	public static final byte ITEM_TEMPLATE = 5;
	public static final byte CHAR_WEARING = 0;
	
	public short id;
	public byte type;
	public byte gender; // 1 là đồ nam, 0 là đồ nữ, 2 là đồ chung
	public String name;
	public String description;
	public byte level;
	public short iconID; // icon small image id
	public short part;
	public short partquan;
	public boolean isUpToUp;
	public int w, h;
	public long gia;
	public String[] despaint;
	
	// ngược(xu)

	public ItemTemplate(short templateID, byte type, byte gender, String name, String description, byte level, short iconID, short part, boolean isUpToUp) {
		super();
		this.id = templateID;
		this.type = type;
		this.gender = gender;
		this.name = name;
		this.description = description;
		this.level = level;
		this.iconID = iconID;
		this.part = part;
		this.isUpToUp = isUpToUp;
	}
	
	public ItemTemplate(short templateID, byte type, byte gender, String name, String description, byte level, short iconID, short part, short partquan, boolean isUpToUp) {
		super();
		this.id = templateID;
		this.type = type;
		this.gender = gender;
		this.name = name;
		this.description = description;
		this.level = level;
		this.iconID = iconID;
		this.part = part;
		this.partquan = partquan;
		//System.out.println("PART QUAN THEM VAO SAU KHI THANH 1 SET ----> "+partquan+" AOOOOO "+part);
		this.isUpToUp = isUpToUp;
	}
	
	public ItemTemplate(short templateID, byte type,String name, short iconID) {
		super();
		this.id = templateID;
//		this.type = type;
//		this.gender = gender;
		this.name = name;
//		this.description = description;
//		this.level = level;
		this.iconID = iconID;
//		this.part = part;
//		this.isUpToUp = isUpToUp;
	}
}
