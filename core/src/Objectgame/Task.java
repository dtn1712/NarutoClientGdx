package Objectgame;

import real.FontSys;
import real.mFont;



public class Task {
	//loại nhiệm vụ
	public static byte DOING_TASK = 1;
	public static byte CAN_RECEIVE_TASK = 0;
	public static byte COMPLETE_TASK = 2;
	public static byte TYPE_DOING_TASK_IP = 2; // vận chuyển
	public static byte TYPE_DOING_TASK_COLLECT = 0;
	public static byte TYPE_DOING_TASK_SKILL = 1;
	public static byte TYPE_DOING_TASK_TALK = 3;
	//
	public int index, max;
	public short counts[];
	public short taskId;
	public String[] names, details;
	public String[] subNames;
	public short count;
	
	
	public Task(short taskId, byte index, String name, String detail, String[] subNames, short[] counts, short count){
		this.taskId = taskId;
		this.index = index;
		this.names = mFont.tahoma_7b_white.splitFontArray(name, 155) ;
		this.details = mFont.tahoma_7.splitFontArray(detail, 155) ;
		this.subNames = subNames;
		this.counts = counts;
		this.count = count;
	}
}
