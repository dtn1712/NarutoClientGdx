package Objectgame;

import java.io.DataInputStream;
import java.util.Vector;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import lib.MyStream;
import screen.old.GameScr;

public class BgItem {
	public int id;
	public int trans;
	public short idImage;
	public int x;
	public int y;
	public int dx;
	public int dy;

	public byte layer;
	public int nTilenotMove;
	public int[] tileX;
	public int[] tileY;
	
	public static Hashtable imgNew = new Hashtable();
	public static Vector vKeysNew = new Vector();
	public static Vector vKeysLast = new Vector();
	public static byte[] newSmallVersion;
	
	public static void clearHashTable() {
		// Res.out("vKey size= " + vKeys.size() + " imgsize= " + imgNew.size() +
		// "---------------------------------------");
		// if (imgNew.size() > 20) {
		// // while (vKeys.size() > 10) {
		// // imgNew.remove((String) vKeys.elementAt(0));
		// // vKeys.removeElementAt(0);
		// // }
		// for (int i = 0; i < 10; i++) {
		// imgNew.remove((String) vKeys.elementAt(i));
		//
		// }
		// for (int i = 0; i < 10; i++) {
		// vKeys.removeElementAt(0);
		// }
		// }
		 imgNew.clear();
		 vKeysNew.removeAllElements();
		 vKeysLast.removeAllElements();
	}

	public static boolean isExistKeyNews(String keyNew) {
		for (int a = 0; a < vKeysNew.size(); a++) {
			String strkeyNew = (String) BgItem.vKeysNew.elementAt(a);
			if (strkeyNew.equals(keyNew))
				return true;
		}
		return false;
	}

	public static boolean isExistKeyLast(String keyLast) {
		for (int a = 0; a < vKeysLast.size(); a++) {
			String keyLaString = (String) BgItem.vKeysLast.elementAt(a);
			if (keyLaString.equals(keyLast))
				return true;
		}
		return false;
	}

	

	boolean isBlur = false;
	public int transX = 0;
	public int transY = 0;
	public static int[] idMiniBg = new int[] { 79, 80, 81, 85, 86, 90, 91, 92, 99, 100, 101, 102, 103, 104, 105, 106,
			107, 108};

	public boolean isMiniBg() {
		
		for (int i = 0; i < idMiniBg.length; i++)
			if (idImage == idMiniBg[i])
				return true;
		return false;
	}

	public static boolean isCorlorNotChange(int id) {
		for (int i = 0; i < idMiniBg.length; i++)
			if (id >= 78 && id <= 118)
				return true;
		return false;
	}
	public BgItem(){
		
	}
	public void paint(MGraphics g) {
		
//		GameScr.resetTranslate(graphic);
//		if(Char.isLoadingMap)
//			return;
		int cmx = GameScr.cmx;
		int cmy = GameScr.cmy;
		Bitmap image = null;
	
//		image = (Bitmap) imgNew.get(idImage+"");
		image = (Bitmap) imgNew.get(idImage+"");
		if (image != null) {
//			if(idImage==96)
//				return;
//			if (layer == 4) {
//				transX = -cmx / 2 + 100;
//			}
//			if (idImage == 28 && layer == 3)
//				transX = -cmx / 3 + 200;
//			if ((idImage == 67 || idImage == 68 || idImage == 69 || idImage == 70) && layer == 3)
//				transX = -cmx / 3 + 200;
//			if (isMiniBg() && layer < 4) {
//				transX = -(cmx >> 4) + 50;
//				transY = (cmy >> 5) - 15;
//			}
			int X = x + dx + transX;
			int Y = y + dy + transY;
//			if (x + dx + image.getWidth() + transX >= cmx && x + dx + transX <= cmx + GameCanvas.w
//					&& y + dy + transY + image.getHeight() >= cmy && y + dy + transY <= cmy + GameCanvas.h) {
//					System.out.println("PAINT IT BG");
					
					g.drawRegion(image/*GameScr.imgBgItem[idImage]*/, 0, 0, image.getWidth(), image.getHeight(), trans, x + dx + transX, y + dy + transY, 0);
					
//				if (idImage == 11) {
//					graphic.setClip(X, Y + 24, 48, 14);
//					for (int i = 0; i < 2; i++) {
//
//						graphic.drawRegion(TileMap.imgWaterflow, 0, ((GameCanvas.gameTick % 8) >> 2) * 24, 24, 24, 0, X + i
//								* 24, Y + 24, 0);
//					}
//					graphic.setClip(GameScr.cmx, GameScr.cmy, GameScr.gW, GameScr.gH);
//				}

//			}
//			System.out.println("ImageIIIII ---> "+X+" ,,, "+Y);
////			if(TileMap.isDoubleMap()&& idImage>137&& idImage!=156 && idImage!=159 && idImage !=157 && idImage!=165&&idImage!=167 && idImage!=168 && idImage!=169 && idImage!=170)
//				if (TileMap.pxw-( x + dx + transX ) >= cmx &&TileMap.pxw-( x+ dx + transX+ image.getWidth()) <= cmx + GameCanvas.w
//				&& y + dy + transY + image.getHeight() >= cmy && y + dy + transY <= cmy + GameCanvas.h) 
//				graphic.drawRegion(image, 0, 0, image.getWidth(),image.getHeight(), 2,TileMap.pxw-( x
//						+ dx + transX)- image.getWidth(), Char.myChar().cy, 0);
		}
	}

	public void changeColor() {
		// TODO Auto-generated method stub
		
	}
	
	public void loadMapItem(){ // đọc dữ liệu data Map item
		DataInputStream dis;
		try{
			dis = new DataInputStream(MyStream.readFile("/mapitem/mapItem"));
			short Count = dis.readShort();
			for(int i = 0; i < Count; i++){
				BgItem bi = new BgItem();
				bi.id = i;
				bi.idImage = dis.readShort();
				bi.layer = dis.readByte();
				bi.dx = dis.readShort();
				bi.dy = dis.readShort();
				byte nTileNotMove = dis.readByte();
				bi.tileX = new int[nTileNotMove];
				bi.tileY = new int[nTileNotMove];
				for (int j = 0; j < nTileNotMove; j++) {
					bi.tileX[i] = (int) dis.readByte(); 
					bi.tileY[i] = (int) dis.readByte();
				}
				TileMap.vItemBg.addElement(bi);

			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadMaptable(){
		DataInputStream dis = null;
		try{
			dis = new DataInputStream(MyStream.readFile("/mapitem/mapTable0"));
			short nTile = dis.readShort();
			TileMap.tileIndex = new int[nTile][][];
			TileMap.tileType = new int[nTile][];
			for (int i = 0; i < nTile; i++) {
				short nTypeSize = dis.readShort();
//				if(i==0)
//					System.out.println("set tile >>>>>>>>"+nTypeSize);
				TileMap.tileType[i] = new int[nTypeSize];
				TileMap.tileIndex[i] = new int[nTypeSize][];
				for (int a = 0; a < nTypeSize; a++) {
					TileMap.tileType[i][a] = dis.readInt();
					short sizeIndex = dis.readShort();
					TileMap.tileIndex[i][a] = new int[sizeIndex];
					for (int b = 0; b < sizeIndex; b++) {
						TileMap.tileIndex[i][a][b] = dis.readByte();
					}
				}
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	public Bitmap Imgmap[] = new Bitmap[230];
	public void loadImgmap(){
		try{
			for(int i = 0; i < Imgmap.length; i++ ){
				Imgmap[i] = GameCanvas.loadImage("/mapobject/"+i+".png");
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
}
