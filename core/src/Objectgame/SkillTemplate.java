package Objectgame;

public class SkillTemplate {
	public byte id;
	public int classId;
	public String name;
	public int maxPoint;
	public int type;
	public int iconId;
	public String[] description;
	public Skill[] skills;
	
	public SkillTemplate(byte id, String name, int idIcon){ // moi them vo để add vào vector template lấy thông tin
		this.id = id;
		this.name = name;
		this.iconId = idIcon;
	}
}
