package Objectgame;

import real.FontSys;
import real.mFont;
import lib.mVector;

public class EmoLineText {
	 public String[] listtext;
	 public mVector listEmo;
	 public int[] wlist;
	 public int loai;
	 
	 public EmoLineText(int loaii,String[] text,mVector listEmoo)
	 {
		 
		  this.wlist = new int[text.length];
		  this.wlist[0] = 0;
		  for (int i = 0; i < text.length; i++)
		  {
			   int with =  mFont.tahoma_7_yellow.getWidth(text[i])  /*+(i==0?0:this.wlist[i-1])+(i)*MGraphics.getImageWidth(MainTabScreen.imgEmo[0])*/;
			   if(i<this.wlist.length-1)
				   {
				    this.wlist[i+1] = with+this.wlist[i];
				   }
		  }
		  this.loai = loaii;
		  this.listtext = text;
		  this.listEmo = listEmoo;
	 }
}
