package Objectgame;

import com.thdgaming.naruto.MGraphics;

import real.Util;
import real.mFont;
import GuiOut.loadImageInterface;


import lib.mVector;

public class NodeChat {
	public int type;
	public static final byte t_TEXT = 0;// text
	public static final byte t_EMO = 1;// icon
	public static final byte t_TEXTEMO = 2;//vua test vua icon
	public static final byte HSTRING=12;//chieu cao font chu
	
	public String[][] textEmo;
	public String[] text;
	public String textdai,name;
	public static String mtextConvertEmo = "zpzp";
	public int hnode,wname;
	public boolean isMe;
	public static int wnode ;
	public mVector listEmoLine = new mVector();
	
	public static String[] maEmo = new String[]{ //14,
			"z($)z","z:Oz","(highfive)","z8-)","z:Sz","(wait)","(oliver)","z(nod)","(facepalm)","z:^)",
			"(muscle)","z:)z" ,"z|-)","(rofl)","(lala)","z(ske)","z:$z","z(n)z","z>o)","z;(z",
			"(bandit)","z(y)z","z(ci)","z:?z","(giggle)","z(a)z","(makeup)","z>2)","(swear)","(wave)",
			"zz(v)z","z(bow)","z(*)z","(emo)","z:]z","z:@z","(doh)","zz(6)","z:&z","(happy)",
			"z;)z","|-(","z(t)z","(whew)","(rock)","z:|z","(yawn)",":P","(hande)","(tmi)z",
			"(fingers)","z:Dz","(smirk)","(punch)","z:(z","z:*z","(wasntme)","8-|z","z::|","z:xz",
			"(waiting)","(clap)","z(mm)","(headbang)"
	};
	
	/*khong cho vao khung
	 * cat chuoi vua add icon hinh va doan chat
	 * isMee: true: la minh, false: thang khac
	 */
	public NodeChat(String textt, boolean isMee,String namee)
	{
		isMee = false;
		this.textdai = textt;
		this.isMe = isMee;
		if(this.isMe)
			namee = Char.myChar().cName;
		this.text  = mFont.tahoma_7_yellow.splitFontArray((isMee==false?namee+": ":"")+textt, wnode);
		this.type = t_TEXT;
		int[] typetext = new int[text.length];
		
		//update icon
		if(text.length>=1){
			for (int m = 0; m < text.length; m++) {
				mVector listEmo = new mVector();
				typetext[m] = t_TEXT;
				for (int i = 0; i < maEmo.length; i++) {

					if(text[0].indexOf(maEmo[i])>=0){
						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
						for (int j = 0; j < nEmo.size(); j++) {
							EmoText emo = (EmoText)nEmo.elementAt(j);
							listEmo.addElement(emo);
						}
						typetext[m] = t_EMO;
						if(this.type==t_TEXT)
						this.type = t_EMO;
					}
				}
				if(listEmo.size()>1)
				listEmo = XepThuTuEmo(listEmo);
				if(typetext[m] == t_EMO){
					int lengEmo = 0;
					for (int i = 0; i < listEmo.size(); i++) {
						EmoText emo = (EmoText)listEmo.elementAt(i);
						lengEmo+= maEmo[emo.loai].length();
					}
					if(lengEmo<text[m].length()){
						if(this.type==t_EMO)
						this.type = t_TEXTEMO;
						typetext[m] = t_TEXTEMO;
						
					}else if(lengEmo<text[m].length()){
						if(this.type==t_TEXT)
						this.type = t_EMO;
						typetext[m] = t_EMO;
					}
				}
				if(typetext[m] == t_TEXTEMO){
					String[] listext = slitFontWithEmo(text[m], listEmo);
					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
					listEmoLine.addElement(emoline);
				} else if(typetext[m] == t_TEXT){
					String[] textline = new String[1];
					textline[0] = text[m];
					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
				else if(typetext[m] == t_EMO){
					String[] textline = new String[1];
					textline[0] = "";
					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
			}
		}
		boolean[] isType = new boolean[3];
		for (int i = 0; i < typetext.length; i++) {
			if(typetext[i]==t_TEXT){
				isType[t_TEXT] = true;
			}
			if(typetext[i]==t_EMO){
				isType[t_EMO] = true;
			}
			if(typetext[i]==t_TEXTEMO){
				isType[t_TEXTEMO] = true;
			}
		}
		if(isType[t_TEXTEMO])
			this.type = t_TEXTEMO;
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
			this.type = t_TEXT;
		}
		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_EMO;
		}
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_TEXTEMO;
		}
		if(this.text!=null)
		this.hnode =HSTRING*(this.text.length)+HSTRING/4;
		this.name = namee;
		wname = mFont.tahoma_7_yellow.getWidth(name);
	}
	
	/*cho vao khung
	 * cat theo w,h de xuong dong
	 * wnodePainInfoGameScr: do rong cua khung
	 */
	public NodeChat(String textt, boolean isMee,String namee,int wnodePainInfoGameScr)
	{
		this.textdai = textt;
		this.isMe = isMee;
		this.text  = mFont.tahoma_7_yellow.splitFontArray((isMee==false?namee+": ":"")+textt, wnodePainInfoGameScr);
		this.type = t_TEXT;
		int[] typetext = new int[text.length];
		
		//update icon
		if(text.length>=1){
			for (int m = 0; m < text.length; m++) {
				mVector listEmo = new mVector();
				typetext[m] = t_TEXT;
				for (int i = 0; i < maEmo.length; i++) {
					if(text[m].indexOf(maEmo[i])>=0){
						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
						for (int j = 0; j < nEmo.size(); j++) {
							EmoText emo = (EmoText)nEmo.elementAt(j);
							listEmo.addElement(emo);
						}
						typetext[m] = t_EMO;
						if(this.type==t_TEXT)
						this.type = t_EMO;
					}
				}
				if(listEmo.size()>1)
				listEmo = XepThuTuEmo(listEmo);
				if(typetext[m] == t_EMO){
				int lengEmo = 0;
					for (int i = 0; i < listEmo.size(); i++) {
						EmoText emo = (EmoText)listEmo.elementAt(i);
						lengEmo+= maEmo[emo.loai].length();
					}
					if(lengEmo<text[m].length()){
						if(this.type==t_EMO)
						this.type = t_TEXTEMO;
						typetext[m] = t_TEXTEMO;
						
					}else if(lengEmo<text[m].length()){
						if(this.type==t_TEXT)
						this.type = t_EMO;
						typetext[m] = t_EMO;
					}
				}
				if(typetext[m] == t_TEXTEMO){
					String[] listext = slitFontWithEmo(text[m]+"   ", listEmo);
					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
					listEmoLine.addElement(emoline);
				} else if(typetext[m] == t_TEXT){
					String[] textline = new String[1];
					textline[0] = text[m];
					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
				else if(typetext[m] == t_EMO){
					String[] textline = new String[1];
					textline[0] = "";
					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
					listEmoLine.addElement(emoline);
				} 
			}
		}
		boolean[] isType = new boolean[3];
		for (int i = 0; i < typetext.length; i++) {
			if(typetext[i]==t_TEXT){
				isType[t_TEXT] = true;
			}
			if(typetext[i]==t_EMO){
				isType[t_EMO] = true;
			}
			if(typetext[i]==t_TEXTEMO){
				isType[t_TEXTEMO] = true;
			}
		}
		if(isType[t_TEXTEMO])
			this.type = t_TEXTEMO;
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
			this.type = t_TEXT;
		}
		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_EMO;
		}
		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
			this.type = t_TEXTEMO;
		}
		if(this.text!=null)
		this.hnode = HSTRING*(this.text.length)+HSTRING/4;
		this.name = namee;
		wname = mFont.tahoma_7_yellow.getWidth(name);
	}
	
	public void paint(MGraphics g, int x, int y){
		switch (type) {
		case t_TEXT:
			for (int j = 0; j < text.length; j++) {
				mFont.tahoma_7_yellow.drawString(g, text[j],x,y+j*HSTRING+1, 0);
			}
			break;
		case t_TEXTEMO:
			for (int i = 0; i < listEmoLine.size(); i++) {
				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
				switch (emoline.loai) {
				case t_TEXTEMO:
					int indexemo=0;
					for (int j = 0; j < emoline.listtext.length; j++) {
						if(emoline.listtext[j].equals(mtextConvertEmo))
						{
							EmoText emo = (EmoText)emoline.listEmo.elementAt(indexemo);
							try {
								indexemo++;
								g.drawImage(loadImageInterface.imgEmo[emo.loai],
										emoline.wlist[j]+x,
										y+i*HSTRING+1, 0);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
						else 
						if(emoline.listtext[j]!=null)
							mFont.tahoma_7_yellow.drawString(g,emoline.listtext[j],emoline.wlist[j]+  x,y+i*HSTRING+1, 0);
					}
				break;
				case t_TEXT:
					if(emoline.listtext[0]!=null)
						mFont.tahoma_7_yellow.drawString(g,emoline.listtext[0],x,y+i*HSTRING+1,0);
					break;
				case t_EMO:
					for (int j = 0; j < emoline.listEmo.size(); j++) {
						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
						g.drawImage(loadImageInterface.imgEmo[emo.loai],x+
								(j* MGraphics.getImageWidth(loadImageInterface.imgEmo[0])),
								y+i*HSTRING+1, 0);
					}
					break;
				default:
					break;
				}
			}
			break;
		case t_EMO:
			for (int i = 0; i < listEmoLine.size(); i++) {
				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
				for (int j = 0; j < emoline.listEmo.size(); j++) {
					EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
					g.drawImage(loadImageInterface.imgEmo[emo.loai],
							x+wnode-emoline.listEmo.size()* MGraphics.getImageWidth(loadImageInterface.imgEmo[0])-HSTRING/2+(j* MGraphics.getImageWidth(loadImageInterface.imgEmo[0])),
							y+i*HSTRING+1, 0);
				}
			}
			break;
		default:
			break;
		}
		
	}
	
	 public  String replace(String _text, String _searchStr, String _replacementStr) {
	        // String buffer to store str
	        StringBuffer sb = new StringBuffer();

	        try {
	        	 // Search for search
		        int searchStringPos = _text.indexOf(_searchStr);
		        int startPos = 0;
		        int searchStringLength = _searchStr.length();

		        // Iterate to add string
		        while (searchStringPos != -1) {
		            sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
		            startPos = searchStringPos + searchStringLength;
		            searchStringPos = _text.indexOf(_searchStr, startPos);
		        }

		        // Create string
		        sb.append(_text.substring(startPos,_text.length()));
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	        return sb.toString();
	    } 
	 
	/*
	 *  kiem tra ma icon
	 */
	public String[] slitFontWithEmo(String text,mVector nEmo){
		mVector listtext = new mVector();
		for (int i = 0; i < nEmo.size(); i++) {
			EmoText emo = (EmoText)nEmo.elementAt(i);
			
			text=Util.replace(text,maEmo[emo.loai],mtextConvertEmo+" ");
		}
		String[] mangcat = mFont.split(text,mtextConvertEmo);	

		if(mangcat==null) return null;
		else {
			int nemo = nEmo.size();
			for (int i = 0; i < mangcat.length; i++) {
				if(!(i==0&&mangcat[i].length()==0)){
					listtext.addElement(mangcat[i]);
					if(nemo>0){
						nemo--;
						listtext.addElement(mtextConvertEmo);
					}
				}
				else {
					if(nemo>0){
						nemo--;
						listtext.addElement(mtextConvertEmo);
					}
				}
			}
		}
		String[] chuoitextt = new String[listtext.size()];
		for (int i = 0; i < listtext.size(); i++) {
			chuoitextt[i] = (String)listtext.elementAt(i);
		}
		return chuoitextt;
	}
	
	/*
	 * su dung de tim ma icon
	 */
	public mVector findAllEmoInText(String text,String maemo,int loaiemo){
		int index = 0;
		mVector nemo = new mVector();
		for (int i = 0; i < text.length(); i++) {
			if(text.charAt(i)==maemo.charAt(0)&&i+maemo.length()<=text.length()){
				boolean isMa = true;
				try {
					for (int j = 0; j < maemo.length(); j++) {
						if(maemo.charAt(j)!=text.charAt(i+j)){
							isMa = false;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(isMa){
					index = maemo.length();
					nemo.addElement(new EmoText(loaiemo,i));
					i +=index-1;
				}
			}
		}
		return nemo;
	}
	
	/*
	 * sort emo
	 */
	public mVector XepThuTuEmo(mVector listEmo){
		int vitri=0,loai=0;
		for (int i = 0; i < listEmo.size()-1; i++) {
			for (int j = i+1; j < listEmo.size(); j++) {
				EmoText emoI =(EmoText)listEmo.elementAt(i);
				EmoText emoJ =(EmoText)listEmo.elementAt(j);
				if(emoI.vitri>emoJ.vitri){
					vitri = emoI.vitri;
					loai = emoI.loai;
					//
					emoI.loai = emoJ.loai;
					emoI.vitri = emoJ.vitri;
					
					emoJ.loai = loai;
					emoJ.vitri = vitri;
				}
				
			}
		}
		return listEmo;
	}
	
	/*
	 * Paint background text
	 */
	public void paintNodeChat(MGraphics g, int xmainTab, int ymainTab, int wmainTab, int hmainTab, int trai)
	{
		try {
			//0 trai 1 phai
			int loai = trai*7;
			int sizeTile = 8;
			int sizewT = (wmainTab-sizeTile*2)/sizeTile;
			int wthieu = (wmainTab-sizeTile*2)%sizeTile;
			int sizewL = (hmainTab-sizeTile*2)/sizeTile;

			int hthieu = (hmainTab-sizeTile*2)%sizeTile;
//			if(sizewL<=0 || hthieu<=0) return;
			
			for (int i = 0; i < sizewT; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1), ymainTab, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(3+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1), ymainTab+hmainTab, g.BOTTOM|g.LEFT);
			}
			//2 duong dap vao cho du
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), wthieu, sizeTile, 0, xmainTab+wmainTab-sizeTile, ymainTab, g.TOP|g.RIGHT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(3+loai), wthieu, sizeTile, 0, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab, g.BOTTOM|g.RIGHT);
		
			
			
			
			for (int i = 0; i < sizewL; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile,sizeTile, 0, xmainTab, ymainTab+sizeTile*(i+1), g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile,sizeTile, 2,  xmainTab+wmainTab, ymainTab+sizeTile*(i+1), g.TOP|g.RIGHT);
			}
			//2 duong dap vao hang doc
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai),sizeTile, hthieu, 0,
					xmainTab,  ymainTab+sizeTile*(sizewL+1), g.TOP|g.LEFT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai), sizeTile,hthieu, 2,
					xmainTab+wmainTab,  ymainTab+sizeTile*(sizewL+1), g.TOP|g.RIGHT);
			for (int i = 0; i < sizewT; i++) {
				for (int j = 0; j < sizewL; j++) {
					g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), sizeTile, sizeTile, 0, xmainTab+sizeTile*(i+1),ymainTab+sizeTile*(j+1), g.TOP|g.LEFT);	
				}
			}
			for (int i = 0; i < sizewL; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), wthieu, sizeTile, 0,
						xmainTab+wmainTab-sizeTile-wthieu, ymainTab+sizeTile*(i+1), g.TOP|g.LEFT);
			}
			for (int i = 0; i < sizewT; i++) {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), sizeTile, hthieu, 0,
						xmainTab+sizeTile*(i+1)          , ymainTab+hmainTab-sizeTile-hthieu, g.TOP|g.LEFT);
			}
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(4+loai), wthieu, hthieu, 0, xmainTab+wmainTab-sizeTile-wthieu, ymainTab+hmainTab-sizeTile-hthieu, g.TOP|g.LEFT);
				
			if(loai==0){
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab, ymainTab, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab, ymainTab+sizeTile/2, g.TOP|g.LEFT);
			
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab, ymainTab, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab, ymainTab+sizeTile/2, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(0+loai), sizeTile, sizeTile, 0, xmainTab-sizeTile, ymainTab, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(5+loai), sizeTile, sizeTile, 2, xmainTab+wmainTab, ymainTab+hmainTab, g.BOTTOM|g.RIGHT);
				
			}else {
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 1, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile/2, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 2, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, g.TOP|g.LEFT);
			
//				graphic.drawRegion(imgChat, 0, sizeTile*(1+loai), sizeTile, sizeTile/2, 2, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, graphic.TOP|graphic.LEFT,true);
//				graphic.drawRegion(imgChat, 0, sizeTile*(2+loai)+sizeTile/2, sizeTile, sizeTile/2, 0, xmainTab+wmainTab-sizeTile, ymainTab+hmainTab-sizeTile, graphic.TOP|graphic.LEFT,true);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(0+loai), sizeTile, sizeTile, 3, xmainTab+wmainTab, ymainTab+hmainTab-sizeTile, g.TOP|g.LEFT);
				g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(6+loai), sizeTile, sizeTile, 2, xmainTab, ymainTab, g.TOP|g.LEFT);
			}
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(6+loai), sizeTile, sizeTile, 0, xmainTab+wmainTab, ymainTab, g.TOP|g.RIGHT);
			g.drawRegion(loadImageInterface.imgChat, 0, sizeTile*(5+loai), sizeTile, sizeTile, 0, xmainTab, ymainTab+hmainTab, g.BOTTOM|g.LEFT);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
}
