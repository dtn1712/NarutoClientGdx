package Objectgame;

public class MovePoint {
	public int xEnd, yEnd, dir, cvx, cvy, status;

	public MovePoint(int xEnd, int yEnd, int act, int dir) {
		this.xEnd = xEnd;
		this.yEnd = yEnd;
		this.dir = dir;
		this.status = act;
	}

	public MovePoint(int xEnd, int yEnd) {
		this.xEnd = xEnd;
		this.yEnd = yEnd;
	}

}
