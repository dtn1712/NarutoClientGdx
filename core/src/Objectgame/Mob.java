package Objectgame;

import java.util.Vector;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import screen.old.GameScr;
import lib.Bitmap;
import lib.Cout;
import model.CRes;
import model.Res;
import model.ServerEffect;
import model.StaticObj;

public class Mob implements IMapObject {
	
	public static int idloadimage;

	public static final byte TYPE_DUNG = 0;
	public static final byte TYPE_DI = 1;
	public static final byte TYPE_NHAY = 2;
	public static final byte TYPE_LET = 3;
	public static final byte TYPE_BAY = 4;
	public static final byte TYPE_BAY_DAU = 5;
	
	public static MobTemplate[] arrMobTemplate;

	// ========================
	public static final byte MA_INHELL = 0; // trạng thái lần đầu tiên
	public static final byte MA_DEADFLY = 1; // chết
	public static final byte MA_STANDWAIT = 2; // nghỉ
	public static final byte MA_ATTACK = 3; // đánh trái
	public static final byte MA_STANDFLY = 4; // đậu khi bay
	public static final byte MA_WALK = 5;// bay
	public static final byte MA_FALL = 6;// rớt
	public static final byte MA_INJURE = 7;// bị thương
	// ========================
	// public static final byte MA_MOVE_LEFT = 5;
	// public static final byte MA_MOVE_RIGHT = 6;
	// public static final byte MA_MOVE_UP = 7;
	// public static final byte MA_MOVE_DOWN = 8;

	// FLYING TEXT
	public String flyString;
	public int flyx, flyy, flyIndex;

	// mobP1: bien dem;
	// mobF: loai quai

	// mobCharIndex:
	public int hp, maxHp, x, y, frame, dir = 1, dirV = 1, status, p1, p2, p3, xFirst, yFirst, vy, exp, w, h, hpInjure, charIndex, timeStatus;
	public short mobId;
	public boolean isx, isy, isDisable, isDontMove, isFire, isIce, isWind;
	public Vector vMobMove = new Vector();
	public boolean isGo;
	public String mobName;
	public int templateId;
	public short pointx, pointy;
	public Char cFocus;
	public int dame, dameMp;
	public int sys, typeAtt;
	public byte levelBoss, level;
	public boolean isBoss;
	private long timeStartDie = 0;
	private int frameIndex = 0;
	
	public int minX, minY, maxX, maxY;
	
	public Mob(byte mobId, boolean isDisable, boolean isDontMove, boolean isFire, boolean isIce, boolean isWind, int templateId, int sys, int hp, byte level, int maxp, short pointx, short pointy, byte status, byte levelBoss, boolean isBos) {
		this.isDisable = isDisable;
		this.isDontMove = isDontMove;
		this.isFire = isFire;
		this.isIce = isIce;
		this.isWind = isWind;
		this.sys = sys;
		this.mobId = mobId;
		this.templateId = templateId;
		this.hp = hp;
		this.level = level;
		this.xFirst = this.x = this.pointx = pointx;
		this.yFirst = this.y = this.pointy = pointy;
		this.status = status;
		this.maxHp = maxp;
		this.levelBoss = levelBoss;
		this.isBoss = isBos;
//		if (arrMobTemplate[templateId].imgs == null) {
//			arrMobTemplate[templateId].imgs = new Bitmap[0];
////			Service.getInstance().requestModTemplate(templateId);
//		}
	}
	public Mob(byte mobId, int hp, byte level, int maxhp, short x, short y,
			short minX, short minY, short maxX, short maxY) { // test thôi chưa
																// đủ
		this.mobId = mobId;
		this.hp = hp;
		this.level = level;
		this.maxHp = maxhp;
		this.x = x;
		xFirst = x;
//		this.x = 175;
//		Cout.println("XXXXXXXXX MOD ----> "+x);
		this.y = y;
		yFirst = y;
//		this.y = 395;
		Cout.println(mobId+"  xxx  "+x+ "YYYYYYYYY MOD -----> "+y);
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.dir = 1;
		this.dir = (CRes.random(2)%2==0?-1:1);
		status = MA_WALK;
		if (arrMobTemplate[templateId].imgs == null) {
			arrMobTemplate[templateId].imgs = new Bitmap[0];
//			Service.getInstance().requestModTemplate(templateId);
		}
	}
	public void update() {
//		for(int i = 0; i < GameScr.vMob.size(); i++){
//			Mob mobCheck  = (Mob) GameScr.vMob.elementAt(i);
//			if(mobCheck.arrMobTemplate[mobCheck.mobId].imgs[0] == null){
//				for (int k = 0; k < mobCheck.arrMobTemplate[mobCheck.mobId].imgs.length; k++) {
//					if(mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k]==null)
//					mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k] = GameCanvas.loadImage("/mob/"+mobCheck.mobId+"/"+k+".png"); 
////					Cout.println(("/mob/"+mobCheck.mobId+"/"+k+".png")+" load "+mobCheck.arrMobTemplate[mobCheck.mobId].imgs[k]);
//				}
//			}
//		}
//		status = Mob.MA_ATTACK;
//		cFocus = Char.myChar();
		if(arrMobTemplate[mobId]==null){
			arrMobTemplate[mobId] = new MobTemplate();
		}else
		{
			if(arrMobTemplate[mobId].imgs[0]==null){
				for (int i = 0; i < arrMobTemplate[mobId].imgs.length; i++) {
					if(arrMobTemplate[mobId].imgs[i]==null)
					arrMobTemplate[mobId].imgs[i] = GameCanvas.loadImage("/mob/"+mobId+"/"+(i+1)+".png"); 
					if(arrMobTemplate[mobId].imgs[i]!=null)
					h = MGraphics.getImageHeight(arrMobTemplate[mobId].imgs[i]);
//					Cout.println(("/mob/"+mobId+"/"+(i+1)+".png")+" load "+arrMobTemplate[mobId].imgs[i]);
//					
				}
			}
		}
//		if (!isUpdate())
//			return;
//
//
//		if (vMobMove == null && arrMobTemplate[templateId].rangeMove != 0)
//			return;
		if (status != MA_ATTACK && isBusyAttackSomeOne) {
			if (cFocus != null) {

				cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
				cFocus = null;
			}
			isBusyAttackSomeOne = false;
		}

		switch (status) {
		case MA_DEADFLY:
			isDisable = false;
			isDontMove = false;
			isFire = false;
			isIce = false;
			isWind = false;
			if (templateId != 98 && templateId != 99) {
				p1++;
				y += p1;
				if (GameCanvas.gameTick % 2 == 0) {
					if (p2 > 1)
						p2--;
					else if (p2 < -1)
						p2++;
				}
				x += p2;
				frame = isBoss ? 10 : 2;
				if (y > GameScr.gssye * 24 || x < GameScr.gssx * 24 || x > GameScr.gssxe * 24) {
					p1 = 0;
					p2 = 0;
					x = y = 0;
					hp = getTemplate().hp;
					status = MA_INHELL;
					frame = 0;
					timeStatus = 0;
					return;
				}
				if (p3 == 0 && (TileMap.tileTypeAtPixel(x, y) & TileMap.T_TOP) == TileMap.T_TOP) {
					p1 = p1 > 4 ? -4 : -p1;
					p3 = 16;
				}
				if (p3 > 0)
					p3--;
			} else {
				long now = System.currentTimeMillis();
				if(now - timeStartDie > 1200)
					status = MA_INHELL;
			}
			break;
		case MA_STANDWAIT:
			timeStatus = 0;
			updateMobStandWait();
			break;
		case MA_STANDFLY:
			timeStatus = 0;
			frame = 0;
			p1++;
			if (p1 > 40 + mobId % 5) {
				y -= 2;
				status = MA_WALK;
				p1 = 0;
			}
			break;
		case MA_ATTACK:
			updateMobAttack();
			break;
		case MA_WALK:
			try {
				if (GameCanvas.gameTick % 4 == 0 && isBoss) {
					frameIndex++;
					if (frameIndex > arrMobTemplate[templateId].frameBossMove.length - 1)
						frameIndex = 0;
				}
			} catch (Exception e) {
			}
			timeStatus = 0;
			updateMobWalk();
			break;
		case MA_FALL:
			timeStatus = 0;
			p1++;
			y += p1;
			if (y >= yFirst) {
				y = yFirst;
				p1 = 0;
				status = MA_WALK;
			}
			break;
		case MA_INJURE:
			updateInjure();
			break;
		}
		if (mobToAttack != null) {
			int dx = mobToAttack.x - x;
			int dy = mobToAttack.y - y;
			x += dx / 4;
			y += dy / 4;
			if (mobToAttack.status==MA_DEADFLY||mobToAttack.status==MA_INHELL||(Res.abs(dx) < 20 && Res.abs(dy) < 20)) {
				ServerEffect.addServerEffect(59, mobToAttack.x, mobToAttack.y, 1);
				mobToAttack = null;
			}
		}
	}

	public void setInjure() {
		if (hp > 0) {
			f = 0;
			timeStatus = 12;
			status = MA_INJURE;
		}
	}
	public boolean isBusyAttackSomeOne=true;
	public void setAttack(Char cFocus) {
		isBusyAttackSomeOne = true;
		this.cFocus = cFocus;
		p1 = 0;
		p2 = 0;
		status = MA_ATTACK;
		frameIndex = 0;
		typeAtt = 0;
	}

//	public void setAttack(BuNhin bFocus) {
//		this.bFocus = bFocus;
//		p1 = 0;
//		p2 = 0;
//		status = MA_ATTACK;
//		typeAtt = 1;
//	}

	private void updateInjure() {
//		frame = isBoss ? (getTemplate().mobTemplateId == 139) ? 4 : (getTemplate().mobTemplateId == 160) ? 12 : 10 : 2;
		f++;
		frame = frameBeAttack[f%frameBeAttack.length];
		if(getTemplate().mobTemplateId == 141)
			frame = 13;
		timeStatus--;
		if (timeStatus <= 0) {
			if ((injureBy != null && injureThenDie) || hp == 0) {
				status = Mob.MA_DEADFLY;
				p2 = injureBy.cdir << 3;
				p1 = -5;
				p3 = 0;
			} else {
				status = MA_WALK;
				if (injureBy != null) {
					dir = -injureBy.cdir;
					if (Res.abs(x - injureBy.cx) < 24)
						status = MA_STANDWAIT;
				}
				p1 = p2 = p3 = 0;
				timeStatus = 0;
			}
			injureBy = null;
			return;
		}
		if (arrMobTemplate[templateId].type != 0) {
			int fallBack = -injureBy.cdir << 1;
			if (x > xFirst - arrMobTemplate[templateId].rangeMove && x < xFirst + arrMobTemplate[templateId].rangeMove)
				x -= fallBack;
		}
	}

	private void updateMobStandWait() {
		switch (arrMobTemplate[templateId].type) {
		case TYPE_DUNG:
		case TYPE_DI:
		case TYPE_NHAY:
		case TYPE_LET:
			frame = 0;
			p1++;
			if (p1 > 10 + mobId % 10) {
				status = MA_WALK;
			}
			if(isBoss)
				frame = GameCanvas.gameTick % 101 > 1 ? 0 : 1;
			break;
		case TYPE_BAY:
		case TYPE_BAY_DAU:
			frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
			p1++;
			if (p1 > mobId % 3) {
				status = MA_WALK;
			}
			break;
		}

	}
	
	private void updateMobAttack() {
		f++;
		if(f>=frameAttack.length) 
			status = Mob.MA_WALK;
		frame = frameAttack[f%frameAttack.length];
		if (p1 == 0) {
			int xx = 0, yy = 0;
			if (typeAtt == 0) {
				xx = cFocus.cx;
				yy = cFocus.cy;
			} 
//			else if (typeAtt == 1) {
//				xx = bFocus.x;
//				yy = bFocus.y;
//			}
			if (Res.abs(xx - x) < 24 || Res.abs(xx - x) < 5 || arrMobTemplate[templateId].type == 0)
				frame = arrMobTemplate[templateId].imgs.length == 3 ? 0 : 3;
			
			if(isBoss && (Res.abs(xx - x) < 48 || Res.abs(xx - x) < 10 || arrMobTemplate[templateId].type == 0)) //level boss
				frame = arrMobTemplate[templateId].imgs.length == 3 ? 0 : 3;
			
			if(isBoss ){
				frameIndex++;
				if((Res.abs(xx - x) < 48 || Res.abs(yy - y) < 10)){
					if(frameIndex >= arrMobTemplate[templateId].frameBossAttack[0].length)
						frameIndex = 0;
					frame = arrMobTemplate[templateId].frameBossAttack[0][frameIndex];
				}
				else{
					if(frameIndex >= arrMobTemplate[templateId].frameBossAttack[1].length)
						frameIndex = 0;
					frame = arrMobTemplate[templateId].frameBossAttack[1][frameIndex];
				}
			}
			if(frame == 3 )
				p1 = 1;
			
			if (arrMobTemplate[templateId].type != 0 && !isDontMove && isIce && isWind)
				x += (xx - x) / 3;
			
			if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
				p1 = 1;
			}
			
			if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
				p1 = 1;
			}
			
			if ((arrMobTemplate[templateId].type == 4 || arrMobTemplate[templateId].type == 5)&& !isDontMove)
				y += (yy - y) / 20;
			p2++;
			if (( isBoss && Res.abs(xx - x) < 48 && Res.abs(yy - y) < 15) || (Res.abs(xx - x) < 12 && Res.abs(yy - y) < 12) || p2 > 12 || p1 == 1) {
				p1 = 1;
				if (typeAtt == 0) {
					if(isBoss && Res.abs(xx - x) < 48 && Res.abs(yy - y) < 15){
					//59, 52
						cFocus.doInjure(dame, dameMp,isBoss, getTemplate().mobTemplateId);
						isBusyAttackSomeOne = false;
						if( getTemplate().mobTemplateId == 114) // Boss Th�?
							ServerEffect.addServerEffect(79, cFocus, 3);
						else if( getTemplate().mobTemplateId == 115){ // Boss Rồng
//							if(cFocus == Char.myChar())
//								GameScr.shaking = 1;
//								GameScr.count = 0;
							ServerEffect.addServerEffect(81, cFocus.cx, yFirst + TileMap.size, 2);
							ServerEffect.addServerEffect(81, cFocus.cx - 40, yFirst + TileMap.size, 2);
							ServerEffect.addServerEffect(81, cFocus.cx + 40, yFirst + TileMap.size, 2);
						}
						else if( getTemplate().mobTemplateId == 116){ // Boss Robo xanh
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							if(cFocus.cx > x)
								ServerEffect.addServerEffect(86, x, y - h / 2 + 5, 1);
							else
								ServerEffect.addServerEffect(88, x, y - h / 2 + 5, 1);
							ServerEffect.addServerEffect(87,cFocus.cx, cFocus.cy - cFocus.ch/2 , 2);
							ServerEffect.addServerEffect(87, cFocus.cx - 40, cFocus.cy - cFocus.ch/2, 2);
							ServerEffect.addServerEffect(87, cFocus.cx + 40, cFocus.cy - cFocus.ch/2, 2);
						}else if(getTemplate().mobTemplateId == 138){ //Sumo : 138, Deamon: 139
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(89, x + w/2 , y - h / 2 - 5, 1);
							else
								ServerEffect.addServerEffect(89, x - w/2 , y - h / 2 - 5, 1, (byte)-1);
							ServerEffect.addServerEffect(90, cFocus, 2);
						}else if(getTemplate().mobTemplateId == 139){ //Sumo : 138, Deamon: 139
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							ServerEffect.addServerEffect(91, cFocus, 2);
						}else if(getTemplate().mobTemplateId == 140 || getTemplate().mobTemplateId == 161){ //Rồng băng : 140
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							//ServerEffect.addServerEffect(109, cFocus, 2);
							ServerEffect.addServerEffect(112, cFocus, 2);
							ServerEffect.addServerEffect(109, cFocus.cx - 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx + 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx - 20, cFocus.cy, 2);
							ServerEffect.addServerEffect(109, cFocus.cx + 20, cFocus.cy, 2);
						} else if (getTemplate().mobTemplateId == 141 || getTemplate().mobTemplateId == 162) { // Kỳ lân
//							if (cFocus == Char.myChar()) {
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							// ServerEffect.addServerEffect(109, cFocus, 2);
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(108, x + w / 2, y, 1);
							else
								ServerEffect.addServerEffect(108, x - w / 2, y, 1, (byte) -1);
							ServerEffect.addServerEffect(122, x, y, 1, (byte)dir);
							ServerEffect.addServerEffect(91, cFocus, 1);
						} else if (getTemplate().mobTemplateId == 144 || getTemplate().mobTemplateId == 163) { // Bí Ngô bay
//							if(cFocus == Char.myChar()){
//								GameScr.shaking = 1;
//								GameScr.count = 0;
//							}
							//ServerEffect.addServerEffect(109, cFocus, 2);
							ServerEffect.addServerEffect(112, cFocus, 2);
							ServerEffect.addServerEffect(109, cFocus.cx - 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx + 40, cFocus.cy - 40, 1);
							ServerEffect.addServerEffect(109, cFocus.cx - 20, cFocus.cy, 2);
							ServerEffect.addServerEffect(109, cFocus.cx + 20, cFocus.cy, 2);
						} else if (getTemplate().mobTemplateId == 160) { // Bí Ngô Bự
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(123, x + w / 2, y - 5, 1);
							else
								ServerEffect.addServerEffect(123, x - w / 2, y - 5, 1, (byte) -1);
							ServerEffect.addServerEffect(91, cFocus, 1);
						} else if (getTemplate().mobTemplateId == 164 || getTemplate().mobTemplateId == 165) { // Người băng
							if (cFocus.cx > x)
								ServerEffect.addServerEffect(125, x + w / 2, y, 1);
							else
								ServerEffect.addServerEffect(125, x - w / 2, y, 1, (byte) -1);
							ServerEffect.addServerEffect(90, cFocus, 1);
						}
						
						
						
					}
					else if (Res.abs(xx - x) < 24 && Res.abs(yy - y) < 15){
						cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
						isBusyAttackSomeOne = false;
					}else
					{
						if(isBoss){
//							if(getTemplate().mobTemplateId == 114 || getTemplate().mobTemplateId == 115)
//								MonsterDart.addMonsterDart(x + (dir - 1)* 15, y - 20, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							else 
								if(getTemplate().mobTemplateId == 116){
								ServerEffect.addServerEffect(84, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 138){
//								if(cFocus == Char.myChar())
//								{
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								ServerEffect.addServerEffect(83, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}
//							else if(getTemplate().mobTemplateId == 139){
//								MonsterDart.addMonsterDart(x + (dir - 1)* 30, y - 30, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							}
							else if(getTemplate().mobTemplateId == 140 || getTemplate().mobTemplateId == 161){
//								if(cFocus == Char.myChar()){
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								ServerEffect.addServerEffect(110, cFocus, 2);
								ServerEffect.addServerEffect(104, cFocus.cx - 20, cFocus.cy, 2);
								ServerEffect.addServerEffect(104, cFocus.cx + 20, cFocus.cy, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 141 || getTemplate().mobTemplateId == 162){
//								if(cFocus == Char.myChar()){
//									GameScr.shaking = 1;
//									GameScr.count = 0;
//								}
								
								ServerEffect.addServerEffect(121, cFocus, 1);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}
//							else if(getTemplate().mobTemplateId == 144 || getTemplate().mobTemplateId == 163){
//								MonsterDart.addMonsterDart(x + (dir - 1)* 15, y - 20, isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
//							}
							else if(getTemplate().mobTemplateId == 160){
								ServerEffect.addServerEffect(124, cFocus, 2);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}else if(getTemplate().mobTemplateId == 164 || getTemplate().mobTemplateId == 165){
								ServerEffect.addServerEffect(126, cFocus, 1);
								isBusyAttackSomeOne = false;
								cFocus.doInjure(dame, dameMp, isBoss, getTemplate().mobTemplateId);
							}				 										
						}
//						else
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10,  isBoss, levelBoss, getTemplate().mobTemplateId, dame, dameMp, cFocus);
						isBusyAttackSomeOne = false;
					}
				} else if (typeAtt == 1) {
					if (Res.abs(xx - x) < 24 && Res.abs(yy - y) < 15)
					{
						isBusyAttackSomeOne = false;
					}
					else
					{
//						if(isBoss)
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10 - 20, bFocus);
//						else
//							MonsterDart.addMonsterDart(x - 5, y + dir * 10, bFocus);
						isBusyAttackSomeOne = false;
					}
				}
			}
			dir = x < xx ? 1 : -1;
		} else if (p1 == 1) {
			if (arrMobTemplate[templateId].type != 0 && !isDontMove && !isIce && !isWind) {
				x += (xFirst - x) / 4;
				y += (yFirst - y) / 4;
			}
			if (Res.abs(xFirst - x) < 5 && Res.abs(yFirst - y) < 5) {
				status = MA_STANDWAIT;
				p1 = 0;
				p2 = 0;
			}
		}
	}

	public void updateMobWalk() {
		arrMobTemplate[templateId].type = 3;
		arrMobTemplate[templateId].speed = 1;
		arrMobTemplate[templateId].rangeMove = 20;
//		if(true)
//			return;
		int line = 0;
		try {
			if (injureThenDie) {
				status = Mob.MA_DEADFLY;
				p2 = injureBy.cdir << 3;
				p1 = -5;
				p3 = 0;
			}
			line = 1;
			if(isIce)
				return;
			if(isDontMove || isWind){

				Cout.println(isDontMove+" isUpdate   "+status+"  isWind "+isWind);
				frame = 0;
				return;
			}
			switch (arrMobTemplate[templateId].type) {
			case TYPE_DUNG: // dung yen
				line = 2;
				frame = 0;
				break;
			case TYPE_DI:
			case TYPE_NHAY:
			case TYPE_LET:
//				dir = 1;
				line = 3;
				byte speed = arrMobTemplate[templateId].speed;
				if (speed == 1) {
					if (GameCanvas.gameTick % 2 == 1)
						break;
				} else if (speed > 2)
					speed += mobId % 2;
				else if (GameCanvas.gameTick % 2 == 1)
					speed--;
				x += speed * dir;
				if (x > xFirst + arrMobTemplate[templateId].rangeMove)
					dir = -1;
				else if (x < xFirst - arrMobTemplate[templateId].rangeMove)
					dir = 1;
				if(!isBoss){
					updatechangeframe();
					frame = framemove[f];
					
				}
//					frame = GameCanvas.gameTick % 4 > 1 ? 1 : 2;
				else{
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				}
				break;
			case TYPE_BAY:
				line = 4;
				byte speed2 = arrMobTemplate[templateId].speed;
				speed2 += mobId % 2;
				x += speed2 * dir;
				if (GameCanvas.gameTick % 10 > 2)
					y += (speed2) * dirV;
				speed2 += (GameCanvas.gameTick + mobId) % 2;
				if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
					dir = -1;
					status = MA_STANDWAIT;
					p1 = 0;
				} else if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
					dir = 1;
					status = MA_STANDWAIT;
					p1 = 0;
				}
				if (y > yFirst + 24)
					dirV = -1;
				else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
					dirV = 1;
				if(!isBoss)
					frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
				else
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				
				break;
			case TYPE_BAY_DAU:
				line = 5;
				byte speed3 = arrMobTemplate[templateId].speed;
				speed3 += mobId % 2;
				x += speed3 * dir;
				speed3 += (GameCanvas.gameTick + mobId) % 2;
				if (GameCanvas.gameTick % 10 > 2)
					y += (speed3) * dirV;

				if (x > xFirst + arrMobTemplate[templateId].rangeMove) {
					dir = -1;
					status = MA_STANDWAIT;
					p1 = 0;
				} else if (x < xFirst - arrMobTemplate[templateId].rangeMove) {
					dir = 1;
					status = MA_STANDWAIT;
					p1 = 0;
				}
				if (y > yFirst + 24)
					dirV = -1;
				else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
					dirV = 1;
				if (TileMap.tileTypeAt(x, y, TileMap.T_TOP)) {
					if (GameCanvas.gameTick % 10 > 5) {
						y = TileMap.tileYofPixel(y);
						status = MA_STANDFLY;
						p1 = 0;
						dirV = -1;
					} else
						dirV = -1;
				}
				if(!isBoss)
					frame = GameCanvas.gameTick % 4 > 1 ? 3 : 1;
				else
					frame =  arrMobTemplate[templateId].frameBossMove[frameIndex];
				break;
			}
		} catch (Exception e) {
//			System.out.println("lineee: " + line);
		}
	}

	public MobTemplate getTemplate() {
		return arrMobTemplate[templateId];
	}

	public boolean isPaint() {
		if (x < GameScr.cmx)
			return false;
		if (x > GameScr.cmx + GameScr.gW)
			return false;
		if (y < GameScr.cmy)
			return false;
		if (y > GameScr.cmy + GameScr.gH + 30)
			return false;
		if (arrMobTemplate[templateId] == null)
			return false;
		if(!isBoss){
			if (frame >= arrMobTemplate[templateId].imgs.length)
				return false;
			if (arrMobTemplate[templateId].imgs[frame] == null)
				return false;
		}
		if (status == MA_INHELL)
			return false;
		return true;
	}

	public boolean isUpdate() {
		if (arrMobTemplate[templateId] == null){
			return false;
		}
		if (arrMobTemplate[templateId].imgs == null){
			return false;
		}
		if(!isBoss){
			if (frame >= arrMobTemplate[templateId].imgs.length){
				return false;
			}
			if (arrMobTemplate[templateId].imgs[frame] == null){
				return false;
			}
		}
		if (status == MA_INHELL){
			return false;
		}
		return true;
	}
	public void paint(MGraphics g) {
//		if (!isPaint())
//			return;
//		graphic.translate(0, GameCanvas.transY);
		MobTemplate mt = arrMobTemplate[mobId];
		if(mt.imgs[frame]!=null)
			g.drawRegion(mt.imgs[frame], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]), MGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 2 : 0, x, y - 10, StaticObj.BOTTOM_HCENTER);
//		if(!isBoss){
//			if (w == 0)
//				w = MGraphics.getImageWidth(mt.imgs[0]);
//			if (h == 0)
//				h = MGraphics.getImageHeight(mt.imgs[0]);
//		}else{
//			w = 40;
//			h = 40;
//		}
//		try {
//			if ((templateId == 98 || templateId == 99) && status == MA_DEADFLY) {
//					long now = System.currentTimeMillis();
//					if(now-timeStartDie < 400){
//						graphic.drawRegion(mt.imgs[frame], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]), MGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
//					} else if (now - timeStartDie < 800){
//						graphic.drawRegion(mt.imgs[frame], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]), 3*MGraphics.getImageHeight(mt.imgs[frame])/5, dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
//					} else if (now - timeStartDie < 1200){
//						graphic.drawRegion(mt.imgs[frame], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]), MGraphics.getImageHeight(mt.imgs[frame])/3, dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
//					}
//					if (GameCanvas.gameTick % 8 < 2)
//						SmallImage.drawSmallImage(graphic, 457, x, y, 0, StaticObj.BOTTOM_HCENTER);
//					else if (GameCanvas.gameTick % 8 < 4)
//						SmallImage.drawSmallImage(graphic, 458, x, y, 0, StaticObj.BOTTOM_HCENTER);
//					else if (GameCanvas.gameTick % 8 < 6)
//						SmallImage.drawSmallImage(graphic, 459, x, y, 0, StaticObj.BOTTOM_HCENTER);
//					
//			} else
//				if(isBoss){
//					if(getTemplate().frameBoss!=null){
//						Frame fr = getTemplate().frameBoss[frame];
//						for (int i = 0; i < fr.dx.length; i++) {
//							ImageInfo im = getTemplate().getImgInfo(fr.idImg[i]);
//
//							if(dir > 0)
//								graphic.drawRegion(getTemplate().imgs[0], im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i] - 1 , MGraphics.TOP | MGraphics.LEFT);
//							else{
//								graphic.drawRegion(getTemplate().imgs[0], im.x0, im.y0, im.w, im.h, 2, x - fr.dx[i], y + fr.dy[i] - 1, MGraphics.TOP | MGraphics.RIGHT);
//							}
//						}
//					}
//				}
//				else{
////					graphic.translate(0, GameCanvas.transY);
//					Cout.println("IMage ----> "+mt.imgs[0]);
//					graphic.drawRegion(mt.imgs[0/*frame*/], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]), MGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 0 : 2, x, y, StaticObj.BOTTOM_HCENTER);
//				}
////			
//			
//		} catch (Exception e) {
////			GameCanvas.debug("MobPaint " + templateId + " " + frame + " " + mt.imgs.length + " " + mt.imgs[frame], 1);
//		}
//		
//		int yy = y;
//		if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.equals(this) && status != MA_DEADFLY) {
//			int mx = maxHp;
//			if (mx < hp)
//				mx = hp;
//			int per = (int) (((long) hp * 100) / mx);
//			int ww = w;
//			int hh = 4;
//			if (levelBoss == 1 || levelBoss == 2 || levelBoss == 3 || isBoss) {
//				hh += hh / 2;
//				ww += ww / 2;
//			}
//			ww += 2;
//			int hpw = (ww * per) / 100;
//			if (hpw < 2)
//				hpw = 2;
//			if (status == MA_DEADFLY)
//				per = 0;
//
//			if(templateId == 140 || templateId == 160)
//				yy -= 20;
//			
//			if(templateId != 142 && templateId != 143){
//				graphic.setColor(0xFFFFFF);
//				graphic.fillRect(x - ww / 2 - 1, yy - h - 12, ww, hh);
//				graphic.setColor(getHPColor());
//				graphic.fillRect(x - ww / 2 - 1, yy - h - 12, hpw, hh);
//				graphic.setColor(0);
//				graphic.drawRect(x - ww / 2 - 1, yy - h - 12, ww, hh);
//			}
//			else{
//				SmallImage.drawSmallImage(graphic, 988, x, yy - h, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//			}
//			if (levelBoss > 0) {
//				if (levelBoss == 1)
//					mFont.tahoma_7_yellow.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
//				else if (levelBoss == 2)
//					mFont.tahoma_7_yellow.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
//				else if (levelBoss == 3)
//					mFont.tahoma_7_blue1.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 26, 2, mFont.tahoma_7_grey);
//				if (isDisable)
//					SmallImage.drawSmallImage(graphic, 494, x, yy - h - 28, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//			} else if (isDisable) {
//				SmallImage.drawSmallImage(graphic, 494, x, yy - h - 15, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//			}
//		} else if (levelBoss > 0) {
//			if (levelBoss == 1)
//				mFont.tahoma_7_yellow.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
//			else if (levelBoss == 2)
//				mFont.tahoma_7_yellow.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
//			else if (levelBoss == 3)
//				mFont.tahoma_7_blue1.drawString(graphic, mResources.BOSS[levelBoss], x, yy - h - 20, 2, mFont.tahoma_7_grey);
//			if (isDisable)
//				SmallImage.drawSmallImage(graphic, 494, x, yy - h - 22, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//		} else if (isDisable) {
//			SmallImage.drawSmallImage(graphic, 494, x, y - h - 5, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//		}
////		mFont.tahoma_7_yellow.drawString(graphic, mobId + "", x, y - h - 20, 2, mFont.tahoma_7_grey);
//		if(isDontMove){
//			if(GameCanvas.gameTick % 2 == 0)
//				SmallImage.drawSmallImage(graphic, 1082, x, y - h/2, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else
//				SmallImage.drawSmallImage(graphic, 1084, x, y - h/2, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//		}
//		if(isIce){
//			SmallImage.drawSmallImage(graphic, 290, x, y, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
//		}
//		if(isWind){
//			int type = GameCanvas.gameTick % 6;
//			if(type == 0 || type == 1)
//				SmallImage.drawSmallImage(graphic, 998, x, y - h - 5, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 2 || type == 3)
//				SmallImage.drawSmallImage(graphic, 999, x, y - h - 5, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 4 || type == 5)
//				SmallImage.drawSmallImage(graphic, 1000, x, y - h - 5, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//		}
//		if(isFire){
//			int type = GameCanvas.gameTick % 16;
//			if(type == 0)
//				SmallImage.drawSmallImage(graphic, 1013, x - w/2, y - h + h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 1)
//				SmallImage.drawSmallImage(graphic, 1014, x - w/2, y - h + h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 2)
//				SmallImage.drawSmallImage(graphic, 1015, x - w/2, y - h + h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 3)
//				SmallImage.drawSmallImage(graphic, 1016, x - w/2, y - h + h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 4)
//				SmallImage.drawSmallImage(graphic, 1013, x + w/2, y - h, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 5)
//				SmallImage.drawSmallImage(graphic, 1014, x + w/2, y - h, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 6)
//				SmallImage.drawSmallImage(graphic, 1015, x + w/2, y - h, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 7)
//				SmallImage.drawSmallImage(graphic, 1016, x + w/2, y - h, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 8)
//				SmallImage.drawSmallImage(graphic, 1013, x - w/2, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 9)
//				SmallImage.drawSmallImage(graphic, 1014, x - w/2, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 10)
//				SmallImage.drawSmallImage(graphic, 1015, x - w/2, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 11)
//				SmallImage.drawSmallImage(graphic, 1016, x - w/2, y, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 12)
//				SmallImage.drawSmallImage(graphic, 1013, x + w/2, y - h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 13)
//				SmallImage.drawSmallImage(graphic, 1014, x + w/2, y - h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 14)
//				SmallImage.drawSmallImage(graphic, 1015, x + w/2, y - h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//			else if(type == 15)
//				SmallImage.drawSmallImage(graphic, 1016, x + w/2, y - h/4, 0, MGraphics.VCENTER | MGraphics.HCENTER);
//		}
	}

	public int getHPColor() {
		if (sys <= 1)
			return 0xFF0000;
		else if (sys == 2)
			return 0x0080FF;
		else if (sys == 3)
			return 0x719563;
		return 0xFF0000;
	}

	byte[] cou = new byte[] { -1, 1 };

	public Char injureBy;

	public boolean injureThenDie = false;

	public void startDie() {
		hp = 0;
		this.timeStartDie = System.currentTimeMillis();
		if (injureBy != null) {
			injureThenDie = true;
		} else {
			injureThenDie = true;
			hp = 0;
			status = Mob.MA_DEADFLY;
			p1 = -5;
			p2 = -dir << 2;
			p3 = 0;
		}
	}

	public Mob mobToAttack;

	public void attackOtherMob(Mob mobToAttack) {
		this.mobToAttack = mobToAttack;
	}
	
	public void attackOtherInRange() {
		if (templateId == 116)
			ServerEffect.addServerEffect(84, Char.myChar(), 1);
		else if (templateId == 115)
			ServerEffect.addServerEffect(81, Char.myChar(), 1);
		else if (templateId == 138)
			ServerEffect.addServerEffect(90, Char.myChar(), 1);
		else if (templateId == 139)
			ServerEffect.addServerEffect(91, Char.myChar(), 1);
		else if (templateId == 140 || templateId == 161)
			ServerEffect.addServerEffect(110, Char.myChar(), 2);
		else if (templateId == 141 || templateId == 162)
			ServerEffect.addServerEffect(121, Char.myChar(), 1);
		else if (templateId == 144 || templateId == 163)
			ServerEffect.addServerEffect(121, Char.myChar(), 1);
		else if (templateId == 160)
			ServerEffect.addServerEffect(124, Char.myChar(), 1);
		else if(templateId == 164 || templateId == 165)
				ServerEffect.addServerEffect(126, cFocus, 1);
		
	}
	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getW() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getH() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void stopMoving() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean isInvisible() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	public static void loadImgMob(int idtemplateMob){
		if(idtemplateMob>arrMobTemplate.length-1) return;
		arrMobTemplate[idtemplateMob] = new MobTemplate();
		for(int i = 0; i < 8; i++){
			arrMobTemplate[idtemplateMob].imgs[i] = GameCanvas.loadImage("/mob/"+idtemplateMob+"/"+i+".png"); 
		}
	}
	public static int framemove[] =     {0,0,0,1,1,1,2,2,2};
	public static int frameBeAttack[] = {4,4,4,4,6,6,6,6,4,4,4,4};
	public static int frameAttack[] =   {3,3,3,3,4,4,4,4,5,5,5,5};
	 public int f = 1; 
	 public void updatechangeframe(){
	   f++;
	  if(f > framemove.length-1){
	   f = 0;
	  }
	 }
	
	public void loadDatafile(){
		
		
		
			
		
	}
	
}
