package android.content;

import java.io.InputStream;

import lib.Cout;
import lib.MyStream;

public class Asset {

	public InputStream open(String string) {
		return  MyStream.readFile(string);
	}

}
