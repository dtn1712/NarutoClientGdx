package real;

import java.util.Vector;

import com.thdgaming.naruto.MGraphics;


public class FontSys {

	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int CENTER = 2;
	public static final int RED = 0;
	
	public static final int YELLOW	= 1;
	public static final int GREEN	= 2;
	public static final int FATAL	= 3;
	public static final int MISS	= 4;
	public static final int ORANGE	= 5;
	public static final int ADDMONEY= 6;
	public static final int MISS_ME	= 7;
	public static final int FATAL_ME= 8;
	public int id, idTemp;
	public Boolean isPaintDoubleFont = false;

	// ===============================================

	public static FontSys tahoma_7b_red = new FontSys(1);
	public static FontSys tahoma_7b_blue = new FontSys(2);
	public static FontSys tahoma_7b_purple = new FontSys(3);
	public static FontSys tahoma_7b_yellow = new FontSys(4);
	public static FontSys tahoma_7b_green = new FontSys(5);

	public static FontSys tahoma_7b_white = new FontSys(7);
	public static FontSys tahoma_7 = new FontSys(8);
	public static FontSys tahoma_7_blue1 = new FontSys(9);
	public static FontSys tahoma_7_white = new FontSys(10);
	public static FontSys tahoma_7_yellow= new FontSys(11);
	public static FontSys tahoma_7_grey = new FontSys(12);
	public static FontSys tahoma_7_red = new FontSys(13);
	public static FontSys tahoma_7_blue = new FontSys(14);
	public static FontSys tahoma_7_green = new FontSys(15);
	public static FontSys tahoma_8b = new FontSys(16);
	public static FontSys number_yellow = new FontSys(17);
	public static FontSys number_red = new FontSys(18);
	public static FontSys number_green = new FontSys(19);
	public static FontSys number_white = new FontSys(20);
	public static FontSys number_orange = new FontSys(21);
	public static FontSys numberb_yellow = new FontSys(22);
	public static FontSys numberb_red = new FontSys(23);
	public static FontSys numberb_green = new FontSys(24);
	public static FontSys numberb_white = new FontSys(25);
	public static FontSys numberb_orange = new FontSys(26);

	public FontSys(int idfont) {
		
		initFontSys();

	}
	
	public static FontSys gI(int id){
		switch (id) {
			case 1: return tahoma_7b_red;
			case 2: return tahoma_7b_blue;
			case 3: return tahoma_7b_purple;
			case 4: return tahoma_7b_yellow;
			case 5: return tahoma_7b_green;
			case 6: return tahoma_7b_white;
			case 7: return tahoma_7b_white;
			case 8: return tahoma_7;
			case 9: return tahoma_7_blue1;
			case 10: return tahoma_7_white;
			case 11: return tahoma_7_yellow;
			case 12: return tahoma_7_grey;
			case 13: return tahoma_7_red;
			case 14: return tahoma_7_blue;
			case 15: return tahoma_7_green;
			case 16: return tahoma_8b;
			case 17: return number_yellow;
			case 18: return number_red;
			case 19: return number_green;
			case 20: return number_white;
			case 21: return number_orange;
			case 22: return numberb_yellow;
			case 23: return numberb_red;
			case 24: return numberb_green;
			case 25: return numberb_white;
			case 26: return numberb_orange;
		}
		return null;
	}
	public void initFontSys() {
	}
	
	
	public int getHeight() {
		int a = 15;
		return a;
	}
	
	public void setHeight(int height){
	}
	
	public int getWidth(String st) {
		return 0;
	}
	
//	public int getWidth(String st) {
//		
//		st.trim();
//		initFontSys();
//		return (int) fPaint.measureText(st)/MGraphics.zoomLevel;
//	}
	
	public void drawString(MGraphics g, String st, int x, int y, int align) {
		drawString(g, st, x, y, align, 0);
	}

	
	public void drawString(MGraphics g, String st, int x, int y, int align, int idFontSys) {
		
	}

	private void setFontColor(int color) {
		
	}

	public void drawString(MGraphics g, String st, int x, int y, int align, FontSys font) {
		
//		isPaintDoubleFont = true;
//		drawString(graphic, st, x, y+1, align, font.id);
		drawString(g, st, x, y, align);
		
	}
	
	public void drawString(MGraphics g, String st, int x, int y, int align, int idfont1, int idfont2) {
		isPaintDoubleFont = true;
		drawString(g, st, x, y+1, align, idfont1);
		drawString(g, st, x, y, align, idfont2);

	}
	
	public Vector splitFontVector(String src, int lineWidth) {
		Vector lines = new Vector();
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n'){
				lines.addElement(line);
				line = "";
			}
			else{
				line += src.charAt(i);
				if(getWidth(line) > lineWidth){
					//System.out.println(line);
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if(line.charAt(j) == ' '){
							break;
						}
					} 
					if(j < 0) j = line.length() - 1;
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if(i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}
	public String splitFirst(String str) {
		String line = "";
		boolean isSplit = false;
		for (int i = 0; i < str.length(); i++) {
			if(!isSplit){
				String strEnd = str.substring(i, str.length()); 
				if(compare(strEnd, " "))
					line += str.charAt(i) + "-";
				else
					line += strEnd;
				isSplit = true;
			}
			else{
				if (str.charAt(i) == ' ')
					isSplit = false;
			}
		}
		return line;
	}
	public String[] splitFontArray(String src, int lineWidth) {
		Vector lines = splitFontVector(src, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}
	
	public boolean compare(String strSource, String str) {
		for (int i = 0; i < strSource.length(); i++) {
			if(String.valueOf(strSource.charAt(i)).equals(str))
				return true;
		}
		return false;
	}
	
	public static String[] split(String original, String separator) {
		Vector nodes = new Vector();
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		nodes.addElement(original);
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);

			}
		}
		return result;
	}
	
	
}