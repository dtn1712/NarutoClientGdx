package real;

import java.util.Vector;

import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import lib.SysTemFont;




public class mFont {


	public SysTemFont f;
	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int CENTER = 2;
	public static final int RED=0;
	
	public static final int YELLOW		= 1;
	public static final int GREEN		= 2;
	public static final int FATAL		= 3;
	public static final int MISS		= 4;
	public static final int ORANGE		= 5;
	public static final int ADDMONEY	= 6;
	public static final int MISS_ME		= 7;
	public static final int FATAL_ME	= 8;
	private int space, id;
	private int height;
	private Bitmap imgFont;
	private String strFont;
	private int fImages[][];
	
	// ===============================================
	public static String str = " 0123456789+-*='_?.,<>/[]{}!@#$%^&*():aáàảãạâấầẩẫậăắằẳẵặbcdđeéèẻẽẹêếềểễệfghiíìỉĩịjklmnoóòỏõọôốồổỗộơớờởỡợpqrstuúùủũụưứừửữựvxyýỳỷỹỵzwAÁÀẢÃẠĂẰẮẲẴẶÂẤẦẨẪẬBCDĐEÉÈẺẼẸÊẾỀỂỄỆFGHIÍÌỈĨỊJKLMNOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢPQRSTUÚÙỦŨỤƯỨỪỬỮỰVXYÝỲỶỸỴZW";
	public static mFont tahoma_7b_red;
	public static mFont tahoma_7b_blue;
	public static mFont tahoma_7b_purple;
	public static mFont tahoma_7b_yellow;
	public static mFont tahoma_7b_green;
	public static mFont tahoma_7b_white;

	public static mFont tahoma_7;
	public static mFont tahoma_7_blue1;
	public static mFont tahoma_7_white;
	public static mFont tahoma_7_yellow ;
	public static mFont tahoma_7_grey;
	public static mFont tahoma_7_red;
	public static mFont tahoma_7_blue ;
	public static mFont tahoma_7_green;

	public static mFont tahoma_10b;
	public static mFont tahoma_8b;
	public static mFont number_yellow;
	public static mFont number_red;
	public static mFont number_green;
	public static mFont number_white;
	public static mFont number_orange;
	public static mFont numberb_yellow;
	public static mFont numberb_red ;
	public static mFont numberb_green;
	public static mFont numberb_white;
	public static mFont numberb_orange;
	
	
	public static void loadFont()
	{
		tahoma_7b_red = new mFont(str, "/font/tahoma_7b_red.png", "fonts/tahoma_7b", 0, 1);
		tahoma_7b_blue = new mFont(str, "/font/tahoma_7b_blue.png", "fonts/tahoma_7b", 0, 2);
		tahoma_7b_purple = new mFont(str, "/font/tahoma_7b_purple.png", "fonts/tahoma_7b", 0, 3);
		tahoma_7b_yellow = new mFont(str, "/font/tahoma_7b_yellow.png", "fonts/tahoma_7b", 0, 4);
		tahoma_7b_green = new mFont(str, "/font/tahoma_7b_green.png", "fonts/tahoma_7", 0, 5);
		tahoma_7b_white = new mFont(str, "/font/tahoma_7b_white.png", "fonts/tahoma_7b", 0, 7);

		tahoma_7 = new mFont(str, "/font/tahoma_7.png", "fonts/tahoma_7", 0, 8);
		tahoma_7_blue1 = new mFont(str, "/font/tahoma_7_blue1.png", "fonts/tahoma_7", 0, 9);
		tahoma_7_white = new mFont(str, "/font/tahoma_7_white.png", "fonts/tahoma_7", 0, 10);
		tahoma_7_yellow = new mFont(str, "/font/tahoma_7_yellow.png", "fonts/tahoma_7", 0, 11);
		tahoma_7_grey = new mFont(str, "/font/tahoma_7_grey.png", "fonts/tahoma_7", 0, 12);
		tahoma_7_red = new mFont(str, "/font/tahoma_7_red.png", "fonts/tahoma_7", 0, 13);
		tahoma_7_blue = new mFont(str, "/font/tahoma_7_blue.png", "fonts/tahoma_7", 0, 14);
		tahoma_7_green = new mFont(str, "/font/tahoma_7_green.png", "fonts/tahoma_7", 0, 15);

		tahoma_8b = new mFont(str, "/font/tahoma_8b.png", "fonts/tahoma_8b", -1, 16);
		number_yellow = new mFont(" 0123456789+-", "/font/number_yellow.png", "fonts/number", 0, 17);
		number_red = new mFont(" 0123456789+-", "/font/number_red.png", "fonts/number", 0, 18);
		number_green = new mFont(" 0123456789+-", "/font/number_green.png", "fonts/number", 0, 19);
		number_white = new mFont(" 0123456789+-", "/font/number_white.png", "fonts/number", 0, 20);
		number_orange = new mFont(" 0123456789+-", "/font/number_orange.png", "fonts/number", 0, 21);
		numberb_yellow = new mFont(" 0123456789+-", "/font/number_yellow.png", "fonts/number", 0, 22);
		numberb_red = new mFont(" 0123456789+-", "/font/number_red.png", "fonts/number", 0,23);
		numberb_green = new mFont(" 0123456789+-", "/font/number_green.png", "fonts/number", 0, 24);
		numberb_white = new mFont(" 0123456789+-", "/font/number_white.png", "fonts/number", 0, 25);
		numberb_orange = new mFont(" 0123456789+-", "/font/number_orange.png", "fonts/number", 0, 26);
		tahoma_10b = new mFont(str, "/font/tahoma_8b.png", "fonts/tahoma_8b", -1, 27);
		
	}
	 public static int[] colorJava = new int[]{
				0xff000000, //cho GI
		        0xfff52323, // 1
		        0xff1871bc, // 2
			    0xffffffff, // 3
				0xffffaa00, // 4
			    0xff532905, // 5
			    0xff005325, // 6
				0xffffffff, // 7
				0xff000000, // 8
				0xffffeacc, // 9
				0xffffffff, // 10
				0xffffff00, // 11
				0xff005325, // 12
				0xfff52323, // 13
			    0xff555555, // 14
				0xffff7777, // 15
		        0xffffffff, // 16
			    0xff84c729, // 17
		        0xffff0000, // 18
			    0xff7a1125, // 19
			    0xffffdf2f, // 20
				0xffe44a55, // 21
				0xff3ef0e3, // 22
				0xffff0000, // 23
				0xfff59c38, // 24
			    0xffff0000, // 25
			    0xffffffff, // 26
			    0xffffff00, // 27
		        0xff00cc00, // 28
				0xfff59c38, // 29
		        0xff637dff, // 30
		        0xffffdf2f, // 31
			};
	public mFont(String strFont, String pathImage, String pathData, int space, int id) {
		int color = colorJava[id];
		f=new SysTemFont(id, color);
		this.height = f.charHeight;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public void setHeight(int height){
		this.height = height;
	}
	
	public int getWidth(String st) {
		if(f==null) 
			return 1;
		return f.getWidth(st);
	}
	
	public void drawString(MGraphics g, String st, int x, int y, int align) {
		f.drawString(g, st, x, y, align);
	}
	public void drawString(MGraphics g, String st, int x, int y, int align, Boolean isUclip) {
		f.drawString(g, st, x, y, align,isUclip);
	}
	public void drawString(MGraphics g, String st, int x, int y, int align, mFont font) {
		if(MGraphics.zoomLevel==1){
			int pos;
			int x1;
			int len = st.length();
			if (align == 0)
				x1 = x;
			else if (align == 1)
				x1 = x - getWidth(st);
			else
				x1 = x - (getWidth(st) >> 1);
			for (int i = 0; i < len; i++) {
				pos = strFont.indexOf(st.charAt(i));
				if (pos == -1)
					pos = 0;
				if (pos > -1){
					g.drawRegion(font.imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1 + 1, y, 20);
					g.drawRegion(font.imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y + 1, 20);
					g.drawRegion(imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y, 20);
				}
				x1 += fImages[pos][2] + space;
			}
		}else{
//		 FontSys.getInstance(id).drawString(graphic, st, x, y, align, font.id, id);
			f.drawString(g, st, x, y, align,false);
		}
	}
	
	public void drawString(MGraphics g, String st, int x, int y, int align, mFont font1, mFont font2) {
		if(MGraphics.zoomLevel==1){
			int pos;
			int x1;
			int len = st.length();
			if (align == 0)
				x1 = x;
			else if (align == 1)
				x1 = x - getWidth(st);
			else
				x1 = x - (getWidth(st) >> 1);
			for (int i = 0; i < len; i++) {
				pos = strFont.indexOf(st.charAt(i));
				if (pos == -1)
					pos = 0;
				if (pos > -1){
					g.drawRegion(font1.imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1 + 1, y, 20);
					g.drawRegion(font2.imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y + 1, 20);
					g.drawRegion(imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y, 20);
				}
				x1 += fImages[pos][2] + space;
			}
		}else{
			//FontSys.getInstance().drawString(graphic, st, x, y, align, font1.id, font2.id);
		}
	}
	
	public Vector splitFontVector(String src, int lineWidth) {
		Vector lines = new Vector();
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n'){
				lines.addElement(line);
				line = "";
			}
			else{
				line += src.charAt(i);
				if(getWidth(line) > lineWidth){
					//System.out.println(line);
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if(line.charAt(j) == ' '){
							break;
						}
					} 
					if(j < 0) j = line.length() - 1;
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if(i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}
	public String splitFirst(String str) {
		String line = "";
		boolean isSplit = false;
		for (int i = 0; i < str.length(); i++) {
			if(!isSplit){
				String strEnd = str.substring(i, str.length()); 
				if(compare(strEnd, " "))
					line += str.charAt(i) + "-";
				else
					line += strEnd;
				isSplit = true;
			}
			else{
				if (str.charAt(i) == ' ')
					isSplit = false;
			}
		}
		return line;
	}
	public String[] splitFontArray(String src, int lineWidth) {
		Vector lines = splitFontVector(src, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}
	
	public boolean compare(String strSource, String str) {
		for (int i = 0; i < strSource.length(); i++) {
			if(String.valueOf(strSource.charAt(i)).equals(str))
				return true;
		}
		return false;
	}
	
	public static String[] split(String original, String separator) {
		Vector nodes = new Vector();
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index-1));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		nodes.addElement(original);
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);

			}
		}
		return result;
	}
}