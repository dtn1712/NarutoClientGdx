package real;

import java.io.DataInputStream;
import java.util.Vector;

import lib.Bitmap;
import lib.Cout;
import model.ChatManager;
import model.ChatPopup;
import model.Cmd;
import model.Command;
import model.Const;
import model.EffectData;
import model.Image;
import model.Party;
import model.SmallImage;
import model.Type_Chat;
import model.Type_Object;
import model.Type_Party;
import model.Waypoint;
import model.mResources;
import network.IMessageHandler;
import network.Message;
import screen.old.GameScr;
import screen.old.LoginScr;
import screen.old.SelectCharScr;
import Gui.Contans;
import Gui.GuiQuest;
import Gui.ShopMain;
import Gui.TradeGui;
import GuiOut.loadImageInterface;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.ChatWorld;
import Objectgame.Friend;
import Objectgame.InfoItem;
import Objectgame.Item;
import Objectgame.ItemMap;
import Objectgame.ItemOptionTemplate;
import Objectgame.ItemTemplate;
import Objectgame.ItemTemplates;
import Objectgame.Mob;
import Objectgame.MobTemplate;
import Objectgame.Npc;
import Objectgame.NpcTemplate;
import Objectgame.Quest;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.Task;
import Objectgame.TileMap;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


public class Controller implements IMessageHandler {

	protected static Controller me;
	public Message messWait;

	public static Controller gI() {
		if (me == null)
			me = new Controller();
		return me;
	}

	public void onConnectOK() {
		Cout.println("Connect ok");
	}
	
	public void onConnectionFail() {
//		if (!isTryGetIPFromWap) {
//			new Thread(new Runnable() {
//				public void run() {
//					try {
//						Thread.sleep(1000);
//						System.out.println(GameMidlet.linkGetHost);
//						HttpConnection connection = (HttpConnection) Connector.open(GameMidlet.linkGetHost);
//						connection.setRequestMethod(HttpConnection.GET);
//						connection.setRequestProperty("Content-Type", "//text plain");
//						connection.setRequestProperty("Connection", "close");
//						if (connection.getResponseCode() == HttpConnection.HTTP_OK) {
//							String str = "";
//							InputStream inputstream = connection.openInputStream();
//							int length = (int) connection.getLength();
//							if (length != -1) {
//								byte incomingData[] = new byte[length];
//								inputstream.read(incomingData);
//								str = new String(incomingData);
//							}
//							System.out.println("New ip: " + str);
//							Rms.saveIP(str);
//							Session_ME.getInstance().connect("socket://" + str);
//							isTryGetIPFromWap = true;
//							return;
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//			}).start();
//			return;
//		}
		GameCanvas.startOK(mResources.SERVER_MAINTENANCE, 8882, null);
	}
	
	public void onDisconnected() {
		GameCanvas.instance.resetToLoginScr();
	}
	
	public void onMessage(Message msg) {
		try{
			int size;
			Char c = new Char();
//			System.out.println("MESSAGE RECIVE ----> "+msg.command);

			Quest q = null;
			switch (msg.command) {
			case Cmd.QUEST:
				GuiQuest.listNhacNv.removeAllElements();

				byte typeUpdate = msg.reader().readByte(); // update
				byte typeQuest = msg.reader().readByte(); // loại nhiệm vụ
				Cout.println(typeUpdate+" MESSAGE RECIVE ---QUEST typeQuest-> "+typeQuest);
				int[] idquestupdate =null;
				String[] chuoinhacUpdate =null;
				if(typeUpdate==1){
					int nsize = msg.reader().readByte();// msg.reader().readShort();
					idquestupdate = new int [nsize];
					chuoinhacUpdate = new String[nsize];
					for (int i = 0; i < nsize; i++) {
						idquestupdate[i] = msg.reader().readShort();//idquesst

						Cout.println(i+" ---QUEST idquest update-> "+idquestupdate[i]);
						Boolean isMain = msg.reader().readBoolean();
						String name = msg.reader().readUTF();
						chuoinhacUpdate[i]= msg.reader().readUTF();
						Cout.println(i+" ---QUEST chuoinhac update-> "+chuoinhacUpdate[i]);
						String[] cat = mFont.tahoma_7_white.splitFontArray(chuoinhacUpdate[i], InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
//		                m.dos.writeUTF(info.getName());
//		                //remind khi dang lam nhiem vu(thit tuoi: 1/5)
//		                m.dos.writeUTF(info.getRemindWorking()+": "+q.getInfoWorking(p, info));
					}
				}
				else {
					if(typeQuest==Task.CAN_RECEIVE_TASK)
						Quest.listUnReceiveQuest.removeAllElements();
					else if(typeQuest==Task.COMPLETE_TASK)
						Quest.vecQuestFinish.removeAllElements();
					else if(typeQuest==Task.DOING_TASK){
						Quest.vecQuestDoing_Main.removeAllElements();
						Quest.vecQuestDoing_Sub.removeAllElements();
					}
					byte sizeQuest = msg.reader().readByte(); // size nhiệm vụ
					for(int i = 0; i < sizeQuest; i++){
						int idQuest = msg.reader().readShort(); //  id nhiệm vụ
						boolean isMain = msg.reader().readBoolean(); //  có phải nhiệm vụ chính k
						String nameQuest = msg.reader().readUTF(); // tên nhiệm vụ
						if(typeQuest == Task.CAN_RECEIVE_TASK){ // nhiệm vụ có thể nhận
							short idNPC = msg.reader().readShort(); // idNpc nhận nv
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 0;
							}
		                     // noi dung nc de nhan nhiem vu(trước khi nhận nv)
							String taklNPC = msg.reader().readUTF();// 
							byte typeQuestThis = msg.reader().readByte(); // loại nhiệm vụ này
							String shortdis = msg.reader().readUTF(); // //Câu mô tả tóm tắt nhiệm vụ phải làm(sau khi nhận nv)
							String gift = msg.reader().readUTF();// phần thưởng
							gift = gift.trim().length()==0?"0/0/0":gift;
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									typeQuestThis, shortdis,gift,nhacnv);
							Quest.listUnReceiveQuest.addElement(q);
						}else if(typeQuest == Task.COMPLETE_TASK){ // nhiệm vụ hoàn thành
							short idNPC = msg.reader().readShort(); // idNPC trả nhiệm vụ
							Cout.println(getClass(), "Quest nv hoan thanh "+idNPC);
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 1;
							}
							String taklNPC = msg.reader().readUTF(); // nội dung nói chuyện NPC
							String talknof = msg.reader().readUTF(); // thông báo và hướng dẫn trả nhiệm vụ từ server
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									talknof,nhacnv);
							Quest.vecQuestFinish.addElement(q);
						}else if(typeQuest == Task.DOING_TASK){ // nhiệm vụ đang làm
							byte typeDoingQuest = msg.reader().readByte();// loai nhiem vu 1killqua//2chuyendo//3nc
	
							String contendQuest = msg.reader().readUTF();// mo ta nhiem vu cho nay (paint trong màn hình nv)
							String supContend = msg.reader().readUTF();//get noi dung support khi dang lam nhiem vu

							Cout.println(getClass(),contendQuest+ " Quest nv daglam supContend "+supContend);
							short idNpcPay = msg.reader().readShort();// idnpc tra nhiem vu
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
							Cout.println(idNpcPay+" idNpcPay nhận nv dg lamtypeDoingQuest "+typeDoingQuest );
							for (int j = 0; j < GameScr.vNpc.size(); j++) {
								Npc npc = (Npc)GameScr.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNpcPay)
									npc.typeNV = 1;
							}
							if(typeDoingQuest == Task.TYPE_DOING_TASK_IP){// vận chuyển
								short idNpc = msg.reader().readShort();// id Npc can chuyen den 
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_COLLECT){ // thu thập
								byte total = msg.reader().readByte();// soluong item
								// id các item cần phải lấy
								short item[] = new short[total];
								// số item cần phải lấy
								short totalitem[] = new short[total];
								// số item đã dc
								short nItemGot[] = new short[total];
								for(int j = 0; j < total; j++){
									item[j] = msg.reader().readShort();//id item
									nItemGot[j] = msg.reader().readShort();// soluong đã nhặt đc 
									totalitem[j] = msg.reader().readShort();// số lượng cần nhặt
								}
	
								String giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.trim().length()==0?"0/0/0":giftforQuest;
								Cout.println("MESSAGE RECIVE ---QUEST-> NV NHAT ITEM");
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, item, totalitem, nItemGot,
										Quest.TYPE_ITEM,giftforQuest,nhacnv);
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_SKILL){//giet quai
								byte totalKill = msg.reader().readByte();
								short idMons[] = new short[totalKill];
								short totalMons[] = new short[totalKill];
								short nMonsKilled[] = new short[totalKill];
								for(int k = 0; k < totalKill; k++){
									idMons[k] = msg.reader().readShort(); // id quái
									nMonsKilled[k] =msg.reader().readShort(); // số lượng đã giết
									totalMons[k] =msg.reader().readShort(); // số lượng cần giết
								}
	
								String giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.trim().length()==0?"0/0/0":giftforQuest;
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, idMons, totalMons,
										nMonsKilled, Quest.TYPE_MONSTER,giftforQuest,nhacnv);
							}
							Cout.println(" nv dang lam "+q);
							if (q != null) {
								if (isMain) {
									Cout.println(" nv dang lam adđ  "+q);
									Quest.vecQuestDoing_Main.addElement(q);
								} else {
									Cout.println(" nv dang lam adđ 222 "+q); 
									Quest.vecQuestDoing_Sub.addElement(q);
								}
							}
						}
					}
				}
				if(InfoItem.wcat==-1) 
					InfoItem.wcat =  10* MGraphics.getImageWidth(loadImageInterface.imgChatConner)-10;
				for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
					Quest qe =  (Quest)Quest.listUnReceiveQuest.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.CAN_RECEIVE_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Sub.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Sub.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestFinish.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestFinish.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.COMPLETE_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				break;
			case Cmd.SKILL_CHAR:
				int sizeKill = msg.reader().readShort();
				for(int i = 0; i < sizeKill; i++){
					byte id =msg.reader().readByte();
					byte idIcon = msg.reader().readByte();
					String name = msg.reader().readUTF();
					SkillTemplate skilltemplate = new SkillTemplate(id, name, idIcon);
					SkillTemplates.add(skilltemplate); // làm giống như itemtemplate
					
					
				}
				break;
			case Cmd.REQUEST_SHOP:
				ShopMain.idItemtemplate = null;
				byte menuId = msg.reader().readByte();
				int itemlistSize = msg.reader().readShort();
				ShopMain.indexidmenu = menuId;
				ShopMain.idItemtemplate = new int[itemlistSize];
				for(int i = 0; i < itemlistSize; i++ ){
//					int idTemplate = msg.reader().readShort();
//					System.out.println("IDtem ---> "+idTemplate);
					
					ShopMain.idItemtemplate[i] = msg.reader().readShort();
				}
				ShopMain.getItemList(ShopMain.indexidmenu,ShopMain.idItemtemplate);
				break;
			case Cmd.CMD_DYNAMIC_MENU:
				Cout.println(getClass(), " nhan ve list tabshop");
				byte menuShopsize = msg.reader().readByte(); // soluong menu shop
				ShopMain.idMenu = new int[menuShopsize];
				ShopMain.nameMenu = new String[menuShopsize];
				ShopMain.dis = new String[menuShopsize];
				ShopMain.cmdShop = new Command[menuShopsize];
//				ShopMain.idItemtemplate = new int[menuShopsize];
				for(int i = 0; i < menuShopsize; i++){
					byte idmenu = msg.reader().readByte();
					ShopMain.idMenu[i] = idmenu;
					String menuname = msg.reader().readUTF();
					ShopMain.nameMenu[i] = menuname;
					String dis = msg.reader().readUTF();
					ShopMain.dis[i] = dis;
				}
				break;
			case Cmd.TRADE:
				byte typeTrade = msg.reader().readByte();

				Cout.println(" typeTrade ");
				if(typeTrade == Contans.INVITE_TRADE){ // moi giao dich 
					short CharIDpartner = msg.reader().readShort();
					Char.myChar().partnerTrade = null;
					Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);
					Cout.println(CharIDpartner+ " moigiao dich "+Char.myChar().partnerTrade);
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.gI().cmdAcceptTrade, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeTrade == Contans.ACCEPT_INVITE_TRADE){ // dong y giao dich 
					short CharIDpartner = msg.reader().readShort();
					if(Char.myChar().partnerTrade==null)
						Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();

					Cout.println(getClass(), CharIDpartner+" moigiao dich2 "+Char.myChar().partnerTrade);
//					
//					GameScr.getInstance().guiMain.moveClose=false;
//					indexpICon = Contans.ICON_TRADE;
//					trade=new TradeGui(GameCanvas.wd6-20, 20);
//					trade.SetPosClose(cmdClose);
//					paintButtonClose=true;
//					MenuIcon.lastTab.add(""+Contans.ICON_TRADE);
					Service.gI().requestinventory();
					//isPaintTrade = true;
				}else if(typeTrade == Contans.MOVE_ITEM_TRADE){
					byte typeItem = msg.reader().readByte(); // loại
					byte typeItemmove = msg.reader().readByte(); // đưa xuống hay đưa lên 
					short charId = msg.reader().readShort();
//					if(Char.myChar().partnerTrade==null){
//						Cout.println(charId+ " moigiao dich222 "+Char.myChar().partnerTrade);
//						Char.myChar().partnerTrade= GameScr.findCharInMap(charId);
//					}
					if(typeItemmove == 0){ // add item
						if(charId  == Char.myChar().charID){ // add vao danh sach item giao dịch của mình
//							Char.myChar().ItemMyTrade = new Item[8];
							for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
							
								if(Char.myChar().ItemMyTrade[i] == null){
									Cout.println(i+" nulll2 "+Char.myChar().ItemMyTrade[i]);
									Char.myChar().ItemMyTrade[i] = new Item();
									short IdItem = msg.reader().readShort();
									Char.myChar().ItemMyTrade[i].itemId = IdItem;
									short iditemTemplate = msg.reader().readByte();
									if(iditemTemplate != -1){
										Char.myChar().ItemMyTrade[i].template = ItemTemplates.get(iditemTemplate);
									}

									Cout.println(i+" nulll2 "+Char.myChar().ItemMyTrade[i]);
									byte optionsize = msg.reader().readByte();
									for (int j = 0; j < optionsize; j++) {
										String info = msg.reader().readUTF();
										byte colorname = msg.reader().readByte();
									}
									break;
								}
							}
						}else{
							for(int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++){ // add vào danh sách item của parner
								if(Char.myChar().partnerTrade.ItemParnerTrade[i] == null){
									//System.out.println("ADD ITEM PARNER TRADE");
									Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
									short IdItem = msg.reader().readShort();
									Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = IdItem;
									short iditemTemplate = msg.reader().readByte();
									if(iditemTemplate != -1){
										Char.myChar().partnerTrade.ItemParnerTrade[i].template = ItemTemplates.get(iditemTemplate);
									}
									byte optionsize = msg.reader().readByte();
									for (int j = 0; j < optionsize; j++) {
										String info = msg.reader().readUTF();
										byte colorname = msg.reader().readByte();
									}
									break;
								}
							}
						}
					}else{ // remove item
						Cout.println(getClass(),Char.myChar().charID+ "remove item "+charId);
						if(Char.myChar().charID == charId){
							Cout.println(getClass(), "remove Me item ");
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
								if(Char.myChar().ItemMyTrade[i] != null){
									if(Char.myChar().ItemMyTrade[i].itemId == IdItem){
										Cout.println(getClass(),i+ "remove item null");
										Char.myChar().ItemMyTrade[i] = null;
										GameCanvas.gameScr.guiMain.menuIcon.trade.indexSelect1 = -1;
										break;
									}
								}
							}
						}else{
							Cout.println(getClass(), "remove parner item ");
//							Char.partnerTrade.ItemParnerTrade = new Item[15]; 
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++){
								if(Char.myChar().partnerTrade.ItemParnerTrade[i] != null){
									if(Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == IdItem){
										Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
//										TradeGui.indexSelect1 = -1;
									}
									
								}
							}
						}
					}
					
					
				}else if(typeTrade == Contans.CANCEL_TRADE){
					short charidP = msg.reader().readShort();
					Char charPar = GameScr.findCharInMap(charidP);
					Cout.println(" CANCEL_TRADE ");
					GameCanvas.gameScr.guiMain.menuIcon.trade=null;
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.startOKDlg(charPar.cName+(msg.reader().readUTF()));
					//GameScr.getInstance().tradeGui = null;
				}else if(typeTrade == Contans.LOCK_TRADE){
					short charidP = msg.reader().readShort();
//					Char charPar = GameScr.findCharInMap(charidP);
					if(Char.myChar().charID != charidP){

						Cout.println(" LOCK_TRADE other ");
						GameCanvas.gameScr.guiMain.menuIcon.trade.block2 = true;
					}else{

						Cout.println(" LOCK_TRADE me ");
						GameCanvas.gameScr.guiMain.menuIcon.trade.block1 = true;
					}

				}else if(typeTrade == Contans.TRADE){
					short charidP = msg.reader().readShort();
					Char cTrade = GameScr.findCharInMap(charidP);
					GameCanvas.startYesNoDlg(cTrade.cName+msg.reader().readUTF(), TradeGui.cmdTradeEnd, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
					//GameScr.getInstance().tradeGui = null;
					for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
				}else if(typeTrade == Contans.END_TRADE){
					Cout.println(" end trade ");
					for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
					Char.myChar().partnerTrade = null;
					GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScr.guiMain.menuIcon.trade = null;
				}
				break;
			case Cmd.FRIEND:
				byte typeFriend = msg.reader().readByte();
				if(typeFriend == Friend.INVITE_ADD_FRIEND){
					Char.myChar().idFriend = msg.reader().readShort();
					String tempDebug = msg.reader().readUTF();
					//System.out.println("IDfiend ----> "+Char.myChar().idFriend+"  "+tempDebug);
					GameCanvas.startYesNoDlg(tempDebug, GameScr.gI().cmdComfirmFriend, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeFriend == Friend.ACCEPT_ADD_FRIEND){
					short CharIDFriend = msg.reader().readShort();
					c = GameScr.findCharInMap(CharIDFriend);
					Char.myChar().vFriend.addElement(c);
				}else if(typeFriend == Friend.REQUEST_FRIEND_LIST){
					Char.myChar().vFriend.removeAllElements();
					byte sizeFriendList = msg.reader().readByte();
					for(int i = 0; i < sizeFriendList; i++){
						Char ch = new Char();
						ch.isOnline = msg.reader().readBoolean();
						if(ch.isOnline){
							ch.charID = msg.reader().readShort(); // không online defaut là 0							
						}
						ch.CharidDB = msg.reader().readShort();
						ch.cName = msg.reader().readUTF();
						ch.clevel = msg.reader().readShort();
						Char.myChar().vFriend.addElement(ch);
					}
				}else if(typeFriend == Friend.UNFRIEND){
					short CharID = msg.reader().readShort();
					short charUser = msg.reader().readShort();
				//	System.out.println("Unfriend -----> "+CharID);
					for(int i = 0; i < Char.myChar().vFriend.size(); i++){
						Char chardel = (Char) Char.myChar().vFriend.elementAt(i);
						if(chardel.charID != 0){ // khi online 
							if(chardel.charID == CharID)
								Char.myChar().vFriend.removeElementAt(i);		
						}else{
							if(chardel.CharidDB == charUser){ // khi off line

								Char.myChar().vFriend.removeElementAt(i);		
							}
						}
					}
				}
				break;
			case Cmd.PARTY:
				byte typeParty = msg.reader().readByte();
				if(typeParty == Type_Party.INVITE_PARTY){
					Party.gI().charId = msg.reader().readShort();
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.gI().cmdAcceptParty, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeParty == Type_Party.GET_INFOR_PARTY){
					short PartyId = msg.reader().readShort();
					short CharLeaderid = msg.reader().readShort();
					byte membersize = msg.reader().readByte();
					short[] charIDmeber =  new short[membersize];
					for(int i = 0; i < membersize; i++){
						charIDmeber[i] = msg.reader().readShort();
					}
					GameScr.hParty.contains(PartyId+"");
					GameScr.hParty.put(PartyId+"",(Object)new Party(PartyId, charIDmeber, CharLeaderid));
				}else if(typeParty == Type_Party.OUT_PARTY){
					short IdChar = msg.reader().readShort();
					Char chaRe = GameScr.findCharInMap(IdChar);
					Party party = null;
					if(chaRe != null){
						party = (Party) GameScr.hParty.get(chaRe.idParty+"");
						for(int i = 0; i < party.vCharinParty.size();i++){
							Char charRemove = (Char) party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
								party.vCharinParty.removeElementAt(i);
							}
							
						}
						
						
					}else{
						chaRe = Char.myChar();
						party = (Party) GameScr.hParty.get(chaRe.idParty+"");
						for(int i = 0; i < party.vCharinParty.size();i++){
							Char charRemove = (Char) party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
								party.vCharinParty.removeElementAt(i);
							}
							
						}
					}
					if(party.vCharinParty.size() <= 0)
						GameScr.hParty.remove(chaRe.idParty+"");
//					for(int i = 0; i < GameScr.vParty.size(); i++){
//						Party party = (Party) GameScr.vParty.elementAt(i);
//						for(int j = 0; j < party.vCharinParty.size(); j++){
//							Char charre = (Char) party.vCharinParty.elementAt(j);
//							if(charre.charID == IdChar)
//								party.vCharinParty.removeElement(charre);
//							
//						}
//						if(party.vCharinParty.size() <= 1);
//					}
				}else if(typeParty == Type_Party.DISBAND_PARTY){
					//System.out.println("DISBAND_PARTY");
					short idParty = msg.reader().readShort();
					//System.out.println("ID Party ---> "+idParty);
					Party party = (Party) GameScr.hParty.get(idParty+"");
					
					for(int i = 0;i < party.vCharinParty.size();i++){
						Char ch = (Char)party.vCharinParty.elementAt(i);
						ch.idParty = -1;
						ch.isLeader = false;
						party.vCharinParty.removeElementAt(i);
					}
//					part.vCharinParty.removeAllElements();
					GameScr.hParty.remove(idParty+"");
				}else if(typeParty == Type_Party.GET_INFOR_NEARCHAR){
					GameScr.charnearByme.removeAllElements();
					byte sizeCharlist = msg.reader().readByte();
					//System.out.println("Size ----> "+sizeCharlist);
					for(int i = 0; i < sizeCharlist; i++){
						Char ch = new Char();
						ch.charID = msg.reader().readShort();
						//System.out.println("IDChar ----> "+ch.charID);
						ch.cName = msg.reader().readUTF();
						//System.out.println("CharName ----> "+ch.cName);
						ch.idParty = msg.reader().readShort();
						//System.out.println("Idparty ----> "+ch.idParty);
						GameScr.charnearByme.addElement(ch);
					}
				}
				break;
			case Cmd.MENU_NPC:
				Npc npc = null;
				for (int i = 0; i < GameScr.vNpc.size(); i++) {
					npc = (Npc) GameScr.vNpc.elementAt(i);
//					if (npc.template.npcTemplateId == ncpTemplateId && npc.equals(Char.myChar().npcFocus)) {
//					for(int j = 0; j < npc.template.menu.length; j++)
//						ChatPopup.addChatPopupMultiLine(npc.template.contendChat, 1000, npc);
//						break;
//					}
				}
				Vector vmenu = new Vector();
				try {
					int sizee = msg.reader().readByte();
					GameScr.showChatPopup(npc.template.contendChat, 0, null, 0, null);
					final String[] arrSub = new String[sizee/*msg.reader().readByte()*/];
					for (int i = 0; i < sizee; i++) {
//						for (int j = 0; j < arrSub.length; j++) {
							arrSub[i] = msg.reader().readUTF();
//						}
						vmenu.addElement(new Command(arrSub[i], GameCanvas.instance,88820,arrSub));
					}
//					GameCanvas.startYesNoDlg(npc.template.contendChat, 0, null, 0, null, arrSub[0], arrSub[1]);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (Char.myChar().npcFocus == null)
					return;
				GameCanvas.menu.startAt(vmenu, 3);
				break;
			case Cmd.NPC:
				byte sizeNpc = msg.reader().readByte();
				for(int i = 0; i < sizeNpc; i++){
					short idNpc = msg.reader().readShort();
					short npcX = msg.reader().readShort();
					short npcY = msg.reader().readShort();
					short npcIdtemplate = msg.reader().readShort();
					Cout.println("npcIdtemplateeeeeeeeeeeeee  "+npcIdtemplate);
					short npciDicon = 0;//msg.reader().readShort();
					Npc npcz = new Npc(npcX, npcY, npcIdtemplate, npciDicon);
					npcz.npcId = npcIdtemplate;
					for (int j = 0; j < Quest.listUnReceiveQuest.size(); j++) {
						Quest q1 = (Quest)Quest.listUnReceiveQuest.elementAt(j);
						if(q1.idNPC_From==npcIdtemplate)
							npcz.typeNV = 0;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Main.size(); j++) {
						Quest q2 = (Quest)Quest.vecQuestDoing_Main.elementAt(j);
						if(q2.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Sub.size(); j++) {
						Quest q3 = (Quest)Quest.vecQuestDoing_Sub.elementAt(j);
						if(q3.idNPC_From==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestFinish.size(); j++) {
						Quest q4 = (Quest)Quest.vecQuestFinish.elementAt(j);
						if(q4.idNPC_From==npcIdtemplate||q4.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					GameScr.vNpc.addElement(npcz);
					
				}
				
				break;
			case Cmd.PICK_REMOVE_ITEM:
				byte typeItemPick = msg.reader().readByte();
				short idItemPick = msg.reader().readShort();
				if(Char.myChar().itemFocus!=null&&Char.myChar().itemFocus.itemMapID==idItemPick){
					Char.myChar().itemFocus = null;
					Char.myChar().clearFocus(10);
				}
				short idPlayerPick = msg.reader().readShort();
				c = GameScr.findCharInMap(idPlayerPick);
				if(c == null){
					c = Char.myChar();
				}
				for (int i = 0; i < GameScr.vItemMap.size(); i++) {
					ItemMap itempick = (ItemMap) GameScr.vItemMap.elementAt(i);
					itempick.setPoint(c.cx, c.cy);
					if (itempick.itemMapID == idItemPick) {
						GameScr.vItemMap.removeElementAt(i);
						break;
					}
				}
				
				break;
			case Cmd.DROP_ITEM:
				Cout.println("NHAN VE ITEM ");
				byte typeDrop = msg.reader().readByte();
				short IdMonterDie = msg.reader().readShort();
//				short iconID = msg.reader().readShort();
				short id = msg.reader().readShort();
				short idTemplate = msg.reader().readShort();
				short xItem = msg.reader().readShort();
				//System.out.println("XItem ----> "+xItem);
				short yItem = msg.reader().readShort();
				//System.out.println("YItem ----> "+yItem);
				ItemMap itemDrop = new ItemMap(id, idTemplate, xItem, yItem);
				itemDrop.type = typeDrop;
				//.template = new ItemTemplate(id,typeItemdrop , name, iconID);
				GameScr.vItemMap.addElement(itemDrop);
				Cout.println("ADD ITEM MAP SIZE "+GameScr.vItemMap.size());
				break;
			case Cmd.REMOVE_TARGET:
				
				byte typeRemove = msg.reader().readByte();
				Mob mobTargetRe = null;
				if(typeRemove == Type_Object.CAT_MONSTER){
					short idMobRetaget = msg.reader().readShort();
					for(int i = 0; i < GameScr.vMob.size(); i++){
						Mob	mobTg = (Mob) GameScr.vMob.elementAt(i);
						if(mobTg.mobId == idMobRetaget)
							mobTargetRe = mobTg;
//						if(mobTargetRe.cFocus != null){
//							mobTargetRe.cFocus = null;
//						}
					}
				}
				break;
				
			case Cmd.DIE:
				byte byteDie = msg.reader().readByte(); // 
				Cout.println("BYTE DIE ----> "+byteDie);
				if(byteDie == Type_Object.CAT_PLAYER){
					short playerID = msg.reader().readShort();
					c = GameScr.findCharInMap(playerID);
					if(c != null)
						c.statusMe = Char.A_DEADFLY;
					
				}else if(byteDie == Type_Object.CAT_MONSTER){
					short mobID = msg.reader().readShort();
	
					if(Char.myChar().mobFocus.mobId == mobID)
						Char.myChar().mobFocus.status = Mob.MA_DEADFLY;
				}
				
				break;
			case Cmd.ATTACK:
				byte typeAtt = msg.reader().readByte(); // 0 PLAYER 1 MONTERS
				if(typeAtt == 0){// PLAY
					byte Cat_type = msg.reader().readByte(); // 0 playerAtt monters 1 player player
					short PlayerAttID = msg.reader().readShort();
					 c = GameScr.findCharInMap(PlayerAttID);
				     if (c == null)
					      c = Char.myChar();
					short mobvictim = msg.reader().readShort();
					for(int i = 0; i < GameScr.vMob.size(); i++){
						Mob mob = (Mob) GameScr.vMob.elementAt(i);
						if(mob.mobId == mobvictim)
							c.mobFocus = mob;
						if(mobvictim == mob.mobId){
							c.mobFocus.hp = msg.reader().readInt();
							c.cdame = msg.reader().readInt();
							GameScr.startFlyText("-" +c.cdame, c.mobFocus.x, c.mobFocus.y-2*mob.h-5, 0, -2, mFont.RED);
//							ServerEffect.addServerEffect(91,  c.mobFocus.x, c.mobFocus.y, 1);
							if(PlayerAttID!=Char.myChar().charID){
								c.mobFocus.setInjure();
								c.mobFocus.injureBy = c;
								c.mobFocus.status = Mob.MA_INJURE;
							}
							if(mob.hp <= 0){
								Char.myChar().mobFocus.status = Mob.MA_DEADFLY;
							}
						}
					}
					
					//paint skill for all char
				    
//				     if ((TileMap.tileTypeAtPixel(c.cx, c.cy) & TileMap.T_TOP) == TileMap.T_TOP)
//				      c.setSkillPaint(GameScr.sks[2],
//				        Skill.ATT_STAND);
//				     else
//				      c.setSkillPaint(GameScr.sks[2],
//				        Skill.ATT_FLY);
					
				}
				else if(typeAtt == 1){
					short mobAttId = msg.reader().readShort();
					//Cout.println(getClass(),mobAttId+ "  monster attack "+Char.myChar().cHP);
					Mob mob = null;
					for(int i = 0; i < GameScr.vMob.size(); i++){
						Mob mobs = (Mob)GameScr.vMob.elementAt(i);
						if(mobs.mobId==mobAttId)
							mob = mobs;
						
					}
					short CharIdVictim = msg.reader().readShort();
				//	System.out.println(Char.myChar().charID+"  Char ----> "+CharIdVictim);
					mob.cFocus = GameScr.findCharInMap(CharIdVictim);
					if(mob.cFocus == null){
						mob.f = -1;
						mob.cFocus = Char.myChar();
						mob.dir = (mob.x-mob.cFocus.cx>0?-1:1);
						mob.cFocus.cHP = msg.reader().readInt();
						mob.dame = msg.reader().readInt();
						mob.status = Mob.MA_ATTACK;
						if(mob.status == Mob.MA_ATTACK){
							//ServerEffect.addServerEffect(91, mob.cFocus.cx, mob.cFocus.cy, 1);
							//Cout.println(getClass(),"   add dame");
							GameScr.startFlyText("-" +mob.dame, mob.cFocus.cx, mob.cFocus.cy-60, 0, -3, mFont.RED);
							//mob.cFocus.statusMe = Char.A_INJURE;						
						}
					}
//					if(mob.cFocus != null){
//						
//					}
				}
				break;
			case Cmd.REQUEST_MONSTER_INFO:
				//for(int i = 0; i < GameScr.vMob.size(); i++){
					//Mob modInMap = (Mob) GameScr.vMob.elementAt(i);
					if(Char.myChar().mobFocus !=  null){
						
						Char.myChar().mobFocus.mobId = msg.reader().readShort();
						Char.myChar().mobFocus.mobName = msg.reader().readUTF();
						Char.myChar().mobFocus.maxHp = msg.reader().readInt();
						Char.myChar().mobFocus.hp = msg.reader().readInt();	
					}
				//}
				break;
			case Cmd.CHAT:
				byte typeChat = msg.reader().readByte();
				if(typeChat == Type_Chat.CHAT_MAP){ // char map
					
					short useriD = msg.reader().readShort();
					String text = msg.reader().readUTF();
					if (Char.myChar().charID == useriD)
						c = Char.myChar();
					else
						c = GameScr.findCharInMap(useriD);
					if (c == null)
						return;
					ChatPopup.addChatPopup(text, 100, c);
					ChatManager.gI().addChat(mResources.PUBLICCHAT[0], c.cName, text);
					
				}else if(typeChat == Type_Chat.CHAT_WORLD){
					
					ChatWorld.ChatWorld(msg);
					
				}else if(typeChat == Type_Chat.CHAT_FRIEND){
					
					ChatPrivate.ChatFriend(msg);
				}
				break;
			case Cmd.GET_ITEM_INVENTORY:
				//Char.myChar().arrItemBag = null;
				byte byteInventory = msg.reader().readByte();
				Char.myChar().xu = msg.reader().readLong();
				Char.myChar().luong = msg.reader().readInt();
				Char.myChar().arrItemBag = new Item[msg.reader().readByte()];
				for (int i = 0; i < Char.myChar().arrItemBag.length; i++) {
				byte typeItem = msg.reader().readByte();
				short idItem = msg.reader().readShort();
				short itemTemplateId = msg.reader().readShort();
					if (itemTemplateId != -1) {
						Char.myChar().arrItemBag[i] = new Item();
						Char.myChar().arrItemBag[i].itemId = idItem;
						Char.myChar().arrItemBag[i].typeUI = Item.UI_BAG;
						Char.myChar().arrItemBag[i].indexUI = i;
						Char.myChar().arrItemBag[i].template = ItemTemplates.get(itemTemplateId);
					}
				}
				break;
			case Cmd.PLAYER_REMOVE:
				short CharIDRemove = msg.reader().readShort();

				Cout.println("PlayerOut mycharr PLAYER_REMOVE " +Char.myChar().charID);
				for(int i = 0; i < GameScr.vCharInMap.size(); i++){
					c = (Char) GameScr.vCharInMap.elementAt(i);
					if(CharIDRemove == c.charID){
						GameScr.vCharInMap.removeElementAt(i);
					}
				}
				break;
			case Cmd.PLAYER_INFO:
				
				short charID = msg.reader().readShort();
				for(int i = 0; i < GameScr.vCharInMap.size(); i++){
					Char ch = (Char) GameScr.vCharInMap.elementAt(i);
					if(ch.charID == charID){
						ch.cMaxHP = msg.reader().readShort();
						ch.cHP = msg.reader().readShort();
						if(ch.cMaxHP<=0) ch.cMaxHP = (ch.cHP<=0?1:ch.cHP);
					}
				}
//				c.charID = msg.reader().readShort();
//				System.out.println("Charinmap ----> "+c.charID);
//				c.cMaxHP = msg.reader().readShort();
//				c.cHP = msg.reader().readShort();

//				GameScr.vCharInMap.addElement(c);
				break;
			case Cmd.REQUEST_IMAGE:// yeu cau hinh tu server
				byte typeImage = msg.reader().readByte();
				if(typeImage == 0){// big image
				byte length = msg.reader().readByte(); // so luong anh big
			//	System.out.println("IMAGE BIG LENGHT ----> "+length);
				SmallImage.imgbig = new Bitmap[length];
					for(int i = 0 ; i < length; i++){
//						int idImage = msg.reader().readInt();
//						System.out.println("idImage ---- > " +idImage);
						SmallImage.imgbig[i] =  createImage(Util.readByteArray(msg));
//						SmallImage.imgNew.put(idImage+"", createImage(Util.readByteArray(msg)));	
					}
				}else if(typeImage == 1){ // small Image
					int idImage = msg.reader().readInt();
					//System.out.println("id ----> "+idImage);
					idImage -= 10000;
					SmallImage.imgNew.put(idImage+"", createImage(Util.readByteArray(msg)));
				}
				break;
			case Cmd.PLAYER_MOVE: // char khac move lai kt
				byte typemove = msg.reader().readByte();
				try {
				if(typemove == Const.CAT_MONSTER){
					
				}else if(typemove == Const.CAT_PLAYER){
					
					int charId = msg.reader().readShort();
					for (int i = 0; i < GameScr.vCharInMap.size(); i++)
					{						
						c = (Char) GameScr.vCharInMap.elementAt(i);				
						if (c.charID == charId)
						{
							c.cxMoveLast = msg.reader().readShort();
							c.cyMoveLast = msg.reader().readShort();
							c.cName = msg.reader().readUTF();
							
							c.moveTo(c.cxMoveLast, c.cyMoveLast);						
							c.lastUpdateTime = System.currentTimeMillis();
						}
					}
				}
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				break;
			case Cmd.CHANGE_MAP:
					Cout.println("CHANGE MAP");
					GameScr.vNpc.removeAllElements();
					TileMap.vGo.removeAllElements();
					BgItem bgi = new BgItem();
					EffectData effdata = new EffectData();
//					effdata.readData("/map/data");
					Char.myChar().ischangingMap = true;
					GameScr.vCharInMap.removeAllElements();
					GameScr.vMob.removeAllElements();
					GameScr.vItemMap.removeAllElements();
					TileMap.mapID = msg.reader().readShort();
					TileMap.mapName = (String)TileMap.listNameAllMap.get(TileMap.mapID+"");
					//Cout.println("Tile MapID ----> "+TileMap.mapID);
					TileMap.zoneID = msg.reader().readByte();
					short vsize = msg.reader().readShort();
					//Cout.println("SIZE LINK MAP -----> "+vsize);
					for (int i = 0; i < vsize; i++){
						TileMap.vGo.addElement(new Waypoint(msg.reader().readShort(),msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort()));
					}
					Char.myChar().cx = msg.reader().readShort();
					Char.myChar().cy = msg.reader().readShort() - 10;
					TileMap.tileID = msg.reader().readByte();
					System.out.println("TILE MAP ----> "+TileMap.tileID);
					// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					byte nTile = msg.reader().readByte();
					System.out.println("TILE MAP nTile ----> "+nTile);
					TileMap.tileIndex = new int[nTile][][];
					TileMap.tileType = new int[nTile][];
//					bgi.loadMapItem();
				
					//bgi.loadMaptable();
					//bgi.loadImgmap();

					for (int i = 0; i < nTile; i++) {
						byte nTypeSize = msg.reader().readByte();
						TileMap.tileType[i] = new int[nTypeSize];
						TileMap.tileIndex[i] = new int[nTypeSize][];
						for (int a = 0; a < nTypeSize; a++) {
							TileMap.tileType[i][a] = msg.reader().readInt();
							byte sizeIndex = msg.reader().readByte();
							TileMap.tileIndex[i][a] = new int[sizeIndex];
							for (int b = 0; b < sizeIndex; b++) {
								TileMap.tileIndex[i][a][b] = msg.reader()
										.readByte();
							}
						}
					}
					TileMap.loadimgTile(TileMap.tileID); // load hinh tile
					TileMap.loadMapfile(TileMap.mapID); // load file map
					TileMap.loadMap(TileMap.tileID); // load va cham 
					
					GameScr.loadMapItem(); // load toan bo file data object
					GameScr.loadMapTable(TileMap.mapID); // object trong 1 map filedata
					
					byte sizeMod = msg.reader().readByte();
				//	System.out.println("SIZE MOB ---> "+sizeMod);
					Mob.arrMobTemplate = new MobTemplate[sizeMod];
					for(int i = 0; i < Mob.arrMobTemplate.length; i++){
						Mob.arrMobTemplate[i] = new MobTemplate();
//						Mob.arrMobTemplate[i].imgs = null;
						byte idModtemp = msg.reader().readByte();
						short idloadmob = msg.reader().readShort();
						Mob.idloadimage = idloadmob;
//						Mob.arrMobTemplate[i].data = new EffectData(); // doc file data quai trong tool
//						Mob.arrMobTemplate[i].data.readData("/x"+MGraphics.zoomLevel+"/mob/"+idloadmob+"/data");
//						effdata.readData("/x"+MGraphics.zoomLevel+"/mob/"+idloadmob+"/data");
						Mob.loadImgMob(idloadmob);
//						Mob.arrMobTemplate[i].data.img = Mob.loadImgMob(idloadmob);
						
						Mob mob = new Mob(idModtemp, msg.reader().readInt(), msg.reader().readByte(),msg.reader().readInt(),
								msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(),msg.reader().readShort(),
								msg.reader().readShort());
						
						GameScr.vMob.addElement(mob);
					}
					
//					Npc.arrNpcTemplate = new NpcTemplate[msg.reader().readByte()];
//
//					for (byte i = 0; i < Npc.arrNpcTemplate.length; i++) {
//						Npc.arrNpcTemplate[i] = new NpcTemplate();
//						Npc.arrNpcTemplate[i].npcTemplateId = i;
//						Npc.arrNpcTemplate[i].name = msg.reader().readUTF();
//						Npc.arrNpcTemplate[i].headId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].bodyId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].legId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].contendChat = msg.reader().readUTF();
////						Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
////						for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
////							Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
////							for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
////								Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
////						}
//					}
				
					
					
//					GameScr.getInstance().loadGameScr();
					Service.gI().requestinventory();
					if(GameCanvas.currentScreen != GameCanvas.gameScr){
						GameCanvas.gameScr =  new GameScr();
						GameCanvas.gameScr.switchToMe();// mo man hình gamescr
					}
					Char.myChar().ischangingMap = false;
					Char.myChar().statusMe = Char.A_FALL;
					GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
					GameCanvas.endDlg();
			
				break;
			case Cmd.ITEM:
				try{

					GameScr.currentCharViewInfo = Char.myChar();
					byte type = msg.reader().readByte();
					Cout.println(type+ " PARTTTTTTTTTT BASIC -----ITEM--> "+ItemTemplate.ITEM_TEMPLATE);
					if(type == ItemTemplate.ITEM_TEMPLATE){
						short nItemTemplate = msg.reader().readShort(); // số template ID
						for(int i = 0; i < nItemTemplate; i++){
							short templateID = msg.reader().readShort(); // id temp
							byte typeItem = msg.reader().readByte(); // loai
							byte gender = msg.reader().readByte(); //  gioi tinh
							String name = msg.reader().readUTF(); // ten 
							String des = msg.reader().readUTF(); // mo ta
							byte level = msg.reader().readByte(); // level
							short iconID = msg.reader().readShort(); // idIcon
							boolean isUptoUp = msg.reader().readBoolean(); // có nâng cấp đc k 
							long giaitem = msg.reader().readLong();
							short part = msg.reader().readShort(); // part cơ bản chắc chắn thằng nào cũng phải có
							short partquan = -1;
//							ItemTemplate itt = new ItemTemplate(msg.reader().readShort(), msg.reader().readByte(),  msg.reader().readByte(),  msg.reader().readUTF(),  msg.reader().readUTF(),  msg.reader().readByte(),  msg.reader().readShort(),  msg.reader().readShort(),  msg.reader().readBoolean());
							if(typeItem == Item.TYPE_AO){ // áo đọc thêm 1 part nữa là part quần
								partquan = msg.reader().readShort(); 
							}
							ItemTemplate itt = null;
							if(typeItem != Item.TYPE_AO)
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part,  isUptoUp);
							else{
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part, partquan,  isUptoUp);
							}
							itt.despaint = mFont.tahoma_7_white.splitFontArray(des, 150);
							itt.gia = giaitem;
							ItemTemplates.add(itt);
						}
					}else if(type == ItemTemplate.CHAR_WEARING){
						short CharID = msg.reader().readShort();
						//System.out.println("char.Id ----> "+CharID);
						byte leng = msg.reader().readByte();
						//System.out.println("LENG WEARING ----> "+leng);
						if(Char.myChar().charID != CharID){
							c = new Char();
							c.charID = CharID;
							readcharInmap(c,msg);
							GameScr.vCharInMap.addElement(c);
						}else{
							Char.myChar().arrItemBody = new Item[leng];
							try {
								for (int i = 0; i < Char.myChar().arrItemBody.length; i++) {
									short idtemplate = msg.reader().readShort();
								//	Cout.println("ID template -----> "+idtemplate);
										if(idtemplate != -1){
										//	Cout.println(" KHAC -1 ----->");
											ItemTemplate template = ItemTemplates.get(idtemplate);
				
											int indexUI = Item.getPosWearingItem(template.type);
											
											
											
//											GameScr.currentCharViewInfo.arrItemBody[indexUI] = new Item();
//										
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].indexUI = indexUI;
//											
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].typeUI = Item.UI_BODY;
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].template = template;
//											GameScr.currentCharViewInfo.arrItemBody[indexUI].isLock = true;
											Char.myChar().arrItemBody[indexUI] = new Item();
											
											Char.myChar().arrItemBody[indexUI].indexUI = indexUI;
											
											Char.myChar().arrItemBody[indexUI].typeUI = Item.UI_BODY;
											Char.myChar().arrItemBody[indexUI].template = template;
											Char.myChar().arrItemBody[indexUI].isLock = true;
			//								GameScr.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
			//								GameScr.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();
											
//											Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
											if (template.type == Item.TYPE_AO){
//												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Char.myChar().body = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
												Cout.println("BODY ----> "+Char.myChar().body);
												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Cout.println("LEG ----> "+Char.myChar().leg);
												
											}
//											else if (template.type == Item.TYPE_LEG){
//												
//												Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
//												Cout.println("LEG ----> "+Char.myChar().leg);
//
//											}
											else if (template.type == Item.TYPE_NON){
												Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI].template.part;
												Cout.println("HEAD ----> "+Char.myChar().head);
											}
									
									}
									
								}
//							Char.myChar().head = charWearing[0];
								
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						
					}
//						Char.myChar().charID = msg.reader().readShort();
//						byte lengtharrbody = msg.reader().readByte();
//						short charWearing[] = new short[lengtharrbody];
//						for(int i = 0; i < lengtharrbody; i++ ){
//							short idItemTemplate = msg.reader().readShort();
//							if(idItemTemplate != -1){
//								charWearing[i] = msg.reader().readShort();
//							}
//								
//						}
//						Char.myChar().head = charWearing[0];
//						Char.myChar().body = charWearing[4];
//						Char.myChar().leg = charWearing[11];
						         
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				                        
				
				break;
			case Cmd.CHAR_INFO:
				Char.myChar().charID = msg.reader().readShort();
				Char.myChar().cName = msg.reader().readUTF();
				Char.myChar().clevel = msg.reader().readShort();
				break;
			case Cmd.CHAR_LIST:
//				Cout.println(getClass(), "  nhan ve charlisst "+Cmd.CHAR_LIST);
				byte sizeChar = msg.reader().readByte();
//				Cout.println(getClass(),sizeChar+ "  nhan ve charlisst "+Cmd.CHAR_LIST);
				LoginScr.isLoggingIn = false;
//				LoginScr.selectCharScr = new SelectCharScr();
				SelectCharScr.gI().initSelectChar();
//				LoginScr.selectCharScr.switchToMe();
				GameCanvas.endDlg();
				if(sizeChar > 0){
//					LoginScr.selectCharScr = new SelectCharScr();
					// dùng biến riêng màn hình login 
//					Char[] c = new Char[sizeChar];
					for(int i = 0; i < sizeChar; i++){
						
//						c[i] = new Char();
//						c[i].charIdDB = msg.reader().readInt();
						SelectCharScr.gI().charIDDB[i] = msg.reader().readInt();
						SelectCharScr.gI().name[i] = msg.reader().readUTF();
						SelectCharScr.gI().gender[i] = msg.reader().readByte();
						SelectCharScr.gI().type[i] = msg.reader().readByte();
						byte sizePart = msg.reader().readByte();
						
						SelectCharScr.gI().parthead[i] = msg.reader().readShort();
//						System.out.println("PARTHEAD ----> "+SelectCharScr.getInstance().parthead[i]);

						
						SelectCharScr.gI().partbody[i] = msg.reader().readShort();
//						System.out.println("PARTBODY ---> "+SelectCharScr.getInstance().partbody[i]);
						
						//System.out.println("Headpart "+msg.reader().readShort());
						SelectCharScr.gI().partleg[i] = msg.reader().readShort();
//						System.out.println("PARTLEG ----> "+SelectCharScr.getInstance().partleg[i]);
						
//						if (SelectCharScr.getInstance().partWp[i] == -1)
//							SelectCharScr.getInstance().partWp[i] = 15;
//						if (SelectCharScr.getInstance().partbody[i] == -1) {
//							if (SelectCharScr.getInstance().gender[i] == 0)
//								SelectCharScr.getInstance().partbody[i] = 10;
//							else
//								SelectCharScr.getInstance().partbody[i] = 1;
//						}
//						if (SelectCharScr.getInstance().partleg[i] == -1) {
//							if (SelectCharScr.getInstance().gender[i] == 0)
//								SelectCharScr.getInstance().partleg[i] = 9;
//							else
//								SelectCharScr.getInstance().partleg[i] = 0;
//						}
//						SelectCharScr.getInstance().partWp[i] = msg.reader().readShort();
//						System.out.println("size part --> "+sizePart);
////						for(int j = 0; j < sizePart; j++){
//							c[i].leg = msg.reader().readShort();
//							c[i].body = msg.reader().readShort();
//							c[i].head = msg.reader().readShort();
//							c[i].wp = msg.reader().readShort();
////						}
					}
					
					LoginScr.selectCharScr.switchToMe(); 
					GameCanvas.endDlg();
				}else{
					LoginScr.creatCharScr.switchToMe();
					GameCanvas.endDlg();
				}
				
					
				break;
			case Cmd.LOGIN:
//				short lengtdata = msg.reader().readShort();
//				System.out.println("LENGTDATA ---> "+lengtdata);
//				short sizebyte = msg.reader().readShort();
				createData(msg.reader());
//				createDataEff(msg.reader());
//				createDataImage(msg.reader());
//				createDataPart(msg.reader());
//				createDataKill(msg.reader());

				// SAVE RMS NEW DATA
				msg.reader().reset();
				byte[] newData = new byte[msg.reader().available()];
				msg.reader().readFully(newData);
				// SAVE RMS NEW DATA VERSION
//				byte[] newDataVersion = new byte[] { GameScr.vcData };
//				Rms.saveRMS("dataVersion", newDataVersion);
//				 =========================
//				LoginScr.isUpdateData = false;
//				if (GameScr.vsData == GameScr.vcData
//						&& GameScr.vsMap == GameScr.vcMap
//						&& GameScr.vsSkill == GameScr.vcSkill
//						&& GameScr.vsItem == GameScr.vcItem) {
//					System.out.println(GameScr.vsData + "," + GameScr.vsMap
//							+ "," + GameScr.vsSkill + "," + GameScr.vsItem);
					//GameScr.getInstance().readDart();
					GameScr.readArrow();
					GameScr.readEfect();// doc ef
					SmallImage.gI().readImage();
					GameScr.readPart();
					GameScr.gI().readSkill();
//					Service.getInstance().clientOk();
//					return;
//				}
				break;
			case Cmd.DIALOG:
				byte typeDialog = msg.reader().readByte();
				if(typeDialog == 0){
					//  ok
					GameCanvas.startOKDlg( msg.reader().readUTF());
				}
				else if(typeDialog == 1){
					GameCanvas.startYesNoDlg(msg.reader().readUTF(),new Command("Yes", GameScr.gI(), 1, null),
							new Command("No", GameScr.gI(), 0, null));
				}else if(typeDialog == 2){
					GameCanvas.startDlgTime(msg.reader().readUTF(),new Command("Yes", GameCanvas.instance, 8882, null),
							msg.reader().readInt());
				}
				break;
			case Cmd.NPC_TEAMPLATE:
				int leng = msg.reader().readByte();

				for (byte i = 0; i < leng; i++) {
					NpcTemplate npctem = new NpcTemplate();
					npctem.npcTemplateId = msg.reader().readShort();
					npctem.name = msg.reader().readUTF();
					npctem.headId = msg.reader().readShort();
					npctem.bodyId = msg.reader().readShort();
					npctem.legId = msg.reader().readShort();
					npctem.contendChat = msg.reader().readUTF();

					Npc.arrNpcTemplate.put(npctem.npcTemplateId+"",npctem);
					/*Npc.arrNpcTemplate[i]. = */msg.reader().readShort();
//					Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
//					for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
//						Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
//						for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
//							Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
//					}
				}
			
				break;
			case Cmd.MAP_TEAMPLATE:
				int sizee = msg.reader().readShort();
				for (int i = 0; i < sizee; i++) {
					short idmap = msg.reader().readShort();
					short idtile = msg.reader().readShort();
					String name = msg.reader().readUTF();
					TileMap.listNameAllMap.put(idmap+"", name);
				}
				break;
			}
			
		}catch(Exception e){
			System.out.println("Error AT ----> "+msg.command);
			e.printStackTrace();
		}
		finally{
			if(msg != null)
				msg.cleanup();
		}
		
	}
		private void createData(DataInputStream d) {
			try{
				Rms.saveRMS("nj_arrow", Util.readByteArray(d));
				Rms.saveRMS("nj_effect", Util.readByteArray(d));
				Rms.saveRMS("nj_image", Util.readByteArray(d));
				Rms.saveRMS("nj_part", Util.readByteArray(d));
				Rms.saveRMS("nj_skill", Util.readByteArray(d));
			}catch(Exception e){
				e.printStackTrace();
			}
//		GameScr.vcData = d.readByte();
		
		}
		
		public void readcharInmap(Char c, Message msg){ // add player trong map
			try{
			
//				c.charID = msg.reader().readShort();
//				byte lengtharrbody = msg.reader().readByte();
				c.arrItemBody = new Item[13];
				
					for (int i = 0; i < c.arrItemBody.length; i++) {
						short idtemlate = msg.reader().readShort();
							if(idtemlate != -1){
								ItemTemplate template = ItemTemplates.get(idtemlate);
								int indexUI = Item.getPosWearingItem(template.type);
								
								
								c.arrItemBody[indexUI] = new Item();
							
								c.arrItemBody[indexUI].indexUI = indexUI;
								
								c.arrItemBody[indexUI].typeUI = Item.UI_BODY;
								c.arrItemBody[indexUI].template = template;
								c.arrItemBody[indexUI].isLock = true;
								
//								GameScr.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
//								GameScr.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();
							
								if (template.type == Item.TYPE_AO){
									
									c.body = c.arrItemBody[indexUI].template.part;
									c.leg = c.arrItemBody[indexUI].template.partquan;
								}
//								else if (template.type == Item.TYPE_LEG){
//									c.leg = c.arrItemBody[indexUI].template.part;
//								}
								else if (template.type == Item.TYPE_NON){
									c.head = c.arrItemBody[indexUI].template.part;
								}
						
						}
						
					}
			
				//return true;
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			//return false;
		}
		
		private Bitmap createImage(byte[] arr) {
			try {
				return Image.createImage(arr, 0, arr.length);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		
		private void createItem(DataInputStream d) throws Exception {
			GameScr.vcItem = d.readByte();
			GameScr.iOptionTemplates = new ItemOptionTemplate[d.readByte()];
			
			for (int i = 0; i < GameScr.iOptionTemplates.length; i++) {
				GameScr.iOptionTemplates[i] = new ItemOptionTemplate();
				GameScr.iOptionTemplates[i].id = i;
				GameScr.iOptionTemplates[i].name = d.readUTF();
				GameScr.iOptionTemplates[i].type = d.readByte();
			}
			int nItemTemplate = d.readShort();
			for (int i = 0; i < nItemTemplate; i++) {
				ItemTemplate itt = new ItemTemplate((short) i, d.readByte(), d.readByte(), d.readUTF(), d.readUTF(), d.readByte(), d.readShort(), d.readShort(), d.readBoolean());
				//System.out.println(itt.id+", "+itt.name);
				ItemTemplates.add(itt);
			}

		}
		
	
	
}

