package real;


import java.io.DataInputStream;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.thdgaming.naruto.GameCanvas;


import lib.mVector;
import network.Message;

public class Util {
	static Random random = new Random();

	public static void onLoadMapComplete() {
		GameCanvas.endDlg();
	}

	public void onLoading() {
		GameCanvas.startWaitDlg("Đang tải dữ liệu");
	}

	public static int randomNumber(int max) {
		return random.nextInt(max);
	}
	
	public static int random(int a, int b) {
		return a + random.nextInt(b - a);
		
	}

	public static byte[] readByteArray(Message msg) {
		try {
			int lengh = msg.reader().readInt();
			byte[] data = new byte[lengh];
			msg.reader().read(data);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] readByteArray(DataInputStream dos) {
		try {
			short lengh = dos.readShort();
			byte[] data = new byte[lengh];
			dos.read(data);
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String replace(String text, String regex, String replacement) {
		StringBuffer sBuffer = new StringBuffer();
		int pos = 0;
		while ((pos = text.indexOf(regex)) != -1) {
			sBuffer.append(text.substring(0, pos) + replacement);
			text = text.substring(pos + regex.length());
		}
		sBuffer.append(text);
		return sBuffer.toString();
	}

	public static String numberToString(String number) {
		String value = "", value1 = "";
		if(number.equals(""))
			return value;
		
		if(number.charAt(0) == '-'){
			value1 = "-";
			number = number.substring(1);
		}
		for (int i = number.length() - 1; i >= 0; i--) {
			if ((number.length() - 1 - i) % 3 == 0
					&& (number.length() - 1 - i) > 0)
				value = number.charAt(i) + "." + value;
			else
				value = number.charAt(i) + value;
		}
		return value1 + value;
	}

	public static void sendMsDK(String syntax, short port) {
//		GameMidlet.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.getInstance(),88827, null), new Command("", GameCanvas.getInstance(),88828, null));
//		ThreadManager.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.getInstance(),88827, null), new Command("", GameCanvas.getInstance(),88828, null));
	}

//	public static void downloadGame(String url) {
//		try {
//			GameMidlet.instance.platformRequest(url);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			GameMidlet.instance.notifyDestroyed();
//		}
//	}


	public static String getTime(int timeRemainS){
		int timeRemainM = 0;
		if (timeRemainS > 60) {
			timeRemainM = timeRemainS / 60;
			timeRemainS = timeRemainS % 60;
		}
		int timeRemainH = 0;
		if (timeRemainM > 60) {
			timeRemainH = timeRemainM / 60;
			timeRemainM = timeRemainM % 60;
		}
		int timeRemainD = 0;
		if (timeRemainH > 24) {
			timeRemainD = timeRemainH / 24;
			timeRemainH = timeRemainH % 24;
		}
		String s = "";
		if (timeRemainD > 0) {
			s += timeRemainD;
			s += "d";
			s += timeRemainH+"h";
					
		}
		else if (timeRemainH > 0) {
			s += timeRemainH;
			s += "h";
			s += timeRemainM+"'";
		}
		else
		{		
			if (timeRemainM > 9)
				s += timeRemainM;
			else
				s += "0" + timeRemainM;
			s += ":";
			
			if (timeRemainS > 9)
				s += timeRemainS;
			else
				s += "0" + timeRemainS;
		}
		return s;
	}

	public static void sleep(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {}
	}
	
	public static String[] split(String original,String separator) {
	    mVector nodes = new mVector();
	    int index = original.indexOf(separator);
	    while(index >= 0) {
	        nodes.addElement( original.substring(0, index) );
	        original = original.substring(index+separator.length());
	        index = original.indexOf(separator);
	    }
	    nodes.addElement( original );
	    String[] result = new String[ nodes.size() ];
	    if( nodes.size() > 0 ) {
	        for(int loop = 0; loop < nodes.size(); loop++)
	        {
	            result[loop] = (String)nodes.elementAt(loop);
	        }

	    }
	    return result;
	}
	
	public static void openUrl(String url) {
		try {
			Gdx.net.openURI(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
