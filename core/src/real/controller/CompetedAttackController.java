package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.Command;
import network.Message;
import real.dto.response.CompetedAttackResponseDTO;
import utils.IOUtils;

public class CompetedAttackController  implements IController
{

    public void processMessage(Message msg)
    {
        try
        {
            CompetedAttackResponseDTO competedAttackResponseDTO = (CompetedAttackResponseDTO) IOUtils.readMessage(msg, CompetedAttackResponseDTO.class);

            long idchar = competedAttackResponseDTO.getPlayerInvitedId();
            String loimoi = competedAttackResponseDTO.getInviteInfo();
            GameCanvas.startYesNoDlg(loimoi,
                new Command("Đồng ý", GameCanvas.instance, GameCanvas.cDongYThachDau, idchar + ""),
                new Command("Không", GameCanvas.instance, GameCanvas.cRejectThachDau, idchar + ""));
        }
        catch (Exception e)
        {
            Gdx.app.error(CompetedAttackController.class.getName(), "Failed to handle compete attack", e);
        }
    }

}