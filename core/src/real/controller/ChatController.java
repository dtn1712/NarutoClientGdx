package real.controller;

import android.util.Log;

import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.ChatWorld;
import model.ChatManager;
import model.ChatPopup;
import model.ChatType;
import model.MResources;
import network.Message;
import real.dto.response.ChatResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ChatController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            ChatResponseDTO chatResponseDTO = (ChatResponseDTO) IOUtils.readMessage(msg, ChatResponseDTO.class);

            byte typeChat = chatResponseDTO.getType();
            if (typeChat == ChatType.CHAT_MAP)
            {
                // chat map
                long useriD = chatResponseDTO.getId();
                String text = chatResponseDTO.getText();
                Char c = Char.myChar().charID == useriD ? Char.myChar() : GameScreen.getInstance().findCharInMap(useriD);

                if (c == null)
                {
                    return;
                }
                ChatPopup.addChatPopup(text, 200, c);
                ChatManager.getInstance().addChat(MResources.PUBLICCHAT[0], c.cName, text);
            }
            else if (typeChat == ChatType.CHAT_WORLD)
            {
                ChatWorld.sendChatWorld(chatResponseDTO);
            }
            else if (typeChat == ChatType.CHAT_FRIEND)
            {
                ChatPrivate.sendChatFriend(chatResponseDTO);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(ChatController.class.getName(), "Failed to handle chat", e);
        }

    }
}