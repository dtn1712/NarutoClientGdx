package real.controller;

import android.util.Log;

import Objectgame.Char;
import lib.Music;
import model.ServerEffect;
import network.Message;
import real.dto.character.CharAttributeDTO;
import real.dto.character.MainCharInfoDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CharInfoController implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            MainCharInfoDTO mainCharInfoDTO = (MainCharInfoDTO) IOUtils.readMessage(msg, MainCharInfoDTO.class);

            Char.myChar().charID = mainCharInfoDTO.getId();
            Char.myChar().cName = mainCharInfoDTO.getCharName();
            Char.myChar().cClass = mainCharInfoDTO.getIdClass();
            Char.myChar().cgender = mainCharInfoDTO.getGender();

            int clevel2 = mainCharInfoDTO.getLevel();
            if (Char.myChar().clevel > 0 && Char.myChar().clevel < clevel2)
            {
                Music.play(Music.LENLV, 10);
                ServerEffect.addServerEffect(22, Char.myChar().cx, Char.myChar().cy, 1);
            }

            Char.myChar().clevel = clevel2;
            Char.myChar().cEXP = mainCharInfoDTO.getPercent();
            Char.myChar().cHP = mainCharInfoDTO.getHp();
            Char.myChar().cMaxHP = mainCharInfoDTO.getMaxHp();
            Char.myChar().cMP = mainCharInfoDTO.getMp();
            Char.myChar().cMaxMP = mainCharInfoDTO.getMaxMp();

            Char.myChar().cHPDisplay = Char.myChar().cHP + "/" + Char.myChar().cMaxHP;
            Char.myChar().cMPDisplay = Char.myChar().cMP + "/" + Char.myChar().cMaxMP;
            Char.myChar().cExpDisplay = Char.myChar().cEXP + "%";
            Char.myChar().cLevelDisplay = Char.myChar().clevel + "";


            Char.myChar().diemTN = null;
            Char.myChar().diemTN = new int[8];
            Char.myChar().diemTN[0] = mainCharInfoDTO.getKhaiMon();
            Char.myChar().diemTN[1] = mainCharInfoDTO.getHuuMon();
            Char.myChar().diemTN[2] = mainCharInfoDTO.getSinhMon();
            Char.myChar().diemTN[3] = mainCharInfoDTO.getDoMon();
            Char.myChar().diemTN[4] = mainCharInfoDTO.getCanhMon();
            Char.myChar().diemTN[5] = mainCharInfoDTO.getThuongMon();
            Char.myChar().diemTN[6] = mainCharInfoDTO.getKinhMon();
            Char.myChar().diemTN[7] = mainCharInfoDTO.getTuMon();
            Char.myChar().totalPoints = mainCharInfoDTO.getBasePoint();

            Char.myChar().body = mainCharInfoDTO.getBodyId();
            Char.myChar().leg = mainCharInfoDTO.getLegId();
            Char.myChar().head = mainCharInfoDTO.getHeadId();

            Char.myChar().cspeed = mainCharInfoDTO.getBaseSpeed();
            Char.myChar().cBonusSpeed = mainCharInfoDTO.getAddedSpeed();

            //gui ve thong tin sub
            int sizeSub = (short) mainCharInfoDTO.getSizeAttribute();
            Char.myChar().subTn = new int[sizeSub];
            for (int i = 0; i < sizeSub; i++)
            {
                CharAttributeDTO charAttribute = mainCharInfoDTO.getCharAttribute().get(i);
                byte indexMon = charAttribute.getId();
                int valueSub = charAttribute.getValue();
                Char.myChar().subTn[indexMon] = valueSub;
            }
            if (GameScreen.getInstance().findCharInMap(Char.myChar().charID) == null)
            {
                GameScreen.getInstance().vCharInMap.addElement(Char.myChar());
            }

            Char.myChar().cCountry = mainCharInfoDTO.getCountry();
        }
        catch (Exception e)
        {
            Gdx.app.error(CharInfoController.class.getName(), "Failed to get char info", e);
        }
    }
}