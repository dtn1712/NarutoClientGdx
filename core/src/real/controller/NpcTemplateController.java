package real.controller;

import android.util.Log;

import Objectgame.Npc;
import Objectgame.NpcTemplate;
import network.Message;
import real.dto.mapper.DTOMapper;
import real.dto.response.NpcTemplateResponseDTO;
import utils.IOUtils;

public class NpcTemplateController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            NpcTemplateResponseDTO npcTemplateResponseDTO = (NpcTemplateResponseDTO) IOUtils.readMessage(msg, NpcTemplateResponseDTO.class);

            for (int i = 0; i < npcTemplateResponseDTO.getNpcTemplates().size(); i++)
            {
                NpcTemplate npcTemplate = DTOMapper.mapNpcTemplateDtoToModel(npcTemplateResponseDTO.getNpcTemplates().get(i));
                Npc.arrNpcTemplate.put(npcTemplate.npcTemplateId + "", npcTemplate);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(NpcTemplateController.class.getName(), "Failed to get npc template", e);
        }

    }
}