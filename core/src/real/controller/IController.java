package real.controller;

import network.Message;

public interface IController {

    void processMessage(Message message);

}
