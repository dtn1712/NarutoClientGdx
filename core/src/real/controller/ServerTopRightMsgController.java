package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import network.Message;
import real.dto.response.ServerTopRightResponseDTO;
import utils.IOUtils;

public class ServerTopRightMsgController  implements IController
    {
        public void processMessage(Message msg)
        {
            try {
                ServerTopRightResponseDTO serverTopRightResponseDTO = (ServerTopRightResponseDTO) IOUtils.readMessage(msg, ServerTopRightResponseDTO.class);
                GameCanvas.getInstance().StartDglThongBao(serverTopRightResponseDTO.getInfo());
            } catch (Exception e) {
                Gdx.app.error(ServerNotificationController.class.getName(), "Failed to get server top right message", e);
            }
        }
    }