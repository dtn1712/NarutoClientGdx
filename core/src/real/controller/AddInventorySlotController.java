package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Gui.TabBag;
import lib.Command;
import network.Message;
import real.dto.response.AddInventorySlotResponseDTO;
import utils.IOUtils;

public class AddInventorySlotController implements IController
{
    public void processMessage(Message message)
    {
        try {
            AddInventorySlotResponseDTO addInventorySlotResponseDTO = (AddInventorySlotResponseDTO) IOUtils.readMessage(message, AddInventorySlotResponseDTO.class);

            GameCanvas.startDlgTField("1 slot = " + addInventorySlotResponseDTO.getSlotGoldValue() + " gold",
                    new Command("OK", (TabBag) GameCanvas.allInfo.VecTabScreen.elementAt(0),
                            TabBag.BTN_COMFIRM_BUY_SLOT_INVENTORY, null)
                    , null, new Command("Cancel", GameCanvas.instance, GameCanvas.END_DIALOG_FIELD, null));
        } catch (Exception e) {
            Gdx.app.error(AddInventorySlotController.class.getName(), "Failed to add inventory slot", e);
        }
    }
}