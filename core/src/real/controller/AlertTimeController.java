package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.Command;
import network.Message;
import real.dto.response.ServerAlertTimeResponseDTO;
import utils.IOUtils;

public class AlertTimeController implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ServerAlertTimeResponseDTO alertTimeResponseDTO = (ServerAlertTimeResponseDTO) IOUtils.readMessage(msg, ServerAlertTimeResponseDTO.class);
            GameCanvas.getInstance().startDlgTime(alertTimeResponseDTO.getInfo(), new Command("Yes", GameCanvas.instance, GameCanvas.END_DIALOG, null), alertTimeResponseDTO.getTime());
        } catch (Exception e) {
            Gdx.app.error(AlertTimeController.class.getName(), "Failed to alert time", e);
        }
    }

}