package real.controller;

import android.util.Log;

import network.Message;
import real.dto.response.RegionInfoResponseDTO;
import screen.RegionScreen;
import utils.IOUtils;

public class RegionInfoController  implements IController {


    public void processMessage(Message msg)
    {
        try
        {
            RegionInfoResponseDTO regionInfoResponseDTO = (RegionInfoResponseDTO) IOUtils.readMessage(msg, RegionInfoResponseDTO.class);

            int size = regionInfoResponseDTO.getRegionStatuses().size();

            RegionScreen.getInstance().listKhu = new byte[size][];

            for (int i = 0; i < size; i++)
            {
                RegionScreen.getInstance().listKhu[i] = new byte[1];
            }

            for (int i = 0; i < size; i++)
            {
                RegionScreen.getInstance().listKhu[i][0] = regionInfoResponseDTO.getRegionStatuses().get(i);
            }

            RegionScreen.getInstance().srclist.selectedItem = -1;
            RegionScreen.getInstance().switchToMe();
        }
        catch (Exception e)
        {
            Gdx.app.error(RegionInfoController.class.getName(), "Failed to handle region info", e);
        }
    }

}