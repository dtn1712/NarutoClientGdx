package real.controller;

import android.util.Log;

import Gui.Constants;
import Objectgame.Char;
import Objectgame.Mob;
import Objectgame.Skill;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import lib.MFont;
import lib.Music;
import model.ServerEffect;
import network.Message;
import real.dto.response.AttackResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class AttackController implements IController {

    public void processMessage(Message msg) {
        try {
            AttackResponseDTO attackResponseDTO = (AttackResponseDTO) IOUtils.readMessage(msg, AttackResponseDTO.class);
            if (attackResponseDTO.getAttackerType() == Constants.TYPE_PLAYER) {
                Char c = GameScreen.getInstance().findCharInMap(attackResponseDTO.getAttackerId());
                if (c == null) {
                    c = Char.myChar();
                }

                c.cMP = attackResponseDTO.getAttackerMp();
                c.cMPDisplay = c.cMP + "/" + c.cMaxMP;
                for (int i = 0; i < attackResponseDTO.getTargets().size(); i++) {
                    AttackResponseDTO.TargetDTO target = attackResponseDTO.getTargets().get(i);
                    if (target.getTargetType() == Constants.TYPE_MONSTER) {
                        for (int j = 0; j < GameScreen.getInstance().vMob.size(); j++) {
                            Mob mob = GameScreen.getInstance().vMob.elementAt(j);
                            if (mob.mobId == target.getId()) {
                                c.mobFocus = mob;
                                c.cdame = target.getHitDamage();
                                c.mobFocus.hp = target.getHp()< 0 ? 0 : target.getHp();
                                c.mobFocus.hpDisplay = c.mobFocus.hp + "/" + c.mobFocus.maxHp;
                                if (c.cdame > 0) {
                                    GameScreen.startFlyText("-" + c.cdame, c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5,
                                            0, -2, MFont.RED);
                                } else {
                                    GameScreen.startFlyText("miss", c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5, 0, -2,
                                            MFont.RED);
                                }

                                if (attackResponseDTO.getAttackerId() != Char.myChar().charID) {
                                    c.cdir = c.cx - c.mobFocus.x > 0 ? -1 : 1;
                                    c.mobFocus.setInjure();
                                    c.mobFocus.injureBy = c;
                                    c.mobFocus.status = Mob.MA_INJURE;
                                }
                                if (mob.hp <= 0) {
                                    mob.status = Mob.MA_DEADFLY;
                                }
                            }
                        }
                    } else if (target.getTargetType() == Constants.TYPE_PLAYER) {
                        Char charTarget = GameScreen.getInstance().findCharInMap(target.getId());
                        if (charTarget != null) {
                            charTarget.cHP = target.getHp();
                            charTarget.cHPDisplay = charTarget.cHP + "/" + charTarget.cMaxHP;
                            charTarget.DoInjure();
                            c.cdir = c.cx - charTarget.cx > 0 ? -1 : 1;
                            ServerEffect.addServerEffect(25, charTarget.cx, charTarget.cy - 20, 1);
                            GameScreen.startFlyText("-" + target.getHitDamage(), charTarget.cx, charTarget.cy - 60, 0, -2,
                                    MFont.RED);
                        }
                    }
                }

                if (attackResponseDTO.getSkillAttackId() > -1 && attackResponseDTO.getSkillAttackId() < GameScreen.sks.length - 1) {
                    if (c.charID != Char.myChar().charID) {
                        Music.play(attackResponseDTO.getSkillAttackId() == 0 ? Music.ATTACK_0 : Music.SKILL2, 0.5f);
                        c.SetSkillPaint(GameScreen.sks[attackResponseDTO.getSkillEffectId()], Skill.ATT_STAND);
                    } else {
                        Music.play(attackResponseDTO.getSkillAttackId() == 0 ? Music.ATTACK_0 : Music.SKILL2, 0.5f);
                        SkillTemplate skillTemplate = SkillTemplates.hSkilltemplate.get((byte) attackResponseDTO.getSkillAttackId()); // skill update lai cai cooldown
                        skillTemplate.updateCoolDown(attackResponseDTO.getSkillCoolDownTime());
                        Char.myChar().SetSkillPaint(GameScreen.sks[attackResponseDTO.getSkillEffectId()], Skill.ATT_STAND);
                        Char.myChar().mQuickslot[GameScreen.idSkillUse].startCoolDown();
                    }
                }
            } else if (attackResponseDTO.getAttackerType() == Constants.TYPE_MONSTER) {
                long mobAttId = attackResponseDTO.getAttackerId();
                Mob mob = null;
                for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                    Mob mobs = GameScreen.getInstance().vMob.elementAt(i);
                    if (mobs.mobId == mobAttId) {
                        mob = mobs;
                    }
                }

                if (mob != null) {
                    AttackResponseDTO.TargetDTO target = attackResponseDTO.getTargets().get(0);
                    Char victimChar = GameScreen.getInstance().findCharInMap(target.getId());
                    mob.cFocus = victimChar;
                    if (mob.cFocus == null) {
                        mob.f = -1;
                        mob.cFocus = Char.myChar();
                    }
                    mob.dir = mob.x - mob.cFocus.cx > 0 ? -1 : 1;

                    int hpmat = target.getHp();
                    mob.cFocus.cHP = hpmat;
                    mob.cFocus.cHPDisplay = mob.cFocus.cHP + "/" + mob.cFocus.cMaxHP;
                    if (victimChar != null) {
                        victimChar.cHP = hpmat;
                        victimChar.cHPDisplay = victimChar.cHP + "/" +victimChar.cMaxHP;
                    }

                    mob.dame = target.getHitDamage();

                    mob.status = Mob.MA_ATTACK;
                    GameScreen.startFlyText("-" + mob.dame, mob.cFocus.cx, mob.cFocus.cy - 60, 0, -3, MFont.RED);

                    if (mob.typeMob == Mob.TYPE_MOB_TOOL) {
                        ServerEffect.addServerEffect(21, mob.x, mob.y, 1);
                    }

                    mob.setAttack(mob.cFocus);
                }
            } else if (attackResponseDTO.getAttackerType() == Constants.TYPE_BOMB) {
                AttackResponseDTO.TargetDTO target = attackResponseDTO.getTargets().get(0);
                Char victimChar = GameScreen.getInstance().findCharInMap(target.getId());
                if (victimChar != null) {
                    victimChar.cHP = target.getHp();
                    victimChar.cHPDisplay = victimChar.cHP + "/" + victimChar.cMaxHP;
                    victimChar.DoInjure();
                    ServerEffect.addServerEffect(25, victimChar.cx, victimChar.cy - 20, 1);
                    GameScreen.startFlyText("-" + target.getHitDamage(), victimChar.cx, victimChar.cy - 60, 0, -2,
                            MFont.RED);
                }
            }
        } catch (Exception e) {
            Gdx.app.error(AttackController.class.getName(), "Failed to handle attack", e);
        }

    }
}