package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.MSystem;
import lib.Rms;
import model.SmallImage;
import network.Message;
import real.dto.response.ImageResponseDTO;
import screen.DownloadImageScreen;
import utils.IOUtils;

public class RequestImageController  implements IController
{

    public void processMessage(Message message)
    {
        try {
            ImageResponseDTO imageResponseDTO = (ImageResponseDTO) IOUtils.readMessage(message, ImageResponseDTO.class);

            if (GameCanvas.getInstance().currentScreen == DownloadImageScreen.getInstance()) {
                DownloadImageScreen.isOKNext = true;
            }
            String path = MSystem.getPathRMS(SmallImage.getPathImage(imageResponseDTO.getImageKey()));
            Rms.saveRMS(path, imageResponseDTO.getData());
        } catch (Exception e) {
            Gdx.app.error(RequestImageController.class.getName(), "Failed to request image", e);
        }
    }
}