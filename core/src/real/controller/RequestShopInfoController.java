package real.controller;

import android.util.Log;

import Gui.ShopMain;
import network.Message;
import real.dto.response.ShopInfoResponseDTO;
import utils.IOUtils;

public class RequestShopInfoController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            ShopInfoResponseDTO shopInfoResponseDTO = (ShopInfoResponseDTO) IOUtils.readMessage(msg, ShopInfoResponseDTO.class);

            byte menuId = shopInfoResponseDTO.getIdMenu();
            int itemListSize = shopInfoResponseDTO.getListTemplateId().size();
            ShopMain.indexidmenu = menuId;
            ShopMain.idItemtemplate = new short[itemListSize];
            for (int i = 0; i < itemListSize; i++)
            {
                ShopMain.idItemtemplate[i] = shopInfoResponseDTO.getListTemplateId().get(i);
            }
            ShopMain.getItemList(ShopMain.indexidmenu, ShopMain.idItemtemplate);
        }
        catch (Exception e)
        {
            Gdx.app.error(RequestShopInfoController.class.getName(), "Failed to request shop info", e);
        }

    }
}