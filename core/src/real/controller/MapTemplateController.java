package real.controller;

import android.util.Log;

import Objectgame.TileMap;
import network.Message;
import real.dto.response.MapTemplateResponseDTO;
import utils.IOUtils;

public class MapTemplateController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            MapTemplateResponseDTO mapTemplateResponseDTO = (MapTemplateResponseDTO) IOUtils.readMessage(msg, MapTemplateResponseDTO.class);

            int size = mapTemplateResponseDTO.getListMapTemplate().size();
            for (int i = 0; i < size; i++)
            {
                short idmap = mapTemplateResponseDTO.getListMapTemplate().get(i).getId();
                byte idtile = mapTemplateResponseDTO.getListMapTemplate().get(i).getTileId();
                String name = mapTemplateResponseDTO.getListMapTemplate().get(i).getName();
                byte country = mapTemplateResponseDTO.getListMapTemplate().get(i).getCountry();
                byte bg = mapTemplateResponseDTO.getListMapTemplate().get(i).getBg();

                TileMap.getInstance().listBgMap.put(idmap, bg);
                TileMap.getInstance().listNameAllMap.put(idmap, name);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(MapTemplateController.class.getName(), "Failed to get map template", e);
        }

    }
}