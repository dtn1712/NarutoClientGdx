package real.controller;

import android.util.Log;

import Objectgame.Mob;
import model.ServerEffect;
import network.Message;
import real.dto.response.MonsterPositionDTO;
import screen.GameScreen;
import utils.IOUtils;

public class MonsterMoveController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            MonsterPositionDTO monsterPositionDTO = (MonsterPositionDTO) IOUtils.readMessage(msg, MonsterPositionDTO.class);

            short idmobb = monsterPositionDTO.getId();
            short idmobx = monsterPositionDTO.getMonX();
            short idmoby = monsterPositionDTO.getMonY();
            String namemobb = monsterPositionDTO.getName();
            int hpmob = monsterPositionDTO.getHp();

            for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                Mob mobb = GameScreen.getInstance().vMob.elementAt(i);
                if (mobb.mobId == idmobb && mobb.status == Mob.MA_INHELL) {
                    mobb.injureThenDie = false;
                    mobb.status = Mob.MA_WALK;
                    mobb.x = idmobx;
                    mobb.y = idmoby - 10;
                    mobb.mobName = namemobb;
                    ServerEffect.addServerEffect(37, mobb.x, mobb.y - mobb.getH() / 4 - 10, 1);
                    mobb.hp = hpmob > mobb.maxHp ? mobb.maxHp : hpmob;
                }
            }
        } catch (Exception e) {
            Gdx.app.error(MonsterMoveController.class.getName(), "Failed to get monster move", e);
        }
    }

}