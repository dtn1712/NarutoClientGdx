package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Gui.TabBag;
import lib.Command;
import network.Message;
import real.dto.response.AddXuResponseDTO;
import utils.IOUtils;

public class AddXuFromGoldController implements IController
{
    public void processMessage(Message message)
    {
        try {
            AddXuResponseDTO addXuResponseDTO = (AddXuResponseDTO) IOUtils.readMessage(message, AddXuResponseDTO.class);
            GameCanvas.startDlgTField("1 gold = " + addXuResponseDTO.getGoldToXuExchangeRate() + " xu",
                    new Command("OK", (TabBag) GameCanvas.allInfo.VecTabScreen.elementAt(0), TabBag.BTN_COMFIRM_GOLD_TO_COIN, null)
                    , null,
                    new Command("Cancel", GameCanvas.instance, GameCanvas.END_DIALOG_FIELD, null));
        } catch (Exception e) {
            Gdx.app.error(AddXuFromGoldController.class.getName(), "Failed to add xu", e);
        }
    }
}