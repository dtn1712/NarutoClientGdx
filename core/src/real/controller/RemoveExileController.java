package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.Command;
import network.Message;
import real.dto.response.RemoveExileResponseDTO;
import utils.IOUtils;

public class RemoveExileController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            RemoveExileResponseDTO removeExileResponseDTO = (RemoveExileResponseDTO) IOUtils.readMessage(msg, RemoveExileResponseDTO.class);

            String content = removeExileResponseDTO.getContent();
            GameCanvas.startYesNoDlg(content,
                    new Command("Rửa tội", GameCanvas.instance, GameCanvas.cmdComfirmRemoveExile, null),
                    new Command("Huỷ", GameCanvas.instance, GameCanvas.END_DIALOG, null) );
        }
        catch (Exception e)
        {
            Gdx.app.error(RemoveExileController.class.getName(), "Failed to remove exile", e);
        }
    }
}