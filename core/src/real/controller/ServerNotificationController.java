package real.controller;

import android.util.Log;

import model.InfoServer;
import network.Message;
import real.dto.response.ServerNotificationResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ServerNotificationController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ServerNotificationResponseDTO serverNotificationResponseDTO = (ServerNotificationResponseDTO) IOUtils.readMessage(msg, ServerNotificationResponseDTO.class);
            GameScreen.getInstance().listInfoServer.add(new InfoServer(InfoServer.CHATSERVER, serverNotificationResponseDTO.getContent()));
        } catch (Exception e) {
            Gdx.app.error(ServerNotificationController.class.getName(), "Failed to get server notification", e);
        }
    }
}