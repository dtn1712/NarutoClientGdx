package real.controller;

import android.util.Log;

import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import network.Message;
import real.dto.response.SkillCharResponseDTO;
import utils.IOUtils;

public class SkillCharController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            SkillCharResponseDTO skillCharResponseDTO = (SkillCharResponseDTO) IOUtils.readMessage(msg, SkillCharResponseDTO.class);

            for (int i = 0; i < skillCharResponseDTO.getListSkill().size(); i++) {

                byte id = skillCharResponseDTO.getListSkill().get(i).getIdSkill();
                short idIcon = skillCharResponseDTO.getListSkill().get(i).getIdIcon();
                short idIconTron = skillCharResponseDTO.getListSkill().get(i).getIdIconShort(); //icon skill tron
                String name = skillCharResponseDTO.getListSkill().get(i).getNameSkill();
                String description = skillCharResponseDTO.getListSkill().get(i).getDescription(); //mo ta
                byte typebuff = skillCharResponseDTO.getListSkill().get(i).getTypeBuff(); //loai skill buff
                short ideff = skillCharResponseDTO.getListSkill().get(i).getIdEffect();

                SkillTemplate skillTemplate = new SkillTemplate(id, name, idIcon, typebuff, description);
                skillTemplate.iconTron = idIconTron;
                skillTemplate.ideff = ideff;

                int nLevel = skillCharResponseDTO.getListSkill().get(i).getListLevel().size();

                skillTemplate.CreateLevel(nLevel);
                for (int j = 0; j < nLevel; j++) {
                    skillTemplate.levelChar = new short[nLevel];
                    skillTemplate.levelChar[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getLvChar();
                    skillTemplate.mphao[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getMp();
                    skillTemplate.cooldown[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getCoolDown();
                    skillTemplate.timebuffSkill[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getTimeBuff();
                    skillTemplate.tExistSubEff[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getTimeEff();
                    skillTemplate.ntargetlv[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getNumTarget();
                    skillTemplate.rangeX[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getRangeX();
                    skillTemplate.rangeY[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getRangeY();
                    skillTemplate.dame[j] = skillCharResponseDTO.getListSkill().get(i).getListLevel().get(j).getDamage();
                }

                SkillTemplates.add(skillTemplate);
            }
        } catch (Exception e) {
            Gdx.app.error(SkillCharController.class.getName(), "Failed to handle char skill", e);
        }
    }
}