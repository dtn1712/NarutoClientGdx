package real.controller;

import android.util.Log;

import Objectgame.Mob;
import network.Message;
import real.dto.response.BossResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class BossDisappearController implements IController
{
    public void processMessage(Message message)
    {
        try {
            BossResponseDTO bossResponseDTO = (BossResponseDTO) IOUtils.readMessage(message, BossResponseDTO.class);

            for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                Mob demMob = GameScreen.getInstance().vMob.elementAt(i);
                if (demMob != null && demMob.mobId == bossResponseDTO.getMonsterId() && demMob.isBoss) {
                    demMob.isBossAppear = false;
                    demMob.startDie();
                    break;
                }
            }
        } catch (Exception e) {
            Gdx.app.error(BossDisappearController.class.getName(), "Failed to handle boss disappear", e);
        }
    }
}