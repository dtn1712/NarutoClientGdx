package real.controller;

import android.util.Log;

import Objectgame.Char;
import Objectgame.ItemMap;
import network.Message;
import real.dto.response.PickUpItemResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class PickUpItemController implements IController
{
    public void processMessage(Message msg)
    {
        try {
            PickUpItemResponseDTO pickUpItemResponseDTO = (PickUpItemResponseDTO) IOUtils.readMessage(msg, PickUpItemResponseDTO.class);

            int idItemPick = pickUpItemResponseDTO.getItemId();

            for (int i = 0; i < GameScreen.getInstance().vItemMap.size(); i++) {
                ItemMap itemMap = GameScreen.getInstance().vItemMap.elementAt(i);
                if (itemMap.itemMapId == idItemPick) {
                    if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.itemMapId == idItemPick) {
                        Char.myChar().itemFocus = null;
                    }

                    GameScreen.getInstance().vItemMap.removeElementAt(i);
                    break;
                }
            }
        } catch (Exception e) {
            Gdx.app.error(PickUpItemController.class.getName(), "Failed to pick up item", e);
        }
    }
}