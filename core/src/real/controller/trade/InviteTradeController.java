package real.controller.trade;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import lib.Command;
import model.MResources;
import network.Message;
import real.controller.IController;
import real.dto.response.InviteTradeResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class InviteTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            InviteTradeResponseDTO inviteTradeResponseDTO = (InviteTradeResponseDTO) IOUtils.readMessage(msg, InviteTradeResponseDTO.class);

            long partnerTradeId = inviteTradeResponseDTO.getId();
            Char.myChar().partnerTrade = null;
            Char.myChar().partnerTrade = GameScreen.getInstance().findCharInMap(partnerTradeId);
            GameCanvas.startYesNoDlg(inviteTradeResponseDTO.getContent(), GameScreen.cmdAcceptTrade,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
        } catch (Exception e) {
            Gdx.app.error(InviteTradeController.class.getName(), "Failed to invite trade", e);
        }
    }
}