package real.controller.trade;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import network.Message;
import real.Service;
import real.controller.IController;
import real.dto.response.TradeAcceptInviteResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class AcceptInviteTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            TradeAcceptInviteResponseDTO tradeAcceptInviteResponseDTO = (TradeAcceptInviteResponseDTO) IOUtils.readMessage(msg, TradeAcceptInviteResponseDTO.class);

            long partnerId = tradeAcceptInviteResponseDTO.getId();
            if (Char.myChar().partnerTrade == null) {
                Char.myChar().partnerTrade = GameScreen.getInstance().findCharInMap(partnerId);
            }

            GameScreen.isBag = false;
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.iconTrade.performAction();
            Service.getInstance().requestInventory();
        } catch (Exception e) {
            Gdx.app.error(AcceptInviteTradeController.class.getName(), "Failed to accept invite trade", e);
        }
    }

}