package real.controller.trade;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Gui.TradeGui;
import Objectgame.Char;
import lib.Command;
import model.MResources;
import network.Message;
import real.controller.IController;
import real.dto.response.TradeConfirmResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ConfirmTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            TradeConfirmResponseDTO tradeConfirmResponseDTO = (TradeConfirmResponseDTO) IOUtils.readMessage(msg, TradeConfirmResponseDTO.class);

            long charidP = tradeConfirmResponseDTO.getId();
            Char cTrade = GameScreen.getInstance().findCharInMap(charidP);

            GameCanvas.startYesNoDlg(cTrade.cName + tradeConfirmResponseDTO.getContent(), TradeGui.cmdTradeEnd,
                new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));

            for (int i = 0; i < Char.myChar().ItemMyTrade.length; i++)
            {
                Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                Char.myChar().ItemMyTrade[i] = null;
            }

        }
        catch (Exception e)
        {
            Gdx.app.error(ConfirmTradeController.class.getName(), "Failed to confirm trade", e);
        }

    }
}