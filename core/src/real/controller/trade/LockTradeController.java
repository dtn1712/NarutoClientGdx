package real.controller.trade;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import network.Message;
import real.controller.IController;
import real.dto.response.TradeLockResponseDTO;
import utils.IOUtils;

public class LockTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            TradeLockResponseDTO tradeLockResponseDTO = (TradeLockResponseDTO) IOUtils.readMessage(msg, TradeLockResponseDTO.class);
            long charId = tradeLockResponseDTO.getId();
            if (Char.myChar().charID != charId) {
                GameCanvas.getInstance().gameScreen.guiMain.menuIcon.trade.block2 = true;
            } else {
                GameCanvas.getInstance().gameScreen.guiMain.menuIcon.trade.block1 = true;
            }
        } catch (Exception e) {
            Gdx.app.error(LockTradeController.class.getName(), "Failed to lock trade", e);
        }
    }
}