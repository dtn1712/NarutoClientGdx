package real.controller.trade;


import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import network.Message;
import real.controller.IController;
import real.dto.response.CancelTradeResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CancelTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            CancelTradeResponseDTO cancelTradeResponseDTO = (CancelTradeResponseDTO) IOUtils.readMessage(msg, CancelTradeResponseDTO.class);

            long charId = cancelTradeResponseDTO.getId();
            Char charPar = GameScreen.getInstance().findCharInMap(charId);
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.trade = null;
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.getInstance().startOKDlg(charPar.cName + cancelTradeResponseDTO.getContent());
        } catch (Exception e) {
            Gdx.app.error(CancelTradeController.class.getName(), "Failed to cancel trade", e);
        }
    }

}