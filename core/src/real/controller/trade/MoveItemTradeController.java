package real.controller.trade;

import android.util.Log;

import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemTemplates;
import network.Message;
import real.controller.IController;
import real.dto.response.TradeMoveItemResponseDTO;
import utils.IOUtils;

public class MoveItemTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            TradeMoveItemResponseDTO tradeMoveItemResponseDTO = (TradeMoveItemResponseDTO) IOUtils.readMessage(msg, TradeMoveItemResponseDTO.class);

            long charId = tradeMoveItemResponseDTO.getId();
            if (charId == Char.myChar().charID)
            {
                for (int i = 0; i < Char.myChar().ItemMyTrade.length; i++)
                {
                    if (Char.myChar().ItemMyTrade[i] == null)
                    {
                        Char.myChar().ItemMyTrade[i] = new Item();
                        Char.myChar().ItemMyTrade[i].itemId = tradeMoveItemResponseDTO.getIdItem();
                        short iditemTemplate = tradeMoveItemResponseDTO.getIdTemplate();
                        if (iditemTemplate != -1)
                            Char.myChar().ItemMyTrade[i].template = ItemTemplates.get(iditemTemplate);

                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++)
                {
                    // add vao danh sach item partner
                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] == null)
                    {
                        Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
                        Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = tradeMoveItemResponseDTO.getIdItem();
                        short iditemTemplate = tradeMoveItemResponseDTO.getIdTemplate();
                        if (iditemTemplate != -1)
                            Char.myChar().partnerTrade.ItemParnerTrade[i].template =
                                ItemTemplates.get(iditemTemplate);

                        break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(MoveItemTradeController.class.getName(), "Failed to move item", e);
        }

    }
}