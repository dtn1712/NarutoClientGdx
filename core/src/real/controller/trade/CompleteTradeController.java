package real.controller.trade;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import network.Message;
import real.controller.IController;

public class CompleteTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            for (int i = 0; i < Char.myChar().ItemMyTrade.length; i++)
            {
                Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                Char.myChar().ItemMyTrade[i] = null;
            }
            Char.myChar().partnerTrade = null;
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.trade = null;
        }
        catch (Exception e)
        {
            Gdx.app.error(CompleteTradeController.class.getName(), "Failed to complete trade", e);
        }

    }
}