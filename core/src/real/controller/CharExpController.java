package real.controller;

import android.util.Log;

import Objectgame.Char;
import lib.MFont;
import network.Message;
import real.dto.response.CharExpResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CharExpController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            CharExpResponseDTO charExpResponseDTO = (CharExpResponseDTO) IOUtils.readMessage(msg, CharExpResponseDTO.class);
            long idCharAttacker = charExpResponseDTO.getCharId();
            long expCharAttacker = charExpResponseDTO.getExp();
            Char charAttacker = GameScreen.getInstance().findCharInMap(idCharAttacker);
            if (charAttacker != null) {
                GameScreen.startFlyText("+" + expCharAttacker + "exp", charAttacker.cx, charAttacker.cy - charAttacker.ch - 5, 0, -2, MFont.YELLOW);
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(CharExpController.class.getName(), "Failed to handle char exp", e);
        }

    }
}