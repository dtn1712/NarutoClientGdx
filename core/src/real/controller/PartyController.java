package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.util.List;

import Objectgame.Char;
import lib.Command;
import model.MResources;
import model.Party;
import network.Message;
import real.dto.response.PartyMemberInfoDTO;
import real.dto.response.PartyResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class PartyController implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            PartyResponseDTO partyResponseDTO = (PartyResponseDTO) IOUtils.readMessage(message, PartyResponseDTO.class);

            byte typeParty = partyResponseDTO.getType();
            if (typeParty == Party.INVITE_PARTY)
            {
                Party.getInstance().CharId = partyResponseDTO.getId();
                GameCanvas.startYesNoDlg(partyResponseDTO.getContent(), GameScreen.getInstance().cmdAcceptParty,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            }
            else if (typeParty == Party.GET_INFO_PARTY)
            {
                int partyId = partyResponseDTO.getIdParty();
                long charLeaderId = partyResponseDTO.getIdCharLeader();
                if (charLeaderId == Char.myChar().charID)
                {
                    Party.getInstance().IsLeader = true;
                }
                int memberSize = partyResponseDTO.getPartyMemberInfo().size();
                List<PartyMemberInfoDTO> infoMemberParty = partyResponseDTO.getPartyMemberInfo();

                long[] memberIdList = new long[memberSize];
                short[] memberLvList = new short[memberSize];
                short[] memberHeadIdList = new short[memberSize];
                for (int i = 0; i < memberSize; i++)
                {
                    memberIdList[i] = infoMemberParty.get(i).getCharIdMember();
                    memberLvList[i] = infoMemberParty.get(i).getLvChar();
                    memberHeadIdList[i] = infoMemberParty.get(i).getIdHead();
                }

                Party.getInstance().SetParty(partyId, memberIdList, charLeaderId, memberLvList, memberHeadIdList);
                GameScreen.hParty.put(partyId + "", Party.getInstance());
            }
            else if (typeParty == Party.OUT_PARTY)
            {
                long idChar = partyResponseDTO.getId();
                if (idChar == Char.myChar().charID)
                {
                    GameScreen.hParty.clear();
                    Party.vCharinParty.removeAllElements();
                }

                Char chaRe = GameScreen.getInstance().findCharInMap(idChar);
                if (chaRe != null)
                {
                    for (int i = 0; i < Party.vCharinParty.size(); i++)
                    {
                        Char charRemove = Party.vCharinParty.elementAt(i);
                        if (charRemove.charID == idChar)
                        {
                            charRemove.idParty = -1;
                            charRemove.isLeader = false;
                            Party.vCharinParty.removeElementAt(i);
                        }
                    }
                    if (Party.vCharinParty.size() < 2)
                    {
                        GameScreen.hParty.clear();
                        Party.vCharinParty.removeAllElements();
                    }
                }
                else
                {
                    chaRe = Char.myChar();
                    for (int i = 0; i < Party.vCharinParty.size(); i++)
                    {
                        Char charRemove = Party.vCharinParty.elementAt(i);
                        if (charRemove.charID == idChar)
                        {
                            charRemove.idParty = -1;
                            charRemove.isLeader = false;
                            Party.vCharinParty.removeElementAt(i);
                        }
                    }
                }
                if (Party.vCharinParty.size() <= 0)
                {
                    GameScreen.hParty.remove(chaRe.idParty + "");
                }
            }
            else if (typeParty == Party.DISBAND_PARTY)
            {
                int idParty = partyResponseDTO.getIdParty();
                for (int i = 0; i < Party.vCharinParty.size(); i++)
                {
                    Char ch = Party.vCharinParty.elementAt(i);
                    ch.idParty = -1;
                    ch.isLeader = false;
                    Party.vCharinParty.removeElementAt(i);
                }
                GameScreen.hParty.remove(idParty + "");
            }

        }
        catch (Exception e)
        {
            Gdx.app.error(PartyController.class.getName(), "Failed to handle party", e);
        }

    }
}