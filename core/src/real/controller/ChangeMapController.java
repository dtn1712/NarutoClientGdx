package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.List;

import Gui.MenuIcon;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.FallingLeafEffect;
import Objectgame.ItemMap;
import Objectgame.Mob;
import Objectgame.MobTemplate;
import Objectgame.TileMap;
import Objectgame.minigame.MiniGameManager;
import lib.MBitmap;
import lib.MSystem;
import lib.Music;
import lib.Rms;
import model.CRes;
import model.Effect;
import model.EffectData;
import model.QuickSlot;
import model.SmallImage;
import model.Waypoint;
import network.Message;
import real.Service;
import real.dto.response.ImageMonsterDTO;
import real.dto.response.LineMapInfoResponseDTO;
import real.dto.response.MapInfoResponseDTO;
import screen.DownloadImageScreen;
import screen.GameScreen;
import screen.WaitingScreen;
import utils.IOUtils;

public class ChangeMapController implements IController {

    public void processMessage(Message msg) {
        try {
            MapInfoResponseDTO mapInfoResponseDTO = (MapInfoResponseDTO) IOUtils.readMessage(msg, MapInfoResponseDTO.class);

            GameScreen.getInstance().loadData();
            GameScreen.getInstance().guiMain.moveClose = false;
            GameScreen.getInstance().guiMain.menuIcon.indexpICon = 0;

            MenuIcon.isShowTab = false;
            GameScreen.getInstance().vNpc.removeAllElements();
            TileMap.getInstance().vGo.removeAllElements();
            GameScreen.getInstance().vMob.removeAllElements();
            GameScreen.getInstance().vItemMap.removeAllElements();
            GameScreen.getInstance().vNhatItemMap.removeAllElements();
            Char.isChangingMap = true;
            GameScreen.getInstance().vCharInMap.removeAllElements();
            GameScreen.getInstance().vCharInMap.addElement(Char.myChar());
            TileMap.mapID = mapInfoResponseDTO.getId();
            if (MiniGameManager.getInstance().isInMiniGame) {
                for (int i = 1; i < Char.myChar().mQuickslot.length; i++) {
                    Char.myChar().mQuickslot[i].idSkill = -1;

                }
            } else {
                for (int i = 1; i < Char.myChar().mQuickslot.length; i++) {
                    QuickSlot.loadRmsQuickSlot();
                }
            }

            for (ImageMonsterDTO imageMonsterDTO : mapInfoResponseDTO.getListMonsterImage()) {
                int size = imageMonsterDTO.getData().size();
                for (int i = 0; i < size; i++) {
                    byte[] dataImg = imageMonsterDTO.getData().get(i);
                    String path = SmallImage.getPathMonsterImage() + Integer.toString(imageMonsterDTO.getMonsterTemplateId()) + "/_" + Integer.toString((size == 2 ? (i == 0 ? 1 : 4) : i + 1));
                    Rms.saveRMS(MSystem.getPathRMS(path), dataImg);
                }
            }

            Effect.vEffect2.removeAllElements();

            if (TileMap.getInstance().listBgMap.containsKey(TileMap.mapID)) {
                TileMap.getInstance().bg = TileMap.getInstance().listBgMap.get(TileMap.mapID);
            }

            GameCanvas.getInstance().loadBG(TileMap.getInstance().bg);

            TileMap.getInstance().mapName = TileMap.getInstance().listNameAllMap.get(TileMap.mapID);
            TileMap.getInstance().zoneID = mapInfoResponseDTO.getRegion();
            TileMap.getInstance().zoneIdDisplay = mapInfoResponseDTO.getRegion() + 1 + "";

            for (LineMapInfoResponseDTO lineMapInfoResponseDTO : mapInfoResponseDTO.getListLineMap()) {
                TileMap.getInstance().vGo.addElement(new Waypoint(lineMapInfoResponseDTO.getMapTemplateId(),
                        lineMapInfoResponseDTO.getMinX(), lineMapInfoResponseDTO.getMinY(),
                        lineMapInfoResponseDTO.getMaxX(), lineMapInfoResponseDTO.getMaxY()));
            }

            Char.myChar().cx = mapInfoResponseDTO.getCharX();
            Char.myChar().cy = mapInfoResponseDTO.getCharY();
            Char.myChar().statusMe = Char.A_FALL;

            TileMap.tileID = mapInfoResponseDTO.getMapTileId();
            int nTile = mapInfoResponseDTO.getListMapHtData().size();

            TileMap.getInstance().tileIndex = new int[nTile][][];
            TileMap.getInstance().tileType = new int[nTile][];
            try {
                for (int i = 0; i < nTile; i++) {
                    int nTypeSize = mapInfoResponseDTO.getListMapHtData().get(i).getSize();
                    TileMap.getInstance().tileType[i] = new int[nTypeSize];
                    TileMap.getInstance().tileIndex[i] = new int[nTypeSize][];

                    for (int a = 0; a < nTypeSize; a++) {
                        TileMap.getInstance().tileType[i][a] = mapInfoResponseDTO.getListMapHtData().get(i).getListDataKeySet().get(a).getKeyType();
                        int sizeIndex = mapInfoResponseDTO.getListMapHtData().get(i).getListDataKeySet().get(a).getLength();
                        TileMap.getInstance().tileIndex[i][a] = new int[sizeIndex];
                        for (int b = 0; b < sizeIndex; b++) {
                            TileMap.getInstance().tileIndex[i][a][b] = mapInfoResponseDTO.getListMapHtData().get(i).getListDataKeySet().get(a).getData().get(b);
                        }
                    }
                }
            } catch (Exception e) {
                Gdx.app.error(ChangeMapController.class.getName(), "Failed to handle tile map", e);
            }

            TileMap.getInstance().loadImgTile(TileMap.tileID); // load hinh tile
            TileMap.getInstance().loadMapFile(); // load file map
            TileMap.getInstance().LoadMap(); // load va cham
            FallingLeafEffect.load();
            GameScreen.loadMapItem(); // load toan bo file data object
            GameScreen.loadMapTable(TileMap.mapID); // object trong 1 map


            TileMap.getInstance().vItemBg.removeAllElements();
            BgItem.imgPathLoad.clear();
            Mob.imgMob.clear();

            int sizeMod = mapInfoResponseDTO.getListMonster().size();
            Mob.arrMobTemplate = new MobTemplate[sizeMod];
            try {
                for (int i = 0; i < Mob.arrMobTemplate.length; i++) {
                    Mob mob = new Mob(mapInfoResponseDTO.getListMonster().get(i).getId(),
                            mapInfoResponseDTO.getListMonster().get(i).getHp(),
                            mapInfoResponseDTO.getListMonster().get(i).getLevel(),
                            mapInfoResponseDTO.getListMonster().get(i).getMaxHp(),
                            mapInfoResponseDTO.getListMonster().get(i).getMonX(),
                            mapInfoResponseDTO.getListMonster().get(i).getMonY());

                    short monsterTemplateId = mapInfoResponseDTO.getListMonster().get(i).getIdTemplate();
                    MobTemplate mobTemplate = new MobTemplate();
                    mobTemplate.mobTemplateId = monsterTemplateId;
                    mobTemplate.type = mapInfoResponseDTO.getListMonster().get(i).getType();
                    mobTemplate.speed = mapInfoResponseDTO.getListMonster().get(i).getSpeed();
                    mobTemplate.rangeMove = mapInfoResponseDTO.getListMonster().get(i).getRangeMove();

                    int size = 8;
                    if (monsterTemplateId != Mob.TYPE_MOCNHAN) {
                        for (ImageMonsterDTO imageMonsterDTO : mapInfoResponseDTO.getListMonsterImage()) {
                            if (mobTemplate.mobTemplateId == imageMonsterDTO.getMonsterTemplateId()) {
                                size = imageMonsterDTO.getData().size();
                            }
                        }
                    }

                    mobTemplate.imgs = new MBitmap[size];

                    mob.isBoss =  mapInfoResponseDTO.getListMonster().get(i).isBoss();
                    mob.isBossAppear = mapInfoResponseDTO.getListMonster().get(i).isBossAppear();

                    if (mob.isBoss) {
                        byte[] dataImage = mapInfoResponseDTO.getListMonster().get(i).getInfoImage();
                        byte[] dataFrame = mapInfoResponseDTO.getListMonster().get(i).getInfoFrame();
                        mob.typeMob = Mob.TYPE_MOB_TOOL;
                        mobTemplate.data = new EffectData(); // doc file data quai trong tool
                        mobTemplate.data.readData(new DataInputStream(new ByteArrayInputStream(dataImage)));
                        mobTemplate.data.readhd(new DataInputStream(new ByteArrayInputStream(dataFrame)));
                    }

                    mob.idloadimage = mapInfoResponseDTO.getListMonster().get(i).getIdTemplate();
                    GameScreen.getInstance().vMob.addElement(mob);

                    Mob.arrMobTemplate[i] = mobTemplate;
                }
            } catch (Exception e) {
                Gdx.app.error(ChangeMapController.class.getName(), "Failed to handle mob", e);
            }

            try {
                int sizeItemMap = mapInfoResponseDTO.getListItem().size();
                for (int i = 0; i < sizeItemMap; i++) {
                    int itemId = mapInfoResponseDTO.getListItem().get(i).getId();
                    short itemTemplateId = mapInfoResponseDTO.getListItem().get(i).getIdTemplate();
                    short itemX = mapInfoResponseDTO.getListItem().get(i).getItemX();
                    short itemY = mapInfoResponseDTO.getListItem().get(i).getItemY();
                    ItemMap itemDropmap = new ItemMap(itemId, itemTemplateId, itemX, itemY);

                    GameScreen.getInstance().vItemMap.addElement(itemDropmap);
                    GameScreen.getInstance().vNhatItemMap.addElement(itemDropmap);
                }
            } catch (Exception e) {
                Gdx.app.error(ChangeMapController.class.getName(), "Failed to handle item", e);
            }

            Service.getInstance().requestInventory();

            Char.myChar().statusMe = Char.A_FALL;
            GameScreen.loadCamera(Char.myChar().cx, Char.myChar().cy);
            GameCanvas.getInstance().endDlg();

            Music.play(CRes.random(1, 4), 0.3f);

            Char.isChangingMap = false;

            if (GameScreen.getInstance().guiMain.menuIcon.cmdClose != null) {
                GameScreen.getInstance().guiMain.menuIcon.cmdClose.performAction();
            }

            List<String> idObjMap = new ArrayList<>();
            for (int i = 0; i < TileMap.getInstance().vCurrItem.size(); i++) {
                BgItem biMap = TileMap.getInstance().vCurrItem.elementAt(i);
                if (biMap != null) {
                    idObjMap.add("mapobject/" + Integer.toString(biMap.idImage));
                }
            }

            GameCanvas.getInstance().nextScreen = WaitingScreen.getInstance();
            DownloadImageScreen.getInstance().switchToMe(idObjMap);
        } catch (Exception e) {
            e.printStackTrace();
            Gdx.app.error(ChangeMapController.class.getName(), "[processMessage] Exception", e);
        }
    }
}