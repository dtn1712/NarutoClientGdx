package real.controller;

import android.util.Log;

import Objectgame.Npc;
import Objectgame.quest.MainQuestManager;
import network.Message;
import real.dto.response.NpcResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class NpcController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            NpcResponseDTO npcResponseDTO = (NpcResponseDTO) IOUtils.readMessage(msg, NpcResponseDTO.class);

            GameScreen.getInstance().vNpc.removeAllElements();
            int sizeNpc = npcResponseDTO.getListNpc().size();
            for (int i = 0; i < sizeNpc; i++) {
                short npcX = npcResponseDTO.getListNpc().get(i).getNpcX();
                short npcY = npcResponseDTO.getListNpc().get(i).getNpcY();
                short npcTemplateId = npcResponseDTO.getListNpc().get(i).getTemplateId();

                Npc npcz = new Npc(npcX, npcY, npcTemplateId);
                if (MainQuestManager.getInstance().NewQuest != null &&
                        MainQuestManager.getInstance().NewQuest.IdNpcReceive == npcTemplateId) {
                    npcz.typeNV = 0;
                }

                if (MainQuestManager.getInstance().WorkingQuest != null &&
                        MainQuestManager.getInstance().WorkingQuest.IdNpcResolve == npcTemplateId) {
                    npcz.typeNV = 1;
                }

                if (MainQuestManager.getInstance().FinishQuest != null &&
                        (MainQuestManager.getInstance().FinishQuest.IdNpcReceive == npcTemplateId
                                || MainQuestManager.getInstance().FinishQuest.IdNpcResolve == npcTemplateId)) {
                    npcz.typeNV = 1;
                }

                GameScreen.getInstance().vNpc.addElement(npcz);
            }
        } catch (Exception e) {
            Gdx.app.error(NpcController.class.getName(), "Failed to get npc", e);
        }
    }
}