package real.controller;

import android.util.Log;

import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemTemplate;
import Objectgame.ItemTemplates;
import network.Message;
import real.dto.character.CharWearingResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CharWearingController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            Char c = new Char();
            CharWearingResponseDTO charWearingResponseDTO = (CharWearingResponseDTO) IOUtils.readMessage(msg, CharWearingResponseDTO.class);

            if (Char.myChar().charID != charWearingResponseDTO.getPlayerId())
            {
                boolean isAdded = false;
                for (int i = 0; i < GameScreen.getInstance().vCharInMap.size(); i++)
                {
                    Char dem = GameScreen.getInstance().vCharInMap.elementAt(i);
                    if (dem != null && dem.charID == charWearingResponseDTO.getPlayerId())
                    {
                        isAdded = true;
                        c = dem;
                    }
                }
                if (!isAdded)
                {
                    c = new Char();
                    c.charID = charWearingResponseDTO.getPlayerId();
                }
                readCharInMap(c, charWearingResponseDTO);
                if (!isAdded)
                {
                    GameScreen.getInstance().vCharInMap.addElement(c);
                }
            }
            else
            {
                Char.myChar().arrItemBody = new Item[charWearingResponseDTO.getItems().length];
                for (int i = 0; i < Char.myChar().arrItemBody.length; i++)
                {
                    CharWearingResponseDTO.CharEquipmentItemDTO item = charWearingResponseDTO.getItems()[i];
                    if (item == null) continue;
                    ItemTemplate template = ItemTemplates.get(item.getItemTemplateId());
                    int indexUI = Item.getPosWearingItem(template.type);
                    if (indexUI >= Char.myChar().arrItemBody.length)
                    {
                        continue;
                    }

                    Char.myChar().arrItemBody[indexUI] = new Item();
                    Char.myChar().arrItemBody[indexUI].indexUI = indexUI;
                    Char.myChar().arrItemBody[indexUI].template = template;
                    Char.myChar().arrItemBody[indexUI].isLock = true;

                    Char.myChar().arrItemBody[indexUI].template.lvItem = charWearingResponseDTO.getItems()[i].getLevelUpgrade();

                    Char.myChar().arrItemBody[indexUI].KhaiMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getKhaiMon();
                    Char.myChar().arrItemBody[indexUI].HuuMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getHuuMon();
                    Char.myChar().arrItemBody[indexUI].DoMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getDoMon();
                    Char.myChar().arrItemBody[indexUI].CanhMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getCanhMon();
                    Char.myChar().arrItemBody[indexUI].TuMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getTuMon();
                    Char.myChar().arrItemBody[indexUI].KinhMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getKinhMon();
                    Char.myChar().arrItemBody[indexUI].SinhMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getSinhMon();
                    Char.myChar().arrItemBody[indexUI].ThuongMon = charWearingResponseDTO.getItems()[i].getAddedAttribute().getThuongMon();

                    if (template.type == Item.TYPE_SKIN)
                    {
                        Char.myChar().body = GameScreen.currentCharViewInfo.arrItemBody[indexUI]
                            .template.part;
                        Char.myChar().leg = GameScreen.currentCharViewInfo.arrItemBody[indexUI]
                            .template.partquan;
                        Char.myChar().head = GameScreen.currentCharViewInfo.arrItemBody[indexUI]
                            .template.partdau;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(CharWearingController.class.getName(), "Failed to handle char wearing", e);
        }
    }


    private void readCharInMap(Char c, CharWearingResponseDTO charWearingResponseDto)
    {
        try
        {
            c.arrItemBody = new Item[charWearingResponseDto.getItems().length];
            for (int i = 0; i < c.arrItemBody.length; i++)
            {
                CharWearingResponseDTO.CharEquipmentItemDTO item = charWearingResponseDto.getItems()[i];
                if (item == null) continue;
                ItemTemplate template = ItemTemplates.get(item.getItemTemplateId());
                int indexUI = Item.getPosWearingItem(template.type);

                c.arrItemBody[indexUI] = new Item();
                c.arrItemBody[indexUI].indexUI = indexUI;
                c.arrItemBody[indexUI].template = template;
                c.arrItemBody[indexUI].isLock = true;
                c.arrItemBody[indexUI].template.lvItem = charWearingResponseDto.getItems()[i].getLevelUpgrade();
                c.arrItemBody[indexUI].KhaiMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getKhaiMon();
                c.arrItemBody[indexUI].HuuMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getHuuMon();
                c.arrItemBody[indexUI].DoMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getDoMon();
                c.arrItemBody[indexUI].CanhMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getCanhMon();
                c.arrItemBody[indexUI].TuMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getTuMon();
                c.arrItemBody[indexUI].KinhMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getKinhMon();
                c.arrItemBody[indexUI].SinhMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getSinhMon();
                c.arrItemBody[indexUI].ThuongMon = charWearingResponseDto.getItems()[i].getAddedAttribute().getThuongMon();

                if (template.type == Item.TYPE_SKIN)
                {
                    c.body = c.arrItemBody[indexUI].template.part;
                    c.leg = c.arrItemBody[indexUI].template.partquan;
                    c.head = c.arrItemBody[indexUI].template.partdau;
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(CharWearingController.class.getName(), "Failed to read char in map", e);
        }
    }
}