package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import Objectgame.Mob;
import Objectgame.TileMap;
import network.Message;
import real.dto.response.ChangeRegionResponseDTO;
import screen.GameScreen;
import screen.WaitingScreen;
import utils.IOUtils;

public class ChangeRegionController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            ChangeRegionResponseDTO changeRegionResponseDTO = (ChangeRegionResponseDTO) IOUtils.readMessage(msg, ChangeRegionResponseDTO.class);

            TileMap.getInstance().zoneID = changeRegionResponseDTO.getRegion();
            TileMap.getInstance().zoneIdDisplay = changeRegionResponseDTO.getRegion() + 1 + "";

            Char.myChar().cx = changeRegionResponseDTO.getCharX();
            Char.myChar().cy = changeRegionResponseDTO.getCharY();

            for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++)
            {
                Mob dem = GameScreen.getInstance().vMob.elementAt(i);
                dem.startDie();
            }
            GameScreen.getInstance().vItemMap.removeAllElements();
            GameScreen.getInstance().vCharInMap.removeAllElements();
            WaitingScreen.getInstance().isChangeKhu = true;
            WaitingScreen.getInstance().switchToMe();
            GameScreen.getInstance().vCharInMap.add(Char.myChar());
            Char.isChangingMap = false;
            Char.myChar().statusMe = Char.A_FALL;
            GameScreen.loadCamera(Char.myChar().cx, Char.myChar().cy);
            GameCanvas.getInstance().endDlg();
        }
        catch (Exception e)
        {
            Gdx.app.error(ChangeRegionController.class.getName(), "Failed to change region", e);
        }
    }
}