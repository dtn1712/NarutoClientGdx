package real.controller;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.dto.response.CharPositionResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CharPositionController implements IController {

    public void processMessage(Message msg) {
        try {
            CharPositionResponseDTO charPositionResponseDTO = (CharPositionResponseDTO) IOUtils.readMessage(msg, CharPositionResponseDTO.class);
            long charId = charPositionResponseDTO.getId();
            short cx = charPositionResponseDTO.getX();
            short cy = charPositionResponseDTO.getY();
            String cName = charPositionResponseDTO.getName();
            int typePk = charPositionResponseDTO.getStatusPK();
            for (int i = 0; i < GameScreen.getInstance().vCharInMap.size(); i++) {
                Char c = GameScreen.getInstance().vCharInMap.elementAt(i);
                if (c.charID != charId) continue;

//                byte cStatus = getStatus(cx - c.cx, cy - c.cy);
                int cDir = getDirection(cx - c.cx, cy - c.cy);

                if (cDir != 0) {
                    c.cdir = cDir;
                }

                c.cName = cName;
                c.typePk = typePk;
                c.cx = cx;
                c.cy = cy;
                c.lastUpdateTime = System.currentTimeMillis();
            }
        } catch (Exception e) {
            Gdx.app.error(CharPositionController.class.getName(), "Failed to handle char position", e);
        }
    }

//    private byte getStatus(int dx, int dy) {
//        if (dx == 0 && dy == 0) {
//            return Char.A_STAND;
//        } else if (dy == 0) {
//            return Char.A_RUN;
//        } else if (dy < 0) {
//            return Char.A_JUMP;
//        } else {
//            return Char.A_FALL;
//        }
//    }

    private int getDirection(int dx, int dy) {
        int dir = 0;
        if (dy == 0) {
            if (dx > 0) {
                dir = 1;
            } else if (dx < 0) {
                dir = -1;
            }
        } else {
            if (dx > 0) {
                dir = 1;
            } else if (dx < 0) {
                dir = -1;
            }
        }
        return dir;
    }
}