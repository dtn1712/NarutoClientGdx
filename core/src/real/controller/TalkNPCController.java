package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.util.Vector;

import Objectgame.Char;
import network.Message;
import real.dto.response.TalkNPCResponseDTO;
import utils.IOUtils;

public class TalkNPCController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            TalkNPCResponseDTO talkNPCResponseDTO = (TalkNPCResponseDTO) IOUtils.readMessage(message, TalkNPCResponseDTO.class);
            GameCanvas.menu.startAtNPC(new Vector(), Char.myChar().npcFocus, talkNPCResponseDTO.getContentChat());
        } catch (Exception e) {
            Gdx.app.error(TalkNPCController.class.getName(), "Failed to talk to npc", e);
        }
    }
}