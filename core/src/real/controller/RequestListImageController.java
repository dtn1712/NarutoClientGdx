package real.controller;

import android.util.Log;

import network.Message;
import real.dto.response.ListImageResponseDTO;
import screen.DownloadImageScreen;
import utils.IOUtils;

public class RequestListImageController implements IController {
    @Override
    public void processMessage(Message message) {
        try {
            ListImageResponseDTO listImageResponseDTO = (ListImageResponseDTO) IOUtils.readMessage(message, ListImageResponseDTO.class);

            DownloadImageScreen.getInstance().switchToMe(listImageResponseDTO.getListImageKeys());

        } catch (Exception e) {
            Gdx.app.error(RequestListImageController.class.getName(), "Exception", e);
        }
    }
}
