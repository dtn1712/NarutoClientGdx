package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import network.Message;
import real.dto.response.TradeTakeOffResponseDTO;
import utils.IOUtils;

public class TakeOffItemTradeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            TradeTakeOffResponseDTO tradeTakeOffResponseDTO = (TradeTakeOffResponseDTO) IOUtils.readMessage(msg, TradeTakeOffResponseDTO.class);

            long charId = tradeTakeOffResponseDTO.getId();
            if (Char.myChar().charID == charId) {
                int itemId = tradeTakeOffResponseDTO.getItemId();
                for (int i = 0; i < Char.myChar().ItemMyTrade.length; i++)
                    if (Char.myChar().ItemMyTrade[i] != null)
                        if (Char.myChar().ItemMyTrade[i].itemId == itemId) {
                            Char.myChar().ItemMyTrade[i] = null;
                            GameCanvas.getInstance().gameScreen.guiMain.menuIcon.trade.indexSelect1 = -1;
                            break;
                        }
            } else {
                int itemId = tradeTakeOffResponseDTO.getItemId();
                for (int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++)
                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] != null)
                        if (Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == itemId)
                            Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
            }
        } catch (Exception e) {
            Gdx.app.error(TakeOffItemTradeController.class.getName(), "Failed to take off item", e);
        }
    }
}