package real.controller;

import android.util.Log;

import Gui.Constants;
import Objectgame.Char;
import Objectgame.ItemMap;
import Objectgame.Mob;
import network.Message;
import real.dto.response.ItemDropResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ItemDropController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ItemDropResponseDTO itemDropResponseDTO = (ItemDropResponseDTO) IOUtils.readMessage(msg, ItemDropResponseDTO.class);

            byte actorType = itemDropResponseDTO.getActorType(); //1 Ä‘Ã¡nh quÃ¡i rá»›t ra, 0 char vá»©t ra.
            long idMonterDie = itemDropResponseDTO.getActorId();
            int itemId = itemDropResponseDTO.getItemId();
            short itemTemplateId = itemDropResponseDTO.getIdTemplate();
            ItemMap itemDrop = null;
            if (actorType == Constants.TYPE_MONSTER) {
                Mob mons = GameScreen.getInstance().findMobInMap(idMonterDie);
                if (mons != null) {
                    itemDrop = new ItemMap(itemId, itemTemplateId, mons.x, mons.yFirst + 10);
                }
                GameScreen.getInstance().vItemMap.addElement(itemDrop);
                GameScreen.getInstance().vNhatItemMap.addElement(itemDrop);
            } else {
                Char ch = GameScreen.getInstance().findCharInMap(idMonterDie);
                if (ch != null) {
                    itemDrop = new ItemMap(itemId, itemTemplateId, ch.cx, ch.cy);
                    GameScreen.getInstance().vItemMap.addElement(itemDrop);
                    GameScreen.getInstance().vNhatItemMap.addElement(itemDrop);
                }
            }
        } catch (Exception e) {
            Gdx.app.error(ItemDropController.class.getName(), "Failed to handle item drop", e);
        }

    }
}