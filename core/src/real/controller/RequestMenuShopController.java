package real.controller;

import android.util.Log;

import Gui.ShopMain;
import lib.Command;
import network.Message;
import real.dto.response.MenuShopResponseDTO;
import utils.IOUtils;

public class RequestMenuShopController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            MenuShopResponseDTO menuShopResponseDTO = (MenuShopResponseDTO) IOUtils.readMessage(msg, MenuShopResponseDTO.class);

            int menuShopSize = menuShopResponseDTO.getListKey().size(); // soluong menu shop

            ShopMain.idMenu = new int[menuShopSize];
            ShopMain.nameMenu = new String[menuShopSize];
            ShopMain.dis = new String[menuShopSize];
            ShopMain.cmdShop = new Command[menuShopSize];

            for (int i = 0; i < menuShopSize; i++)
            {
                ShopMain.idMenu[i] = menuShopResponseDTO.getListKey().get(i).getKey();
                ShopMain.nameMenu[i] = menuShopResponseDTO.getListKey().get(i).getName();
                ShopMain.dis[i] = menuShopResponseDTO.getListKey().get(i).getDescription();
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(RequestMenuShopController.class.getName(), "Failed to request menu shop", e);
        }

    }
}