package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.MGraphics;

import network.Message;
import network.Session;
import real.Service;
import real.dto.response.RequestAuthKeyResponseDTO;
import screen.LoginScreen;
import utils.IOUtils;

public class RequestAuthKeyController  implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            RequestAuthKeyResponseDTO requestAuthKeyResponseDTO = (RequestAuthKeyResponseDTO) IOUtils.readMessage(message, RequestAuthKeyResponseDTO.class);

            Session.getInstance().setKey(requestAuthKeyResponseDTO.getKey());
            Session.getInstance().setSessionId(requestAuthKeyResponseDTO.getSessionId());

            if (LoginScreen.getInstance().getAction().equals(LoginScreen.LOGIN_ACTION))
            {
                Service.getInstance().DoLogin(LoginScreen.getInstance().getUser(), LoginScreen.getInstance().getPass(),
                    (byte) MGraphics.zoomLevel);
            }
            else if (LoginScreen.getInstance().getAction().equals(LoginScreen.REGISTER_ACTION))
            {
                Service.getInstance()
                    .DoRegister(LoginScreen.getInstance().getUser(), LoginScreen.getInstance().getPass());
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(RequestAuthKeyController.class.getName(), "Failed to request auth key", e);
        }
    }
}