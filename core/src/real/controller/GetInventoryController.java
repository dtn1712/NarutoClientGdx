package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.util.Vector;

import Gui.MainTabNew;
import Gui.TabBag;
import Gui.TabInfoChar;
import Gui.TabMySeftNew;
import Gui.TabNangCap;
import Gui.TabSkill;
import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemTemplates;
import network.Message;
import real.dto.character.CharInventoryDTO;
import utils.IOUtils;

public class GetInventoryController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            CharInventoryDTO charInventoryDTO = (CharInventoryDTO) IOUtils.readMessage(msg, CharInventoryDTO.class);

            Char.myChar().luong = charInventoryDTO.getGold();
            Char.myChar().xu = charInventoryDTO.getXu();
            Char.myChar().arrItemBag = new Item[charInventoryDTO.getItems().size()];
            Char.myChar().inventorySize = charInventoryDTO.getInventorySize();

            TabNangCap.numberItemInBag = Char.myChar().arrItemBag.length;
            for (int i = 0; i < Char.myChar().arrItemBag.length; i++)
            {
                // add them theo cap do nang cap lvItem
                CharInventoryDTO.CharInventoryItemDTO charItemDto = charInventoryDTO.getItems().get(i);
                if (charItemDto.getItemTemplateId() != -1)
                {
                    Char.myChar().arrItemBag[i] = new Item();
                    Char.myChar().arrItemBag[i].itemId = charItemDto.getPositionIndex();
                    Char.myChar().arrItemBag[i].indexUI = i;
                    Char.myChar().arrItemBag[i].template = ItemTemplates.get(charItemDto.getItemTemplateId());
                    Char.myChar().arrItemBag[i].template.lvItem = charItemDto.getLevelUpgrade();
                    Char.myChar().arrItemBag[i].KhaiMon = charItemDto.getAddedAttribute().getKhaiMon();
                    Char.myChar().arrItemBag[i].HuuMon = charItemDto.getAddedAttribute().getHuuMon();
                    Char.myChar().arrItemBag[i].SinhMon = charItemDto.getAddedAttribute().getSinhMon();
                    Char.myChar().arrItemBag[i].DoMon = charItemDto.getAddedAttribute().getDoMon();
                    Char.myChar().arrItemBag[i].CanhMon = charItemDto.getAddedAttribute().getCanhMon();
                    Char.myChar().arrItemBag[i].ThuongMon = charItemDto.getAddedAttribute().getThuongMon();
                    Char.myChar().arrItemBag[i].KinhMon = charItemDto.getAddedAttribute().getKinhMon();
                    Char.myChar().arrItemBag[i].TuMon = charItemDto.getAddedAttribute().getTuMon();
                    Char.myChar().arrItemBag[i].Luck = charItemDto.getAddedAttribute().getLuck();

                    if (Char.myChar().arrItemBag[i].template != null &&  Item.isBlood(Char.myChar().arrItemBag[i].template.type)
                        || Char.myChar().arrItemBag[i].template != null && Item.isMP(Char.myChar().arrItemBag[i].template.type))
                    {
                        Char.myChar().arrItemBag[i].quantity = charItemDto.getQuantity();
                        Char.myChar().arrItemBag[i].value = charItemDto.getValueBuff();
                    }
                }
            }

            Vector<MainTabNew> v = new Vector<MainTabNew>();
            v.addElement(new TabBag("Hành Trang"));
            v.addElement(new TabMySeftNew("Trang Bị"));
            v.addElement(new TabInfoChar("Thông tin"));
            v.addElement(new TabSkill("Kỹ năng"));
            v.addElement(new TabNangCap("Nâng cấp"));

            GameCanvas.allInfo.addMoreTab(v);
        }
        catch (Exception e)
        {
            Gdx.app.error(GetInventoryController.class.getName(), "Failed to get inventory controller", e);
        }
    }
}