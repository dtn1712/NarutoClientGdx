package real.controller;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.dto.character.StatusAttackResponseDTO;
import screen.FlagScreen;
import screen.GameScreen;
import utils.IOUtils;

public class StatusAttackController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            StatusAttackResponseDTO statusAttackResponseDTO = (StatusAttackResponseDTO) IOUtils.readMessage(msg, StatusAttackResponseDTO.class);

            long idchar = statusAttackResponseDTO.getId();
            int typeAttack = statusAttackResponseDTO.getStatus();
            boolean isClanWarAttack = statusAttackResponseDTO.isClanWarAttack();
            boolean isMiniGameAttack = statusAttackResponseDTO.isMiniGameAttack();

            if (Char.myChar().charID == idchar)
            {
                FlagScreen.time = System.currentTimeMillis();
            }

            Char ch = GameScreen.getInstance().findCharInMap(idchar);
            if (ch == null) return;

            ch.isClanWarAttack = isClanWarAttack;
            ch.isMiniGameAttack = isMiniGameAttack;
            ch.typePk = typeAttack;
        }
        catch (Exception e)
        {
            Gdx.app.error(StatusAttackController.class.getName(), "Failed to handle status attack", e);
        }
    }
}