package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import network.Message;
import real.dto.response.ErrorVersionResponseDTO;
import utils.IOUtils;

public class UpdateClientController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ErrorVersionResponseDTO errorVersionResponseDTO = (ErrorVersionResponseDTO) IOUtils.readMessage(msg, ErrorVersionResponseDTO.class);
            GameCanvas.startOK("Bạn đang dùng phiên bản cũ. Vui lòng tải phiên bản mới !", GameCanvas.cTaiVersionMoi, errorVersionResponseDTO.getLink());
        } catch (Exception e) {
            Gdx.app.error(UpdateClientController.class.getName(), "Failed to get client update", e);
        }
    }
}