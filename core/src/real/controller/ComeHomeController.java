package real.controller;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.dto.response.CharGoHomeResponseDTO;
import utils.IOUtils;

public class ComeHomeController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            CharGoHomeResponseDTO charGoHomeResponseDTO = (CharGoHomeResponseDTO) IOUtils.readMessage(msg, CharGoHomeResponseDTO.class);

            Char.myChar().statusMe = Char.A_FALL;
            Char.myChar().cHP = charGoHomeResponseDTO.getHp();
            Char.myChar().cMP = charGoHomeResponseDTO.getMp();
        } catch (Exception e) {
            Gdx.app.error(ComeHomeController.class.getName(), "Failed to come home", e);
        }

    }
}