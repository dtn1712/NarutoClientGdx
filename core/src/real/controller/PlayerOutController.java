package real.controller;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.dto.response.PlayerOutResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class PlayerOutController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            PlayerOutResponseDTO playerOutResponseDTO = (PlayerOutResponseDTO) IOUtils.readMessage(msg, PlayerOutResponseDTO.class);

            long charIdRemove = playerOutResponseDTO.getId();
            if (charIdRemove == Char.myChar().charID)
            {
                return;
            }

            for (int i = 0; i < GameScreen.getInstance().vCharInMap.size(); i++)
            {
                Char c = GameScreen.getInstance().vCharInMap.elementAt(i);
                if (charIdRemove == c.charID)
                {
                    GameScreen.getInstance().vCharInMap.removeElementAt(i);
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(PlayerOutController.class.getName(), "Failed to handle player out", e);
        }

    }
}