package real.controller;

import android.util.Log;

import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemTemplate;
import Objectgame.ItemTemplates;
import lib.FontManager;
import network.Message;
import real.dto.response.ItemTemplateResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ItemTemplateController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            ItemTemplateResponseDTO itemTemplateResponseDto = (ItemTemplateResponseDTO) IOUtils.readMessage(msg, ItemTemplateResponseDTO.class);

            GameScreen.currentCharViewInfo = Char.myChar();

            int nItemTemplate = itemTemplateResponseDto.getListItem().size(); // so luong template ID
            for (int i = 0; i < nItemTemplate; i++)
            {
                short templateId = itemTemplateResponseDto.getListItem().get(i).getId(); // id temp
                byte typeItem = itemTemplateResponseDto.getListItem().get(i).getType(); // loai
                byte gender = itemTemplateResponseDto.getListItem().get(i).getGender(); //  gioi tinh
                String name = itemTemplateResponseDto.getListItem().get(i).getName(); // ten
                String description = itemTemplateResponseDto.getListItem().get(i).getDescription(); // mo ta
                short levelRequired = itemTemplateResponseDto.getListItem().get(i).getLevelRequired(); // level
                short iconId = itemTemplateResponseDto.getListItem().get(i).getIcon(); // idIcon
                long giaitem = itemTemplateResponseDto.getListItem().get(i).getPrice();
                byte charClass = itemTemplateResponseDto.getListItem().get(i).getCharClass();
                byte country = itemTemplateResponseDto.getListItem().get(i).getCountry();
                short part = itemTemplateResponseDto.getListItem().get(i).getIdPartBody();
                int KhaiMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getKhaiMon();
                int HuuMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getHuuMon();
                int SinhMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getSinhMon();
                int DoMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getDoMon();
                int CanhMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getCanhMon();
                int ThuongMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getThuongMon();
                int KinhMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getKinhMon();
                int TuMon = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getTuMon();
                int Luck = itemTemplateResponseDto.getListItem().get(i).getBaseAttribute().getLuck();

                short partquan = -1;
                short partdau = -1;
                if (typeItem == Item.TYPE_SKIN)
                {
                    partquan = itemTemplateResponseDto.getListItem().get(i).getIdPartLeg();
                    partdau = itemTemplateResponseDto.getListItem().get(i).getIdPartHead();
                }

                ItemTemplate itt = typeItem != Item.TYPE_SKIN ?
                        new ItemTemplate(templateId, typeItem, gender, name, levelRequired, iconId, part, KhaiMon, HuuMon, SinhMon, DoMon, CanhMon, ThuongMon, KinhMon, TuMon, Luck ) :
                        new ItemTemplate(templateId, typeItem, gender, name, levelRequired, iconId, part, partquan, partdau, KhaiMon, HuuMon, SinhMon, DoMon, CanhMon, ThuongMon, KinhMon, TuMon, Luck);
                itt.descriptionPaint = FontManager.getInstance().tahoma_7_white.splitFontArray(description, 140);
                itt.gia = giaitem;
                itt.clazz = charClass;
                itt.country = country;
                itt.typeSell = (byte) ((itemTemplateResponseDto.getListItem().get(i).isSellByGold()) ? 1 : 0);

                if (!ItemTemplates.contains(itt.id))
                {
                    ItemTemplates.add(itt);
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(ItemTemplateController.class.getName(), "Failed to get item template", e);
        }
    }
}