package real.controller.minigame;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Objectgame.minigame.BombZone;
import Objectgame.minigame.MiniGameManager;
import network.Message;
import real.controller.IController;
import real.dto.response.BombZoneResponseDTO;
import utils.IOUtils;

public class MiniGameBombZoneController implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            BombZoneResponseDTO bombZoneResponseDTO = (BombZoneResponseDTO) IOUtils.readMessage(message, BombZoneResponseDTO.class);
            List<BombZone> bombZones = new ArrayList<BombZone>();
            for (int i = 0; i < bombZoneResponseDTO.getBombZones().size(); i++)
            {
                short xBombBoard = bombZoneResponseDTO.getBombZones().get(i).getX();
                short yBombBoard = bombZoneResponseDTO.getBombZones().get(i).getY();
                short wBombBoard = bombZoneResponseDTO.getBombZones().get(i).getWidth();
                short hBombBoard = bombZoneResponseDTO.getBombZones().get(i).getHeight();
                bombZones.add(new BombZone(xBombBoard,yBombBoard,wBombBoard,hBombBoard));
            }

            MiniGameManager.getInstance().BombZones = bombZones;
        }
        catch (Exception e)
        {
            Gdx.app.error(MiniGameBombZoneController.class.getName(), "Failed to get mini game bomb zone", e);
        }
    }
}