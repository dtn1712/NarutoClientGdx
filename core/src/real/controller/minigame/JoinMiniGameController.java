package real.controller.minigame;

import android.util.Log;

import Objectgame.minigame.MiniGameInfo;
import Objectgame.minigame.MiniGameManager;
import network.Message;
import real.controller.IController;
import real.dto.response.JoinMiniGameResponseDTO;
import utils.IOUtils;

public class JoinMiniGameController implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            JoinMiniGameResponseDTO joinMiniGameResponseDTO = (JoinMiniGameResponseDTO) IOUtils.readMessage(message, JoinMiniGameResponseDTO.class);

            MiniGameManager.getInstance().isInMiniGame = joinMiniGameResponseDTO.isInMiniGame();
            if (joinMiniGameResponseDTO.isInMiniGame())
            {
                MiniGameInfo miniGameInfo = new MiniGameInfo();
                miniGameInfo.id = joinMiniGameResponseDTO.getMiniGameTemplate().getId();
                miniGameInfo.name = joinMiniGameResponseDTO.getMiniGameTemplate().getName();
                miniGameInfo.description = joinMiniGameResponseDTO.getMiniGameTemplate().getDescription();
                miniGameInfo.gameType = joinMiniGameResponseDTO.getMiniGameTemplate().getGameType();
                miniGameInfo.iconImage = joinMiniGameResponseDTO.getMiniGameTemplate().getIconImage();
                miniGameInfo.totalPlayers = joinMiniGameResponseDTO.getMiniGameTemplate().getTotalPlayers();
                miniGameInfo.groupPlayers = joinMiniGameResponseDTO.getMiniGameTemplate().getGroupPlayers();
                miniGameInfo.feeAmount = joinMiniGameResponseDTO.getMiniGameTemplate().getFeeAmount();
                miniGameInfo.feeType = joinMiniGameResponseDTO.getMiniGameTemplate().getFeeType();

                MiniGameManager.getInstance().MiniGameInfo = miniGameInfo;
            }
            else
            {
                MiniGameManager.getInstance().ClearGameData();
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(JoinMiniGameController.class.getName(), "Failed to join mini game", e);
        }
    }
}