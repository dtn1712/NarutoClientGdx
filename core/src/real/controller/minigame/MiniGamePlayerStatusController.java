package real.controller.minigame;

import android.util.Log;

import Objectgame.minigame.MiniGameManager;
import network.Message;
import real.controller.IController;
import real.dto.response.MiniGamePlayerStatusResponseDTO;
import utils.IOUtils;

public class MiniGamePlayerStatusController implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            MiniGamePlayerStatusResponseDTO miniGamePlayerStatusResponseDTO = (MiniGamePlayerStatusResponseDTO) IOUtils.readMessage(message, MiniGamePlayerStatusResponseDTO.class);
            MiniGameManager.getInstance().isGetBombed = miniGamePlayerStatusResponseDTO.isInBombZone();
        }
        catch (Exception e)
        {
            Gdx.app.error(MiniGamePlayerStatusController.class.getName(), "Failed to get mini game status", e);
        }
    }
}