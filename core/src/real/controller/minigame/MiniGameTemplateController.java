package real.controller.minigame;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Objectgame.minigame.MiniGameInfo;
import Objectgame.minigame.MiniGameManager;
import network.Message;
import real.controller.IController;
import real.dto.response.MiniGameTemplateResponseDTO;
import utils.IOUtils;

public class MiniGameTemplateController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            MiniGameTemplateResponseDTO miniGameTemplateResponseDTO = (MiniGameTemplateResponseDTO) IOUtils.readMessage(message, MiniGameTemplateResponseDTO.class);

            List<MiniGameInfo> miniGameInfoList = new ArrayList<MiniGameInfo>();
            for (int i = 0; i < miniGameTemplateResponseDTO.getMiniGameTemplates().size(); i++) {
                MiniGameInfo miniGameInfo = new MiniGameInfo();
                miniGameInfo.id = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getId();
                miniGameInfo.name = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getName();
                miniGameInfo.description = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getDescription();
                miniGameInfo.gameType = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getGameType();
                miniGameInfo.iconImage = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getIconImage();
                miniGameInfo.totalPlayers = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getTotalPlayers();
                miniGameInfo.groupPlayers = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getGroupPlayers();
                miniGameInfo.feeAmount = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getFeeAmount();
                miniGameInfo.feeType = miniGameTemplateResponseDTO.getMiniGameTemplates().get(i).getFeeType();

                miniGameInfoList.add(miniGameInfo);
            }

            MiniGameManager.getInstance().MiniGameList = miniGameInfoList;
        } catch (Exception e) {
            Gdx.app.error(MiniGameTemplateController.class.getName(), "Failed to get mini game template", e);
        }
    }
}