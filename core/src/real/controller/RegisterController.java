package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import network.Message;
import real.dto.response.RegisterResponseDTO;
import utils.IOUtils;

public class RegisterController implements IController {

    public void processMessage(Message msg) {
        try {
            RegisterResponseDTO registerResponseDTO = (RegisterResponseDTO) IOUtils.readMessage(msg, RegisterResponseDTO.class);
            GameCanvas.startOK(registerResponseDTO.getInfo(), GameCanvas.DISCONNECT, null);
        } catch (Exception e) {
            Gdx.app.error(RegisterController.class.getName(), "[processMessage.Register] Exception", e);
        }
    }

}