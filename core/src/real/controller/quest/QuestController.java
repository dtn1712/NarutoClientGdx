package real.controller.quest;

import android.util.Log;

import Gui.GuiQuest;
import Objectgame.InfoItem;
import Objectgame.Npc;
import Objectgame.quest.MainQuestManager;
import Objectgame.quest.QuestStatus;
import Objectgame.quest.QuestTemplate;
import network.Message;
import real.controller.IController;
import real.dto.mapper.DTOMapper;
import real.dto.quest.QuestDTO;
import screen.GameScreen;
import utils.IOUtils;

public class QuestController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            GuiQuest.getInstance().mainQuestShortReminderList.removeAllElements();
            QuestDTO questDTO = (QuestDTO) IOUtils.readMessage(message, QuestDTO.class);

            QuestTemplate quest = DTOMapper.mapQuestTemplateDtoToModel(questDTO.getQuestTemplate());
            if (questDTO.getStatus().equals(QuestStatus.NEW.name())) {
                ResetNpcQuest();
                SetNpcQuest(quest.IdNpcReceive);
                MainQuestManager.getInstance().FinishQuest = null;
                MainQuestManager.getInstance().NewQuest = quest;
                MainQuestManager.getInstance().NewQuest.Paint();

            } else if (questDTO.getStatus().equals(QuestStatus.WORKING.name())) {
                MainQuestManager.getInstance().NewQuest = null;
                MainQuestManager.getInstance().WorkingQuest = quest;
                MainQuestManager.getInstance().WorkingQuest.Paint();
            } else if (questDTO.getStatus().equals(QuestStatus.COMPLETED.name())) {
                MainQuestManager.getInstance().WorkingQuest = null;
                MainQuestManager.getInstance().FinishQuest = quest;
                MainQuestManager.getInstance().FinishQuest.Paint();
            }

            for (String t : quest.RemindContentPaint)
            {
                GuiQuest.getInstance().mainQuestShortReminderList.add(new InfoItem(t));
            }
        } catch (Exception e) {
            Gdx.app.error(QuestController.class.getName(), "Failed to get quest", e);
        }
    }

    private static void ResetNpcQuest()
    {
        for (int j = 0; j < GameScreen.getInstance().vNpc.size(); j++)
        {
            Npc npc = GameScreen.getInstance().vNpc.elementAt(j);
            if (npc != null && npc.typeNV == 0)
            {
                npc.typeNV = -1;
            }
        }
    }

    private static void SetNpcQuest(int npcId)
    {
        for (int j = 0; j < GameScreen.getInstance().vNpc.size(); j++)
        {
            Npc npc = GameScreen.getInstance().vNpc.elementAt(j);
            if (npc != null && npc.npcId == npcId)
            {
                npc.typeNV = 0;
            }
        }
    }
}