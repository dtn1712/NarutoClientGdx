package real.controller.quest;

import android.util.Log;

import Gui.GuiQuest;
import Objectgame.InfoItem;
import Objectgame.quest.DailyQuestManager;
import Objectgame.quest.QuestStatus;
import Objectgame.quest.QuestTemplate;
import network.Message;
import real.controller.IController;
import real.dto.mapper.DTOMapper;
import real.dto.quest.QuestDTO;
import utils.IOUtils;

public class DailyQuestController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            GuiQuest.getInstance().dailyQuestShortReminderList.removeAllElements();
            QuestDTO questDTO = (QuestDTO) IOUtils.readMessage(message, QuestDTO.class);

            QuestTemplate quest = DTOMapper.mapQuestTemplateDtoToModel(questDTO.getQuestTemplate());
            if (questDTO.getStatus().equals(QuestStatus.NEW.name())) {
                DailyQuestManager.getInstance().WorkingQuest = null;
                DailyQuestManager.getInstance().NewQuest = quest;
                DailyQuestManager.getInstance().NewQuest.Paint();

            } else if (questDTO.getStatus().equals(QuestStatus.WORKING.name())) {
                DailyQuestManager.getInstance().NewQuest = null;
                DailyQuestManager.getInstance().WorkingQuest = quest;
                DailyQuestManager.getInstance().WorkingQuest.Paint();
            }

            for (String t : quest.RemindContentPaint)
            {
                GuiQuest.getInstance().dailyQuestShortReminderList.add(new InfoItem(t));
            }
        } catch (Exception e) {
            Gdx.app.error(DailyQuestController.class.getName(), "Failed to get daily quest", e);
        }
    }

}