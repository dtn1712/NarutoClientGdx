package real.controller;

import android.util.Log;

import Objectgame.CharConfig;
import Objectgame.ItemType;
import network.Message;
import real.dto.character.CharConfigDTO;
import real.dto.game.ItemTypeDTO;
import utils.IOUtils;

public class CharConfigController implements IController
{
    public void processMessage(Message message)
    {
        try
        {
            CharConfigDTO charConfigDTO = (CharConfigDTO) IOUtils.readMessage(message, CharConfigDTO.class);

            ItemType[] equipmentPositions = new ItemType[charConfigDTO.getEquipmentPositions().length];
            for (int i = 0; i < charConfigDTO.getEquipmentPositions().length; i++)
            {
                ItemTypeDTO itemTypeDto = charConfigDTO.getEquipmentPositions()[i];
                ItemType itemType = new ItemType();
                itemType.setId(itemTypeDto.getId());
                itemType.setName(itemTypeDto.getName());
                itemType.setTypeKey(itemTypeDto.getTypeKey());
                equipmentPositions[i] = itemType;
            }
            CharConfig.getInstance().setEquipmentPositions(equipmentPositions);
        }
        catch (Exception e)
        {
            Gdx.app.error(CharConfigController.class.getName(), "Failed to get char config", e);
        }
    }
}