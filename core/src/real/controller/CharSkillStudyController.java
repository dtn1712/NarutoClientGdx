package real.controller;

import android.util.Log;

import Gui.TabSkill;
import Objectgame.Char;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.minigame.MiniGameManager;
import model.QuickSlot;
import network.Message;
import real.dto.response.CharSkillStudyResponseDTO;
import utils.IOUtils;

public class CharSkillStudyController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            CharSkillStudyResponseDTO charSkillStudiedResponseData = (CharSkillStudyResponseDTO) IOUtils.readMessage(msg, CharSkillStudyResponseDTO.class);

            for (int i = 0; i < charSkillStudiedResponseData.getListSkill().size(); i++)
            {
                byte skillId = charSkillStudiedResponseData.getListSkill().get(i).getIdSkill(); // idSkill dahoc
                SkillTemplate skillTemplate = SkillTemplates.hSkilltemplate.get(skillId);
                short levelSkill = charSkillStudiedResponseData.getListSkill().get(i).getLevel(); // level skill hien tai

                skillTemplate.level = levelSkill;
                if (skillTemplate.level >= skillTemplate.nLevelSkill)
                {
                    skillTemplate.level = (short) (skillTemplate.nLevelSkill - 1);
                }

                if (TabSkill.skillIndex != null && skillId == TabSkill.skillIndex.id)
                {
                    TabSkill.skillIndex.level = levelSkill;
                    TabSkill.isDaHoc = true;
                }
                if (!MiniGameManager.getInstance().isInMiniGame)
                {
                    if (i == 0)
                    {
                        QuickSlot.idSkillCoBan = skillId;
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        QuickSlot.idSkillCoBan = skillId;
                    }
                    else
                    {
                        Char.myChar().mQuickslot[i].setIdSkill(skillId, false);
                    }
                }

                if (!MiniGameManager.getInstance().isInMiniGame)
                {
                    QuickSlot.loadRmsQuickSlot();
                }

            }

        }
        catch (Exception e)
        {
            Gdx.app.error(CharSkillStudyController.class.getName(), "Failed to study char skill", e);
        }

    }
}