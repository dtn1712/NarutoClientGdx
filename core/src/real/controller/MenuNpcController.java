package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import Objectgame.Char;
import Objectgame.MenuNpcManager;
import Objectgame.MenuTemplate;
import Objectgame.Npc;
import lib.Command;
import network.Message;
import real.dto.mapper.DTOMapper;
import real.dto.response.MenuNpcResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class MenuNpcController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            MenuNpcResponseDTO menuNpcResponseDto = (MenuNpcResponseDTO) IOUtils.readMessage(msg, MenuNpcResponseDTO.class);

            int idNpc = menuNpcResponseDto.getIdNpc();
            int size = menuNpcResponseDto.getListMenu().size();

            List<MenuTemplate> menuTemplates = new ArrayList<MenuTemplate>();
            Vector<Command> vmenu = new Vector<Command>();
            for (int i = 0; i < size; i++) {
                menuTemplates.add(DTOMapper.mapMenuTemplateDtoToModel(menuNpcResponseDto.getListMenu().get(i)));
                vmenu.addElement(new Command(menuNpcResponseDto.getListMenu().get(i).getName(), GameCanvas.instance, GameCanvas.cMenuNpc, i + "")); //i index_menu
            }

            Npc npcDem = null;
            for (int i = 0; i < GameScreen.getInstance().vNpc.size(); i++) {
                Npc npc = GameScreen.getInstance().vNpc.elementAt(i);
                if (npc != null && npc.template != null && npc.template.npcTemplateId == idNpc && npc.equals(Char.myChar().npcFocus))
                {
                    npcDem = npc;
                }
            }
            if (npcDem == null || vmenu.size() <= 0) return;

            MenuNpcManager.getInstance().setMenuTemplates(menuTemplates);
            GameCanvas.menu.doCloseMenu();
            GameCanvas.menu.startAtNPC(vmenu, npcDem, "");
        } catch (Exception e) {
            Gdx.app.error(MenuNpcController.class.getName(), "Failed to get menu Npc", e);
        }
    }

}
