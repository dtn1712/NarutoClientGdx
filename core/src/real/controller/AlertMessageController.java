package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import network.Message;
import real.dto.response.AlertMessageResponseDTO;
import utils.IOUtils;

public class AlertMessageController implements IController
{
    public void processMessage(Message msg)
    {
        try {
            AlertMessageResponseDTO alertMessageDTO = (AlertMessageResponseDTO) IOUtils.readMessage(msg, AlertMessageResponseDTO.class);
            GameCanvas.getInstance().startOKDlg(alertMessageDTO.getInfo());
        } catch (Exception e) {
            Gdx.app.error(AlertMessageController.class.getName(), "Failed to handle alert message", e);
        }
    }
}