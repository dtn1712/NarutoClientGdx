package real.controller;

import android.util.Log;

import Objectgame.Char;
import model.ServerEffect;
import network.Message;
import real.dto.response.CharReviveResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class CharReviveController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            CharReviveResponseDTO charReviveResponseDTO = (CharReviveResponseDTO) IOUtils.readMessage(msg, CharReviveResponseDTO.class);

            long charId = charReviveResponseDTO.getId();
            Char chs = GameScreen.getInstance().findCharInMap(charId);

            if (chs == null) return;

            ServerEffect.addServerEffect(34, chs.cx, chs.cy, 3);
            chs.statusMe = Char.A_FALL;
            chs.cHP = charReviveResponseDTO.getHp();
            chs.cMP = charReviveResponseDTO.getMp();
        } catch (Exception e) {
            Gdx.app.error(CharReviveController.class.getName(), "Failed to handle char revive", e);
        }
    }
}