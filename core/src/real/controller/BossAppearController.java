package real.controller;

import android.util.Log;

import Objectgame.Mob;
import model.ServerEffect;
import network.Message;
import real.dto.response.BossResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class BossAppearController implements IController
{
    public void processMessage(Message message)
    {
        try {
            BossResponseDTO bossResponseDTO = (BossResponseDTO) IOUtils.readMessage(message, BossResponseDTO.class);

            for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                Mob demMob = GameScreen.getInstance().vMob.elementAt(i);
                if (demMob != null && demMob.mobId == bossResponseDTO.getMonsterId() && demMob.isBoss) {
                    demMob.isBossAppear = true;
                    demMob.injureThenDie = false;
                    demMob.status = Mob.MA_WALK;
                    demMob.x = demMob.xFirst;
                    demMob.y = demMob.yFirst;
                    ServerEffect.addServerEffect(37, demMob.x, demMob.y - demMob.getH() / 4 - 10, 1);
                    demMob.hp = demMob.maxHp;
                    break;
                }
            }
        } catch (Exception e) {
            Gdx.app.error(BossAppearController.class.getName(), "Failed to handle boss appear", e);
        }
    }
}