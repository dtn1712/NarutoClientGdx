package real.controller;

import android.util.Log;

import java.util.List;

import Gui.GuiMain;
import Objectgame.Char;
import Objectgame.ItemThucAn;
import lib.MFont;
import network.Message;
import real.dto.response.InfoFoodBuff;
import real.dto.response.UpdateCharDTO;
import screen.GameScreen;
import utils.IOUtils;

public class UpdateCharController implements IController
{
    
    public static final byte UPDATE_BLOOD_LOW = 0;//su dung cho bom mau
    public static final byte UPDATE_EXP = 1;//su dung tang exp
    public static final byte UPDATE_HP = 2;
    public static final byte UPDATE_MP = 3;
    
    public void processMessage(Message msg)
    {
        try
        {
            //UPDATE_BLOOD = 0;//UPDATE_EXP = 1;//su dung tang exp/// UPDATE_HP = 2; //UPDATE_MP = 3;
            UpdateCharDTO updateCharDTO = (UpdateCharDTO) IOUtils.readMessage(msg, UpdateCharDTO.class);
            
            long idchar = updateCharDTO.getIdChar();
            byte type = updateCharDTO.getType();
            
            Char ccharupdate = GameScreen.getInstance().findCharInMap(idchar);
            if (ccharupdate == null) return;
            switch (type)
            {
                case UPDATE_BLOOD_LOW:
                    
                    GuiMain.vecItemOther.removeAllElements();
                    
                    int sizeThucAn = updateCharDTO.infoFoodBuff.size();
                    List<InfoFoodBuff> infoFoodBuff = updateCharDTO.infoFoodBuff;

                    short[] idtemplate = new short[sizeThucAn];
                    short[] time = new short[sizeThucAn];
                    for (int i = 0; i < sizeThucAn; i++)
                    {
                        idtemplate[i] = infoFoodBuff.get(i).getIdTemplateItem();
                        time[i] = infoFoodBuff.get(i).getTimeFood();
                        GuiMain.vecItemOther.add(new ItemThucAn((short) i, (byte) 0, idtemplate[i], time[i], 100));
                    }

                    break;
                case UPDATE_EXP:
                    if (ccharupdate == Char.myChar())
                    {
                        Char.myChar().cEXP = updateCharDTO.getExp();
                        Char.myChar().cExpDisplay = Char.myChar().cEXP + "%";
                        Char.myChar().totalPoints = updateCharDTO.getTotalPoint();
                    }
                    break;
                case UPDATE_HP: //hp
                    ccharupdate.cMaxHP = updateCharDTO.getCharHpMax(); //maxhp
                    ccharupdate.cHP = updateCharDTO.getCharHp(); //hp
                    int hpcong = updateCharDTO.getHpPlus(); //hp cong them
                    GameScreen.startFlyText("+" + hpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, MFont.RED);
                    break;
                case UPDATE_MP: //hp
                    ccharupdate.cMaxMP = updateCharDTO.getCharMpMax(); //maxhp
                    ccharupdate.cMP = updateCharDTO.getCharMp(); //hp
                    int mpcong = updateCharDTO.getMpPlus(); //hp cong them
                    GameScreen.startFlyText("+" + mpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, MFont.GREEN);
                    break;
            }
    
        }
        catch (Exception e)
        {
            Gdx.app.error(UpdateCharController.class.getName(), "Failed to update char", e);
        }
    }
}