package real.controller.clan;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.Command;
import network.Message;
import real.controller.IController;
import real.dto.response.ShowCreateClanResponseDTO;
import utils.IOUtils;

public class CreateClanController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            ShowCreateClanResponseDTO showCreateClanResponseDTO = (ShowCreateClanResponseDTO) IOUtils.readMessage(message, ShowCreateClanResponseDTO.class);

            GameCanvas.requestCreateClanType = showCreateClanResponseDTO.getClanType();

            String title = "Tạo Bang (Tốn " + showCreateClanResponseDTO.getFeeAmount() + " " + showCreateClanResponseDTO.getFeeType() + ")";
            GameCanvas.startDlgTField(title, new Command("Tạo", GameCanvas.getInstance(), GameCanvas.cTaoClan, null), null,
                    new Command("Cancel", GameCanvas.getInstance(), GameCanvas.END_DIALOG_FIELD, null));
        } catch (Exception e) {
            Gdx.app.error(CreateClanController.class.getName(), "Failed to create clan", e);
        }
    }
}