package real.controller.clan;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Objectgame.clan.Clan;
import Objectgame.clan.ClanType;
import network.Message;
import real.controller.IController;
import real.dto.mapper.DTOMapper;
import real.dto.response.ListClanResponseDTO;
import screen.ClanScreen;
import utils.IOUtils;

public class GetListClanController implements IController
{
    public void processMessage(Message message)
    {
        try {
            ListClanResponseDTO listClanResponseDTO = (ListClanResponseDTO) IOUtils.readMessage(message, ListClanResponseDTO.class);

            List<Clan> clans = new ArrayList<Clan>();
            for (int i = 0; i < listClanResponseDTO.getClans().size(); i++) {
                clans.add(DTOMapper.mapClanBasicDtoToModel(listClanResponseDTO.getClans().get(i)));
            }

            ClanScreen.getInstance().Clear();
            if (listClanResponseDTO.getClanType().equals(ClanType.GLOBAL.name())) {
                ClanScreen.getInstance().ListGlobalClan = clans;
            } else {
                ClanScreen.getInstance().ListLocalClan = clans;
            }
        } catch (Exception e) {
            Gdx.app.error(GetListClanController.class.getName(), "Failed to get list clan", e);
        }

    }
}