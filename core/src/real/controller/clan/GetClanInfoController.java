package real.controller.clan;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Objectgame.clan.ClanMember;
import Objectgame.clan.ClanType;
import network.Message;
import real.controller.IController;
import real.dto.clan.ClanInfoDTO;
import real.dto.mapper.DTOMapper;
import screen.ClanScreen;
import utils.IOUtils;

public class GetClanInfoController  implements IController
{
    public void processMessage(Message message)
    {
        try {
            ClanInfoDTO clanInfoDTO = (ClanInfoDTO) IOUtils.readMessage(message, ClanInfoDTO.class);

            List<ClanMember> clanMembers = new ArrayList<ClanMember>();
            for (int i = 0; i < clanInfoDTO.getMembers().size(); i++) {
                clanMembers.add(DTOMapper.mapClanMemberDtoToModel(clanInfoDTO.getMembers().get(i)));
            }

            ClanScreen.getInstance().Clear();
            if (clanInfoDTO.getClanType().equals(ClanType.GLOBAL.name())) {
                ClanScreen.getInstance().clanGlobalName = clanInfoDTO.getName();
                ClanScreen.getInstance().ListMemberGlobalClan = clanMembers;
            } else {
                ClanScreen.getInstance().clanLocalName = clanInfoDTO.getName();
                ClanScreen.getInstance().ListMemberLocalClan = clanMembers;
            }
        } catch (Exception e) {
            Gdx.app.error(GetClanInfoController.class.getName(), "Failed to get clan info", e);
        }
    }
}