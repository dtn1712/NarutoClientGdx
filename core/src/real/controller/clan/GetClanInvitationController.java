package real.controller.clan;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Objectgame.clan.Clan;
import network.Message;
import real.controller.IController;
import real.dto.mapper.DTOMapper;
import real.dto.response.ListClanInvitationResponseDTO;
import screen.ClanScreen;
import utils.IOUtils;

public class GetClanInvitationController implements IController
{
    public void processMessage(Message message)
    {
        try {
            ListClanInvitationResponseDTO listClanInvitationResponseDTO = (ListClanInvitationResponseDTO) IOUtils.readMessage(message, ListClanInvitationResponseDTO.class);

            List<Clan> clans = new ArrayList<Clan>();
            for (int i = 0; i < listClanInvitationResponseDTO.getClans().size(); i++) {
                clans.add(DTOMapper.mapClanBasicDtoToModel(listClanInvitationResponseDTO.getClans().get(i)));
            }

            ClanScreen.getInstance().Clear();
            ClanScreen.getInstance().ListClanInvitations = clans;
        } catch (Exception e) {
            Gdx.app.error(GetClanInvitationController.class.getName(), "Failed to get clan invitation", e);
        }
    }
}