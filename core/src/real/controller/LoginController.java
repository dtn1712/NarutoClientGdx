package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import lib.Rms;
import model.SmallImage;
import network.Message;
import real.dto.response.LoginResponseDTO;
import screen.CreateCharScreen;
import screen.GameScreen;
import utils.IOUtils;

public class LoginController implements IController
{
    public void processMessage(Message msg)
    {
        try {
            LoginResponseDTO loginResponseDto = (LoginResponseDTO) IOUtils.readMessage(msg, LoginResponseDTO.class);

            if (loginResponseDto.isSuccess()) {

                saveData(loginResponseDto);

                GameScreen.readEffect();
                SmallImage.readImage();
                GameScreen.readPart();
                GameScreen.readSkill();

                CreateCharScreen.getInstance().idPartHoa = loginResponseDto.getHoaQuocSkin();
                CreateCharScreen.getInstance().idPartLoi = loginResponseDto.getLoiQuocSkin();
                CreateCharScreen.getInstance().idPartPhong = loginResponseDto.getPhongQuocSkin();
                CreateCharScreen.getInstance().idPartTho = loginResponseDto.getThoQuocSkin();
                CreateCharScreen.getInstance().idPartThuy = loginResponseDto.getThuyQuocSkin();
            } else {
                GameCanvas.startOK(loginResponseDto.getErrorMessage(), GameCanvas.DISCONNECT, null);
            }
        } catch (Exception e) {
            Gdx.app.error(LoginController.class.getName(), "Failed to login", e);
        }

    }


    private void saveData(LoginResponseDTO loginResponseDTO)
    {
        try
        {
            Rms.saveRMS("nj_arrow", loginResponseDTO.getNjArrow());
            Rms.saveRMS("nj_effect", loginResponseDTO.getNjEffect());
            Rms.saveRMS("nj_image", loginResponseDTO.getNjImages());
            Rms.saveRMS("nj_part", loginResponseDTO.getNjPart());
            Rms.saveRMS("nj_skill", loginResponseDTO.getNjSkill());
        }
        catch (Exception e)
        {
            Gdx.app.error(LoginController.class.getName(), "Failed to save data when login", e);
        }
    }
}