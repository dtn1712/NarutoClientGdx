package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import network.Message;
import real.Service;
import real.dto.character.CharBasicInfoDTO;
import real.dto.response.CharListResponseDTO;
import screen.CreateCharScreen;
import screen.LoginScreen;
import screen.SelectCharScreen;
import utils.IOUtils;

public class CharListController  implements IController
{

    private String[] imageTypes = new String[] {"img", "img/bg", "img/bg/low"};

    public void processMessage(Message msg)
    {
        try {
            CharListResponseDTO charListData = (CharListResponseDTO) IOUtils.readMessage(msg, CharListResponseDTO.class);

            LoginScreen.isLoggingIn = false;
            SelectCharScreen.getInstance().initSelectChar();

            if (charListData.getChars().size() > 0) {
                for (int i = 0; i < charListData.getChars().size(); i++) {
                    CharBasicInfoDTO charBasicInfo = charListData.getChars().get(i);

                    SelectCharScreen.getInstance().charIDDB[i] = charBasicInfo.getId();
                    SelectCharScreen.getInstance().name[i] = charBasicInfo.getCharName();
                    SelectCharScreen.getInstance().gender[i] = charBasicInfo.getGender();
                    SelectCharScreen.getInstance().type[i] = charBasicInfo.getCharClass();
                    SelectCharScreen.getInstance().lv[i] = charBasicInfo.getLevel();
                    SelectCharScreen.getInstance().parthead[i] = charBasicInfo.getHeadId();
                    SelectCharScreen.getInstance().partbody[i] = charBasicInfo.getBodyId();
                    SelectCharScreen.getInstance().partleg[i] = charBasicInfo.getLegId();
                }

                GameCanvas.getInstance().nextScreen = SelectCharScreen.getInstance();
            } else {
                GameCanvas.getInstance().nextScreen = CreateCharScreen.getInstance();
            }

            Service.getInstance().RequestListImage(imageTypes);
            GameCanvas.getInstance().endDlg();
        } catch (Exception e) {
            Gdx.app.error(CharListController.class.getName(), "Failed to get char list", e);
        }
    }
}