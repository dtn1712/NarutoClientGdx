package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Gui.TabNangCap;
import network.Message;
import real.dto.response.ItemUpgradeResponseDTO;
import utils.IOUtils;

public class UpgradeItemController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ItemUpgradeResponseDTO itemUpgradeResponseDTO = (ItemUpgradeResponseDTO) IOUtils.readMessage(msg, ItemUpgradeResponseDTO.class);

            byte typeUpgrade = itemUpgradeResponseDTO.getType();
            switch (typeUpgrade) {
                case TabNangCap.UPDATE_INFO_UPGRADE:
                    int itemLength = itemUpgradeResponseDTO.getListItemTemplateId().length;
                    int feeAmount = itemUpgradeResponseDTO.getFeeAmount();
                    int successRate = itemUpgradeResponseDTO.getSuccessRate();
                    short[] listItemId = new short[itemLength];
                    for (int i = 0; i < itemLength; i++) {
                        listItemId[i] = itemUpgradeResponseDTO.getListItemTemplateId()[i];
                    }

                    TabNangCap tabNangCap = (TabNangCap) GameCanvas.allInfo.VecTabScreen.elementAt(4);
                    tabNangCap.feeAmount = feeAmount;
                    tabNangCap.sucessRate = successRate;

                    if (tabNangCap != null) {
                        tabNangCap.updateListItemNangCap(listItemId);
                    }
                    break;
                case TabNangCap.UPGRADE_FAIL:
                    TabNangCap.isFail = true;
                    break;
                case TabNangCap.UPGRADE_SUCCESS:
                    TabNangCap.isSuccess = true;
                    break;
            }
        } catch (Exception e) {
            Gdx.app.error(UpgradeItemController.class.getName(), "Failed to upgrade item", e);
        }
    }
}