package real.controller.friend;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.controller.IController;
import real.dto.response.FriendListResponseDTO;
import utils.IOUtils;

public class RequestFriendListController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            FriendListResponseDTO friendListResponseDTO = (FriendListResponseDTO) IOUtils.readMessage(msg, FriendListResponseDTO.class);

            Char.myChar().vFriend.removeAllElements();
            int sizeFriendList = friendListResponseDTO.getListFriend().size();
            for (int i = 0; i < sizeFriendList; i++) {
                Char ch = new Char();
                ch.isOnline = friendListResponseDTO.getListFriend().get(i).isOnline();
                ch.charID = friendListResponseDTO.getListFriend().get(i).getId();
                ch.cName = friendListResponseDTO.getListFriend().get(i).getCharName();
                ch.clevel = friendListResponseDTO.getListFriend().get(i).getCharLevel();
                ch.head = friendListResponseDTO.getListFriend().get(i).getIdHead();
                Char.myChar().vFriend.addElement(ch);
            }
        } catch (Exception e) {
            Gdx.app.error(RequestFriendListController.class.getName(), "Failed to request friend list", e);
        }
    }
}