package real.controller.friend;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.controller.IController;
import real.dto.response.FriendRemoveResponseDTO;
import utils.IOUtils;

public class UnfriendController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            FriendRemoveResponseDTO friendRemoveResponseDTO = (FriendRemoveResponseDTO) IOUtils.readMessage(msg, FriendRemoveResponseDTO.class);

            long charId = friendRemoveResponseDTO.getId();
            for (int i = 0; i < Char.myChar().vFriend.size(); i++) {
                Char chardel = (Char) Char.myChar().vFriend.elementAt(i);
                if (chardel.charID != 0) {
                    // khi online
                    if (chardel.charID == charId) {
                        Char.myChar().vFriend.removeElementAt(i);
                    }
                } else {
                    if (chardel.charID == charId) {
                        Char.myChar().vFriend.removeElementAt(i);
                    }
                }
            }
        } catch (Exception e) {
            Gdx.app.error(UnfriendController.class.getName(), "Failed to unfriend", e);
        }
    }
}