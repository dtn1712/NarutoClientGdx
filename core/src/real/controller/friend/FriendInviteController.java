package real.controller.friend;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Objectgame.Char;
import lib.Command;
import model.MResources;
import network.Message;
import real.controller.IController;
import real.dto.response.InviteFriendResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class FriendInviteController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            InviteFriendResponseDTO inviteFriendResponseDTO = (InviteFriendResponseDTO) IOUtils.readMessage(msg, InviteFriendResponseDTO.class);

            Char.myChar().idFriend = inviteFriendResponseDTO.getPlayerId();
            GameCanvas.startYesNoDlg(inviteFriendResponseDTO.getContent(), GameScreen.getInstance().cmdComfirmFriend,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
        } catch (Exception e) {
            Gdx.app.error(FriendInviteController.class.getName(), "Failed to invite friend", e);
        }
    }
}