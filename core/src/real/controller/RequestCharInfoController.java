package real.controller;

import android.util.Log;

import Objectgame.Char;
import lib.Music;
import model.ServerEffect;
import network.Message;
import real.dto.mapper.DTOMapper;
import real.dto.response.CharInfoResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class RequestCharInfoController  implements IController
{
    public void processMessage(Message msg)
    {
        try
        {
            CharInfoResponseDTO charInfoResponseDTO = (CharInfoResponseDTO) IOUtils.readMessage(msg, CharInfoResponseDTO.class);

            long charId = charInfoResponseDTO.getId();
            int charMaxHp = charInfoResponseDTO.getMaxHp();
            int charHp = charInfoResponseDTO.getHp();
            short clevel = charInfoResponseDTO.getLevel();
            boolean isJinchuuriki = charInfoResponseDTO.isJinchuuriki();
            boolean cNinjaExile = charInfoResponseDTO.isNinjaExile();

            for (int i = 0; i < GameScreen.getInstance().vCharInMap.size(); i++)
            {
                Char ch = GameScreen.getInstance().vCharInMap.elementAt(i);
                if (ch.charID != charId) continue;

                ch.cName = charInfoResponseDTO.getCharName();
                ch.cMaxHP = charMaxHp;
                ch.cHP = charHp;
                ch.cHPDisplay = ch.cHP + "/" + ch.cMaxHP;
                ch.clevel = clevel;
                ch.cLevelDisplay = ch.clevel + "";
                ch.globalClan = (charInfoResponseDTO.getGlobalClan() != null) ? DTOMapper.mapClanBasicDtoToModel(charInfoResponseDTO.getGlobalClan()) : null;
                ch.localClan = (charInfoResponseDTO.getLocalClan() != null) ? DTOMapper.mapClanBasicDtoToModel(charInfoResponseDTO.getLocalClan()) : null;
                ch.isNinjaExile = cNinjaExile;
                ch.head = charInfoResponseDTO.getHeadId();
                ch.body = charInfoResponseDTO.getBodyId();
                ch.leg = charInfoResponseDTO.getLegId();
                if (ch.clevel > 0 && ch.clevel < clevel)
                {
                    Music.play(Music.LENLV, 10);
                    ServerEffect.addServerEffect(22, ch.cx, ch.cy, 1);
                }

                if (isJinchuuriki)
                {
                    ServerEffect.addServerEffect(40, ch, true);
                }
                else
                {
                    ServerEffect.removeEffect(40);
                }
            }
        }
        catch (Exception e)
        {
            Gdx.app.error(RequestCharInfoController.class.getName(), "Failed to request char info", e);
        }
    }
}