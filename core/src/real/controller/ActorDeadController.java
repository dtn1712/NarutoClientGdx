package real.controller;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;

import Gui.Constants;
import Gui.MenuIcon;
import Gui.TabScreenNew;
import Objectgame.Char;
import Objectgame.Mob;
import network.Message;
import real.dto.response.ActorDeadResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class ActorDeadController implements IController
{
    public void processMessage(Message msg)
    {
        try {
            ActorDeadResponseDTO actorDeadResponseDto = (ActorDeadResponseDTO) IOUtils.readMessage(msg, ActorDeadResponseDTO.class);
            if (actorDeadResponseDto.getActorType() == Constants.CAT_PLAYER) {
                long playerId = actorDeadResponseDto.getId();
                if (playerId == Char.myChar().charID) {
                    GameScreen.getInstance().guiMain.moveClose = true;
                    if (MenuIcon.isShowTab)
                        GameCanvas.getInstance().gameScreen.guiMain.menuIcon.cmdClose.performAction();
                    if (GameScreen.isBag)
                        TabScreenNew.cmdClose.performAction();
                }
                Char player = GameScreen.getInstance().findCharInMap(playerId);

                if (player == null) return;
                player.cHP = 0;
                player.statusMe = Char.A_DEAD;
            } else if (actorDeadResponseDto.getActorType() == Constants.CAT_MONSTER) {
                long mobId = actorDeadResponseDto.getId();
                for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                    Mob mob = GameScreen.getInstance().vMob.elementAt(i);
                    if (mob.mobId != mobId) continue;
                    mob.status = Mob.MA_DEADFLY;
                    mob.startDie();

                    if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobId == mobId) {
                        Char.myChar().mobFocus = null;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            Gdx.app.error(ActorDeadController.class.getName(), "Failed to handle actor dead", e);
        }
    }
}
