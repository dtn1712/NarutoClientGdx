package real.controller;

import android.util.Log;

import Objectgame.Char;
import network.Message;
import real.dto.response.MoveResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class MoveController implements IController {

    @Override
    public void processMessage(Message message) {
        try {
            MoveResponseDTO moveResponseDTO = (MoveResponseDTO) IOUtils.readMessage(message, MoveResponseDTO.class);
            for (int i = 0; i < GameScreen.getInstance().vCharInMap.size(); i++) {
                Char c = GameScreen.getInstance().vCharInMap.elementAt(i);
                if (c.charID != moveResponseDTO.getPlayerId()) continue;

                c.statusMe = moveResponseDTO.getStatus();
                c.cdir = moveResponseDTO.getDirection();
                c.cvx = moveResponseDTO.getVelocityX();
                c.cvy = moveResponseDTO.getVelocityY();

                c.cx = moveResponseDTO.getX();
                c.cy = moveResponseDTO.getY();

                c.lastUpdateTime = System.currentTimeMillis();

            }
        } catch (Exception e) {
            Gdx.app.error(CharPositionController.class.getName(), "Failed to handle char position", e);
        }
    }
}
