package real.controller;

import android.util.Log;

import Objectgame.Mob;
import network.Message;
import real.dto.response.MonsterInfoResponseDTO;
import screen.GameScreen;
import utils.IOUtils;

public class MonsterInfoController  implements IController
{
    public void processMessage(Message msg)
    {
        try {
            MonsterInfoResponseDTO monsterInfoResponseDTO = (MonsterInfoResponseDTO) IOUtils.readMessage(msg, MonsterInfoResponseDTO.class);

            long mobId = monsterInfoResponseDTO.getId();
            String mobName = monsterInfoResponseDTO.getName();
            int maxHp = monsterInfoResponseDTO.getMaxHp();
            int hp = monsterInfoResponseDTO.getHp();
            for (int i = 0; i < GameScreen.getInstance().vMob.size(); i++) {
                Mob modInMap = GameScreen.getInstance().vMob.elementAt(i);
                if (modInMap == null || modInMap.mobId != mobId) {
                    continue;
                }
                modInMap.mobName = mobName;
                modInMap.maxHp = maxHp;
                modInMap.hp = hp;
                modInMap.hpDisplay = hp + "/" + maxHp;
            }
        } catch (Exception e) {
            Gdx.app.error(MonsterInfoController.class.getName(), "Failed to get monster info", e);
        }
    }
}