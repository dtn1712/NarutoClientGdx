package real;

import java.io.IOException;
import java.util.Vector;

import lib.Cout;
import lib.Session_ME;
import model.Cmd;
import network.ISesion;
import network.Message;

import Objectgame.Char;
import Objectgame.Item;
import Objectgame.Mob;

public class Service {

	ISesion session = Session_ME.gI();
	protected static Service instance;

	public static Service gI() {
		if (instance == null)
			instance = new Service();
		return instance;
	}
	
	public void doLogin(String user, String pass,byte zoomlevel){ // login
		Message m;
		try {
			Cout.println(getClass(), user+" user "+pass);
			m = new Message(Cmd.LOGIN);
			m.writer().writeUTF(user);
			m.writer().writeUTF(pass);
			m.writer().writeByte(zoomlevel);
			session.sendMessage(m);
			m.cleanup();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void createChar(byte typeChar, byte country , byte gender, String charName){ // tạo char
		Message m;
		try{
			Cout.println(charName+" CreateChar "+country+"  typechar "+typeChar+" gen "+gender);
			m = new Message(Cmd.CREATE_CHAR);
			m.writer().writeByte(country);
			m.writer().writeByte(typeChar);
			m.writer().writeByte(gender);
			m.writer().writeUTF(charName);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void selectChar(int charIDDB){ // select char to play
		Message m;
		try{
			m = new Message(Cmd.SELECT_CHAR);
			m.writer().writeInt(charIDDB);
			session.sendMessage(m);
			m.cleanup();
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void charMove() { // gui char di chuyen
		int dx = Char.myChar().cx - Char.myChar().cxSend;
		int dy = Char.myChar().cy - Char.myChar().cySend;

		if (Char.ischangingMap || (dx == 0 && dy == 0)){
			return;
		}
		try {
			Message m = new Message(Cmd.PLAYER_MOVE);
			Char.myChar().cxSend = Char.myChar().cx;
			Char.myChar().cySend = Char.myChar().cy;
			Char.myChar().cdirSend = Char.myChar().cdir;
			Char.myChar().cactFirst = Char.myChar().statusMe;
			m.writer().writeShort(Char.myChar().cx);
//			if (dy != 0)
			m.writer().writeShort(Char.myChar().cy);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void requestImage(int id){ // yeu cau hinh tu server
		Message m = null;
		try {
			//System.out.println("idicon --> "+id);
			m = new Message(Cmd.REQUEST_IMAGE);
			m.writer().writeInt(id);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestChangeMap() { // server tự biết nó đứng đâu mà chuyển đến
		// map nào
		try{
			Message m = new Message(Cmd.CHANGE_MAP);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	public void requestinventory(){ // yeu cau request Inventory
		try{
			System.out.println("REQUEST ITEM INVENTORY");
			Message m = new Message(Cmd.GET_ITEM_INVENTORY);
			session.sendMessage(m);
			m.cleanup();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void requestIcon(int id){ // request hinh nho
		Message m = null;
		try {
			//System.out.println("idicon --> "+id);
			m = new Message(Cmd.REQUEST_IMAGE);
			m.writer().writeInt(id);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chat(String text, byte type) { // yeu cau chat trong map
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void chatGlobal(String text, byte type) { // yeu cau chat kenh the gioi
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}		
		
	}
	
	public void requetsInfoMod(short idMod){ // yeu cau thong tin quai khi focus
		Message m = null;
		try{
			m = new Message(Cmd.REQUEST_MONSTER_INFO);
			m.writer().writeShort(idMod);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void sendPlayerAttack(Vector vMob, Vector vChar, int type) { 
		try {
			// 1 player
			// 2 monter

			Message m = null;

			m = new Message(Cmd.ATTACK);
			m.writer().writeByte(type);// type tan cong 0//châ//quai 0
			m.writer().writeByte((byte)vMob.size());//ntarget
//			m.writer().writeByte(1);
//			if (vMob.size() > 0) {
//				if(Char.myChar().mobFocus != null){
//					if(Char.myChar().mobFocus != null){
//						m.writer().writeShort(Char.myChar().mobFocus.mobId);
//						
//					}
//				}

//		} 

				for (int i = 0; i < vMob.size(); i++) {
					Mob b = (Mob) vMob.elementAt(i);
					if(b != null)
						m.writer().writeShort(b.mobId);
				}
			session.sendMessage(m);

		} catch (Exception e) {
		}
	}
	public void itemPick(byte Type, short idItem){ // yêu cầu nhặt item
		Message m = null;
		try {
			m = new Message(Cmd.PICK_REMOVE_ITEM);
			m.writer().writeByte(Type);
			m.writer().writeShort(idItem);
			System.out.println("idItem ---> "+idItem);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void requestNPC(short IdNpc){
		Message m = null;
		try{
			System.out.println("REQUEST CHAR INFO");
			m = new Message(Cmd.NPC_REQUEST);
			m.writer().writeShort(IdNpc);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			m.cleanup();
		}
	}
	
	public void giveUpItem(byte index){
		Message m = null;
		try{
			System.out.println("VUC BO ITEM");
			m = new Message(Cmd.GIVEUP_ITEM);
			m.writer().writeByte(index);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void useItem(byte index, Item type){
		System.out.println("USE ITEM -------> ");
		Message m = null;
		try{
			m = new Message(Cmd.ITEM);
			m.writer().writeByte(type.typeUI);
			m.writer().writeByte(index);
//			for(int i = index; i < Char.myChar().arrItemBag.length; i++)
//			type = Char.myChar().arrItemBag[index];
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteParty(short charID, byte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void AcceptParty(byte type, short charID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void leaveParty(byte type, short charID){
		System.out.println("lave ");
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void kickPlayeleaveParty(byte type, short charID){
		System.out.println("kick ---- >");
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void removeParty(byte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(Char.myChar().charID);
			session.sendMessage(m);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestjoinParty(byte type, short CharID){ // yeu cau vao party
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestinfoPartynearME(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestAddfriend(byte type, short CharID){
		System.out.println("Request add friend");
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void AcceptFriend(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestFriendList(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void deleteFriend(byte type,short myCharID ,short CharID){ // xóa bạn 
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(myCharID);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chatPrivate(byte type, short userid, String contend){
		Message m = null;
		try{
			Cout.println(getClass(), " gui chat chatPrivate "+contend);
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeShort(userid);
			m.writer().writeUTF(contend);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteTrade(byte typeTrade, short CharID){ // mời trao đổi
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void acceptTrade(byte typeTrade, short CharID){ // đồng ý trao đổi 
		
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void moveItemTrade(byte typeTrade, short charID, byte typeMoveItem, short Iditem){ // chuyển item trao đổi
		//System.out.println("SendItem move ----> "+typeTrade+", "+charID+", "+typeMoveItem+", "+Iditem);
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(charID);
			m.writer().writeByte(typeMoveItem);
			m.writer().writeShort(Iditem);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void cancelTrade(byte typeTrade, short CharID){ // hủy trao đổi 
		Cout.println(" cancelTradeTradeeeeeeeeeeeeeeeeeeeee");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(Char.myChar().partnerTrade.charID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void comfirmTrade(byte typeTrade, short CharID){ // lock
		System.out.println("Comfirm Trade");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void doTrade(byte typeTrade, short CharID){ // giao dịch 
		System.out.println("TRADE TRADE TRADE");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestShop(int idMenuShop){ // thong tin shop
		Message m = null;
		try{
			Cout.println(getClass(), "sendquest shop "+idMenuShop);
			m = new Message(Cmd.REQUEST_SHOP);
			m.writer().writeByte(idMenuShop);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void BuyItemShop(byte idmenu,short iditem){ // thong tin shop
		Message m = null;
		try{
			Cout.println(getClass(),idmenu+ "  BuyItemShop "+iditem);
			m = new Message(Cmd.BUY_ITEM_FROM_SHOP);
			m.writer().writeByte(idmenu);
			m.writer().writeShort(iditem);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestMenuShop(){
		Message m = null;
		try{
			m = new Message(Cmd.CMD_DYNAMIC_MENU);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}

	public void Quest(byte type, short questId, byte mainSub)
	{
		Message m= new Message(Cmd.QUEST);		
		try {
			Cout.println(getClass(), "type "+type +"  questId  "+questId+"  mainSub  "+mainSub);
			m.writer().writeByte(type);//0 nhan // 1 tra// 2 huy
			m.writer().writeByte(questId);//
			m.writer().writeByte(mainSub);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void PlayerOut()
	{
		Message m= new Message(Cmd.PLAYER_REMOVE);		
		try {
			Cout.println("PlayerOut mycharr " +Char.myChar().charID);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
}
