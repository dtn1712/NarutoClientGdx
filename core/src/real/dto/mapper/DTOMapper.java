package real.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import Objectgame.MenuTemplate;
import Objectgame.Npc;
import Objectgame.NpcTemplate;
import Objectgame.clan.Clan;
import Objectgame.clan.ClanMember;
import Objectgame.clan.ClanType;
import Objectgame.quest.QuestRequirement;
import Objectgame.quest.QuestTemplate;
import real.dto.clan.ClanBasicInfoDTO;
import real.dto.clan.ClanMemberDTO;
import real.dto.game.MenuTemplateDTO;
import real.dto.game.NpcTemplateDTO;
import real.dto.quest.QuestRequirementDTO;
import real.dto.quest.QuestTemplateDTO;


public class DTOMapper {


    public static MenuTemplate mapMenuTemplateDtoToModel(MenuTemplateDTO menuTemplateDto)
    {
        return new MenuTemplate(menuTemplateDto.getId(), menuTemplateDto.getName());
    }

    public static NpcTemplate mapNpcTemplateDtoToModel(NpcTemplateDTO npcTemplateDto)
    {
        NpcTemplate npcTemplate = new NpcTemplate();
        npcTemplate.setNpcTemplateId(npcTemplateDto.getNpcTemplateId());
        npcTemplate.setName(npcTemplateDto.getName());
        npcTemplate.setHeadId(npcTemplateDto.getHeadId());
        npcTemplate.setBodyId(npcTemplateDto.getBodyId());
        npcTemplate.setLegId(npcTemplateDto.getLegId());
        npcTemplate.setIdavatar(npcTemplateDto.getAvatar());

        if (npcTemplate.npcTemplateId != Npc.idXaPhu && npcTemplate.headId == -1 && npcTemplate.bodyId == -1 &&
                npcTemplate.legId == -1)
        {
            npcTemplate.typeKhu = -1;
        }

        return npcTemplate;
    }

    public static ClanMember mapClanMemberDtoToModel(ClanMemberDTO clanMemberDto)
    {
        ClanMember clanMember = new ClanMember();
        clanMember.setId(clanMemberDto.getId());
        clanMember.setName(clanMemberDto.getName());
        clanMember.setIdHead(clanMemberDto.getIdHead());
        clanMember.setLevel(clanMemberDto.getLevel());
        clanMember.setOnline(clanMember.isOnline());
        clanMember.setPosition(clanMember.getPosition());
        return clanMember;
    }

    public static Clan mapClanBasicDtoToModel(ClanBasicInfoDTO clanBasicInfoDto)
    {
        Clan clan = new Clan();
        clan.setId(clanBasicInfoDto.getId());
        clan.setClanType((ClanType.GLOBAL.name().equals(clanBasicInfoDto.getClanType())) ? ClanType.GLOBAL : ClanType.LOCAL);
        clan.setName(clanBasicInfoDto.getName());
        clan.setHost(mapClanMemberDtoToModel(clanBasicInfoDto.getHost()));
        clan.setTotalMembers(clanBasicInfoDto.getTotalMembers());
        return clan;
    }


    public static QuestTemplate mapQuestTemplateDtoToModel(QuestTemplateDTO questTemplateDto)
    {
        List<QuestRequirement> requirements = new ArrayList<QuestRequirement>();
        for (QuestRequirementDTO questRequirementDTO : questTemplateDto.getRequirements()) {
            requirements.add(mapQuestRequirementDtoToModel(questRequirementDTO));
        }

        QuestTemplate questTemplate = new QuestTemplate();
        questTemplate.setId(questTemplateDto.getId());
        questTemplate.setContent(questTemplateDto.getContent());
        questTemplate.setName(questTemplateDto.getName());
        questTemplate.setLv(questTemplateDto.getLv());
        questTemplate.setIdNpcReceive(questTemplateDto.getIdNpcReceive());
        questTemplate.setIdNpcResolve(questTemplateDto.getIdNpcReceive());
        questTemplate.setSupportContent(questTemplateDto.getSupportContent());
        questTemplate.setResolveContent(questTemplateDto.getResolveContent());
        questTemplate.setShortContent(questTemplateDto.getShortContent());
        questTemplate.setRemindContent(questTemplateDto.getRemindContent());
        questTemplate.setExp(questTemplateDto.getExp());
        questTemplate.setGold(questTemplateDto.getGold());
        questTemplate.setXu(questTemplateDto.getXu());
        questTemplate.setRequirements(requirements);
        return questTemplate;
    }

    private static QuestRequirement mapQuestRequirementDtoToModel(QuestRequirementDTO questRequirementDto)
    {
        QuestRequirement questRequirement = new QuestRequirement();
        questRequirement.setId(questRequirementDto.getId());
        questRequirement.setQuestTemplateId(questRequirementDto.getQuestTemplateId());
        questRequirement.setType(questRequirementDto.getType());
        questRequirement.setKey(questRequirementDto.getKey());
        questRequirement.setValue(questRequirementDto.getValue());
        questRequirement.setDescription(questRequirementDto.getDescription());
        return questRequirement;
    }

}
