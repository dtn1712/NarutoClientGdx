package real.dto.clan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ClanBasicInfoDTO {
    private long id;
    private String name;
    private String clanType;
    private ClanMemberDTO host;
    private int totalMembers;
}
