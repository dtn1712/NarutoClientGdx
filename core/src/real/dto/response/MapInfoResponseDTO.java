package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MapInfoResponseDTO {

    private short id;
    private byte region;
    private short charX;
    private short charY;
    private byte mapTileId;
    private List<LineMapInfoResponseDTO> listLineMap;
    private List<MapHtDataDTO> listMapHtData;
    private List<MapMonsterInfoDTO> listMonster;
    private List<MapItemDTO> listItem;
    private List<ImageMonsterDTO> listMonsterImage;
}
