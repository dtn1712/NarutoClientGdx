package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.game.MenuTemplateDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MenuNpcResponseDTO {
    private int idNpc;
    private List<MenuTemplateDTO> listMenu;
}
