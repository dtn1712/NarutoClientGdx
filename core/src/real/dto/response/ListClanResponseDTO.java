package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.clan.ClanBasicInfoDTO;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ListClanResponseDTO {
    private String clanType;
    private List<ClanBasicInfoDTO> clans;
}
