package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.minigame.MiniGameTemplateDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class JoinMiniGameResponseDTO {
    private boolean inMiniGame;
    private MiniGameTemplateDTO miniGameTemplate;
}
