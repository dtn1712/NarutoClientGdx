package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MoveResponseDTO {
    private long playerId;
    private short x;
    private short y;
    private byte direction;
    private byte status;
    private short velocityX;
    private short velocityY;
}

