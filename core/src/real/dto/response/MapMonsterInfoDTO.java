package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MapMonsterInfoDTO {
    private int id;
    private short idTemplate;
    private int hp;
    private byte level;
    private int maxHp;
    private short monX;
    private short monY;
    private boolean boss;
    private boolean bossAppear;
    private byte[] infoImage;
    private byte[] infoFrame;
    private short rangeMove;
    private short type;
    private short speed;
}
