package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class PartyResponseDTO {
    private byte type;
    private long id;
    private String content;
    private int idParty;
    private long idCharLeader;
    private List<PartyMemberInfoDTO> partyMemberInfo;

}
