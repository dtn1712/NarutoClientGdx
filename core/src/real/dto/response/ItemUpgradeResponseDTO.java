package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ItemUpgradeResponseDTO {
    private byte type;
    private int feeAmount;
    private int successRate;
    private short[] listItemTemplateId;
}
