package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class LoginResponseDTO {

    private boolean success;
    private String errorMessage;

    private boolean showPayment;

    private byte[] njArrow;
    private byte[] njEffect;
    private byte[] njImages;
    private byte[] njPart;
    private byte[] njSkill;

    private int[][][] hoaQuocSkin;
    private int[][][] thuyQuocSkin;
    private int[][][] thoQuocSkin;
    private int[][][] loiQuocSkin;
    private int[][][] phongQuocSkin;
}
