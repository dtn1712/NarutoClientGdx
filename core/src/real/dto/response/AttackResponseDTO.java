package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class AttackResponseDTO {

    private List<TargetDTO> targets;
    private long attackerId;
    private int attackerMp;
    private byte attackerType;
    private int skillAttackId;
    private int skillEffectId;
    private long skillCoolDownTime;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuppressWarnings("PMD.UnusedPrivateField")
    public static class TargetDTO {
        private Long id;
        private byte targetType;
        private int hp;
        private int hitDamage;
    }

}
