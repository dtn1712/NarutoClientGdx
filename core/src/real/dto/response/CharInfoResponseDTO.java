package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.clan.ClanBasicInfoDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharInfoResponseDTO {
    private long id;
    private String charName;
    private int maxHp;
    private int hp;
    private short level;
    private boolean jinchuuriki;
    private boolean ninjaExile;
    private ClanBasicInfoDTO globalClan;
    private ClanBasicInfoDTO localClan;
    private short headId;
    private short bodyId;
    private short legId;
}
