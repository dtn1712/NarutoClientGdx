package real.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.minigame.BombZoneDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BombZoneResponseDTO {
    List<BombZoneDTO> bombZones;
}
