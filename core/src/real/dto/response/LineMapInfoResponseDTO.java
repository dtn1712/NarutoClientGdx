package real.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class LineMapInfoResponseDTO {
    private short mapTemplateId;
    private short minX;
    private short minY;
    private short maxX;
    private short maxY;
}
