package real.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharExpResponseDTO {
    private long charId;
    private long exp;
//    public List<PartyMemberBonusExpDTO> partyMemberBonusExp;

}
