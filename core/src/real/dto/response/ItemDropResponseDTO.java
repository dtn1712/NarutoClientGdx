package real.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ItemDropResponseDTO {
    private byte actorType;
    private long actorId;
    private short idTemplate;
    private int itemId;
}
