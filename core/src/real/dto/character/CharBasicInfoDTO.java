package real.dto.character;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharBasicInfoDTO {

    private long id;
    private String charName;
    private byte gender;
    private byte charClass;
    private short level;
    private short headId;
    private short bodyId;
    private short legId;
}
