package real.dto.character;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.game.ItemAttributeDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CharWearingResponseDTO {
    private long playerId;
    CharEquipmentItemDTO[] items;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuppressWarnings("PMD.UnusedPrivateField")
    public static class CharEquipmentItemDTO {
        private short itemTemplateId;
        private short levelUpgrade;
        private ItemAttributeDTO addedAttribute;
    }
}
