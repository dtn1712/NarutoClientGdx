package real.dto.character;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import real.dto.game.ItemTypeDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CharConfigDTO {

    private ItemTypeDTO[] equipmentPositions;

}
