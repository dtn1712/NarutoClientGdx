package real.dto.character;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class SkillLevelInfoDTO {
    private short lvChar;
    private short mp;
    private long coolDown;
    private short timeBuff;
    private short timeEff;
    private byte numTarget;
    private short rangeX;
    private short rangeY;
    private short damage;
    private int price;
}
