package real.dto.character;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MainCharInfoDTO {

    private long id;

    private String charName;
    private byte idClass;
    private byte gender;
    private int level;
    private int percent;
    private int hp;
    private int maxHp;
    private int mp;
    private int maxMp;

    private int khaiMon;
    private int huuMon;
    private int sinhMon;
    private int doMon;
    private int canhMon;
    private int thuongMon;
    private int kinhMon;
    private int tuMon;
    private int basePoint;

    private byte country;

    private short headId;
    private short bodyId;
    private short legId;

    private short baseSpeed;
    private short addedSpeed;

    private int sizeAttribute;
    private List<CharAttributeDTO> charAttribute;


}

