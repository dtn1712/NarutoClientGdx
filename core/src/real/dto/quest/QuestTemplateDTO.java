package real.dto.quest;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class QuestTemplateDTO {

    private int id;
    private short lv;
    private short idNpcReceive;
    private short idNpcResolve;
    private String name;
    private String content;
    private String supportContent;
    private String resolveContent;
    private String shortContent;
    private String remindContent;
    private int exp;
    private int gold;
    private int xu;
    private List<QuestRequirementDTO> requirements;
}
