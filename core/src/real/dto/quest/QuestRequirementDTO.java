package real.dto.quest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class QuestRequirementDTO {
    private int id;
    private int questTemplateId;
    private String type;
    private String key;
    private String value;
    private String description;
}
