package real.dto.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemAttributeDTO {
    private int khaiMon;                     //tăng hp:Khai Môn + HP
    private int huuMon;                      //Hưu Môn + Tấn Công Vật lý (Tăng Dmg đánh nếu khác hệ)
    private int sinhMon;                     //Sinh Môn + Tấn Công Tuyệt Kĩ (Tăng Dmg Skill)
    private int doMon;                       //Đỗ Môn + Né Tránh (Né)
    private int canhMon;                     //Cảnh Môn + Chống Đỡ (Giảm Dmg/Block Dmg)
    private int thuongMon;                   //Thương Môn + Bạo Kích (Critical/Nhảy Dmg)
    private int kinhMon;                     //Kinh Môn + Tốc Đánh (giảm count down của skill)
    private int tuMon;                       //Tử Môn + Chakra (Chakra khỏi điểm từ đầu) :mp
    private int luck;
}
