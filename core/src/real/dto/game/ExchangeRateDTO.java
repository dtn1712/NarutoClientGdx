package real.dto.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateDTO {

    private Long gameMoneyAmount;
    private String gameMoneyUnit;
    private Long realMoneyAmount;
    private String realMoneyUnit;
    private String display;
}
