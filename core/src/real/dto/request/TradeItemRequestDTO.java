package real.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class TradeItemRequestDTO {

    private byte tradeRequestType;
    private long playerJoinId;
    private byte moveItemType;
    private short itemId;
    private long xu;

}
