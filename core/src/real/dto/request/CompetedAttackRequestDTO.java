package real.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CompetedAttackRequestDTO {

    private byte actionType;
    private long playerInvitedId;
//    private int betAmount;
    private byte betType;

}
