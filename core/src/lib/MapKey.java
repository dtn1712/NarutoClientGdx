package lib;

import com.badlogic.gdx.Input;

import java.util.HashMap;
import java.util.Map;

public class MapKey {

    private static Map<String, Integer> h = new HashMap<String, Integer>();

    public static void load() {
        h.put("A", (int) 'a');
        h.put("B", (int) 'b');
        h.put("C", (int) 'c');
        h.put("D", (int)'d');
        h.put("E", (int)'e');
        h.put("F", (int)'f');
        h.put("G", (int)'spriteBatch');
        h.put("H", (int)'h');
        h.put("I", (int)'i');
        h.put("J", (int)'j');
        h.put("K", (int)'k');
        h.put("L", (int)'l');
        h.put("M", (int)'m');
        h.put("N", (int)'n');
        h.put("O", (int)'o');
        h.put("P", (int)'p');
        h.put("Q", (int)'q');
        h.put("R", (int)'r');
        h.put("S", (int)'s');
        h.put("T", (int)'t');
        h.put("U", (int)'u');
        h.put("V", (int)'v');
        h.put("W", (int)'w');
        h.put("X", (int)'x');
        h.put("Y", (int)'y');
        h.put("Z", (int)'z');
        h.put("0", (int)'0');
        h.put("1", (int)'1');
        h.put("2", (int)'2');
        h.put("3", (int)'3');
        h.put("4", (int)'4');
        h.put("5", (int)'5');
        h.put("6", (int)'6');
        h.put("7", (int)'7');
        h.put("8", (int)'8');
        h.put("9", (int)'9');
        h.put("SPACE", (int)' ');
        h.put("F1", -21);
        h.put("F2", -22);
        h.put("EQUALS", -25);
        h.put("MINUS", 45);
        h.put("F3", -23);
        h.put("UP", -1);
        h.put("DOWN", -2);
        h.put("LEFT", -3);
        h.put("RIGHT", -4);
        h.put("BACKSPACE", -8);
        h.put("PERIOD", (int)'.');
        h.put("AT", (int)'@');
        h.put("TAB", -26);
        h.put("DELET", -8);
    }

    public static int map(int kyeCode) {
        try {
            String k = Input.Keys.toString(kyeCode).toUpperCase();
            return  h.get(k);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return 0;
    }
}

