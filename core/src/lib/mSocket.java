package lib;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class mSocket {
	Socket s;

	public mSocket(String str, int port) {
		try {
			s = new Socket(str, port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setKeepAlive(boolean isAlive){
		try {
			s.setKeepAlive(isAlive);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public DataOutputStream getOutputStream(){
		try {
			DataOutputStream dos=new DataOutputStream(s.getOutputStream());
			return dos ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public DataInputStream getInputStream(){
		try {
			DataInputStream dis=new DataInputStream(s.getInputStream());
			return dis ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
