package lib;



import GuiOut.loadImageInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.thdgaming.naruto.GameCanvas;
import com.team.njonline.GameMidlet;
import com.thdgaming.naruto.MGraphics;

import real.mFont;


import model.Command;
import model.IActionListener;
import model.Image;
import model.Screen;
import model.mResources;
import model.myEditText;
import model.KeyConstant;

public class TField implements IActionListener{
	public int x;
	public int y;
	public int width = 200;
	public int height = 19;
	public boolean isFocus,isOKReturn,isPaintImg;
	public boolean lockArrow = false;
	public boolean paintFocus = true;
	public static int typeXpeed = 2;

	private static final int[] MAX_TIME_TO_CONFIRM_KEY = { 18, 14, 11, 9, 6, 4,
		2 };
	private static int CARET_HEIGHT = 0;
	private static final int CARET_WIDTH = 1;
	private static final int CARET_SHOWING_TIME = 5;
	private static final int TEXT_GAP_X = 4;
	private static final int MAX_SHOW_CARET_COUNER = 10;
	public static final int INPUT_TYPE_ANY = 0;
	public static final int INPUT_TYPE_NUMERIC = 1;
	public static final int INPUT_TYPE_PASSWORD = 2;
	public static final int INPUT_ALPHA_NUMBER_ONLY = 3;

	public String title ="";
	private String text = "";
	private String passwordText = "";
	private String paintedText = "";
	private int caretPos = 0;
	private int counter = 0;
	private int indexOfActiveChar = 0;
	private int maxTextLenght = 500;
	private int offsetX = 0;
	private int lastKey = -1984;
	private int keyInActiveState = 0;
	private int showCaretCounter = MAX_SHOW_CARET_COUNER;
	public int inputType = INPUT_TYPE_ANY;
	public static boolean isQwerty;
	public static int typingModeAreaWidth;
	public int mode = 0;
	public static final String modeNotify[] = { "abc", "Abc", "ABC", "123" };
	public static final int NOKIA = 0;
	public static final int MOTO = 1;
	public static final int ORTHER = 2;
	public static int changeModeKey = 11;
	public static GameCanvas c;
	public static GameMidlet m;
	public boolean justReturnFromTextBox;

	static mFont curFont = mFont.tahoma_8b;

	public boolean visible = false;

	public int ID;

	public myEditText editText;

	// public static myEditText editTextField;

public void doChangeToTextBox(boolean is) {
		
		System.out.println("goi ");
		isOKReturn = false;
		Gdx.input.getTextInput(new TextInputListener() {			
			@Override
			public void input(String text) {
				// TODO Auto-generated method stub
				setText(text);
				isOKReturn = true;
				isFocus = false;
				
			}			
			@Override
			public void canceled() {
				// TODO Auto-generated method stub
				//justReturnFromTextBox = true;
				isFocus = false;
				
			}
		}, "Nhập", text);	
//		Gdx.app.postRunnable(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//			//	justReturnFromTextBox=false;
//			
//			}
//		});
	
		
	}
	
	public void doChangeToTextBox() {
		doChangeToTextBox(false);
	}

	public static boolean setNormal(char ch) {
		if ((ch < '0' || ch > '9') && (ch < 'A' || ch > 'Z')
				&& (ch < 'a' || ch > 'z')) {
			return false;
		}
		return true;
	}

	public Command cmdClear;

	public Command cmdExit;

	private void init() {
		CARET_HEIGHT = curFont.getHeight() + 1;
		cmdClear = new Command(mResources.DELETE, this, 1000, null);

		if (parentScr != null)
			parentScr.right = cmdClear;
		typingModeAreaWidth = curFont.getWidth("ABC") + 8;

		GameMidlet.allField.add(this);

	}

	Screen parentScr;

	public TField(Screen parentScr) {
		this.parentScr = parentScr;
		text = "";
		init();
	}

	public TField() {
		text = "";
		init();
	}

	public TField(String text, int maxLen, int inputType) {
		this.text = text;
		this.maxTextLenght = maxLen;
		this.inputType = inputType;
		init();
	}

	public void clear() {
		if (caretPos > 0 && text.length() > 0) {
			text = text.substring(0, caretPos - 1)
					+ text.substring(caretPos, text.length());
			caretPos--;
			setOffset();
			setPasswordTest();
		}
//		if (editText != null)
//			editText.clear();
	}

	public void setOffset() {
		if (inputType == INPUT_TYPE_PASSWORD)
			paintedText = passwordText;
		else
			paintedText = text;
		if (offsetX < 0 && mFont.tahoma_8b.getWidth(paintedText) + offsetX < width - TEXT_GAP_X - 13 - typingModeAreaWidth)
			offsetX = width - 10 - typingModeAreaWidth - mFont.tahoma_8b.getWidth(paintedText);
		if (offsetX + mFont.tahoma_8b.getWidth(paintedText.substring(0, caretPos)) <= 0) {
			offsetX = -mFont.tahoma_8b.getWidth(paintedText.substring(0, caretPos));
			offsetX = offsetX + 40;
		} else if (offsetX + mFont.tahoma_8b.getWidth(paintedText.substring(0, caretPos)) >= width - 12 - typingModeAreaWidth)
			offsetX = width - 10 - typingModeAreaWidth - mFont.tahoma_8b.getWidth(paintedText.substring(0, caretPos)) - 2 * TEXT_GAP_X;
		if (offsetX > 0)
			offsetX = 0;
	}

	private void keyPressedAny(int keyCode) {

	}

	private void keyPressedAscii(int keyCode) {

	}

	public void keyHold(int keyCode) {

	}

	public boolean keyPressed(int keyCode) {
		// BB:
		System.out.println("TF KEY PRESSS");
		if (keyCode == 8 || keyCode == -8 || keyCode == 204) {
			clear();
			return true;
		}

		if (keyCode >= 'A' && keyCode <= 'z') {
			isQwerty = true;
			typingModeAreaWidth = 0;
		}
		if (isQwerty) {// BB:
			// Nokia E71 character '_' by 2x '-'
			if (keyCode == '-') {
				if (keyCode == lastKey
						&& keyInActiveState < MAX_TIME_TO_CONFIRM_KEY[typeXpeed]) {
					text = text.substring(0, caretPos - 1) + '_';
					this.paintedText = text;
					setPasswordTest();
					setOffset();
					lastKey = -1984;
					return false;
				}
				lastKey = '-';
			}
			if (keyCode >= 32) {
				keyPressedAscii(keyCode);
				return false; // swallow
			}
		}
		if (keyCode == changeModeKey) {
			mode++;
			if (mode > 3)
				mode = 0;
			keyInActiveState = 1;
			lastKey = keyCode;
			return false; // swallow
		}
		if (keyCode == '*')
			keyCode = '9' + 1;
		if (keyCode == '#')
			keyCode = '9' + 2;

		if (keyCode >= '0' && keyCode <= '9' + 2) {
			if (inputType == INPUT_TYPE_ANY || inputType == INPUT_TYPE_PASSWORD
					|| inputType == INPUT_ALPHA_NUMBER_ONLY)
				keyPressedAny(keyCode);
			else if (inputType == INPUT_TYPE_NUMERIC) {
				keyPressedAscii(keyCode);
				keyInActiveState = 1;
			}
		} else {
			indexOfActiveChar = 0;
			lastKey = -1984;
			if (keyCode == KeyConstant.KEY_LEFT && !lockArrow) {
				if (caretPos > 0) {
					caretPos--;
					setOffset();
					showCaretCounter = MAX_SHOW_CARET_COUNER;
					return false;
				}
			} else if (keyCode == KeyConstant.KEY_RIGHT && !lockArrow) {
				if (caretPos < text.length()) {
					caretPos++;
					setOffset();
					showCaretCounter = MAX_SHOW_CARET_COUNER;
					return false;
				}
			} else if (keyCode == KeyConstant.KEY_CLEAR) {
				clear();
				return false;
			} else {
				lastKey = keyCode;
			}
		}

		return true; // Not swallow
	}
	public void paintInputTf(MGraphics g, boolean is, int x, int y, int w, int h, int xText, int yText, String text,
                             String info, Bitmap imgTf) {
		// graphic.setColor(0x777777);
		g.setColor(0);
		if (is) {
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/, Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)), 0, x, y, MGraphics.TOP| MGraphics.LEFT);
			for (int i = 0; i < (w - 2 * Image.getWidth(loadImageInterface.imgTf3)) / Image.getWidth(loadImageInterface.imgTf3); i++)
				g.drawRegion(loadImageInterface.imgTf3, 0, 0/*4 * (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf3), (Image.getHeight(loadImageInterface.imgTf3)), 0, x + Image.getWidth(loadImageInterface.imgTf3) + i * Image.getWidth(loadImageInterface.imgTf3), y,  MGraphics.TOP| MGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf3, 0, 0/* * (Image.getHeight(imgTf)/3)*/,
					 (w - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3),
					 (Image.getHeight(loadImageInterface.imgTf3)), 0,
					 x + w - Image.getWidth(loadImageInterface.imgTf3)-(	 (w - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3)),
					 y,  MGraphics.TOP| MGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/,
					Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)),
					2, x + w - Image.getWidth(loadImageInterface.imgTf2), y,  MGraphics.TOP| MGraphics.LEFT,true);
			
		} else {
			g.drawRegion(loadImageInterface.imgTf0, 0, 0 /** (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x, y,  MGraphics.TOP| MGraphics.LEFT);
			for (int i = 0; i < (w - 2 *Image.getWidth(loadImageInterface.imgTf0)) / Image.getWidth(loadImageInterface.imgTf0); i++)
				g.drawRegion(loadImageInterface.imgTf1, 0,  0, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x + Image.getWidth(loadImageInterface.imgTf0) + i * Image.getWidth(loadImageInterface.imgTf0), y,  MGraphics.TOP| MGraphics.LEFT);
			g.drawRegion(loadImageInterface.imgTf0, 0, 0/*2 * (Image.getHeight(imgTf)/3)*/,
					 Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)),
					2, x + w - Image.getWidth(imgTf), y,  MGraphics.TOP| MGraphics.LEFT);
			g.drawRegion(loadImageInterface.imgTf1, 0, 0/*1 * (Image.getHeight(imgTf)/3)*/, 
					(w - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0),
					(Image.getHeight(loadImageInterface.imgTf1)), 0,
					x + w -  Image.getWidth(imgTf)
					-((w - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0)),
					y,  MGraphics.TOP| MGraphics.LEFT,true);
		
		}
		// graphic.drawRect(x + 1, y + 1, w - 2, h - 2);
//		graphic.setClip(x + 3, y + 1, w - 4, h);

//		if (text != null && !text.equals(""))
//			mFont.tahoma_8b.drawString(graphic, text, xText, yText - 2, 0);
//		else if (info != null) {
//			if (is)
//				mFont.tahoma_7b_focus.drawString(graphic, info, xText, yText + 2, 0);
//			else
//				mFont.tahoma_7b_unfocus.drawString(graphic, info, xText, yText + 2, 0);
//		}
	}
	public static void paintInputTf(MGraphics g, boolean is, int x, int y, int w, int h, int xText, int yText, String text,
                                    String info, Bitmap imgTf, int w2) {
		// graphic.setColor(0x777777);
		g.setColor(0);
		if (is) {
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/, Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)), 0, x, y, MGraphics.TOP| MGraphics.LEFT);
			for (int i = 0; i < (w2 - 2 * Image.getWidth(loadImageInterface.imgTf3)) / Image.getWidth(loadImageInterface.imgTf3); i++)
				g.drawRegion(loadImageInterface.imgTf3, 0, 0/*4 * (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf3), (Image.getHeight(loadImageInterface.imgTf3)), 0, x + Image.getWidth(loadImageInterface.imgTf3) + i * Image.getWidth(loadImageInterface.imgTf3), y,  MGraphics.TOP| MGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf3, 0, 0/* * (Image.getHeight(imgTf)/3)*/,
					 (w2 - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3),
					 (Image.getHeight(loadImageInterface.imgTf3)), 0,
					 x + w2 - Image.getWidth(loadImageInterface.imgTf3)-(	 (w2 - 2 *Image.getWidth(loadImageInterface.imgTf3)) % Image.getWidth(loadImageInterface.imgTf3)),
					 y,  MGraphics.TOP| MGraphics.LEFT,true);
			
			g.drawRegion(loadImageInterface.imgTf2, 0, 0 /** (Image.getHeight(loadImageInterface.imgTf2))*/,
					Image.getWidth(loadImageInterface.imgTf2), (Image.getHeight(loadImageInterface.imgTf2)),
					2, x + w2 - Image.getWidth(loadImageInterface.imgTf2), y,  MGraphics.TOP| MGraphics.LEFT,true);
			
		} else {
			if(w2>=loadImageInterface.imgTf0.width*2)
			{
				g.drawRegion(loadImageInterface.imgTf0, 0, 0 /** (Image.getHeight(imgTf)/3)*/, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x, y,  MGraphics.TOP| MGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgTf0, 0, 0/*2 * (Image.getHeight(imgTf)/3)*/,
						 Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)),
						2, x + w2 - Image.getWidth(imgTf), y,  MGraphics.TOP| MGraphics.LEFT);
				for (int i = 0; i < (w2 - 2 *Image.getWidth(loadImageInterface.imgTf0)) / Image.getWidth(loadImageInterface.imgTf0); i++)
					g.drawRegion(loadImageInterface.imgTf1, 0,  0, Image.getWidth(loadImageInterface.imgTf0), (Image.getHeight(loadImageInterface.imgTf0)), 0, x + Image.getWidth(loadImageInterface.imgTf0) + i * Image.getWidth(loadImageInterface.imgTf0), y,  MGraphics.TOP| MGraphics.LEFT);
				
				g.drawRegion(loadImageInterface.imgTf1, 0, 0/*1 * (Image.getHeight(imgTf)/3)*/, 
						(w2 - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0),
						(Image.getHeight(loadImageInterface.imgTf1)), 0,
						x + w2 -  Image.getWidth(imgTf)
						-((w2 - 2 *Image.getWidth(loadImageInterface.imgTf0)) % Image.getWidth(loadImageInterface.imgTf0)),
						y,  MGraphics.TOP| MGraphics.LEFT,true);
				
			}
			else{
				g.drawRegion(loadImageInterface.imgTf0, 0, 0 /** (Image.getHeight(imgTf)/3)*/, w2/2+w2%2, (Image.getHeight(loadImageInterface.imgTf0)), 0, x, y,  MGraphics.TOP| MGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgTf0, 0, 0/*2 * (Image.getHeight(imgTf)/3)*/,
						 w2/2, (Image.getHeight(loadImageInterface.imgTf0)),
						2, x + w2 -w2/2, y,  MGraphics.TOP| MGraphics.LEFT);
				
			}
		
		}
		// graphic.drawRect(x + 1, y + 1, w - 2, h - 2);
//		graphic.setClip(x + 3, y + 1, w - 4, h);

		if (text != null && !text.equals(""))
			mFont.tahoma_8b.drawString(g, text, xText, yText - 2, 0);
//		else if (info != null) {
//			if (is)
//				mFont.tahoma_7b_focus.drawString(graphic, info, xText, yText + 2, 0);
//			else
//				mFont.tahoma_7b_unfocus.drawString(graphic, info, xText, yText + 2, 0);
//		}
	}
	public void paint(MGraphics g) {
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		boolean isFocus = isFocused();
		if (inputType == INPUT_TYPE_PASSWORD)
			paintedText = passwordText;
		else
			paintedText = text;
		if(!isPaintImg)
		paintInputTf(g, isFocus, x, y, width, height*2, TEXT_GAP_X + offsetX + x, y + (height/2 - mFont.tahoma_8b.getHeight()) / 2+2, paintedText, paintedText, loadImageInterface.imgTf);

		if (text != null && !text.equals(""))
			mFont.tahoma_8b.drawString(g, text, TEXT_GAP_X + offsetX + x, y + (height/2 - mFont.tahoma_8b.getHeight()) / 2+2, 0);
		g.setClip(x + 3, y + 1, width - 4, height - 4);
		g.setColor(0);

		if (isFocused() ) {
			if (keyInActiveState == 0 && (showCaretCounter > 0 || (counter / CARET_SHOWING_TIME) % 3 == 0)) {
				g.setColor(0xffefe310);
				g.fillRect(TEXT_GAP_X + 1 + offsetX + x + mFont.tahoma_8b.getWidth(paintedText.substring(0, caretPos)) - CARET_WIDTH,
						y + (height - CARET_HEIGHT) / 2 - 1,
						CARET_WIDTH, CARET_HEIGHT+2,true);
			}
		}
		GameCanvas.resetTrans(g);
	}

	private boolean isFocused() {
		return isFocus;
	}

	private void setPasswordTest() {
		if (inputType == INPUT_TYPE_PASSWORD) {
			passwordText = "";
			for (int i = 0; i < text.length(); i++)
				passwordText = passwordText + "*";
			if (keyInActiveState > 0 && caretPos > 0)
				passwordText = passwordText.substring(0, caretPos - 1)
						+ text.charAt(caretPos - 1)
						+ passwordText.substring(caretPos,
								passwordText.length());
		}
	}

	public void update() {
		counter++;
		height = 19;
		if (keyInActiveState > 0) {
			keyInActiveState--;
			if (keyInActiveState == 0) {
				indexOfActiveChar = 0;
				if (mode == 1 && lastKey != changeModeKey)
					mode = 0;
				lastKey = -1984;
				setPasswordTest();
			}
		}
		if (showCaretCounter > 0)
			showCaretCounter--;
//		if (GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick) {
//			if (GameCanvas.isPointerHoldIn(x, y, width, height)) {

		if (GameCanvas.isPointer(x, y, width, height)){
			GameCanvas.clearPointerEvent();
				setTextBox();
			}
//		}

	}

	public void setTextBox() {

		//if (GameCanvas.isPointer(x, y, width, height)) {
			// if(!isFocus)
			// else
		
			doChangeToTextBox();
			myEditText.isVisible = true;
			isFocus = true;
			// System.out.println("update");
	//	} else {
		//	isFocus = false;
		//}
	}

	public boolean isTouch = false;

	public String getText() {

		return text;
	}

	public void setText(String text) {
		if (text == null)
			return;
		this.text = text;
		this.paintedText = text;
		setPasswordTest();
		caretPos = text.length();
		setOffset();

		// editText.setText(text);
	}

	public void insertText(String text) {
		this.text = this.text.substring(0, caretPos) + text + this.text.substring(caretPos);
		setPasswordTest();
		caretPos += text.length();
		setOffset();
	}

	public int getMaxTextLenght() {
		return maxTextLenght;
	}

	public void setMaxTextLenght(int max) {
		maxTextLenght = max;

		// editText.setMaxTextInput(max);
	}

	public int getIputType() {
		return inputType;
	}

	public void setIputType(int iputType) {
		this.inputType = iputType;

		/*
		 * switch (iputType) { case INPUT_TYPE_ANY:
		 * editText.setInputType(InputType.TYPE_CLASS_TEXT); break; case
		 * INPUT_TYPE_NUMERIC:
		 * editText.setInputType(InputType.TYPE_CLASS_NUMBER); break; case
		 * INPUT_TYPE_PASSWORD:
		 * editText.setInputType(InputType.TYPE_CLASS_TEXT);
		 * editText.setTransformationMethod
		 * (PasswordTransformationMethod.getInstance()); break; case
		 * INPUT_ALPHA_NUMBER_ONLY:
		 * editText.setInputType(InputType.TYPE_CLASS_NUMBER); break; }
		 */

	}

	@Override
	public void perform(int idAction, Object p) {
		switch (idAction) {
		case 1000:
			clear();
			break;

		default:
			break;
		}
		
	}
}
