package lib;

/*
 * DrawRegion class
 * Author : Giang.LC
 * create: 08-03-2011
 *
 * How to use :
 * in class need DrawRegion, import DrawRegion class and insert code:
 * "private DrawRegion  graphic = new DrawRegion(canvas);"
 * when use it just right like j2me.
 */


import java.util.Enumeration;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.thdgaming.naruto.MGraphics;
import com.thdgaming.naruto.GameMidlet;

import model.Paint;
import model.Sprite;

public class Graphics {

    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT90 = 5;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;

    public static final int TRANS_MIRROR = 2;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_MIRROR_ROT180 = 1;
    public static final int TRANS_MIRROR_ROT270 = 4;

    // for graphics anchor
    public static int TOP = 16;
    public static int BOTTOM = 32;
    public static int VCENTER = 2;

    public static int LEFT = 4;
    public static int RIGHT = 8;
    public static int HCENTER = 1;

    public boolean isTranslate, isClip;

    public static Hashtable cachedTextures = new Hashtable();
    private float r, gl, b, a;
    public int clipX, clipY, clipW, clipH;
    public int translateX = 0, translateY = 0;
    public boolean isRorate;
    public float xRotate, yRotate;
    public float rotation;

    public SpriteBatch spriteBatch;

    public Graphics(SpriteBatch spriteBatch) {
        this.spriteBatch = spriteBatch;
    }

    public void drawRegion(Texture imgscr, int x_src, int y_src, int width,
                           int height, int flip, float x_dest, float y_dest, int anchor, boolean isUseSetClip) {
        boolean isScale = false;
        if (imgscr == null)
            return;
        // graphic.draw(imgscr, x_dest, y_dest);
        // if(true)return;
        if (isTranslate) {
            x_dest += translateX;
            y_dest += translateY;
        }
        if (isClipWithWHZero())
            return;
        boolean flipX = false;
        boolean flipY = true;
        int scX = 1;
        int scY = 1;
        int y0 = 0;
        float orx = 0;
        float ory = 0;
        int ixA = 0, iyA = 0;
        switch (anchor) {
            // Graphics.TOP | Graphics.LEFT ok
            case 20:
            case 0:
                ixA = 0;
                iyA = 0;
                break;
            // Graphics.TOP | Graphics.HCENTER//ok
            case 17:
                ixA = width / 2;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90
                        || flip == Sprite.TRANS_MIRROR_ROT90) {
                    ixA = height / 2;
                }
                iyA = 0;
                break;
            // Graphics.TOP | Graphics.RIGHT ok
            case 24:
                ixA = width;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90
                        || flip == Sprite.TRANS_MIRROR_ROT90) {
                    ixA = height;
                }
                iyA = 0;
                break;
            // Graphics.VCENTER | Graphics.LEFT
            case 6:
                ixA = 0;
                iyA = height / 2;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    iyA = width / 2;
                }
                break;
            // Graphics.VCENTER | Graphics.HCENTER
            case 3:
                ixA = width / 2;
                iyA = height / 2;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    ixA = height / 2;
                    iyA = width / 2;
                }
                break;
            // Graphics.VCENTER | Graphics.RIGHT
            case 10:
                ixA = width;
                iyA = height / 2;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    ixA = height;
                    iyA = width / 2;
                }
                break;
            // Graphics.BOTTOM | Graphics.LEFT
            case 36:
                ixA = 0;
                iyA = height;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    iyA = width;
                }
                break;
            // Graphics.BOTTOM | Graphics.HCENTER
            case 33:
                ixA = width / 2;
                iyA = height;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    ixA = height / 2;
                    iyA = width;
                }
                break;
            // Graphics.BOTTOM | Graphics.RIGHT
            case 40:
                ixA = width;
                iyA = height;
                if (flip == Sprite.TRANS_MIRROR_ROT270
                        || flip == Sprite.TRANS_MIRROR_ROT90
                        || flip == Sprite.TRANS_ROT270
                        || flip == Sprite.TRANS_ROT90) {
                    ixA = height;
                    iyA = width;
                }
                break;
        }
        x_dest -= ixA;
        y_dest -= iyA;
        int ix = 0, iy = 0;
        switch (flip) {
            case Sprite.TRANS_MIRROR:
                flip = 0;
                flipX = true;
                break;
            case Sprite.TRANS_ROT180:
                flip = 180;
                iy = -height;
                ix = -width;
                break;
            case Sprite.TRANS_ROT270:
                flip = 90;
                flipX = true;
                scY = -1;
                break;
            case Sprite.TRANS_ROT90:
                flip = 90;
                flipX = true;
                ix = -height;
                iy = -width;
                scX = -1;
                break;
            case Sprite.TRANS_MIRROR_ROT180:
                flip = 0;
                flipY = false;
                break;
            case Sprite.TRANS_MIRROR_ROT270:
                flip = 90;
                flipY = false;
                flipX = true;
                ix = -height;
                iy = -width;
                scX = -1;
                break;
            case Sprite.TRANS_MIRROR_ROT90:
                flip = 270;
                flipY = false;
                flipX = true;
                scX = -1;
                break;
        }
        // orx+=(flip/30);
        // ory+=(flip/30);
        // flip+=30;

        // orx+=30*zoomLevel;
        // ory+=30*zoomLevel;
        if (isRorate) {
            orx = (-x_dest + xRotate * MGraphics.zoomLevel + (isTranslate ? translateX
                    : 0));
            ory = (-y_dest + yRotate * MGraphics.zoomLevel + (isTranslate ? translateY
                    : 0));
            flip += rotation;
        }

        if (isClip && isUseSetClip) {
            beginClip();
        }
        spriteBatch.draw(imgscr, x_dest - ix, y_dest - iy, orx, ory, width, height,
                scX, scY + (isScale ? 0.05f : 0), flip, x_src, y_src, width,
                height, flipX, flipY);
        if (isClip && isUseSetClip)
            endClip0();

    }
    // draw image
    public void drawImage(Texture image, int x, int y) {
        // TODO Auto-generated method stub
//		if (image == null)
//			return;
//		canvas.drawBitmap(image, x, y, null);
    }

    // draw image with anchor
    public void drawImage(Texture img, int x, int y, int anchor, boolean isUseSetClip) {
        drawRegion(img, 0, 0, img.getWidth(), img.getHeight(),
                Sprite.TRANS_NONE, x, y, anchor, isUseSetClip);
    }

    // draw image with rotate
    public void drawImage(Texture image, int x, int y, int rotate, int arChor) {
        // TODO Auto-generated method stub

    }


    public void drawString(String s, int x, float y, BitmapFont font, HAlignment al, boolean isUclip) {
        if (isClipWithWHZero())
            return;
        if (isTranslate) {
            x += translateX;
            y += translateY;
        }
        if (isClip)
            beginClip();
        font.drawMultiLine(spriteBatch, s, x, y, 0, al);
        if (isClip)
            endClip0();
    }

    public void drawString(String s, int x, int y, int Anchor, Paint f) {

    }

    public void fillRect(int x, int y, int w, int h, boolean uClip) {
        if (w < 0 || h < 0 || isClipWithWHZero()) {
            return;
        }
        uClip = true;
        if (isTranslate) {
            x += translateX;
            y += translateY;
        }
        Texture rgb_texture;
        String key = "fr" + r + gl + b + a;
        rgb_texture = (Texture) cachedTextures.get(key);
        if (rgb_texture == null) {
            Pixmap p = new Pixmap(1, 1, Format.RGBA8888);
            p.setColor(r, b, gl, a);
            p.drawPixel(0, 0);
            rgb_texture = new Texture(p);
            p.dispose();
            p = null;
            cache(key, rgb_texture);
        }
        if (isClip && uClip) {
            beginClip();
        }
        spriteBatch.draw(rgb_texture, x, y, 0, 0, 1, 1, w, h, 0, 0, 0, 1, 1, false,
                false);
        if (isClip && uClip)
            endClip0();
//		if(isSetOpacity){
//			isSetOpacity = false;
//			disableBlending();
//		}
    }

    public void drawRect(int x, int y, int w, int h, boolean uClip) {
        int xx = 1;
        fillRect(x, y, w, xx, uClip);
        fillRect(x, y, xx, h, uClip);
        fillRect(x + w, y, xx, h + 1, uClip);
        fillRect(x, y + h, w + 1, xx, uClip);
    }

    public void setAlpha(int alpha) {
        enableBlending(alpha);
    }

    public void setColor(int color) {

        float R = (float) ((color >> 16) & 0xff);
        float B = (float) ((color >> 8) & 0xff);
        float G = (float) (color & 0xff);
        b = B / 256;
        gl = G / 256;
        r = R / 256;
        a = 255f / 255;
    }

    public void setColor(int color, int alpha) {

        float R = (float) ((color >> 16) & 0xff);
        float B = (float) ((color >> 8) & 0xff);
        float G = (float) (color & 0xff);
        b = B / 256;
        gl = G / 256;
        r = R / 256;
        a = (float) alpha / 255;
    }


    public void drawLine(int x1, int y1, int x2, int y2, boolean uClip) {
        if (isClipWithWHZero())
            return;
//		canvas.drawLine(startX, startY, stopX, stopY, graphic);
        Texture rgb_texture;
        String key = "dl" + r + gl + b;
        if (isTranslate) {
            x1 += translateX;
            y1 += translateY;
            x2 += translateX;
            y2 += translateY;
        }
        rgb_texture = (Texture) (Texture) cachedTextures.get(key);
        if (rgb_texture == null) {
            Pixmap p = new Pixmap(1, 1, Format.RGBA8888);
            p.setColor(r, b, gl, a);
            p.drawPixel(0, 0);
            rgb_texture = new Texture(p);
            p.dispose();
            p = null;
            cache(key, rgb_texture);
        }
        float xSl = (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1)
                * (x2 - x1));
        int ySl = MGraphics.zoomLevel;
        Vector2 start = new Vector2(x1, y1);
        Vector2 end = new Vector2(x2, y2);
        float angle = getAngle(start, end);
        if (isClip && uClip) {
            beginClip();
        }
        spriteBatch.draw(rgb_texture, x1, y1, 0, 0, 1, 1, xSl, ySl, angle, x1, y1, 1, 1,
                false, false);
        if (isClip && uClip)
            endClip0();
    }


    public void translate(int tx, int ty) {
//		canvas.translate((float) x, (float) y);
//		translateX += x;
//		translateY += y;
        translateX += tx;
        translateY += ty;
        isTranslate = true;
        if (translateX == 0 && translateY == 0)
            isTranslate = false;

    }

    public int getTranslateX() {
        return translateX;
    }

    public int getTranslateY() {
        return translateY;
    }



    public void setClip(int x, int y, int w, int h) {
        // TODO Auto-generated method stub
        if (isTranslate) {
            x += translateX;
            y += translateY;
        }
        // if(w>GameMidlet.getWidth())
        // w=GameMidlet.getWidth();
        // if(h>GameMidlet.getHeight())
        // h=GameMidlet.getHeight();
        clipX = x;
        clipY = y;
        clipW = w;
        clipH = h;
        isClip = true;

    }


    public void enableBlending(float alpha) {
        spriteBatch.setColor(1f, 1f, 1f, alpha);
    }

    public void disableBlending() {
        spriteBatch.setColor(1f, 1f, 1f, 1f);
    }

    public void drawRoundRect(int x, int y, int Width, int Height, int rX,
                              int rY, Boolean isSetOpacity) {
        // TODO Auto-generated method stub
        drawRect(x, y, Width, Height, isRorate);
//		graphic.setStyle(Paint.Style.STROKE);
//		RectF rect = new RectF(x, y, x + Width, y + Height);
//		canvas.drawRoundRect(rect, rX, rY, graphic);
//		
//		Pixmap pixmap = new Pixmap(Width, Height, Format.RGBA8888);
////
////		 // Pink rectangle
//		 pixmap.fillRectangle(x, y, pixmap.getWidth(), pixmap.getHeight()-2*radius);
//		if(isSetOpacity){
//			disableBlending();
//			isSetOpacity = false;
//		}
    }

    public void fillRoundRect(int x, int y, int w, int h, int aa, int bb,
                              boolean uClip) {
        if (w < 0 || h < 0 || isClipWithWHZero())
            return;
        uClip = true;
        if (isTranslate) {
            x += translateX;
            y += translateY;
        }
        Texture rgb_texture;
        String key = "fr" + r + gl + b + a;
        rgb_texture = (Texture) cachedTextures.get(key);
        if (rgb_texture == null) {
            Pixmap p = new Pixmap(1, 1, Format.RGBA8888);
            p.setColor(r, b, gl, a);
            p.drawPixel(0, 0);
            rgb_texture = new Texture(p);
            p.dispose();
            p = null;
            cache(key, rgb_texture);
        }
        if (isClip && uClip) {
            beginClip();
        }
        spriteBatch.draw(rgb_texture, x, y, 0, 0, 1, 1, w, h, 0, 0, 0, 1, 1, false,
                false);
        if (isClip && uClip)
            endClip0();
//		if(isSetOpacity){
//			isSetOpacity = false;
//			disableBlending();
//		}
    }


    public boolean isClipWithWHZero() {
        if (isClip) {
            if (clipH == 0 || clipW == 0)
                return true;
        }
        return false;
    }

    public void beginClip() {
        Rectangle scissors = new Rectangle();
        Rectangle clipBounds = new Rectangle(clipX, clipY, clipW, clipH);
        ScissorStack.calculateScissors(GameMidlet.me.camera,
                spriteBatch.getTransformMatrix(), clipBounds, scissors);
        ScissorStack.pushScissors(scissors);
    }

    public void endClip0() {
        spriteBatch.flush();
        ScissorStack.popScissors();
        // Pools.free(ScissorStack.popScissors());
    }

    void cache(String key, Texture value) {
        if (cachedTextures.size() > 500) {
            Enumeration<Object> k = cachedTextures.keys();
            while (k.hasMoreElements()) {
                String k0 = (String) k.nextElement();
                Texture img = (Texture) cachedTextures.get(k0);
                cachedTextures.remove(k0);
                cachedTextures.remove(img);
                img.dispose();
                img = null;
            }
            cachedTextures.clear();
            System.gc();
        }
        cachedTextures.put(key, value);
    }

    public void clearCache() {
        Enumeration<Object> k = cachedTextures.keys();
        while (k.hasMoreElements()) {
            String k0 = (String) k.nextElement();
            Texture img = (Texture) cachedTextures.get(k0);
            cachedTextures.remove(img);
            cachedTextures.remove(k0);
            img.dispose();
            img = null;
        }
        cachedTextures.clear();
        System.gc();
    }

    public float getAngle(Vector2 centerPt, Vector2 target) {
        float angle = (float) Math.toDegrees(Math.atan2(target.y - centerPt.y,
                target.x - centerPt.x));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    public void drawText(char[] tempChar, int i, int j, int x, int y, Paint paint) {
        // TODO Auto-generated method stub

    }

    public void drawImage(Texture image, int i, int j, Object object) {
        // TODO Auto-generated method stub

    }


    public void drawRect(int x, int y, int w, int h, Paint paint) {
        // TODO Auto-generated method stub
        int xx = 1;
        fillRect(x, y, w, xx, false);
        fillRect(x, y, xx, h, false);
        fillRect(x + w, y, xx, h + 1, false);
        fillRect(x, y + h, w + 1, xx, false);
    }

    public void translate(float x, float y) {
        // TODO Auto-generated method stub
        translateX += x;
        translateY += y;
        isTranslate = true;
        if (translateX == 0 && translateY == 0)
            isTranslate = false;
    }

    public void drawBitmap(Texture image, int x_dest, int y_dest, Object object) {
        // TODO Auto-generated method stub
        drawImage(image, x_dest, y_dest, 0, false);
    }
}
