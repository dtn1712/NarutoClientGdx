package lib;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.thdgaming.naruto.MGraphics;

public class SysTemFont {
	/*
	 * ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890À�?
	 * ẢÃẠaàáảãạÂẦẤẨẪẬâầấẩẫậĂẰẮẲẴẶăằắẳẵặ
	 * �?đ₫EÈÉẺẼẸeèéẻẽẹÊỀẾỂỄỆê�?ếểễệIÌ�?ỈĨỊiìíỉĩịOÒÓỎÕỌoòó
	 * �?õ�?ÔỒ�?ỔỖỘôồốổỗộƠỜỚỞỠỢơ
	 * �?ớởỡợUÙÚỦŨỤuùúủũụƯỪỨỬỮỰưừứửữựYỲ�?ỶỸỴyỳýỷỹỵ"!`?'.,;:()[]{}<>|/@\^$-%+=#_&~
	 * *“�?«»—–•’…¹²³½
	 */
	public static String charLits =
			" áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêế�?ểễệíìỉĩịóò�?õ�?ôốồổỗộơớ�?ởỡợúùủũụưứừửữựýỳỷỹỵđ�?ÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆ�?ÌỈĨỊÓÒỎÕỌÔ�?ỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰ�?ỲỶỸỴ�?";
	// public Label lb;
	public BitmapFont font;
	private float r, g, b, a;
	public float[] charWidth;
	public byte charHeight;
	private float charSpace;
	float yAddFont;
	public Color cl;

	public SysTemFont(int ID, int color) {
		String name = "barmeneb";
		if ((ID >= 10 && ID <= 18) || (ID >= 20 && ID <= 24)) {
			name = "chelthm7";
			yAddFont = 2.5f;
			if (MGraphics.zoomLevel == 1)
				yAddFont = 0f;
		}
		// else if(ID ==10)
		// {
		// name = "chelthm9";
		// }
		else if (ID >= 25 && ID <= 30) {
			name = "staccato";
		}if (ID ==27) {
			name = "chelthm27";
		}
		font = new BitmapFont(Gdx.files.internal(LibSysTem.font
				+ MGraphics.zoomLevel + "/" + name + ".fnt"),
				Gdx.files.internal(LibSysTem.font + (MGraphics.zoomLevel) + "/"
						+ name + ".png"), true);

		cl = mSystem.setColor(color);
		font.setColor(cl);
		charWidth = new float[charLits.length()];
		for (int i = 0; i < charWidth.length; i++) {
			TextBounds v = font.getBounds(charLits.charAt(i) + "");
			charWidth[i] = (v.width / MGraphics.zoomLevel);
		}
		TextBounds v = font.getBounds("A");
		this.charHeight = (byte) ((byte) v.height / MGraphics.zoomLevel);
		v = font.getBounds(" ");
		this.charSpace = (v.width / MGraphics.zoomLevel);

	}

	public Color rgba8888ToColor(int rgba8888) {
		Color color = new Color();
		color.r = ((rgba8888 & 0xff000000) >>> 24) / 255f;
		color.g = ((rgba8888 & 0x00ff0000) >>> 16) / 255f;
		color.b = ((rgba8888 & 0x0000ff00) >>> 8) / 255f;
		color.a = ((rgba8888 & 0x000000ff)) / 255f;
		return color;
	}

	public SysTemFont(String name, int color, float a) {
		font = new BitmapFont(
				Gdx.files.internal(LibSysTem.font + name + ".fnt"),
				Gdx.files.internal(LibSysTem.font + name + ".png"), true);
		// LabelStyle labelStyle = new LabelStyle();
		// labelStyle.font = font;
		cl = mSystem.setColor(color);

		font.setColor(cl);

		// labelStyle.fontColor =cl;
		// lb = new Label("", labelStyle);
		charWidth = new float[charLits.length()];
		for (int i = 0; i < charWidth.length; i++) {
			TextBounds v = font.getBounds(charLits.charAt(i) + "");
			charWidth[i] = (v.width / MGraphics.zoomLevel);
		}
		TextBounds v = font.getBounds("A");
		this.charHeight = (byte) v.height;
		v = font.getBounds(" ");
		this.charSpace = v.width / MGraphics.zoomLevel;
	}

	public int getWidth(String st) {
		int pos;
		float len = 0;
		for (int i = 0; i < st.length(); i++) {
			pos = charLits.indexOf(st.charAt(i));
			if (pos == -1)
				pos = 0;
			len += (charWidth[pos] + charSpace);
		}
		return (int) len;
	}

	public int convert_RGB_to_ARGB(int rgb) {

		int r = (rgb >> 16) & 0xFF;

		int g = (rgb >> 8) & 0xFF;

		int b = (rgb >> 0) & 0xFF;

		return 0xff000000 | (r << 16) | (g << 8) | b;

	}

	public String[] splitString(String _text, String _searchStr) {
		int count = 0, pos = 0;
		int searchStringLength = _searchStr.length();
		int aa = _text.indexOf(_searchStr, pos);
		while (aa != -1) {
			pos = aa + searchStringLength;
			aa = _text.indexOf(_searchStr, pos);
			count++;
		}
		String[] sb = new String[count + 1];
		int searchStringPos = _text.indexOf(_searchStr);
		int startPos = 0;
		int index = 0;
		while (searchStringPos != -1) {
			sb[index] = _text.substring(startPos, searchStringPos);
			startPos = searchStringPos + searchStringLength;
			searchStringPos = _text.indexOf(_searchStr, startPos);
			index++;
		}
		sb[index] = _text.substring(startPos, _text.length());
		return sb;
	}

	public String[] splitFontArray(String src, int lineWidth) {

		mVector lines = _splitFont(src, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}

	public static String[] _splitString(String _text, String _searchStr) {
		int count = 0, pos = 0;
		int searchStringLength = _searchStr.length();
		int aa = _text.indexOf(_searchStr, pos);
		while (aa != -1) {
			pos = aa + searchStringLength;
			aa = _text.indexOf(_searchStr, pos);
			count++;
		}
		String[] sb = new String[count + 1];
		int searchStringPos = _text.indexOf(_searchStr);
		int startPos = 0;
		int index = 0;
		while (searchStringPos != -1) {
			sb[index] = _text.substring(startPos, searchStringPos);
			startPos = searchStringPos + searchStringLength;
			searchStringPos = _text.indexOf(_searchStr, startPos);
			index++;
		}
		sb[index] = _text.substring(startPos, _text.length());
		return sb;
	}

	public String replace(String _text, String _searchStr,
			String _replacementStr) {
		StringBuffer sb = new StringBuffer();
		int searchStringPos = _text.indexOf(_searchStr);
		int startPos = 0;
		int searchStringLength = _searchStr.length();
		while (searchStringPos != -1) {
			sb.append(_text.substring(startPos, searchStringPos)).append(
					_replacementStr);
			startPos = searchStringPos + searchStringLength;
			searchStringPos = _text.indexOf(_searchStr, startPos);
		}
		sb.append(_text.substring(startPos, _text.length()));
		return sb.toString();
	}

	public String[] splitFont(String src, int lineWidth) {
		mVector lines = _splitFont(src, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}

	public mVector _splitFont(String src, int lineWidth) {
		mVector lines = new mVector();
		if (lineWidth <= 0) {
			lines.add(src);
			return lines;
		}
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n') {
				lines.addElement(line);
				line = "";
			} else {
				line += src.charAt(i);
				if (getWidth(line) > lineWidth) {
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if (line.charAt(j) == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.length() - 1;
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if (i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}

	public int getHeight() {
		return charHeight;
	}

	public void drawString(MGraphics g, String st, int x, int y, int align, Boolean isUclip) {
		// lb.setText(st);
		// CharSequence s= st;
		HAlignment al = HAlignment.LEFT;
		switch (align) {
		case 0:
			// lb.setAlignment(Align.left,Align.left);
			// font.drawMultiLine(graphic.graphic, s, x, y, 0, HAlignment.LEFT);
			break;
		case 1:
			// lb.setAlignment(Align.right,Align.right);
			al = HAlignment.RIGHT;
			// font.drawMultiLine(graphic.graphic, s, x, y, 0, HAlignment.RIGHT);
			break;
		case 2:
			// lb.setAlignment(Align.center,Align.center);
			al = HAlignment.CENTER;
			break;
		}
		// font.drawMultiLine(graphic.graphic, s, x, y, 0, al);
		g.drawString(st, x, y + yAddFont, font, al,isUclip);
	}
	
	public void drawString(MGraphics g, String st, int x, int y, int align) {
		// lb.setText(st);
		// CharSequence s= st;
		HAlignment al = HAlignment.LEFT;
		switch (align) {
		case 0:
			// lb.setAlignment(Align.left,Align.left);
			// font.drawMultiLine(graphic.graphic, s, x, y, 0, HAlignment.LEFT);
			break;
		case 1:
			// lb.setAlignment(Align.right,Align.right);
			al = HAlignment.RIGHT;
			// font.drawMultiLine(graphic.graphic, s, x, y, 0, HAlignment.RIGHT);
			break;
		case 2:
			// lb.setAlignment(Align.center,Align.center);
			al = HAlignment.CENTER;
			break;
		}
		// font.drawMultiLine(graphic.graphic, s, x, y, 0, al);
		g.drawString(st, x, y + yAddFont, font, al);
	}

	public MyVT splitFontVector(String src, int lineWidth) {
		MyVT lines = new MyVT("vLine");
		if (lineWidth <= 0) {
			lines.add(src);
			return lines;
		}
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n' || src.charAt(i) == '\b') {
				lines.addElement(line);
				line = "";
			} else {
				line += src.charAt(i);
				if (getWidth(line) > lineWidth) {
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if (line.charAt(j) == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.length();
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if (i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}
}
