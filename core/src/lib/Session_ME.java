package lib;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.thdgaming.naruto.GameMidlet;

import network.IMessageHandler;
import network.ISesion;
import network.Message;


import model.Cmd;

public class Session_ME implements ISesion {
	protected static Session_ME instance = new Session_ME();

	public static Session_ME gI() {
		return instance;
	}

	private DataOutputStream dos;
	public DataInputStream dis;
	public static IMessageHandler messageHandler;
	private mSocket sc;
	public boolean connected, connecting;
	private final Sender sender = new Sender();
	public Thread initThread;
	public Thread collectorThread;
	public Thread sendThread;
	public int sendByteCount;
	public int recvByteCount;
	boolean getKeyComplete;
	public byte[] key = null;
	private byte curR, curW;
	long timeConnected;
	public String strRecvByteCount = "";

	public void clearSendingMessage() {
		sender.sendingMessage.removeAllElements();
	}
	
	public boolean isConnected() {
		return connected;
	}
	public void setHandler(IMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
	}

	public void connect(String host,int port) {
		Cout.println(getClass(),"connect to IP = " + host+" Port = "+port);
		if (connected || connecting) {
			return;
		} else {
			sender.removeAllMessage();
			getKeyComplete = false;
			sc = null;
			initThread = new Thread(new NetworkInit(host,port));
			initThread.start();
		}
	}//

	public static boolean isCancel;

	class NetworkInit implements Runnable {
		private final String host;
		int port;

		NetworkInit(String host,int port) {
			this.host = host;
			this.port = port;
		}

		public void run() {
			isCancel = false;
			new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(20000);
					} catch (InterruptedException e) {
					}
					if (connecting) {
						try {
							sc.close();
						} catch (Exception e) {
						}
						isCancel = true;
						connecting = false;
						connected = false;
						messageHandler.onConnectionFail();
					}
				}
			}).start();
			connecting = true;
			Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
			
			connected = true;
			try {
				doConnect(host,port);
				messageHandler.onConnectOK();
			} catch (Exception ex) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				if (isCancel)
					return;
				if (messageHandler != null) {
					close();
					messageHandler.onConnectionFail();
				}
			}
		}

		public void doConnect(String host, int port) throws Exception {
			Cout.println(getClass(),host+ "     doconect "+port);
			sc = new mSocket(host, port);
		    sc.setKeepAlive(true);
		    dos = sc.getOutputStream();
		    dis = sc.getInputStream();
			new Thread(sender).start();
			collectorThread = new Thread(new MessageCollector());			
			collectorThread.start();
			timeConnected = mSystem.currentTimeMillis();
			//
			doSendMessage(new Message((byte) -27));
			connecting = false;			
			
		}
	}

	public void sendMessage(Message message) {
		// System.out.println("SEND MSG: " + message.command);
		sender.AddMessage(message);
	}

	 public static mVector recieveMsg = new mVector();

	    public static void onRecieveMsg(Message msg){		
	        if (Thread.currentThread().getName() == GameMidlet.mainThreadName)
	            messageHandler.onMessage(msg);
	        else
	           recieveMsg.addElement(msg);
	    }   
	    public static void onDisConnect(){		
	        if (Thread.currentThread().getName() == GameMidlet.mainThreadName)
				messageHandler.onDisconnected();
	        else{
	        	isDisConnect = true;
	        }
	    }   
	int countMsg = 0;
	
	private synchronized void doSendMessage(Message m) throws IOException {
//		 System.out.println("DO SEND MSG: " + m.command);
		byte[] data = m.getData();
		try {
			if (getKeyComplete) {
				byte b = (writeKey(m.command));
				dos.writeByte(b);
			} else
				dos.writeByte(m.command);
			
			if (data != null) {
				int size = data.length;
				if (getKeyComplete) {
					int byte1 = writeKey((byte) (size >> 8));
					dos.writeByte(byte1);
					int byte2 = writeKey((byte) (size & 0xFF));
					dos.writeByte(byte2);
				} else
					dos.writeShort(size);
				//
				if (getKeyComplete)
					for (int i = 0; i < data.length; i++)
						data[i] = writeKey(data[i]);
				dos.write(data);
				sendByteCount += (5 + data.length);
			} else {
				dos.writeShort(0);
				sendByteCount += 5;
			}
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static boolean isDisConnect;
	public static void update(){
//		if (Session_ME.getInstance ().timeOut != 0) {
//			if((mSystem.currentTimeMillis () / 1000 - Session_ME.getInstance().timeOut) > 10)
//			{
//				GlobalLogicHandler.isDisconnect=true;
//				Session_ME.getInstance ().timeOut = 0;
//				if(sc!=null)
//					cleanNetwork();
//				return;
//			}
//
//		}
		if(isDisConnect){
			isDisConnect = false;
		}
		
        while(recieveMsg.size() > 0){
            Message msg = (Message)recieveMsg.elementAt(0);
           
            if (msg == null)
            {
                recieveMsg.removeElementAt(0);
                return;
            }            
           
            messageHandler.onMessage(msg);		
			recieveMsg.removeElementAt(0);         
			

        }
		
    }
	private byte readKey(byte b) {
		byte i = (byte) ((key[curR++] & 0xff) ^ (b & 0xff));
		if (curR >= key.length)
			curR %= key.length;
		return i;
	}

	private byte writeKey(byte b) {
		byte i = (byte) ((key[curW++] & 0xff) ^ (b & 0xff));
		if (curW >= key.length)
			curW %= key.length;
		return i;
	}

	private class Sender implements Runnable {
		private final mVector sendingMessage;

		public Sender() {
			sendingMessage = new mVector();
		}

		public void AddMessage(Message message) {
			sendingMessage.addElement(message);
		}
		public void removeAllMessage() {
			if (sendingMessage != null) {
				sendingMessage.removeAllElements();
			}
		}
		public void run() {
			while (connected) {
				try {
					if (getKeyComplete)
						while (sendingMessage.size() > 0) {
							Message m = (Message) sendingMessage.elementAt(0);
							sendingMessage.removeElementAt(0);
							doSendMessage(m);
						}
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	class MessageCollector implements Runnable {
		public void run() {
			Message message;			
			try {
				while (isConnected()) {					
					message = readMessage();
					if (message != null) {
						try {							
							if (message.command == -27) {
								getKey(message);
							} else {
//								messageHandler.onMessage(message);
								messageHandler.onMessage(message);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}						
					} else {
						
						break;
					}
				}
			} catch (Exception ex) {
			}
			if (connected) {
				if (messageHandler != null) {
					if (mSystem.currentTimeMillis() - timeConnected > 500)
						onDisConnect();
					else
						messageHandler.onConnectionFail();
				}
				if (sc != null){
					cleanNetwork();
				}
			}
		}

		private void getKey(Message message) throws IOException {
			byte keySize = message.reader().readByte();
			key = new byte[keySize];
			for (int i = 0; i < keySize; i++) {
				key[i] = message.reader().readByte();
			}
			for (int i = 0; i < key.length - 1; i++)
				key[i + 1] ^= key[i];
			getKeyComplete = true;
		}

		private Message readMessage2(byte cmd) throws Exception {
			// read message command
			// read size of data
			int size;
			int b1 = readKey(dis.readByte()) + 128;
			int b2 = readKey(dis.readByte()) + 128;
			int b3 = readKey(dis.readByte()) + 128;
			size = (b3 * 256 + b2) * 256 + b1;

			byte data[] = new byte[size];
			int len = 0;
			int byteRead = 0;
			while (len != -1 && byteRead < size) {
				len = dis.read(data, byteRead, size - byteRead);
				if (len > 0) {
					byteRead += len;
					recvByteCount += (5 + byteRead);
					int Kb = (Session_ME.gI().recvByteCount + Session_ME.gI().sendByteCount);
					strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
				}
			}
			if (getKeyComplete)
				for (int i = 0; i < data.length; i++) {
					data[i] = readKey(data[i]);
				}
			Message msg = new Message(cmd, data);
			return msg;
		}

		private Message readMessage() throws Exception {
			// read message command			
			byte cmd = dis.readByte();			
			if (getKeyComplete)
				cmd = readKey(cmd);
			// Res.out("................cmd= " + cmd);
//			if (cmd == -32 || cmd == -66 || cmd == 11 || cmd == -67 || cmd==-74) {
//				return readMessage2(cmd);
//			}
			// read size of data
			int size;
			if (getKeyComplete) {
				if(cmd == Cmd.LOGIN || cmd == Cmd.REQUEST_IMAGE){
					 byte b1 = readKey(dis.readByte());
						byte b2 = readKey(dis.readByte());
						byte b3 = readKey(dis.readByte());
						byte b4 = readKey(dis.readByte());
						size = ((b1 & 0xff) << 24) | ((b2 & 0xff) << 16)  | ((b3 & 0xff) << 8)  | (b4 & 0xff);
					
				}else{ // khac -27
					byte b1 = dis.readByte();
					byte b2 = dis.readByte();
					size = (readKey(b1) & 0xff) << 8 | readKey(b2) & 0xff;	
					
				}
			} else
				size = dis.readUnsignedShort();
			byte data[] = new byte[size];
			int len = 0;
			int byteRead = 0;
			while (len != -1 && byteRead < size) {
				len = dis.read(data, byteRead, size - byteRead);
				if (len > 0) {
					byteRead += len;
					recvByteCount += (5 + byteRead);
					int Kb = (Session_ME.gI().recvByteCount + Session_ME.gI().sendByteCount);
					strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
				}
			}
			if (getKeyComplete)
				for (int i = 0; i < data.length; i++) {
					data[i] = readKey(data[i]);
				}
			
			Message msg = new Message(cmd, data);
			return msg;
		}
	}

	public void close() {
		cleanNetwork();
	}

	private void cleanNetwork() {
		key = null;
		curR = 0;
		curW = 0;
		try {
			connected = false;
			connecting = false;
			if (sc != null) {
				sc.close();
				sc = null;
			}
			if (dos != null) {
				dos.close();
				dos = null;
			}
			if (dis != null) {
				dis.close();
				dis = null;
			}
			sendThread = null;
			collectorThread = null;
			// if (initThread != null && initThread.isAlive()) {
			// initThread.interrupt();
			// initThread = null;
			// }

//			mSystem.gcc();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connect(String host) {
		// TODO Auto-generated method stub
		
	}
}
