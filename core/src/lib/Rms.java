package lib;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;


public class Rms {
	

public static final String path = "rms/";
	
	public static void saveRMS(String filename, byte[] data) {
		saveRMSData(filename,data);
	}

	public static void saveIP(String strID) {
		saveRMS("NJIP", strID.getBytes());
	}

	public static String loadIP() {
		byte[] data = loadRMS("NJIP");
		if (data == null)
			return null;
		else
			return new String(data);
	}

	public static byte[] loadRMS(String filename) {
		return loadRMSData(filename);
	}

	public static final byte[] loadRMSData(String name) {

		byte[] data=null;
		try {
			FileHandle file = Gdx.files.local(path+name);			
			data = file.readBytes();
			
		} catch (Exception e) {
			// TODO: handle exception
//			 Cout.println("ERROR loadRMS RMS !!!!!!!!! "+"__ "+name);
		}
		return data;

	}

	public static void saveRMSInt(String file, int x) {
		try {
			saveRMS(file, new byte[] { (byte) x });
		} catch (Exception e) {
		}
	}

	public static void saveRMSString(String filename, String s) {
		try {
			saveRMS(filename, s.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String loadRMSString(String filename) {
		byte[] data = loadRMS(filename);
		if (data == null)
			return null;
		else
			try {
				String s = new String(data, "UTF-8");
				return s;
			} catch (Exception e) {
				return new String(data);
			}
	}

	public static final void saveRMSData(String name, byte[] data) {

		try {

			FileHandle file = Gdx.files.local(path+name);
			file.writeBytes(data, false);
		} catch (Exception ex) {
//			System.out.println("save rms '" + name + "' error!");
		}

	}

	public static int loadRMSInt(String file) {
		byte[] b = loadRMS(file);
		return b == null ? -1 : b[0];
	}
	
	
	public static void deleteRecord(String fileName) {
		//GameCanvas.gCanvas.deleteFile(fileName);
	}
	
	public static void clearRMS() {
		deleteRecord("data");
		deleteRecord("dataVersion");
		deleteRecord("map");
		deleteRecord("mapVersion");
		deleteRecord("skill");
		deleteRecord("killVersion");
		deleteRecord("item");
		deleteRecord("itemVersion");

	}

}
