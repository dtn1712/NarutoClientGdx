package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import Objectgame.Char;
import lib.Command;
import lib.FontManager;
import lib.Image;
import lib.LoadImageInterface;
import model.IActionListener;
import model.Paint;
import model.Screen;
import model.Scroll;

public class FlagScreen extends Screen implements IActionListener{

	private static FlagScreen instance;
	public static long time;
	private Command cmdClose;
	public int coutFc, lastSelect;

	public int day, hour, minute, second;

	public byte[] listKhu = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1};
	public int minKhu = 15;
	public String sday = "";
	public Scroll srclist = new Scroll();
	public long timeRemove = 180000; // mili giấy server trả về là s;
	public int wKhung = 170, hKhung = 150;
	public int xpaint, ypaint;

	public FlagScreen()
	{
		xpaint = GameCanvas.w / 2 - wKhung / 2;
		ypaint = GameCanvas.h / 2 - hKhung / 2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint + wKhung - LoadImageInterface.closeTab.width / 2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}

	//@Override
	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case 2:
				GameScreen.getInstance().switchToMe();
				break;
		}
	}

	public static FlagScreen getInstance()
	{
		if (instance == null) {
			instance = new FlagScreen();
		}
		return instance;
	}

	public void updateKey()
	{
		if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdClose))
			if (cmdClose != null)
			{
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				keyTouch = -1;
				if (cmdClose != null) {
					cmdClose.performAction();
				}
			}

		srclist.updateKey();
		srclist.updatecm();
		if (GameCanvas.isPointerJustRelease && srclist.selectedItem != -1 && srclist.selectedItem < listKhu.length)
			if (listKhu[srclist.selectedItem] != Char.myChar().typePk)
			{
				if (lastSelect == srclist.selectedItem)
				{
					GameScreen.getInstance().switchToMe();
				}
				lastSelect = srclist.selectedItem;
				GameCanvas.isPointerJustRelease = false;
			}

		super.updateKey();
	}

	public void update()
	{
		super.update();
		GameScreen.getInstance().update();
		if (GameCanvas.gameTick % 4 == 0)
		{
			coutFc++;
			if (coutFc > 2)
				coutFc = 0;
		}
		if (Char.myChar().typePk > -1)
		{
			long currtime = System.currentTimeMillis();
			if (currtime - time >= timeRemove)
			{
				sday = "";
			}
			else
			{
				if ((timeRemove - (currtime - time)) / 1000 <= 60)
				{
					second = (int) (timeRemove - (currtime - time)) / 1000;
					day = minute = hour = 0;
				}
				else if ((timeRemove - (currtime - time)) / 1000 <= 3600)
				{
					second = (int) (timeRemove - (currtime - time)) / 1000;
					minute = second / 60;
					second = second % 60;
					second = hour = day = 0;
				}
				else if ((timeRemove - (currtime - time)) / 1000 <= 86400)
				{
					second = (int) (timeRemove - (currtime - time)) / 1000;
					hour = second / 3600;
					second = minute = day = 0;
				}
				else
				{
					second = (int) (timeRemove - (currtime - time)) / 1000;
					day = second / 86400;
					second = minute = hour = 0;
				}
			}

			sday = (day <= 0 ? "" : (day < 10 ? "0" + day : day + "") + "d") +
					(hour <= 0 ? "" : (hour < 10 ? "0" + hour : hour + "") + "h") +
					(minute <= 0 ? "" : (minute < 10 ? "0" + minute : minute + "") + "'") +
					(second <= 0 ? "" : (second < 10 ? "0" + second : second + "") + "s") + "";
		}
		else
		{
			sday = "";
		}
	}

	//@Override
	public void paint(MGraphics g)
	{
		super.paint(g);
		GameScreen.getInstance().paint(g);
		g.setColor(0x000000, GameCanvas.opacityTab);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		g.disableBlending();
		Paint.paintFrameNaruto(xpaint, ypaint, wKhung, hKhung + 2, g);
		Paint.PaintBoxName("Cờ", xpaint + wKhung / 2 - 40, ypaint, 80, g);
		cmdClose.paint(g);
		if (listKhu != null)
		{
			srclist.setStyle((listKhu.length > minKhu ? listKhu.length : minKhu) / 5,
					Image.getWidth(LoadImageInterface.ImgItem),
					xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
					wKhung - 30, hKhung - 32, true, 5);
			srclist.setClip(g, xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
					wKhung - 30, hKhung - 32);
			for (int i = 0; i < (listKhu.length > minKhu ? listKhu.length : minKhu) / 5; i++)
				for (int j = 0; j < 5; j++)
				{
					g.drawImage(LoadImageInterface.ImgItem,
							xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j,
							ypaint + 30 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2, 0);
					if (i * 5 + j < listKhu.length)
						if (listKhu[i * 5 + j] > -1)
						{
							g.drawRegion(LoadImageInterface.iconpk, 0,
									12 * (listKhu[i * 5 + j] * 3 + GameCanvas.gameTick / 3 % 3), 12, 12, 0,
									xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									ypaint + 30 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									MGraphics.VCENTER | MGraphics.HCENTER);
							if (listKhu[i * 5 + j] == Char.myChar().typePk)
								FontManager.getInstance().tahoma_7_white.drawString(g, sday,
										xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
												Image.getWidth(LoadImageInterface.ImgItem) / 2,
										ypaint + 32 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
												Image.getWidth(LoadImageInterface.ImgItem) / 2, 2);
						}
						else
						{
							FontManager.getInstance().tahoma_7_white.drawString(g, "Tháo",
									xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
											Image.getWidth(LoadImageInterface.ImgItem) / 2, 2);
						}
				}
			if (srclist.selectedItem >= 0 && srclist.selectedItem < listKhu.length)
				Paint.paintFocus(g,
						xpaint + 15 + srclist.selectedItem % 5 * Image.getWidth(LoadImageInterface.ImgItem) + 11 -
								LoadImageInterface.ImgItem.getWidth() / 4,
						ypaint + 30 + srclist.selectedItem / 5 * Image.getHeight(LoadImageInterface.ImgItem) + 13 -
								LoadImageInterface.ImgItem.getWidth() / 4
						, LoadImageInterface.ImgItem.getWidth() - 9, LoadImageInterface.ImgItem.getWidth() - 9, coutFc);
			GameCanvas.resetTrans(g);
		}
	}
}
