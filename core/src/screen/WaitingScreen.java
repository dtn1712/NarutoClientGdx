package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.Mob;
import Objectgame.TileMap;
import lib.FontManager;
import lib.LoadImageInterface;
import model.Screen;
import model.SmallImage;

public class WaitingScreen extends Screen{

	private static WaitingScreen instance = new WaitingScreen();

	public boolean isChangeKhu;

	private boolean isRemoveImg;
	private int timeSleepLoadTile;
	private long timeStartOff;
	private StringBuilder text = new StringBuilder();
	private String display;

	public void switchToMe()
	{
		timeStartOff = System.currentTimeMillis();
		timeSleepLoadTile = 20;
		isRemoveImg = true;
		super.switchToMe();
	}

	public static WaitingScreen getInstance()
	{
		return instance;
	}

	public void update()
	{
		if (isRemoveImg)
		{
			if (!isChangeKhu)
			{
				Mob.cleanImg();
				BgItem.cleanImg();
			}
			isChangeKhu = false;
			SmallImage.cleanImg();
//			System.gc();
			isRemoveImg = false;
		}
		if (TileMap.imgMaptile == null)
		{
			timeSleepLoadTile++;
			if (timeSleepLoadTile > 20)
			{
				timeSleepLoadTile = 0;
				TileMap.getInstance().loadImgTile(TileMap.tileID);
			}
		}
		if ((System.currentTimeMillis() - timeStartOff) / 1000 > 3 && GameCanvas.getInstance().gameScreen != null)
		{
			Char.myChar().timeLastchangeMap = System.currentTimeMillis();
			GameCanvas.getInstance().gameScreen.switchToMe();
			GameCanvas.getInstance().cleanImgBg();
		}


		int nCham = GameCanvas.gameTick / 10 % 4;
		for (int i = 0; i < nCham; i++)
		{
			text.append(".");
		}
		for (int i = 0; i < 3 - nCham; i++)
		{
			text.append(" ");
		}

		display = TileMap.getInstance().mapName + text.toString();

		super.update();
	}

	public void paint(MGraphics g)
	{
		g.setColor(0x000000);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

		g.drawImage(LoadImageInterface.imgMap, GameCanvas.w / 2, GameCanvas.h / 2,
				MGraphics.VCENTER | MGraphics.HCENTER);

		if (display != null) {
			int textW = FontManager.getInstance().tahoma_7.getWidth(display);
			int posX = GameCanvas.w - ((GameCanvas.w - LoadImageInterface.imgMap.getWidth()) / 2) - (LoadImageInterface.imgMap.getWidth() / 2) - textW / 2;
			int posY = GameCanvas.h / 2;

			FontManager.getInstance().tahoma_7.drawString(g, display, posX, posY, MGraphics.VCENTER | MGraphics.HCENTER);
			g.drawRegion(LoadImageInterface.imgXinCho, 0, 16 * (GameCanvas.gameTick / 2 % 3), 16, 16, 0,
					GameCanvas.w / 2, 3 * GameCanvas.h / 4 + 20, MGraphics.VCENTER | MGraphics.HCENTER);

			text.delete(0, text.length());
		}

		super.paint(g);
	}

}
