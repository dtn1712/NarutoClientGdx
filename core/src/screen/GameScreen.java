package screen;


import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.MGraphics;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;

import Gui.Constants;
import Gui.FatherChat;
import Gui.GuiChatClanWorld;
import Gui.GuiContact;
import Gui.GuiMain;
import Gui.IconChat;
import Gui.InfoCharEquipment;
import Gui.MenuIcon;
import Gui.QShortQuest;
import Gui.TabChat;
import Gui.TradeGui;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.FallingLeafEffect;
import Objectgame.ItemMap;
import Objectgame.MenuObject;
import Objectgame.Mob;
import Objectgame.NodeChat;
import Objectgame.Npc;
import Objectgame.Skill;
import Objectgame.SkillInfoPaint;
import Objectgame.SkillPaint;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.TileMap;
import Objectgame.minigame.MiniGameManager;
import Objectgame.quest.MainQuestManager;
import Objectgame.quest.QuestTemplate;
import lib.Command;
import lib.FontManager;
import lib.Image;
import lib.LoadImageInterface;
import lib.MBitmap;
import lib.MFont;
import lib.Music;
import lib.Rms;
import lib.TField;
import lib.Utils;
import lib.mHashtable;
import model.CRes;
import model.ChatTextField;
import model.Effect;
import model.EffectCharPaint;
import model.EffectInfoPaint;
import model.EffectKill;
import model.IActionListener;
import model.IChatable;
import model.InfoDlg;
import model.InfoServer;
import model.Key;
import model.MResources;
import model.MainEffect;
import model.Part;
import model.PartImage;
import model.Party;
import model.QuickSlot;
import model.Screen;
import model.Scroll;
import model.SmallImage;
import model.Waypoint;
import real.Service;


public class GameScreen extends Screen implements IActionListener, IChatable {

    public InfoCharEquipment infoCharEquipment;
    public static long delayPressAtt = 500, lastpress;
    public Vector<EffectKill> veffClient = new Vector<EffectKill>();
//    public static GameScreen instance;

    // FLYING TEXT
    private static String[] flyTextString;

    public static int[] flyTextX, flyTextY, flyTextDx, flyTextDy, flyTextState, flyTextColor;

    public static int gW, gH, gW2, gssw, gssh, gH23, gW23, gH2, csPadMaxH, cmdBarH, gW6;

    // camera
    public static int gssx, gssy, gssxe, gssye;

    public static int cmx, cmy, cmdx, cmdy, cmvx, cmvy, cmtoX, cmtoY, cmxLim, cmyLim;

    public static SkillPaint[] sks;
    public static Part[] parts;
    public static EffectCharPaint[] efs;
    private static Scroll scrMain = new Scroll();

    public Vector<Char> vCharInMap = new Vector<Char>();
    public Vector<ItemMap> vItemMap = new Vector<ItemMap>();
    public Vector<ItemMap> vNhatItemMap = new Vector<ItemMap>();
    public Vector<Mob> vMob = new Vector<Mob>();
    public Vector<Npc> vNpc = new Vector<Npc>();

    public Vector<InfoServer> listInfoServer = new Vector<InfoServer>();
    public Vector<InfoServer> listWorld = new Vector<InfoServer>();

    public static mHashtable hParty = new mHashtable();
    public static int indexSize = 28,
            indexRow = -1;

    public static boolean isMessageMenu;

    public static boolean isAutoDanh, isAutoNhatItem;

    public static Char currentCharViewInfo;

//        public static byte vcData, vcMap, vcSkill, vcItem;

    public static MBitmap imgMenu,
            imgSkill,
            imgFocusActor;

    public static MBitmap imgQuest;

    private static MBitmap[] imgCloudy = new MBitmap[3];
    public static int wcloudy1, ncloudy1, wcloudy0, ncloudy0, kcwcloudy0, wcloudy2, ncloudy2;
    public static int[] xpaintcloudy1, xpaintcloudy0, ypaintcloudy1, ypaintcloudy0, xpaintcloudy2, ypaintcloudy2;


    //chat friend
    public static Command chat;

    public static Command bntIconChat;
    public static boolean ispaintChat = false;

    public static boolean isBag;

    // trade command
    public static Command cmdAcceptTrade;

    // quest main
    private static TradeGui tradeGui; //test thu

    private static Skill[] onScreenSkill = {null, null, null, null, null};

    private static int yTouchBar;
    private static int[] xS;
    private static int[] yS;
    private static int xSkill, ySkill, padSkill;


    public static int cmdBarY,
            hpBarX,
            hpBarY,
            hpBarW;

    public static int popupY, popupX;


    public static int widthGui = 237, heightGui = 232, xGui, yGui;

    public static int xstart, ystart, popupW = 140, popupH = 160;

    private static TField tfCharFriend;

    public int auto;

    private byte currentIdSkill = 1;
    public Command cmdAcceptParty, cmdComfirmFriend;


    //chat clan,world
    public GuiChatClanWorld guiChatClanWorld = new GuiChatClanWorld(-FatherChat.popw + 5,
            GameCanvas.h - FatherChat.poph + 7, new String[]{"World", "Clan"});

    //gui Contact
    private GuiContact guiContact = new GuiContact(GameCanvas.w / 2, GameCanvas.h / 2);

    //main guis
    public GuiMain guiMain;

    public int cLastFocusID = -1;

    public int indexKeyTouchAuto, timeDow, xStartAuto, yStartAuto, rangeAuto = 240;

    private Command menu;


    public int popx, popy, popw, poph;

    //quest screen
    private QShortQuest qShortQuest = new QShortQuest();
    private TField tfText = null;

    public GameScreen() {
        if (GameCanvas.w == 128 || GameCanvas.h <= 208) {
            indexSize = 20;
        }

        init();
        initCanvas();
        initTField();
        initCommand();
    }


    public void perform(int idAction, Object p) {
        switch (idAction) {
            case 10:
                MenuObject menu3 = (MenuObject) p;
                Npc npc = null;
                for (int i = 0; i < vNpc.size(); i++) {
                    Npc dem = vNpc.elementAt(i);
                    if (dem != null && menu3 != null && dem.npcId == menu3.idActor) {
                        npc = dem;
                    }
                }
                if (npc != null) npc.HandleQuest();
                break;
            case 9999:
                GameCanvas.getInstance().resetToLoginScreen(true);
                break;
        }
    }

    public void onCancelChat() {
    }

    //chat world
    public void onChatFromMe(String text, String to) {
        if (GameCanvas.isTouch)
            ChatTextField.getInstance().isShow = false;
        if (text.equals(""))
            return;
        if (to.equals(MResources.PUBLICCHAT[0]))
            Service.getInstance().ChatMap(text);

        else if (to.equals(MResources.GLOBALCHAT[0]))
            Service.getInstance().ChatGlobal(text);
    }


    public static void loadBegin() {
        flyTextX = new int[5];
        flyTextY = new int[5];
        flyTextDx = new int[5];
        flyTextDy = new int[5];
        flyTextState = new int[5];
        flyTextString = new String[5];
        flyTextColor = new int[8];
        for (int i = 0; i < 5; i++) {
            flyTextState[i] = -1;
        }
    }


    public void switchToMe() {
        super.switchToMe();
        isBag = false;
    }

    public static void readPart() {
        DataInputStream file = null;
        try {
            file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_part")));
            int sum = file.readShort();
            parts = new Part[sum];
            for (int i = 0; i < sum; i++) {
                int type = file.readByte();
                parts[i] = new Part(type);
                for (int j = 0; j < parts[i].pi.length; j++) {
                    parts[i].pi[j] = new PartImage();
                    parts[i].pi[j].id = file.readShort();
                    parts[i].pi[j].dx = file.readByte();
                    parts[i].pi[j].dy = file.readByte();
                }
            }
        } catch (Exception e) {
            Log.e(GameScreen.class.getName(), "Exception", e);
        } finally {
            try {
                if (file != null) file.close();
            } catch (IOException e) {
                Log.e(GameScreen.class.getName(), "Exception", e);
            }
        }
    }

    public static void readEffect() {
        DataInputStream file = null;
        try {
            file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_effect")));
            int sum = file.readShort();
            efs = new EffectCharPaint[sum];
            for (int i = 0; i < sum; i++) {
                efs[i] = new EffectCharPaint();
                efs[i].idEf = file.readShort();
                efs[i].arrEfInfo = new EffectInfoPaint[file.readByte()];
                for (int j = 0; j < efs[i].arrEfInfo.length; j++) {
                    efs[i].arrEfInfo[j] = new EffectInfoPaint();
                    efs[i].arrEfInfo[j].idImg = file.readShort();
                    efs[i].arrEfInfo[j].dx = file.readByte();
                    efs[i].arrEfInfo[j].dy = file.readByte();
                }
            }
        } catch (Exception e) {
            Log.e(GameScreen.class.getName(), "Exception", e);
        } finally {
            try {
                if (file != null) file.close();
            } catch (IOException e) {
                Log.e(GameScreen.class.getName(), "Exception", e);
            }
        }
    }


    public static void readSkill() {
        DataInputStream file = null;
        try {
            file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_skill")));
            int sum = file.readShort();

            sks = new SkillPaint[sum + 1];
            for (int i = 1; i < sum; i++) {
                sks[i] = new SkillPaint();
                sks[i].id = file.readShort();
                sks[i].effId = file.readShort();
                sks[i].numEff = file.readByte();
                sks[i].skillStand = new SkillInfoPaint[file.readByte()];

                for (int j = 0; j < sks[i].skillStand.length; j++) {
                    sks[i].skillStand[j] = new SkillInfoPaint();
                    sks[i].skillStand[j].status = file.readByte();
                    sks[i].skillStand[j].effS0Id = file.readShort();
                    sks[i].skillStand[j].e0dx = file.readShort();
                    sks[i].skillStand[j].e0dy = file.readShort();

                    sks[i].skillStand[j].effS1Id = file.readShort();
                    sks[i].skillStand[j].e1dx = file.readShort();
                    sks[i].skillStand[j].e1dy = file.readShort();

                    sks[i].skillStand[j].effS2Id = file.readShort();
                    sks[i].skillStand[j].e2dx = file.readShort();
                    sks[i].skillStand[j].e2dy = file.readShort();

                    sks[i].skillStand[j].arrowId = file.readShort();
                    sks[i].skillStand[j].adx = file.readShort();
                    sks[i].skillStand[j].ady = file.readShort();
                }

                sks[i].skillfly = new SkillInfoPaint[file.readByte()];
                for (int j = 0; j < sks[i].skillfly.length; j++) {
                    sks[i].skillfly[j] = new SkillInfoPaint();
                    sks[i].skillfly[j].status = file.readByte();
                    sks[i].skillfly[j].effS0Id = file.readShort();
                    sks[i].skillfly[j].e0dx = file.readShort();
                    sks[i].skillfly[j].e0dy = file.readShort();

                    sks[i].skillfly[j].effS1Id = file.readShort();
                    sks[i].skillfly[j].e1dx = file.readShort();
                    sks[i].skillfly[j].e1dy = file.readShort();

                    sks[i].skillfly[j].effS2Id = file.readShort();
                    sks[i].skillfly[j].e2dx = file.readShort();
                    sks[i].skillfly[j].e2dy = file.readShort();

                    sks[i].skillfly[j].arrowId = file.readShort();
                    sks[i].skillfly[j].adx = file.readShort();
                    sks[i].skillfly[j].ady = file.readShort();
                }
            }
        } catch (Exception e) {
            Log.e(GameScreen.class.getName(), "Exception", e);
        } finally {
            try {
                if (file != null) file.close();
            } catch (IOException e) {
                Log.e(GameScreen.class.getName(), "Exception", e);
            }
        }
    }


    public static GameScreen getInstance() {
        if (GameCanvas.getInstance().gameScreen == null) {
            GameCanvas.getInstance().gameScreen = new GameScreen();
        }
        return GameCanvas.getInstance().gameScreen;
    }

    private void initCanvas() {
        popy = 50;
        popw = 100;
        poph = GameCanvas.h - 50 - 40;
        widthGui = 240;

        xGui = GameCanvas.w / 2 - widthGui / 2;

        popx = xGui - popw;
        popx = popx < 0 ? 0 : popx;
        yGui = 50;
        NodeChat.wnode = widthGui - 10;
        heightGui = GameCanvas.h - 50 - 40;
    }

    public void loadData() {
        Service.getInstance().requestFriendList(Char.myChar().charID);
        Service.getInstance().requestMenuShop();
        Service.getInstance().requestShopInfo((byte) 0);
    }

    private void init() {
        GuiMain.loadCmdBar();
        guiMain = new GuiMain();
    }

    private void initCommand() {
        bntIconChat = new Command("", Constants.BUTTON_ICON_CHAT);
        bntIconChat.setPos(xGui + 5 + tfCharFriend.width + 5, tfCharFriend.y, LoadImageInterface.imgEmo[7],
                LoadImageInterface.imgEmo[7]);
        bntIconChat.w = 40;
        bntIconChat.h = 20;
        //create button sen chat
        chat = new Command("Gửi", Constants.BUTTON_SEND);
        chat.setPos(bntIconChat.x + bntIconChat.w + 5, tfCharFriend.y, imgSkill, imgSkill);

        chat.w = 60;
        chat.h = 20;

        menu = new Command(MResources.MENU, 11000);
        cmdAcceptParty = new Command("Đồng ý", 111037);
        cmdComfirmFriend = new Command("Đồng ý", 111038);
        cmdAcceptTrade = new Command("Đồng ý", 111039);


        // ----- INIT COMMAND FOR TOUCHSCREEN
        if (!GameCanvas.isTouch || !GameCanvas.isTouchControl) return;
        menu.x = gW - 135;
        menu.y = 6;
        menu.img = imgMenu;

        if (!GameCanvas.isTouchControlSmallScreen) return;
        menu.x = gW / 2 - 38;
        menu.y = gH - 34;

    }

    public static void loadCamera(int cx, int cy) {
        gW = GameCanvas.w;
        gH = GameCanvas.h;
        cmdBarH = 39;

        csPadMaxH = GameCanvas.h / 6;
        if (csPadMaxH < 48)
            csPadMaxH = 48;

        gW2 = gW >> 1;
        gH2 = gH >> 1;
        gW23 = gH - 120;
        gH23 = gH * 2 / 3;
        gW6 = gW / 6;

        gssw = gW / TileMap.TILE_MAP_SIZE + 2;
        gssh = gH / TileMap.TILE_MAP_SIZE + 2;
        if (gW % 24 != 0)
            gssw += 1;
        cmxLim = TileMap.tmw * TileMap.TILE_MAP_SIZE - gW;
        cmyLim = TileMap.tmh * TileMap.TILE_MAP_SIZE - gH;
        if (cx == -1 && cy == -1) {
            cmx = cmtoX = Char.myChar().cx - gW2 + gW6
                    * Char.myChar().cdir;
            cmy = cmtoY = Char.myChar().cy - gH23;
        } else {
            cmx = cmtoX = Char.myChar().cx - gW23 + gW6 * Char.myChar().cdir;
            cmy = cmtoY = Char.myChar().cy - gH23;
        }
        if (cmx < 24)
            cmx = cmtoX = 24;
        if (cmx > cmxLim)
            cmx = cmtoX = cmxLim;
        if (cmy < 24)
            cmy = cmtoY = 24;
        if (cmy > cmyLim)
            cmy = cmtoY = cmyLim;

            gssx = cmx / TileMap.TILE_MAP_SIZE - 1;
            if (gssx < 0)
                gssx = 0;
            gssy = cmy / TileMap.TILE_MAP_SIZE;
            gssxe = gssx + gssw;
            gssye = gssy + gssh;
            if (gssy < 0)
                gssy = 0;
            if (gssye > TileMap.tmh - 1)
                gssye = TileMap.tmh - 1;

            TileMap.countx = (gssxe - gssx) * 4;
            if (TileMap.countx > TileMap.tmw)
                TileMap.countx = TileMap.tmw;
            TileMap.county = (gssye - gssy) * 4;
            if (TileMap.county > TileMap.tmh)
                TileMap.county = TileMap.tmh;

            TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.TILE_MAP_SIZE;
            if (TileMap.gssx < 0)
                TileMap.gssx = 0;
            TileMap.gssxe = TileMap.gssx + TileMap.countx;
            if (TileMap.gssxe > TileMap.tmw)
                TileMap.gssxe = TileMap.tmw;

            TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.TILE_MAP_SIZE;
            if (TileMap.gssy < 0)
                TileMap.gssy = 0;

            TileMap.gssye = TileMap.gssy + TileMap.county;

            if (TileMap.gssye > TileMap.tmh)
                TileMap.gssye = TileMap.tmh;


//        ChatTextField.getInstance().parentScreen = instance;

//        ChatTextField.getInstance().parentScreen = this;
        ChatTextField.getInstance().tfChat.y = GameCanvas.h - 35 - ChatTextField.getInstance().tfChat.height;

        if (GameCanvas.isTouch) {
            // INIT TOUCH CONTROL POSITION
            yTouchBar = gH - 88;
            int xtrungtam = 3 * Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;

            GuiMain.xCenter = xtrungtam;
            GuiMain.yCenter = yTouchBar + 60;

            GuiMain.xL = 3 * Image.getWidth(LoadImageInterface.imgMoveNormal) / 2 -
                    Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;
            GuiMain.yL = GuiMain.yCenter; // left
            GuiMain.xR = xtrungtam + Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;
            GuiMain.yR = GuiMain.yCenter; // right

            GuiMain.xU = xtrungtam;
            GuiMain.yU = GuiMain.yCenter - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2; // up

            GuiMain.gamePad = new GamePad();
        }


        xS = new int[onScreenSkill.length];
        yS = new int[onScreenSkill.length];
        if (GameCanvas.isTouch) {
            if (GameCanvas.isTouchControlSmallScreen) {
                xSkill = 2;
                ySkill = 55;
                padSkill = 5;
                for (int i = 0; i < xS.length; i++) {
                    xS[i] = i * (25 + padSkill);
                    yS[i] = ySkill;
                }
            } else {
                if (GameCanvas.w <= 320)
                    xSkill = gW2 - onScreenSkill.length * 25 / 2 - 15;
                else
                    xSkill = gW2 - onScreenSkill.length * 25 / 2;
                ySkill = yTouchBar + 58;
                padSkill = 5;

                for (int i = 0; i < xS.length; i++) {
                    xS[i] = i * (25 + padSkill);
                    yS[i] = ySkill;
                }
            }
        } else {
            xSkill = 0;
            for (int i = 0; i < yS.length; i++) {
                xS[i] = 2;
                yS[i] = 2 + 25 * i;
            }
        }
    }

    private static void UpdateCamera() {
        if (cmx != cmtoX || cmy != cmtoY) {
            cmvx = (cmtoX - cmx) << 2;
            cmvy = (cmtoY - cmy) << 2;

            cmdx += cmvx;
            cmx += cmdx >> 4;
            cmdx = cmdx & 0xf;

            cmdy += cmvy;
            cmy += cmdy >> 4;
            cmdy = cmdy & 0xf;

            if (cmx < 24)
                cmx = 24;
            if (cmx > cmxLim)
                cmx = cmxLim;
            if (cmy < 0)
                cmy = 0;
            if (cmy > cmyLim)
                cmy = cmyLim;
        }

        gssx = cmx / TileMap.TILE_MAP_SIZE - 1;
        if (gssx < 0)
            gssx = 0;
        gssy = cmy / TileMap.TILE_MAP_SIZE;
        gssxe = gssx + gssw;
        gssye = gssy + gssh;
        if (gssy < 0)
            gssy = 0;
        if (gssye > TileMap.tmh - 1)
            gssye = TileMap.tmh - 1;
        //
        TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.TILE_MAP_SIZE;
        if (TileMap.gssx < 0)
            TileMap.gssx = 0;
        TileMap.gssxe = TileMap.gssx + TileMap.countx;
        if (TileMap.gssxe > TileMap.tmw) {
            TileMap.gssxe = TileMap.tmw;
            TileMap.gssx = TileMap.gssxe - TileMap.countx;
        }

        TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.TILE_MAP_SIZE;
        if (TileMap.gssy < 0)
            TileMap.gssy = 0;

        TileMap.gssye = TileMap.gssy + TileMap.county;

        if (TileMap.gssye > TileMap.tmh) {
            TileMap.gssye = TileMap.tmh;
            TileMap.gssy = TileMap.gssye - TileMap.county;
        }
        scrMain.updatecm();
    }

    //execute event key press
    public void keyPress(int keyCode) {
        if (tfText != null && tfText.isFocus)
            tfText.keyPressed(keyCode);
        if (tfCharFriend != null && tfCharFriend.isFocus)
            tfCharFriend.keyPressed(keyCode);
        if (isBag)
            GameCanvas.allInfo.keyPress(keyCode);

        if (guiChatClanWorld != null) {
            guiChatClanWorld.KeyPress(keyCode);
        }
        guiMain.keyPress(keyCode);
        super.keyPress(keyCode);
    }

    public void updateKey() {
        if (infoCharEquipment != null) {
            infoCharEquipment.updatePointer();
            infoCharEquipment.updateKey();
            infoCharEquipment.updatePointer();
        }
        if (isAutoDanh && (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[6] || GameCanvas.keyPressed[4]))
            isAutoDanh = false;
        if (!guiChatClanWorld.moveClose && !GameCanvas.menu.showMenu)
            guiMain.UpdateKey();
        if (MenuIcon.isShowTab) return;

        guiChatClanWorld.updatePointer();

        guiChatClanWorld.UpdateKey();

        boolean isUpdatePhim = false;

        if (qShortQuest != null) {
            qShortQuest.UpdateKey();
        }


        if (!MenuIcon.isShowTab && (getCmdPointerLast(guiMain.bntAttack) || GameCanvas.keyPressed[5])) {
            GameCanvas.keyPressed[5] = false;

            DoTouchQuickSlot(0);
            isUpdatePhim = true;
        }
        if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_1)) {
            DoTouchQuickSlot(1);
            isUpdatePhim = true;
        }
        if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_2)) {
            DoTouchQuickSlot(2);
            isUpdatePhim = true;
        }
        if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_3)) {
            DoTouchQuickSlot(3);
            isUpdatePhim = true;
        }
        if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_4)) {
            DoTouchQuickSlot(4);
            isUpdatePhim = true;
        }
        if (ispaintChat) {
            if (getCmdPointerLast(chat))
                if (chat != null) {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    chat.performAction();
                }
            if (getCmdPointerLast(bntIconChat))
                if (bntIconChat != null) {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    bntIconChat.performAction();
                }
            if (Char.toCharChatSelected != null)
                Char.toCharChatSelected.update();
        }

        //chat private
        if (GameCanvas.menu.showMenu)
            return;
        if (InfoDlg.isLock)
            return;

        //chat private
        if (ChatTextField.getInstance().isShow && GameCanvas.keyAsciiPress != 0) {
            ChatTextField.getInstance().keyPressed(GameCanvas.keyAsciiPress);
            GameCanvas.keyAsciiPress = 0;
        }

        if (GameCanvas.menu.showMenu || Char.isLockKey)
            return;

        if (GameCanvas.keyPressed[10]) {
            GameCanvas.keyPressed[10] = false;
            GameCanvas.clearKeyPressed();
        }
        if (GameCanvas.keyPressed[11]) {
            GameCanvas.keyPressed[11] = false;
            GameCanvas.clearKeyPressed();
        }
        if (GameCanvas.keyAsciiPress != 0)
            if (TField.isQwerty)
                if (GameCanvas.keyAsciiPress == ' ') {
                    GameCanvas.keyAsciiPress = 0;
                    GameCanvas.clearKeyPressed();
                } else if (GameCanvas.keyAsciiPress == '@') {
                    GameCanvas.keyAsciiPress = 0;
                    GameCanvas.clearKeyPressed();
                } else if (GameCanvas.keyAsciiPress == '0') {
                    GameCanvas.keyAsciiPress = 0;
                    GameCanvas.clearKeyPressed();
                } else if (GameCanvas.keyAsciiPress == '?') {
                    GameCanvas.keyAsciiPress = 0;
                    GameCanvas.clearKeyPressed();
                }

        if (Char.myChar().skillPaint != null)
            return;

        if (Char.myChar().statusMe == Char.A_STAND) {
            if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack)) {
                GameCanvas.keyPressed[5] = false;
                isUpdatePhim = true;
                DoTouchQuickSlot(0);

            } else if (GameCanvas.keyHold[2]) {
                Char.myChar().updateCharJump();
                isUpdatePhim = true;
                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                    setCharJump(0);
                Char.myChar().updateCharJump();
                isAutoDanh = false;
            } else if (GameCanvas.keyHold[1]) {
                Char.myChar().cdir = -1;
                isUpdatePhim = true;
                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                    setCharJump(-4);
                isAutoDanh = false;
            } else if (GameCanvas.keyHold[3]) {
                Char.myChar().cdir = 1;
                isUpdatePhim = true;
                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                    setCharJump(4);
                isAutoDanh = false;
            } else if (GameCanvas.keyHold[4]) {
                isUpdatePhim = true;
                Char.myChar().updateCharRun();
                isAutoDanh = false;
                if (Char.myChar().cdir == 1) {
                    Char.myChar().cdir = -1;
                } else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                    Char.myChar().statusMe = Char.A_RUN;
                    Char.myChar().isStartSoundRun = true;
                    Music.play(Music.RUN, 12);
                    Char.myChar().cvx = -Char.myChar().getBaseSpeed();
                }
            } else if (GameCanvas.keyHold[6]) {
                isUpdatePhim = true;
                Char.myChar().updateCharRun();
                isAutoDanh = false;
                if (Char.myChar().cdir == -1) {
                    Char.myChar().cdir = 1;
                } else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                    Char.myChar().statusMe = Char.A_RUN;
                    Char.myChar().isStartSoundRun = true;
                    Music.play(Music.RUN, 12);
                    isAutoDanh = false;
                    isAutoDanh = false;
                    Char.myChar().cvx = Char.myChar().getBaseSpeed();
                }
            }
        } else if (Char.myChar().statusMe == Char.A_RUN) {
            if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5]) {
                GameCanvas.keyPressed[5] = false;
                isUpdatePhim = true;
                DoTouchQuickSlot(0);
            } else if (GameCanvas.keyHold[2]) {
                isUpdatePhim = true;
                Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                Char.myChar().statusMe = Char.A_JUMP;
                isAutoDanh = false;
                Char.myChar().cp1 = 0;

            } else if (GameCanvas.keyHold[1]) {
                Char.myChar().cdir = -1;
                Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                Char.myChar().cvx = -4;
                Char.myChar().statusMe = Char.A_JUMP;
                isAutoDanh = false;
                Char.myChar().cp1 = 0;
                isUpdatePhim = true;

            } else if (GameCanvas.keyHold[3]) {
                Char.myChar().cdir = 1;
                Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                Char.myChar().cvx = 4;
                Char.myChar().statusMe = Char.A_JUMP;
                isAutoDanh = false;
                Char.myChar().cp1 = 0;
                isUpdatePhim = true;

            } else if (GameCanvas.keyHold[4]) {
                if (Char.myChar().cdir == 1) {
                    Char.myChar().cdir = -1;
                } else {
                    Char.myChar().cvx = -Char.myChar().getBaseSpeed() - Char.myChar().getBonusSpeed();
                }
                isUpdatePhim = true;

            } else if (GameCanvas.keyHold[6]) {
                if (Char.myChar().cdir == -1) {
                    Char.myChar().cdir = 1;
                } else {
                    Char.myChar().cvx = Char.myChar().getBaseSpeed() + Char.myChar().getBonusSpeed();
                }
                isUpdatePhim = true;
            }
        } else if (Char.myChar().statusMe == Char.A_JUMP) {
            if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5]) {
                GameCanvas.keyPressed[5] = false;
                isUpdatePhim = true;
                DoTouchQuickSlot(0);
            }

            if (GameCanvas.keyHold[4] || GameCanvas.keyHold[1]) {
                if (Char.myChar().cdir == 1) {
                    Char.myChar().cdir = -1;
                } else {
                    Char.myChar().cvx = -Char.myChar().getBaseSpeed();
                }
                isAutoDanh = false;

            } else if (GameCanvas.keyHold[6] || GameCanvas.keyHold[3]) {
                if (Char.myChar().cdir == -1) {
                    Char.myChar().cdir = 1;
                } else {
                    Char.myChar().cvx = Char.myChar().getBaseSpeed();
                }
                isAutoDanh = false;
            }

            if (GameCanvas.keyHold[2] || GameCanvas.keyHold[1] || GameCanvas.keyHold[3]) {
                if (Char.myChar().canJumpHigh && Char.myChar().cp1 == 0 && Char.myChar().cvy > -4) {
                    Char.myChar().cp1++;
                    Char.myChar().cvy = -7;
                }

                isAutoDanh = false;
            }
        } else if (Char.myChar().statusMe == Char.A_FALL) {
            if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5]) {
                GameCanvas.keyPressed[5] = false;

                DoTouchQuickSlot(0);
            }
            if (GameCanvas.keyPressed[2]) {
                GameCanvas.clearKeyPressed();
            }

            if (GameCanvas.keyHold[4]) {
                if (Char.myChar().cdir == 1) {
                    Char.myChar().cdir = -1;
                } else {
                    Char.myChar().cvx = -Char.myChar().getBaseSpeed();
                }

            } else if (GameCanvas.keyHold[6]) {
                if (Char.myChar().cdir == -1) {
                    Char.myChar().cdir = 1;
                } else {
                    Char.myChar().cvx = Char.myChar().getBaseSpeed();
                }
            }
        } else if (Char.myChar().statusMe == Char.A_ATTK) {
            if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5]) {
                GameCanvas.keyPressed[5] = false;

                DoTouchQuickSlot(0);
            }
            if (GameCanvas.keyHold[4]) {
                if (Char.myChar().cdir == 1) {
                    Char.myChar().cdir = -1;
                } else {
                    Char.myChar().cvx = -Char.myChar().getBaseSpeed() + 2;
                }
                isAutoDanh = false;

            } else if (GameCanvas.keyHold[6]) {
                if (Char.myChar().cdir == -1) {
                    Char.myChar().cdir = 1;
                } else {
                    Char.myChar().cvx = Char.myChar().getBaseSpeed() - 2;
                }
                isAutoDanh = false;
            }
        }

        Service.getInstance().CharMove();

        if (GameCanvas.keyPressed[Key.NUM8] && GameCanvas.keyAsciiPress != '8') {
            GameCanvas.keyPressed[Key.NUM8] = false;
        }

        guiChatClanWorld.updatePointer();
        guiContact.UpdateKey();

        if (!isUpdatePhim && GameCanvas.isPointerJustRelease && infoCharEquipment == null) {
            for (int i = 0; i < vNpc.size(); i++) {
                Npc npc = vNpc.elementAt(i);
                if (npc != null && GameCanvas.isPointer(npc.cx - 20 - cmx, npc.cy - 60 - cmy, 40, 60)) {
                    Char.myChar().clearAllFocus();
                    Char.myChar().npcFocus = npc;
                    GameCanvas.isPointerJustRelease = false;
                    return;
                }
            }
            for (int i = 0; i < vCharInMap.size(); i++) {
                Char player = vCharInMap.elementAt(i);
                if (player == null || !GameCanvas.isPointer(player.cx - 20 - cmx, player.cy - 60 - cmy, 40, 60))
                    continue;

                if (player.charID == Char.myChar().charID &&
                        (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)) {
                    GameCanvas.startCommandDlg("Bạn muốn hồi sinh tại chỗ (1 gold)?",
                            new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                            new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
                    return;
                }

                if (player.charID != Char.myChar().charID) {
                    Char.myChar().clearAllFocus();
                    Char.myChar().charFocus = player;
                    Service.getInstance().RequestPlayerInfo(player.charID);
                    GameCanvas.isPointerJustRelease = false;
                    return;
                }
            }
            for (int i = 0; i < vMob.size(); i++) {
                Mob mob = vMob.elementAt(i);
                if (mob == null || !GameCanvas.isPointer(mob.x - mob.getW() / 2 - cmx, mob.y - mob.getH() - cmy,
                        mob.getW(), mob.getH())) continue;

                if (mob.mobName == null || mob.mobName.equals("")) {
                    Service.getInstance().RequestMonsterInfo(mob.mobId);
                }

                if (Char.myChar().mobFocus != null && mob.mobId == Char.myChar().mobFocus.mobId) {
                    DoTouchQuickSlot(0);
                    if (!isAutoDanh) {
                        xStartAuto = Char.myChar().cx;
                        yStartAuto = Char.myChar().cy;
                    }
//                    byte[] listAuto = Rms.loadRMS(Rms.autoSetting);
//                    if (listAuto != null && listAuto[SettingScreen.AUTO_DANH] == 1)
//                        isAutoDanh = true;

                    return;
                }
                if (mob.isBoss && !mob.isBossAppear) continue;
                Char.myChar().clearAllFocus();
                Char.myChar().mobFocus = mob;
                GameCanvas.isPointerJustRelease = false;
                if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY) {
                    return;
                }

                int maxkcX = CRes.abs(Char.myChar().cx - mob.x);
                int kc = 1, leng = 0;
                if (maxkcX > 20 || CRes.abs(Char.myChar().cy - mob.y) > 10) {
                    for (int j = 0; j < maxkcX; j++) {
                        leng += j;
                        if (leng >= maxkcX) {
                            kc = j - 1;
                            break;
                        }
                    }
                    if (Char.myChar().cx > mob.x) {
                        Char.myChar().updateCharRun();
                        if (Char.myChar().cdir == 1)
                            Char.myChar().cdir = -1;
                        if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                            Char.myChar().updateCharRun();
                            Char.myChar().statusMe = Char.A_RUN;

                            Char.myChar().cvx = -kc;
                        }
                    } else {
                        Char.myChar().updateCharRun();
                        if (Char.myChar().cdir == -1)
                            Char.myChar().cdir = 1;
                        if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                            Char.myChar().updateCharRun();
                            Char.myChar().statusMe = Char.A_RUN;
                            Char.myChar().cvx = kc;
                        }
                    }
                }
                return;
            }
            for (int i = 0; i < vItemMap.size(); i++) {
                ItemMap item = vItemMap.elementAt(i);
                if (item != null && GameCanvas.isPointer(item.x - 14 - cmx, item.y - 28 - cmy, 14, 28)) {
                    Char.myChar().clearAllFocus();
                    Char.myChar().itemFocus = item;
                    GameCanvas.isPointerJustRelease = false;
                    return;
                }
            }
        }
        GameCanvas.clearKeyPressed();
    }


    private void setCharJump(int cvx) {
        Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
        Char.myChar().cvx = cvx;
        Char.myChar().statusMe = Char.A_JUMP;
        Char.myChar().cp1 = 0;
    }


    public void update() {

        FallingLeafEffect.UpdateFallingLeaf();
        UpdateCloudy();

        scrMain.updatecm();

        if (guiChatClanWorld != null) {
            guiChatClanWorld.Update();
        }
        if (isBag) {
            GameCanvas.allInfo.updatekey();
            GameCanvas.allInfo.update();
        }


        for (int i = 0; i < veffClient.size(); i++) {
            MainEffect eff = veffClient.elementAt(i);
            eff.update();
        }
        guiMain.update();
        UpdateCamera();

        ChatTextField.getInstance().update();
        for (int i = 0; i < vCharInMap.size(); i++) {
            if (vCharInMap.elementAt(i).charID != Char.myChar().charID) {
                vCharInMap.elementAt(i).update();
            }

        }
        Char.myChar().update();

        for (int i = 0; i < vItemMap.size(); i++) {
            // paint item
            vItemMap.elementAt(i).update();
        }

        for (int i = 0; i < vMob.size(); i++) {
            vMob.elementAt(i).update();
        }

        for (int i = 0; i < vNpc.size(); i++) {
            vNpc.elementAt(i).update();
        }

        updateFlyText();
        for (int i = 0; i < Effect.vEffect2.size(); i++) {
            Effect l = (Effect) Effect.vEffect2.elementAt(i);
            l.update();
        }

        autoFocus();
        if (isBag) {
            GameCanvas.allInfo.updatePointer();
        }

        if (qShortQuest != null) {
            qShortQuest.Update();
        }
        guiContact.Update();
        if (isAutoDanh)
            AutoDanh();

        if (isAutoNhatItem) {
            autoPickUpItem();
        }
    }


    public static void loadImages() {
        imgFocusActor = GameCanvas.loadImage("/GuiNaruto/imgFocus.png");
        imgQuest = GameCanvas.loadImage("/imgNpcQuest.png");

        for (int i = 0; i < imgCloudy.length; i++) {
            imgCloudy[i] = GameCanvas.loadImage("/bg/may" + i + ".png");
        }
        wcloudy0 = MGraphics.getImageWidth(imgCloudy[0]);
        ncloudy0 = GameCanvas.w / wcloudy0 + 1;
        xpaintcloudy0 = new int[ncloudy0 * 3];
        ypaintcloudy0 = new int[ncloudy0 * 3];
        kcwcloudy0 = GameCanvas.w % wcloudy0;
        for (int i = 0; i < xpaintcloudy0.length; i++) {
            xpaintcloudy0[i] = (wcloudy0 + kcwcloudy0) * i;
            ypaintcloudy0[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
        }
        wcloudy1 = MGraphics.getImageWidth(imgCloudy[1]);
        ncloudy1 = GameCanvas.w / wcloudy1 + 2;
        xpaintcloudy1 = new int[ncloudy1 * 3];
        ypaintcloudy1 = new int[ncloudy1 * 3];
        for (int i = 0; i < xpaintcloudy1.length; i++) {
            xpaintcloudy1[i] = wcloudy1 * i;
            ypaintcloudy1[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
        }

        wcloudy1 = MGraphics.getImageWidth(imgCloudy[2]);
        ncloudy1 = GameCanvas.w / wcloudy1 + 2;
        xpaintcloudy2 = new int[ncloudy1 * 3];
        ypaintcloudy2 = new int[ncloudy1 * 3];
        for (int i = 0; i < xpaintcloudy2.length; i++) {
            xpaintcloudy2[i] = wcloudy1 * i;
            ypaintcloudy2[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
        }

    }


    public void paint(MGraphics g) {
        g.setColor(0x3aadfe);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        GameCanvas.getInstance().paintBGGameScreen(g);
        g.translate(-cmx, -cmy);

        if (!GameCanvas.isLowGraphic) {
            PaintCloudy0(g);
            PaintCloudy1(g);
            PaintCloudy2(g);
            FallingLeafEffect.PaintFallingLeaf(g);
        }

        TileMap.getInstance().paintMap(g);

        for (int i = 0; i < vNpc.size(); i++)
            vNpc.elementAt(i).paint(g);

        for (int i = 0; i < vMob.size(); i++)
            vMob.elementAt(i).paint(g);

        if (Char.myChar().mobFocus != null)
            paintInfoMob(g);

        if (Char.myChar().npcFocus != null)
            g.drawImage(imgFocusActor, Char.myChar().npcFocus.cx,
                    Char.myChar().npcFocus.cy - Char.myChar().npcFocus.ch - 20 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER);

        if (Char.myChar().charFocus != null)
            g.drawImage(imgFocusActor, Char.myChar().charFocus.cx,
                    Char.myChar().charFocus.cy - Char.myChar().charFocus.ch - 20 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER);

        if (Char.myChar().mobFocus != null)
            g.drawImage(imgFocusActor, Char.myChar().mobFocus.x,
                    Char.myChar().mobFocus.y - Char.myChar().mobFocus.h - 35 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER);

        if (Char.myChar().itemFocus != null)
            g.drawImage(imgFocusActor, Char.myChar().itemFocus.x,
                    Char.myChar().itemFocus.y - 65 + GameCanvas.gameTick % 20 / 4, MGraphics.VCENTER | MGraphics.HCENTER);


        for (int i = 0; i < vCharInMap.size(); i++) {
            Char c = vCharInMap.elementAt(i);
            if (c.charID != Char.myChar().charID) {
                c.paint(g);
                if (c.skillPaint != null && c.statusMe != Char.A_DEAD && c.statusMe != Char.A_DEADFLY) {
                    c.PaintCharWithSkill(g);
                }
            }
        }

        Char.myChar().paint(g);
        if (Char.myChar() != null && Char.myChar().skillPaint != null && Char.myChar().statusMe != Char.A_DEAD && Char.myChar().statusMe != Char.A_DEADFLY) {
            Char.myChar().PaintCharWithSkill(g);
        }

        paintFlyText(g);
        for (int i = 0; i < veffClient.size(); i++) {
            MainEffect eff = veffClient.elementAt(i);
            eff.paint(g);
        }
        paintWaypointArrow(g);
        ChatTextField.getInstance().paint(g);

        for (int i = 0; i < vItemMap.size(); i++) {
            vItemMap.elementAt(i).paint(g);
        }


        for (int i = 0; i < Effect.vEffect2.size(); i++) {
            Effect l = (Effect) Effect.vEffect2.elementAt(i);
            l.paint(g);
        }

        for (int i = 0; i < Effect.vEffect2Outside.size(); i++) {
            Effect l = (Effect) Effect.vEffect2Outside.elementAt(i);
            l.paint(g);
        }

        paintBgItem(g);

        if (MiniGameManager.getInstance().isInMiniGame && MiniGameManager.getInstance().BombZones.size() > 0) {
            for (int i = 0; i < MiniGameManager.getInstance().BombZones.size(); i++) {
                if (MiniGameManager.getInstance().BombZones.get(i) != null) {
                    MiniGameManager.getInstance().BombZones.get(i).paint(g);
                }
            }
        }

        if (MiniGameManager.getInstance().isInMiniGame) {
            if (MiniGameManager.getInstance().isGetBombed) {
                g.drawRegionScale(LoadImageInterface.imgBloodScreen, 0, 0, LoadImageInterface.imgBloodScreen.getWidth(), LoadImageInterface.imgBloodScreen.getHeight(), 0,
                        GameCanvas.w / 2, GameCanvas.h / 2, MGraphics.VCENTER | MGraphics.HCENTER);
            }
        }

        ChatTextField.getInstance().paint(g);


        //paint short quest
        if (qShortQuest != null) {
            qShortQuest.Paint(g);
        }

        paintCmdBar(g);
        guiMain.Paint(g);

        if (isBag) {
            g.setColor(0x000000, GameCanvas.opacityTab);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.disableBlending();
            GameCanvas.allInfo.paint(g);
        }


        if (guiChatClanWorld != null) {
            if (!MenuIcon.isShowTab) {
                guiChatClanWorld.Paint(g);
            }
        }

        if (infoCharEquipment != null) {
            infoCharEquipment.paint(g);
        }

        super.paint(g);

    }


    public static void resetTranslate(MGraphics g) {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.setClip(0, -200, GameCanvas.w, 200 + GameCanvas.h);
    }


    public static void startFlyText(String flyString, int x, int y, int dx, int dy, int color) {
        int n = -1;
        for (int i = 0; i < 5; i++)
            if (flyTextState[i] == -1) {
                n = i;
                break;
            }
        if (n == -1)
            return;
        flyTextColor[n] = color;
        flyTextString[n] = flyString;
        flyTextX[n] = x;
        flyTextY[n] = y;
        flyTextDx[n] = dx;
        flyTextDy[n] = dy;
        flyTextState[n] = 0;
    }

    private static void updateFlyText() {
        for (int i = 0; i < 5; i++) {
            if (flyTextState[i] != -1) {
                flyTextState[i] += CRes.abs(flyTextDy[i]);
                if (flyTextState[i] > 30)
                    flyTextState[i] = -1;
                flyTextX[i] += flyTextDx[i];
                flyTextY[i] += flyTextDy[i];
            }
        }
    }


    private static void paintFlyText(MGraphics g) {
        for (int i = 0; i < 5; i++)
            if (flyTextState[i] != -1) {
                if (!GameCanvas.isPaint(flyTextX[i], flyTextY[i]))
                    continue;

                if (flyTextColor[i] == MFont.RED)
                    FontManager.getInstance().number_red_hp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                else if (flyTextColor[i] == MFont.YELLOW)
                    FontManager.getInstance().number_yellow_xp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                else if (flyTextColor[i] == MFont.GREEN)
                    FontManager.getInstance().number_green_mp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                else if (flyTextColor[i] == MFont.FATAL)
                    FontManager.getInstance().tahoma_7b_yellow.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER, FontManager.getInstance().tahoma_7b_blue);
                else if (flyTextColor[i] == MFont.FATAL_ME)
                    FontManager.getInstance().tahoma_7b_white.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER, FontManager.getInstance().tahoma_7b_blue);
                else if (flyTextColor[i] == MFont.MISS)
                    SmallImage.drawSmallImage(g, 1062, flyTextX[i], flyTextY[i], 0, MGraphics.VCENTER | MGraphics.HCENTER);
                else if (flyTextColor[i] == MFont.ORANGE)
                    FontManager.getInstance().number_orange.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                else if (flyTextColor[i] == MFont.ADDMONEY)
                    FontManager.getInstance().tahoma_7_yellow.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], MFont.CENTER, FontManager.getInstance().tahoma_7_red);
                else if (flyTextColor[i] == MFont.MISS_ME)
                    SmallImage.drawSmallImage(g, 655, flyTextX[i], flyTextY[i], 0, MGraphics.VCENTER | MGraphics.HCENTER);
            }
    }

    private static void paintCmdBar(MGraphics g) {
        g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

        // // PAINT HP
        int hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
        if (hpWidth > hpBarW) {
            hpWidth = 0;
        }

        g.setColor(0x770000);
        g.fillRect(hpBarX + 4, hpBarY - 2, hpWidth, 10);

        g.setColor(0xCC0000);
        g.fillRect(hpBarX + 4, hpBarY - 2 + 1, hpWidth, 10);

        // paint MP
        hpWidth = Char.myChar().cMP * hpBarW / Char.myChar().cMaxMP;
        if (hpWidth > hpBarW) {
            hpWidth = 0;
        }
        g.setColor(0x001188);
        g.fillRect(hpBarX + 4, hpBarY - 12, hpWidth, 10);
    }

    public static void setPopupSize(int w, int h) {
        popupW = w;
        popupH = h;
        popupX = gW2 - w / 2;
        popupY = gH2 - h / 2;

        if (GameCanvas.h <= 250) {
            popupY -= 10;
        }
        if (GameCanvas.isTouchControlLargeScreen && GameCanvas.getInstance().currentScreen instanceof GameScreen) {
            popupW = 310;
            popupX = gW / 2 - popupW / 2;
        }
        if (popupY < -10)
            popupY = -10;

        if (GameCanvas.h > 208 && popupY < 0) {
            popupY = 0;
        }
        if (GameCanvas.h == 208 && popupY < 10) {
            popupY = 10;
        }
    }

    public Char findCharInMap(long charId) {
        for (int i = 0; i < vCharInMap.size(); i++) {
            Char c = vCharInMap.elementAt(i);
            if (c.charID == charId) {
                return c;
            }
        }
        return null;
    }

    public Mob findMobInMap(long monsterId) {
        for (int i = 0; i < vMob.size(); i++) {
            Mob c = vMob.elementAt(i);
            if (c.mobId == monsterId) {
                return c;
            }
        }
        return null;
    }

    public void actionPerform(int idAction, Object p) {
        switch (idAction) {
            case 111039: // dong y trao doi 
                Service.getInstance().AcceptTrade((short) Char.myChar().partnerTrade.charID);
                tradeGui = new TradeGui(45, 0);
                Service.getInstance().requestInventory();
                GameCanvas.getInstance().endDlg();
                break;
            case Constants.BUTTON_SEND:
                if (TabChat.tfCharFriend.getText().toLowerCase().trim().length() > 0 && Char.toCharChat != null) {
                    Service.getInstance().ChatPrivate(Char.toCharChat.charID, TabChat.tfCharFriend.getText());
                }

                TabChat.tfCharFriend.setText("");
                TabChat.tfCharFriend.isFocus = false;
                break;

            case Constants.BUTTON_ICON_CHAT:
                // xy ly button icon o cho nay
                TabChat.iconChat = new IconChat(popupX, popupY);
                TabChat.getInstance().isPaintListIcon = true;
                break;
            case Constants.CHAT_PRIVATE: // mo giao dien chat rieng
                if (indexRow >= 0) {
                    Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
                    ChatPrivate.AddNewChater((short) Char.myChar().charID, charF.cName);
                }
                indexRow = -1;
                left = null;
                center = null;
                OpenUiChatTab();
                break;
            case Constants.CONFIRM_UNFRIEND:
                Char player = (Char) Char.myChar().vFriend.elementAt(indexRow);
                Service.getInstance().DeleteFriend(player.charID);
                break;
            case 111037:
                Service.getInstance().AcceptParty(Party.getInstance().CharId);
                GameCanvas.getInstance().endDlg();
                break;
            case 111038:
                Service.getInstance().AcceptFriend(Char.myChar().idFriend);
                Char.myChar().idFriend = -1;
                GameCanvas.getInstance().endDlg();
                break;
        }
    }

    private void autoFocus() {
        if (cLastFocusID >= 0 && vCharInMap.size() > 0) {
            int cIndex = Char.getIndexChar(cLastFocusID);
            if (cIndex >= 0 && cIndex < vCharInMap.size()) {
                Char cFocus = vCharInMap.elementAt(cIndex);
                if (cFocus == null) return;
                if (Char.isCharInScreen(cFocus)) {
                    Char.myChar().mobFocus = null;
                    Char.myChar().DoFocusNPC();
                    Char.myChar().itemFocus = null;
                    Char.isManualFocus = true;
                    Char.myChar().charFocus = cFocus;
                    Service.getInstance().RequestPlayerInfo(cFocus.charID);

                }
            } else {
                cLastFocusID = -1;
                Char.myChar().charFocus = null;
            }
        } else {
            cLastFocusID = -1;
        }
    }

    private void paintWaypointArrow(MGraphics g) {
        for (int i = 0; i < TileMap.getInstance().vGo.size(); i++) {
            Waypoint way = TileMap.getInstance().vGo.elementAt(i);
            if (way.name != null) {
                FontManager.getInstance().tahoma_7.drawString(g, way.name, way.minX + (way.maxX - way.minX) / 2,
                        way.minY + 2 + LoadImageInterface.imgXinCho.getHeight() / 3, 2);
            }
        }
    }


    private void OpenUiChatTab() {
        TabChat.getInstance().switchToMe();
        setPopupSize(175, 200);
        left = center = null;
    }

    private void paintInfoMob(MGraphics g) {
        if (Char.myChar().mobFocus.mobName != null) {
            FontManager.getInstance().tahoma_7_white.drawStringShadow(g, Char.myChar().mobFocus.mobName, Char.myChar().mobFocus.x, Char.myChar().mobFocus.y - Char.myChar().mobFocus.h - 20, 2);
        }
    }

    private void doFire() {

        if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().mobFocus != null) {
            if (System.currentTimeMillis() - (lastpress + delayPressAtt) > 0) {
                Char.myChar().cdir = Char.myChar().cx - Char.myChar().mobFocus.x > 0 ? -1 : 1;

                lastpress = System.currentTimeMillis();

                Char.myChar().mobFocus.setInjure();
                Service.getInstance().sendPlayerAttackMonster(Char.myChar().mobFocus.mobId, currentIdSkill);
            }
        } else if (Char.myChar().statusMe != Char.A_DEADFLY &&
                Char.myChar().statusMe != Char.A_DEAD &&
                Char.myChar().charFocus != null &&
                Char.myChar().charFocus.statusMe != Char.A_DEADFLY &&
                Char.myChar().charFocus.statusMe != Char.A_DEAD) {

            Char.myChar().cdir = Char.myChar().cx - Char.myChar().charFocus.cx > 0 ? -1 : 1;

            if (Char.myChar().typePk != -1) {
                if (Char.myChar().isClanWarAttack) // pkclan hay noi chien 
                {
                    if (Char.myChar().typePk != Char.myChar().charFocus.typePk) {
                        Char.myChar().charFocus.DoInjure();
                        Service.getInstance().sendPlayerAttackPlayer(Char.myChar().charFocus.charID, currentIdSkill);
                    }
                } else if (Char.myChar().isMiniGameAttack) // pk minigame
                {
                    if (Char.myChar().typePk != Char.myChar().charFocus.typePk) {
                        Char.myChar().charFocus.DoInjure();
                        Service.getInstance().sendPlayerAttackPlayer(Char.myChar().charFocus.charID, currentIdSkill);
                    }
                } else if (Char.myChar().typePk == Char.myChar().charFocus.typePk ||
                        Char.myChar().charFocus.typePk == 0 ||
                        Char.myChar().typePk == 0) // pk thach dau va do sat
                {
                    Char.myChar().charFocus.DoInjure();
                    Service.getInstance().sendPlayerAttackPlayer(Char.myChar().charFocus.charID, currentIdSkill);
                }
            } else {
                Char.myChar().cdir = Char.myChar().cx - Char.myChar().charFocus.cx > 0 ? -1 : 1;
                guiMain.menuIcon.indexpICon = Constants.ICON_GIAOTIEP;
                guiMain.menuIcon.paintButtonClose = true;
                guiMain.menuIcon.contact = new GuiContact(GameCanvas.hw, 20);
                guiMain.menuIcon.contact.SetPosClose(guiMain.menuIcon.cmdClose);
                MenuIcon.lastTab.add("" + Constants.ICON_GIAOTIEP);
                MenuIcon.isShowTab = true;
            }
        } else if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().npcFocus != null) {
            Char.myChar().cdir = Char.myChar().cx - Char.myChar().npcFocus.cx > 0 ? -1 : 1;

            if (Char.myChar().npcFocus.typeNV > -1) {
                Vector<Command> menuList = new Vector<Command>();
                menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));
                menuList.addElement(new Command("Menu", GameCanvas.instance, GameCanvas.cMenuNpc, -1 + ""));

                GameCanvas.menu.startAtNPC(menuList, Char.myChar().npcFocus, "");
            } else {
                if (MainQuestManager.getInstance().NewQuest != null) {
                    QuestTemplate quest = MainQuestManager.getInstance().NewQuest;
                    if (quest.IdNpcReceive == Char.myChar().npcFocus.npcId) {
                        Char.myChar().npcFocus.typeNV = 0;
                        Vector<Command> menuList = new Vector<Command>();
                        menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));
                        GameCanvas.menu.startAtNPC(menuList, Char.myChar().npcFocus, "");
                    }
                }

                Service.getInstance().MenuNpc((short) Char.myChar().npcFocus.template.npcTemplateId, (short) -1);
            }
        } else if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().itemFocus != null) {
            Service.getInstance().itemPick(Char.myChar().itemFocus.itemMapId);
        }
    }

    private static void initTField() {
        tfCharFriend = new TField();
        tfCharFriend.width = popupW - 30;
        tfCharFriend.height = ITEM_HEIGHT + 2;
        tfCharFriend.x = xGui + 5;
        tfCharFriend.y = yGui + heightGui - 25;
        tfCharFriend.isFocus = false;

        tfCharFriend.setInputType(TField.INPUT_TYPE_ANY);
    }

    public static void loadMapItem() {
        // đọc dữ liệu data Map item
        DataInputStream dis = null;
        try {
            dis = new DataInputStream(GameMidlet.asset.open("mapitem/mapItem"));
            short nMapItem = dis.readShort();
            for (int i = 0; i < nMapItem; i++) {
                BgItem biSe = new BgItem();
                biSe.id = i;
                biSe.idImage = dis.readShort(); // id hinh 
                biSe.layer = dis.readByte(); // layer truoc sau so voi tile
                biSe.dx = dis.readShort(); // tam item trong map
                biSe.dy = dis.readShort();
                byte nTileNotMove = dis.readByte();
                if (nTileNotMove < 0) return;
                biSe.tileX = new int[nTileNotMove];
                biSe.tileY = new int[nTileNotMove];
                for (int j = 0; j < nTileNotMove; j++) {
                    biSe.tileX[j] = dis.readByte();
                    biSe.tileY[j] = dis.readByte();
                }
                TileMap.getInstance().vItemBg.addElement(biSe);
            }
        } catch (Exception e) {
            Log.e(GameScreen.class.getName(), "Exception", e);
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void loadMapTable(int mapId) {
        DataInputStream dis = null;
        try {
            dis = new DataInputStream(GameMidlet.asset.open("mapitem/mapTable" + mapId));
            short count = dis.readShort();
            for (int i = 0; i < count; i++) {
                BgItem biMap = new BgItem();
                short id = dis.readShort();
                BgItem currBi = TileMap.getInstance().getBgById(id);
                biMap.id = currBi.id;
                biMap.x = dis.readShort() * TileMap.TILE_MAP_SIZE;
                biMap.y = dis.readShort() * TileMap.TILE_MAP_SIZE;
                biMap.idImage = currBi.idImage;
                biMap.dx = currBi.dx;
                biMap.dy = currBi.dy;
                biMap.layer = currBi.layer;
                TileMap.getInstance().vCurrItem.addElement(biMap);
            }
        } catch (Exception e) {
            Log.e(GameScreen.class.getName(), "Exception", e);
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void paintBgItem(MGraphics g) {
        for (int a = 0; a < TileMap.getInstance().vCurrItem.size(); a++) {
            BgItem bi = TileMap.getInstance().vCurrItem.elementAt(a);
            if (bi.idImage == -1) continue;
            if (bi.layer >= 1 && GuiMain.isPaintOjectMap) {
                bi.paint(g);
            }
        }
    }

    public static int idSkillUse;

    private void DoTouchQuickSlot(int id) {
        idSkillUse = id;
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY) {
            GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?",
                    new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                    new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
            return;
        }
        if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.typeKhu == -1) {
            Service.getInstance().RequestRegionInfo();
            return;
        }
        if (Char.myChar().npcFocus != null) {
            doFire();
        }

        if (GameCanvas.isTouch) {
            QuickSlot ql = Char.myChar().mQuickslot[id];
            if (ql.canFight()) {
                boolean isNgoaiRange = false;
                if (Char.myChar().mobFocus != null) {
                    SkillTemplate skill = SkillTemplates.hSkilltemplate.get(ql.idSkill);
                    int range = CRes.getDistance(Char.myChar().cx, Char.myChar().cy, Char.myChar().mobFocus.x,
                            Char.myChar().mobFocus.y);
                    isNgoaiRange = range > skill.rangeX[skill.level >= skill.rangeX.length ? 0 : skill.level];
                }
                currentIdSkill = ql.idSkill;
                if (Char.myChar().mobFocus != null && isNgoaiRange) {
                    GameCanvas.getInstance().StartDglThongBao("Mục tiêu ở quá xa");
                    int maxkcX = CRes.abs(Char.myChar().cx - Char.myChar().mobFocus.x);
                    int kc = 1, leng = 0;
                    if (maxkcX > 20 || CRes.abs(Char.myChar().cy - Char.myChar().mobFocus.y) > 10) {
                        for (int j = 0; j < maxkcX; j++) {
                            leng += j;
                            if (leng >= maxkcX) {
                                kc = j - 1;
                                break;
                            }
                        }
                        if (Char.myChar().cx > Char.myChar().mobFocus.x) {
                            Char.myChar().updateCharRun();
                            if (Char.myChar().cdir == 1)
                                Char.myChar().cdir = -1;
                            if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                                Char.myChar().updateCharRun();
                                Char.myChar().statusMe = Char.A_RUN;
                                Char.myChar().cvx = -kc;
                            }
                        } else {
                            Char.myChar().updateCharRun();
                            if (Char.myChar().cdir == -1)
                                Char.myChar().cdir = 1;
                            if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                                Char.myChar().updateCharRun();
                                Char.myChar().statusMe = Char.A_RUN;
                                Char.myChar().cvx = kc;
                            }
                        }
                    }
                    return;
                }
                if (Char.myChar().mobFocus != null) {
                    if (!isAutoDanh) {
                        xStartAuto = Char.myChar().cx;
                        yStartAuto = Char.myChar().cy;
                    }
                    doFire();
                } else if (Char.myChar().npcFocus != null || Char.myChar().itemFocus != null ||
                        Char.myChar().charFocus != null) {
                    if (Char.myChar().charFocus != null) {
                        if (Char.myChar().typePk == Char.myChar().charFocus.typePk) {
                            doFire();
                        }
                    } else {
                        doFire();
                    }
                }
                if (Char.myChar().npcFocus == null && Char.myChar().charFocus != null &&
                        Char.myChar().charFocus.statusMe != Char.A_DEAD &&
                        Char.myChar().typePk != Char.myChar().charFocus.typePk) {
                    doFire();        // ko start cooldow
                }
            }
        }
    }

    private static void UpdateCloudy() {
        for (int i = 0; i < ncloudy1; i++)
            if (xpaintcloudy1[i] > TileMap.pxw)
                ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
            else xpaintcloudy1[i] += 1;
        for (int i = 0; i < ncloudy1; i++)
            if (xpaintcloudy1[i] > TileMap.pxw)
                if (i == ncloudy1 - 1) {
                    xpaintcloudy1[i] = 0 - wcloudy1;
                    ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                } else {
                    xpaintcloudy1[i] = 0 - wcloudy1;
                    ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                }
        for (int i = 0; i < ncloudy2; i++)
            if (xpaintcloudy2[i] > TileMap.pxw)
                ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
            else xpaintcloudy2[i] += 2;
        for (int i = 0; i < ncloudy2; i++)
            if (xpaintcloudy2[i] > TileMap.pxw)
                if (i == ncloudy2 - 1) {
                    xpaintcloudy2[i] = 0 - wcloudy2;
                    ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                } else {
                    xpaintcloudy2[i] = 0 - wcloudy2;
                    ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                }
        for (int i = 0; i < ncloudy0; i++)
            if (xpaintcloudy0[i] > TileMap.pxw)
                ypaintcloudy0[i] = CRes.random(80, TileMap.pxh);
            else xpaintcloudy0[i] += 3;

        for (int i = 0; i < ncloudy0; i++)
            if (xpaintcloudy0[i] > TileMap.pxw)
                if (i == ncloudy0 - 1) {
                    xpaintcloudy0[i] = 0 - wcloudy0 - kcwcloudy0;
                    ypaintcloudy0[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                } else {
                    xpaintcloudy0[i] = 0 - wcloudy0 - kcwcloudy0;
                    ypaintcloudy0[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                }
    }

    private static void PaintCloudy1(MGraphics g) {
        if (imgCloudy[1] == null) return;
        for (int i = 0; i < ncloudy1; i++) {
            g.drawImage(imgCloudy[1], xpaintcloudy1[i], ypaintcloudy1[i], MGraphics.BOTTOM | MGraphics.LEFT);
        }
    }

    private static void PaintCloudy0(MGraphics g) {

        if (imgCloudy[0] == null) return;
        for (int i = 0; i < ncloudy0; i++) {
            g.drawImage(imgCloudy[0], xpaintcloudy0[i], ypaintcloudy0[i], MGraphics.BOTTOM | MGraphics.LEFT);
        }
    }

    private static void PaintCloudy2(MGraphics g) {
        if (imgCloudy[2] == null) return;
        for (int i = 0; i < ncloudy2; i++) {
            g.drawImage(imgCloudy[2], xpaintcloudy2[i], ypaintcloudy2[i], MGraphics.BOTTOM | MGraphics.LEFT);
        }

    }

    public void addEffectKillMobAttack(Mob m, Char c, int idEffect, byte ideffsub) {
        veffClient.add(new EffectKill(idEffect, m, c, ideffsub));
    }

    public void addEffectEnd(byte type, int x, int y, int xTo, int toY) {
        veffClient.add(new EffectKill(type, x, y, xTo, toY));
    }

    private void AutoDanh() {
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_ATTK
                || Char.myChar().statusMe == Char.A_RUN
                || Char.myChar().skillPaint != null)
            return;

        if (Char.myChar().statusMe == Char.A_ATTK || Char.myChar().statusMe == Char.A_RUN)
            return;

        timeDow--;
        if (timeDow <= 0 && Char.myChar().mobFocus != null && !Char.myChar().mobFocus.injureThenDie) {
            indexKeyTouchAuto = (indexKeyTouchAuto + 1) % 5;
            QuickSlot ql = Char.myChar().mQuickslot[indexKeyTouchAuto];
            if (ql.idSkill == -1) return;
            SkillTemplate skill = SkillTemplates.hSkilltemplate.get(ql.idSkill);
            short range = skill.rangeX[skill.level];
            if (Utils.distance(Char.myChar().cx, Char.myChar().cy, Char.myChar().mobFocus.x,
                    Char.myChar().mobFocus.y) <= range) {
                if (ql.quickslotType == QuickSlot.TYPE_SKILL)
                    DoTouchQuickSlot(indexKeyTouchAuto);
            } else {
                timeDow = 100;
            }
        } else if (timeDow <= 0 && (Char.myChar().mobFocus == null || Char.myChar().mobFocus.injureThenDie)) {
            // tim con quai trong range auTo
            Char.myChar().clearAllFocus();
            if (Char.myChar().statusMe == Char.A_STAND)
                for (int i = 0; i < vMob.size(); i++) {
                    Mob ac = vMob.elementAt(i);
                    if (ac != null && !ac.injureThenDie && CRes.abs(ac.y - Char.myChar().cy) < 30 &&
                            Utils.distance(xStartAuto, yStartAuto, ac.x, ac.y) <= rangeAuto) {
                        int dis = Utils.distance(Char.myChar().cx, Char.myChar().cy, ac.x, ac.y);
                        if (dis < 160) {
                            int maxkcX = CRes.abs(Char.myChar().cx - ac.x);
                            int kc = 1, leng = 0;
                            if (maxkcX > 20 || CRes.abs(Char.myChar().cy - ac.y) > 10) {
                                for (int j = 0; j < maxkcX; j++) {
                                    leng += j;
                                    if (leng >= maxkcX) {
                                        kc = j - 1;
                                        break;
                                    }
                                }
                                if (Char.myChar().cx > ac.x) {
                                    Char.myChar().updateCharRun();
                                    if (Char.myChar().cdir == 1)
                                        Char.myChar().cdir = -1;
                                    if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                                        if (ac.isBoss && !ac.isBossAppear) continue;
                                        Char.myChar().updateCharRun();
                                        Char.myChar().statusMe = Char.A_RUN;
                                        Char.myChar().mobFocus = ac;
                                        Char.myChar().cvx = -kc;
                                        break;
                                    }
                                } else {
                                    Char.myChar().updateCharRun();
                                    if (Char.myChar().cdir == -1)
                                        Char.myChar().cdir = 1;
                                    if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
                                        if (ac.isBoss && !ac.isBossAppear) continue;
                                        Char.myChar().updateCharRun();
                                        Char.myChar().statusMe = Char.A_RUN;
                                        Char.myChar().mobFocus = ac;
                                        Char.myChar().cvx = kc;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (i == vMob.size() - 1)
                        timeDow = 120; // sleep thoi gian tim quai
                }
        }
    }

    private void autoPickUpItem() {
        for (int i = 0; i < vNhatItemMap.size(); i++) {
            ItemMap item = vNhatItemMap.elementAt(i);
            if (item.timeTonTai < 3 || item.isSendNhat) continue;
            item.isSendNhat = true;

//                var startWaitTime = System.currentTimeMillis();
//                while (isPickingUpItem && System.currentTimeMillis() - startWaitTime < Constants.PICK_UP_ITEM_WAIT_TIME)
//                {
//                    Thread.Sleep(100);
//                }

            Service.getInstance().itemPick(item.itemMapId); //.itemMapID
//                isPickingUpItem = true;

            vNhatItemMap.removeElementAt(i);
            i--;
        }
    }
}
