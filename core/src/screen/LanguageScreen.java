package screen;



import com.sakura.thelastlegend.GameCanvas;

import lib.LoadImageInterface;
import Objectgame.Char;
import model.IActionListener;
import model.MResources;
import model.Screen;
import lib.Command;
import lib.Rms;

public class LanguageScreen extends Screen implements IActionListener {

	public static Command cmdEng, cmdVn;
	private int indexRow = -1;
	private int popupW, popupH, popupX, popupY;

	public LanguageScreen()
	{
		cmdEng = new Command("English", 1);
		cmdEng.setPos(GameCanvas.w / 2 - 120 + 5, GameCanvas.h / 2 + 20, LoadImageInterface.img_use,
				LoadImageInterface.img_use_focus);
		cmdVn = new Command("Tiếng Việt", 2);
		cmdVn.setPos(GameCanvas.w / 2 + 5, GameCanvas.h / 2 + 20, LoadImageInterface.img_use,
				LoadImageInterface.img_use_focus);
	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case 1:
				break;
			case 2:
				break;
			case 1000:
				GameCanvas.getInstance().currentDialog = null;
				MResources.languageID = indexRow == 0 ? MResources.Lang_VI : MResources.Lang_EN;
				saveLanguageID(MResources.languageID);
				MResources.loadLanguage();
				GameCanvas.getInstance().initGameCanvas();
				GameCanvas.getInstance().loginScreen.switchToMe();
				break;
		}
	}

	public void switchToMe()
	{
		GameScreen.gH = GameCanvas.h;
		GameCanvas.getInstance().loadBG(0);

		super.switchToMe();

//		GameScreen.instance = null;

		// ==============================

		GameScreen.loadCamera(Char.myChar().cx, Char.myChar().cy);
		GameScreen.cmx = 100;
		// ==============================
		popupW = 170;
		popupH = 175;
		if (GameCanvas.w == 128 || GameCanvas.h <= 208)
		{
			popupW = 126;
			popupH = 160;
		}
		popupX = GameCanvas.w / 2 - popupW / 2;
		popupY = GameCanvas.h / 2 - popupH / 2;
		if (GameCanvas.h <= 250)
			popupY -= 10;

		indexRow = -1;
		if (!GameCanvas.isTouch)
			indexRow = 0;
	}


	private void saveLanguageID(int languageID)
	{
		Rms.saveRMSInt("indLanguage", languageID);
	}

	public void update()
	{
		GameScreen.cmx++;
		if (GameScreen.cmx > GameCanvas.w * 3 + 100)
			GameScreen.cmx = 100;

		super.update();
	}

	public void updateKey()
	{
		if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4] || GameCanvas.keyPressed[6] ||
				GameCanvas.keyPressed[8])
			indexRow = indexRow == 0 ? 1 : 0;

		if (GameCanvas.isPointerJustRelease)
			if (GameCanvas.isPointerHoldIn(popupX + 10, popupY + 45, popupW - 10, 70))
			{
				if (GameCanvas.isPointerClick)
					indexRow = (GameCanvas.py - (popupY + 45)) / 35;
				perform(1000, null);
			}
		super.updateKey();
		GameCanvas.clearKeyPressed();
	}

}
