package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import java.util.List;

import Objectgame.TileMap;
import lib.LoadImageInterface;
import lib.MBitmap;
import lib.MSystem;
import lib.Rms;
import model.Screen;
import model.SmallImage;
import real.Service;

public class DownloadImageScreen extends Screen {

    public static boolean isOKNext;

    public static DownloadImageScreen instance;
//    public int idAdd;
    private int idNext;
    private MBitmap imgBgLoading, frameLoading, ongLoading;

    private MBitmap[] imgFrameCharLoading = new MBitmap[6];
    private int indexPercent, indexPaintLoad;
//    public boolean isTheoList;
//    public int[] listId;

    private long timeStart;

    private boolean isDownLoadedSuccess;

    private List<String> imageKeyList;

    public void switchToMe(List<String> imageKeyList) {
        this.isDownLoadedSuccess = false;
        this.imageKeyList = imageKeyList;
//        this.isTheoList = false;
        this.idNext = 0;
        this.indexPercent = indexPaintLoad = 0;
        super.switchToMe();
    }

//    public void switchToMe(int idAdd, int[] listIdd) {
//        this.idAdd = idAdd;
//        listId = listIdd;
//        isDownLoadedSuccess = false;
//        isTheoList = true;
//        idNext = 0;
//        indexPercent = indexPaintLoad = 0;
//        super.switchToMe();
//    }

    private void loadingImg() {
        if (imgBgLoading == null) {
            imgBgLoading = GameCanvas.loadImage("/GuiNaruto/loading/imgBgLoading.png");
        }

        if (frameLoading == null) {
            frameLoading = GameCanvas.loadImage("/GuiNaruto/loading/ongLoading.png");
        }

        if (ongLoading == null) {
            ongLoading = GameCanvas.loadImage("/GuiNaruto/loading/frameLoading.png");
        }

        if (imgFrameCharLoading[0] == null) {
            for (int i = 0; i < imgFrameCharLoading.length; i++) {
                if (imgFrameCharLoading[i] == null) {
                    imgFrameCharLoading[i] = GameCanvas.loadImage("/GuiNaruto/loading/image-" + (i + 1) + ".png");
                }
            }
        }
    }

    public void update() {
        loadingImg();

        if (idNext < imageKeyList.size()) {
            indexPercent = (idNext) * frameLoading.getWidth() / imageKeyList.size();
            indexPaintLoad += (indexPercent - indexPaintLoad) / 2;
            if (indexPaintLoad >= frameLoading.getWidth()) {
                indexPaintLoad = frameLoading.getWidth();
            }
        } else {
            indexPaintLoad = frameLoading.getWidth();
        }

        if (imageKeyList != null && idNext < imageKeyList.size()) {
            byte[] data = Rms.loadRMS(MSystem.getPathRMS(SmallImage.getPathImage(imageKeyList.get(idNext))));
            long timeCoolDownNextImage = 15000;
            if (data != null) {
                idNext++;
                if (idNext >= imageKeyList.size()) {
                    isDownLoadedSuccess = true;
                }
            } else if (isOKNext || (System.currentTimeMillis() - timeStart) > timeCoolDownNextImage) {
                timeStart = System.currentTimeMillis();
                Service.getInstance().RequestImage(imageKeyList.get(idNext));
                isOKNext = false;
                idNext++;
                if (idNext >= imageKeyList.size()) {
                    isDownLoadedSuccess = true;
                }
            }
        }

        if (isDownLoadedSuccess) {
            GameCanvas.getInstance().loadBG(TileMap.getInstance().bg);
            GameCanvas.getInstance().nextScreen.switchToMe();
        }
        super.update();
    }

    public void paint(MGraphics g) {
        g.setColor(0x000000);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.drawRegionScale(imgBgLoading, 0, 0, imgBgLoading.getWidth(), imgBgLoading.getHeight(), 0,
                GameCanvas.w / 2, GameCanvas.h / 2, MGraphics.VCENTER | MGraphics.HCENTER);
        g.setColor(0x3c3a39);
        g.fillRect(GameCanvas.w / 2 - frameLoading.getWidth() / 2, 7 * GameCanvas.h / 8 - frameLoading.getHeight() / 2,
                frameLoading.getWidth(), frameLoading.getHeight());
        g.drawRegion(frameLoading, 0, 0, indexPaintLoad, frameLoading.getHeight(),
                0, GameCanvas.w / 2 - frameLoading.getWidth() / 2, 7 * GameCanvas.h / 8 - frameLoading.getHeight() / 2,
                MGraphics.TOP | MGraphics.LEFT);
        g.drawImage(ongLoading, GameCanvas.w / 2, 7 * GameCanvas.h / 8, MGraphics.VCENTER | MGraphics.HCENTER);
        if (imgFrameCharLoading != null && imgFrameCharLoading[GameCanvas.gameTick / 2 % 6] != null)
            g.drawImage(imgFrameCharLoading[GameCanvas.gameTick / 2 % 6],
                    GameCanvas.w / 2 - frameLoading.getWidth() / 2 + indexPaintLoad,
                    7 * GameCanvas.h / 8 - 18, MGraphics.VCENTER | MGraphics.HCENTER);

        super.paint(g);
    }

    public static DownloadImageScreen getInstance() {
        if (instance == null) {
            instance = new DownloadImageScreen();
        }

        instance.loadingImg();
        return instance;
    }
}
