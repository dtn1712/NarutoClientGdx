package screen;


import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.MGraphics;

import Gui.Constants;
import Gui.GuiMain;
import Objectgame.Char;
import lib.Command;
import lib.FontManager;
import lib.LoadImageInterface;
import lib.MBitmap;
import lib.Music;
import lib.Rms;
import lib.TField;
import lib.Utils;
import model.IActionListener;
import model.MResources;
import model.Menu;
import model.Paint;
import model.Screen;
import network.Session;

public class LoginScreen extends Screen implements IActionListener {

    private MBitmap imgBgLogin;
    public static final String LOGIN_ACTION = "LOGIN";
    public static final String REGISTER_ACTION = "REGISTER";

    public static boolean isLogout;
    private static MBitmap imgTitle;
    private static LoginScreen instance;

    public static boolean isLoggingIn;
    private Command cmdLogin;
    private Command cmdRes;
    private Command cmdLoginFB;
    private Command cmdLoginGoogle;

    private String[] _currentTip;
    private int dir = 1;
    private int g;
    private boolean isRes = false;

    private String _user, _pass;

    private TField tfIp;
    private TField tfPass;
    private TField tfRegPass;
    private TField tfUser;

    private int tipid = -1;

    private int v = 2;
    private int wC;
    private int yL;
    private int defYL;
    private int ylogo = -40;

    private int yt;

    private String _connectAction;

    public static final int actionLogin = 1;
    public static final int actionLoginGoogle = 2;
    public static final int actionLoginFacebook = 3;
    public static final int actionRegister = 4;
    public static final int comfirmRegister = 5;
    public static final int term = 6;

    public LoginScreen()
    {
        imgBgLogin = GameCanvas.loadImage("/bg/bgLogin.png");
        imgTitle = GameCanvas.loadImage("/screen/title.png");
        instance = this;
        GameScreen.loadCamera(Char.myChar().cx, Char.myChar().cy);
        GameScreen.cmx = 100;
        if (GameCanvas.h > 200) {
            defYL = GameCanvas.hh - 80;
        } else {
            defYL = GameCanvas.hh - 65;
        }

        resetLogo();
        wC = GameCanvas.w - 30;
        if (wC < 70) {
            wC = 70;
        }

        if (wC > 99) {
            wC = 99;
        }

        yt = GameCanvas.h - 150;
        yt = GameCanvas.h / 2 - 100 + 60 > 0 ? GameCanvas.h / 2 - 100 + 60 : 0;

        if (GameCanvas.h <= 160)
            yt = 20;

        tfUser = new TField();
        tfUser.x = GameCanvas.hw - 75;
        tfUser.y = yt + 20;
        tfUser.width = wC + 50;
        tfUser.height = ITEM_HEIGHT + 2;
        tfUser.isFocus = false;

        tfUser.setInputType(TField.INPUT_ALPHA_NUMBER_ONLY);
        tfPass = new TField();
        tfPass.x = GameCanvas.hw - 75;
        tfPass.y = yt + 60;
        tfPass.width = wC + 50;
        tfPass.height = ITEM_HEIGHT + 2;
        tfPass.isFocus = false;
        tfPass.setInputType(TField.INPUT_TYPE_PASSWORD);

        tfIp = new TField();
        tfIp.x = GameCanvas.hw - 75;
        tfIp.y = yt + 100;
        tfIp.width = wC + 50;
        tfIp.height = ITEM_HEIGHT + 2;
        tfIp.isFocus = false;

        tfIp.setInputType(TField.INPUT_ALPHA_NUMBER_ONLY);

        tfRegPass = new TField();
        tfRegPass.x = GameCanvas.hw - 20;
        tfRegPass.y = yt += 35;
        tfRegPass.width = wC;
        tfRegPass.height = ITEM_HEIGHT + 2;
        tfRegPass.isFocus = false;
        tfRegPass.setInputType(TField.INPUT_TYPE_PASSWORD);

        byte[] listAuto = Rms.loadRMS(Rms.autoSetting);
        if (listAuto == null || listAuto.length < SettingScreen.defaultAuto.length)
        {
            Rms.saveRMS(Rms.autoSetting, SettingScreen.defaultAuto);
        }
        else
        {
            GameScreen.isAutoDanh = listAuto[SettingScreen.AUTO_DANH] == 1;
//            GameScreen.isAutoNhatItem = listAuto[SettingScreen.AUTO_NHAT] == 1;
            Music.isplayMusic = listAuto[SettingScreen.MUSIC] == 1;
            Music.isplaySound = listAuto[SettingScreen.SOUND] == 1;
            GuiMain.isAnalog = listAuto[SettingScreen.TYPE_MOVE] == 1;
        }

        tfUser.setText(Rms.loadRMSString("acc"));
        tfPass.setText(Rms.loadRMSString("pass"));
        tfPass.setText(Rms.loadRMSString("pass"));

        cmdLogin = new Command(GameCanvas.w > 200 ? MResources.LOGIN1 : MResources.LOGIN2, this, actionLogin, null);
        cmdLogin.setPos(tfPass.x - 2 + 80, tfPass.y + 30, LoadImageInterface.btnLogin0,
                LoadImageInterface.btnLogin1);

        cmdRes = new Command(MResources.REGISTER, this, actionRegister, null);
        cmdRes.setPos(tfPass.x - 2 + 38, tfPass.y + 30, LoadImageInterface.btnLogin0,
                LoadImageInterface.btnLogin1);

        cmdLoginFB = new Command("", this, actionLoginFacebook, null);
        cmdLoginFB.setPos(tfPass.x - 2, tfPass.y + 30, LoadImageInterface.imgIconFb0,
                LoadImageInterface.imgIconFb1);

        cmdLoginGoogle = new Command("", this, actionLoginGoogle, null);
        cmdLoginGoogle.setPos(tfPass.x + 120, tfPass.y + 30, LoadImageInterface.imgIconGoogle0,
                LoadImageInterface.imgIconGoogle1);


    }

    public void perform(int idAction, Object p)
    {
        switch (idAction)
        {
            case actionLoginFacebook:
            case actionLoginGoogle:
                GameCanvas.getInstance().StartDglThongBao("Chức năng đang phát triển");
                break;
            case actionLogin:
                GameCanvas.menu.showMenu = false;
                doLogin();
                break;
            case actionRegister:
                confirmRegister();
                break;
            case comfirmRegister:
                doRegister();
                break;
            case term:
                Utils.openUrl(Constants.WEBSITE_URL + "about/term");
                break;
        }
    }

    public void switchToMe()
    {
        resetLogo();
        GameScreen.gH = GameCanvas.h;
        GameCanvas.getInstance().loadBG(0);
        super.switchToMe();

        GameCanvas.menu = new Menu();
        int isSoftKey = Rms.loadRMSInt("isSoftKey");
        if (isSoftKey <= 0)
        {
            Rms.saveRMSInt("isSoftKey", 1);
        }

        int sound = Rms.loadRMSInt("isSound");
        if (sound < 0)
        {
            Rms.saveRMSInt("isSound", 1);
        }

        Music.init();
        Music.play(Music.MLogin, 5);
        if (!isLogout) return;
        isLogout = false;
        GameCanvas.getInstance().gameScreen = null;
    }

    public static LoginScreen getInstance()
    {
        if (instance == null) {
            instance = new LoginScreen();
        }
        return instance;
    }

    private void confirmRegister()
    {
        if (tfUser.getText().equals("")) {
            GameCanvas.getInstance().startOKDlg(MResources.NOT_INPUT_USERNAME);
        } else if (tfPass.getText().equals("")) {
            GameCanvas.getInstance().startOKDlg(MResources.NOT_INPUT_PASS1);
        } else if (tfUser.getText().length() < 8) {
            GameCanvas.getInstance().startOKDlg(MResources.USERNAME_LENGHT);
        } else {
            char[] ch = tfUser.getText().toCharArray();
            for (int i = 0; i < ch.length; i++)
            {
                if (TField.setNormal(ch[i])) continue;
                GameCanvas.getInstance().startOKDlg(MResources.NOT_SPEC_CHARACTER);
                return;
            }

            GameCanvas.msgdlg.setInfo(
                    MResources.REGISTER_TEXT[0] + " " + tfUser.getText() + " " + MResources.REGISTER_TEXT[1],
                    new Command(MResources.ACCEPT, this, comfirmRegister, null),
                    new Command("Điều Khoản", this, term, null),
                    new Command(MResources.NO, GameCanvas.instance, GameCanvas.END_DIALOG, null));

            GameCanvas.msgdlg.isPaintCham = false;
            GameCanvas.getInstance().currentDialog = GameCanvas.msgdlg;
        }
    }

    private void doRegister()
    {
        GameCanvas.getInstance().disconnect();

        _connectAction = REGISTER_ACTION;
        _user = tfUser.getText().trim();
        _pass = tfPass.getText().trim();

        boolean isConnectSuccess = GameCanvas.getInstance().connect();
        if (!isConnectSuccess) {
            _connectAction = null;
        } else {
            GameCanvas.getInstance().startWaitDlg(MResources.REGISTERING);
        }

    }

    private void doLogin()
    {
        GameCanvas.getInstance().disconnect();

        _connectAction = LOGIN_ACTION;
        _user = tfUser.getText().trim();
        _pass = tfPass.getText().trim();

        SavePass(_user, _pass);

        if (_user.equals("") || _pass.equals(""))
        {
            GameCanvas.getInstance().StartDglThongBao("Xin nhập email và password");
            if (_pass.equals("")) {
                tfUser.isFocus = false;
                tfPass.isFocus = true;
            }
            return;
        }
        byte[] listauto = Rms.loadRMS(Rms.autoSetting);
        if (listauto == null || listauto.length < SettingScreen.defaultAuto.length)
        {
            Rms.saveRMS(Rms.autoSetting, SettingScreen.defaultAuto);
        }
        else
        {
            GameScreen.isAutoDanh = listauto[SettingScreen.AUTO_DANH] == 1;
//            GameScreen.isAutoNhatItem = listauto[SettingScreen.AUTO_NHAT] == 1;
        }


        isLoggingIn = true;

        boolean isConnectSuccess = GameCanvas.getInstance().connect();
        if (!isConnectSuccess)
        {
            _connectAction = null;
        }
        else
        {
            GameCanvas.getInstance().startWaitLoginDialog();
        }
    }

    private static void SavePass(String user, String pass)
    {
        Rms.saveRMSInt("check", 1);
        Rms.saveRMSString("acc", user);
        Rms.saveRMSString("pass", pass);
    }

    public void update()
    {
        updateKey();
        if (connectCallBack && !isConnect)
        {
            isConnect = connectCallBack = false;
            GameCanvas.getInstance().startOKDlg(!isCloseConnect ? MResources.SERVER_MAINTENANCE : "Mất kết nối với máy chủ. Vui lòng thử lại !");
            isCloseConnect = false;
        }

        if (GameCanvas.imgCloud == null && GameCanvas.gameTick % 2 == 0 && GameCanvas.getInstance().imgBG != null &&
                GameCanvas.getInstance().imgBG[0] == null)
        {
            GameCanvas.getInstance().loadBG(0);
        }

        GameScreen.cmx++;
        if (GameScreen.cmx > GameCanvas.w * 3 + 100) {
            GameScreen.cmx = 100;
        }

        tfUser.update();
        tfPass.update();

        UpdateLogo();
        if (g >= 0)
        {
            ylogo += dir * g;
            g += dir * v;
            if (g <= 0)
                dir *= -1;
            if (ylogo > 0)
            {
                dir *= -1;
                g -= 2 * v;
            }
        }
        if (tipid >= 0 && GameCanvas.gameTick % 100 == 0)
        {
            tipid++;
            if (tipid >= MResources.tips.length) tipid = 0;
            _currentTip = FontManager.getInstance().tahoma_7_white.splitFontArray(MResources.tips[tipid], GameCanvas.w - 40);
        }
    }

    private void UpdateLogo() {
        if (defYL != yL) {
            yL += (defYL - yL) >> 1;
        }
    }

    public void keyPress(int keyCode)
    {
        if (tfUser.isFocus) {
            tfUser.keyPressed(keyCode);
        } else if (tfPass.isFocus) {
            tfPass.keyPressed(keyCode);
        } else if (isRes && tfRegPass.isFocus) {
            tfRegPass.keyPressed(keyCode);
        } else if (tfIp.isFocus) {
            tfIp.keyPressed(keyCode);
        }

        super.keyPress(keyCode);
    }

    public void paint(MGraphics g)
    {
        try
        {
            g.setColor(0x3aadfe);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.drawRegionScale(imgBgLogin, 0, 0, imgBgLogin.getWidth(), imgBgLogin.getHeight(), 0,
                    GameCanvas.w / 2, GameCanvas.h / 2, MGraphics.VCENTER | MGraphics.HCENTER);
            GameCanvas.resetTrans(g);
            if (GameCanvas.getInstance().currentDialog == null)
            {
                Paint.SubFrame(GameCanvas.hw - 85, yt - 60, 170, 160, g);

                if (imgTitle != null)
                {
                    g.drawImage(imgTitle, GameCanvas.hw, yt - 66, 3);
                }
                FontManager.getInstance().tahoma_7_white.drawString(g, "Email", tfUser.x, tfUser.y - 12, 0);
                FontManager.getInstance().tahoma_7_white.drawString(g, "Password", tfPass.x, tfPass.y - 12, 0);
                tfUser.paint(g);
                tfPass.paint(g);
                g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
            }
            else
            {
                if (_currentTip != null)
                {
                    for (int i = 0; i < _currentTip.length; i++)
                    {
                        FontManager.getInstance().tahoma_7_white.drawString(g, _currentTip[i], GameCanvas.w / 2,
                                tfUser.y - 10 + 10 * i, 2);
                    }
                }
            }

            String s = GameMidlet.VERSION;
            if (isLoggingIn)
            {
                s = Session.getInstance().getByteCount();
            }
            FontManager.getInstance().tahoma_7_grey.drawString(g, s, GameCanvas.w - 5, 5, 1);
            if (GameCanvas.getInstance().currentDialog == null)
            {
                cmdLoginFB.paint(g);
                cmdLoginGoogle.paint(g);
                cmdLogin.paint(g);
                cmdRes.paint(g);
            }

            super.paint(g);
        }
        catch (Exception e)
        {
            Log.e(LoginScreen.class.getName(), "Failed to paint", e);
        }

    }

    public void updateKey()
    {
        if (getCmdPointerLast(cmdLoginFB))
        {
            cmdLoginFB.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdLoginGoogle))
        {
            cmdLoginGoogle.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdLogin))
        {
            cmdLogin.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdRes))
        {
            cmdRes.performAction();
            GameCanvas.clearPointerEvent();
        }

        super.updateKey();
        GameCanvas.clearKeyPressed();
    }

    private void resetLogo()
    {
        yL = -50;
    }

    public String getAction()
    {
        return _connectAction;
    }

    public String getUser()
    {
        return _user;
    }

    public String getPass()
    {
        return _pass;
    }

    public void clear()
    {
        _connectAction = null;
        _user = null;
        _pass = null;
    }

}
