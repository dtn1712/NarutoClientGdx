package screen.old;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import model.Screen;
import lib.Bitmap;
import lib.Rms;

public class SplashScr extends Screen{
	public static Bitmap imgLogo;
	public static int splashScrStat;
	public static SplashScr instance;
	static int time = 0;
	public SplashScr() {
		instance = this;
	}

	public static void loadSplashScr() {
		splashScrStat = 0;
//		if (!LoginScr.isAutoLogin)
			time = 50;
		
	}

	public void update() {
//
		if (splashScrStat >= time) {
			if (Rms.loadRMSInt("indLanguage") >= 0) {
				GameCanvas.loginScr.switchToMe();
//				CreatCharScr.getInstance().switchToMe();
			} else {

				GameCanvas.loginScr.switchToMe();
//				GameCanvas.languageScr.switchToMe();
			}
		}
		splashScrStat++;

	}

	public void paint(MGraphics g) {
		g.setColor(0);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		
		if (imgLogo != null)
			g.drawImage(imgLogo, GameCanvas.w >> 1, GameCanvas.h >> 1, MGraphics.HCENTER | MGraphics.VCENTER);
		
		

	}
}
