package screen.old;

import real.Service;
import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Char;
import lib.Bitmap;
import lib.Cout;
import lib.TField;
import model.Command;
import model.IActionListener;
import model.Paint;
import model.Part;
import model.Screen;
import model.SmallImage;
import model.mResources;

public class CreatCharScr extends Screen implements IActionListener {
	
	public static CreatCharScr instance;
	public static String[] typeHero =  new String[] {"Hunter","Ninja"};
	public static String[] typeCountry = new String[] {"Hỏa Quốc" ,"Thủy Quốc" ,"Lôi Quốc" ,"Mộc Quốc" ,"Thổ Quốc"};

	public static CreatCharScr gI() {
		if (instance == null) {
			instance = new CreatCharScr();
		}
		return instance;
	}
	public final int NAM = 10;
	public final int NU = 111;
	public static TField tAddName;
	Command male,female;
	public static int indexHair, selected, indexBody, indexleg;
	public byte indexGender,indexCountry,indexClass;
	public static Bitmap imgItem,characterbar,gender_bar;
	public static Bitmap[] gender = new Bitmap[4];
	public int[][] xyItemClass = new int[3][];
	public int[][] xyItemQuocGia = new int[5][];
	public int[][] xyItemInfo = new int[2][];
	public int[] hKhung = new int[3];
	public int wpaintKhung = 110,yKhung1,yKhung2,xkhungTrai=15,sizeItem = 26;

	private static int[][] hairID = { { 0, 26, 27, 28 }, { 3, 23, 24, 25 } }; // Danh
	private static int[] defaultLeg = { 2, 5 }, defaultBody = { 1, 4 }; // Quần
	public int[][][] idPartHoa,idPartThuy,idPartTho,idPartLoi,idPartPhong;
 

	public CreatCharScr() {
		// TODO Auto-generated constructor stub
//		GameScr.getInstance().readPart();// đọc part
//		SmallImage.init();// đọc dữ liệu hình ảnh
		//Service.getInstance().requestImage(id);
		hKhung[0] = 60;
		hKhung[1] = 90;
		hKhung[2] = 160;
		
		if (GameCanvas.w == 128) {
			GameScr.setPopupSize(128, 120);
			GameScr.popupX = (GameCanvas.w - 128) / 2;
			GameScr.popupY = 0;
		}
		else {
			GameScr.setPopupSize(170, 190);
			GameScr.popupX = (GameCanvas.w - 170) / 2;
			GameScr.popupY = (GameCanvas.h - 220) / 2;
		}
		
		tAddName = new TField();
		tAddName.isPaintImg = true;
//		tAddName.name = mResources.CHARNAME;
		tAddName.y = GameCanvas.h-30;
		tAddName.width = 100;

		if(GameCanvas.w==128)
			tAddName.width = 60;

		tAddName.x = GameCanvas.w/2 -tAddName.width/2+5;//GameCanvas.w/2, GameCanvas.h-20
		tAddName.height = 26;
		tAddName.setIputType(TField.INPUT_TYPE_ANY);
		indexHair = 0;
		yKhung1 = (GameCanvas.h/6+240)>=GameCanvas.h?GameCanvas.h-240:GameCanvas.h/6;
		Cout.println("yKhung1  "+yKhung1);
//		center = new Command(mResources.NEWCHAR, this,8000,null);
		male = new Command("", this, NAM, null);
		male.setPos(xkhungTrai+wpaintKhung/2-30, yKhung1, gender[0], gender[1]);
		female = new Command("", this, NU, null);
		female.setPos(xkhungTrai+wpaintKhung/2+5, yKhung1, gender[3], gender[2]);
		male.w = female.w = 25;
		for (int i = 0; i < xyItemClass.length; i++) {
			xyItemClass[i] = new int[2];
			xyItemClass[i][0] = xkhungTrai+3+sizeItem*(i)+(i+1)*(wpaintKhung-sizeItem*xyItemClass.length)/(xyItemClass.length+1);
			xyItemClass[i][1] = yKhung1+62;
		}
		for (int i = 0; i < xyItemQuocGia.length; i++) {
			xyItemQuocGia[i] = new int[2];
			xyItemQuocGia[i][0] = xkhungTrai+3+sizeItem*(i%3)+(i%3+1)*(wpaintKhung-sizeItem*(i<=2?3:2))/((i<=2?3:2)+1);
			xyItemQuocGia[i][1] = yKhung1+hKhung[0]+72+(i/3)*(30);
		}
		for (int i = 0; i < xyItemInfo.length; i++) {
			xyItemInfo[i] = new int[2];
			xyItemInfo[i][0] = GameCanvas.w-wpaintKhung-xkhungTrai+4;
			xyItemInfo[i][1] = yKhung1+hKhung[0]+40+(i)*(45);
		}
		center = null;
		left = new Command(mResources.BACK, this,8001,null);
		right = new Command(mResources.NEWCHAR, this, 8000, null);
		
//		SmallImage.init();
		idPartHoa = new int[][][]{ 
			new int[][]{ //ao thuat
				new int[]{54, 55, 56}, 
				new int[]{96, 97, 98}, 
			},
			new int[][]{ //the thuat
				new int[]{6,7,8}, 
				new int[]{51, 52, 53}, 
			},
			new int[][]{ //nhan thuat
				new int[]{60, 61, 62}, 
				new int[]{45, 46, 47}, 
			},
	};
	idPartThuy = new int[][][]{ 
		new int[][]{ //ao thuat
			new int[]{48, 49, 50}, 
			new int[]{9, 10, 11}, 
		},
		new int[][]{ //the thuat
			new int[]{69,70,71}, 
			new int[]{30,31,32}, 
		},
		new int[][]{ //nhan thuat
			new int[]{33,34,35}, 
			new int[]{45, 56, 47}, 
		},
		
	};
	idPartTho = new int[][][]{ 
		new int[][]{ //ao thuat
			new int[]{39, 40, 41}, 
			new int[]{36, 37, 38}, 
		},
		new int[][]{ //the thuat
			new int[]{102,103,104}, 
			new int[]{24,25,26}, 
		},
		new int[][]{ //nhan thuat
			new int[]{48, 49, 50}, 
			new int[]{45, 46, 47}, 
		},
		
	};
	idPartLoi = new int[][][]{ 
		new int[][]{ //ao thuat
			new int[]{69,70,71}, 
			new int[]{81, 82, 83}, 
		},
		new int[][]{ //the thuat
			new int[]{0,1,2}, 
			new int[]{96,97,98}, 
		},
		new int[][]{ //nhan thuat
			new int[]{33, 34, 35}, 
			new int[]{9, 10, 11}, 
		},
		
	};
	idPartPhong = new int[][][]{ 
		new int[][]{ //ao thuat
			new int[]{39, 40, 41}, 
			new int[]{81, 82, 83}, 
		},
		new int[][]{ //the thuat
			new int[]{78,79,80}, 
			new int[]{96, 97, 98}, 
		},
		new int[][]{ //nhan thuat
			new int[]{90, 91, 92}, 
			new int[]{24,25,26}, 
		},
		
	};
	}
	public static void loadImage(){
		//imgItem,characterbar,gender_bar;
		for (int i = 0; i < gender.length; i++) {
			gender[i] = GameCanvas.loadImage("/GuiNaruto/createChar/gender"+(i+1)+".png");
		}
		imgItem =  GameCanvas.loadImage("/GuiNaruto/createChar/itemCreateC.png");
		characterbar =  GameCanvas.loadImage("/GuiNaruto/createChar/characterbar.png");
		gender_bar =  GameCanvas.loadImage("/GuiNaruto/createChar/gender_bar.png");
	}

	public void switchToMe() {
//		GameScr.getInstance().readPart();
//		SmallImage.init();
//		indexGender = GameCanvas.gameTick % 2;
		tAddName.setText("");
		indexCountry = 0;
		indexClass = 0;
		male.performAction();
		ResetPart();
//		indexHair = GameCanvas.gameTick % 4;
		super.switchToMe();

	}

	public void keyPress(int keyCode) {
		tAddName.keyPressed(keyCode);
	}

	public void update() {
		GameScr.cmx++;
		if (GameScr.cmx > GameCanvas.w * 3 + 100)
			GameScr.cmx = 100;
		if(GameCanvas.isTouch && GameCanvas.w >= 320){
			if(left != null){
				left.x = GameCanvas.w/2 - 160;
				left.y = GameCanvas.h - 26;
			}
			if(center != null){
				center.x = GameCanvas.w/2 - 35;
				center.y = GameCanvas.h - 26;
			}
			
			if(right!= null){
				right.x = GameCanvas.w/2 + 88;
				right.y = GameCanvas.h - 26;
			}
		}
	}

	int curIndex = 0;

	public void updateKey() {
		if (GameCanvas.keyPressed[2]) {
			selected--;
			if (selected < 0)
				selected = mResources.MENUNEWCHAR.length - 1;
		}
		if (GameCanvas.keyPressed[8]) {
			selected++;
			if (selected >= mResources.MENUNEWCHAR.length)
				selected = 0;
		}

		if (selected == 0)// Name
		{
//			right = tAddName.cmdClear;
			tAddName.update();
		}
		if (selected == 1) { // Gender
			if (GameCanvas.keyPressed[4]) {
				indexGender--;
				if (indexGender < 0)
					indexGender = (byte)(mResources.MENUGENDER.length - 1);
			}
			if (GameCanvas.keyPressed[6]) {

				indexGender++;
				if (indexGender > mResources.MENUGENDER.length - 1)
					indexGender = (byte)0;
			}
			right = null;
		}
//		if (selected == 2)// Hair
//		{
//			if (GameCanvas.keyPressed[4]) {
//				indexHair--;
//				if (indexHair < 0)
//					indexHair = mResources.HAIR_STYLE[0].length - 1;
//			}
//			if (GameCanvas.keyPressed[6]) {
//				indexHair++;
//				if (indexHair > mResources.HAIR_STYLE[0].length - 1)
//					indexHair = 0;
//			}
//		}
		
		if(selected == 2) // Loại 
		{
			if(GameCanvas.keyPressed[4]){
				indexClass--;
				if(indexClass < 0)
					indexClass = (byte)(typeHero.length - 1);
			}
			
			if (GameCanvas.keyPressed[6]) {
				indexClass++;
				if (indexClass > typeHero.length - 1)
					indexClass = 0;
			}
		}
		if(selected == 3)
		{
			if(GameCanvas.keyPressed[4]){
				indexCountry--;
				if(indexCountry < 0)
					indexCountry = (byte)(typeCountry.length - 1);
			}
			
			if (GameCanvas.keyPressed[6]) {
				indexCountry++;
				if (indexCountry > typeCountry.length - 1)
					indexCountry = 0;
			}
		}
		
		
		if (GameCanvas.isPointerJustRelease) {
			if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 65, GameScr.popupW - 5, ITEM_HEIGHT))
				selected = 0;

			if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 106, GameScr.popupW - 5, ITEM_HEIGHT)) {
				curIndex = 1;
				if (curIndex == selected) {
					indexGender--;
					if (indexGender < 0)
						indexGender = (byte)(mResources.MENUGENDER.length - 1);
				} else
					selected = 1;

			}
			if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 150, GameScr.popupW - 5, ITEM_HEIGHT)) {
				curIndex = 2;
				if (curIndex == selected) {
					indexClass++;
					if (indexClass > typeHero[0].length() - 1)
						indexClass = 0;
				} else
					selected = 2;
			}
		}
		for (int i = 0; i < xyItemClass.length; i++) {
			if(GameCanvas.isPointerClick&&GameCanvas.isPoint(xyItemClass[i][0], xyItemClass[i][1], sizeItem, sizeItem)){
				indexClass = (byte)i;
				ResetPart();
				GameCanvas.clearPointerEvent();
			}
		}
		//indexCountry
		for (int i = 0; i < xyItemQuocGia.length; i++) {
			if(GameCanvas.isPointerClick&&GameCanvas.isPoint(xyItemQuocGia[i][0], xyItemQuocGia[i][1], sizeItem, sizeItem)){
				indexCountry = (byte)i;
				ResetPart();
				GameCanvas.clearPointerEvent();
			}
		}
		if(Screen.getCmdPointerLast(male)){
			GameCanvas.isPointerJustRelease = false;
			male.performAction();
		}
		if(Screen.getCmdPointerLast(female)){
			GameCanvas.isPointerJustRelease = false;
			female.performAction();
		}
		super.updateKey();
		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
		return;
	}
	public void ResetPart(){
		switch (indexCountry) {
		case 0: //hoa
			indexHair = idPartHoa[indexClass][indexGender][0];
			indexBody = idPartHoa[indexClass][indexGender][1];
			indexleg  = idPartHoa[indexClass][indexGender][2];
			break;
		case 1: //thuy
			indexHair = idPartThuy[indexClass][indexGender][0];
			indexBody = idPartThuy[indexClass][indexGender][1];
			indexleg  = idPartThuy[indexClass][indexGender][2];
			break;
		case 2: //tho
			indexHair = idPartThuy[indexClass][indexGender][0];
			indexBody = idPartThuy[indexClass][indexGender][1];
			indexleg  = idPartThuy[indexClass][indexGender][2];
			break;
		case 3: //loi
			indexHair = idPartLoi[indexClass][indexGender][0];
			indexBody = idPartLoi[indexClass][indexGender][1];
			indexleg  = idPartLoi[indexClass][indexGender][2];
			break;
		case 4: //phong
			indexHair = idPartPhong[indexClass][indexGender][0];
			indexBody = idPartPhong[indexClass][indexGender][1];
			indexleg  = idPartPhong[indexClass][indexGender][2];
			break;

		default:
			break;
		}
	}
	public void paint(MGraphics g) {
		int line = 0;
		try{
			//GameScr.getInstance().readPart();
			GameCanvas.paintBGGameScr(g,true);
			line = 1;
	
			//Paint.paintFrame(GameScr.popupX, GameScr.popupY, GameScr.popupW, GameScr.popupH, graphic);
			int r=40;
			if(GameCanvas.w==128)r=20;
			line = 2;
//			int hair = hairID[indexGender][indexHair];
//			int indexHair = hairID[0][0];
//			int leg = defaultLeg[indexGender][in]
			
//			int indexleg = defaultLeg[0];
//			int indexBody = defaultBody[0];
//			int leg = 0;
//			int body = 1;
			line = 3;
//			System.out.println("Gamsr "+GameScr.parts.length+"hair ----> "+hair +" "+leg+" "+body);
			g.drawImage(loadImageInterface.imgRock, GameCanvas.w/2,
					GameCanvas.h-10-loadImageInterface.imgRock.getHeight()/2, MGraphics.VCENTER| MGraphics.HCENTER);

					
//			System.out.println("Part ---> "+ph +" "+pl+" "+pb);
			int cx = GameCanvas.w / 2, cy = GameCanvas.h-loadImageInterface.imgRock.getHeight()/2 - 30;
			line = 4;
//			System.out.println(ph + "," + pl + "," + pb);
			try {

				Part ph = GameScr.parts[indexHair], pl = GameScr.parts[indexleg], pb = GameScr.parts[indexBody];

				SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id, cx
						+ Char.CharInfo[0][0][1] + ph.pi[Char.CharInfo[0][0][0]].dx, cy
						- Char.CharInfo[0][0][2] + ph.pi[Char.CharInfo[0][0][0]].dy, 0,
						0);
				SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[0][1][0]].id, cx
						+ Char.CharInfo[0][1][1] + pl.pi[Char.CharInfo[0][1][0]].dx, cy
						- Char.CharInfo[0][1][2] + pl.pi[Char.CharInfo[0][1][0]].dy, 0,
						0);
				SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[0][2][0]].id, cx
						+ Char.CharInfo[0][2][1] + pb.pi[Char.CharInfo[0][2][0]].dx, cy
						- Char.CharInfo[0][2][2] + pb.pi[Char.CharInfo[0][2][0]].dy, 0,
						0);
			} catch (Exception e) {
				// TODO: handle exception
			}
//			line = 5;
//			for (int i = 0; i < mResources.MENUNEWCHAR.length; i++) {
//				if (selected == i) {
//	
//					SmallImage.drawSmallImage(graphic, 989, GameScr.popupX + 10 + (GameCanvas.gameTick % 7 > 3 ? 1 : 0), GameScr.popupY + 76 + i * r, 2, StaticObj.VCENTER_HCENTER);
//					SmallImage.drawSmallImage(graphic, 989, GameScr.popupX + GameScr.popupW - 15 - (GameCanvas.gameTick % 7 > 3 ? 1 : 0), GameScr.popupY + 76 + i * r, 0, StaticObj.VCENTER_HCENTER);
//	
//				}
//	
//				mFont.tahoma_7b_white.drawString(graphic, mResources.MENUNEWCHAR[i], GameScr.popupX + 20, GameScr.popupY + 70 + i * r, 0);
//	
//			}
//			line = 6;
//			mFont.tahoma_7b_white.drawString(graphic, mResources.MENUGENDER[indexGender], GameScr.popupX + 90, GameScr.popupY + 70 + 1 * r, mFont.CENTER);
////			mFont.tahoma_7b_white.drawString(graphic, mResources.HAIR_STYLE[indexGender][indexHair], GameScr.popupX + 85, GameScr.popupY + 70 + 2 * r, mFont.CENTER);
//			mFont.tahoma_7b_white.drawString(graphic, typeHero[indexType],GameScr.popupX + 100, GameScr.popupY + 70 + 2 * r, mFont.CENTER);
//			mFont.tahoma_7b_white.drawString(graphic, typeCountry[indexCountry],GameScr.popupX + 100, GameScr.popupY + 70 + 3 * r, mFont.CENTER);
//			line = 7;
			g.drawImage(gender_bar, xkhungTrai+wpaintKhung/2, male.y+male.img.getHeight()/2, MGraphics.VCENTER| MGraphics.HCENTER);
			Paint.SubFrame(xkhungTrai, male.y+40, wpaintKhung, hKhung[0], g,0xff037ddb,80);
			mFont.tahoma_7_white.drawString(g, "Class",xkhungTrai+wpaintKhung/2, male.y+45, mFont.CENTER);
			for (int i = 0; i < xyItemClass.length; i++) {
				g.drawImage(imgItem, xyItemClass[i][0],  xyItemClass[i][1], MGraphics.TOP| MGraphics.LEFT);
				if(i==indexClass)
					g.drawImage(loadImageInterface.imgFocusSelectItem,
							xyItemClass[i][0]+imgItem.getWidth()/2,  xyItemClass[i][1]+imgItem.getHeight()/2, MGraphics.VCENTER| MGraphics.HCENTER);
					
			}
			Paint.SubFrame(xkhungTrai, male.y+110, wpaintKhung, hKhung[1], g,0xff037ddb,80);
			for (int i = 0; i < xyItemQuocGia.length; i++) {
				g.drawImage(imgItem, xyItemQuocGia[i][0],  xyItemQuocGia[i][1], MGraphics.TOP| MGraphics.LEFT);
				if(i==indexCountry)
					g.drawImage(loadImageInterface.imgFocusSelectItem,
							xyItemQuocGia[i][0]+imgItem.getWidth()/2,  xyItemQuocGia[i][1]+imgItem.getHeight()/2, MGraphics.VCENTER| MGraphics.HCENTER);
				
			}
			mFont.tahoma_7_white.drawString(g, "Quốc gia",xkhungTrai+wpaintKhung/2, male.y+115, mFont.CENTER);
			//khung thong tin
			Paint.SubFrame(GameCanvas.w-wpaintKhung-xkhungTrai,male.y+40, wpaintKhung, hKhung[2], g,0xff037ddb,80);
			mFont.tahoma_7_white.drawString(g, "Gender",GameCanvas.w-wpaintKhung/2-xkhungTrai,
					male.y+45, mFont.CENTER);
			if(gender[0]!=null)
				g.drawImage(indexGender==0?gender[0]:gender[2], GameCanvas.w-wpaintKhung/2-xkhungTrai,male.y+75, MGraphics.VCENTER| MGraphics.HCENTER);
			male.paint(g);
			for (int i = 0; i < xyItemInfo.length; i++) {
				g.drawImage(imgItem, xyItemInfo[i][0],  xyItemInfo[i][1], MGraphics.TOP| MGraphics.LEFT);
			}
			female.paint(g);
			g.drawImage(characterbar, GameCanvas.w/2, GameCanvas.h-20, MGraphics.VCENTER| MGraphics.HCENTER,true);
			if(tAddName.isFocus)
				tAddName.paint(g);
			else mFont.tahoma_7b_white.drawString(g, tAddName.getText(),GameCanvas.w/2, GameCanvas.h-25, 2);
//			super.paint(graphic);
			g.setColor(0);
			// graphic.fillRect(GameScr.popupX + 5,
			// GameScr.popupY + 65, GameScr.popupW - 5, ITEM_HEIGHT);
		}catch (Exception e) {
			//System.out.println("CreateCharScr.paint(): " + line);
		}
		super.paint(g);
	}

	public void perform(int idAction, Object p) {
		switch (idAction) {
		case 8000:
//			Service.getInstance().createChar(tAddName.getText(), indexGender, hairID[indexGender][indexHair]);
			Service.gI().createChar(indexClass, indexCountry, indexGender, tAddName.getText());
			break;
		case 8001:
			GameCanvas.loadBG(1);
			LoginScr.selectCharScr.switchToMe();
			break;
		case NAM:
			female.img = gender[3];
			female.imgFocus = gender[2];
			male.img = gender[0];
			male.imgFocus = gender[1];
			indexGender = 0;
			ResetPart();
			break;
		case NU:
			female.img = gender[2];
			female.imgFocus = gender[3];
			male.img = gender[1];
			male.imgFocus = gender[0];
			indexGender = 1;
			ResetPart();
			break;
		default:
			break;
		}
		
	}

	public void perform() {
		// TODO Auto-generated method stub
		
	}

}
