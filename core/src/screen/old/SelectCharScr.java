package screen.old;

import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Char;
import lib.Cout;
import lib.Session_ME;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Part;
import model.Screen;
import model.SmallImage;
import model.mResources;

public class SelectCharScr extends Screen implements IActionListener {
	public static SelectCharScr instance;

	public static int w1char, h1char, padchar, x, y, indexSelect, parthead[], partleg[], partbody[], partWp[], level[];
	public static String[] phai;
	public static String[] name ;
	public static int[] charIDDB;
	public int[] part;
	public static byte[] gender, type;
	public static GameScr gameScr;

	public static SelectCharScr gI() {
		if (instance == null) {
			instance = new SelectCharScr();
		}
		return instance;
	}

	public void initSelectChar() {
		charIDDB = new int[3];
		name = new String[3];
		parthead = new int[3];
		partleg = new int[3];
		partbody = new int[3];
		partWp = new int[3];
		level = new int[3];
		phai = new String[3];
		gender = new byte[3];
		type = new byte[3];
		if (GameCanvas.isTouch)
			indexSelect = -1;
		else
			indexSelect = 0;
		GameScr.readPart();// đọc part
		SmallImage.init();// đọc dữ liệu hình ảnh
	}

//	Command cmdSelect;

	private int waitToPerform;

	public SelectCharScr() {
	
		w1char = 48;
		h1char = 85;
		if (GameCanvas.w < 160)
			w1char = 32;
		padchar = 7;
		x = (GameCanvas.w - (3 * w1char) >> 1) - 5;
		y = (GameCanvas.hh) - (h1char >> 1) + 10;
		if (GameCanvas.isTouch && GameCanvas.w > 200) {
			w1char = 74;
			padchar = 25;
			h1char = 110;
			x = (GameCanvas.w - (3 * w1char) >> 1) - 20;
			y = (GameCanvas.hh) - (h1char >> 1);

			if (GameCanvas.w < 320) {
				padchar = 6;
				x = (GameCanvas.w - (3 * w1char) >> 1) - 6;
			}
		}
		Cout.println(getClass(), "  yuyyy   "+y);
		y = GameCanvas.h- MGraphics.getImageHeight(loadImageInterface.imgTatus);
		Cout.println(getClass(), "  yuyyy   "+y);
		left = null;
		// left = new Command(Resources.DELETE, new IAction() {
		// public void perform() {
		// GameCanvas.startYesNoDlg("Bạn có chắc muốn xóa nhân vật này không",
		// new IAction() {
		// public void perform() {
		// GameCanvas.endDlg();
		// }
		// }, new IAction() {
		// public void perform() {
		// GameCanvas.endDlg();
		// }
		// });
		// }
		// });
//		cmdSelect = new Command(mResources.SELECT, this, 1000, null);
		center = new Command("", this, 1000, null);
		right = new Command(mResources.EXIT, this, 1001, null);
		right.setPos( GameCanvas.w/2 + 200, GameCanvas.h - 26, loadImageInterface.img_use ,loadImageInterface.img_use_focus);

	
//		if (GameCanvas.isTouch) {
//			center = null;
//			left = null;
//		}
//		if(GameCanvas.isTouch && GameCanvas.w >= 320){
//			right.x = GameCanvas.w/2 + 88;
//			right.y = GameCanvas.h - 26;
//		}
		
		
//		gameScr = new GameScr();

	}

	protected void doSelect() {
		if (name[indexSelect] != null) {
//			Service.getInstance().selectCharToPlay(name[indexSelect]);
//			Service.getInstance().selectChar(charIDDB[indexSelect]);
//			GameCanvas.startWaitDlg(mResources.PLEASEWAIT);
			CreatCharScr.gI().switchToMe();
//			GameCanvas.isLoading = true;
			
		} else
			CreatCharScr.gI().switchToMe();
	}

	public void updateKey() {
		super.updateKey();
//		if (GameCanvas.currentDialog != null)
//			return;

		if (GameCanvas.keyPressed[6]) {
			indexSelect++;
			if (indexSelect >= 3)
				indexSelect = 0;
		}
		if (GameCanvas.keyPressed[4]) {
			indexSelect--;
			if (indexSelect < 0)
				indexSelect = 2;
		}

		if (GameCanvas.isPointerDown) {
			if (GameCanvas.isPointerHoldIn(x, y + 140, 3 * (w1char + padchar), h1char)) {
				int index = (GameCanvas.px - x) / (w1char + padchar);
				if (index > 2)
					index = 2;
				if (index < 0)
					index = 0;
				indexSelect = index;
			}
		}
		if (GameCanvas.isPointerJustRelease) {
			if (GameCanvas.isPointerHoldIn(x, y + 140, 3 * (w1char + padchar), h1char)) {
				waitToPerform = 5;
			} else
				indexSelect = -1;
		}
		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
	}
	public int dem = 0;
	public void update() {
		if(GameCanvas.imgCloud==null) GameCanvas.loadBG(1);
		GameScr.cmx++;
		dem++;
		if(dem >= 1000)
			dem = 0;
		if (GameScr.cmx > GameCanvas.w * 3 + 100)
			GameScr.cmx = 100;
		updateOpen();
		if (waitToPerform > 0) {
			waitToPerform--;
			if (waitToPerform == 0) {
				if (indexSelect >= 0)
					doSelect();
			}
		}
	}
	int gsgreenField1Y;
	public void switchToMe() {
//		TileMap.freeTilemap();
		GameCanvas.loadBG(1);
		gsgreenField1Y = GameScr.gH - Image.getHeight(loadImageInterface.imgTrangtri) + 170;
		System.gc();
		super.switchToMe();
	}

	public void paint(MGraphics g) {
//		GameCanvas.typeBg = 1;
//		SmallImage.readImage();
//		GameScr.getInstance().readPart();
//		graphic.setColor(0);
//		graphic.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paintBGGameScr(g);
//		parthead[0] = 4;
//		partleg[0] = 8;
//		partbody[0] = 9;
//		parthead[1] = 4;
//		partleg[1] = 8;
//		partbody[1] = 9;
//		parthead[2] = 4;
//		partleg[2] = 8;
//		partbody[3] = 9;
		g.drawImage(loadImageInterface.imgTatus,GameCanvas.w/2, y + 200, MGraphics.HCENTER| MGraphics.BOTTOM);
		
//		for(int i = GameCanvas.w/2; i < GameCanvas.w; i += (Image.getWidth(loadImageInterface.imgTrangtri)) ){
//			
//			graphic.drawImage(loadImageInterface.imgTrangtri, i, y + 200, MGraphics.BOTTOM | MGraphics.HCENTER);
//		}
		for (int i = -((GameScr.cmx >> 1) % Image.getWidth(loadImageInterface.imgTrangtri)); i < GameScr.gW; i += Image.getWidth(loadImageInterface.imgTrangtri)/*24*/)
			g.drawImage(loadImageInterface.imgTrangtri, i, gsgreenField1Y - 150, 0);
		for (int i = 0; i < 3; i++) {
			g.drawImage(loadImageInterface.imgRock, x + i * (w1char + padchar), y + 175,0);
//			if (indexSelect == i)
//				GameCanvas.paint.paintFrameInsideSelected(x + i * (w1char + padchar), y, w1char, h1char, graphic);
//			else
//				GameCanvas.paint.paintFrameInside(x + i * (w1char + padchar), y, w1char, h1char, graphic);
//
//			GameCanvas.paint.paintFrameBorder(x + i * (w1char + padchar), y, w1char, h1char, graphic);
		}
		
		for (int i = 0; i < 3; i++) {
			if (name[i] == null)
				continue;
		
//			System.out.println("PART ---> "+parthead[i]+" ,,, "+partleg[i]+" ,,, "+partbody[i]);
			//Part ph = GameScr.parts[parthead[i]], pl = GameScr.parts[partleg[i]], pb = GameScr.parts[partbody[i]], pwp = GameScr.parts[partWp[i]];
			Part ph = GameScr.parts[parthead[i]], pl = GameScr.parts[partleg[i]], pb = GameScr.parts[partbody[i]], pwp = GameScr.parts[partWp[i]];
//			Part ph = GameScr.parts[0], pl = GameScr.parts[2], pb = GameScr.parts[1]/*, pwp = GameScr.parts[-1]*/;
//			Part ph = GameScr.parts[4], pl = GameScr.parts[8], pb = GameScr.parts[9], pwp = GameScr.parts[-];
//			if (ph.pi == null || ph.pi.length < 8) {
//				ph = Char.myChar().getDefaultHead(gender[i]);
//			} else {
//				for (int j = 0; j < ph.pi.length; j++) {
//					if (ph.pi[j] == null || !SmallImage.isExitsImage(ph.pi[j].id)) {
//						ph = Char.myChar().getDefaultHead(gender[i]);
//						break;
//					}
//				}
//			}
			
			int cx = x + i * (w1char + padchar) + w1char / 2;
			int cy = 0;
			if (!GameCanvas.isTouch) {
				cy = y + (h1char / 2) + 16;
//				SmallImage.drawSmallImage(graphic, pwp.pi[Char.CharInfo[0][3][0]].id, cx + Char.CharInfo[0][3][1] + pwp.pi[Char.CharInfo[0][3][0]].dx, cy - Char.CharInfo[0][3][2] + pwp.pi[Char.CharInfo[0][3][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[dem % 2 > 0 ? 1 : 0][0][0]].id, cx + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][1] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dx, (cy - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dy) + 200, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[dem % 2 > 0 ? 1 : 0][1][0]].id, cx + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][1] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dx, (cy - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dy) + 200, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[dem % 2 > 0 ? 1 : 0][2][0]].id, cx + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dx, (cy - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dy) + 200, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][1] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][1] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pwp.pi[Char.CharInfo[0][3][0]].id, cx + Char.CharInfo[0][3][1] + pwp.pi[Char.CharInfo[0][3][0]].dx, cy - Char.CharInfo[0][3][2] + pwp.pi[Char.CharInfo[0][3][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[0][0][0]].id, cx + Char.CharInfo[0][0][1] + ph.pi[Char.CharInfo[0][0][0]].dx, cy - Char.CharInfo[0][0][2] + ph.pi[Char.CharInfo[0][0][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[0][2][0]].id, cx + Char.CharInfo[0][2][1] + pb.pi[Char.CharInfo[0][2][0]].dx, cy - Char.CharInfo[0][2][2] + pb.pi[Char.CharInfo[0][2][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[0][1][0]].id, cx + Char.CharInfo[0][1][1] + pl.pi[Char.CharInfo[0][1][0]].dx, cy - Char.CharInfo[0][1][2] + pl.pi[Char.CharInfo[0][1][0]].dy, 0, 0);
				if (indexSelect == i) {
					mFont.tahoma_8b.drawString(g, mResources.CHARINGFO[0] + ": " + name[i], GameCanvas.hw, y - 45, mFont.CENTER);
					mFont.tahoma_7b_white.drawString(g, mResources.CHARINGFO[1] + ": " + level[i], GameCanvas.hw, y - 28, 2, mFont.tahoma_7b_blue);
//					mFont.tahoma_7b_white.drawString(graphic, phai[i], GameCanvas.hw, y - 16, 2, mFont.tahoma_7b_blue);
				}

			} else {
				int a = GameCanvas.isTouchControlLargeScreen ? -25 : 16;
				cy = y + (h1char / 2) - 15;
				SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][0][0]].id, cx + Char.CharInfo[dem % 10 > 0 ? 1 : 0][0][1] + ph.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][0][0]].dx, (cy - Char.CharInfo[dem % 15 > 0 ? 1 : 0][0][2] + ph.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][0][0]].dy) + 140, 0, 0);
				SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][1][0]].id, cx + Char.CharInfo[dem % 10 > 0 ? 1 : 0][1][1] + pl.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][1][0]].dx, (cy - Char.CharInfo[dem % 15 > 0 ? 1 : 0][1][2] + pl.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][1][0]].dy) + 140, 0, 0);
				SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][2][0]].id, cx + Char.CharInfo[dem % 10 > 0 ? 1 : 0][2][1] + pb.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][2][0]].dx, (cy - Char.CharInfo[dem % 15 > 0 ? 1 : 0][2][2] + pb.pi[Char.CharInfo[dem % 15 > 0 ? 1 : 0][2][0]].dy) + 140, 0, 0);

//				SmallImage.drawSmallImage(graphic, pwp.pi[Char.CharInfo[0][3][0]].id, cx + Char.CharInfo[0][3][1] + pwp.pi[Char.CharInfo[0][3][0]].dx, cy - Char.CharInfo[0][3][2] + pwp.pi[Char.CharInfo[0][3][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[0][0][0]].id, cx + Char.CharInfo[0][0][1] + ph.pi[Char.CharInfo[0][0][0]].dx, cy - Char.CharInfo[0][0][2] + ph.pi[Char.CharInfo[0][0][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[0][1][0]].id, cx + Char.CharInfo[0][1][1] + pl.pi[Char.CharInfo[0][1][0]].dx, cy - Char.CharInfo[0][1][2] + pl.pi[Char.CharInfo[0][1][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[0][2][0]].id, cx + Char.CharInfo[0][2][1] + pb.pi[Char.CharInfo[0][2][0]].dx, cy - Char.CharInfo[0][2][2] + pb.pi[Char.CharInfo[0][2][0]].dy, 0, 0);
				
//				SmallImage.drawSmallImage(graphic, pwp.pi[Char.CharInfo[0][3][0]].id, cx + Char.CharInfo[0][3][1] + pwp.pi[Char.CharInfo[0][3][0]].dx, cy - Char.CharInfo[0][3][2] + pwp.pi[Char.CharInfo[0][3][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[0][0][0]].id, cx + Char.CharInfo[0][0][1] + ph.pi[Char.CharInfo[0][0][0]].dx, (cy - Char.CharInfo[0][0][2] + ph.pi[Char.CharInfo[0][0][0]].dy) + 135, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[0][1][0]].id, cx + Char.CharInfo[0][1][1] + pl.pi[Char.CharInfo[0][1][0]].dx, (cy - Char.CharInfo[0][1][2] + pl.pi[Char.CharInfo[0][1][0]].dy) + 135, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[0][2][0]].id, cx + Char.CharInfo[0][2][1] + pb.pi[Char.CharInfo[0][2][0]].dx, (cy - Char.CharInfo[0][2][2] + pb.pi[Char.CharInfo[0][2][0]].dy) + 135, 0, 0);
//				SmallImage.drawSmallImage(graphic, ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][1] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][1] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
//				SmallImage.drawSmallImage(graphic, pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].id, x + Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dx, y + a - Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[Char.myChar().cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);

				mFont.tahoma_8b.drawString(g, name[i], cx, y + (h1char / 2) + 55, mFont.CENTER);
				mFont.tahoma_7b_white.drawString(g, mResources.CHARINGFO[1] + ": " + level[i], cx, y + (h1char / 2) + 72, 2);
//				if (GameCanvas.w > 200)
//					mFont.tahoma_7b_white.drawString(graphic, phai[i], cx, y + (h1char / 2) + 34, 2);

			}
		}
		super.paint(g);

	}

	int moveUp = GameCanvas.h / 2 - 2, moveDow = GameCanvas.h / 2 + 2;
	boolean isstarOpen;

	public void updateOpen() {
		if (!isstarOpen)
			return;

		if (moveUp > -1) {
			moveUp -= 4;
		}
		if (moveDow < GameCanvas.h) {
			moveDow += 4;
		}
	}

	public void perform(int idAction, Object p) {
		switch (idAction) {
		case 1000:
			doSelect();
			break;
		case 1001:
			Session_ME.gI().close();
			GameCanvas.instance.resetToLoginScr();
			break;

		default:
			break;
		}
		
	}

	public void perform() {
		// TODO Auto-generated method stub
		
	}
}
