package screen;

import android.util.Log;

import Objectgame.Char;
import Objectgame.clan.Clan;
import Objectgame.clan.ClanMember;
import Objectgame.clan.ClanType;
import lib.FontManager;
import lib.LoadImageInterface;
import lib.Command;
import lib.Image;
import lib.MBitmap;
import model.IActionListener;
import model.Paint;
import model.Part;
import model.Screen;
import model.Scroll;
import model.SmallImage;
import real.Service;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import java.util.ArrayList;
import java.util.List;

public class ClanScreen extends Screen implements IActionListener{

	private static final int CLOSE_SCREEN = 99;

	private static final int GET_LOCAL_CLAN = 2;
	private static final int GET_GLOBAL_CLAN = 3;
	private static final int CONFIRM_LEAVE_CLAN = 4;
	private static final int LEAVE_CLAN = 5;
	private static final int CONFIRM_KICK_CLAN_MEMBER = 6;
	private static final int KICK_CLAN_MEMBER = 7;
	private static final int CONFIRM_DISBAND_CLAN = 8;
	private static final int DISBAND_CLAN = 9;
	private static final int GET_CLAN_INVITATION = 10;
	private static final int ACCEPT_INVITATION = 11;
	private static final int REJECT_INVITATION = 12;

	private static ClanScreen instance;
	private static int clickNumber;

	private static MBitmap imgClanName;
	public String clanLocalName, clanGlobalName;

	private String clanName;
	public Command cmdClanLocal, cmdClanGlobal, cmdListInvited;

	private Command cmdClose;
	private Command cmdKick;
	private Command cmdHostLeave;
	private Command cmdMemberLeave;
	private Command cmdDisband;
	private Command cmdAcceptInvite;
	private Command cmdRejectInvite;

	private Command selectedCommand;
	private ClanMember selectedClanMember;
	private Clan selectedClan;

	private int indexRow;

	public List<ClanMember> ListMemberGlobalClan = new ArrayList<ClanMember>();
	public List<ClanMember> ListMemberLocalClan = new ArrayList<ClanMember>();

	public List<Clan> ListLocalClan = new ArrayList<Clan>();
	public List<Clan> ListGlobalClan = new ArrayList<Clan>();

	public List<Clan> ListClanInvitations = new ArrayList<Clan>();

	private Scroll scrClan = new Scroll();

	public int wkhung, hKhung, xpaint, ypaint;


	private ClanScreen()
	{
		wkhung = 240;
		hKhung = 200;

		xpaint = GameCanvas.w / 2 - wkhung / 2;
		ypaint = GameCanvas.h / 2 - hKhung / 2 - 10;
		cmdClose = new Command("", this, CLOSE_SCREEN, null);
		cmdKick = new Command("Kích", this, CONFIRM_KICK_CLAN_MEMBER, null);
		cmdHostLeave = new Command("Rời", this, CONFIRM_LEAVE_CLAN, null);
		cmdMemberLeave = new Command("Rời", this, CONFIRM_LEAVE_CLAN, null);
		cmdDisband = new Command("Giải tán", this, CONFIRM_DISBAND_CLAN, null);
		cmdClanLocal = new Command("Quốc gia", this, GET_LOCAL_CLAN, null);
		cmdClanGlobal = new Command("Thế giới", this, GET_GLOBAL_CLAN, null);
		cmdListInvited = new Command("Mời bang", this, GET_CLAN_INVITATION, null);
		cmdAcceptInvite = new Command("Đồng ý", this, ACCEPT_INVITATION, null);
		cmdRejectInvite = new Command("Từ chối", this, REJECT_INVITATION, null);

		int wbutton = Image.getWidth(LoadImageInterface.btnTab);
		int hbutton = Image.getHeight(LoadImageInterface.btnTab);

		cmdKick.setPos(xpaint + wkhung / 2 - wbutton / 2,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		cmdMemberLeave.setPos(xpaint + wkhung / 2 - wbutton / 2,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		cmdHostLeave.setPos(xpaint + wkhung / 2 - 7 * wbutton / 4,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		cmdDisband.setPos(xpaint + wkhung / 2 + 3 * wbutton / 4,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		cmdAcceptInvite.setPos(xpaint + wkhung / 2 - 7 * wbutton / 4,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		cmdRejectInvite.setPos(xpaint + wkhung / 2 + 3 * wbutton / 4,
				ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);


		cmdClose.setPos(xpaint + wkhung - LoadImageInterface.closeTab.width / 2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);

		cmdClanLocal.setPos(xpaint + wkhung - 10,
				ypaint + hKhung / 2 - hbutton * 2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);

		cmdClanGlobal.setPos(xpaint + wkhung - 10,
				ypaint + hKhung / 2 - hbutton / 2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);

		cmdListInvited.setPos(xpaint + wkhung - 10,
				ypaint + hKhung / 2 + hbutton, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);

	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case CLOSE_SCREEN:
				GameScreen.getInstance().switchToMe();
				Clear();
				break;
			case GET_CLAN_INVITATION:
				cmdClanLocal.isFocus = false;
				cmdClanGlobal.isFocus = false;
				cmdListInvited.isFocus = true;
				Service.getInstance().GetClanInvitations();
				break;
			case GET_LOCAL_CLAN:
				cmdClanLocal.isFocus = true;
				cmdClanGlobal.isFocus = false;
				cmdListInvited.isFocus = false;
				if (Char.myChar().localClan == null)
				{
					Service.getInstance().GetListClan(ClanType.LOCAL.name());
				}
				else
				{
					Service.getInstance().GetClanInfo(Char.myChar().localClan.Id, ClanType.LOCAL.name());
				}
				break;
			case GET_GLOBAL_CLAN:
				cmdClanLocal.isFocus = false;
				cmdClanGlobal.isFocus = true;
				cmdListInvited.isFocus = false;
				if (Char.myChar().globalClan == null)
				{
					Service.getInstance().GetListClan(ClanType.GLOBAL.name());
				}
				else
				{
					Service.getInstance().GetClanInfo(Char.myChar().globalClan.Id, ClanType.GLOBAL.name());
				}
				break;
			case CONFIRM_LEAVE_CLAN:
				GameCanvas.startYesNoDlg("Bạn có muốn rời bang?",
						new Command("Có", this, LEAVE_CLAN, null),
						new Command("Không", GameCanvas.instance, GameCanvas.END_DIALOG, null));
				break;
			case LEAVE_CLAN:
				String leaveClanType = (cmdClanLocal.isFocus) ? ClanType.LOCAL.name() : ClanType.GLOBAL.name();
				long leaveClanId = (cmdClanLocal.isFocus) ? Char.myChar().localClan.Id : Char.myChar().globalClan.Id;
				Service.getInstance().LeaveClan(leaveClanType, leaveClanId, Char.myChar().charID);
				GameCanvas.getInstance().endDlg();
				GameScreen.getInstance().switchToMe();
				Clear();
				break;
			case CONFIRM_DISBAND_CLAN:
				GameCanvas.startYesNoDlg("Bạn có muốn giải tán bang?",
						new Command("Có", this, DISBAND_CLAN, null),
						new Command("Không", GameCanvas.instance, GameCanvas.END_DIALOG, null));
				break;
			case DISBAND_CLAN:
				String disbandClanType = (cmdClanLocal.isFocus) ? ClanType.LOCAL.name() : ClanType.GLOBAL.name();
				long disbandClanId = (cmdClanLocal.isFocus) ? Char.myChar().localClan.Id : Char.myChar().globalClan.Id;
				Service.getInstance().DisbandClan(disbandClanType, disbandClanId);
				GameCanvas.getInstance().endDlg();
				GameScreen.getInstance().switchToMe();
				Clear();
				break;
			case CONFIRM_KICK_CLAN_MEMBER:
				GameCanvas.startYesNoDlg("Bạn có muốn kick thành viên này?",
						new Command("Có", this, KICK_CLAN_MEMBER, null),
						new Command("Không", GameCanvas.instance, GameCanvas.END_DIALOG, null));
				break;
			case KICK_CLAN_MEMBER:
				if (selectedClanMember != null)
				{
					String kickMemberClanType = (cmdClanLocal.isFocus) ? ClanType.LOCAL.name() : ClanType.GLOBAL.name();
					long kickMemberClanId = (cmdClanLocal.isFocus) ? Char.myChar().localClan.Id : Char.myChar().globalClan.Id;
					Service.getInstance().KickMemberClan(kickMemberClanType, kickMemberClanId, selectedClanMember.Id);
					GameCanvas.getInstance().endDlg();
					GameScreen.getInstance().switchToMe();
					Clear();
				}
				break;
			case ACCEPT_INVITATION:
				if (selectedClan != null)
				{
					Service.getInstance().AcceptClanInvitation(selectedClan.Id, selectedClan.ClanType.name());
					GameCanvas.getInstance().endDlg();
					GameScreen.getInstance().switchToMe();
					Clear();
				}
				break;
			case REJECT_INVITATION:
				if (selectedClan != null)
				{
					Service.getInstance().RejectClanInvitation(selectedClan.Id, selectedClan.ClanType.name());
					GameCanvas.getInstance().endDlg();
					GameScreen.getInstance().switchToMe();
					Clear();
				}
				break;
		}
	}

	public static ClanScreen getInstance()
	{
		if (instance == null) {
			instance = new ClanScreen();
		}
		return instance;
	}

	public void update()
	{
		GameScreen.getInstance().update();
		if (scrClan.selectedItem == -1) return;
		indexRow = scrClan.selectedItem;
	}

	public void updateKey()
	{
		super.updateKey();

		scrClan.updateKey();
		scrClan.updatecm();

		updateKeyCloseCommandEvent(cmdClose);


		if (selectedCommand == cmdKick)
		{
			updateKeyCommandEvent(cmdKick);
		}

		if (selectedCommand == cmdMemberLeave)
		{
			updateKeyCommandEvent(cmdMemberLeave);
		}

		if (selectedCommand == cmdHostLeave)
		{
			updateKeyCommandEvent(cmdDisband);
			updateKeyCommandEvent(cmdHostLeave);
		}

		if (selectedCommand == cmdAcceptInvite)
		{
			updateKeyCommandEvent(cmdAcceptInvite);
			updateKeyCommandEvent(cmdRejectInvite);
		}

		updateKeyCommandEvent(cmdClanLocal);
		updateKeyCommandEvent(cmdClanGlobal);
		updateKeyCommandEvent(cmdListInvited);

	}

	private void updateKeyCommandEvent(Command command)
	{
		if (command != null && getCmdPointerLast(command))
		{
			GameCanvas.isPointerJustRelease = false;
			GameCanvas.keyPressed[5] = false;
			keyTouch = -1;
			command.performAction();
			GameCanvas.clearPointerEvent();
		}
	}

	private void updateKeyCloseCommandEvent(Command command)
	{
		if (GameCanvas.keyPressed[13] || getCmdPointerLast(command))
		{
			if (command != null)
			{
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				keyTouch = -1;
				command.performAction();
				GameCanvas.clearPointerEvent();
			}
		}
	}

	public void paint(MGraphics g)
	{
		super.paint(g);
		GameScreen.getInstance().paint(g);
		g.setColor(0x000000, GameCanvas.opacityTab);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		g.disableBlending();

		Paint.paintFrameNaruto(xpaint, ypaint, wkhung, hKhung + 2, g);
		Paint.PaintBoxName("Bang hội", xpaint + wkhung / 2 - 40, ypaint, 80, g);

		cmdClanGlobal.paint(g);
		cmdClanLocal.paint(g);
		cmdListInvited.paint(g);
		cmdClose.paint(g);

		if (cmdClanLocal.isFocus || cmdClanGlobal.isFocus)
		{
			List<ClanMember> currentListMember = (cmdClanLocal.isFocus) ? ListMemberLocalClan : ListMemberGlobalClan;
			List<Clan> currentListClan = (cmdClanLocal.isFocus) ? ListLocalClan : ListGlobalClan;

			clanName = (cmdClanLocal.isFocus) ? clanLocalName : clanGlobalName;

			g.drawImage(LoadImageInterface.imgName, xpaint + 120, ypaint + 32, MGraphics.VCENTER | MGraphics.HCENTER);
			FontManager.getInstance().tahoma_7_white.drawString(g, (clanName != null) ? clanName : "Tất cả bang", xpaint + 120, ypaint + 1 + 1 * 24, 2);

			if (IsInClan())
			{
				PaintClanMembers(g, currentListMember);
			} else {
				PaintListClan(g, currentListClan);
			}

			if (indexRow > -1)
			{
				List<ClanMember> clanMembers = (cmdClanLocal.isFocus) ? ListMemberLocalClan : ListMemberGlobalClan;
				if (clanMembers.size() > indexRow)
				{
					selectedClanMember = clanMembers.get(indexRow);
				}

				if (IsCanKickMember(indexRow))
				{
					cmdKick.paint(g);
					selectedCommand = cmdKick;
				} else
				{
					PaintCommandIfInClan(g);
				}
			}
			else
			{
				PaintCommandIfInClan(g);
			}

			scrClan.setStyle(cmdClanLocal.isFocus ? ListMemberLocalClan.size(): ListMemberGlobalClan.size(), 50, xpaint + 5,
					ypaint + 40, wkhung - 3, 180, true, 1);
			scrClan.setClip(g, xpaint + 5, ypaint + 40, wkhung - 3, 140);

		} else if (cmdListInvited.isFocus)
		{
			g.drawImage(LoadImageInterface.imgName, xpaint + 120, ypaint + 32, MGraphics.VCENTER | MGraphics.HCENTER);
			FontManager.getInstance().tahoma_7_white.drawString(g, "Thư mời", xpaint + 120, ypaint + 1 + 24, 2);

			PaintClanInvitations(g);
			if (indexRow > -1)
			{
				if (ListClanInvitations.size() > indexRow)
				{
					selectedClan = ListClanInvitations.get(indexRow);

					cmdAcceptInvite.paint(g);
					cmdRejectInvite.paint(g);
					selectedCommand = cmdAcceptInvite;
				}

			}

			scrClan.setStyle(ListClanInvitations.size(), 50, xpaint + 5,
					ypaint + 40, wkhung - 3, 180, true, 1);
			scrClan.setClip(g, xpaint + 5, ypaint + 40, wkhung - 3, 140);
		}

		GameCanvas.resetTrans(g);
		super.paint(g);
	}

	private void PaintCommandIfInClan(MGraphics g)
	{
		if (!IsInClan()) return;
		if (IsClanHost())
		{
			cmdDisband.paint(g);
			cmdHostLeave.paint(g);
			selectedCommand = cmdHostLeave;
		}
		else
		{
			cmdMemberLeave.paint(g);
			selectedCommand = cmdMemberLeave;
		}
	}

	private void PaintClanInvitations(MGraphics g)
	{
		int yOffset = 0;
		for (int i = 0; i < ListClanInvitations.size(); i++)
		{
			Clan clan = ListClanInvitations.get(i);
			if (clan == null || clan.Name == null || clan.Host == null) continue;
			Paint.PaintBGListQuest(xpaint + 40, ypaint + yOffset + 40 , 160, g);
			if (indexRow == i)
			{
				Paint.PaintBGListQuestFocus(xpaint + 40, ypaint + yOffset + 40, 160, g);
			}

			FontManager.getInstance().tahoma_7_white.drawString(g, "Bang " + GetClanTypeName(clan.ClanType) + " " + clan.Name + " (" + clan.TotalMembers + " thành viên)", xpaint + 78, ypaint + yOffset + 48, 0);
			FontManager.getInstance().tahoma_7_white.drawString(g, "Lời mời từ bang chủ " + clan.Host.Name, xpaint + 78, ypaint + yOffset + 64, 0);

			PaintClanMemberAvatar(g, clan.Host.IdHead, yOffset);
			yOffset += 50;
		}
	}

	private void PaintListClan(MGraphics g, List<Clan> listClan)
	{
		int yOffset = 0;
		for (int i = 0; i < listClan.size(); i++)
		{
			Clan clan = listClan.get(i);
			if (clan == null || clan.Name == null || clan.Host == null) continue;
			Paint.PaintBGListQuest(xpaint + 40, ypaint + yOffset + 40 , 160, g);
			FontManager.getInstance().tahoma_7_white.drawString(g, clan.Name + " (" + clan.TotalMembers + " thành viên)", xpaint + 78, ypaint + yOffset + 48, 0);
			FontManager.getInstance().tahoma_7_white.drawString(g, "Bang chủ: " + clan.Host.Name + " (level " + clan.Host.Level + ")", xpaint + 78, ypaint + yOffset + 64, 0);

			PaintClanMemberAvatar(g, clan.Host.IdHead, yOffset);
			yOffset += 50;
		}
	}

	private void PaintClanMembers(MGraphics g, List<ClanMember> listMember)
	{
		int yOffset = 0;
		for (int i = 0; i < listMember.size(); i++)
		{
			ClanMember user = listMember.get(i);
			if (user == null || user.Name == null) continue;
			PaintClanMember(g, user, yOffset, i);
			yOffset += 50;
		}
	}

	private void PaintClanMember(MGraphics g, ClanMember user, int yOffset, int index)
	{

		Paint.PaintBGListQuest(xpaint + 40, ypaint + yOffset + 40 , 160, g);
		if (indexRow == index)
		{
			Paint.PaintBGListQuestFocus(xpaint + 40, ypaint + yOffset + 40, 160, g);
		}
		g.drawImage(LoadImageInterface.imgName, xpaint + 105, ypaint + yOffset + 55,
				MGraphics.VCENTER | MGraphics.HCENTER);
		FontManager.getInstance().tahoma_7_white.drawString(g, user.Name, xpaint + 78, ypaint + yOffset + 48, 0);
		FontManager.getInstance().tahoma_7_white.drawString(g, user.Position + " (level " + user.Level + ")", xpaint + 78, ypaint + yOffset + 64, 0);

		MBitmap imgOnline = (user.Online) ? LoadImageInterface.imgOnline[1] : LoadImageInterface.imgOnline[0];
		g.drawImage(imgOnline, xpaint + 185, ypaint + yOffset + 60,
				MGraphics.VCENTER | MGraphics.HCENTER);

		PaintClanMemberAvatar(g, user.IdHead, yOffset);
	}

	private void PaintClanMemberAvatar(MGraphics g, short idHead, int yOffset)
	{
		g.drawImage(LoadImageInterface.charPic, xpaint + 50, ypaint + yOffset + 60,
				MGraphics.VCENTER | MGraphics.HCENTER);

		try
		{
			Part ph2 = GameScreen.parts[idHead];
			SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
					xpaint + 50, ypaint + yOffset + 60, 0, MGraphics.VCENTER | MGraphics.HCENTER);
		}
		catch (Exception e)
		{
			Log.e(ClanScreen.class.getName(), "Failed to paint clan member avatar", e);
		}
	}

	public void Clear()
	{
		ListLocalClan = new ArrayList<Clan>();
		ListGlobalClan = new ArrayList<Clan>();
		ListClanInvitations = new ArrayList<Clan>();
		ListMemberLocalClan = new ArrayList<ClanMember>();
		ListMemberGlobalClan = new ArrayList<ClanMember>();
		clanGlobalName = null;
		clanLocalName = null;
		scrClan.selectedItem = -1;
		indexRow = -1;
	}

	private boolean IsInClan()
	{
		return cmdClanLocal.isFocus && Char.myChar().localClan != null ||
				cmdClanGlobal.isFocus && Char.myChar().globalClan != null;
	}

	private boolean IsClanHost()
	{
		List<ClanMember> clanMembers = (cmdClanLocal.isFocus) ? ListMemberLocalClan : ListMemberGlobalClan;
		return clanMembers.size() > 0 && clanMembers.get(0).Id == Char.myChar().charID;
	}

	private boolean IsCanKickMember(int index)
	{
		List<ClanMember> clanMembers = (cmdClanLocal.isFocus) ? ListMemberLocalClan : ListMemberGlobalClan;
		if (index < 0 || index >= clanMembers.size()) return false;
		ClanMember member = clanMembers.get(index);
		return IsClanHost() && member.Id != Char.myChar().charID;
	}

	private String GetClanTypeName(ClanType clanType)
	{
		switch (clanType)
		{
			case GLOBAL: return "thế giới";
			case LOCAL: return "quốc gia";
		}
		return "";
	}
}
