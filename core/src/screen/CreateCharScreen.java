package screen;

import android.util.Log;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import Gui.Text;
import Objectgame.Char;
import lib.Command;
import lib.FontManager;
import lib.Image;
import lib.LoadImageInterface;
import lib.MBitmap;
import lib.MFont;
import lib.TField;
import model.IActionListener;
import model.MResources;
import model.Paint;
import model.Part;
import model.Screen;
import model.Scroll;
import model.SmallImage;
import real.Service;

/**
 * @author Monkey D Lyffy
 *
 */
public class CreateCharScreen extends Screen implements IActionListener {

	private Command btnCreatChar, btnCancel; // nut tao char va nut cancle
	public static final int NAM = 10;
	public static final int NU = 111;
	public static CreateCharScreen instance;
	public static TField tAddName;
	public static int indexHair, selected, indexBody, indexleg;
	public static MBitmap imgItem, characterBar, genderBar;
	public static MBitmap[] gender = new MBitmap[4];
	public static MBitmap[] imgClazz = new MBitmap[6];
	public static MBitmap[] imgQuocGia = new MBitmap[10];

	private int curIndex;
	private int dem;
	private int[] hKhung = new int[3];
	public int[][][] idPartHoa, idPartThuy, idPartTho, idPartLoi, idPartPhong;
	public byte indexGender, indexCountry, indexClass;
	public String[][] infoClazz, infoQuocGia;
	private Command male;
	private Command female;

	private Scroll scrInfoClass = new Scroll();
	private Scroll scrQuocGia = new Scroll();
	public int wpaintKhung = 110, yKhung1, xkhungTrai = 15, sizeItem = 26;
	private int[][] xyItemClass = new int[3][];
	private int[][] xyItemInfo = new int[2][];
	private int[][] xyItemQuocGia = new int[5][];


	private CreateCharScreen()
	{
		hKhung[0] = 60;
		hKhung[1] = 90;
		hKhung[2] = 160;

		if (GameCanvas.w == 128)
		{
			GameScreen.setPopupSize(128, 120);
			GameScreen.popupX = (GameCanvas.w - 128) / 2;
			GameScreen.popupY = 0;
		}
		else
		{
			GameScreen.setPopupSize(170, 190);
			GameScreen.popupX = (GameCanvas.w - 170) / 2;
			GameScreen.popupY = (GameCanvas.h - 220) / 2;
		}

		tAddName = new TField();
		tAddName.isPaintImg = true;
		tAddName.y = GameCanvas.h - 30;
		tAddName.width = 100;
		

		if (GameCanvas.w == 128)
		{
			tAddName.width = 60;
		}

		tAddName.x = GameCanvas.w / 2 - tAddName.width / 2 + 5; //GameCanvas.w/2, GameCanvas.h-20
		tAddName.height = 21;
		tAddName.setInputType(TField.INPUT_TYPE_ANY);
		indexHair = 0;
		yKhung1 = GameCanvas.h / 6 + 240 >= GameCanvas.h ? GameCanvas.h - 240 : GameCanvas.h / 6;
		male = new Command("", this, NAM, null);
		male.setPos(xkhungTrai + wpaintKhung / 2 - 30, yKhung1, gender[2], gender[3]);
		female = new Command("", this, NU, null);
		female.setPos(xkhungTrai + wpaintKhung / 2 + 5, yKhung1, gender[1], gender[0]);
		male.w = female.w = 25;
		btnCancel = new Command("Đóng", this, 8001, null);
		btnCancel.setPos(GameCanvas.w  -  100, GameCanvas.h - 26, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
		btnCreatChar = new Command("Tạo", this, 8000, null);
		btnCreatChar.setPos(40, GameCanvas.h - 26, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

		int wkc = (wpaintKhung - sizeItem * xyItemClass.length) / (xyItemClass.length + 1);
		int wkc2 = (wpaintKhung - sizeItem * (xyItemClass.length - 1)) / xyItemClass.length;
		for (int i = 0; i < xyItemClass.length; i++)
		{
			xyItemClass[i] = new int[2];
			xyItemClass[i][0] = xkhungTrai + sizeItem * i + (i + 1) * wkc;
			xyItemClass[i][1] = yKhung1 + 62;
		}
		for (int i = 0; i < xyItemQuocGia.length; i++)
		{
			xyItemQuocGia[i] = new int[2];
			xyItemQuocGia[i][0] = xkhungTrai + sizeItem * (i % 3) + (i % 3 + 1) * (i <= 2 ? wkc : wkc2);
			xyItemQuocGia[i][1] = yKhung1 + hKhung[0] + 72 + i / 3 * 30;
		}
		for (int i = 0; i < xyItemInfo.length; i++)
		{
			xyItemInfo[i] = new int[2];
			xyItemInfo[i][0] = GameCanvas.w - wpaintKhung - xkhungTrai + 4;
			xyItemInfo[i][1] = yKhung1 + hKhung[0] + 36 + i * 52;
		}
		center = null;
		left = null;
		right = null;

		String[] infoClazzz =
				{
						//the thuat
						"Thể thuật, với lệ thế cận chiến mạnh tốc độ di chuyển nhanh hơn thế là khả năng y thuật tuyệt vời."
								+ " Riêng với thể thuật có thể đi theo hướng cận chiến hoặc y liệu cả hai điều có lệ thế riêng của mình.",
						//ao thuat
						"Ảo thuật là những thuật sử dụng chakra trong hệ thần kinh của đối thủ để tạo ra ảo giác; "
								+ "về cơ bản được coi là nhẫn thuật cao cấp về trí óc. Ảo thuật mạnh về những chiêu thức gây choáng và làm ngộp đối thủ.",
						//nhan thuat
						"Nhẫn thuật, chủ yếu dùng chakra để hiện thực hóa các đòn tấn công cũng như phòng thủ."
								+ " Để trở thành một nhẫn thuật giỏi cần thừa hưởng lượng chakra phong phú. Lợi thế class Nhẫn Thuật dồi giàu về chakra."
				};
		String[] infoQuocGiaa =
				{
						//hoa
						"Giống như tên của nó, Hỏa Quốc có khuynh hướng thiên về yếu tố lửa, với thời tiết thoáng đãng và ấm áp.",
						//thuy
						"Nó có nhiều hòn đảo, mỗi đảo lại có một truyền thống riêng. Thủy Quốc và làng Sương mù chưa bao giờ lộ diện trong truyện, nhưng vài ninja đến từ khu vực này đã xuất hiện.",
						//loi
						"Giữa Lôi Quốc là những dãy núi lớn, nơi có nhiều bão sấm sét, đây là nguồn gốc tên đất nước này. "
								+ "Từ những rặng núi này nhiều con sông đổ ra biển, tạo nên bờ biển uốn cong, cảnh biển ấn tượng và ngoạn mục. "
								+ "Có nhiều suối nước nóng trong lãnh thổ nước này.",
						// phong
						"Giống như tên của nó, Hỏa Quốc có khuynh hướng thiên về yếu tố Phong là một đất nước rộng lớn, khô cằn, và hoang vắng với những thành phố ở gần nguồn nước. ",
						//tho
						"Thổ Quốc bao gồm chủ yếu các khu vực hoang và nhiều đá, biên giới của nó chạy dọc theo dãy núi đá, ngăn liên lạc với các nước khác."
								+ " Gió bắc thổi qua rặng núi này, mang những viên đá nhỏ tới các nước lân cận. Hiện tượng thiên nhiên kì thú này được gọi là Gan'u."
				};
		infoClazz = new String[infoClazzz.length][];
		infoQuocGia = new String[infoQuocGiaa.length][];
		for (int i = 0; i < infoClazzz.length; i++)
			infoClazz[i] = FontManager.getInstance().tahoma_6_white.splitFontArray(infoClazzz[i], wpaintKhung - 26 - 8);
		for (int i = 0; i < infoQuocGiaa.length; i++)
			infoQuocGia[i] = FontManager.getInstance().tahoma_6_white.splitFontArray(infoQuocGiaa[i], wpaintKhung - 26 - 8);
	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case 8000:
				Service.getInstance().CreateChar(indexClass, indexCountry, indexGender, tAddName.getText());
				break;
			case 8001:
				GameCanvas.getInstance().loadBG(1);
				SelectCharScreen.getInstance().switchToMe();
				break;
			case NAM:
				female.img = gender[1];
				female.imgFocus = gender[0];
				male.img = gender[2];
				male.imgFocus = gender[3];
				indexGender = 0;
				resetPart();
				break;
			case NU:
				female.img = gender[0];
				female.imgFocus = gender[1];
				male.img = gender[3];
				male.imgFocus = gender[2];
				indexGender = 1;
				resetPart();
				break;
		}
	}

	public static CreateCharScreen getInstance()
	{
		if (instance == null) {
			instance = new CreateCharScreen();
		}
		return instance;
	}

	public static void loadImage()
	{
		for (int i = 0; i < gender.length; i++)
		{
			gender[i] = GameCanvas.loadImage("/GuiNaruto/createChar/gender" + (i + 1) + ".png");
		}

		for (int i = 0; i < imgClazz.length; i++)
		{
			imgClazz[i] = GameCanvas.loadImage("/GuiNaruto/createChar/iconClass" + i + ".png");
		}

		for (int i = 0; i < imgQuocGia.length; i++)
		{
			imgQuocGia[i] = GameCanvas.loadImage("/GuiNaruto/createChar/imgQuocGia" + i + ".png");
		}

		imgItem = GameCanvas.loadImage("/GuiNaruto/createChar/itemCreateC.png");
		characterBar = GameCanvas.loadImage("/GuiNaruto/createChar/characterbar.png");
		genderBar = GameCanvas.loadImage("/GuiNaruto/createChar/gender_bar.png");
	}

	public void switchToMe()
	{
		tAddName.setText("");
		indexCountry = 0;
		indexClass = 0;
		GameCanvas.getInstance().loadBG(1);
		male.performAction();
		resetPart();
		super.switchToMe();
	}

	public void keyPress(int keyCode)
	{
		tAddName.keyPressed(keyCode);
		super.keyPress(keyCode);
	}

	public void update()
	{
		GameScreen.cmx++;
		if (selected == 0) // Name
			tAddName.update();
		if (GameCanvas.getInstance().imgBG[0] == null || GameCanvas.getInstance().imgBG[1] == null || GameCanvas.getInstance().imgBG[2] == null)
			GameCanvas.getInstance().loadBG(1);
		dem++;
		if (dem >= 1000)
			dem = 0;
		if (GameScreen.cmx > GameCanvas.w * 3 + 100)
			GameScreen.cmx = 100;

		if (GameCanvas.isTouch && GameCanvas.w >= 320)
		{
			if (left != null)
			{
				left.x = GameCanvas.w / 2 - 160;
				left.y = GameCanvas.h - 26;
			}
			if (center != null)
			{
				center.x = GameCanvas.w / 2 - 35;
				center.y = GameCanvas.h - 26;
			}

			if (right != null)
			{
				right.x = GameCanvas.w / 2 + 88;
				right.y = GameCanvas.h - 26;
			}
		}
	}

	public void updateKey()
	{
		scrInfoClass.updateKey();
		scrInfoClass.updatecm();
		scrQuocGia.updateKey();
		scrQuocGia.updatecm();
		if (GameCanvas.keyPressed[2])
		{
			selected--;
			if (selected < 0)
				selected = MResources.MENUNEWCHAR.length - 1;
		}
		if (GameCanvas.keyPressed[8])
		{
			selected++;
			if (selected >= MResources.MENUNEWCHAR.length)
				selected = 0;
		}


		if (selected == 1)
		{
			// Gender
			if (GameCanvas.keyPressed[4])
			{
				indexGender--;
				if (indexGender < 0)
					indexGender = (byte) (MResources.MENUGENDER.length - 1);
			}
			if (GameCanvas.keyPressed[6])
			{
				indexGender++;
				if (indexGender > MResources.MENUGENDER.length - 1)
					indexGender = 0;
			}
			right = null;
		}

		if (selected == 2) // Loáº¡i 
		{
			if (GameCanvas.keyPressed[4])
			{
				indexClass--;
			}

			if (GameCanvas.keyPressed[6])
			{
				indexClass++;
			}
		}

		if (selected == 3)
		{
			if (GameCanvas.keyPressed[4])
			{
				indexCountry--;
				if (indexCountry < 0)
					indexCountry = (byte) (Text.typeCountry.length - 1);
			}

			if (GameCanvas.keyPressed[6])
			{
				indexCountry++;
				if (indexCountry > Text.typeCountry.length - 1)
					indexCountry = 0;
			}
		}


		if (GameCanvas.isPointerJustRelease)
		{
			if (GameCanvas.isPointerHoldIn(GameScreen.popupX + 5, GameScreen.popupY + 65, GameScreen.popupW - 5, ITEM_HEIGHT))
				selected = 0;

			if (GameCanvas.isPointerHoldIn(GameScreen.popupX + 5, GameScreen.popupY + 106, GameScreen.popupW - 5, ITEM_HEIGHT))
			{
				curIndex = 1;
				if (curIndex == selected)
				{
					indexGender--;
					if (indexGender < 0)
						indexGender = (byte) (MResources.MENUGENDER.length - 1);
				}
				else
				{
					selected = 1;
				}
			}
			if (GameCanvas.isPointerHoldIn(GameScreen.popupX + 5, GameScreen.popupY + 150, GameScreen.popupW - 5, ITEM_HEIGHT))
			{
				curIndex = 2;
				if (curIndex == selected)
				{
					indexClass++;
				}
				else
				{
					selected = 2;
				}
			}
		}
		for (int i = 0; i < xyItemClass.length; i++) {
			if (GameCanvas.isPointerJustRelease && GameCanvas.isPoint(xyItemClass[i][0], xyItemClass[i][1], sizeItem, sizeItem)) {
				indexClass = (byte) i;
				scrInfoClass.cmtoY = 0;
				resetPart();
				GameCanvas.clearPointerEvent();
			}
		}
		//indexCountry
		for (int i = 0; i < xyItemQuocGia.length; i++) {
			if (GameCanvas.isPointerJustRelease && GameCanvas.isPoint(xyItemQuocGia[i][0], xyItemQuocGia[i][1], sizeItem, sizeItem)) {
				indexCountry = (byte) i;
				scrQuocGia.cmtoY = 0;
				resetPart();
				GameCanvas.clearPointerEvent();
			}
		}
		if (getCmdPointerLast(male))
		{
			GameCanvas.isPointerJustRelease = false;
			male.performAction();
		}
		if (getCmdPointerLast(female))
		{
			GameCanvas.isPointerJustRelease = false;
			female.performAction();
		}
		if (getCmdPointerLast(btnCancel))
		{
			GameCanvas.isPointerJustRelease = false;
			btnCancel.performAction();
		}
		if (getCmdPointerLast(btnCreatChar))
		{
			GameCanvas.isPointerJustRelease = false;
			btnCreatChar.performAction();
		}
		super.updateKey();
		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
	}

	private void resetPart()
	{
		switch (indexCountry)
		{
			case 0: //hoa
				indexHair = idPartHoa[indexClass][indexGender][0];
				indexBody = idPartHoa[indexClass][indexGender][1];
				indexleg = idPartHoa[indexClass][indexGender][2];
				break;
			case 1: //thuy
				indexHair = idPartThuy[indexClass][indexGender][0];
				indexBody = idPartThuy[indexClass][indexGender][1];
				indexleg = idPartThuy[indexClass][indexGender][2];
				break;
			case 2: //loi
				indexHair = idPartLoi[indexClass][indexGender][0];
				indexBody = idPartLoi[indexClass][indexGender][1];
				indexleg = idPartLoi[indexClass][indexGender][2];
				break;
			case 3: //phong
				indexHair = idPartPhong[indexClass][indexGender][0];
				indexBody = idPartPhong[indexClass][indexGender][1];
				indexleg = idPartPhong[indexClass][indexGender][2];
				break;
			case 4: //tho
				indexHair = idPartTho[indexClass][indexGender][0];
				indexBody = idPartTho[indexClass][indexGender][1];
				indexleg = idPartTho[indexClass][indexGender][2];
				break;
		}
	}

	public void paint(MGraphics g)
	{
		try
		{
			GameCanvas.getInstance().paintBGGameScreen(g);

			//khung thong tin
			Paint.SubFrame(GameCanvas.w - wpaintKhung - xkhungTrai, male.y + 40, wpaintKhung, hKhung[2], g, 0x037ddb, 80);
			scrInfoClass.setStyle(infoClazz[indexClass].length, 8, xyItemInfo[0][0] + imgItem.getWidth() + 2,
					xyItemInfo[0][1] - 4, wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2, true, 1);

			scrInfoClass.setClip(g, xyItemInfo[0][0] + imgItem.getWidth() + 2, xyItemInfo[0][1] - 4,
					wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2 - 4);

			for (int j = 0; j < infoClazz[indexClass].length; j++)
			{
				FontManager.getInstance().tahoma_6_white.drawString(g, infoClazz[indexClass][j],
						xyItemInfo[0][0] + imgItem.getWidth() + 2, xyItemInfo[0][1] + j * 8, 0);
			}
			GameCanvas.resetTrans(g);

			scrQuocGia.setStyle(infoQuocGia[indexCountry].length, 8, xyItemInfo[1][0] + imgItem.getWidth() + 2,
					xyItemInfo[1][1] - 4, wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2, true, 1);
			scrQuocGia.setClip(g, xyItemInfo[1][0] + imgItem.getWidth() + 2, xyItemInfo[1][1] - 4,
					wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2 - 4);

			for (int j = 0; j < infoQuocGia[indexCountry].length; j++)
			{
				FontManager.getInstance().tahoma_6_white.drawString(g, infoQuocGia[indexCountry][j],
						xyItemInfo[1][0] + imgItem.getWidth() + 2, xyItemInfo[1][1] + 8 * j, 0);
			}

			GameCanvas.resetTrans(g);
			g.drawImage(LoadImageInterface.imgRock, GameCanvas.w / 2,
					GameCanvas.h - 10 - LoadImageInterface.imgRock.getHeight() / 2, MGraphics.VCENTER | MGraphics.HCENTER);

			int cx = GameCanvas.w / 2, cy = GameCanvas.h - LoadImageInterface.imgRock.getHeight() / 2 - 30;
			try
			{
				Part ph = GameScreen.parts[indexHair], pl = GameScreen.parts[indexleg], pb = GameScreen.parts[indexBody];
				SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[0][1][0]].id, cx + Char.CharInfo[0][1][1] + pl.pi[Char.CharInfo[0][1][0]].dx, cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dy,0,0);
				SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[0][2][0]].id, cx + Char.CharInfo[0][2][1] + pb.pi[Char.CharInfo[0][2][0]].dx, cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].dy,0,0);
				SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id, cx + Char.CharInfo[0][0][1] + ph.pi[Char.CharInfo[0][0][0]].dx, cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].dy,0,0);
			}
			catch (Exception e)
			{
				Log.e(CreateCharScreen.class.getName(), "Failed to paint", e);
			}
			g.drawImage(genderBar, xkhungTrai + wpaintKhung / 2, male.y + male.img.getHeight() / 2,
					MGraphics.VCENTER | MGraphics.HCENTER);
			Paint.SubFrame(xkhungTrai, male.y + 40, wpaintKhung, hKhung[0], g, 0x037ddb, 80);
			FontManager.getInstance().tahoma_7_white.drawString(g, "Class", xkhungTrai + wpaintKhung / 2, male.y + 45, MFont.CENTER);
			for (int i = 0; i < xyItemClass.length; i++)
			{
				g.drawImage(imgItem, xyItemClass[i][0], xyItemClass[i][1], MGraphics.TOP | MGraphics.LEFT);
				if (i == indexClass)
				{
					g.drawImage(imgClazz[indexClass * 2 + 1],
							xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);

					g.drawImage(LoadImageInterface.imgFocusSelectItem,
							xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);
				}
				else
				{
					g.drawImage(imgClazz[i * 2],
							xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);
				}
			}
			Paint.SubFrame(xkhungTrai, male.y + 110, wpaintKhung, hKhung[1], g, 0x037ddb, 80);
			for (int i = 0; i < xyItemQuocGia.length; i++)
			{
				g.drawImage(imgItem, xyItemQuocGia[i][0], xyItemQuocGia[i][1], MGraphics.TOP | MGraphics.LEFT);
				if (i == indexCountry)
				{
					g.drawImage(imgQuocGia[indexCountry * 2 + 1],
							xyItemQuocGia[i][0] + imgItem.getWidth() / 2, xyItemQuocGia[i][1] + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);

					g.drawImage(LoadImageInterface.imgFocusSelectItem,
							xyItemQuocGia[i][0] + imgItem.getWidth() / 2, xyItemQuocGia[i][1] + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);
				}
				else
				{
					g.drawImage(imgQuocGia[i * 2],
							xyItemQuocGia[i][0] + imgItem.getWidth() / 2,
							xyItemQuocGia[i][1] + imgItem.getHeight() / 2, MGraphics.VCENTER | MGraphics.HCENTER);
				}
			}
			FontManager.getInstance().tahoma_7_white.drawString(g, "Quốc gia", xkhungTrai + wpaintKhung / 2, male.y + 115, MFont.CENTER);
			for (int i = 0; i < wpaintKhung / LoadImageInterface.imgLineTrade.getWidth() - 1; i++)
			{
				g.drawImage(LoadImageInterface.imgLineTrade,
						xyItemInfo[0][0] + Image.getWidth(LoadImageInterface.imgLineTrade) * i, xyItemInfo[0][1] - 8, 0);
				g.drawImage(LoadImageInterface.imgLineTrade,
						xyItemInfo[1][0] + Image.getWidth(LoadImageInterface.imgLineTrade) * i, xyItemInfo[1][1] - 8, 0);
			}
			FontManager.getInstance().tahoma_7_white.drawString(g, "Gender", GameCanvas.w - wpaintKhung / 2 - xkhungTrai,
					male.y + 45, MFont.CENTER);
			if (gender[0] != null)
				g.drawImage(indexGender == 0 ? gender[2] : gender[0], GameCanvas.w - wpaintKhung / 2 - xkhungTrai,
						male.y + 70, MGraphics.VCENTER | MGraphics.HCENTER);
			for (int i = 0; i < xyItemInfo.length; i++)
			{
				g.drawImage(imgItem, xyItemInfo[i][0], xyItemInfo[i][1] + 6, MGraphics.TOP | MGraphics.LEFT);
				if (i == 1)
					g.drawRegion(imgQuocGia[indexCountry * 2 + 1], 0, 0, imgQuocGia[indexCountry * 2 + 1].getWidth(),
							imgQuocGia[indexCountry * 2 + 1].getHeight(), 0,
							xyItemInfo[i][0] + imgItem.getWidth() / 2, xyItemInfo[i][1] + 6 + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);
				else
					g.drawRegion(imgClazz[indexClass * 2 + 1], 0, 0, imgClazz[indexClass * 2 + 1].getWidth(),
							imgClazz[indexClass * 2 + 1].getHeight(), 0,
							xyItemInfo[i][0] + imgItem.getWidth() / 2, xyItemInfo[i][1] + 6 + imgItem.getHeight() / 2,
							MGraphics.VCENTER | MGraphics.HCENTER);
			}
			male.paint(g);
			female.paint(g);
			btnCancel.paint(g);
			btnCreatChar.paint(g);
			g.drawImage(characterBar, GameCanvas.w / 2, GameCanvas.h - 20, MGraphics.VCENTER | MGraphics.HCENTER);


			if (tAddName.isFocus)
				tAddName.paint(g);
			else FontManager.getInstance().tahoma_7b_white.drawString(g, tAddName.getText(), GameCanvas.w / 2, GameCanvas.h - 25, 2);

			// graphic.fillRect(GameScr.popupX + 5,
			// GameScr.popupY + 65, GameScr.popupW - 5, ITEM_HEIGHT);
		}
		catch (Exception e)
		{
			Log.e(CreateCharScreen.class.getName(), "Failed to paint", e);
		}
		super.paint(g);
	}

}
