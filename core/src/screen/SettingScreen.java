package screen;

import android.service.quicksettings.Tile;

import Gui.GuiMain;
import Objectgame.TileMap;
import lib.FontManager;
import lib.LoadImageInterface;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import java.util.Arrays;

import lib.Command;
import lib.Music;
import lib.Rms;
import model.Command;
import model.IActionListener;
import model.Paint;
import model.Screen;
import real.Service;

public class SettingScreen extends Screen implements IActionListener{

	public static final int MUSIC = 0;
	public static final int SOUND = 1;
	public static final int TYPE_MOVE = 2;
	public static final int AUTO_DANH = 3;
	public static final int LOW_GRAPHIC = 4;
//	public static final int AUTO_NHAT = 5;

	private static final int CLOSE_CMD = 5;

	private static SettingScreen instance;

	public static byte[] defaultAuto = {
			(byte) (Music.isplayMusic ? 1 : 0),
			(byte) (Music.isplaySound ? 1 : 0),
			(byte) (GuiMain.isAnalog ? 1 : 0),
			(byte) (GameScreen.isAutoDanh ? 1 : 0),
			(byte) (GameCanvas.isLowGraphic ? 1 : 0)};

	private Command cmdClose;

	private String[] listText = {"Âm nhạc nền", "Âm thanh game", "Analog di chuyển", "Tự động đánh", "Đồ hoạ thấp"};

	private boolean[] isListAuto = new boolean[listText.length];
	private byte[] listAuto = new byte[listText.length];


	private int wkhung, hKhung, xpaint, ypaint;

	private SettingScreen()
	{
		wkhung = 200;
		hKhung = 150;

		xpaint = GameCanvas.w / 2 - wkhung / 2;
		ypaint = GameCanvas.h / 2 - hKhung / 2 - 10;
		cmdClose = new Command("", this, CLOSE_CMD, null);
		cmdClose.setPos(xpaint + wkhung - LoadImageInterface.closeTab.width / 2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case CLOSE_CMD:
				GameScreen.getInstance().switchToMe();
				break;
		}
	}

	public static SettingScreen getInstance()
	{
		if (instance == null) {
			instance = new SettingScreen();
		}
		return instance;
	}

	public void switchToMe()
	{
		super.switchToMe();
		byte[] listAuto = Rms.loadRMS(Rms.autoSetting);
		if (listAuto == null || listAuto.length < defaultAuto.length) {
			Rms.saveRMS(Rms.autoSetting, defaultAuto);
		} else {
			for (int i = 0; i < listAuto.length; i++) {
				isListAuto[i] = listAuto[i] == 1;
			}
		}
	}

	public void updateKey()
	{
		super.updateKey();
		if (GameCanvas.keyPressed[13] || getCmdPointerLast(cmdClose))
			if (cmdClose != null)
			{
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				keyTouch = -1;
				if (cmdClose != null) {
					cmdClose.performAction();
				}
			}
		for (int i = 0; i < listText.length; i++) {
			if (GameCanvas.isPointerJustRelease &&
					GameCanvas.isPoint(xpaint + 20, ypaint + 31 + 20 * i, wkhung - 40, 18)) {
				GameCanvas.isPointerJustRelease = false;
				if (i == MUSIC) {
					isListAuto[MUSIC] = !isListAuto[MUSIC];
					Music.isplayMusic = isListAuto[MUSIC];
					if (isListAuto[MUSIC]) {
						Music.resumeMusic();
					} else {
						Music.pauseMusic();
					}
				} else if (i == SOUND) {
					isListAuto[SOUND] = !isListAuto[SOUND];
					Music.isplaySound = isListAuto[SOUND];
				} else if (i == AUTO_DANH) {
					isListAuto[AUTO_DANH] = !isListAuto[AUTO_DANH];
					GameScreen.isAutoDanh = isListAuto[AUTO_DANH];

//				} else if (i == AUTO_NHAT)
//				{
//					isListAuto[AUTO_NHAT] = !isListAuto[AUTO_NHAT];
//					GameScreen.isAutoNhatItem = isListAuto[AUTO_NHAT];

				} else if (i == TYPE_MOVE) {
					isListAuto[TYPE_MOVE] = !isListAuto[TYPE_MOVE];
					GuiMain.isAnalog = isListAuto[TYPE_MOVE];
				} else if (i == LOW_GRAPHIC) {
					isListAuto[LOW_GRAPHIC] = !isListAuto[LOW_GRAPHIC];
					GameCanvas.isLowGraphic = isListAuto[LOW_GRAPHIC];
					GameCanvas.getInstance().loadBG(TileMap.getInstance().bg);

				}
				for (int j = 0; j < listAuto.length; j++) {
					listAuto[j] = (byte) (isListAuto[j] ? 1 : 0);
				}

				Rms.saveRMS(Rms.autoSetting, listAuto);
				break;
			}
		}
	}

	public void update()
	{
		super.update();
		GameScreen.getInstance().update();
	}

	public void paint(MGraphics g)
	{
		super.paint(g);
		GameScreen.getInstance().paint(g);
		g.setColor(0x000000, GameCanvas.opacityTab);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		g.disableBlending();
		Paint.paintFrameNaruto(xpaint, ypaint, wkhung, hKhung + 2, g);
		Paint.PaintBoxName("Cài Đặt", xpaint + wkhung / 2 - 40, ypaint, 80, g);
		for (int i = 0; i < listText.length; i++)
		{
			FontManager.getInstance().tahoma_7.drawString(g, listText[i], xpaint + 20, ypaint + 40 + 20 * i, 0);

			g.drawRegion(LoadImageInterface.imgCheckSetting, 0,
					(isListAuto[i] ? 1 : 0) * LoadImageInterface.imgCheckSetting.getHeight() / 2,
					LoadImageInterface.imgCheckSetting.getWidth(), LoadImageInterface.imgCheckSetting.getHeight() / 2,
					0, xpaint + wkhung - 30, ypaint + 45 + 20 * i, MGraphics.VCENTER | MGraphics.HCENTER);
		}
		cmdClose.paint(g);
	}

}
