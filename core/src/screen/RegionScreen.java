package screen;

import lib.FontManager;
import lib.LoadImageInterface;
import real.Service;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import lib.Command;
import lib.Image;
import model.IActionListener;
import model.Paint;
import model.Screen;
import model.Scroll;

public class RegionScreen extends Screen implements IActionListener{

	private static RegionScreen instance;
	private Command cmdClose;
	private int coutFc;

	public byte[][] listKhu;
	private int minKhu = 15;
	public Scroll srclist = new Scroll();
	public int wKhung = 170, hKhung = 150;
	public int xpaint, ypaint;

	public RegionScreen()
	{
		xpaint = GameCanvas.w / 2 - wKhung / 2;
		ypaint = GameCanvas.h / 2 - hKhung / 2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint + wKhung - LoadImageInterface.closeTab.width / 2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case 2:
				GameScreen.getInstance().switchToMe();
				break;
		}
	}

	public static RegionScreen getInstance()
	{
		if (instance == null) {
			instance = new RegionScreen();
		}
		return instance;
	}

	public void updateKey()
	{
		if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdClose))
			if (cmdClose != null)
			{
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				keyTouch = -1;
				if (cmdClose != null) {
					cmdClose.performAction();
				}
			}
		srclist.updateKey();
		srclist.updatecm();
		if (GameCanvas.isPointerJustRelease && srclist.selectedItem != -1 && srclist.selectedItem < listKhu.length)
		{
			Service.getInstance().RequestChangeRegion((byte) srclist.selectedItem);
			GameCanvas.isPointerJustRelease = false;
		}

		super.updateKey();
	}

	public  void update()
	{
		super.update();
		GameScreen.getInstance().update();
		if (GameCanvas.gameTick % 4 == 0)
		{
			coutFc++;
			if (coutFc > 2)
				coutFc = 0;
		}
	}

	public void paint(MGraphics g)
	{
		super.paint(g);
		GameScreen.getInstance().paint(g);
		Paint.paintFrameNaruto(xpaint, ypaint, wKhung, hKhung + 2, g);
		Paint.PaintBoxName("Khu", xpaint + wKhung / 2 - 40, ypaint, 80, g);
		cmdClose.paint(g);
		if (listKhu != null)
		{
			srclist.setStyle((listKhu.length > minKhu ? listKhu.length : minKhu) / 5,
					Image.getWidth(LoadImageInterface.ImgItem),
					xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
					wKhung - 30, hKhung - 32, true, 5);

			srclist.setClip(g, xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
					wKhung - 30, hKhung - 32);

			for (int i = 0; i < (listKhu.length > minKhu ? listKhu.length : minKhu) / 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					g.drawImage(LoadImageInterface.ImgItem,
							xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j,
							ypaint + 30 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2, 0);
					if (i * 5 + j < listKhu.length)
						if (listKhu[i * 5 + j][0] == 0)
							FontManager.getInstance().tahoma_7_green.drawString(g, i * 5 + j + 1 + "",
									xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
											Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
						else if (listKhu[i * 5 + j][0] == 1)
							FontManager.getInstance().tahoma_7_yellow.drawString(g, i * 5 + j + 1 + "",
									xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
											Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
						else
							FontManager.getInstance().tahoma_7_red.drawString(g, i * 5 + j + 1 + "",
									xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
											Image.getWidth(LoadImageInterface.ImgItem) / 2,
									ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
											Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
				}
			}
			if (srclist.selectedItem > 0 && srclist.selectedItem < listKhu.length)
			{
				Paint.paintFocus(g,
						xpaint + 15 + srclist.selectedItem % 5 * Image.getWidth(LoadImageInterface.ImgItem) + 11 -
								LoadImageInterface.ImgItem.getWidth() / 4,
						ypaint + 30 + srclist.selectedItem / 5 * Image.getHeight(LoadImageInterface.ImgItem) + 13 -
								LoadImageInterface.ImgItem.getWidth() / 4
						, LoadImageInterface.ImgItem.getWidth() - 9, LoadImageInterface.ImgItem.getWidth() - 9, coutFc);
			}
			GameCanvas.resetTrans(g);
		}
	}
}
