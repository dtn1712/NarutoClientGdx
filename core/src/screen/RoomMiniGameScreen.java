package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import Gui.GuiFriend;
import Objectgame.Char;
import lib.Command;
import lib.FontManager;
import lib.Image;
import lib.LoadImageInterface;
import lib.MFont;
import model.IActionListener;
import model.MResources;
import model.Paint;
import model.Screen;
import model.Scroll;
import real.Service;

public class RoomMiniGameScreen extends Screen implements IActionListener {


    public static boolean isRoomMiniGameScr;

    public boolean isReady;
    public static Scroll scrMain = new Scroll();

    public GuiFriend guiFriendInvite;


    /////  Action tuong tac
    private Command btnStartGame;
    private Command btnKickPlayerOutRoom;
    private Command btnOutRoom;
    private Command btnReady;
    private Command btnCancleReady;

    ///// Player in room
    public Char[] charInRoom;

    /// action btn
    private static final int START_GAME = 0;

    private static final int KICKPLAYER_GAME = 1;
    private static final int OUT_GAME = 2;
    private static final int READY = 5;
    private static final int CANCEL_READY = 6;

    public int indexRow;

    public int popupX, popupY, popupW, popupH = 220;
    public long timestartUpdate;

//        public static RoomMiniGameScr getInstanRoomMiniGameScr()
//        {
//            return InstanRoomMiniGameScr ?? (InstanRoomMiniGameScr = new RoomMiniGameScr());
//        }

    public RoomMiniGameScreen()
    {
        isRoomMiniGameScr = true;
        guiFriendInvite = new GuiFriend();
        charInRoom = new Char[4];
        charInRoom[0] = Char.myChar();
        popupX = GameCanvas.w / 2 - GameScreen.widthGui / 2 - 50;
        popupY = GameScreen.popupY;

        popupW = GameScreen.widthGui - 70;

        btnStartGame = new Command("Play game", this, START_GAME, null, 0, 0);
        btnKickPlayerOutRoom = new Command("Kích", this, KICKPLAYER_GAME, null, 0, 0);
        btnOutRoom = new Command("Rời phòng", this, OUT_GAME, null, 0, 0);
        btnReady = new Command("Sẵn sàn", this, READY, null, 0, 0);
        btnCancleReady = new Command("Huỷ", this, CANCEL_READY, null, 0, 0);
        btnStartGame.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 40, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
        btnKickPlayerOutRoom.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab), LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
        btnOutRoom.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
        btnReady.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
        btnCancleReady.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
    }

    public void perform(int idAction, Object p)
    {
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case START_GAME:
                break;
            case KICKPLAYER_GAME:
                break;
            case OUT_GAME:
                break;
            case READY:
                isReady = true;
                break;
            case CANCEL_READY:
                isReady = false;
                break;
        }
    }

    public  void updateKey()
    {
        // TODO Auto-generated method stub
        scrMain.updatecm();
        scrMain.updateKey();
        if (getCmdPointerLast(btnStartGame))
            if (btnStartGame != null)
            {
                GameCanvas.isPointerJustRelease = false;
                keyTouch = -1;
                if (btnStartGame != null)
                    btnStartGame.performAction();
            }
        //unfriend
        if (getCmdPointerLast(btnOutRoom))
            if (btnOutRoom != null)
            {
                GameCanvas.isPointerJustRelease = false;
                keyTouch = -1;
                if (btnOutRoom != null)
                    btnOutRoom.performAction();
            }

        if (getCmdPointerLast(btnKickPlayerOutRoom))
            if (btnKickPlayerOutRoom != null)
            {
                GameCanvas.isPointerJustRelease = false;
                keyTouch = -1;
                if (btnKickPlayerOutRoom != null)
                    btnKickPlayerOutRoom.performAction();
            }
    }

    public void update()
    {
        // TODO Auto-generated method stub
        if ((System.currentTimeMillis() - timestartUpdate) / 1000 > 30)
        {
            timestartUpdate = System.currentTimeMillis();
            Service.getInstance().requestFriendList(Char.myChar().charID);
        }
        if (scrMain.selectedItem == -1)
            return;
        indexRow = scrMain.selectedItem;
        if (indexRow < Char.myChar().vFriend.size() && Char.myChar().vFriend.size() > 0)
        {
            Char.toCharChat = (Char) Char.myChar().vFriend.elementAt(indexRow);
        }
    }

    public void paint(MGraphics g)
    {
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();

        if (guiFriendInvite != null)
        {
            guiFriendInvite.paint(g);
        }
        PaintMemberRoomMiniGame(g);

    }

    public void setPosClose(Command cmd)
    {
        cmd.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.closeTab), popupY,
                LoadImageInterface.closeTab,
                LoadImageInterface.closeTab);
    }


    private void PaintMemberRoomMiniGame(MGraphics g)
    {


        Paint.paintFrameNaruto(popupX, popupY, popupW, popupH, g);
        if (charInRoom.length > 0)
        {
            GameScreen.xstart = popupX + 5;
            GameScreen.ystart = popupY + 40;

            scrMain.setStyle(charInRoom.length, 50, GameScreen.xstart, GameScreen.ystart, popupW - 3, 180, true,
                    1);
            scrMain.setClip(g, GameScreen.xstart, GameScreen.ystart - 10, popupW - 3, 180);
            int yBGFriend = 0;
            for (int i = 0; i < charInRoom.length; i++)
            {
                Char c =  charInRoom[i];
                if (c != null)

                {
                    Paint.PaintBGListQuest(GameScreen.xstart + 20, GameScreen.ystart + yBGFriend, 120, g); //new quest
                    if (indexRow == i)
                        Paint.PaintBGListQuestFocus(GameScreen.xstart + 20, GameScreen.ystart + yBGFriend, 120, g);


                    FontManager.getInstance().tahoma_7_white.drawString(g, "Level: " + c.clevel, GameScreen.xstart + 73,
                            GameScreen.ystart + yBGFriend + 24, 0);

                    g.drawImage(LoadImageInterface.charPic, GameScreen.xstart + 35, GameScreen.ystart + yBGFriend + 20,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                    g.drawImage(LoadImageInterface.imgName, GameScreen.xstart + 90, GameScreen.ystart + yBGFriend + 15,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                    FontManager.getInstance().tahoma_7_white.drawString(g, c.cName, GameScreen.xstart + 63,
                            GameScreen.ystart + yBGFriend + 8, 0);


                    yBGFriend += 50;
                }

            }
            resetTranslate(g);
            if (Char.myChar().isHostRoom)
            {
                btnStartGame.paint(g);
                btnKickPlayerOutRoom.paint(g);
            }
            else
            {
                btnReady.paint(g);
                btnCancleReady.paint(g);
            }
            btnOutRoom.paint(g);

//                if (btnChat != null)
//                {
//                    btnChat.paint(graphic);
//                    btnUnfriend.paint(graphic);
//                }
        }
        else
        {
            FontManager.getInstance().tahoma_7_white.drawString(g, MResources.NO_FRIEND, popupX + popupW / 2, popupY + 40,
                    MFont.CENTER);
        }
        GameScreen.resetTranslate(g);
        Paint.PaintBoxName("PHÒNG CHỜ", popupX + 35, popupY, 100, g);

    }
}
