package screen;


import Gui.GuiMain;
import lib.LoadImageInterface;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import model.CRes;
import model.Res;

public class GamePad {
    private int deltaX, deltaY, delta, angle;
    private boolean isGamePad;
    private int xC, yC, xM, yM, xMLast, yMLast;
    private int R;
    private int r, d, xTemp, yTemp;

    public GamePad()
    {
        R = 18;
        xC = xM = 60;
        yC = GameCanvas.h - 52;
    }

    public void update()
    {
        if (GameCanvas.isPointerDown && !GameCanvas.isPointerJustRelease)
        {
            if (!isGamePad)
            {
                xM = xC;
                yM = yC;
            }

            if (xC <= 100 &&
                    yC >= GameCanvas.h - 100 && GameCanvas.px < 100 && GameCanvas.py >= GameCanvas.h - 100 &&
                    GameCanvas.py < GameCanvas.h)
            {
                isGamePad = true;
                delta = CRes.pow(deltaX, 2) + CRes.pow(deltaY, 2);
                d = CRes.sqrt(delta);

                if (CRes.abs(deltaX) > 4 || CRes.abs(deltaY) > 4)
                {
                    angle = CRes.angle(deltaX, deltaY);

                    if (!GameCanvas.isPointerHoldIn(xC - R, yC - R, 2 * R, 2 * R))
                    {
                        if (d != 0)
                        {
                            yM = deltaY * R / d;
                            xM = deltaX * R / d;
                            xM += xC;
                            yM += yC;
                            if (!CRes.inRect(xC - R, yC - R, 2 * R, 2 * R, xM,
                                    yM))
                            {
                                xM = xMLast;
                                yM = yMLast;
                            }
                            else
                            {
                                xMLast = xM;
                                yMLast = yM;
                            }
                        }
                        else
                        {
                            xM = xMLast;
                            yM = yMLast;
                        }
                    }
                    else
                    {
                        xM = GameCanvas.px;
                        yM = GameCanvas.py;
                    }
                    resetHold();

                    if (angle <= 360 && angle > 340 || angle > 0 && angle <= 25)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[6] = true;
                        GameCanvas.keyPressed[6] = true;
                    }
                    else if (angle > 25 && angle <= 70)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[6] = true;
                        GameCanvas.keyPressed[6] = true;
                    }

                    else if (angle > 70 && angle <= 115)
                    {
                        GameCanvas.keyHold[8] = true;
                        GameCanvas.keyPressed[8] = true;
                    }
                    else if (angle > 115 && angle <= 160)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[4] = true;
                        GameCanvas.keyPressed[4] = true;
                    }

                    else if (angle > 160 && angle <= 205)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[4] = true;
                        GameCanvas.keyPressed[4] = true;
                    }
                    else if (angle > 205 && angle <= 250)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[1] = true;
                        GameCanvas.keyPressed[1] = true;
                    }
                    else if (angle > 250 && angle <= 295)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[2] = true;
                        GameCanvas.keyPressed[2] = true;
                    }
                    else if (angle > 295 && angle <= 340)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[3] = true;
                        GameCanvas.keyPressed[3] = true;
                    }
                }
            }
        }
        else
        {
            ResetMove();
        }
        if (GameCanvas.isPointerDown &&
                !GameCanvas.isPointerJustRelease &&
                GameCanvas.pxFirst < xC + 50 &&
                GameCanvas.pyFirst >= yC - 50 &&
                GameCanvas.pyFirst < GameCanvas.h)
        {
            if (GameCanvas.pxFirst < xC + 50 && GameCanvas.pyFirst >= yC - 50)
            {
                xTemp = GameCanvas.pxFirst;
                yTemp = GameCanvas.pyFirst;
            }
            if (xTemp <= 100 && yTemp >= GameCanvas.h - 100)
            {
                if (!isGamePad)
                {
                    xC = xM = xTemp;
                    yC = yM = yTemp;
                }
                isGamePad = true;

                deltaX = GameCanvas.px - xC;
                deltaY = GameCanvas.py - yC;

                delta = CRes.pow(deltaX, 2) + CRes.pow(deltaY, 2);
                d = CRes.sqrt(delta);

                if (CRes.abs(deltaX) > 4 || CRes.abs(deltaY) > 4)
                {
                    angle = CRes.angle(deltaX, deltaY);

                    if (!GameCanvas.isPointerHoldIn(xC - R, yC - R, 2 * R, 2 * R))
                    {
                        if (d != 0)
                        {
                            yM = deltaY * R / d;
                            xM = deltaX * R / d;
                            xM += xC;
                            yM += yC;
                            if (!CRes.inRect(xC - R, yC - R, 2 * R, 2 * R, xM,
                                    yM))
                            {
                                xM = xMLast;
                                yM = yMLast;
                            }
                            else
                            {
                                xMLast = xM;
                                yMLast = yM;
                            }
                        }
                        else
                        {
                            xM = xMLast;
                            yM = yMLast;
                        }
                    }
                    else
                    {
                        xM = GameCanvas.px;
                        yM = GameCanvas.py;
                    }
                    resetHold();

                    if (angle <= 360 && angle > 340 || angle > 0 && angle <= 25)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[6] = true;
                        GameCanvas.keyPressed[6] = true;
                    }
                    else if (angle > 25 && angle <= 70)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[6] = true;
                        GameCanvas.keyPressed[6] = true;
                    }

                    else if (angle > 70 && angle <= 115)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[8] = true;
                        GameCanvas.keyPressed[8] = true;
                    }
                    else if (angle > 115 && angle <= 160)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[4] = true;
                        GameCanvas.keyPressed[4] = true;
                    }

                    else if (angle > 160 && angle <= 205)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[4] = true;
                        GameCanvas.keyPressed[4] = true;
                    }
                    else if (angle > 205 && angle <= 250)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[1] = true;
                        GameCanvas.keyPressed[1] = true;
                    }
                    else if (angle > 250 && angle <= 295)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[2] = true;
                        GameCanvas.keyPressed[2] = true;
                    }
                    else if (angle > 295 && angle <= 340)
                    {
                        GameScreen.getInstance().auto = 0;
                        GameCanvas.keyHold[3] = true;
                        GameCanvas.keyPressed[3] = true;
                    }
                }
            }
        }
        else
        {
            ResetMove();
        }
    }

    public void ResetMove()
    {
        xC = (GuiMain.xL + GuiMain.xR) / 2;
        yC = (GuiMain.yU + GuiMain.yU) / 2;
        xM = xC;
        yM = yC;
        isGamePad = false;
        resetHold();
    }


    private void resetHold()
    {
        if (GuiMain.isAnalog &&
                (GameCanvas.isPointerDown || GameCanvas.isPointerClick || GameCanvas.isPointerJustRelease) &&
                GameCanvas.px >= xC - 50 && GameCanvas.px <= xC + 50 && GameCanvas.py >= yC - 50)
        {
            GameCanvas.keyPressed[1] = false;
            GameCanvas.keyPressed[2] = false;
            GameCanvas.keyPressed[3] = false;
            GameCanvas.keyPressed[4] = false;
            GameCanvas.keyPressed[6] = false;
            GameCanvas.keyPressed[7] = false;
            GameCanvas.keyPressed[8] = false;
            GameCanvas.keyPressed[9] = false;

            GameCanvas.keyHold[1] = false;
            GameCanvas.keyHold[2] = false;
            GameCanvas.keyHold[3] = false;
            GameCanvas.keyHold[4] = false;
            GameCanvas.keyHold[6] = false;
            GameCanvas.keyHold[7] = false;
            GameCanvas.keyHold[8] = false;
            GameCanvas.keyHold[9] = false;
//            if (GameCanvas.isPointerJustRelease)
//                GameCanvas.clearAllPointerEvent();
        }
    }

    public void paint(MGraphics g)
    {
        g.drawRegion(LoadImageInterface.imgHotKey[0], 0,
                isGamePad == false ? 0 : MGraphics.getImageHeight(LoadImageInterface.imgHotKey[0]) / 2,
                MGraphics.getImageWidth(LoadImageInterface.imgHotKey[0]),
                MGraphics.getImageHeight(LoadImageInterface.imgHotKey[0]) / 2, 0, xC, yC,
                MGraphics.HCENTER | MGraphics.VCENTER);

        g.drawRegion(LoadImageInterface.imgHotKey[1], 0,
                isGamePad == false ? 0 : MGraphics.getImageHeight(LoadImageInterface.imgHotKey[1]) / 2,
                MGraphics.getImageWidth(LoadImageInterface.imgHotKey[1]),
                MGraphics.getImageHeight(LoadImageInterface.imgHotKey[1]) / 2, 0, xM, yM,
                MGraphics.HCENTER | MGraphics.VCENTER);
    }
}
