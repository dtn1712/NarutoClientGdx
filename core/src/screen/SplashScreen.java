package screen;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import lib.MBitmap;
import model.Screen;

public class SplashScreen extends Screen{
	public static MBitmap imgLogo;
	private static int splashScrStat;
	private static int time;
	private boolean isSwitch = false;

	public static void loadSplashScreen()
	{
		splashScrStat = 0;
		time = 80;
	}

	public void update()
	{
		if (splashScrStat >= time && !isSwitch)
		{
			GameCanvas.getInstance().loginScreen.switchToMe();
			isSwitch = true;
		}
		splashScrStat++;
	}

	public void paint(MGraphics g)
	{
		g.setColor(0xffffff);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

		if (imgLogo != null)
		{
			g.drawImage(imgLogo, GameCanvas.w >> 1, GameCanvas.h >> 1, MGraphics.HCENTER | MGraphics.VCENTER);
		}
	}
}
