package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import Objectgame.Char;
import lib.Command;
import lib.FontManager;
import lib.Image;
import lib.LoadImageInterface;
import model.IActionListener;
import model.MResources;
import model.Part;
import model.Screen;
import model.SmallImage;
import real.Service;

public class SelectCharScreen extends Screen implements IActionListener {
	
	private static SelectCharScreen instance;

	public static int w1char, h1char, padchar, x, y, indexSelect;
	public long[] charIDDB;

	public int dem;
	public byte[] gender, type;

	private int gsgreenField1Y;
	public boolean isLoadImg;
	public int[] lv;
	public String[] name;
	public int[] parthead, partleg, partbody, partWp, level;
	public String[] phai;

	private int waitToPerform;

	private SelectCharScreen()
	{
		w1char = 48;
		h1char = 85;
		if (GameCanvas.w < 160)
		{
			w1char = 32;
		}

		padchar = 7;
		x = ((GameCanvas.w - 3 * w1char) >> 1) - 5;
		y = GameCanvas.hh - (h1char >> 1) + 10;
		if (GameCanvas.isTouch && GameCanvas.w > 200)
		{
			w1char = 74;
			padchar = 25;
			h1char = 110;
			x = ((GameCanvas.w - 3 * w1char) >> 1) - 20;
			y = GameCanvas.hh - (h1char >> 1);

			if (GameCanvas.w < 320)
			{
				padchar = 6;
				x = ((GameCanvas.w - 3 * w1char) >> 1) - 6;
			}
		}
		y = GameCanvas.h - MGraphics.getImageHeight(LoadImageInterface.imgTatus) - 20;
		y = y < 0 ? 0 : y;
		left = null;
		center = new Command("", this, 1000, null);
		right = new Command(MResources.EXIT, this, 1001, null);
		right.setPos(GameCanvas.w - LoadImageInterface.img_use.getWidth() - 2,
				GameCanvas.h - LoadImageInterface.img_use.getHeight() - 2, LoadImageInterface.img_use,
				LoadImageInterface.img_use_focus);
	}

	public void perform(int idAction, Object p)
	{
		switch (idAction)
		{
			case 1000:
				doSelect();
				break;
			case 1001:
				GameCanvas.getInstance().resetToLoginScreen(true);
				break;
		}
	}

	public static SelectCharScreen getInstance()
	{
		if (instance == null) {
			instance = new SelectCharScreen();
		}
		return instance;
	}

	public void initSelectChar()
	{
		charIDDB = new long[3];
		name = new String[3];
		parthead = new int[3];
		partleg = new int[3];
		partbody = new int[3];
		partWp = new int[3];
		level = new int[3];
		lv = new int[3];
		phai = new String[3];
		gender = new byte[3];
		type = new byte[3];
		indexSelect = (GameCanvas.isTouch) ?  -1 :  0;

		GameScreen.readPart(); // đọc part
		SmallImage.init(); // đọc dữ liệu hình ảnh
	}

	private void doSelect()
	{
		if (indexSelect < name.length) {
			if (name[indexSelect] != null) {
				Service.getInstance().SelectChar(charIDDB[indexSelect]);
				GameCanvas.getInstance().startWaitDlg(MResources.PLEASEWAIT);
			} else {
				CreateCharScreen.getInstance().switchToMe();
			}
		}
		indexSelect = -1;
	}

	public void updateKey()
	{

//		super.updateKey();
//
//		if (GameCanvas.keyPressed[6])
//		{
//			indexSelect++;
//			if (indexSelect >= 3)
//				indexSelect = 0;
//		}
//		if (GameCanvas.keyPressed[4])
//		{
//			indexSelect--;
//			if (indexSelect < 0)
//				indexSelect = 2;
//		}
//
//		if (GameCanvas.isPointerDown)
//			if (GameCanvas.isPointerHoldIn(x, y + 140, 3 * (w1char + padchar), h1char))
//			{
//				int index = (GameCanvas.px - x) / (w1char + padchar);
//				if (index > 2)
//					index = 2;
//				if (index < 0)
//					index = 0;
//				indexSelect = index;
//			}
//		if (GameCanvas.isPointerJustRelease)
//		{
//			if (GameCanvas.isPointer(x, y + 140, 3 * (w1char + padchar), h1char))
//				waitToPerform = 5;
//			else
//				indexSelect = -1;
//			GameCanvas.isPointerJustRelease = false;
//		}
//		GameCanvas.clearKeyHold();
//		GameCanvas.clearKeyPressed();


		super.updateKey();
//		if (GameCanvas.getInstance().currentDialog != null)
//			return;


		if (GameCanvas.keyPressed[6]) {
			indexSelect++;
			if (indexSelect >= 3)
				indexSelect = 0;
		}
		if (GameCanvas.keyPressed[4]) {
			indexSelect--;
			if (indexSelect < 0)
				indexSelect = 2;
		}

		if (GameCanvas.isPointerDown) {
			if (GameCanvas.isPointerHoldIn(x, y + 140, 3 * (w1char + padchar), h1char)) {
				int index = (GameCanvas.px - x) / (w1char + padchar);
				if (index > 2)
					index = 2;
				if (index < 0)
					index = 0;
				indexSelect = index;
			}
		}
		if (GameCanvas.isPointerJustRelease) {
			if (GameCanvas.isPointer(x, y + 140, 3 * (w1char + padchar), h1char)) {
				waitToPerform = 5;
			} else
				indexSelect = -1;
			GameCanvas.isPointerJustRelease = false;
		}

		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
	}

	public void update()
	{
		if(GameCanvas.imgCloud==null||!isLoadImg){
			isLoadImg = GameCanvas.getInstance().loadBG(1);
		}
		GameScreen.cmx++;
		dem++;
		if(dem >= 1000)
			dem = 0;
		if (GameScreen.cmx > GameCanvas.w * 3 + 100)
			GameScreen.cmx = 100;

		if (waitToPerform > 0) {
			waitToPerform--;
			if (waitToPerform == 0) {

				if (indexSelect >= 0)
					doSelect();
			}
		}
	}


	public void switchToMe()
	{
		isLoadImg = GameCanvas.getInstance().loadBG(1);
		gsgreenField1Y = GameScreen.gH - Image.getHeight(LoadImageInterface.imgTrangtri) + 170;
//		System.gc();
		super.switchToMe();
	}


	public void paint(MGraphics g)
	{
		GameCanvas.getInstance().paintBGGameScreen(g);
		g.drawImage(LoadImageInterface.imgTatus, GameCanvas.w / 2, y + 200, MGraphics.HCENTER | MGraphics.BOTTOM);

		for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(LoadImageInterface.imgTrangtri));
			 i < GameScreen.gW;
			 i += Image.getWidth(LoadImageInterface.imgTrangtri))
			g.drawImage(LoadImageInterface.imgTrangtri, i, gsgreenField1Y - 150, 0);
		for (int i = 0; i < 3; i++)
			g.drawImage(LoadImageInterface.imgRock, x + i * (w1char + padchar), y + 175, 0);

		for (int i = 0; i < 3; i++)
		{
			if (name[i] == null)
				continue;


			Part ph = GameScreen.parts[parthead[i]],
					pl = GameScreen.parts[partleg[i]],
					pb = GameScreen.parts[partbody[i]];

			int cx = x + i * (w1char + padchar + 2) + w1char / 2;
			if (!GameCanvas.isTouch)
			{
				if (indexSelect == i)
				{
					FontManager.getInstance().tahoma_8b.drawStringShadow(g, MResources.CHARINGFO[0] + ": " + name[i], GameCanvas.hw - 15,
							y - 45, 0);
					FontManager.getInstance().tahoma_7b_white.drawStringBorder(g, MResources.CHARINGFO[1] + ": " + lv[i], GameCanvas.hw - 15,
							y - 28, 0);
				}
			}
			else
			{
				int cy = y + h1char / 2 - 15;
				g.drawImage(LoadImageInterface.bongChar,
						cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][1] +
								pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dx - 5,
						cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][2] +
								pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dy + 150, 0);

				SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].id,
						cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][1] +
								pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dx,
						cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][2] +
								pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dy + 140, 0, 0);
				SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].id,
						cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][1] +
								pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].dx,
						cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][2] +
								pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].dy + 140, 0, 0);
				SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].id,
						cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][1] +
								ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].dx,
						cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][2] +
								ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].dy + 140, 0, 0);

				FontManager.getInstance().tahoma_8b.drawStringShadow(g, name[i], cx - 15, y + h1char / 2 + 55, 0);
				FontManager.getInstance().tahoma_7b_white.drawStringShadow(g, MResources.CHARINGFO[1] + ": " + lv[i], cx - 15,
						y + h1char / 2 + 72, 0);
			}
		}
		super.paint(g);
	}
}
