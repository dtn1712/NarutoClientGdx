package screen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.MGraphics;

import java.util.List;

import Gui.Constants;
import Objectgame.minigame.MiniGameInfo;
import Objectgame.minigame.MiniGameManager;
import lib.Command;
import lib.FontManager;
import lib.LoadImageInterface;
import model.IActionListener;
import model.Key;
import model.Paint;
import model.Screen;
import model.Scroll;
import real.Service;

public class ListMiniGameScreen extends Screen implements IActionListener {

    private static final int PLAY_GAME_CMD = 1;
//        private static final int CLOSE_CMD = 2;

    private static Scroll scrMainListMiniGame = new Scroll();
    private static Scroll scrMainGuide = new Scroll();

    private Command btnPlayGame;
    private Command btnClose;

    public int popupX, popupY, popupW, popupH = 220;

    private int _indexRow;


    public ListMiniGameScreen()
    {
        popupX = GameCanvas.w / 2 - GameScreen.widthGui / 2 - 50;
        popupY = GameScreen.popupY;
        popupW = GameScreen.widthGui - 70;
        btnPlayGame = new Command("Play", this, PLAY_GAME_CMD,  null, 0, 0);
        btnPlayGame.setPos(popupX + popupW / 2 - 30, popupY + popupH - 35, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
//            btnClose = new Command("", this, CLOSE_CMD, null, 0, 0);
//            btnClose.setPos(popupX + popupW * 2 - 10, popupY, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }

    public void updateKey()
    {
        if (btnPlayGame != null && getCmdPointerLast(btnPlayGame))
        {
            btnPlayGame.performAction();
        }

//            if (btnClose != null && getCmdPointerLast(btnClose))
//            {
//                btnClose.performAction();
//            }

        scrMainListMiniGame.updateKey();
        scrMainGuide.updateKey();
        super.updateKey();
    }

    public void update()
    {
        updateKey();
        if (GameCanvas.keyPressed[Key.NUM8])
        {
            _indexRow++;
            if (_indexRow >= MiniGameManager.getInstance().MiniGameList.size())
            {
                _indexRow = MiniGameManager.getInstance().MiniGameList.size() - 1;
            }
            scrMainListMiniGame.moveTo(_indexRow * scrMainListMiniGame.ITEM_SIZE);
        }
        else if (GameCanvas.keyPressed[Key.NUM2])
        {
            _indexRow--;
            if (_indexRow < 0)
            {
                _indexRow = 0;
            }
            scrMainListMiniGame.moveTo(_indexRow * scrMainListMiniGame.ITEM_SIZE);
        }
        scrMainListMiniGame.updatecm();
        scrMainGuide.updatecm();

        if (MiniGameManager.getInstance().MiniGameList.size() == 0 && scrMainListMiniGame.selectedItem != -1)
        {
            _indexRow = scrMainListMiniGame.selectedItem;
            scrMainListMiniGame.selectedItem = -1;
        }
        else if (MiniGameManager.getInstance().MiniGameList.size() != 0 && scrMainListMiniGame.selectedItem != -1)
        {
            _indexRow = scrMainListMiniGame.selectedItem;
            scrMainListMiniGame.selectedItem = -1;
        }
    }

    public void perform(int idAction, Object p)
    {
        switch (idAction)
        {
            case PLAY_GAME_CMD:
                Service.getInstance().JoinMiniGame(MiniGameManager.getInstance().MiniGameList.get(_indexRow).id);
                break;
        }
    }

    public void paint(MGraphics g)
    {
        if (GameScreen.getInstance().guiMain.menuIcon.indexpICon != Constants.ICON_MINIGAME)
        {
            return;
        }

        resetTranslate(g);
        Paint.paintFrameNaruto(popupX + popupW, popupY, popupW, popupH, g);
        Paint.SubFrame(popupX, popupY, popupW, popupH, g);
        Paint.PaintBoxName("DANH SÁCH GAME", popupX + popupW + 35, popupY + 10, 100, g);
        Paint.PaintBoxName("MÔ TẢ", popupX + 35, popupY + 10, 100, g);
//            btnClose.paint(graphic);

        List<MiniGameInfo> miniGameList = MiniGameManager.getInstance().MiniGameList;

        if (miniGameList != null)
        {
            int yBGinfoGame = 0;
            for (int i = 0; i < miniGameList.size(); i++)
            {
                Paint.PaintBGListQuest(popupX + popupW+ 25, popupY + 45 + yBGinfoGame, 120, g); //new quest	

                if (_indexRow == i)
                {
                    btnPlayGame.paint(g);
                    Paint.PaintBGListQuestFocus(popupX + 25 + popupW, popupY + 45 + yBGinfoGame, 120, g);
                    scrMainListMiniGame.setStyle(miniGameList.size(), 50, popupX + 10 + popupW, popupY + 40,  popupW, popupH, true, 1);
                    scrMainListMiniGame.setClip(g, popupX + 10 + popupW, popupY + 40, popupW, popupH - 30);
                    scrMainGuide.setStyle(miniGameList.size(), 50, popupX + 10 , popupY + 40,  popupW, popupH, true, 1);
                    scrMainGuide.setClip(g, popupX + 10, popupY + 40, popupW, popupH - 30);

                    String[] text = FontManager.getInstance().tahoma_7b_yellow.splitFontArray(miniGameList.get(i).description, 140);
                    for (int j = 0; j < text.length; j++)
                    {
                        FontManager.getInstance().tahoma_7_white.drawString(g, text[j], popupX + 10, (popupY + 40) + j * 10, 0);
                    }

                    if (miniGameList.get(i).feeAmount > 0)
                    {
                        int x = popupX + 10;
                        int y = popupY + 40 + (text.length + 1) * 10;
                        String feeText = "Phí chơi: " + miniGameList.get(i).feeAmount + " " + miniGameList.get(i).feeType;
                        FontManager.getInstance().tahoma_7_white.drawString(g, feeText, x, y, 0);
                    }

                }
                resetTranslate(g);

                g.drawImage(LoadImageInterface.charPic, popupX + popupW + 35, popupY + 65 + yBGinfoGame,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                g.drawImage(LoadImageInterface.imgName, popupX + popupW + 90, popupY + 55 + yBGinfoGame,
                        MGraphics.VCENTER | MGraphics.HCENTER);

                FontManager.getInstance().tahoma_7_white.drawString(g,  miniGameList.get(i).name, popupX + popupW + 65,
                        popupY + 47 + yBGinfoGame, 0);
                FontManager.getInstance().tahoma_7_white.drawString(g, "Người chơi: tối đa " + miniGameList.get(i).totalPlayers, popupX + popupW + 65,
                        popupY + 60 + yBGinfoGame, 0);
                FontManager.getInstance().tahoma_7_white.drawString(g, "Nhóm: tối đa " + miniGameList.get(i).groupPlayers, popupX + popupW + 65,
                        popupY + 71 + yBGinfoGame, 0);

                yBGinfoGame += 50;
            }
        }
        super.paint(g);
    }

    public void SetPosClose(Command cmd)
    {
        cmd.setPos(popupX + popupW * 2 - 10, popupY, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }
    
    
}
