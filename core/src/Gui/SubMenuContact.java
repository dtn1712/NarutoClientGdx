package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.Friend;
import lib.Cout;
import real.Service;
import screen.old.GameScr;
import model.Command;
import model.Image;
import model.Paint;
import model.Screen;

public class SubMenuContact extends MenuIcon {

	public static int width2=72;
	int height=50;
	Command iconSubFriend,iconSubParty;//sun contact
	

	
	public SubMenuContact(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
		
		this.x=x;
		this.y=y;
		InitComand();
	}
	@Override
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		//Image.getWidth(loadImageInterface.imgSubMenu[0])
		Cout.println(getClass(), "submenu setposclose");
		cmd.setPos(x+Image.getWidth(loadImageInterface.imgSubMenu[0])*7-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
	}
	public void InitComand() {
		// TODO Auto-generated method stub
		int xx=0;
		//trade
		xx=width2+this.x-37;
		iconSubFriend= new Command("", this, Contans.ICON_SUB_FRIEND, null, 0, 0);
		iconSubFriend.setPos(xx,y, loadImageInterface.imgFriendIcon, loadImageInterface.imgFriendIcon);
		
		xx=width2+this.x+3;
		iconSubParty= new Command("", this, Contans.ICON_SUB_TEAM, null, 0, 0);
		iconSubParty.setPos(xx,y, loadImageInterface.imgTeamIcon, loadImageInterface.imgTeamIcon);
		
	}
	public void update() {
		// TODO Auto-generated method stub
		super.update();
	}
	public void updateKey() {
		// TODO Auto-generated method stub
		//click sun friend
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconSubFriend)) {
			
			if (iconSubFriend != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (iconSubFriend != null)
				{
					iconSubFriend.performAction();	
				}
			}
		}
		
		//click sub party
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconSubParty)) {

			if (iconSubParty != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (iconSubParty != null)
				{
					iconSubParty.performAction();	
				}
			}
		}
		
		//click sub party
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {

			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();	
				}
			}
		}
		GameCanvas.clearKeyPressed();
	}
	public void updatePointer() {
		// TODO Auto-generated method stub
		super.updatePointer();
		if (GameCanvas.isPointerDown) {
			if (!GameCanvas.isPointSelect(this.x,this.y,width2*2,height)) {
				MenuIcon.isCloseSub=true;
			}
			
		}
	}

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
//		super.perform(idAction, p);
		Cout.println(getClass(), " idAction "+idAction);
		switch (idAction) {
		
		case Contans.ICON_SUB_FRIEND:

			GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
			MenuIcon.lastTab.add(""+Contans.ICON_SUB_FRIEND);
			Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
			GameScr.gI().guiMain.menuIcon.friend =new GuiFriend();
			GameScr.gI().guiMain.menuIcon.indexpICon = Contans.ICON_SUB_FRIEND;
			GameScr.gI().guiMain.menuIcon.friend.SetPosClose(GameScr.gI().guiMain.menuIcon.cmdClose);
			GameScr.gI().guiMain.menuIcon.paintButtonClose=true;
//			GameScr.getInstance().guiMain.menuIcon.subMenu = null;
//			GameScr.isPaintFriend = true;
//			MenuIcon.isCloseSub=true;
			break;
		case Contans.ICON_SUB_TEAM:

			GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
			GameScr.gI().guiMain.menuIcon.indexpICon = Contans.ICON_TEAM;
			if(GameScr.gI().guiMain.menuIcon.party==null)
				GameScr.gI().guiMain.menuIcon.party=new TabParty(GameCanvas.hw, 20);

			MenuIcon.lastTab.add(""+Contans.ICON_TEAM);
			GameScr.gI().guiMain.menuIcon.paintButtonClose=true;
			GameScr.gI().guiMain.menuIcon.party.SetPosClose(GameScr.gI().guiMain.menuIcon.cmdClose);
//			GameScr.getInstance().guiMain.menuIcon.subMenu = null;
//			GameScr.getInstance().guiMain.menuIcon.indexpICon = 0;
//			MenuIcon.isCloseSub=true;
//			GameScr.isPaintTeam = true;
			break;
			
		case Contans.CMD_TAB_CLOSE:

			GameScr.gI().guiMain.menuIcon.indexpICon = 0;
			GameScr.isPaintTeam=false;
			GameScr.isPaintFriend=false;
			break;
		}
	}

	public void paint(MGraphics g) {
		// TODO Auto-generated method stub
		GameScr.resetTranslate(g);
		Paint.PaintBGSubIcon(x,y,2,g);
		
		iconSubFriend.paint(g);
		iconSubParty.paint(g);
		
		
	}
}
