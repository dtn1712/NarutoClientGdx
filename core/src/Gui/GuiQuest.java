package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Quest;
import real.mFont;
import screen.old.GameScr;
import lib.Cout;
import lib.mVector;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Paint;
import model.Screen;

public class GuiQuest extends Screen implements IActionListener {
	public mVector VecTabScreen = new mVector();
	
	int x,y,width=180,height=232,widthSub=170,heightSub=230;
	int indexSize;
	int wScollReceived=100,hScollReceived=60;
	int indexRow=-1,indexRowUnRecive;
	public static mVector listNhacNv = new mVector();
	String nameMenu[]={"Chính","Phụ","Phó bản"};
	int xMenu,yMenu;
	int wMenu,hMenu;
	public int indexselectNV = 0;//0 nv moi, 1 nv da nhan
	public int selectTab = 0;
	
	int xM;//diem nam giua frame
	
	public GuiQuest(int x, int yy)
	{
		indexselectNV = 0;
		int wall = width+35+widthSub;
		this.x=(GameCanvas.w/2-wall/2);
		this.y=(GameCanvas.h/2-heightSub/2<0?0:GameCanvas.h/2-heightSub/2);
		this.xM=this.x+width/2;

		xMenu=this.x+width-25;
		yMenu=y+40;
	
		wMenu=Image.getWidth(loadImageInterface.btnTab);
		hMenu=Image.getHeight(loadImageInterface.btnTab);
		
		indexSize=mFont.tahoma_7_yellow.getHeight()+2;
		
		QMain qMain= new QMain();
		VecTabScreen.addElement(qMain);
		
		QMain qMain1= new QMain();
		VecTabScreen.addElement(qMain1);
		
		QMain qMain2= new QMain();
		VecTabScreen.addElement(qMain2);
		
	}
	
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(x+width-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}
	public void updateKey() {
//		x+40,y+70,160
		if(GameCanvas.isPointer(x+40,y+70,160, Image.getHeight(loadImageInterface.bgQuestLineFocus))){
			indexselectNV = 0;
			GameCanvas.isPointerClick= false;
		}
		else if(GameCanvas.isPointer(x+40,y+165,160, Image.getHeight(loadImageInterface.bgQuestLineFocus))){
			indexselectNV = 1;
			GameCanvas.isPointerClick= false;
		}
	}
	
	
	public void updatePointer() {
		// TODO Auto-generated method stub
		int ySelect=yMenu;
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=0;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=1;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=2;
			GameCanvas.isPointerClick= false;
			
		}
	}


	public void paint(MGraphics g)
	{
		QuestMain questMain= (QuestMain) VecTabScreen.elementAt(selectTab);
		GameScr.resetTranslate(g);
		GameCanvas.paint.paintFrameNaruto(this.x,this.y, this.width,this.height,g);
		Paint.PaintBoxName(questMain.nameTab,x+55,20,width-110,g);
		Paint.SubFrame(x+width+35,y,widthSub,heightSub, g);//sub
		// thong tin quest focus 
		if(indexselectNV==0
				&&(Quest.listUnReceiveQuest.size()>0)
				)
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
			Quest q = (Quest)Quest.listUnReceiveQuest.elementAt(i);
			paintInfoQuestFocus(g, x+width+45,y+5, q,(byte)0);
		}
		//new quest
		g.drawImage(loadImageInterface.imgName,xM,y+50,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, "Mới",xM,y+45,2);
		Paint.PaintBGListQuest(x+30,y+70,width-50,g);//new quest
		if(indexselectNV==0&&Quest.listUnReceiveQuest.size()>0&&(GameCanvas.gameTick/10)%2==0)
			Paint.PaintBGListQuestFocus(x+30,y+70,width-50,g);//new focus quest
		//paint ds nv moi
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
			Quest q = (Quest)Quest.listUnReceiveQuest.elementAt(i);
			mFont.tahoma_7b_yellow.drawString(g,q.name,
					x+30,y+75,0);
		}
		Paint.PaintLine(x+5,y+125,width-30, g);//line
		
		//reciviced
		g.drawImage(loadImageInterface.imgName,xM,y+145,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, "Đã nhận",xM,y+140,2);
		Paint.PaintBGListQuest(x+30,y+165,width-50,g);//dang lam or hoan thanh
		if(indexselectNV==1
				&&(Quest.vecQuestDoing_Main.size()>0||Quest.vecQuestFinish.size()>0)
				&&(GameCanvas.gameTick/10)%2==0)
			Paint.PaintBGListQuestFocus(x+30,y+165,width-50,g);//new focus quest
		//paint ds nv dg lam
		for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
			Quest q = (Quest)Quest.vecQuestDoing_Main.elementAt(i);
			mFont.tahoma_7b_yellow.drawString(g,q.name,
					x+30,y+170,0);
		}
		// thong tin quest dg lamfocus 
		if(indexselectNV==1
				&&(Quest.vecQuestDoing_Main.size()>0)
				)
				for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
					Quest q = (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					paintInfoQuestFocus(g, x+width+45,y+5, q,(byte)1);
				}
		for(int i=0;i<VecTabScreen.size();i++)
		{
			g.drawImage(loadImageInterface.btnTab, xMenu,yMenu+(5+Image.getHeight(loadImageInterface.btnTab))*i, 0);
		}
		
		g.drawImage(loadImageInterface.btnTabFocus,xMenu,yMenu+(5+Image.getHeight(loadImageInterface.btnTabFocus))*selectTab, 0);
		for(int i=0;i<VecTabScreen.size();i++)
		{
			mFont.tahoma_7b_white.drawString(g,nameMenu[i],xMenu+32,yMenu+Image.getHeight(loadImageInterface.btnTab)/4+(5+Image.getHeight(loadImageInterface.btnTab))*i,2);
		}
		
	}
	public void paintInfoQuestFocus(MGraphics g, int x, int y, Quest q, byte typequest){ // 0 co the nha,1dg lam//2 hoan thanh
		//name
		g.drawImage(loadImageInterface.imgName,x+30,y+10,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,"Nhiệm vụ",x+30,y+5,2);
		mFont.tahoma_7b_yellow.drawString(g,q.name,x+15,y+20,0);
		// mo ta
		g.drawImage(loadImageInterface.imgName,x+30,y+40,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,"Mô tả",x+30,y+35,2);
		if(q.strPaintDetailHelp!=null)
		for (int i = 0; i < q.strPaintDetailHelp.length; i++) {
			mFont.tahoma_7_white.drawString(g,q.strPaintDetailHelp[i],x+15,y+50+12*i,0);
		}
		switch (typequest) {
		case 0:
			// qua
			g.drawImage(loadImageInterface.imgName,x+30,y+65+12*q.strPaintDetailHelp.length,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Thưởng",x+30,y+60+12*q.strPaintDetailHelp.length,2);
			//mFont.tahoma_7_white.drawString(graphic,q.strGift,x+15,y+80+12*q.strPaintDetailHelp.length,0);
			if(q.luong>0){
				g.drawImage(loadImageInterface.coins[1], x+15,y+75+12*q.strPaintDetailHelp.length, MGraphics.TOP| MGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.luong+"",x+40,y+80+12*q.strPaintDetailHelp.length,0);
			}
			if(q.xu>0){
				g.drawImage(loadImageInterface.coins[0], x+15,y+75+(q.luong>0?20:0)+12*q.strPaintDetailHelp.length, MGraphics.TOP| MGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.xu+"",x+40,y+75+(q.luong>0?28:0)+12*q.strPaintDetailHelp.length,0);
			}
			
			break;
		case 1: //dag lam
			g.drawImage(loadImageInterface.imgName,x+30,y+60+12*q.strPaintDetailHelp.length,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Yêu cầu",x+30,y+55+12*q.strPaintDetailHelp.length,2);
//			mFont.tahoma_7_white.drawString(graphic,q.strGift,x+15,y+80+12*q.strPaintDetailHelp.length,0);
			if(q.strPaintDetailHelp!=null)
				for (int i = 0; i < q.strPaintDetailHelp.length; i++) {
					mFont.tahoma_7_white.drawString(g,q.strPaintDetailHelp[i],x+15,y+70+12*q.strPaintDetailHelp.length+12*i,0);
				}
			// qua
			g.drawImage(loadImageInterface.imgName,x+30,y+85+12*q.strPaintDetailHelp.length*2,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Thưởng",x+30,y+80+12*q.strPaintDetailHelp.length*2,2);
			if(q.luong>0){
				g.drawImage(loadImageInterface.coins[1], x+15,y+95+12*q.strPaintDetailHelp.length*2, MGraphics.TOP| MGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.luong+"",x+40,y+100+12*q.strPaintDetailHelp.length*2,0);
			}
			if(q.xu>0){
				g.drawImage(loadImageInterface.coins[0], x+15,y+95+(q.luong>0?20:0)+12*q.strPaintDetailHelp.length*2, MGraphics.TOP| MGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.xu+"",x+40,y+95+(q.luong>0?28:0)+12*q.strPaintDetailHelp.length*2,0);
			}
			break;
		default:
			break;
		}
		}
	public void perform(int idAction, Object p) {
	}

}
