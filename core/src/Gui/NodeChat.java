//package code.screen;
//
//import lib.Cout;
//import lib.TCanvas;
//import lib.MGraphics;
//import lib.mVector;
//import lib2.mFont;
//
//public class NodeChat {
//	public int type;
//	public static final byte t_TEXT = 0;
//	public static final byte t_EMO = 1;
//	public static final byte t_TEXTEMO = 2;
//	
//	public String[][] textEmo;
//	public String[] text;
//	public String textdai,name;
//	public static String mtextConvertEmo = "zpzp";
//	public int hnode,wname;
//	public boolean isMe;
//	public static int wnode = 100*TCanvas.TileZoom;
//	public mVector listEmoLine = new mVector();
//	
//	public static String[] maEmo = new String[]{ //14,
//			"z($)z","z:Oz","(highfive)","z8-)","z:Sz","(wait)","(oliver)","z(nod)","(facepalm)","z:^)",
//			"(muscle)","z:)z" ,"z|-)","(rofl)","(lala)","z(ske)","z:$z","z(n)z","z>o)","z;(z",
//			"(bandit)","z(y)z","z(ci)","z:?z","(giggle)","z(a)z","(makeup)","z>2)","(swear)","(wave)",
//			"zz(v)z","z(bow)","z(*)z","(emo)","z:]z","z:@z","(doh)","zz(6)","z:&z","(happy)",
//			"z;)z","|-(","z(t)z","(whew)","(rock)","z:|z","(yawn)",":P","(hande)","(tmi)z",
//			"(fingers)","z:Dz","(smirk)","(punch)","z:(z","z:*z","(wasntme)","8-|z","z::|","z:xz",
//			"(waiting)","(clap)","z(mm)","(headbang)"
//	};
//	
//	
//	public NodeChat(String textt, boolean isMee,String namee)
//	{
//		this.textdai = textt;
//		this.isMe = isMee;
//		this.text  = mFont.tahoma_7_yellowsmall.splitFontArray((isMee==false?namee+": ":"")+textt, wnode);
//		this.type = t_TEXT;
//		int[] typetext = new int[text.length];
//		if(text.length>=1){
//			for (int m = 0; m < text.length; m++) {
//				mVector listEmo = new mVector();
//				typetext[m] = t_TEXT;
//				for (int i = 0; i < maEmo.length; i++) {
//					if(text[m].contains(maEmo[i])){
//						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
//						for (int j = 0; j < nEmo.size(); j++) {
//							EmoText emo = (EmoText)nEmo.elementAt(j);
//							listEmo.addElement(emo);
//						}
//						typetext[m] = t_EMO;
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//					}
//				}
//				if(listEmo.size()>1)
//				listEmo = XepThuTuEmo(listEmo);
//				if(typetext[m] == t_EMO){
//					int lengEmo = 0;
//					for (int i = 0; i < listEmo.size(); i++) {
//						EmoText emo = (EmoText)listEmo.elementAt(i);
//						lengEmo+= maEmo[emo.loai].length();
//					}
//					if(lengEmo<text[m].length()){
//						if(this.type==t_EMO)
//						this.type = t_TEXTEMO;
//						typetext[m] = t_TEXTEMO;
//						
//					}else if(lengEmo<text[m].length()){
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//						typetext[m] = t_EMO;
//					}
//				}
//				Cout.println(getClass(), m+"  typetext[m] "+typetext[m]);
//				if(typetext[m] == t_TEXTEMO){
//					String[] listext = slitFontWithEmo(text[m], listEmo);
//					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
//					listEmoLine.addElement(emoline);
//				} else if(typetext[m] == t_TEXT){
//					String[] textline = new String[1];
//					textline[0] = text[m];
//					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//				else if(typetext[m] == t_EMO){
//					String[] textline = new String[1];
//					textline[0] = "";
//					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//			}
//		}
//		boolean[] isType = new boolean[3];
//		for (int i = 0; i < typetext.length; i++) {
//			if(typetext[i]==t_TEXT){
//				isType[t_TEXT] = true;
//			}
//			if(typetext[i]==t_EMO){
//				isType[t_EMO] = true;
//			}
//			if(typetext[i]==t_TEXTEMO){
//				isType[t_TEXTEMO] = true;
//			}
//		}
//		if(isType[t_TEXTEMO])
//			this.type = t_TEXTEMO;
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
//			this.type = t_TEXT;
//		}
//		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_EMO;
//		}
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_TEXTEMO;
//		}
//
//		Cout.println(getClass(), t_TEXT+" "+isType[t_TEXT]+" "+ t_EMO+" "+isType[t_EMO]+" "+ t_TEXTEMO+" "+isType[t_TEXTEMO]);
//		Cout.println(getClass(), " this.type "+this.type);
//		if(this.text!=null)
//		this.hnode = mScreen.HSTRING*(this.text.length)+mScreen.HSTRING/4;
//		this.name = namee;
//		wname = mFont.tahoma_7_yellow.getWidth(name);
//	}
//	public NodeChat(String textt, boolean isMee,String namee,int wnodePainInfoGameScr)
//	{
//		this.textdai = textt;
//		this.isMe = isMee;
//		this.text  = mFont.tahoma_7_yellowsmall.splitFontArray((isMee==false?namee+": ":"")+textt, wnodePainInfoGameScr);
//		this.type = t_TEXT;
//		int[] typetext = new int[text.length];
//		if(text.length>=1){
//			for (int m = 0; m < text.length; m++) {
//				mVector listEmo = new mVector();
//				typetext[m] = t_TEXT;
//				for (int i = 0; i < maEmo.length; i++) {
//					if(text[m].contains(maEmo[i])){
//						mVector nEmo = findAllEmoInText(text[m], maEmo[i], i);
//						for (int j = 0; j < nEmo.size(); j++) {
//							EmoText emo = (EmoText)nEmo.elementAt(j);
//							listEmo.addElement(emo);
//						}
//						typetext[m] = t_EMO;
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//					}
//				}
//				if(listEmo.size()>1)
//				listEmo = XepThuTuEmo(listEmo);
//				if(typetext[m] == t_EMO){
//				int lengEmo = 0;
//					for (int i = 0; i < listEmo.size(); i++) {
//						EmoText emo = (EmoText)listEmo.elementAt(i);
//						lengEmo+= maEmo[emo.loai].length();
//					}
//					if(lengEmo<text[m].length()){
//						if(this.type==t_EMO)
//						this.type = t_TEXTEMO;
//						typetext[m] = t_TEXTEMO;
//						
//					}else if(lengEmo<text[m].length()){
//						if(this.type==t_TEXT)
//						this.type = t_EMO;
//						typetext[m] = t_EMO;
//					}
//				}
//				if(typetext[m] == t_TEXTEMO){
//					String[] listext = slitFontWithEmo(text[m]+"   ", listEmo);
//					EmoLineText emoline = new EmoLineText(t_TEXTEMO,listext, listEmo);
//					listEmoLine.addElement(emoline);
//				} else if(typetext[m] == t_TEXT){
//					String[] textline = new String[1];
//					textline[0] = text[m];
//					EmoLineText emoline = new EmoLineText(t_TEXT,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//				else if(typetext[m] == t_EMO){
//					String[] textline = new String[1];
//					textline[0] = "";
//					EmoLineText emoline = new EmoLineText(t_EMO,textline, listEmo);
//					listEmoLine.addElement(emoline);
//				} 
//			}
//		}
//		boolean[] isType = new boolean[3];
//		for (int i = 0; i < typetext.length; i++) {
//			if(typetext[i]==t_TEXT){
//				isType[t_TEXT] = true;
//			}
//			if(typetext[i]==t_EMO){
//				isType[t_EMO] = true;
//			}
//			if(typetext[i]==t_TEXTEMO){
//				isType[t_TEXTEMO] = true;
//			}
//		}
//		if(isType[t_TEXTEMO])
//			this.type = t_TEXTEMO;
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&!isType[t_EMO]){
//			this.type = t_TEXT;
//		}
//		else if(!isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_EMO;
//		}
//		else if(isType[t_TEXT]&&!isType[t_TEXTEMO]&&isType[t_EMO]){
//			this.type = t_TEXTEMO;
//		}
//		if(this.text!=null)
//		this.hnode = mScreen.HSTRING*(this.text.length)+mScreen.HSTRING/4;
//		this.name = namee;
//		wname = mFont.tahoma_7_yellow.getWidth(name);
//	}
////	posNV[4] + 4 +spriteBatch.getImageWidth(imgTheBai[1])
//	public void paint(MGraphics spriteBatch,int x,int y){
//		switch (type) {
//		case t_TEXT:
//			for (int j = 0; j < text.length; j++) {
//				mFont.tahoma_7_yellowsmall.drawString(spriteBatch, text[j],x,y+j*mScreen.HSTRING+1, 0,true);
//			}
//			break;
//		case t_TEXTEMO:
//			for (int i = 0; i < listEmoLine.size(); i++) {
//				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
//				switch (emoline.loai) {
//				case t_TEXTEMO:
//					int indexemo=0;
//					for (int j = 0; j < emoline.listtext.length; j++) {
//						if(emoline.listtext[j].equals(mtextConvertEmo))
//						{
////							Cout.println(getClass(), " index Emove "+indexemo);
//							EmoText emo = (EmoText)emoline.listEmo.elementAt(indexemo);
//							try {
//								indexemo++;
//								spriteBatch.drawImage(MainTabScreen.imgEmo[emo.loai],
//										emoline.wlist[j]+x,
//										y+i*mScreen.HSTRING+1, 0,true);
//							} catch (Exception e) {
//								// TODO: handle exception
//							}
//						}
//						else 
//						mFont.tahoma_7_yellowsmall.drawString(spriteBatch,emoline.listtext[j],emoline.wlist[j]
//								/*(j>0?emoline.wlist[j-1]+spriteBatch.getImageWidth(MainTabScreen.imgEmo[0])*(j):0)*/+  x,y+i*mScreen.HSTRING+1, 0,true);
//					}
////					for (int j = 0; j < emoline.wlist.length; j++) {
////						
////					}
////					for (int j = 0; j < emoline.listtext.length; j++) {
////						mFont.tahoma_7_yellowsmall.drawString(spriteBatch,emoline.listtext[j],
////								(j>0?emoline.wlist[j-1]+spriteBatch.getImageWidth(MainTabScreen.imgEmo[0])*(j):0)+  x,y+i*mScreen.HSTRING+1, 0,true);
////					}
////					for (int j = 0; j < emoline.listEmo.size(); j++) {
////						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
////						try {
////							spriteBatch.drawImage(MainTabScreen.imgEmo_small[emo.loai],
////									emoline.wlist[j]+x+2*j,
////									y+i*mScreen.HSTRING+1, 0,true);
////						} catch (Exception e) {
////							// TODO: handle exception
////						}
////					}
//				break;
//				case t_TEXT:
//					mFont.tahoma_7_yellowsmall.drawString(spriteBatch,emoline.listtext[0],x,y+i*mScreen.HSTRING+1, 0,true);
//					break;
//				case t_EMO:
//					for (int j = 0; j < emoline.listEmo.size(); j++) {
//						EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
//						spriteBatch.drawImage(MainTabScreen.imgEmo[emo.loai],x+
//								(j*MGraphics.getImageWidth(MainTabScreen.imgEmo[0])),
//								y+i*mScreen.HSTRING+1, 0,true);
//					}
//					break;
//				default:
//					break;
//				}
//			}
//			break;
//		case t_EMO:
//			for (int i = 0; i < listEmoLine.size(); i++) {
//				EmoLineText emoline = (EmoLineText)listEmoLine.elementAt(i);
//				for (int j = 0; j < emoline.listEmo.size(); j++) {
//					EmoText emo = (EmoText)emoline.listEmo.elementAt(j);
//					spriteBatch.drawImage(MainTabScreen.imgEmo[emo.loai],
//							x+wnode-emoline.listEmo.size()*MGraphics.getImageWidth(MainTabScreen.imgEmo[0])-mScreen.HSTRING/2+(j*MGraphics.getImageWidth(MainTabScreen.imgEmo[0])),
//							y+i*mScreen.HSTRING+1, 0,true);
//				}
//			}
//			break;
//		default:
//			break;
//		}
//		
//	}
//	
//	public String[] slitFontWithEmo(String text,mVector nEmo){
//		mVector listtext = new mVector();
//		for (int i = 0; i < nEmo.size(); i++) {
//			EmoText emo = (EmoText)nEmo.elementAt(i);
//			text = text.replace(maEmo[emo.loai], mtextConvertEmo+" ");
//		}
//		String[] mangcat = text.split(mtextConvertEmo);	
//
////		for (int i = 0; i < nEmo.size(); i++) {
////			EmoText emo = (EmoText)nEmo.elementAt(i);
////			String dem = text.substring(indexcat, emo.vitri);
////			if(dem!=null){
////				listtext.addElement(dem.length()==0?mtextConvertEmo:dem);
////				indexcat = emo.vitri+maEmo[emo.loai].length();
////			}
////			
////			if(indexcat==0&&emo.vitri==0){
////				listtext.addElement("");
////				indexcat = emo.vitri+maEmo[emo.loai].length();
////			}
////			if(i==nEmo.size()-1){
////				int lencat = text.length()-(emo.vitri+maEmo[emo.loai].length());
////				if(lencat>0){
////					String textend = text.substring(emo.vitri+maEmo[emo.loai].length(),emo.vitri+maEmo[emo.loai].length()+ lencat);
////					listtext.addElement(textend);
////				}
////			}
////		}
//		if(mangcat==null) return null;
//		else {
//			int nemo = nEmo.size();
//			for (int i = 0; i < mangcat.length; i++) {
//				if(!(i==0&&mangcat[i].length()==0)){
//					listtext.addElement(mangcat[i]);
//					if(nemo>0){
//						nemo--;
//						listtext.addElement(mtextConvertEmo);
//					}
//				}
//				else {
//					if(nemo>0){
//						nemo--;
//						listtext.addElement(mtextConvertEmo);
//					}
//				}
//			}
//		}
//		String[] chuoitextt = new String[listtext.size()];
//		for (int i = 0; i < listtext.size(); i++) {
//			chuoitextt[i] = (String)listtext.elementAt(i);
//		}
//		return chuoitextt;
//	}
//	
//	public mVector findAllEmoInText(String text,String maemo,int loaiemo){
//		int index = 0;
//		mVector nemo = new mVector();
//		for (int i = 0; i < text.length(); i++) {
//			if(text.charAt(i)==maemo.charAt(0)&&i+maemo.length()<=text.length()){
//				boolean isMa = true;
//				try {
//					for (int j = 0; j < maemo.length(); j++) {
//						if(maemo.charAt(j)!=text.charAt(i+j)){
//							isMa = false;
//						}
//					}
//				} catch (Exception e) {
//					// TODO: handle exception
//					Cout.println(getClass(), e.getMessage());
//				}
//				
//				if(isMa){
//					index = maemo.length();
//					nemo.addElement(new EmoText(loaiemo,i));
//					i +=index-1;
//				}
//			}
//		}
//		return nemo;
//	}
//	public mVector XepThuTuEmo(mVector listEmo){
//		int vitri=0,loai=0;
//		for (int i = 0; i < listEmo.size()-1; i++) {
//			for (int j = i+1; j < listEmo.size(); j++) {
//				EmoText emoI =(EmoText)listEmo.elementAt(i);
//				EmoText emoJ =(EmoText)listEmo.elementAt(j);
//				if(emoI.vitri>emoJ.vitri){
//					vitri = emoI.vitri;
//					loai = emoI.loai;
//					//
//					emoI.loai = emoJ.loai;
//					emoI.vitri = emoJ.vitri;
//					
//					emoJ.loai = loai;
//					emoJ.vitri = vitri;
//				}
//				
//			}
//		}
//		return listEmo;
//	}
//}
