package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.Friend;
import Objectgame.OtherChar;
import lib.Cout;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Paint;
import model.Screen;
import model.Scroll;
import model.ScrollResult;
import model.mResources;
import real.Service;
import real.mFont;
import screen.old.GameScr;

public class GuiFriend  extends Screen implements IActionListener{

	public static Scroll scrMain = new Scroll();
	public int indexRow;
	public int popupX,popupY,popupW,popupH =220;
	//friend list gui
		public static Command btnUnfriend;//display quest info
		public static Command btnChat;//display quest info
	public GuiFriend(){
		popupX = GameCanvas.w/2-GameScr.widthGui/2;
		popupY = GameScr.popupY;
		popupW = GameScr.widthGui;
		btnUnfriend = new Command("Xóa bạn",this, Contans.UN_FRIEND, null, 0, 0);
		btnChat = new Command("Chat riêng", this, Contans.CHAT_PRIVATE, null, 0, 0);
		btnUnfriend.setPos(popupX + popupW-Image.getWidth(loadImageInterface.btnTab)/2,
				popupY+40, loadImageInterface.btnTab,loadImageInterface.btnTabFocus);
		btnChat.setPos(popupX + popupW-Image.getWidth(loadImageInterface.btnTab)/2,
				popupY + 3*loadImageInterface.btnTab.height/2, loadImageInterface.btnTab,loadImageInterface.btnTabFocus);
	
	}
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		scrMain.updatecm();
		ScrollResult s1 =  scrMain.updateKey();
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnChat)) {
			if (btnChat != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnChat != null)
					btnChat.performAction();
			}
		}
		//unfriend
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnUnfriend)) {
			if (btnUnfriend != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnUnfriend != null)
					btnUnfriend.performAction();
			}
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(scrMain.selectedItem == -1)
			return;
		indexRow = scrMain.selectedItem;
		if (indexRow<Char.myChar().vFriend.size()&&Char.myChar().vFriend.size() > 0) {
			Char c = (Char) Char.myChar().vFriend.elementAt(indexRow);
			Char.toCharChat=c;
//			yMenu+(5+loadImageInterface.btnTab.getHeight())*i
		}
	}

	@Override
	public void paint(MGraphics g) {
		// TODO Auto-generated method stub
		paintFriend(g);
	}

	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(popupX+popupW-Image.getWidth(loadImageInterface.closeTab), popupY, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}
	
	
	
	public void paintFriend(MGraphics g) {

			String str =  mResources.FRIENDS[0] ;
			
			GameCanvas.paint.paintFrameNaruto(popupX, popupY,popupW,popupH, g);
			if (Char.myChar().vFriend.size() > 0) {
				GameScr.xstart =popupX + 5;
				GameScr.ystart =popupY + 40;
				
				resetTranslate(g);
				scrMain.setStyle(Char.myChar().vFriend.size(), 50, GameScr.xstart, GameScr.ystart,  popupW- 3, 180, true, 1);
				scrMain.setClip(g, GameScr.xstart, GameScr.ystart-10, popupW - 3, 180);
				int friendCount = 0;
				int yBGFriend=0;
				for (int i = 0; i < Char.myChar().vFriend.size(); i++) {
					Char c = (Char) Char.myChar().vFriend.elementAt(i);
//					
					
//					Paint.PaintBGListQuest(x+40,y+70,width-50,graphic);//new quest
//					if(indexselectNV==0&&Quest.listUnReceiveQuest.size()>0&&(GameCanvas.gameTick/10)%2==0)
//						Paint.PaintBGListQuestFocus(x+40,y+70,width-50,graphic);//new focus quest
					   
						Paint.PaintBGListQuest(GameScr.xstart +35,GameScr.ystart+yBGFriend,160,g);//new quest	
						if (indexRow == i) {
//							graphic.setColor(Paint.COLORLIGHT);
//							graphic.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							graphic.setColor(0xffffff);
//							graphic.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							btnChat.paint(graphic);
//							btnUnfriend.paint(graphic);
						
							Paint.PaintBGListQuestFocus(GameScr.xstart +35,GameScr.ystart+yBGFriend,160,g);
						}
//					else {
//							graphic.setColor(Paint.COLORBACKGROUND);
//							graphic.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//							graphic.setColor(0xd49960);
//							graphic.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//						}

						
						mFont.tahoma_7_white.drawString(g, "Level: "+c.clevel, GameScr.xstart + 73, GameScr.ystart+yBGFriend+24 , 0,true);
						
						g.drawImage(loadImageInterface.charPic,GameScr.xstart +45,GameScr.ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
						if(c.isOnline)
						{
							g.drawImage(loadImageInterface.imgName,GameScr.xstart+100,GameScr.ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
							mFont.tahoma_7_white.drawString(g, c.cName, GameScr.xstart + 73, GameScr.ystart+yBGFriend+8 , 0,true);
							g.drawImage(loadImageInterface.imgOnline[1],GameScr.xstart +180,GameScr.ystart+yBGFriend+20,g.VCENTER|g.HCENTER,true);
							
						}
							
						else 
						{
							g.drawImage(loadImageInterface.imgName,GameScr.xstart+100,GameScr.ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
							mFont.tahoma_7_white.drawString(g, c.cName, GameScr.xstart + 73, GameScr.ystart +yBGFriend+8, 0,true);	
							g.drawImage(loadImageInterface.imgOnline[0],GameScr.xstart +180,GameScr.ystart+yBGFriend+20,g.VCENTER|g.HCENTER,true);
						}
						
				
						friendCount++;
						yBGFriend+=50;
				}
				resetTranslate(g);
				if(btnChat!=null)
				{
					btnChat.paint(g);
					btnUnfriend.paint(g);
				}
			} else {
				mFont.tahoma_7_white.drawString(g, mResources.NO_FRIEND, popupX + popupW / 2,popupY + 40,
						mFont.CENTER);
			}
			GameScr.resetTranslate(g);
			//paint name box 
			Paint.PaintBoxName("DANH SÁCH BẠN BÈ",popupX+55,popupY,130,g);
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Contans.UN_FRIEND:
			if(indexRow>=0&&indexRow<Char.myChar().vFriend.size()){
				Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
				Service.gI().deleteFriend((byte)Friend.UNFRIEND,(short)Char.myChar().charID ,(short)charF.CharidDB);

				indexRow = -1;
			}
			break;
		case Contans.CHAT_PRIVATE: // mo giao dien chat rieng
//			ChatTab currentTab = ChatManager.getInstance().getCurrentChatTab();
//			currentTab.type = 2;
//			indexRow = -1;
//			isPaintFriend = false;
//			left = null;
//			center = null;
//			openUIChatTab();
			GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
			if(indexRow>=0&&indexRow<Char.myChar().vFriend.size()){
				Char charFf = (Char) Char.myChar().vFriend.elementAt(indexRow);
				if(charFf!=null){
					Cout.println(getClass(), "CHAT_PRIVATE charFf  "+charFf.cName);
					boolean isAdded = false;
					for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
						OtherChar other =(OtherChar)ChatPrivate.vOtherchar.elementAt(i);
						if(other.name.equals(charFf.cName)||other.id==charFf.charID){
							isAdded = true;
							break;
						}
						
					}
					if(!isAdded)
						ChatPrivate.AddNewChater((short)charFf.charID, charFf.cName);
				}
			}
			indexRow = -1;
			left = null;
			center = null;
			TabChat.gI().switchToMe();
//			isLockKey = true;
			GameScr.setPopupSize(175, 200);
			left = center = null;
			
			break;
		}
		
	}
}
