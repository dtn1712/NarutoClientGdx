package Gui;

import real.mFont;
import screen.old.GameScr;

import GuiOut.loadImageInterface;
import Objectgame.ChatPrivate;
import Objectgame.NodeChat;
import Objectgame.OtherChar;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.Cout;
import lib.TField;
import model.Command;
import model.IActionListener;
import model.Paint;
import model.Screen;
import model.Scroll;
import model.ScrollResult;

public class TabChat extends Screen implements IActionListener{

	public static TabChat instance;
	public Scroll scrListActor;
	//icon chat
	public static Iconchat iconChat;
	public static Command cmdchat,cmdClose;
	public static Command cmdIconChat;
	public static TField tfCharFriend;
	public int heightGui = 220,popupY,popw=100,popupW=220,popx;
	public int indexRow;
	public Boolean isPaintListIcon = false;	
	public  int indexSize = 28;
	
	public static TabChat gI()
	{
		if(instance==null)
			instance = new TabChat();
		return instance;
	}
	public TabChat()
	{
		cmdchat = new Command("Gửi",Contans.BUTTON_SEND);
		cmdIconChat = new Command("",Contans.BUTTON_ICON_CHAT);
	    tfCharFriend = new TField();
	    scrListActor = new Scroll();
	}
	
	private void initTfied(){

		  tfCharFriend.width = popupW - 64;
		  tfCharFriend.height = ITEM_HEIGHT + 2;
		  popx = GameCanvas.w/2-(popw+popupW)/2-5;
		  tfCharFriend.x = popx+popw + 8;
		  popupY = GameCanvas.h/2 - heightGui/2;
		  tfCharFriend.y = popupY + heightGui- 30;
		  Cout.println(getClass(), (GameCanvas.h/2)+" tfCharFriend.y   "+ tfCharFriend.y );
		  tfCharFriend.isFocus =false;
		  tfCharFriend.setIputType(TField.INPUT_TYPE_ANY);
		  cmdIconChat.setPos( popx+GameScr.gI().popw+5+tfCharFriend.width+5, tfCharFriend.y, loadImageInterface.imgEmo[7],loadImageInterface.imgEmo[7]);
		  cmdIconChat.w = 15;
		  cmdIconChat.h = 20;
		  cmdchat.setPos(cmdIconChat.x+cmdIconChat.w+2, tfCharFriend.y-5,loadImageInterface.img_use ,loadImageInterface.img_use_focus);
		  cmdchat.w = 30;
		  cmdchat.h = 20;

		  tfCharFriend.y = popupY + heightGui- 32;
//		  tfCharFriend.name = "Chat riêng";
//		  tfCharFriend.m = GameMidlet.instance;
//		  tfCharFriend.c = MotherCanvas.instance;
//		  tfCharFriend.color = 0xffffff;
	}
	
	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		initTfied();
		cmdClose = new Command("", this, 20, null);
//		GameScr.xGui+5, GameScr.yGui+5,popupW-20
		cmdClose.setPos(popx+(popw+popupW)-loadImageInterface.closeTab.height/2, popupY, loadImageInterface.closeTab, loadImageInterface.closeTab);
		super.switchToMe();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(isPaintListIcon){
			iconChat.Updatecm();
			iconChat.updateKeySelectIconChat();
			if(iconChat.indexSelect>=0)
			{
				tfCharFriend.setText(tfCharFriend.getText()+NodeChat.maEmo[iconChat.indexSelect]) ; 
				iconChat.indexSelect=-1;
				isPaintListIcon = false;
			}
		}else{
			scrListActor.updatecm();
			ScrollResult s1 = scrListActor.updateKey();
			indexRow = scrListActor.selectedItem;
			if(indexRow>=0&&indexRow<ChatPrivate.vOtherchar.size()&&
					((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow))!=null)
			{
				((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).update();
			}
		}
		GameScr.gI().update();
		super.update();
	}

	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		updatePointer();
		scrListActor.updateKey();
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
					cmdClose.performAction();
			}
		}
		super.updateKey();
	}

	@Override
	public void paint(MGraphics g) {
		// TODO Auto-generated method stub
		GameScr.gI().paint(g);
		resetTranslate(g);
		paintListFiendChat(g);
		resetTranslate(g);
		tfCharFriend.paint(g);
		Paint.PaintBoxName("Chat Riêng",popx+popw+popupW/2-55,popupY,popupW-110,g);
		//paint content of chat friend
		if(indexRow>=0&&((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow))!=null)
		{
			((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).Paint(g,GameScr.xGui+5, GameScr.yGui+5,popupW-20,tfCharFriend.y-GameScr.yGui-7);
		}
		resetTranslate(g);
		cmdchat.paintW(g);
		cmdIconChat.paint(g);
		if(isPaintListIcon&&iconChat!=null)
		{
			 iconChat.paint(g);
		}

		cmdClose.paint(g,true);
		super.paint(g);
	}

	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
		if(isPaintListIcon){
			if(GameCanvas.isPointerClick&&!GameCanvas.isPoint(iconChat.scrMain.xPos,
					iconChat.scrMain.yPos, iconChat.scrMain.width, iconChat.scrMain.height)){
				isPaintListIcon = false;
				GameCanvas.isPointerClick = false;
			}
		}else{
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdchat)) {
				if (cmdchat!= null) {
					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdchat != null)
						cmdchat.performAction();
				}
			}
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdIconChat)) {
				if (cmdIconChat!= null) {
					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdIconChat != null)
						cmdIconChat.performAction();
				}
			}
			tfCharFriend.update();
		}
		super.updatePointer();
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 20:
			GameScr.gI().switchToMe();
			break;

		default:
			break;
		}
		
	}
	public void paintListFiendChat(MGraphics g) {
//			GameCanvas.paint.paintFrame(popx + 70, popy +30, popw, poph + 20, graphic);//paint list chater
			GameCanvas.paint.paintFrameNaruto(popx,popupY, popw, heightGui, g);//paint list chater
			GameCanvas.paint.paintFrameNaruto(popx+popw, popupY,popupW,heightGui, g);//paint frame chat
			if (ChatPrivate.vOtherchar.size() > 0) {
//				xstart = popupX + 5;
//				ystart = popupY + 40;
				GameScr.xstart = popx+5;
				GameScr.ystart = popupY+5;
//				graphic.setColor(0x001919);
//				graphic.fillRect(xstart - 2, ystart - 2, popw - 6, indexSize * 5 + 8);
	
				resetTranslate(g);
				
				scrListActor.setStyle(ChatPrivate.vOtherchar.size(), indexSize, popx,popupY, popw , heightGui, true, 1);
				scrListActor.setClip(g, popx, popupY, popw, heightGui);
	
//				indexRowMax = ChatPrivate.vOtherchar.size();
				int friendCount = 0;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++)
				{
					OtherChar c = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
//	
					if (indexRow == i) 
					{
						g.setColor(Paint.COLORLIGHT);
						g.fillRect(GameScr.xstart + 4, GameScr.ystart + (indexRow * GameScr.indexSize) + 4, popw - 19, indexSize - 8);
						g.setColor(0xffffff);
						g.drawRect(GameScr.xstart + 4, GameScr.ystart + (indexRow * GameScr.indexSize) + 4, popw - 19, indexSize - 8);
								
					} 
//					else
//					{
//						graphic.setColor(Paint.COLORBACKGROUND);
//						graphic.fillRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//						graphic.setColor(0xd49960);
//						graphic.drawRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//					}
							
					mFont.tahoma_7_red.drawString(g, c.name, GameScr.xstart + 8, GameScr.ystart + i * indexSize + indexSize / 2 - 6, 0);
		
					friendCount++;
				}
				
//				paintNumCount(graphic);
			} 
	}
	private void paintNumCount(MGraphics g) {
//		resetTranslate(graphic);
//		int curRow = indexRow;
//		if (isPaintAuctionBuy)
//			curRow = indexSelect;
//		if (curRow >= 0 && indexRowMax > 0) {
//			int row = ((curRow + 1) < indexRowMax) ? (curRow + 1) : indexRowMax;
//			
//			mFont.tahoma_7b_white.drawString(graphic, row + "/" + indexRowMax, popx + popw / 2, popy + poph - 20, 2);
//		}
	}
}
