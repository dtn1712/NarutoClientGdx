package Gui;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import real.Service;
import real.mFont;
import GuiOut.loadImageInterface;
import Objectgame.Item;
import Objectgame.ItemTemplates;
import lib.Bitmap;
import lib.Cout;
import lib.TField;
import lib.mVector;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Paint;
import model.Screen;
import model.Scroll;
import model.ScrollResult;

/*
 * Paint and execute shop gui
 */

public class ShopMain extends Screen implements IActionListener{

	public static Scroll scrMain = new Scroll();
	
	public mVector VecTabScreen = new mVector();
	int x,y,width=200,height=232,widthSub=170,heightSub=230,wKhoangCach=20;
	int indexSize,xBegin,yBegin;
	int wScollReceived=100,hScollReceived=60;
	int indexRow=-1,indexRowUnRecive,witem = 34;
	public static int idMenu[],indexidmenu;
	public static String nameMenu[];
	public static String dis[];
	public static Command cmdShop[];
	public static Command cmdMua;
	public static Item itemshop[][];
	public TField tfsl;
	public static int idItemtemplate[];
	public Bitmap imgCoins1,imgCoins2;
	int xMenu,yMenu;
	int wMenu,hMenu;
	private Bitmap[] arrayBGShop=new Bitmap[25];
	public int selectTab = 0;
	
	int xM;//diem nam giua frame
	public ShopMain(int x, int y)
	{
		int wAll = width+widthSub+wKhoangCach; 
		this.x=(GameCanvas.w/2-wAll/2);
		this.y=(GameCanvas.h-y-heightSub<0?GameCanvas.h-heightSub:y);
		y= this.y;
		this.xM=this.x+width/2;
		x = this.x;
		xMenu=this.x+width-25;
		yMenu=y+40;
	
		wMenu=Image.getWidth(loadImageInterface.btnTab);
		hMenu=Image.getHeight(loadImageInterface.btnTab);
		
		indexSize=mFont.tahoma_7_yellow.getHeight()+2;
		
		ShopWeapon sWeapon= new ShopWeapon();
		VecTabScreen.addElement(sWeapon);
		
		ShopWeapon sWeapon1= new ShopWeapon();
		VecTabScreen.addElement(sWeapon1);
		
		ShopWeapon sWeapon2= new ShopWeapon();
		VecTabScreen.addElement(sWeapon2);
//		getItemList(0,idItemtemplate);
		LoadImage();
		//name[i], this,i+2, null, 0, 0
		creatMenuShop(nameMenu, cmdShop);
		cmdMua = new Command("Mua ngay",this, 1, null, 0, 0);
//		x+width+wKhoangCach,y,widthSub,heightSub
		cmdMua.setPos( x+width+wKhoangCach+widthSub/2-loadImageInterface.img_use.getWidth()/2+7,y+heightSub-2*loadImageInterface.img_use.getHeight()-2, loadImageInterface.img_use ,loadImageInterface.img_use_focus);
		xBegin = x+width+wKhoangCach/2+4;
		yBegin = y+heightSub-5;
		tfsl = new TField();
		tfsl.width = loadImageInterface.img_use.getWidth();
		tfsl.x = cmdMua.x;
		tfsl.y = cmdMua.y-25;
	}
	
	public void LoadImage()
	{
		for(int i=0;i<25;i++)
		{
			arrayBGShop[i]=GameCanvas.loadImage("/GuiNaruto/shop/shell_"+(i+1)+".png");
		}
	}
	public void update(){
		if(imgCoins1==null){
			imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
			imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
		}
	}
	public void updateKey() {
		// TODO Auto-generated method stub
		scrMain.updatecm();
		ScrollResult s1 =  scrMain.updateKey();
		if(Screen.getCmdPointerLast(cmdMua))
			cmdMua.performAction();
		if(cmdShop!=null)
		for (int i = 0; i < cmdShop.length; i++) {
			if(Screen.getCmdPointerLast(cmdShop[i]))
			{
				Cout.println(getClass(), " iiiiii "+i);
				cmdShop[i].performAction();
				GameCanvas.clearPointerEvent();
			}
		}
		tfsl.update();
	}
	
	public void updatePointer() {

		int ySelect=yMenu;
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), "   updatePointer    1  ");
			selectTab=0;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			selectTab=1;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			selectTab=2;
			GameCanvas.isPointerClick= false;
		}
		
	}
	
	//paint bg human
	private void PaintBGShell(int x,int y,MGraphics g)
	{
		int wImage=Image.getWidth(arrayBGShop[0]);
		int index=0;
	
		for(int j=1;j<5;j++)
		{
			g.drawImage(arrayBGShop[index==3?4:index],x+j*wImage,y,0,true);
			index++;
		}
	}
	
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(x+width-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}
	
	public void paint(MGraphics g)
	{
//		GameScr.resetTranslate(graphic);
		//tab
//		for(int i=0;i<VecTabScreen.size();i++)
//		{
//			graphic.drawImage(loadImageInterface.btnTab, xMenu+9,yMenu+(5+Image.getHeight(loadImageInterface.btnTab))*i, 0);
//		}
		
//		for(int i=0;i<VecTabScreen.size();i++)
//		{
//			mFont.tahoma_7b_white.drawString(graphic,nameMenu[i],xMenu+40,yMenu+6+(5+Image.getHeight(loadImageInterface.btnTab))*i,graphic.HCENTER|graphic.VCENTER);
//		}
		
		
		
		Shop questMain= (Shop) VecTabScreen.elementAt(selectTab);
		
		GameCanvas.paint.paintFrameNaruto(this.x,this.y, this.width,this.height,g);
		
		Paint.PaintBoxName(nameMenu[indexidmenu],x+55,20,width-110,g);
	
		Paint.SubFrame(x+width+wKhoangCach,y,widthSub,heightSub, g);//sub
		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length){
		mFont.tahoma_7_white.drawString(g, "Số lượng: ", x+width+wKhoangCach+10,cmdMua.y-24, 0);
			if(cmdMua!=null)
				cmdMua.paint(g);
			//money
			g.drawImage(imgCoins1, x+width+wKhoangCach,yBegin-Image.getHeight(imgCoins1)-1,g.TOP|g.LEFT);
			if(itemshop!=null&&itemshop[indexidmenu]!=null&&scrMain.selectedItem!=-1
					&&itemshop[indexidmenu][scrMain.selectedItem]!=null)
			mFont.tahoma_7b_white.drawString(g,itemshop[indexidmenu][scrMain.selectedItem].template.gia+"",
					x+width+wKhoangCach+33,yBegin-Image.getHeight(imgCoins1)/2-2, 0);
			
			//gold
			g.drawImage(imgCoins2, x+width+wKhoangCach+(Image.getWidth(imgCoins2)+6),yBegin-Image.getHeight(imgCoins2)-1,g.TOP|g.LEFT);
			mFont.tahoma_7b_white.drawString(g, "0",
					x+width+wKhoangCach+(Image.getWidth(imgCoins2)+6)+35,yBegin-Image.getHeight(imgCoins1)/2-2,0);
		}
//		paintNameItem(graphic, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 20, 170, name, colorName);
//		paintNameItem(graphic, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 30, longwidth, "Tấn công: 1000", colorName);
//		paintNameItem(graphic, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 40, longwidth, "Độ bền: 10", colorName);
//		paintNameItem(graphic, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 50, longwidth, "tắng sát thương vật lý 10%", colorName);
		//content
//		GameScr.resetTranslate(graphic);
		scrMain.setStyleWH(itemshop[indexidmenu].length/4+(itemshop[indexidmenu].length%4!=0?1:0),witem, 50, 
				x+40, y+50, width-90, height-70, true, 4);
		scrMain.setClip(g,x, y+50, width, height-70);
		//new quest
		for (int i = 0; i < (itemshop[indexidmenu].length/4+(itemshop[indexidmenu].length%4!=0?1:0)); i++) {
			PaintBGShell(x-20,y+50*(i+1)+20,g);
		}
//		PaintBGShell(x-23,y+70,graphic);
//		PaintBGShell(x-23,y+120,graphic);
//		PaintBGShell(x-23,y+170,graphic);
		for (int i = 0; i < itemshop[indexidmenu].length; i++) {
			
			
//			TField.paintInputTf(graphic,false,
//					x+30+(i%4)*witem, y+(i/4+1)*50+28,
//					witem, 30, 
//					x+40+(i%4)*witem+14,y+(i/4+1)*50+16, "", "", loadImageInterface.imgTf0, witem);
//			if(itemshop[indexidmenu][i].template.id != -1 && itemshop[indexidmenu][i].template.name != null){
//				mFont.tahoma_7_white.drawString(graphic, itemshop[indexidmenu][i].template.name,x+30+(i%4)*witem+17, y+(i/4+1)*50+witem-4,2);
//			}
			g.drawImage(loadImageInterface.ImgItem,x+30+(i%4)*witem+17, y+(i/4+1)*50+14, MGraphics.VCENTER | MGraphics.HCENTER);
			
			if(itemshop[indexidmenu][i] != null&&itemshop[indexidmenu][i]!=null){
				itemshop[indexidmenu][i].paintItem(g,x+30+(i%4)*witem+17, y+(i/4+1)*50+14); 
			}
			
			if(i==scrMain.selectedItem){
				g.drawImage(loadImageInterface.imgFocusSelectItem, x+30+(i%4)*witem+17, y+(i/4+1)*50+14, MGraphics.VCENTER| MGraphics.HCENTER);
			}
		}
		
//		paintItem(graphic);
		
		
//		if (idSelect > -1 && Focus == INFO) {//focus
//			setPaintInfo();
//			
//			graphic.drawImage(loadImageInterface.imgFocusSelectItem, (xBegin + ((idSelect % numW)*(Image.getWidth(loadImageInterface.ImgItem)+2)))+2 , yBegin
//					+ (idSelect / numW) * (Image.getHeight(loadImageInterface.ImgItem) + 2) +4 , 0);
//			Paint.SubFrame(xTab-widthSubFrame,yTab-30+ GameCanvas.h / 5,widthSubFrame,heightSubFrame, graphic);
//			paint.paintItemInfo(graphic, itemFocus, xTab-widthSubFrame + 10,yTab-20+ GameCanvas.h / 5);
//		
//
//		}
		GameCanvas.resetTrans(g);
		for(int i = 0; i < cmdShop.length; i++){
			if(cmdShop[i]==null) continue;
			if(i!=indexidmenu)
				cmdShop[i].paint(g);
			else cmdShop[i].paintFocus(g,true);
		}
		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length){
			Paint.paintItemInfo(g, itemshop[indexidmenu][scrMain.selectedItem], x+width+wKhoangCach+10,y+10);
		}
		//tab focus
//		graphic.drawImage(loadImageInterface.btnTabFocus,xMenu+9,yMenu+(5+Image.getHeight(loadImageInterface.btnTabFocus))*selectTab, 0);
//		mFont.tahoma_7b_white.drawString(graphic,nameMenu[selectTab],xMenu+40,yMenu+6+(5+Image.getHeight(loadImageInterface.btnTab))*selectTab,graphic.HCENTER|graphic.VCENTER);
		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length)
		tfsl.paint(g);
	}
	
	public void creatMenuShop(String name[], Command menubtn[]){
		if(name!=null)
		for(int i = 0; i < name.length; i++){
			menubtn[i] = new Command(name[i], this,i+2, null, 0, 0);
			menubtn[i].setPos( xMenu-8,yMenu+(5+Image.getHeight(loadImageInterface.btnTab))*i, loadImageInterface.btnTab, loadImageInterface.btnTabFocus);
		}
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		Cout.println("idAction  "+idAction);
		if(idAction!=1)
		{
			if(idAction-2<=nameMenu.length&&itemshop[idAction-2]==null)
				Service.gI().requestShop(idAction-2);
			else if(itemshop[idAction-2]!=null){
				indexidmenu = idAction-2;
			}
		}
		switch(idAction){
			case 1: // mua item
				if(scrMain.selectedItem!=-1&&scrMain.selectedItem<itemshop[indexidmenu].length)
				{
					String textsl = tfsl.getText();
					
					Service.gI().BuyItemShop(((byte)indexidmenu),(short)itemshop[indexidmenu][scrMain.selectedItem].itemId);
				}
//				Service.getInstance().requestShop(idAction);
				break;
		}

		scrMain.selectedItem = -1;
	}
	
	public static void getItemList(int idmenu,int itemtemplate[]){
		if(itemtemplate==null) return;
		if(itemshop==null)
			itemshop = new Item[nameMenu.length][];
		itemshop[idmenu] = new Item[itemtemplate.length];
		for(int i = 0; i < itemtemplate.length; i++){
			itemshop[idmenu][i] = new Item(); 
			itemshop[idmenu][i].itemId = (short)itemtemplate[i];
			itemshop[idmenu][i].template = ItemTemplates.get((short)itemtemplate[i]);
		}
	}
	
	public void paintItem(MGraphics g){
		int wImage=24;
		int index=0;
		
		for(int i = 0; i < itemshop[indexidmenu].length; i++){
			if(itemshop[indexidmenu][i] != null){
				for(int j = 1; j < 5; j++){
					itemshop[indexidmenu][i].paintItem(g, x+j*wImage + (j * 20),y + 65);
				}
			}
		}
//		for(int j=1;j<4;j++)
//		{
//			graphic.drawImage(arrayBGShop[index],x+j*wImage - 24,y,0);
//			index++;
//		}
	}
}
