package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import real.Service;
import real.mFont;
import screen.old.GameScr;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import lib.Cout;
import model.Command;
import model.Image;
import model.ScrollResult;

/*
 * Giao dien giao tiep giua cac nhan vat voi nhau
 */
public class GuiContact extends FatherChat{
	
	int x,y;
	int xM;//toan do giua cua ma hinh
	String [] listName={"Trò chuyện","Kết bạn","Thông tin","Mời nhóm","Giao dịch","Thách đấu"};
	
	public final int cTroChuyen = 2;
	public final int cKetban = 3;
	public final int cthongtin = 4;
	public final int cMoinhom = 5;
	public final int cGiaodich = 6;
	public final int cThachdau = 7;
	
	public GuiContact(int x,int y) {
		
		popw=156;
		poph=203;
		this.xM=x;
		this.x=x-popw/2;
		this.y=y;
		scrMain.selectedItem = -1;
	}
	public void perform(int idAction, Object p) {
		
		
	}
	
	void PaintList(MGraphics g, String[] list, int x, int y, int width)
	{
		int nCol=width/Image.getWidth(loadImageInterface.imgLineTrade);
//		System.out.println("");
		int yy=0;
		for(int i=0;i<list.length;i++)
		{
			yy+=30;
			mFont.tahoma_7b_red.drawString(g, list[i], x+width/2, y+yy-17, 2);
			for(int j=0;j<nCol;j++)
			{
				g.drawImage(loadImageInterface.imgLineTrade,x+(Image.getWidth(loadImageInterface.imgLineTrade)-1)*j+13,y+yy, 0,true);
			}
		}
	}
	public void Update() {
		// TODO Auto-generated method stub
		if(scrMain!=null)
		{
			scrMain.updatecm();
		}
		if(scrMain.selectedItem>-1){
			switch (scrMain.selectedItem+2) {
			case cTroChuyen:
				Cout.println(getClass(), " cTroChuyen ");
				scrMain.selectedItem = -1;
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
				break;
			case cKetban:
				Cout.println(getClass(), " cKetban ");
				scrMain.selectedItem = -1;
				if(Char.myChar().charFocus!=null)
				Service.gI().requestAddfriend((byte)0, (short)Char.myChar().charFocus.charID);
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
				break;
			case cthongtin:
				Cout.println(getClass(), " cthongtin ");
				scrMain.selectedItem = -1;
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
				break;
			case cMoinhom:
				Cout.println(getClass(), " cMoinhom ");
				scrMain.selectedItem = -1;
				if(Char.myChar().charFocus!=null)
				Service.gI().inviteParty((short)Char.myChar().charFocus.charID, (byte)0);
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
				break;
			case cGiaodich:
				Cout.println(getClass(), " cGiaodich ");
				scrMain.selectedItem = -1;
				Char.myChar().partnerTrade = null;
				if(Char.myChar().charFocus!=null)
				Service.gI().inviteTrade((byte)0, (short)Char.myChar().charFocus.charID);
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
				break;
			case cThachdau:
				Cout.println(getClass(), " cThachdau ");
				scrMain.selectedItem = -1;
				GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
//				Service.getInstance().
				break;
			}
		}
	}
	public void UpdateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.isTouch) {
			   ScrollResult r = scrMain.updateKey();
			   if (r.isDowning || r.isFinish) {
				   
			    indexRow = r.selected;
			   }
		}
	}
	public void Paint(MGraphics g)
	{
		GameScr.resetTranslate(g);
		model.Paint.paintFrameNaruto(x, y, popw, poph, g);
		
		g.drawImage(loadImageInterface.charPic,xM,y+poph/3-22,g.VCENTER|g.HCENTER);
		g.drawImage(loadImageInterface.smallTest,xM-3,y+poph/3-24,g.VCENTER|g.HCENTER);
		
		g.drawImage(loadImageInterface.imgName,xM,y+poph/3+8,g.VCENTER|g.HCENTER);

		if(Char.myChar().charFocus!=null&&Char.myChar().charFocus.cName!=null)
		mFont.tahoma_7b_white.drawString(g,Char.myChar().charFocus.cName, xM,y+poph/3+2,g.VCENTER);
		
		
		model.Paint.PaintBoxName("Giao Tiếp", xM-40, y, 80, g);
		if(scrMain!=null)
		{
			scrMain.setStyle(listName.length,30,x, y+80,popw ,100, true,0);
			scrMain.setClip(g);
		}
		
		if(indexRow>=0)
		{
			g.setColor(0x964210);
			g.fillRect(x+10,y+87+30*indexRow,popw-20,20);
		}
		PaintList(g,listName,x,y+80,popw);
		GameCanvas.resetTrans(g);
	}
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
		cmdClose.setPos(x+popw-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
		
	}

}
