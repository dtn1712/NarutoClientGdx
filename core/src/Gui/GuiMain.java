package Gui;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import real.mFont;
import screen.old.GameScr;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.TileMap;
import lib.Cout;
import lib.mSystem;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Info;
import model.Screen;
import model.Sprite;

public class GuiMain extends Screen implements IActionListener{
	public static int cmdBarX, cmdBarY, cmdBarW, cmdBarLeftW, cmdBarRightW, cmdBarCenterW, hpBarX, hpBarY, hpBarW, mpBarW, expBarW, lvPosX,
	moneyPosX, hpBarH, girlHPBarY;
	public static int xL, yL; // left
	public static int xC, yC;
	public static int xR, yR; // right
	public static int xF, yF; // fire
	public static int xU, yU; // up
	public static int xCenter, yCenter;
	Command btnchangeSkill;
	Command bntCharBoard;
	public Command bntAttack;
	Command bntAttack_1;
	Command bntAttack_2;
	Command bntAttack_3;
	Command bntAttack_4;
	
	int xMove,limMove=45;
	public boolean moveClose,moveOpen;
	int v;//gia toc tang toc do move
	int y=GameCanvas.h-43;
	int timeoff=10;//30s
	long timestart;
	
	//menu icon
	public MenuIcon menuIcon= new MenuIcon(GameCanvas.hw-215,GameCanvas.h-50);
	
	public GuiMain()
	{
		int xAttack=GameCanvas.w-50/*loadImageInterface.imgAttack_1.getWidth()*/-10;
		int yAttack=GameCanvas.h - 50/*loadImageInterface.imgAttack_1.getHeight()*/-10;
		
		
		bntCharBoard= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntCharBoard.setPos(1,1, loadImageInterface.imgCharacter_info, loadImageInterface.imgCharacter_info);
		
		btnchangeSkill= new Command("ChangeKill", this, 20, null, 0, 0);
		btnchangeSkill.setPos(GameCanvas.w-60,1, loadImageInterface.img_use,loadImageInterface.img_use_focus);
		
		bntAttack= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntAttack.setPos(xAttack,yAttack, loadImageInterface.imgAttack_1, loadImageInterface.imgAttack_1);
		
		xAttack=xAttack-Image.getWidth(loadImageInterface.imgAttack);
		yAttack=yAttack+Image.getHeight(loadImageInterface.imgAttack_1)/2;
		bntAttack_1= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntAttack_1.setPos(xAttack,yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);
		
		xAttack=xAttack+5;
		yAttack=yAttack-Image.getHeight(loadImageInterface.imgAttack)-5;
		bntAttack_2= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntAttack_2.setPos(xAttack,yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);
		
		xAttack=xAttack+Image.getWidth(loadImageInterface.imgAttack)/3+10;
		yAttack=yAttack-Image.getHeight(loadImageInterface.imgAttack)+5;
		bntAttack_3= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntAttack_3.setPos(xAttack,yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);
		
		xAttack=xAttack+Image.getWidth(loadImageInterface.imgAttack)+5;
		yAttack=yAttack;
		bntAttack_4= new Command("", this, Contans.CHAR_BROAD, null, 0, 0);
		bntAttack_4.setPos(xAttack,yAttack, loadImageInterface.imgAttack, loadImageInterface.imgAttack);
		
		
	}
	public static int indexshow;
	public void update()
	{
//		updateKey();
//		updatePointer();
//		if(GameScr.isShowFocus){
//			if(GameScr.ypaintFocus<0)
//			indexshow++;
//			GameScr.ypaintFocus+=indexshow;
//			if(GameScr.ypaintFocus>0) GameScr.ypaintFocus = 0;
//		}else {
//			if(GameScr.ypaintFocus>-loadImageInterface.imgfocusActor.getHeight())
//			indexshow++;
//			GameScr.ypaintFocus-=indexshow;
//			if(GameScr.ypaintFocus<-loadImageInterface.imgfocusActor.getHeight()) GameScr.ypaintFocus = -loadImageInterface.imgfocusActor.getHeight();
//		}
		if(moveClose)
		{
			if(xMove>limMove)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v++;
			}
			
			
		}else
		{
			
			if(xMove<=0)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v--;
			}
			if((mSystem.currentTimeMillis()-timestart)/1000>timeoff)
				moveClose = true;
		}

		menuIcon.update();
	}
	
	public void UpdateKey()
	{
		boolean isPress = false;
		if(moveClose&&!GameScr.gI().guiChatClanWorld.moveClose){
			isPress = updateKeyTouchControl();
//			updateRightCOntrol();
		}
		if (menuIcon.indexpICon==0&&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntCharBoard)) {

			if (bntCharBoard != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (bntCharBoard != null)
				{
					bntCharBoard.performAction();	
				}
			}
		}
		if (menuIcon.indexpICon==0&&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnchangeSkill)) {

			if (bntCharBoard != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnchangeSkill != null)
				{
					btnchangeSkill.performAction();	
				}
			}
		}
		
		menuIcon.updateKey();
		if (!isPress&&GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick&&!moveClose){
			moveClose = true;
		}
		GameCanvas.clearKeyPressed();
	}
	public void Paint(MGraphics g)
	{
		//GameScr.paintCmdBar(graphic);
		int iMppaint = (int)((Char.myChar().cMP*(MGraphics.getImageWidth(loadImageInterface.imgMp)-19))//phần dư ko paint
				/Char.myChar().cMaxMP);
		g.drawRegion(loadImageInterface.imgMp, 0, 0,
				iMppaint+19,
				MGraphics.getImageHeight(loadImageInterface.imgMp),
				0, bntCharBoard.x+36, bntCharBoard.y+12, MGraphics.TOP| MGraphics.LEFT, false);
		
		int hppaint = ((Char.myChar().cHP* MGraphics.getImageWidth(loadImageInterface.imgBlood))/Char.myChar().cMaxHP);
		g.drawRegion(loadImageInterface.imgBlood, 0, 0,
				hppaint,
				MGraphics.getImageHeight(loadImageInterface.imgBlood),
				0,  bntCharBoard.x+56, bntCharBoard.y+21, MGraphics.TOP| MGraphics.LEFT, false);
		int Exppaint = (int)((Char.myChar().cEXP* MGraphics.getImageWidth(loadImageInterface.imgExp))/Char.myChar().cMaxEXP);
		g.drawRegion(loadImageInterface.imgExp, 0, 0,
				Exppaint,
				MGraphics.getImageHeight(loadImageInterface.imgExp),
				0,  bntCharBoard.x+54, bntCharBoard.y+35, MGraphics.TOP| MGraphics.LEFT, false);
		g.fillRect( GameCanvas.w-30,38, 30, 20);
		if(TileMap.mapName!=null)
		mFont.tahoma_7_red.drawString(g,TileMap.mapName, GameCanvas.w,40, 1);
		
//		graphic.drawImage(loadImageInterface.imgMp, bntCharBoard.x+36, bntCharBoard.y+12);
		//		graphic.drawImage(loadImageInterface.imgBlood, bntCharBoard.x+56, bntCharBoard.y+21);
//		graphic.drawImage(loadImageInterface.imgExp, bntCharBoard.x+54, bntCharBoard.y+35);
		btnchangeSkill.paint(g);
		bntCharBoard.paint(g);
		if(Char.myChar()!=null&&Char.myChar().cName!=null)
		mFont.tahoma_7b_white.drawString(g,Char.myChar().cName,  bntCharBoard.x+60, bntCharBoard.y+45, 0);
		mFont.tahoma_7_white.drawString(g, Char.myChar().cHP+"/"+Char.myChar().cMaxHP,
				 bntCharBoard.x+95, bntCharBoard.y+20, 2);
		mFont.tahoma_7_white.drawString(g, Char.myChar().cMP+"/"+Char.myChar().cMaxMP,
				 bntCharBoard.x+95, bntCharBoard.y+31, 2);
		
		if(moveClose)//khi menu hien thi
		{
			paintTouchControl(g);
			bntAttack.paint(g);
			bntAttack_1.paint(g);
			bntAttack_2.paint(g);
			bntAttack_3.paint(g);
			bntAttack_4.paint(g);
		}
		
		GameScr.resetTranslate(g);
		if(Char.myChar().npcFocus!=null||Char.myChar().mobFocus!=null||Char.myChar().charFocus!=null
				||Char.myChar().itemFocus!=null||Char.myChar().npcFocus!=null){
			int hppaintFocus = 0;
			if(Char.myChar().npcFocus!=null) hppaintFocus = MGraphics.getImageWidth(loadImageInterface.imgBlood);
			else if(Char.myChar().charFocus!=null) {
				hppaintFocus = Char.myChar().charFocus.cHP* MGraphics.getImageWidth(loadImageInterface.imgBlood)/Char.myChar().charFocus.cMaxHP;
			}
			else if(Char.myChar().mobFocus!=null) {
				hppaintFocus = Char.myChar().mobFocus.hp* MGraphics.getImageWidth(loadImageInterface.imgBlood)/Char.myChar().mobFocus.maxHp;
			}
			g.drawRegion(loadImageInterface.imgBlood, 0, 0,
					hppaintFocus,
					MGraphics.getImageHeight(loadImageInterface.imgBlood),
					0,   GameCanvas.w/2+24, loadImageInterface.imgfocusActor.getHeight()/2-6/*+GameScr.ypaintFocus*/, MGraphics.TOP| MGraphics.LEFT, false);
			
			g.drawImage(loadImageInterface.imgfocusActor, GameCanvas.w/2+43, loadImageInterface.imgfocusActor.getHeight()/2/*+GameScr.ypaintFocus*/, MGraphics.VCENTER| MGraphics.HCENTER);
			if(Char.myChar().npcFocus!=null&&Char.myChar().npcFocus.template.name!=null)
				mFont.tahoma_7b_white.drawString(g, Char.myChar().npcFocus.template.name,  GameCanvas.w/2+28, loadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScr.ypaintFocus*/, 0);
			else if(Char.myChar().charFocus!=null&&Char.myChar().charFocus.cName!=null){
				mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.cName,  GameCanvas.w/2+28, loadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScr.ypaintFocus*/, 0);
				mFont.tahoma_7_white.drawString(g, Char.myChar().charFocus.cHP+"/"+Char.myChar().charFocus.cMaxHP,
						GameCanvas.w/2+68, loadImageInterface.imgfocusActor.getHeight()/2-7/*+GameScr.ypaintFocus*/, 2);
				
			}
			else if(Char.myChar().mobFocus!=null&&Char.myChar().mobFocus.mobName!=null){
				mFont.tahoma_7_white.drawString(g, Char.myChar().mobFocus.hp+"/"+Char.myChar().mobFocus.maxHp,
						GameCanvas.w/2+68, loadImageInterface.imgfocusActor.getHeight()/2-7/*+GameScr.ypaintFocus*/,2);
				mFont.tahoma_7b_white.drawString(g, Char.myChar().mobFocus.mobName,  GameCanvas.w/2+28, loadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScr.ypaintFocus*/, 0);
			}
			
		}
		menuIcon.y=this.y+xMove;
		menuIcon.paint(g);
	}

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		switch (idAction) {
		case Contans.CHAR_BROAD:
			
			if(!moveClose)
			{
				moveClose=true;
			}
			else
			{
				moveClose=false;
			}
			if(!moveClose) 
				timestart = mSystem.currentTimeMillis();
			break;
		case 20:

			GameScr.changeKillID++;
			if(GameScr.changeKillID > 18)
				GameScr.changeKillID = 0;
			break;
			
		default:
			break;
		}
	}
	public static void paintCmdBar(MGraphics g) {
		Char.myChar().cHP  = 100;
		Char.myChar().cMaxHP = 100;
		Char.myChar().cMP = 50;
		Char.myChar().cMaxMP = 50;
		
		int exp=2;
		int expMax=100;
	
		g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

		// // PAINT HP
		int hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
		Cout.println(" paint  "+loadImageInterface.imgBlood);
		//hp
		if (hpWidth > hpBarW)
			hpWidth = 0;
		g.drawRegion(loadImageInterface.imgBlood,0,0,hpWidth,Image.getHeight(loadImageInterface.imgBlood), 0,hpBarX + 4, hpBarY - 2, 0);

		//mp
		hpWidth=Char.myChar().cMP * Image.getWidth(loadImageInterface.imgExp) / Char.myChar().cMaxMP;
		g.drawRegion(loadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(loadImageInterface.imgExp), 0,hpBarX + 4, hpBarY -12, 0);

		//exp
		hpWidth=exp * Image.getWidth(loadImageInterface.imgExp) / expMax;
		g.drawRegion(loadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(loadImageInterface.imgExp), 0,hpBarX + 4, hpBarY +12, 0);

	}
	
	public static void loadCmdBar() {
//		if (imgCmdBar == null) {
//			imgCmdBar = new Image[2];
//			for (int i = 0; i < 2; i++)
//				imgCmdBar[i] = GameCanvas.loadImage("/u/c" + i + ".png");
//		}
//		cmdBarLeftW = MGraphics.getImageWidth(imgCmdBar[0]);
//		cmdBarRightW = MGraphics.getImageWidth(imgCmdBar[1]);
//		cmdBarCenterW = gW - cmdBarLeftW - cmdBarRightW + 1;

		hpBarX = 78 - 15 + 10;
		hpBarY = cmdBarY;
		hpBarW = GameScr.gW - 84 - 30;
		expBarW = GameScr.gW - 44 - 4;
		hpBarH = 20;

//		if (GameCanvas.w > 176) {
//			cmdBarCenterW -= 50;
//			hpBarW -= 50;
//			expBarW -= 50;
//			hpBarX += 15;
//			hpBarW -= 15;
//		}
		loadInforBar();
	}
	
	public static void loadInforBar() {
		if (!GameCanvas.isTouch)
			return;
		hpBarW = 83;
		mpBarW = 57;
		hpBarX = 52;
		hpBarY = 10 + Info.hI;
		expBarW = GameScr.gW - 61;
	}
	
	public static void paintTouchControl(MGraphics g) {
		try{
			GameScr.resetTranslate(g);
//			// ---CHAT
//			graphic.drawImage(GameScr.imgChat, xC + 17, yC + 17, MGraphics.HCENTER | MGraphics.VCENTER);
//			
			
			// ---LEFT
//			graphic.drawImage(imgButton, xL, yL, 0);
			g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR, xL + 21, yL + 16,
					MGraphics.HCENTER | MGraphics.VCENTER);
			if (keyTouch == 4) {
//				graphic.drawImage(imgButton2, xL, yL, 0);
				g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL + 21, yL + 16, MGraphics.HCENTER | MGraphics.VCENTER);
			}

			// ----RIGHT
//			graphic.drawImage(imgButton, xR, yR, 0);
			g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal),  Image.getHeight(loadImageInterface.imgMoveNormal), 0, xR + 30, yR + 16, MGraphics.HCENTER
					| MGraphics.VCENTER);
			if (keyTouch == 6) {
//				graphic.drawImage(imgButton2, xR, yR, 0);
				g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0,Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), 0, xR + 30, yR + 16,
						MGraphics.HCENTER | MGraphics.VCENTER);
			}

			// ----UP
//			graphic.drawImage(imgButton, xU, yU, 0);
			g.drawRegion(loadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(loadImageInterface.imgMoveNormal), Image.getHeight(loadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR_ROT90, xU + 30,
					yU + 20, MGraphics.HCENTER | MGraphics.VCENTER);
			if (keyTouch == 3) {
//				graphic.drawImage(imgButton2, xU, yU, 0);
				g.drawRegion(loadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(loadImageInterface.imgMoveFocus), Image.getHeight(loadImageInterface.imgMoveFocus), Sprite.TRANS_MIRROR_ROT90, xU + 30,
						yU + 20, MGraphics.HCENTER | MGraphics.VCENTER);
			}
			
			// CENTER
			g.drawImage(loadImageInterface.imgMoveCenter, xCenter + 50   , yCenter + 15,  MGraphics.HCENTER | MGraphics.VCENTER);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
//		if (!GameCanvas.isTouch || (GameCanvas.menu.showMenu && GameCanvas.isTouchControlSmallScreen))
//			return;
//		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
//			return;
		
	}
	
	public static boolean updateKeyTouchControl() {

		boolean isPress = false;
		keyTouch = -1;
//		if (GameCanvas.isPointerHoldIn(TileMap.posMiniMapX, TileMap.posMiniMapY, TileMap.wMiniMap, TileMap.hMiniMap)) {
//			if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease) {
//				doShowMap();
//				isPress = true;
//			}
//		}

//		if ( isNotPaintTouchControl())
//			return;
		if (GameCanvas.isPointerHoldIn(xU, yU, 34, 34)) {
			keyTouch = 3;
			GameCanvas.keyHold[2] = true;
//			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown) {
			GameCanvas.keyHold[2] = false;
		}

		if (GameCanvas.isPointerHoldIn(xU - 30, yU, 30, 34)) {

			GameCanvas.keyHold[1] = true;
//			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[1] = false;

		if (GameCanvas.isPointerHoldIn(xU + 34, yU, 30, 34)) {

			GameCanvas.keyHold[3] = true;
//			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[3] = false;

		if (GameCanvas.isPointerHoldIn(xL, yL, 34, 34)) {
			keyTouch = 4;
			GameCanvas.keyHold[4] = true;
//			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[4] = false;

		if (GameCanvas.isPointerHoldIn(xR - 5, yR, 50, 34)) {
			keyTouch = 6;
			GameCanvas.keyHold[6] = true;
//			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[6] = false;


		if (GameCanvas.isPointerHoldIn(xF, yF, 54, 54)) {
			keyTouch = 5;
			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
				System.out.println("ATTACK");
				isPress = true;
			}
		}
		
	
		

		if (GameCanvas.isPointerJustRelease) {
			GameCanvas.keyHold[1] = false;
			GameCanvas.keyHold[2] = false;
			GameCanvas.keyHold[3] = false;
			GameCanvas.keyHold[4] = false;
			GameCanvas.keyHold[6] = false;

			// GameCanvas.keyHold[5] = false;

		}
		return isPress;

	}
	
	public void updateRightCOntrol(){
//		boolean isPress = false;
		if(Screen.getCmdPointerLast(bntAttack)){
			Cout.println("PAIT SKILL");
			keyTouch = 5;
//			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
//				System.out.println("ATTACK");
//				isPress = true;
//			}
		}
	}
}
