package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import GuiOut.loadImageInterface;
import Objectgame.InfoItem;
import screen.old.GameScr;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Screen;
import model.Sprite;
import real.mFont;

public class QShortQuest implements IActionListener{
	
	int width=100,height=100;
	int x=GameCanvas.w-width ,y=GameCanvas.h/2-height/2;//x,y
	
	int xMove,limMove=70;
	Command bntOpen;
	boolean moveClose,moveOpen;
	int v;//gia toc tang toc do move
	
	public QShortQuest()
	{
		bntOpen= new Command("", this, Contans.BUTTON_OPEN, null, 0, 0);
		bntOpen.setPos(x, y+height/2-Image.getHeight(loadImageInterface.imgShortQuest)/2, loadImageInterface.imgShortQuest_Close, loadImageInterface.imgShortQuest_Close);
	}
	
	public void Update()
	{
		if(moveClose)
		{
			if(xMove>limMove)
			{
				v=0;
				xMove +=GameCanvas.w - ( bntOpen.x + Image.getWidth(loadImageInterface.imgShortQuest));
			}
			else
			{
				xMove+=v;
				v++;
			}
		}else
		{
			
			if(xMove<=0)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v--;
			}
		}
	}
	public void UpdateKey()
	{
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntOpen)) {
				if (bntOpen != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (bntOpen != null)
						bntOpen.performAction();
				}
			}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Contans.BUTTON_OPEN:
			if(!moveClose)
			{
				moveClose=true;
//				bntOpen.setPos(x, y+height/2-loadImageInterface.imgShortQuest.getHeight()/2 + 27, loadImageInterface.imgShortQuest_Close, loadImageInterface.imgShortQuest_Close);
				bntOpen.setPos(x, y+height/2-Image.getHeight(loadImageInterface.imgShortQuest)/2, loadImageInterface.imgShortQuest, loadImageInterface.imgShortQuest_Close);
			}
			else
			{
				moveClose=false;
				bntOpen.setPos(x, y+height/2-Image.getHeight(loadImageInterface.imgShortQuest)/2, loadImageInterface.imgShortQuest_Close, loadImageInterface.imgShortQuest_Close);
			}
			break;

		default:
			break;
		}
	}
	public void Paint (MGraphics g)
	{
		GameScr.resetTranslate(g);
		PaintFrameChat(g);
//		graphic.setColor(0x001919);
//		graphic.fillRect(x+xMove+Image.getWidth(loadImageInterface.imgShortQuest),y, width,height);
		for (int i = 0; i < GuiQuest.listNhacNv.size(); i++) {
			InfoItem infonv = (InfoItem)GuiQuest.listNhacNv.elementAt(i);
			if(infonv!=null)
				mFont.tahoma_7_white.drawString(g, infonv.s, x+4+xMove+Image.getWidth(loadImageInterface.imgShortQuest),y+4+i*(Screen.ITEM_HEIGHT+2), 0);
		}
		bntOpen.x=x+xMove;
		bntOpen.y=bntOpen.y;
		bntOpen.paint(g,2);//paint button open-close
		
	}

	private void PaintFrameChat(MGraphics g)
	{
		int xpaint = x+Image.getWidth(loadImageInterface.imgShortQuest)-1;
		int w = 10,h = 8;
		//first row
				g.drawImage(loadImageInterface.imgChatConner, xpaint+xMove, y, MGraphics.TOP | MGraphics.LEFT);
				for(int i=1;i<w;i++)
				{
					g.drawImage(loadImageInterface.imgChatRec, xpaint+i*(Image.getWidth(loadImageInterface.imgChatRec))+xMove, y, 0,true);
				}
				g.drawRegion(loadImageInterface.imgChatConner, 0, 0,Image.getWidth(loadImageInterface.imgChatConner),
						Image.getHeight(loadImageInterface.imgChatConner), Sprite.TRANS_MIRROR, xpaint +w*(Image.getWidth(loadImageInterface.imgChatConner))+xMove, y, MGraphics.TOP | MGraphics.LEFT);
				
				//paint all rac of frame
				for(int i=1;i<h;i++)
					for(int j=0;j<w;j++)
					{
						g.drawImage(loadImageInterface.imgChatRec, xpaint+j*(Image.getWidth(loadImageInterface.imgChatRec))+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*i, 0,true);
					}
				
				
				//graphic.drawImage(loadImageInterface.imgChatConner, xpaint+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*(h), MGraphics.TOP | MGraphics.LEFT);
				g.drawRegion(loadImageInterface.imgChatConner, 0, 0, Image.getHeight(loadImageInterface.imgChatRec), Image.getHeight(loadImageInterface.imgChatRec),
				1,xpaint+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*(h), MGraphics.TOP | MGraphics.LEFT);
				for(int i=1;i<w;i++)
				{
					g.drawImage(loadImageInterface.imgChatRec, xpaint+i*(Image.getWidth(loadImageInterface.imgChatRec))+xMove, y+Image.getHeight(loadImageInterface.imgChatRec)*(h), 0,true);
				}
				
				g.drawRegion(loadImageInterface.imgChatRec, 0, 0,Image.getWidth(loadImageInterface.imgChatRec),
						Image.getHeight(loadImageInterface.imgChatRec), Sprite.TRANS_MIRROR, xpaint +w*(Image.getWidth(loadImageInterface.imgChatRec))+xMove,y+Image.getHeight(loadImageInterface.imgChatRec)*(h), MGraphics.TOP | MGraphics.LEFT);
			}
	


}
