package Gui;

import model.Image;

import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;


public class FrameImage {
	
	public int frameWidth;
	public int frameHeight;
	public int nFrame;

	private Bitmap imgFrame;
	private int pos[];
	private int totalHeight;

	private Bitmap[] imgList;
	private boolean isRotate;

	public FrameImage(Bitmap img, int width, int height) {
		imgFrame = img;
		frameWidth = width;
		frameHeight = height;
		totalHeight = Image.getHeight(img);
		nFrame = totalHeight / height;
		pos = new int[nFrame];
		for (int i = 0; i < nFrame; i++){
			pos[i] = i * height;
		}
	}

	public void drawFrame(int idx, int x, int y, int trans, int anchor, MGraphics g) {
			if (idx >= 0 && idx < nFrame){
				g.drawRegion(imgFrame, 0, pos[idx], frameWidth, frameHeight, trans, x, y, anchor);
			}
	}

	public void unload() {
		imgFrame = null;
		pos = null;
	}

}
