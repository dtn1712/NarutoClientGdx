package Gui;

import java.io.DataInputStream;

import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import model.Image;



public class mImage {
	public Bitmap image;

	public static String getLink(String str) {
		return str;
	}

	public static mImage createImage(String url) {
		mImage img = new mImage();
		try {
//			img.image = Bitmap.createBitmap(img.image);
			img.image = Image.createImage("/x" + MGraphics.zoomLevel +url);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(img.image==null)
			return null;
		return img;
	}
	public static mImage createImageAll(String url) {
		mImage img = new mImage();
		try {
			img.image = Image.loadImageFromAsset(url);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(img.image==null)
			return null;
		return img;
	}
	public static DataInputStream openFile(String path) {
		DataInputStream file = null;
		file = new DataInputStream("".getClass().getResourceAsStream(
				mImage.getLink(path)));
		return file;
	}

	public static mImage createImage(int w, int h) {
		mImage img = new mImage();
		img.image = Image.createImage(w* MGraphics.zoomLevel, h* MGraphics.zoomLevel);
		return img;
	}

	public static mImage createImage(byte[] data, int w, int h) {// chua dung dau ca
		mImage img = new mImage();
		img.image = Image.createImage(data, 0, data.length);
		return img;
	}

//	public TemGraphics getGraphics() {
//		TemGraphics tem=new TemGraphics();
//		tem.spriteBatch=this.image.getGraphics();
//		return tem;
//	}

	public static int getImageWidth(Bitmap image) {
		return image.getWidth() / MGraphics.zoomLevel;
	}

	public static int getImageHeight(Bitmap image) {
		return image.getHeight() / MGraphics.zoomLevel;
	}
//	public void getRGB(int[] rgbData, int offset, int scanlength, int x, int y, int width, int height)
//	{
//		this.image.getRGB(rgbData, offset, scanlength, x, y, width, height);
//	}
//	public static mImage createRGBImage(int[] rgb, int width, int height, boolean processAlpha)
//	{
//		mImage img=new mImage();
//		img.image=Bitmap.createRGBImage(rgb,width, height,processAlpha);
//		return img;
//	}
}
