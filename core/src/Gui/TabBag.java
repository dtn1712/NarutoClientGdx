package Gui;




import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import real.Service;
import real.mFont;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.Item;
import model.Command;
import model.Image;
import model.Paint;
import model.Screen;
import lib.Bitmap;
import lib.Cout;

/*
 * this class inventory tab
 */
public class TabBag extends MainTabNew{
	
	int maxw, maxh, indexPaint = 12, winfo = 140;
	int numW = 7, numH = 6, numHPaint, maxSize = 60;
//	int[] xItem = new int[Char.myChar().arrItemBag.length], yItem = new int[Char.myChar().arrItemBag.length];
	int[] xItem = new int[9], yItem = new int[9];
	public ListNew listContent = null;
	ListNew list;
	int idSelect = 0;
	Command cmdSelect;//use
	Command cmdXoaItem;//drop item
	int hcmd = 0;
	Bitmap imgCoins1,imgCoins2;


	public TabBag(String name)
	{
		if (GameCanvas.isTouch)
			idSelect = -1;
		else
			idSelect = 0;
		typeTab=INVENTORY;
		this.nameTab=name;
		xBegin = super.xTab + wOneItem/2 -2;
		yBegin = super.yTab + 30 + wOneItem - 5;
		maxw = (wblack - 8) / 32;
		maxh = (hblack - 8) / 32;
		cmdSelect = new Command("Sử dụng", this, Contans.BTN_USE_ITEM,null, 0,0);
		cmdSelect.setPos(xBegin-widthSubFrame, yBegin+heightSubFrame*2/3-10,loadImageInterface.img_use ,loadImageInterface.img_use_focus);
		
		cmdXoaItem = new Command("Vứt bỏ", this, Contans.BTN_DROP_ITEM,null, 0,0);
		cmdXoaItem.setPos(xBegin+Image.getWidth(loadImageInterface.img_use)-widthSubFrame+13, yBegin+heightSubFrame*2/3-10,loadImageInterface.img_use ,loadImageInterface.img_use_focus);
		init();
		
	}
	private void LoadImage()
	{
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
	}
	int cItem = 0, rItem = 0;
	public void paint(MGraphics g)
	{
		
		g.setColor(color[1]);
		int dem = 0;
		
		//money
		g.drawImage(imgCoins1, xBegin,yBegin-Image.getHeight(imgCoins1)-1,0);
		mFont.tahoma_7b_white.drawString(g, "10000",
				xBegin+33,yBegin-Image.getHeight(imgCoins1)/2-2, 0);
		
		//gold
		g.drawImage(imgCoins2, xBegin+(Image.getWidth(loadImageInterface.ImgItem))*7,yBegin-Image.getHeight(imgCoins2)-1,g.TOP|g.RIGHT);
		mFont.tahoma_7b_white.drawString(g, "10000",
				xBegin+(Image.getWidth(loadImageInterface.ImgItem)+2)*7-Image.getWidth(imgCoins1)+35,yBegin-Image.getHeight(imgCoins1)/2-2,0);
		//paint image item
		for(int i=0;i<numW;i++)
			for(int j=0;j<numH;j++)
			{
				g.drawImage(loadImageInterface.ImgItem,xBegin+(Image.getWidth(loadImageInterface.ImgItem))*i,yBegin+(Image.getHeight(loadImageInterface.ImgItem))*j+2, 0);
			}
		// paint item
		for(int i = 0; i < Char.myChar().arrItemBag.length; i++){	
			dem++;
			
			Item it = Char.myChar().arrItemBag[i];
			int r = it.indexUI / 7;
			int c = it.indexUI - (r * 7);
		

			if(it != null){
//				it.paintItem(graphic, xBegin+(Image.getWidth(loadImageInterface.ImgItem)+2)*c + 13,
//						yBegin+(Image.getHeight(loadImageInterface.ImgItem)+2)*r + 13);
//				System.out.println(" LOCAT ----> "+(((xBegin)+(Image.getWidth(loadImageInterface.ImgItem)+2)*c + 13)));
				it.paintItem(g, xBegin+(Image.getWidth(loadImageInterface.ImgItem))*c + 14, 
						(yBegin+(Image.getHeight(loadImageInterface.ImgItem))*r + 14) + 2);
			}
				
			if (idSelect > -1 && Focus == INFO) {//focus
				setPaintInfo();
				
				g.drawImage(loadImageInterface.imgFocusSelectItem, (xBegin + ((idSelect % numW)*(Image.getWidth(loadImageInterface.ImgItem))))+14 , yBegin
						+ (idSelect / numW) * (Image.getHeight(loadImageInterface.ImgItem) ) +16, MGraphics.VCENTER| MGraphics.HCENTER);
				Paint.SubFrame(xTab-widthSubFrame,yTab,widthSubFrame,heightSubFrame, g);
				paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame + 10,yTab+10);
				cmdSelect.paint(g);
				cmdXoaItem.paint(g);
//				paint.paintItemInfo(graphic, itemFocus, xTab-widthSubFrame + 10,yTab-25+ GameCanvas.h / 5);
//				paintNameItem(graphic, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 20, longwidth, name, colorName);
//				paintNameItem(graphic, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 30, longwidth, "Tấn công: 1000", colorName);
//				paintNameItem(graphic, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 40, longwidth, "Độ bền: 10", colorName);
//				paintNameItem(graphic, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 50, longwidth, "tắng sát thương vật lý 10%", colorName);
//				paintPopupContent(graphic, false);
			}
			
//			if (!GameCanvas.menu2.isShowMenu && GameCanvas.currentDialog == null) {
				if (Focus == INFO && timePaintInfo > timeRequest) {
////					GameCanvas.menu2.isShowMenu = true;
					
					//paintNameItem(graphic, xTab-widthSubFrame ,yTab-30+ GameCanvas.h / 5, longwidth, name, colorName);
//					paintPopupContent(graphic, false);
					
				}
//			}
		}
//		paintRect(graphic);
	}
	
	
	public Item getItem(Item[] item){
		for(int i = 0; i < item.length; i++){
			Item it = item[i];
			return it;
		}
		return null;
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Contans.BTN_USE_ITEM:
			Cout.println("su dung");
			Service.gI().useItem((byte)idSelect, Char.myChar().arrItemBag[idSelect]);
//			Service.getInstance().giveUpItem((byte)idSelect);
			break;
		case Contans.BTN_DROP_ITEM:
			Cout.println("vut bo");
//			Service.getInstance().useItem((byte)idSelect, Char.myChar().arrItemBag[idSelect]);
			Service.gI().giveUpItem((byte)idSelect);
			idSelect = -1;
			break;
		default:
			break;
		}
	}
	public void updatePointer() {
		if(imgCoins1==null){
			LoadImage();
		}
		if (GameCanvas.isPointSelect(xBegin, yBegin, numW * (Image.getWidth(loadImageInterface.ImgItem) + 2), 
				numH * (Image.getHeight(loadImageInterface.ImgItem) + 4))) {
			
			int row=(GameCanvas.px - xBegin)/ (Image.getWidth(loadImageInterface.ImgItem));
			int col=(GameCanvas.py - yBegin) / (Image.getWidth(loadImageInterface.ImgItem));
			
			 row=(GameCanvas.px - xBegin-(row-1)*2)/ (Image.getWidth(loadImageInterface.ImgItem));
			 col=(GameCanvas.py - yBegin-(col-1)*2) / (Image.getWidth(loadImageInterface.ImgItem));
			
			
			if(row>6)
				row=6;
			if(col>5)
				col=5;
			
			int select=row+col*numW;
			
			int size = 0;
			if (typeTab == INVENTORY){
				size = Char.myChar().arrItemBag.length;
				
			}
			 
			if (select >= 0 && select < size) {
				GameCanvas.isPointerClick = false;
				if (select == idSelect) {

						
				} else {
					timePaintInfo = 0;
					idSelect = select;
					listContent = null;
				}
				if (MainTabNew.Focus != MainTabNew.INFO)
					MainTabNew.Focus = MainTabNew.INFO;
			} else {
				idSelect = -1;
				timePaintInfo = 0;
				listContent = null;
			}
		}

		
//		if (timePaintInfo == MainTabNew.timeRequest) {
//			setPaintInfo();
//		}
	}
	
	public void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdSelect)) {
			if (cmdSelect != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdSelect != null)
					cmdSelect.performAction();
			}
		}
		
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdXoaItem)) {
			if (cmdXoaItem != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdXoaItem != null)
					cmdXoaItem.performAction();
			}
		}
	}
	Item itemFocus = null;
	public void setPaintInfo() {
		Item item = Char.myChar().arrItemBag[idSelect];
		itemFocus = Char.myChar().arrItemBag[idSelect];
		name = item.template.name;

	}

}
