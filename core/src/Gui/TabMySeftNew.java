package Gui;


import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;


import model.Command;
import model.Image;
import model.Paint;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.Item;
import real.mFont;
import screen.old.GameScr;
import lib.Bitmap;
import lib.mVector;

/*
 * this equip
 */
public class TabMySeftNew  extends MainTabNew{
	int numW, numH;
	public static int maxSize = 12;
	int h12, w5;
	public static int delta = 0;
	byte idSelect;
	// info
	int xStart, yStart;
	int[] mColorInfo;
	// list item
	mVector vecItemMenu = new mVector();
	public static String[] meffskill = new String[5];
	boolean isList = false;
	int maxList, selectList, xList, yList, timeUpdateInfo;
	boolean isShowInfo = false;
	
	private Bitmap[] arrayBGHuman=new Bitmap[25];
	private Bitmap imgButton_dressed,imgButton_dressed2,
	imgChien_luc,imgCoins1,imgCoins2,imgExp_tube,imgExp,imgLevel;
	
	int xBGHuman=xTab+widthFrame/6-10;
	int yBGHuman=yTab+ GameCanvas.h /6 -10;

	int wsize;
	
	int widthBGChar=130;
	int heightBGChar=100;
	
	Command dropAllItem,shop,improve;
	
	public TabMySeftNew(String nametab) {
		xBGHuman=xTab+16;
//		LoadImage();
		if (GameCanvas.isTouch)
			idSelect = -1;
		else
			idSelect = 0;
		xStart = xBegin + wblack / 2 - (numW * wOneItem) / 2 +(numW / 2);
		yStart = yBegin +10;
		wsize = wOneItem;
		typeTab = EQUIP;
		yBGHuman=yTab+15;
		this.nameTab = nametab;
		int xCmd=xBGHuman+widthBGChar+32;
		int yCmd=yBGHuman+27;
		
		dropAllItem= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		dropAllItem.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		shop= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		shop.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		improve= new Command("", this, Contans.BTN_USE_ITEM,null, 0,0);
		improve.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
	}
	public void init() {
		// TODO Auto-generated method stub
		if (GameCanvas.isTouch)
			idSelect = -1;
		else
			idSelect = 0;
		isList = false;

	}
	public void LoadImage()
	{
		for(int i=0;i<25;i++)
		{
			arrayBGHuman[i]=GameCanvas.loadImage("/GuiNaruto/imageBGChar/bgCharacter_01_"+(i+1)+".png");
		}
		imgButton_dressed=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed.png");
		imgButton_dressed2=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed2.png");;
		imgChien_luc=GameCanvas.loadImage("/GuiNaruto/myseft/chien_luc.png");
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
		imgExp_tube=GameCanvas.loadImage("/GuiNaruto/myseft/exp_tube.png");
		imgExp=GameCanvas.loadImage("/GuiNaruto/myseft/exp.png");
		imgLevel=GameCanvas.loadImage("/GuiNaruto/myseft/level.png");
		
	}
	
	//paint bg human
	private void PaintBGHuman(int x,int y,MGraphics g)
	{
		int wImage=Image.getWidth(arrayBGHuman[0]);
		int hImage=Image.getHeight(arrayBGHuman[0]);
		int index=0;
		for(int i=1;i<6;i++)
			for(int j=1;j<6;j++)
			{
				g.drawImage(arrayBGHuman[index],x+j*wImage,y+i*hImage,0);
				index++;
			}
	}
	
	//paint exp
	void PaintExp(int x,int y ,MGraphics g)
	{
		int Exppaint = (int)((Char.myChar().cEXP* MGraphics.getImageHeight(imgExp))/Char.myChar().cMaxEXP);
		Exppaint = (Exppaint<7&&Exppaint!=0?7:Exppaint);
		g.drawRegion(imgExp, MGraphics.getImageHeight(imgExp)-Exppaint,0,Image.getWidth(imgExp),
				Exppaint,
				0,x+3,y+2+(MGraphics.getImageHeight(imgExp)-Exppaint), MGraphics.TOP| MGraphics.LEFT);
		g.drawImage(imgExp_tube,x,y,0);
		g.drawImage(imgLevel,x+Image.getWidth(imgExp)/2+2,y+Image.getHeight(imgExp)+10,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,"15",x+Image.getWidth(imgExp)/2 - 3,y+Image.getHeight(imgExp)+3,g.VCENTER|g.HCENTER);
	}
	
	void PaintItem(int x,int y ,MGraphics g)
	{
//		graphic.drawImage(loadImageInterface.ImgItem,x,y-(loadImageInterface.ImgItem.getHeight()+2), 0);
//		graphic.drawImage(loadImageInterface.ImgItem,x+(loadImageInterface.ImgItem.getWidth()+2)*6,y-(loadImageInterface.ImgItem.getHeight()+2), 0);
		for(int i=0;i<2;i++)
			for(int j=0;j<7;j++)
			{
				g.drawImage(loadImageInterface.ImgItem,x+(Image.getWidth(loadImageInterface.ImgItem))*j,y+(Image.getHeight(loadImageInterface.ImgItem))*i, 0);
			}
		
		
		
	}
	void PaintStrong(MGraphics g, int x, int y)
	{
		g.drawImage(imgChien_luc,x,y, g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, "123456",x - 15, y-2,g.VCENTER|g.HCENTER);
	}
	void PaintGoldMoney(int x,int y ,MGraphics g)
	{
		
	}
	public void paint(MGraphics g)
	{
		
		PaintBGHuman(xBGHuman,yBGHuman,g);
		PaintExp(xBGHuman+5,yBGHuman+20,g);//paint exp
		PaintStrong(g,xBGHuman+26+widthBGChar/2,yBGHuman+20+heightBGChar+3);
		PaintItem(xBGHuman-5,yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10,g);
		
		int pointPk=100;
		int PointSucKhoe=200;
		
//		graphic.setColor(color[1]);
//		graphic.fillRect(xBegin + w5 * 2 + delta, yBegin + h12 / 4, 1, h12 * 8
//				- h12 / 2);
	
//		mFont.tahoma_7b_green.drawString(graphic, "Pk: " + pointPk,
//				xBegin + 4, yBegin + h12 * 6 - h12 / 2 + 4, 0);
//		mFont.tahoma_7b_red.drawString(graphic, T.suckhoe
//				+ PointSucKhoe, xBegin + 4, yBegin + h12 * 6
//				- h12 / 2 + GameCanvas.hText + 4, 0);
		
		// paint 12 item trang bi
//		graphic.setColor(color[4]);
		try{
			for (byte i = 0; i < 12; i++) {// paint 12 item trang bi
				int xp = ((xBGHuman-5) + i % 7 * (24)) + ((i * 4) + 1), yp = ((yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10) + i / 7
						* (24)) + 1 ;
				
				if(Char.myChar().arrItemBody!=null){
					Item item = (Item) Char.myChar().arrItemBody[i];
					if (item != null) {
					    if (item.template.id > -1){ // paints trang bi
					    		item.paintItem(g, xp + wOneItem / 2+1, yp + wOneItem / 2+1 );
					    	}
					   
				   }
				}
				 if (idSelect > -1 && Focus == INFO ) {//focus
					 	setPaintInfo();
						g.drawImage(loadImageInterface.imgFocusSelectItem,
								(xBGHuman-5+ ((idSelect % 7)*(Image.getWidth(loadImageInterface.ImgItem))))+2,
								yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10
								+ (idSelect / 7) * (Image.getHeight(loadImageInterface.ImgItem) )+2 , 0);
						Paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame + 10,yTab-20+ GameCanvas.h / 5);
//						Paint.SubFrame(xTab-widthSubFrame,yTab-30+ GameCanvas.h / 5,widthSubFrame,heightSubFrame, graphic);
//						cmdSelect.paint(graphic);
//						cmdXoaItem.paint(graphic);
					}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		if (Focus == INFO && idSelect >= 0) {// paint select
//			int xp = xStart + idSelect % numW * (wsize), yp = yStart + idSelect
//			/ numW * (wsize);
////			setPaintInfo();
//		paintPopupContent(graphic, false);
//		graphic.drawRect(xp, yp, (wsize), (wsize));
//		graphic.setColor(color[2]);
//		graphic.drawRect(xp + 1, yp + 1, (wsize) - 2, (wsize) - 2);
//		
//		}
		
		//paint background		
		//left col
//		for(int i=0;i<5;i++)
//		{
//			graphic.drawImage(loadImageInterface.ImgItem,xTab + width/8+(loadImageInterface.ImgItem.getWidth()+2),yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*i, 0);
//		}
//		
//		//right col
//		for(int i=0;i<5;i++)
//		{
//			graphic.drawImage(loadImageInterface.ImgItem,xTab + width*4/8+(loadImageInterface.ImgItem.getWidth()+2),yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*i, 0);
//		}
//		
//		//bottom rows
//		for(int i=2;i<4;i++)
//		{
//			graphic.drawImage(loadImageInterface.ImgItem,xTab + width/8+(loadImageInterface.ImgItem.getWidth()+5)*i,yBegin-5+(loadImageInterface.ImgItem.getHeight()+2)*4, 0);
//		}
		
//		GameScr.currentCharViewInfo.paintChar(graphic, xTab + width/2, yBegin + h12 * 5 / 2 + 50 );
		GameScr.currentCharViewInfo.paintChar(g,xBGHuman+90,yBGHuman+120);
		
		dropAllItem.paint(g);
		shop.paint(g);
		improve.paint(g);
		
	}
	
	public void updatePointer() {
		boolean ismove = false;
		if(imgButton_dressed==null)
			LoadImage();
		if (isList) {

			if (listContent != null) {
				if (GameCanvas.isPoint(listContent.x, listContent.y,
						listContent.maxW, listContent.maxH)) {
					listContent.update_Pos_UP_DOWN();
					ismove = true;
				}
			}
//			if (GameCanvas.isTouch && !ismove) {
//				list.updatePos_LEFT_RIGHT();
//				GameScreen.cameraSub.xCam = list.cmx;
//			}
			if (GameCanvas.isPointerClick /*&& !ismove*/) {
				if (GameCanvas.isPoint(xList, yList, (wsize) * maxList,
						wOneItem)) {
					byte select = (byte) ((/*GameScreen.cameraSub.xCam*/
							+ GameCanvas.px - xList) / (wsize));
					if (select >= 0 && select < vecItemMenu.size()) {
//						if (select == selectList) {
//							cmdChangeEquip.perform();
//						} else {
//							selectList = select;
//							timePaintInfo = 0;
//						}
//						listContent = null;
					}
					GameCanvas.isPointerClick = false;
				} else if (!GameCanvas.isPoint(0, GameCanvas.h
						- GameCanvas.hCommand, GameCanvas.w,
						GameCanvas.hCommand)) {
//					cmdCloseChange.perform();
					GameCanvas.isPointerClick = false;
				}

			}
		} else {
			if (listContent != null) {
				if (GameCanvas.isPoint(listContent.x, listContent.y,
						listContent.maxW, listContent.maxH)) {
					listContent.update_Pos_UP_DOWN();
					ismove = true;
				}
			}
			if (GameCanvas.isPointSelect(xStart, yStart, (wsize) * numW,
					(wsize) * numH)
					&& !ismove) {
				GameCanvas.isPointerClick = false;
				byte select = (byte) ((GameCanvas.px - xStart) / (wsize) + ((GameCanvas.py - yStart) / (wsize))
						* numW);
				if (select >= 0 && select < maxSize) {
					if (select == idSelect) {
						setPaintInfo();
//						cmdChange.perform();
					} else {
						idSelect = select;
						timePaintInfo = 0;

					}
					listContent = null;
					if (MainTabNew.Focus != MainTabNew.INFO)
						MainTabNew.Focus = MainTabNew.INFO;
				}

			}
		}
		int x=xBGHuman-5;
		int y=yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10;
		int w=(Image.getWidth(loadImageInterface.ImgItem))*7;
		int h=(Image.getHeight(loadImageInterface.ImgItem))*2;
		
		if (GameCanvas.isPointSelect(x,y,w,h))
		{
			
			//khoang cach 2 pixel giua cac o
			int row=(GameCanvas.px - x)/ (Image.getWidth(loadImageInterface.ImgItem));
			int col=(GameCanvas.py - y) / (Image.getHeight(loadImageInterface.ImgItem));
			
			 row=(GameCanvas.px - x-(row-1)*2)/ (Image.getWidth(loadImageInterface.ImgItem));
			 col=(GameCanvas.py - y-(col-1)*2) / (Image.getHeight(loadImageInterface.ImgItem));
			
			if(row>6)
				row=6;
			if(col>1)
				col=1;
			
			idSelect=(byte) (row+col*7);
			GameCanvas.clearKeyPressed();
			GameCanvas.clearKeyHold();
			
		}

		
	}
	
	Item itemFocus = null;
	public void setPaintInfo() {
//		Item item = Char.myChar().arrItemBody[idSelect];
		itemFocus = Char.myChar().arrItemBody[idSelect];
//		name = item.template.name;

	}
	
}
