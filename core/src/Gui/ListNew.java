package Gui;

import com.thdgaming.naruto.GameCanvas;

import lib.mVector;
import model.Command;
import model.Res;



public class ListNew {

	public int maxW, itemH, maxH, maxSize, x, y, value;
	public int cmtoX, cmx, cmdy, cmvy, cmxLim;
	private int pointerDownTime;
	private int pointerDownFirstX;
	private int[] pointerDownLastX = new int[3];
	private boolean pointerIsDowning, isDownWhenRunning;//, wantUpdateList;
	private int cmRun;
	mVector vecCmd;
	Command cmdCenter = null;

	public ListNew(int x, int y, int maxW, int maxH, int itemH, int maxSize,
			int limX) {
		this.x = x;
		this.y = y;
		this.maxW = maxW;
		this.maxH = maxH;
		this.itemH = itemH;
		this.maxSize = maxSize;
		this.cmxLim = limX;
	}

	public int w;
	int pa = 0;
	boolean trans = false;

	public void updateMenuKey() {
		boolean changeFocus = false;
		if (GameCanvas.keyPressed[2]) {
			changeFocus = true;
			value--;
			if (value < 0)
				value = maxSize - 1;
			GameCanvas.clearKeyPressed();
			GameCanvas.clearKeyPressed();
		} else if (GameCanvas.keyPressed[8]) {
			changeFocus = true;
			value++;
			if (value > maxSize - 1)
				value = maxSize - 1;
			GameCanvas.clearKeyPressed();
			GameCanvas.clearKeyPressed();
		}
		if (changeFocus) {
			cmtoX = (value + 1) * itemH - maxH / 2;
			if (cmtoX > cmxLim)
				cmtoX = cmxLim;
			if (cmtoX < 0)
				cmtoX = 0;
			if (value == maxSize - 1 || value == 0)
				cmx = cmtoX;
		}
		update_Pos_UP_DOWN();
	}

	public void update_Pos_UP_DOWN() {
		if (GameCanvas.isPointerDown) {
			if (!pointerIsDowning && GameCanvas.isPointer(x, y, maxW, maxH)) {
				for (int i = 0; i < pointerDownLastX.length; i++)
					pointerDownLastX[0] = GameCanvas.py;
				pointerDownFirstX = GameCanvas.py;
				pointerIsDowning = true;
				isDownWhenRunning = cmRun != 0;
				cmRun = 0;
			} else if (pointerIsDowning) {
				pointerDownTime++;
				if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.py
						&& !isDownWhenRunning) {
					pointerDownFirstX = -1000;
				}
				int dx = GameCanvas.py - pointerDownLastX[0];
				if (dx != 0 && value != -1) {
					value = -1;
				}
				for (int i = pointerDownLastX.length - 1; i > 0; i--)
					pointerDownLastX[i] = pointerDownLastX[i - 1];
				pointerDownLastX[0] = GameCanvas.py;

				cmtoX -= dx;
				if (cmtoX < 0)
					cmtoX = 0;
				if (cmtoX > cmxLim)
					cmtoX = cmxLim;
				if (cmx < 0 || cmx > cmxLim)
					dx /= 2;
				cmx -= dx;
			}

		}
		if (GameCanvas.isPointerClick && pointerIsDowning) {
			int dx = GameCanvas.py - pointerDownLastX[0];
			GameCanvas.isPointerClick = false;
			if (Res.abs(dx) < 20
					&& Res.abs(GameCanvas.py - pointerDownFirstX) < 20
					&& !isDownWhenRunning&&GameCanvas.isPointerClick) {
				cmRun = 0;
				cmtoX = cmx;
				pointerDownFirstX = -1000;
				pointerDownTime = 0;

			} else if (value != -1 && pointerDownTime > 5) {
				pointerDownTime = 0;

			} else if (value == -1 && !isDownWhenRunning) {
				if (cmx < 0)
					cmtoX = 0;
				else if (cmx > cmxLim)
					cmtoX = cmxLim;
				else {
					int s = ((GameCanvas.py - pointerDownLastX[0])
							+ (pointerDownLastX[0] - pointerDownLastX[1]) + (pointerDownLastX[1] - pointerDownLastX[2]));
					if (s > 10)
						s = 10;
					else if (s < -10)
						s = -10;
					else
						s = 0;
					cmRun = -s * 100;
				}
			}
			pointerIsDowning = false;
			pointerDownTime = 0;
			GameCanvas.isPointerClick = false;
		}
	}

	public void updatePos_LEFT_RIGHT() {
		if (GameCanvas.isPointerDown) {
			if (!pointerIsDowning && GameCanvas.isPointer(x, y, maxW, maxH)) {
				for (int i = 0; i < pointerDownLastX.length; i++)
					pointerDownLastX[0] = GameCanvas.px;
				pointerDownFirstX = GameCanvas.px;
				pointerIsDowning = true;
				isDownWhenRunning = cmRun != 0;
				cmRun = 0;
			} else if (pointerIsDowning) {
				pointerDownTime++;
				if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.px
						&& !isDownWhenRunning) {
					pointerDownFirstX = -1000;
				}
				int dx = GameCanvas.px - pointerDownLastX[0];
				if (dx != 0 && value != -1) {
					value = -1;
				}
				for (int i = pointerDownLastX.length - 1; i > 0; i--)
					pointerDownLastX[i] = pointerDownLastX[i - 1];
				pointerDownLastX[0] = GameCanvas.px;

				cmtoX -= dx;
				if (cmtoX < 0)
					cmtoX = 0;
				if (cmtoX > cmxLim)
					cmtoX = cmxLim;
				if (cmx < 0 || cmx > cmxLim)
					dx /= 2;
				cmx -= dx;
			}

		}
		if (GameCanvas.isPointerClick && pointerIsDowning) {
			int dx = GameCanvas.px - pointerDownLastX[0];
			GameCanvas.isPointerClick = false;
			if (Res.abs(dx) < 20
					&& Res.abs(GameCanvas.px - pointerDownFirstX) < 20
					&& !isDownWhenRunning&&GameCanvas.isPointerClick) {
				cmRun = 0;
				cmtoX = cmx;
				pointerDownFirstX = -1000;
				pointerDownTime = 0;
			} else if (value != -1 && pointerDownTime > 5) {
				pointerDownTime = 0;
			} else if (value == -1 && !isDownWhenRunning) {
				if (cmx < 0)
					cmtoX = 0;
				else if (cmx > cmxLim)
					cmtoX = cmxLim;
				else {
					int s = ((GameCanvas.px - pointerDownLastX[0])
							+ (pointerDownLastX[0] - pointerDownLastX[1]) + (pointerDownLastX[1] - pointerDownLastX[2]));
					if (s > 10)
						s = 10;
					else if (s < -10)
						s = -10;
					else
						s = 0;
					cmRun = -s * 100;
				}
			}
			pointerIsDowning = false;
			pointerDownTime = 0;
			GameCanvas.isPointerClick = false;
		}
	}

	int cmvx, cmdx;

	public void moveCamera() {
		if (cmRun != 0 && !pointerIsDowning) {
			cmtoX += cmRun / 100;
			if (cmtoX < 0)
				cmtoX = 0;
			else if (cmtoX > cmxLim)
				cmtoX = cmxLim;
			else
				cmx = cmtoX;
			cmRun = cmRun * 9 / 10;
			if (cmRun < 100 && cmRun > -100)
				cmRun = 0;

		}
		if (cmx != cmtoX && !pointerIsDowning) {
			cmvx = (cmtoX - cmx) << 2;
			cmdx += cmvx;
			cmx += cmdx >> 4;
			cmdx = cmdx & 0xf;
		}
	}

	public void updateMenu() {
		moveCamera();
		updateMenuKey();
	}
}
