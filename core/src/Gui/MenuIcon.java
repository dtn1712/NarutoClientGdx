package Gui;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import real.Service;
import screen.old.GameScr;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import lib.Cout;
import lib.mVector;
import model.Command;
import model.IActionListener;
import model.Image;
import model.Paint;
import model.Screen;
import model.mResources;

public class MenuIcon extends Screen implements IActionListener{
	
	public static boolean isCloseSub=false;
	int x,y;
	public int indexpICon;
	public Command iconChar,iconMission,iconShop,iconContact,iconImprove,iconTrade,iconLogout;//main
	public static mVector lastTab = new mVector();
	int width=430;

	GuiFriend friend;
	ShopMain shop;//shop
	GuiQuest quest;//quest
	public TradeGui trade;//trade
	QuestMain questMain;
	public GuiContact contact;
	TabParty party;
	
	public boolean paintButtonClose;
	
	MenuIcon subMenu;
	
	public MenuIcon(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x=x;
		this.y=y;
		
		InitComand();
		
	}
	
	public void InitComand()
	{
		int x=0;
		//trade
		//Logout

		int indexcong = 20;
		x=this.x+width/2 -156+indexcong;

		iconLogout = new Command("", this, Contans.ICON_LOGOUT, null, 0, 0);
		iconLogout.setPos(x,y, loadImageInterface.imgLogout, loadImageInterface.imgLogout);

		x=this.x+width/2 -125+indexcong;

		iconTrade= new Command("", this, Contans.ICON_TRADE, null, 0, 0);
		iconTrade.setPos(x,y+1, loadImageInterface.imgTradeIcon, loadImageInterface.imgTradeIcon);
		
		//improve
		x=this.x+width/2 -84+indexcong;
		iconImprove= new Command("", this, Contans.ICON_MISSION, null, 0, 0);
		iconImprove.setPos(x,y-10, loadImageInterface.imgImproveIcon, loadImageInterface.imgImproveIcon);
		
		//quest
		x=this.x+width/2 -47+indexcong;
		iconMission= new Command("", this, Contans.ICON_MISSION, null, 0, 0);
		iconMission.setPos(x,y+4, loadImageInterface.imgMissionIcon, loadImageInterface.imgMissionIcon);
		
		//char
		x=this.x+width/2-10+indexcong;
		iconChar= new Command("", this, Contans.ICON_CHAR, null, 0, 0);
		iconChar.setPos(x,y, loadImageInterface.imgCharIcon, loadImageInterface.imgCharIcon);
		
		//shop
		x=this.x+width/2+27+indexcong;
		iconShop= new Command("", this, Contans.ICON_SHOP, null, 0, 0);
		iconShop.setPos(x,y+2, loadImageInterface.imgShopIcon, loadImageInterface.imgShopIcon);
		
		//contact
		x=this.x+width/2+64+indexcong;
		iconContact= new Command("", this, Contans.ICON_CONTACT, null, 0, 0);
		iconContact.setPos(x,y+2, loadImageInterface.imgContactIcon, loadImageInterface.imgContactIcon);

		
		//close gui
		cmdClose = new Command(" ",this, Contans.CMD_TAB_CLOSE,null,0,0);
		cmdClose.setPos(GameCanvas.hw+180, y, loadImageInterface.closeTab, loadImageInterface.closeTab);
	}

	public void updatePointer()
	{
		if(shop!=null)//shop
		{
			shop.updatePointer();
		}
		
//		if(quest!=null)//quest
//		{
//			quest.updatePointer();
//		}
//		
//		if(trade!=null)//trade
//		{
//			trade.updatePointer();
//		}
//		
//		if(subMenu!=null)//sub menu
//		{
//			subMenu.updatePointer();
//		}
	}
	public void update(){
		switch (indexpICon) {
		case Contans.ICON_CONTACT:
			if(contact!=null)
				contact.Update();
			break;
		case Contans.ICON_SHOP:
			if(shop!=null)//shop
			{
				shop.update();
			}
			break;
		case Contans.ICON_TEAM:
			if(party!=null)//shop
			{
				party.update();
			}
			break;
		case Contans.ICON_SUB_FRIEND:
			if(friend!=null)
				friend.update();
			break;
		case Contans.ICON_GIAOTIEP:
			if(contact!=null)
				contact.Update();
			break;
		}
	}
	public void updateKey()
	{

		//click close
				if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose) && paintButtonClose) {
					if (iconShop != null) {
						GameCanvas.isPointerJustRelease = false;
						GameCanvas.keyPressed[5] = false;
						Screen.keyTouch = -1;
						if (cmdClose != null)
						{
							cmdClose.performAction();	
						}
					}
				}

		switch (indexpICon) { //chi dc update 1 trong các man hinh
		case Contans.ICON_SHOP:
			if(shop!=null)//shop
			{
				shop.updateKey();
				return;
			}
			break;

		case Contans.ICON_TRADE:
			if(trade!=null)//trade
			{
				trade.updateKey();
				return;
//				trade.update();
			}
			break;
		case Contans.ICON_CHAR:
			
			break;
		case Contans.ICON_MISSION:
			if(quest!=null)//shop
			{
				quest.updateKey();
				return;
			}
			break;
		case Contans.ICON_CONTACT:
			if(contact!=null)//
			{
				contact.UpdateKey();
				return;
			}
			break;

		case Contans.ICON_GIAOTIEP:
			if(contact!=null)
				contact.UpdateKey();
			break;
		case Contans.ICON_TEAM:
				if(party!=null){
					party.updateKey();
					return;
				}
				
			break;
		case Contans.ICON_SUB_FRIEND:
			if(friend!=null)
				friend.updateKey();
			break;
		case 0:
			//click quest
//			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconMission)) {
//				if (iconMission != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (iconMission != null)
//					{
//						iconMission.performAction();	
//					}
//				}
//			}
			
			//click char
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconChar)) {
				if (iconChar != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconChar != null)
					{
						iconChar.performAction();	
					}
				}
			}
			
			//click shop
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconShop)) {
				if (iconShop != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconShop != null)
					{
						iconShop.performAction();	
					}
				}
			}
			//logout
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconLogout)) {
				if (iconLogout != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconLogout != null)
					{
						iconLogout.performAction();	
					}
				}
			}
			
			//click trade
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconTrade)) {
				if (iconTrade != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconTrade != null)
					{
						iconTrade.performAction();	
					}
				}
			}
			
			//click contact
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconContact)) {
				if (iconTrade != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconContact != null)
					{
						iconContact.performAction();	
					}
				}
			}
			
			//click quest
					if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconMission)) {
						if (iconMission != null) {
							GameCanvas.isPointerJustRelease = false;
							GameCanvas.keyPressed[5] = false;
							Screen.keyTouch = -1;
							if (iconMission != null)
							{
								iconMission.performAction();	
							}
						}
					}
					
			break;
		}
		
		
		if(subMenu!=null)//sub contact
		{
			subMenu.updateKey();
		}
		if(!GameCanvas.isPointerDown &&isCloseSub)
		{
			isCloseSub=false;
			subMenu=null;
		}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub

		switch (idAction) {
		case Contans.ICON_CHAR:
			GameScr.gI().guiMain.moveClose=false;
			indexpICon = Contans.ICON_CHAR;
			GameScr.isBag=true;
			Service.gI().requestinventory();
			MenuIcon.lastTab.add(""+Contans.ICON_CHAR);
			break;

		case Contans.ICON_MISSION:
			GameScr.gI().guiMain.moveClose=false;
			indexpICon = Contans.ICON_MISSION;
			if(quest==null)
				quest= new GuiQuest(GameCanvas.wd6-20, 20);
			MenuIcon.lastTab.add(""+Contans.ICON_MISSION);
			paintButtonClose=true;
			quest.SetPosClose(cmdClose);
			break;
		case Contans.ICON_TEAM:

			indexpICon = Contans.ICON_TEAM;
			if(party==null)
				party=new TabParty(GameCanvas.hw, 20);

			MenuIcon.lastTab.add(""+Contans.ICON_TEAM);
			paintButtonClose=true;
			party.SetPosClose(cmdClose);
			
			break;
		case Contans.ICON_SHOP:
			GameScr.gI().guiMain.moveClose=false;
			indexpICon = Contans.ICON_SHOP;
			if(shop==null)
				shop=new ShopMain(GameCanvas.wd6-20, 20);
			shop.indexidmenu = 0;
			paintButtonClose=true;
			shop.SetPosClose(cmdClose);

			MenuIcon.lastTab.add(""+Contans.ICON_SHOP);
			break;
			
		case Contans.ICON_TRADE:
			if(Char.myChar().partnerTrade!=null){
				GameScr.gI().guiMain.moveClose=false;
				indexpICon = Contans.ICON_TRADE;
				trade=new TradeGui(GameCanvas.wd6-20, 20);
				trade.SetPosClose(cmdClose);
				paintButtonClose=true;
				MenuIcon.lastTab.add(""+Contans.ICON_TRADE);
			}
			break;
			
		case Contans.ICON_CONTACT:

			indexpICon = Contans.ICON_CONTACT;
			GameScr.gI().guiMain.moveClose=false;
			paintButtonClose=true;
//			giaotiep = new GuiGiaoTiep(GameCanvas.hw, 20);
//			giaotiep.SetPosClose(cmdClose);

//			contact = new GuiContact(GameCanvas.hw, 20);
//			contact.SetPosClose(cmdClose);
//			contact
			if(subMenu==null)
				subMenu=new SubMenuContact(iconContact.x-SubMenuContact.width2,iconContact.y-50);
			subMenu.SetPosClose(cmdClose);
			MenuIcon.lastTab.add(""+Contans.ICON_CONTACT);
			break;
			
		case Contans.CMD_TAB_CLOSE:
			indexpICon = 0;

			paintButtonClose=false;
			if(lastTab.size()>0){
				String indextabx = (String)lastTab.elementAt(lastTab.size()-1);
				
				try {
					int indextab =Integer.parseInt(indextabx);
					if(lastTab.size()>1){
						indexpICon = Integer.parseInt((String)lastTab.elementAt(lastTab.size()-2));
						setPostCloseTab(indexpICon);
					}
					lastTab.removeElementAt(lastTab.size()-1);
					switch (indextab) {
					case Contans.ICON_GIAOTIEP:
						if(contact!=null)
							contact=null;
						break;
					case Contans.ICON_CONTACT:
						if(subMenu!=null)
							subMenu=null;
						break;
					case Contans.ICON_TRADE:
						if(trade!=null) {
							Service.gI().cancelTrade(Contans.CANCEL_TRADE, (short) Char.myChar().partnerTrade.charID);
							trade = null;
						}
						break;
					case Contans.ICON_SHOP:
						if(shop!=null)
							shop=null;
						break;
					case Contans.ICON_TEAM:
						if(party!=null)
							party = null;
						break;
					case Contans.ICON_MISSION:
						if(quest!=null)
							quest=null;
						break;
					case Contans.ICON_SUB_FRIEND:
						if(friend!=null)
							friend = null;
						break;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			if(GameScr.gI().guiChatClanWorld.moveClose)//TRUONG HOP DANG MO CHAT WORLD
			{
				GameScr.gI().guiMain.menuIcon.indexpICon = 0;//truong hop dong chat ko upda menuicon
				
				GameScr.gI().guiChatClanWorld.moveClose=false;
				GameScr.gI().guiChatClanWorld.bntOpen.setPos(GameScr.gI().guiChatClanWorld.x+GameScr.gI().guiChatClanWorld.xBtnMove-Image.getWidth(loadImageInterface.imgShortQuest) + 35, GameScr.gI().guiChatClanWorld.y+130/2-Image.getHeight(loadImageInterface.imgShortQuest)/2 +25, loadImageInterface.imgShortQuest, loadImageInterface.imgShortQuest);
			}
			break;
		case Contans.ICON_LOGOUT:
			GameCanvas.startYesNoDlg("Bạn có muốn thoát game?", new Command(mResources.YES, GameCanvas.instance, GameCanvas.cLogout, null), new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
			break;
		}
	}
	public void setPostCloseTab(int indexTab){
		switch (indexTab) {
		case Contans.ICON_CHAR:
			break;

		case Contans.ICON_MISSION:
			GameScr.gI().guiMain.moveClose=false;
			if(quest!=null)
				quest.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
		case Contans.ICON_TEAM:
			if(party!=null)
				party.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
		case Contans.ICON_SHOP:
			GameScr.gI().guiMain.moveClose=false;
			if(shop!=null)
				shop.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
			
		case Contans.ICON_TRADE:
			GameScr.gI().guiMain.moveClose=false;
			indexpICon = Contans.ICON_TRADE;
			if(trade!=null)
				trade.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
			
		case Contans.ICON_CONTACT:
			GameScr.gI().guiMain.moveClose=false;
			paintButtonClose=true;
			if(contact!=null)
				contact.SetPosClose(cmdClose);
			break;
		}
	}

	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		Cout.println(getClass(), "menuicon setposclose");
		cmd.setPos(x+width-Image.getWidth(loadImageInterface.closeTab), y, loadImageInterface.closeTab, loadImageInterface.closeTab);
	}
	
	public void paint(MGraphics g)
	{
		Paint.PaintBGMenuIcon(x,y,10,g);
		
		iconTrade.y=y+1;
		iconImprove.y=y-10;
		iconMission.y=y+4;
		iconChar.y=y;
		iconShop.y=y+2;
		iconContact.y=y+2;
		iconLogout.y=y+1;
		iconLogout.paint(g);
		iconTrade.paint(g);
		iconMission.paint(g);
		iconChar.paint(g);
		iconShop.paint(g);
		iconImprove.paint(g);
		iconContact.paint(g);
		
		if(shop!=null)//shop
			shop.paint(g);
		if(party!=null)
			party.paint(g);
		if(quest!=null)//quest
			quest.paint(g);
		
		if(trade!=null)//trade
			trade.paint(g);

		if(contact!=null)
			contact.Paint(g);
		if(subMenu!=null)
		{
			subMenu.paint(g);
		}
		if(friend!=null)
			friend.paint(g);
		if(paintButtonClose){
			cmdClose.paint(g);
		}
		
		GameScr.resetTranslate(g);
		
		
		
	}

}
