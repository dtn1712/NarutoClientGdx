package Gui;


import real.mFont;

import com.thdgaming.naruto.GameCanvas;
import com.thdgaming.naruto.MGraphics;

import lib.Bitmap;
import lib.TField;
import model.Image;

/*
 * This class: paint info char
 */
public class TabInfoChar extends MainTabNew {
	
	private Bitmap[] arrayHuman=new Bitmap[25];
	public int xTabInfo = 0;
	public TabInfoChar(String name) {
		typeTab = MY_INFO;
		this.nameTab = name;
		tfPoint = new TField();
		xTabInfo = xTab-22;
		for (int i = 0; i < xyTField.length; i++) {
			xyTField[i][0] = xTabInfo-10+(i>3?200:60);//xTab-10,yTab-30+ GameCanvas.h / 5
			xyTField[i][1] = yTab+ 37*(i%4+1)+20;
		}
		xySo[0][0] = 113; xySo[0][1] = 30;
		xySo[1][0] = 94; xySo[1][1] = 35;
		xySo[2][0] = 95; xySo[2][1] = 66;
		xySo[3][0] = 95; xySo[3][1] = 79;
		xySo[4][0] = 95; xySo[4][1] = 90;
		xySo[5][0] = 95; xySo[5][1] = 100;
		xySo[6][0] = 106; xySo[6][1] = 112;
		xySo[7][0] = 115; xySo[7][1] = 80;
		tfPoint.x = 0;
		tfPoint.y = GameCanvas.h-25;
		tfPoint.isFocus = false;

		tfPoint.width = 120;
		tfPoint.height = ITEM_HEIGHT + 2;
		tfPoint.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
//		tfPoint.doChangeToTextBox();
		tfPoint.setMaxTextLenght(40);
	}
	public int indexFocus = -1;
	public int[][] xyTField = new int[8][2];
	public int[][] xySo = new int[8][2];
	public static int[] point = new int[8];
	public int[] maptoSo = new int[]{2,3,4,5,0,1,7,6};

	public void LoadImage()
	{
		for(int i=0;i<25;i++)
		{
			arrayHuman[i]=GameCanvas.loadImage("/GuiNaruto/human/human_"+(i+1)+".png");
		}
		
	}
	public void init() {

	}
	
	private void PaintHuman(int x,int y,MGraphics g)
	{
		
		int wImage=Image.getWidth(arrayHuman[0]);
		int hImage=Image.getHeight(arrayHuman[0]);
		int index=0;
		for(int i=1;i<6;i++)
			for(int j=1;j<6;j++)
			{
				g.drawImage(arrayHuman[index],x+j*wImage,y+i*hImage,0);
				index++;
				
			}
	}
	
	private void drawline(MGraphics g){
		g.setColor(0xFFFFFF); /// 1
		g.drawLine(xTabInfo + 60, yTab+35+ GameCanvas.h / 5, xTabInfo+120, yTab+35+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); //// 2
		g.drawLine(xTabInfo + 140, yTab+35+ GameCanvas.h / 5, xTabInfo+200, yTab+35+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 3
		g.drawLine(xTabInfo + 40, yTab+70+ GameCanvas.h / 5, xTabInfo+130, yTab+70+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 4
		g.drawLine(xTabInfo + 40, yTab+82+ GameCanvas.h / 5, xTabInfo+130, yTab+82+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 5
		g.drawLine(xTabInfo + 120, yTab+93+ GameCanvas.h / 5, xTabInfo+200, yTab+93+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 6
		g.drawLine(xTabInfo + 40, yTab+100+ GameCanvas.h / 5, xTabInfo+130, yTab+100+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 7
		g.drawLine(xTabInfo + 120, yTab+115+ GameCanvas.h / 5, xTabInfo+200, yTab+115+ GameCanvas.h / 5);
		g.setColor(0xFFFFFF); /// 8
		g.drawLine(xTabInfo + 140, yTab+78+ GameCanvas.h / 5, xTabInfo+200, yTab+78+ GameCanvas.h / 5);
	}
	
	private void pointGate(MGraphics g){
		g.setColor(0XFFFFFF); // 1
		g.fillRect(xTabInfo + 50, yTab+30+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo + 50, yTab+30+ GameCanvas.h / 5, MGraphics.HCENTER| MGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 2
		g.fillRect(xTabInfo+200, yTab+25+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo+205, yTab+30+ GameCanvas.h / 5, MGraphics.HCENTER| MGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 3
		g.fillRect(xTabInfo + 40,  yTab+70+ GameCanvas.h / 5, 10, 10);
		mFont.tahoma_7_green.drawString(g, "10", xTabInfo + 40,  yTab+70+ GameCanvas.h / 5, MGraphics.HCENTER| MGraphics.BOTTOM);
		g.setColor(0XFFFFFF); // 4
		g.fillRect(xTabInfo + 40, yTab+82+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 5
		g.fillRect(xTabInfo+200, yTab+93+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 6
		g.fillRect(xTabInfo + 40,  yTab+100+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 7
		g.fillRect(xTabInfo+200, yTab+115+ GameCanvas.h / 5, 10, 10);
		g.setColor(0XFFFFFF); // 8
		g.fillRect(xTabInfo+200, yTab+78+ GameCanvas.h / 5, 10, 10);
		
	}
	public static TField tfPoint;
	private void updatePoiter(){
		for (int i = 0; i < xyTField.length; i++) {
			if(GameCanvas.isPointSelect(xyTField[i][0],xyTField[i][1],20,20)){
				indexFocus = i;
			tfPoint.setTextBox();
			GameCanvas.isPointerClick = false;
			}
		}
		
	}
	
	public void updateKey(){
		updatePoiter();
		tfPoint.update();
	}
	public void keyPress(int keyCode) {
		if (tfPoint.isFocus)
			tfPoint.keyPressed(keyCode);
		super.keyPress(keyCode);
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(arrayHuman[0]==null){
			LoadImage();
		}
		if(tfPoint.isFocus){
			tfPoint.update();
		}
		if(tfPoint.isOKReturn){
			tfPoint.isOKReturn = false;
			int diem = 0;
			try {
				diem = Integer.parseInt(tfPoint.getText());
				tfPoint.setText("");
			} catch (Exception e) {
				// TODO: handle exception
			}
			point[indexFocus] +=diem;
			indexFocus = 0;
		}
	}
	
	public void paint(MGraphics g) {
		PaintHuman(xTabInfo-10,yTab,g);
		//drawline(graphic);
		paintListTFied(g);
		//pointGate(graphic);
		if(tfPoint.isFocus)
		tfPoint.paint(g);
		
	}
	public void paintListTFied(MGraphics g)
	{
		
		for (int i = 0; i < xyTField.length; i++) {
			g.setColor(0xff000000);
			g.fillRect(xyTField[i][0], xyTField[i][1], 20, 20);
			//ve duong line to o so
			g.setColor(0xfffffff);
			g.drawLine(xyTField[i][0]-5, xyTField[i][1]+21,xyTField[i][0]+25, xyTField[i][1]+21);
			if(i<4)
				g.drawLine(xyTField[i][0]+25, xyTField[i][1]+21,xySo[maptoSo[i]][0]+xTabInfo+30, xySo[maptoSo[i]][1]+yTab+7+30);
			else 
				g.drawLine(xySo[maptoSo[i]][0]+xTabInfo+30, xySo[maptoSo[i]][1]+yTab+7+30,xyTField[i][0]-5, xyTField[i][1]+21);
		}
		for (int i = 0; i < point.length; i++) {
			mFont.tahoma_7_green.drawString(g, point[i]+"",xyTField[i][0]+10, xyTField[i][1]+5 ,2);
		}
	}
}
