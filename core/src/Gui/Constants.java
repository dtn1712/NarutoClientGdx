package Gui;

import Objectgame.Char;
import screen.GameScreen;

public class Constants {

	public static final String WEBSITE_URL = "http://huyenthoaicuoicung.com/";

	public static final long CONNECT_TIMEOUT_SECONDS = 20;

	public static final byte CAT_PLAYER = 0; //32000
	public static final byte CAT_MONSTER = 1; //32000

	//friend gui
	public static final byte CONFIRM_UNFRIEND = 11;
	public static final byte CHAT_PRIVATE = 12;
	public static final byte UNFRIEND = 13;
	public static final byte INVITE_MINI_GAME = 15;

	//chat friend gui
	public static final byte BUTTON_SEND = 13;

	public static final byte BUTTON_ICON_CHAT = 14;

	//chat world gui
	public static final int BUTTON_SEND_CHAT_WORLD = 21;

	public static final int BUTTON_ICON_CHAT_WORLD = 15;

	//quest
	public static final int BUTTON_OPEN = 22;

	//char info
	public static final int CHAR_BROAD = 27;

	//tab
	public static final int CMD_TAB_CLOSE = 28;

	//trade:type trade
	public static final byte INVITE_TRADE = 0;
	public static final byte ACCEPT_INVITE_TRADE = 1;
	public static final byte MOVE_ITEM_TRADE = 2;
	public static final byte TRADE = 3;
	public static final byte CANCEL_TRADE = 4;
	public static final byte END_TRADE = 5;
	public static final byte LOCK_TRADE = 6; //khoa giao dich
	public static final byte MOVE_MONEY = 7; //xu giao dich

	public static final byte MOVE_ITEM_TO_TRADE = 0;
	public static final byte MOVE_ITEM_TO_INVENTORY = 1;

	// button
	public static final int BTN_SELECT = 29;
	public static final int BTN_lEFT_ARROW = 30;
	public static final int BTN_RIGHT_ARROW = 31;
	public static final int BTN_TRADE = 32;
	public static final int BTN_XU = 47;
	public static final int BTN_OK_XU = 48;
	public static final int BTN_USE_ITEM = 33;
	public static final int BTN_DROP_ITEM = 34;
	public static final int BTN_SALE_ITEM = 35;

	//menu icon
	public static final int ICON_CHAR = 35;

	public static final int ICON_MISSION = 36;
	public static final int ICON_SHOP = 37;
	public static final int ICON_TRADE = 38;
	public static final int ICON_CONTACT = 39;
	public static final int ICON_TEAM = 42;
	public static final int ICON_GIAOTIEP = 43;
	public static final int ICON_LOGOUT = 44;
	public static final int ICON_IMPROVE = 45;
	public static final int ICON_DOSAT = 46;
	public static final int ICON_CLAN = 47;
	public static final int ICON_SETTING = 48;
	public static final int ICON_MINIGAME = 49;


	//sub icon friend
	public static final int ICON_SUB_FRIEND = 40;
	public static final int ICON_SUB_TEAM = 41;


	public static final byte TYPE_PLAYER = 0;
	public static final byte TYPE_MONSTER = 1;
	public static final byte TYPE_ITEM = 3;
	public static final byte TYPE_BOMB = 4;

	public static final int MAX_READ_ERROR_MESSAGE_RETRY = 10;

	public static final byte INVITED_PK = 0;
	public static final byte ACCEPT_PK = 1;
	public static final byte REJECT_PK = 2;
	public static final int DEFAULT_COIN_PK_BET = 50;
	public static final byte PK_GOLD_BET_TYPE = 1;
	public static final byte PK_COIN_BET_TYPE = 0;
	public static final byte PK_NO_BET_TYPE = -1;
	public static final int MAX_GOLD_PK_BET_ALLOW = 10;
	public static final int MAX_COIN_PK_BET_ALLOW = 1000;

	public static final byte START_QUEST = 0;
	public static final byte COMPLETE_QUEST = 1;
	public static final byte CANCEL_QUEST = 2;

	public static final long PICK_UP_ITEM_WAIT_TIME = 2 * 1000;


//	public static boolean isPaint() {
//		if (Char.myChar().cx < GameScreen.cmx)
//			return false;
//		if (Char.myChar().cx > GameScreen.cmx + GameScreen.gW)
//			return false;
//		if (Char.myChar().cy < GameScreen.cmy)
//			return false;
//		if (Char.myChar().cy > GameScreen.cmy + GameScreen.gH + 30)
//			return false;
//		return true;
//	}

}
