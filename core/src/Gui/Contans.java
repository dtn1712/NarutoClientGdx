package Gui;

public class Contans {
 
	//friend gui
	public static final byte UN_FRIEND=11;
	public static final byte CHAT_PRIVATE=12;
	
	//chat friend gui
	public static final byte BUTTON_SEND=13;
	public static final byte BUTTON_ICON_CHAT=14;
	
	//chat world gui
	public static final int BUTTON_SEND_CHAT_WORLD=21;
	public static final int BUTTON_ICON_CHAT_WORLD=15;
	
	//menu
	public static final int MENU_LIST_FRIEND=16;
	public static final int MENU_LIST_PARTY=17;
	public static final int MENU_CHAT_PRIVATE=18;
	public static final int MENU_CHAT_WORLD=19;
	public static final int MENU_BAG=20;
	public static final int MENU_QUEST=23;
	
	//quest
	public static final int BUTTON_OPEN=22;
	
	//tab bag
	public static final int BUTTON_TAB_BAG=24;
	public static final int BUTTON_TAB_INFO_CHAR=25;
	public static final int BUTTON_TAB_MYSEFT_NEW=26;
	
	//char info
	public static final int CHAR_BROAD=27;
	
	//tab
	public static final int CMD_TAB_CLOSE = 28;
	
	//trade:type trade
	public static final byte INVITE_TRADE = 0;
	public static final byte ACCEPT_INVITE_TRADE = 1;
	public static final byte MOVE_ITEM_TRADE = 2;
	public static final byte TRADE = 3;
	public static final byte CANCEL_TRADE = 4;
	public static final byte END_TRADE = 5;
	public static final byte LOCK_TRADE = 6;//khoa giao dich
	public static final byte MOVE_MONEY = 7;//xu giao dich
	
	//command trade
	public static final int BTN_SELECT= 29;
	public static final int BTN_lEFT_ARROW= 30;
	public static final int BTN_RIGHT_ARROW = 31;
	public static final int BTN_TRADE = 32;
	
	//
	public static final int BTN_USE_ITEM = 33;
	public static final int BTN_DROP_ITEM = 34;
	
	//menu icon
	public static final int ICON_CHAR= 35;
	public static final int ICON_MISSION = 36;
	public static final int ICON_SHOP = 37;
	public static final int ICON_TRADE = 38;
	public static final int ICON_CONTACT = 39;
	public static final int ICON_TEAM = 42;
	public static final int ICON_GIAOTIEP = 43;
	public static final int ICON_LOGOUT = 44;
	
	
	//sub icon friend
	public static final int ICON_SUB_FRIEND = 40;
	public static final int ICON_SUB_TEAM = 41;
	//skill 
	public static final int SKILL0= 42;
	public static final int SKILL1= 43;
	public static final int SKILL2= 44;
	public static final int SKILL3= 45;
	public static final int SKILL4= 46;
}
