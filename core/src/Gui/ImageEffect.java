package Gui;

import java.util.Enumeration;
import java.util.Hashtable;



public class ImageEffect {
	public static Hashtable hashImageEff = new Hashtable();
	long timeRemove;
	int IdImage;
	mImage img;

	public ImageEffect(int Id) {
		this.IdImage = Id;
		img = mImage.createImage("/eff/graphic" + Id + ".png");
//		timeRemove = GameCanvas.timeNow;
	}

	public static mImage setImage(int Id) {
		ImageEffect img = (ImageEffect) hashImageEff.get("" + Id);
		if (img == null) {
			img = new ImageEffect(Id);
			hashImageEff.put("" + Id, img);
		} 
//		else
//			img.timeRemove = GameCanvas.timeNow;
		return img.img;
	}

	public static void SetRemove() {
		Enumeration k = hashImageEff.keys();
		while (k.hasMoreElements()) {
			String keyset = (String) k.nextElement();
			ImageEffect img = (ImageEffect) hashImageEff.get(keyset);
//			if ((GameCanvas.timeNow - img.timeRemove) / 1000 > 300) {
//				hashImageEff.remove(keyset);
//			}
		}
	}

	public static void SetRemoveAll() {
		hashImageEff.clear();
	}
}
