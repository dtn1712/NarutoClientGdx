package com.thdgaming.naruto;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.content.Asset;

import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import Gui.Constants;
import lib.MapKey;

public class GameMidlet implements ApplicationListener, InputProcessor {

	public static String IP = "209.97.168.91";

	public static int TCP_PORT = 19158;
	public static int UDP_PORT = 19159;

	public static final String VERSION = "0.1.3";

	public static GameCanvas gameCanvas;

	private MyInputProcessor inputProcessor;

	public static final float zoom = 1.0f;

	public static GameMidlet me;
	private MGraphics graphics;

	public OrthographicCamera camera;

	public static int getWidth() {
		return Gdx.graphics.getWidth();
	}

	public static int getHeight() {
		return Gdx.graphics.getHeight();
	}

	public static String mainThreadName;

	private void initInputProcessors() {
		InputMultiplexer inputMultiplexer = new InputMultiplexer();

		Gdx.input.setInputProcessor(inputMultiplexer);

		inputProcessor = new MyInputProcessor(camera, zoom);
		MyGestureHandler gestureHandler = new MyGestureHandler();

		inputMultiplexer.addProcessor(new GestureDetector(gestureHandler));
		inputMultiplexer.addProcessor(inputProcessor);
	}

	@Override
	public void create() {
		if (!Thread.currentThread().getName().equals("Main"))
			Thread.currentThread().setName("Main");
		mainThreadName = Thread.currentThread().getName();

		MapKey.load();
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);

		initInputProcessors();
		Gdx.input.setInputProcessor(inputProcessor);

		gameCanvas = new GameCanvas(this);
		graphics = new MGraphics(new SpriteBatch());
		com.team.njonline.GameMidlet.asset = new Asset();
		GameCanvas.w =Gdx.graphics.getWidth();
		GameCanvas.h =Gdx.graphics.getHeight();
		GameCanvas.instance = new GameCanvas();
		camera.zoom = zoom;
	}


	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		graphics.getGraphic().spriteBatch.setProjectionMatrix(camera.combined);
		camera.translate(-(Gdx.graphics.getWidth() - zoom*Gdx.graphics.getWidth())/2,
				-((Gdx.graphics.getHeight() - zoom*Gdx.graphics.getHeight()))/2);
		camera.update();
		update();
		graphics.begin();
		GameCanvas.instance.paint(graphics);
		graphics.end();
		camera.translate((Gdx.graphics.getWidth() - zoom*Gdx.graphics.getWidth())/2,
				((Gdx.graphics.getHeight() - zoom*Gdx.graphics.getHeight()))/2);
	}

	private void update() {
		GameCanvas.instance.update();
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean keyDown(int keycode) {
		gameCanvas.keyPressed(MapKey.map(keycode));
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		gameCanvas.keyReleased(MapKey.map(keycode));
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}


	private void setupServerIP()
	{
		String serverIpUrl = Constants.WEBSITE_URL + "server_ip";

		try {
			URL url = new URL(serverIpUrl);
			URLConnection connection = url.openConnection();

			ObjectMapper objectMapper = new ObjectMapper();
			List response = objectMapper.readValue(connection.getInputStream(), List.class);

			if (response != null && !response.isEmpty()) {
				Map serverIP = (Map) response.get(0);
				GameMidlet.IP = (String) serverIP.get("ip");
				GameMidlet.TCP_PORT = Integer.parseInt((String) serverIP.get("tcpPort"));
				GameMidlet.UDP_PORT = Integer.parseInt((String) serverIP.get("udpPort"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
