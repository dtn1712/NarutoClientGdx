package com.thdgaming.naruto;

import com.team.njonline.GameMidlet;

import java.util.Vector;

import lib.Bitmap;
import lib.Cout;
import lib.Session_ME;
import model.Command;
import model.Dialog;
import model.IActionListener;
import model.Image;
import model.InfoDlg;
import model.Input2Dlg;
import model.InputDlg;
import model.Menu;
import model.MsgDlg;
import model.Music;
import model.Paint;
import model.Position;
import model.Screen;
import model.SmallImage;
import model.StaticObj;
import model.Timer;
import model.mResources;
import real.Controller;
import real.Service;
import real.mFont;
import screen.old.CreatCharScr;
import screen.old.GameScr;
import screen.old.LanguageScr;
import screen.old.LoginScr;
import screen.old.SplashScr;
import Gui.TabScreenNew;
import GuiOut.loadImageInterface;
import Objectgame.Char;
import Objectgame.TileMap;

public class GameCanvas implements IActionListener {

	public static int transY = 15;
	public static GameCanvas instance;
	public static MGraphics g = new MGraphics();
	public static GameScr gameScr;
	
	
	// 0 1 2 3 4 5 6 7 8 9 * LSoft RSoft
	public static boolean[] keyPressed = new boolean[15];
	public static boolean[] keyReleased = new boolean[15];
	public static boolean[] keyHold = new boolean[15];
	public static boolean isTouch = false; // ---> MÃ n hÃ¬nh cáº£m á»©ng
	public static boolean isTouchControl; // ----> Ä�iá»�u khiá»ƒn báº±ng cáº£m á»©ng
	public static boolean isTouchControlSmallScreen; // ----> Ä�iá»�u khiá»ƒn báº±ng cáº£m á»©ng mÃ n hÃ¬nh nhá»�
	public static boolean isTouchControlLargeScreen; // ----> Ä�iá»�u khiá»ƒn báº±ng cáº£m á»©ng mÃ n hÃ¬nh lá»›n
	public static boolean isPointerDown;
	public static boolean isPointerClick;
	public static boolean isPointerJustRelease;
	
	public static int px, py, pxFirst, pyFirst, pxLast, pyLast;
	public static int gameTick;

	public static int zoomLevel = 1;
	public static int w, h, rw, rh, hw, hh, wd3, hd3, w2d3, h2d3, w3d4, h3d4, wd6, hd6; // rw: real widht
	public static Screen currentScreen;

	public static Menu menu = new Menu();
	public static LoginScr loginScr;
	public static LanguageScr languageScr;
	public static Dialog currentDialog;
	public static MsgDlg msgdlg = new MsgDlg();
	public static InputDlg inputDlg;
	public static Input2Dlg input2Dlg;
	public static Vector listPoint;
	public static Paint paint;
	public static Position[] arrPos = new Position[4];
	public static boolean lowGraphic = false;
	public static boolean isLoading;
	public static Bitmap imgBG[];
	public static int skyColor, curPos=0;
	public static int[] imgBGWidth;
	public static TabScreenNew AllInfo;
	
	public static int hText = 14;
	
	public static int wCommand, hCommand = 25;


	public GameCanvas() {

		instance = this;
		
		GameScr.d = (w > h ? w : h) + 20;
		
		
		// bringToFront();
		
		// init sound
//		Music.init(GameCanvas.gCanvas);
//		Music.setVolume(0, 0,100);
		
		initGameCanvas();
		SplashScr.loadSplashScr();		
		GameCanvas.currentScreen = new SplashScr();
		listPoint = new Vector();
		languageScr = new LanguageScr();
		AllInfo = new TabScreenNew();
		loadImages();
		
	}

	private void loadImages() {
		imgShuriken = GameCanvas.loadImage("/u/f.png");
		imgCheckPass = GameCanvas.loadImage("/u/check.png");
		
		for (int i = 0; i < 3; i++) {
			imgBorder[i] = GameCanvas.loadImage("/hd/bd" + i + ".png");
		}
		CreatCharScr.loadImage();
		borderConnerW = MGraphics.getImageWidth(imgBorder[0]);
		borderConnerH = MGraphics.getImageHeight(imgBorder[0]);
		borderLineW = MGraphics.getImageWidth(imgBorder[1]);
		borderLineH = MGraphics.getImageHeight(imgBorder[1]);
		borderCenterW = MGraphics.getImageWidth(imgBorder[2]);
		borderCenterH = MGraphics.getImageHeight(imgBorder[2]);
		
		SplashScr.imgLogo = Image.getResizedBitmap(Image.loadImageFromAsset("njOnline.jpg"));
	}

	public static GameCanvas getInstance() {
//		 if (instance == null) {
//		 instance = new GameCanvas();
//		 }
		return instance;
	}

	public void initPaint() {
		paint = new model.Paint();
	}

	public void initGameCanvas() {

		getScreenSize();
		initPaint();
		
		loadDust();
//		loadWaterSplash();
//		if (!GameMidlet.isEngVersion) {
//			int languageID = Rms.loadRMSInt("indLanguage");
//			if (languageID < 0)
				mResources.languageID = mResources.Lang_VI;
//			else{
//				mResources.languageID = languageID;
//			}
//		}else{
//			mResources.languageID = mResources.Lang_EN;
//		}
		mResources.loadLanguage();
		SmallImage.loadBigImage();

		  Session_ME.gI().setHandler(Controller.gI());
		
		System.gc();
		mFont.loadFont();
		Screen.ITEM_HEIGHT = mFont.tahoma_8b.getHeight() + 8;
		hw = w / 2;
		hh = h / 2;
		wd3 = w / 3;
		hd3 = h / 3;
		w2d3 = 2 * w / 3;
		h2d3 = 2 * h / 3;
		w3d4 = 3 * w / 4;
		h3d4 = 3 * h / 4;
		wd6 = w / 6;
		hd6 = h / 6;
		//Screen.initPos();
//		GameScr.loadDataRMS();
		GameScr.loadImages();
		MsgDlg.loadbegin();
		loadImageInterface.loadImage();
		loginScr = new LoginScr();
		inputDlg = new InputDlg();
		input2Dlg = new Input2Dlg();
		GameScr.loadImageBgItem();
	
//		loadImages();
		isTouch = true;
		if (GameCanvas.w >= 240)
			isTouchControl = true;
		if (GameCanvas.w < 320)
			isTouchControlSmallScreen = true;
		if (GameCanvas.w >= 320)
			isTouchControlLargeScreen = true;
		
	}

	public void paint(MGraphics g) {
		try {
	
			if (currentScreen != null && !isLoading) {
				currentScreen.paint(g);
				
			}
			Screen.resetTranslate(g);
			InfoDlg.paint(g); 
			if (currentDialog != null) {
				GameCanvas.debug("PC", 1);
				currentDialog.paint(g);
			} else if (menu.showMenu) {
				GameCanvas.debug("PD", 1);
				menu.paintMenu(g);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void endDlg() {
		GameCanvas.inputDlg.tfInput.setMaxTextLenght(500);
		GameCanvas.input2Dlg.tfInput.setMaxTextLenght(500);
		GameCanvas.input2Dlg.tfInput2.setMaxTextLenght(500);
		currentDialog = null;
	}

	public static void startOKDlg(String info) {
		msgdlg.setInfo(info, null, new Command(mResources.OK, GameCanvas.instance ,8882,null), null);
//		LoginScr.isAutoLogin = false;
//		SplashScr.imgLogo = null;
		currentDialog = msgdlg;
	}

	public static void startWaitDlg(String info) {
		msgdlg.setInfo(info, null, new Command(mResources.CANCEL, GameCanvas.instance,8882,null), null);
		currentDialog = msgdlg;
		msgdlg.isWait = true;
	}


	public static void startWaitDlg() {
		startWaitDlg(mResources.PLEASEWAIT);
	}

	

	public static void startOK(String info, int actionID, Object p) {
		// BB: move command from left to center
		msgdlg.setInfo(info, null, new Command(mResources.OK, instance, actionID, p), null);
		msgdlg.show();
	}
	
	public static void startWaitDlgWithoutCancel() {
		msgdlg.timeShow = 500;
		msgdlg.setInfo(mResources.PLEASEWAIT, null, null, null);
		currentDialog = msgdlg;
		msgdlg.isWait = true;
	}
	
	public static void startYesNoDlg(String info, int iYes,Object pYes,int iNo,Object pNo) {
		msgdlg.setInfo(info, new Command(mResources.YES, instance,iYes,pYes), new Command("", instance,iYes,pYes), new Command(mResources.NO,instance, iNo,pNo));
		msgdlg.show();
	}
	
	public static void startYesNoDlg(String info, Command cmdYes, Command cmdNo) {
		cmdYes.caption = mResources.YES;
		cmdNo.caption = mResources.NO;
		msgdlg.setInfo(info, cmdYes, null, cmdNo);
		msgdlg.show();
	}
	public static void startDlgTime(String info, Command cmdYes, int time) {
		cmdYes.caption = mResources.OK;
		msgdlg.setInfo(info, null,cmdYes,  null);
		msgdlg.setTimeLive(time);
		msgdlg.show();
	}

	public void update() {

		GameCanvas.debug("A", 0);
		gameTick=(gameTick+1)%1000;
		if (currentScreen != null) {
			if (currentDialog != null) {
				GameCanvas.debug("B", 0);
				currentDialog.update();
				
			} else if (menu.showMenu) {
				GameCanvas.debug("C", 0);
				menu.updateMenuKey();
				menu.updateMenu();
				GameCanvas.debug("D", 0);
			}
			GameCanvas.debug("E", 0);
			if(currentDialog==null)
			currentScreen.updateKey();
			if(!isLoading&&currentDialog==null)
				currentScreen.update();
			GameCanvas.debug("F", 0);
		}
		GameCanvas.clearKeyPressed();
		GameCanvas.debug("Ix", 0);
		Timer.update();
		GameCanvas.debug("Hx", 0);
		InfoDlg.update();

		GameCanvas.debug("G", 0);
		if (resetToLoginScr) {
			Music.stopAll();
			resetToLoginScr = false;
			doResetToLoginScr();
		}
	}

	public static int[] bgSpeed;
	public int[] dustX, dustY, dustState;

	public boolean startDust(int dir, int x, int y) {
		if (lowGraphic)
			return false;
		int i = dir == 1 ? 0 : 1;
		if (dustState!=null&&dustState[i] != -1)
			return false;
		if (dustState!=null)
		dustState[i] = 0;
		dustX[i] = x;
		dustY[i] = y;
		return true;
	}

	public static int[] wsX, wsY, wsState, wsF;
	public static Bitmap imgWS[], imgShuriken, imgCheckPass;
	public static Bitmap imgDust[][];


	public void loadDust() {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;
		if (imgDust == null) {
			imgDust = new Bitmap[2][5];
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 5; j++)
					imgDust[i][j] = GameCanvas.loadImage(("/e/d" + i) + (j + ".png"));
		}
		dustX = new int[2];
		dustY = new int[2];
		dustState = new int[2];
		dustState[0] = dustState[1] = -1;
	}


	public static boolean isPaint(int x, int y) {
		if (x < GameScr.cmx)
			return false;
		if (x > GameScr.cmx + GameScr.gW)
			return false;
		if (y < GameScr.cmy)
			return false;
		if (y > GameScr.cmy + GameScr.gH + 30)
			return false;
		return true;
	}


	public boolean startWaterSplash(int x, int y) {
		if (lowGraphic)
			return false;
		int i = wsState[0] == -1 ? 0 : 1;
		if (wsState[i] != -1)
			return false;
		wsState[i] = 0;
		wsX[i] = x;
		wsY[i] = y;
		return true;
	}


	// Catch key-down input and save in to Input Array
	public static long lastTimePress = 0;



	public static int keyAsciiPress;

	public void keyPressed(int keyCode) {
//		 Screen.test="key: "+keyCode;
		lastTimePress = System.currentTimeMillis();
		if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 122) || keyCode == 10 || keyCode == 8 || keyCode == 13 || keyCode == 32) {
			keyAsciiPress = keyCode;
		}
		mapKeyPress(keyCode);
	}

	public void mapKeyPress(int keyCode) {
//		if (Char.lockKey)
//			return;
		switch (keyCode) {
		case 0:
			keyHold[0] = true;
			keyPressed[0] = true;
			break;
			
		case 1:// 1
			keyHold[1] = true;
			keyPressed[1] = true;
			break;
			
		case -1:
			if (currentScreen instanceof GameScr)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[2] = true;
			keyPressed[2] = true;
			break;
			
		case 3:// 3
			keyHold[3] = true;
			keyPressed[3] = true;
			break;
			
		case -3:
			if (currentScreen instanceof GameScr)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[4] = true;
			keyPressed[4] = true;

			break;
			
		case 5:
			if (currentScreen instanceof GameScr)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[5] = true;
			keyPressed[5] = true;
			
			break;
			
		case -4:
			if (currentScreen instanceof GameScr)
				if (Char.myChar().isAttack) {

					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[6] = true;
			keyPressed[6] = true;
			break;
			
		case 7:// 7
			keyHold[7] = true;
			keyPressed[7] = true;
			break;
			
		case -2:
			if (currentScreen instanceof GameScr)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[8] = true;
			keyPressed[8] = true;
			break;
			
		case 9:// 9
			keyHold[9] = true;
			keyPressed[9] = true;
			break;

		case 12:
			keyHold[12] = true;
			keyPressed[12] = true;
			// Soft1
			break;
		case 13:
			keyHold[13] = true;
			keyPressed[13] = true;
			// Soft2
			break;
		case 14:
			keyHold[14] = true;
			keyPressed[14] = true;
			// Soft2
			break;

		}

		if (currentScreen != null) {
			if (currentDialog != null) {
				currentDialog.update();
				return;
			}else if( menu.showMenu){
				menu.updateMenuKey();
				return;
			}
			currentScreen.updateKey();

		}
	}

	// Catch key-up input and clear from Input Array
	public void keyReleased(int keyCode) {
		keyAsciiPress = 0;
		mapKeyRelease(keyCode);
	}

	public void mapKeyRelease(int keyCode) {

		switch (keyCode) {
		case 48:
			keyHold[0] = false;
			keyReleased[0] = true;
			return;
		case 49:// 1
			keyHold[1] = false;
			keyReleased[1] = true;
			return;
		case 51:// 3
			keyHold[3] = false;
			keyReleased[3] = true;
			return;
		case 55:// 7
			keyHold[7] = false;
			keyReleased[7] = true;
			return;
		case 57:// 9
			keyHold[9] = false;
			keyReleased[9] = true;
			return;
		case 42:
			keyHold[10] = false;
			keyReleased[10] = true;
			return; // Key [*]
		case 35:
			keyHold[11] = false;
			keyReleased[11] = true;
			return; // Key [#]
		case -6:
		case -21:
			keyHold[12] = false;
			keyReleased[12] = true;
			return; // Soft1
		case -7:
		case -22:
			keyHold[13] = false;
			keyReleased[13] = true;
			return; // Soft2
		case -5:
		case 10:
			keyHold[5] = false;
			keyReleased[5] = true;
			return; // [i]
		case -1:
		case -38:

			keyHold[2] = false;

			return; // UP
		case -2:
		case -39:

			keyHold[8] = false;
			return; // DOWN
		case -3:

			keyHold[4] = false;
			return; // LEFT
		case -4:

			keyHold[6] = false;
			return; // RIGHT
		}

	}
	
	public static boolean isHoldPress(){
		if(System.currentTimeMillis() - lastTimePress >= 800)
			return true;
		return false;
	}
	
	public static void pointerPressed(int x, int y) {
		isPointerDown = true;
		isPointerClick = true;
		lastTimePress = System.currentTimeMillis();
		pxFirst = x;
		pyFirst = y;
		pxLast = x ;
		pyLast = y ;
		px = x ;
		py = y ;
	}
	

	public static void pointerDragged(int x, int y) {
		if (model.Res.abs(x - pxLast) >= 10 || model.Res.abs(y - pyLast) >= 10){
			isPointerClick = false;
		}
		curPos++;
		if (curPos > 3)
			curPos = 0;
		arrPos[curPos] = new Position(x, y);
		px = x ;
		py = y ;
	
		// pXLast = x ;
		// pYLast = y ;
	}



	public static void pointerReleased(int x, int y) {
		isPointerDown = false;
		isPointerJustRelease = true;
		Screen.keyTouch = -1;
		px = x ;
		py = y ;
		
		
		// CScreen.clearKey();
		// curScr.pointerReleased(pX, pY);
	
	}

	public static boolean isPointerInGame(int x, int y, int w, int h) {
		
		int px1 = px + GameScr.cmx, py1 = GameScr.cmy + py;
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (px1 >= x && px1 <= x + w && py1 >= y && py1 <= y + h){
			return true;}
		return false;
	}

	public static boolean isPointerHoldIn(int x, int y, int w, int h) {

//		x *=MGraphics.zoomLevel;
//		y *=MGraphics.zoomLevel;
//		w *=MGraphics.zoomLevel;
//		h *=MGraphics.zoomLevel;
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (px >= x && px <= x + w && py >= y && py <= y + h){
			return true;}
		return false;
	}

	public static void clearKeyPressed() {
		isPointerJustRelease = false;
		for (int i = 0; i < keyPressed.length; i++)
			keyPressed[i] = false;
	}

	public static void clearKeyHold() {

		for (int i = 0; i < keyHold.length; i++)
			keyHold[i] = false;
	}



	public static void debug(String str, int index) {
	//	System.out.println(">>>>>> " + str + index);
	}


	public static String getMoneys(int m) {
		String str = "";
		int mm = m / 1000 + 1;
		for (int i = 0; i < mm; i++) {
			if (m >= 1000) {
				int a = m % 1000;
				if (a == 0)
					str = ".000" + str;
				else if (a < 10)
					str = ".00" + a + str;
				else if (a < 100)
					str = ".0" + a + str;
				else
					str = "." + a + str;
				m /= 1000;
			} else {
				str = m + str;
				break;
			}
		}
		return str;
	}
	
	public void getScreenSize() {
		// get size of screen
		GameScr.d = (w > h ? w : h) + 20;
//		if (w < h && Build.VERSION.SDK_INT >= 11) {
//			h = display.getWidth();
//			w = display.getHeight();
//		}

		w += (3 - w % 3);
		h += (3 - h % 3);
		
		if (com.team.njonline.GameMidlet.Model.compareTo("A101IT") == 0){// for archos 10.1 in
			w -= 44;
			
		}
//		else if (GameMidlet.Model.compareTo("GT-P7500") == 0) // for galaxy tab
//			h -= 48;
		System.out.println("w: " + w + ", h: " + h);

		rw = w;
		rh = h;
		
		if ((w * h) >= (1280 * 960)) {
			MGraphics.zoomLevel = 2;
			zoomLevel = 2;
		} else if ((w * h) >= (960 * 720)) {
			MGraphics.zoomLevel = 2;
			zoomLevel = 2;
		}else if ((w * h) >= (600 * 400)) {
			MGraphics.zoomLevel = 2;
			zoomLevel = 2;
		}else if ((w * h) < (600 * 400)){
			MGraphics.zoomLevel = 2;
			zoomLevel = 2;
		}
		Cout.println(getClass(), " zoomlevel lllll  "+ MGraphics.zoomLevel);
		w = w / MGraphics.zoomLevel;
		h = h / MGraphics.zoomLevel;
		hw = w / 2;
		hh = h / 2;
	}

	public static void clearPointerEvent()
	{
		isPointerJustRelease = isPointerDown = isPointerClick = false;
		Cout.println("trueeeeeeeeeeclearPointerEventeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}

	public static boolean isPointer(int x, int y, int w, int h) {
		if (!isPointerDown && !isPointerJustRelease){
			return false;
		}
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}

	public static void connect() {
//		GameMidlet.IP = "25.40.63.246";
//		GameMidlet.IP = "192.168.1.136";
//		GameMidlet.IP = "192.168.1.119"; // server Tuan
//		GameMidlet.IP = "192.168.1.7"; // server Hoa
//		GameMidlet.IP = "25.81.27.184"; // server Hoa
		com.team.njonline.GameMidlet.IP = /*"25.68.147.188";*/ "127.0.0.1"; //25.49.57.140//127.0.0.1// server Hoa25.68.147.188 //25.70.197.73
		//27.0.14.78T
//		GameMidlet.IP = "27.0.14.78"; // servoer Hoa25.68.147.188
		com.team.njonline.GameMidlet.PORT = 19158;
		// String host = "socket://" + GameMidlet.IP + ":" + GameMidlet.PORT;
		Session_ME.gI().connect(com.team.njonline.GameMidlet.IP, GameMidlet.PORT);
//		if(!LoginScr.isAutoLogin)
//			GameCanvas.startWaitDlg(mResources.LOGGING);
//		else
//			GameCanvas.startWaitDlg(mResources.LOGGING, true);
	}
	
	private void doResetToLoginScr() {
		GameCanvas.loginScr.switchToMe();
		try {
			Char.clearMyChar();
			GameScr.clearGameScr();
			GameScr.resetAllvector();
			GameCanvas.endDlg();
			InfoDlg.hide();
			GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
			GameScr.cmx = 100;
			GameCanvas.loadBG(TileMap.bgID);
//			GameScr.vParty.removeAllElements();
			GameScr.vFriend.removeAllElements();
			GameScr.vEnemies.removeAllElements();
			GameScr.vClan.removeAllElements();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static boolean paintBG;
	public static int gsskyHeight;
	public static int gsgreenField1Y, gsgreenField2Y, gshouseY, gsmountainY,
			bgLayer0y, bgLayer1y;
	public static Bitmap imgCloud, imgSun;
	public static Bitmap[] imgBorder = new Bitmap[3];
	public static int borderConnerW, borderConnerH, borderLineW, borderLineH, borderCenterW, borderCenterH;
	public static int cloudX[], cloudY[], sunX, sunY;

	public static void paintBGGameScr(MGraphics g) {
		// Paint the Sky
		if (paintBG && !lowGraphic) {
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScr.gW, gsskyHeight);
			// Paint the GreenField1
			if (typeBg >= 0 && typeBg <= 1) {
				if(GameCanvas.currentScreen == loginScr.selectCharScr){
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
							g.drawImage(imgBG[3], i, gsmountainY, 0);
					if (imgBG[2] != null)
						for (int i = -((GameScr.cmx >> 3) % 192); i < GameScr.gW; i += Image.getWidth(imgBG[2])/*192*/)
							g.drawImage(imgBG[2], i, gshouseY, 0);
					
					// // Paint the GreenField2
					if (imgBG[1] != null){
						
//						for (int i = -((GameScr.cmx >> 2) % 24); i < GameScr.gW; i += Image.getWidth(imgBG[1])/*24*/)
						for(int i = GameCanvas.w/2; i < GameCanvas.w + Image.getWidth(imgBG[1]); i += (Image.getWidth(imgBG[1])) ){
								
								g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);
							}
								
						for(int i = GameCanvas.w/2; i > -Image.getWidth(imgBG[1]); i -= (Image.getWidth(imgBG[1]) ) ){
							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);
						}
//					for(int j = 0; j > 0; j -= (Image.getWidth(imgBG[1]))){
//						graphic.drawImage(imgBG[1], j, gsgreenField2Y, MGraphics.BOTTOM | MGraphics.HCENTER);
//					}
//						for(int i = GameCanvas.w/2; i > 0; i += (Image.getWidth(imgBG[1]) - 100))
//							graphic.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);
//						for(int k = GameCanvas.w/2; k < GameCanvas.w; k -= Image.getWidth(imgBG[1]) )
//							graphic.drawImage(imgBG[1], k, gsgreenField2Y - 120, MGraphics.HCENTER | MGraphics.HCENTER);
						
					}
					if (imgBG[0] != null)
						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0])/*24*/)
							g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0);
					// // Paint Mountain
//					if (imgBG[3] != null)
//						for (int i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
//							graphic.drawImage(imgBG[3], i, gsmountainY, 0);
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
					// Paint House
					
				}else if(GameCanvas.currentScreen == GameCanvas.loginScr || GameCanvas.currentScreen == GameCanvas.languageScr){
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScr.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0);
//					if (imgBG[0] != null)
//						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
//							graphic.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
					// // Paint the GreenField2
					if (imgBG[2] != null)
						for (int i = -((GameScr.cmx >> 3) % Image.getWidth(imgBG[2])); i < GameScr.gW; i += Image.getWidth(imgBG[2]))
							g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0);
					if (imgBG[1] != null)
						for (int i = -((GameScr.cmx >> 2) % Image.getWidth(imgBG[1])); i < GameScr.gW; i += Image.getWidth(imgBG[1]))
							g.drawImage(imgBG[1], 0, gshouseY + 3, 0);
					// // Paint Mountain
					
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
					// Paint House
					
					if (imgBG[0] != null)
						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
				}
			

				// ==========================
			} else if (typeBg >= 2 && typeBg <= 6) {
//				System.out.println("PAINT BG GAME SCR");
//				if (imgSun != null) {
//					// Paint sun and cloud
//					graphic.drawImage(imgSun, sunX, sunY, 3);
//				}
//				if (imgCloud != null)
//					for (int i = 0; i < cloudX.length; i++)
//						graphic.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

//				if(typeBg!=2){
//					 Paint Mountain
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
							g.drawImage(imgBG[3], i, gsmountainY, 0);
					// Paint House
					if (imgBG[2] != null){
						for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY + 40, 0);
					}
					// Paint the GreenField2
					if (imgBG[1] != null){
						for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y - 250, 0);
					}
					// Paint the GreenField1
					if (imgBG[0] != null) {
						for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
							g.drawImage(imgBG[0], i, bgLayer0y, 0);
					}
//				}
				
			} else if (typeBg >= 7 && typeBg <= 12) {
				g.setColor(skyColor);
				g.fillRect(0, 0, GameScr.gW, GameScr.gH);
				if (typeBg != 8 && imgBG[3] != null) {

					for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
						if (typeBg == 11 || typeBg == 12) {
							g.drawImage(imgBG[3], i, GameScr.gH - MGraphics.getImageHeight(imgBG[3]), 0);

						} else
							g.drawImage(imgBG[3], i, gsmountainY, 0);

				}

				if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null) {

					if (TileMap.mapID == 45)
						g.drawImage(imgBG[2], GameScr.gW, gshouseY, 0);
					else
						for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY, 0);
				}
				if (typeBg != 11 && typeBg != 12 && imgBG[1] != null) {

					if (TileMap.mapID != 52)
						for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y, 0);

				}
				if (TileMap.mapID == 45 || TileMap.mapID == 55) {
					g.setColor(0x110000);
					g.fillRect(0, bgLayer0y + 20, GameScr.gW, GameScr.gH);
				}

				if (imgBG[0] != null) {

					for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
						g.drawImage(imgBG[0], i, bgLayer0y, 0);

				}

				if (imgCloud != null) {

					for (int i = 0; i < 2; i++)
						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

				}
			}
			// ==============================
		} else {
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScr.gW, GameScr.gH);
		}
	}

	public static void paintBGGameScr(MGraphics g, boolean Uclip) {
		// Paint the Sky
		if (paintBG && !lowGraphic) {		
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScr.gW, gsskyHeight,Uclip);
			// Paint the GreenField1
			if (typeBg >= 0 && typeBg <= 1) {
				if(GameCanvas.currentScreen == loginScr.selectCharScr||GameCanvas.currentScreen ==CreatCharScr.gI()){
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScr.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
					if (imgBG[2] != null)
						for (int i = -((GameScr.cmx >> 3) % 192); i < GameScr.gW; i += Image.getWidth(imgBG[2])/*192*/)
							g.drawImage(imgBG[2], i, gshouseY, 0,Uclip);
					
					// // Paint the GreenField2
					if (imgBG[1] != null){
						
//						for (int i = -((GameScr.cmx >> 2) % 24); i < GameScr.gW; i += Image.getWidth(imgBG[1])/*24*/)
						for(int i = GameCanvas.w/2; i < GameCanvas.w + Image.getWidth(imgBG[1]); i += (Image.getWidth(imgBG[1])) ){
								
								g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER,Uclip);
							}
								
						for(int i = GameCanvas.w/2; i > -Image.getWidth(imgBG[1]); i -= (Image.getWidth(imgBG[1]) ) ){
							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER,Uclip);
						}
//					for(int j = 0; j > 0; j -= (Image.getWidth(imgBG[1]))){
//						graphic.drawImage(imgBG[1], j, gsgreenField2Y, MGraphics.BOTTOM | MGraphics.HCENTER);
//					}
//						for(int i = GameCanvas.w/2; i > 0; i += (Image.getWidth(imgBG[1]) - 100))
//							graphic.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);
//						for(int k = GameCanvas.w/2; k < GameCanvas.w; k -= Image.getWidth(imgBG[1]) )
//							graphic.drawImage(imgBG[1], k, gsgreenField2Y - 120, MGraphics.HCENTER | MGraphics.HCENTER);
						
					}
					if (imgBG[0] != null)
						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0])/*24*/)
							g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0,Uclip);
					// // Paint Mountain
//					if (imgBG[3] != null)
//						for (int i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
//							graphic.drawImage(imgBG[3], i, gsmountainY, 0);
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3,Uclip);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);
					// Paint House
					
				}else if(GameCanvas.currentScreen == GameCanvas.loginScr || GameCanvas.currentScreen == GameCanvas.languageScr){
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScr.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
//					if (imgBG[0] != null)
//						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
//							graphic.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
					// // Paint the GreenField2
					if (imgBG[2] != null)
						for (int i = -((GameScr.cmx >> 3) % Image.getWidth(imgBG[2])); i < GameScr.gW; i += Image.getWidth(imgBG[2]))
							g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0,Uclip);
					if (imgBG[1] != null)
						for (int i = -((GameScr.cmx >> 2) % Image.getWidth(imgBG[1])); i < GameScr.gW; i += Image.getWidth(imgBG[1]))
							g.drawImage(imgBG[1], 0, gshouseY + 3, 0,Uclip);
					// // Paint Mountain
					
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3,Uclip);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);
					// Paint House
					
					if (imgBG[0] != null)
						for (int i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScr.gW; i += Image.getWidth(imgBG[0]))
							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0,Uclip);
				}
			

				// ==========================
			} else if (typeBg >= 2 && typeBg <= 6) {
//				System.out.println("PAINT BG GAME SCR");
//				if (imgSun != null) {
//					// Paint sun and cloud
//					graphic.drawImage(imgSun, sunX, sunY, 3);
//				}
//				if (imgCloud != null)
//					for (int i = 0; i < cloudX.length; i++)
//						graphic.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

//				if(typeBg!=2){
//					 Paint Mountain
					if (imgBG[3] != null)
						for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
					// Paint House
					if (imgBG[2] != null){
						for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY + 40, 0,Uclip);
					}
					// Paint the GreenField2
					if (imgBG[1] != null){
						for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y - 250, 0,Uclip);
					}
					// Paint the GreenField1
					if (imgBG[0] != null) {
						for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
							g.drawImage(imgBG[0], i, bgLayer0y, 0,Uclip);
					}
//				}
				
			} else if (typeBg >= 7 && typeBg <= 12) {
				g.setColor(skyColor);
				g.fillRect(0, 0, GameScr.gW, GameScr.gH,Uclip);
				if (typeBg != 8 && imgBG[3] != null) {

					for (int i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
						if (typeBg == 11 || typeBg == 12) {
							g.drawImage(imgBG[3], i, GameScr.gH - MGraphics.getImageHeight(imgBG[3]), 0,Uclip);

						} else
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);

				}

				if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null) {

					if (TileMap.mapID == 45)
						g.drawImage(imgBG[2], GameScr.gW, gshouseY, 0,Uclip);
					else
						for (int i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScr.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY, 0,Uclip);
				}
				if (typeBg != 11 && typeBg != 12 && imgBG[1] != null) {

					if (TileMap.mapID != 52)
						for (int i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y, 0,Uclip);

				}
				if (TileMap.mapID == 45 || TileMap.mapID == 55) {
					g.setColor(0x110000);
					g.fillRect(0, bgLayer0y + 20, GameScr.gW, GameScr.gH,Uclip);
				}

				if (imgBG[0] != null) {

					for (int i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
						g.drawImage(imgBG[0], i, bgLayer0y, 0,Uclip);

				}

				if (imgCloud != null) {

					for (int i = 0; i < 2; i++)
						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);

				}
			}
			// ==============================
		} else {
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScr.gW, GameScr.gH,Uclip);
		}
	}

	
	public static int typeBg = -1;
	
	public static void loadBG(int typeBG) {
		int deltaLayer1 = 0;
		int deltaLayer2 = 0;
		int deltaLayer3 = 0;
		typeBg = typeBG;
		switch (typeBG) {
		case 0:
			// typeBg = 0;
			
			break;
		case 2:
			deltaLayer1 = 16;
			deltaLayer2 = 10;
			deltaLayer3 = 10;
			// typeBg = 2;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 3:
			// typeBg = 3;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 4:
			// typeBg = 4;
			deltaLayer1 = 9;
			deltaLayer2 = 6;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 5:
			// typeBg = 5;
			bgSpeed = new int[] { 1, 1, 1, 1 };
			break;
		case 6:
			// typeBg = 6;
			deltaLayer1 = 12;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 12:
			// typeBg = 12;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 11:
			// typeBg = 11;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 10:
			// typeBg = 10;
			bgSpeed = new int[] { 1, 1, 1, 1 };
			break;
		case 9:
			// typeBg = 9;
			deltaLayer1 = 16;
			deltaLayer2 = 10;
			deltaLayer3 = 10;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 8:
			// typeBg = 8;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 7:
			// typeBg = 7;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		}
		
		GameCanvas.skyColor = StaticObj.SKYCOLOR[typeBg];

		try {
			if (!lowGraphic) {
				imgBG = new Bitmap[4];
				imgBGWidth = new int[4];
				for (int i = 0; i < 4; i++) {
					try {
						if (StaticObj.TYPEBG[typeBg][i] != -1) {
							// System.out.println("LOAD " + "/bg/bg" + i +
							// StaticObj.TYPEBG[typeBg][i] + ".png");
							imgBG[i] = GameCanvas.loadImage("/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png");
							if(typeBg == 2){
								System.out.println("PATH IMAGE ::: /bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png " +imgBG[i]);
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (imgBG[i] != null)
						imgBGWidth[i] = MGraphics.getImageWidth(imgBG[i]);
				}

				if (typeBg == 10) {
					imgBG[1] = GameCanvas.loadImage("/bg/bg09.png");
					imgBG[2] = GameCanvas.loadImage("/bg/bg09.png");
					imgBGWidth[1] = MGraphics.getImageWidth(imgBG[1]);
					imgBGWidth[2] = MGraphics.getImageWidth(imgBG[2]);
				}
				if (typeBg == 12) {
					imgBG[3] = GameCanvas.loadImage("/bg/bg39.png");
					imgBGWidth[3] = MGraphics.getImageWidth(imgBG[3]);
				}
			}

			if (typeBg >= 0 && typeBg <= 1) {
				imgCloud = GameCanvas.loadImage("/bg/cl0.png");
				imgSun = GameCanvas.loadImage("/bg/sun0.png");
			} else {
				imgCloud = null;
				imgSun = null;
			}
//			if (typeBg == 2) {
//				imgCloud = GameCanvas.loadImage("/bg/cl1.png");
//				imgSun = GameCanvas.loadImage("/bg/sun1.png");
//			}
			if (typeBg == 7 || typeBg == 11 || typeBg == 12) {
				if (TileMap.mapID == 20) {
					imgCloud = null;
				} else
					imgCloud = GameCanvas.loadImage("/bg/cl0.png");
			}
		} catch (Exception e) {
			System.out.println("BG ERR");
		}

		paintBG = false;
		if (!lowGraphic) {
			paintBG = true;
//			if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
//				gsskyHeight = GameScr.gH - (MGraphics.getImageHeight(imgBG[0]) + MGraphics.getImageHeight(imgBG[1]) + MGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
//			if (imgBG[0] != null)
//				gsgreenField1Y = GameScr.gH - MGraphics.getImageHeight(imgBG[0]);
//			if (imgBG[1] != null)
//				gsgreenField2Y = gsgreenField1Y - MGraphics.getImageHeight(imgBG[1]);
//			if (imgBG[2] != null)
//				gshouseY = gsgreenField2Y - MGraphics.getImageHeight(imgBG[2]);
//			if (imgBG[3] != null)
//				gsmountainY = gsgreenField2Y - MGraphics.getImageHeight(imgBG[3]) /*- 10*/;
			if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
				gsskyHeight = GameScr.gH - (MGraphics.getImageHeight(imgBG[0]) + MGraphics.getImageHeight(imgBG[1]) + MGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
			if (imgBG[0] != null)
				gsgreenField1Y = GameScr.gH - Image.getHeight(imgBG[0]);
			if (imgBG[1] != null)
				gsgreenField2Y = gsgreenField1Y - Image.getHeight(imgBG[1]);
			if (imgBG[2] != null)
				gshouseY = gsgreenField2Y - Image.getHeight(imgBG[2]);
			if (imgBG[3] != null)
				gsmountainY = gsgreenField2Y - Image.getHeight(imgBG[3]) /*- 10*/;
			// ================
			if (typeBg >= 2 && typeBg <= 12) {
				int tem = GameScr.gH - MGraphics.getImageHeight(imgBG[0]);
				bgLayer0y = tem;
				if (imgBG[1] != null)
					tem = tem - MGraphics.getImageHeight(imgBG[1]) + deltaLayer1;
				bgLayer1y = tem;
				if (imgBG[3] != null)
					tem = tem - MGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
				gsmountainY = tem;
				//
				gsskyHeight = tem;

				if (imgBG[2] != null)
					gshouseY = bgLayer1y - MGraphics.getImageHeight(imgBG[2]) + deltaLayer2;
				if(typeBg == 2)
					gsskyHeight = GameCanvas.h;
			}
			
			if(typeBG == 3){
				int tem = GameScr.gH - MGraphics.getImageHeight(imgBG[0]);
				bgLayer0y = tem;
				if (imgBG[1] != null)
					tem = tem - MGraphics.getImageHeight(imgBG[1])  + 30+ deltaLayer1;
				bgLayer1y = tem;
				if (imgBG[3] != null)
					tem = tem - MGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
				gsmountainY = tem;
				//
				gsskyHeight = tem;

				if (imgBG[2] != null)
					gshouseY = bgLayer1y - MGraphics.getImageHeight(imgBG[2]) +20 + deltaLayer2;
				if(typeBg == 2)
					gsskyHeight = GameCanvas.h;

			}

		}
		int bgDelta = 0;
		if (typeBg >= 2 && typeBg <= 12) {
			bgDelta = (2 * GameScr.gH / 3) - bgLayer1y;
		} else {
			bgDelta = (2 * GameScr.gH / 3) - gsgreenField2Y;
		}
		if (bgDelta < 0)
			bgDelta = 0;
		if (TileMap.mapID == 48 && TileMap.mapID == 51)
			bgLayer0y += bgDelta;
		if (typeBg >= 2 && typeBg <= 6)
			bgLayer1y += bgDelta;

		gsskyHeight += bgDelta;
		gsgreenField1Y += bgDelta;
		gsgreenField2Y += bgDelta;
		gshouseY += bgDelta;
		gsmountainY += bgDelta;
		sunX = 3 * GameScr.gW / 4;
		sunY = gsskyHeight / 3;
		cloudX = new int[2];
		cloudY = new int[2];
		cloudX[0] = GameScr.gW / 3;
		cloudY[0] = gsskyHeight / 2 - 8;
		cloudX[1] = 2 * GameScr.gW / 3;
		cloudY[1] = gsskyHeight / 2 + 8;
		if(typeBg == 2){
			sunY = gsskyHeight / 5;
			cloudX = new int[5];
			cloudY = new int[5];
			cloudX[0] = GameScr.gW / 3;
			cloudY[0] = gsskyHeight / 3 - 35;
			cloudX[1] = 3 * GameScr.gW / 4;
			cloudY[1] = gsskyHeight / 3 + 12;
			cloudX[2] = GameScr.gW / 3 -15;
			cloudY[2] = gsskyHeight / 3 + 12;
			cloudX[3] =  GameScr.gW / + 15;
			cloudY[3] = gsskyHeight / 2 + 12;
			cloudX[4] = 2 * GameScr.gW / 3 + 25;
			cloudY[4] = gsskyHeight / 3 + 12;
		}
		
		if (!lowGraphic) {
			if (typeBg == 8)
				bgLayer0y = bgLayer1y = GameScr.gH2 - 50;
			if (typeBg == 10) {
				if (imgBG[3] != null)
					gsmountainY = gshouseY - MGraphics.getImageHeight(imgBG[3]);
			}
			if (typeBg == 11 || typeBg == 12) {
				gsmountainY = 0;

			}
		}

	}

	
	private boolean resetToLoginScr;

	public void resetToLoginScr() {
		isLoading = false;
		resetToLoginScr = true;
	}

	
	public static Bitmap loadImage(String path) {
		path = "x" + zoomLevel + path;
		return Image.loadImageFromAsset(path);
	}

	
	public static boolean isPointSelect(int x, int y, int w, int h) {

		if (!isPointerClick) {
			return false;
		}
		return isPoint(x, y, w, h);
	}
	
	public static boolean isPoint(int x, int y, int w, int h) {
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
	public static final int cLogout = 9990;
	@Override
	public void perform(int idAction, Object p) {
		Cout.println(getClass(), " idAction  "+idAction);
		switch (idAction) {
		case 8882:
			currentDialog = null;
			break;
		case cLogout:
			LoginScr.isLogout = true;
			Service.gI().PlayerOut();
			LoginScr.gI().switchToMe();
			currentDialog = null;
			break;
		}

		Cout.println(getClass(), " currentDialog  "+currentDialog);
	}

	public static void resetTrans(MGraphics g){
		  g.translate(-g.getTranslateX(), -g.getTranslateY());
		  g.setClip(0, 0, w, h);  
	}
}