package com.thdgaming.naruto;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

import lib.MapKey;

public class MyInputProcessor implements InputProcessor {

    private float zoom;
    private OrthographicCamera camera;

    public MyInputProcessor(OrthographicCamera camera, float zoom) {
        this.camera = camera;
        this.zoom = zoom;
    }

    @Override
    public boolean scrolled(int amount) {

        // Zoom out
        if (amount > 0 && zoom < 1) {
            zoom += 0.01f;
        }

        // Zoom in
        if (amount < 0 && zoom > 0.6) {
            zoom -= 0.01f;
        }

        return true;
    }

    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        int k = MapKey.map(keycode);
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)
                || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) {
            if (keycode == 9) {
                k = 64;
            }
        }

        GameCanvas.instance.keyPressed(k);
        // md.temCanvas.keyPressed(k);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        int k = MapKey.map(keycode);
        GameCanvas.instance.keyReleased(k);
        // md.temCanvas.keyReleased(k);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
                             int button) {

        Vector3 touch = new Vector3(screenX, screenY, 0);
        camera.unproject(touch);
        int delX = (int) touch.x - screenX;
        int delY = (int) touch.y - screenY;
        GameCanvas.pointerPressed((int) (screenX + delX)/ MGraphics.zoomLevel,
                (int) (screenY + delY)/ MGraphics.zoomLevel);
        // gmidlet.instance.canvas.onPointerPressed((screenX+delX)/MGraphics.zoomLevel,
        // (screenY+ delY)/MGraphics.zoomLevel);
        // md.temCanvas.pointerPressed(screenX+delX, screenY+ delY);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        Vector3 touch = new Vector3(screenX, screenY, 0);
        camera.unproject(touch);
        int delX = (int) touch.x - screenX;
        int delY = (int) touch.y - screenY;
        GameCanvas.pointerReleased((int) (screenX + delX)/ MGraphics.zoomLevel,
                (int) (screenY + delY)/ MGraphics.zoomLevel);
        // gmidlet.instance.canvas.onPointerReleased((screenX+delX)/MGraphics.zoomLevel,
        // (screenY+ delY)/MGraphics.zoomLevel);
        // md.temCanvas.pointerReleased((int)(screenX+delX),(int)(
        // screenY+delY));

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        Vector3 touch = new Vector3(screenX, screenY, 0);
        camera.unproject(touch);
        int delX = (int) touch.x - screenX;
        int delY = (int) touch.y - screenY;
        GameCanvas.pointerDragged((int) (screenX + delX)/ MGraphics.zoomLevel ,
                (int) (screenY + delY)/ MGraphics.zoomLevel);
        // gmidlet.instance.canvas.onPointerDragged((screenX+delX)/MGraphics.zoomLevel,
        // (screenY+ delY)/MGraphics.zoomLevel);
        // md.temCanvas.pointerDragged((int)(screenX+delX),(int)(
        // screenY+delY));
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }


}
