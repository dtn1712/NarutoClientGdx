/*
 * DrawRegion class
 * Author : Giang.LC
 * create: 08-03-2011
 *
 * How to use :
 * in class need DrawRegion, import DrawRegion class and insert code:
 * "private DrawRegion  g = new DrawRegion(canvas);"
 * when use it just right like j2me, delete anchor parameter:
 * g.drawRegion( Bitmap img, int x_src, int y_src, int width, int height, int x_dest, int y_dest);
 * or
 * g.drawRegion( Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest);
 *
 * g.drawChar(char character, int x, int y);
 *
 * g.drawString(String str, int x, int y);
 */

package com.thdgaming.naruto;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import lib.Bitmap;
import lib.Graphics;
import model.Image;
import model.Paint;

public class MGraphics {

    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT90 = 5;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;

    public static final int TRANS_MIRROR = 2;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_MIRROR_ROT180 = 1;
    public static final int TRANS_MIRROR_ROT270 = 4;

    // for graphics anchor
    public static int TOP = 16;
    public static int BOTTOM = 32;
    public static int VCENTER = 2;

    public static int LEFT = 4;
    public static int RIGHT = 8;
    public static int HCENTER = 1;

    private Graphics graphic;
    private int translateX = 0, translateY = 0;
    public static int zoomLevel = 1;
    public Paint paint;

    public MGraphics() {
        paint = new Paint();
    }

    public MGraphics(SpriteBatch spriteBatch) {
        graphic = new Graphics(spriteBatch);
    }

    // draw without transform
    public void drawRegion(Bitmap img, int x_src, int y_src, int width, int height, int x_dest, int y_dest) {
        // create bitmap in area draw
        x_src *= zoomLevel;
        y_src *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;
        x_dest *= zoomLevel;
        y_dest *= zoomLevel;
        Bitmap image = Bitmap.createBitmap(img, x_src, y_src, width, height);
        graphic.drawBitmap(image.image, x_dest, y_dest, null);
    }

    // draw with transform
    public void drawRegion(Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest) {
//		x_src *= zoomLevel;
//		y_src *= zoomLevel;
//		width *= zoomLevel;
//		height *= zoomLevel;
//		x_dest *= zoomLevel;
//		y_dest *= zoomLevel;
        drawRegion(img, x_src, y_src, width, height, transform, x_dest, y_dest, 20);
    }

    public void drawRegion(Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor) {

        x_src *= zoomLevel;
        y_src *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;
        x_dest *= zoomLevel;
        y_dest *= zoomLevel;

        if (img != null && img.image != null)
            graphic.drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, Anchor, false);
    }

    public void drawRegion(Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor, Boolean isClip) {

        x_src *= zoomLevel;
        y_src *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;
        x_dest *= zoomLevel;
        y_dest *= zoomLevel;

        if (img != null && img.image != null)
            graphic.drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, Anchor, isClip);
    }

    // draw image
    public void drawImage(Bitmap image, int x, int y) {
        // TODO Auto-generated method stub
        x *= zoomLevel;
        y *= zoomLevel;
        graphic.drawBitmap(image.image, x, y, null);
    }

    public void drawImage(Bitmap image, int x, int y, int anchor, boolean isUclip) {
        // TODO Auto-generated method stub
        if (image == null || image.image == null) return;
        x *= zoomLevel;
        y *= zoomLevel;
        graphic.drawRegion(image.image, 0, 0, image.width, image.height, 0, x, y, anchor, isUclip);
    }

    // draw image with anchor
    public void drawImage(Bitmap image, int x, int y, int anchor) {
        // TODO Auto-generated method stub

        x *= zoomLevel;
        y *= zoomLevel;
        if (image != null && image.image != null)
            graphic.drawRegion(image.image, 0, 0, image.width, image.height, 0, x, y, anchor, false);
    }

    public void drawString(String str, int x, int y) {
    }

    public void drawString(String str, int x, int y, int Anchor, Paint p) {
    }

    public void drawString(String str, int x, int y, int Anchor) {


    }

    public void fillRect(int xLeft, int yTop, int width, int height) {

        xLeft *= zoomLevel;
        yTop *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;
        int right = width;
        int bottom = height;
//		graphic.fillRect(x, y, w, h, uClip);
        graphic.fillRect(xLeft, yTop, right, bottom, false);
    }

    public void fillRect(int xLeft, int yTop, int width, int height, Boolean isUclip) {

        xLeft *= zoomLevel;
        yTop *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;
        int right = width;
        int bottom = height;
//		graphic.fillRect(x, y, w, h, uClip);
        graphic.fillRect(xLeft, yTop, right, bottom, isUclip);
    }

    public void drawRect(int xLeft, int yTop, int width, int height) {
        xLeft *= zoomLevel;
        yTop *= zoomLevel;
        width *= zoomLevel;
        height *= zoomLevel;

        int right = width;
        int bottom = height;
        graphic.drawRect(xLeft, yTop, right, bottom, paint);

    }

    public void setColor(int color) {
        graphic.setColor(color);
    }

    public void setColor(int RGB, int phantramOpacity) {
        graphic.setColor(RGB);
        graphic.setAlpha(256 - phantramOpacity * 255 / 100);
    }

    public void disableBlending() {
        graphic.disableBlending();
    }

    public void setColor(int r, int g, int b) {
    }

    public void drawLine(int x1, int y1, int x2, int y2) {

        x1 *= zoomLevel;
        y1 *= zoomLevel;
        x2 *= zoomLevel;
        y2 *= zoomLevel;
        graphic.drawLine(x1, y1, x2, y2, false);
    }

    public void drawLine(int x1, int y1, int x2, int y2, Boolean isUclip) {

        x1 *= zoomLevel;
        y1 *= zoomLevel;
        x2 *= zoomLevel;
        y2 *= zoomLevel;
        graphic.drawLine(x1, y1, x2, y2, isUclip);
    }

    public void translate(int x, int y) {
        x *= zoomLevel;
        y *= zoomLevel;
        graphic.translate((float) x, (float) y);
        translateX += x;
        translateY += y;

    }

    public int getTranslateX() {
        return translateX / zoomLevel;
    }

    public int getTranslateY() {
        return translateY / zoomLevel;
    }

    public void setClip(int x, int y, int Width, int Height) {
        x *= zoomLevel;
        y *= zoomLevel;
        Width *= zoomLevel;
        Height *= zoomLevel;
        graphic.setClip(x, y, Width, Height);
    }


    public void drawRoundRect(int x, int y, int Width, int Height, int rX, int rY) {
        x *= zoomLevel;
        y *= zoomLevel;
        Width *= zoomLevel;
        Height *= zoomLevel;
        for (int i = 0; i < zoomLevel; i++) {
            graphic.drawRoundRect(x + i, y + i, Width, Height, rX, rY, true);
        }
    }

    public void fillRoundRect(int x, int y, int Width, int Height, int rX, int rY) {
        x *= zoomLevel;
        y *= zoomLevel;
        Width *= zoomLevel;
        Height *= zoomLevel;
        rX *= zoomLevel;
        rY *= zoomLevel;
        graphic.fillRoundRect(x, y, Width, Height, rX, rY, true);
    }

    public static int getImageHeight(Bitmap image) {
        return Image.getHeight(image);
    }

    public static int getImageWidth(Bitmap image) {
        return Image.getWidth(image);
    }

    public void begin() {
        graphic.spriteBatch.begin();
    }

    public void end() {
        graphic.isClip = false;
        graphic.translateX = 0;
        graphic.translateY = 0;
        graphic.isTranslate = false;
        graphic.isClip = false;
        graphic.spriteBatch.end();
    }

    public void drawString(String st, int x, float y, BitmapFont font, HAlignment al) {
        x *= zoomLevel;
        y *= zoomLevel;
        graphic.drawString(st, x, y, font, al, false);
    }

    public void drawString(String st, int x, float y, BitmapFont font, HAlignment al, Boolean isUclip) {
        if (st == null) return;
        x *= zoomLevel;
        y *= zoomLevel;
        graphic.drawString(st, x, y, font, al, isUclip);
    }

    public Graphics getGraphic() {
        return graphic;
    }
}
