package utils;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.IOException;

import network.Message;
import network.Session;

public class IOUtils
{
    private static final ObjectMapper objectMapper = new ObjectMapper(new MessagePackFactory());

    public static void writeMessage(byte cmd, Object data)
    {
        try {
            Message response = new Message(cmd);
            byte[] finalData = (data instanceof byte[]) ? (byte[]) data : objectMapper.writeValueAsBytes(data);
            response.getDos().write(finalData);

            Session.getInstance().sendMessage(response);

            response.cleanup();
        } catch (Exception e) {
            Gdx.app.error(IOUtils.class.getName(), "[writeMessage] Exception", e);
        }
    }

    public static void writeMessage(byte cmd)
    {
        try {
            Message response = new Message(cmd);
            Session.getInstance().sendMessage(response);
            response.cleanup();
        } catch (Exception e) {
            Gdx.app.error(IOUtils.class.getName(), "[writeMessage] Exception", e);
        }
    }

    public static Object readMessage(Message message, Class clazz) throws IOException {
        int length = message.getDis().available();
        byte[] data = new byte[length];
        message.getDis().readFully(data);
        return objectMapper.readValue(data, clazz);
    }
}
